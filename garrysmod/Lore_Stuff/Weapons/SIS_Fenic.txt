
(KITS LOG)

"A small weapon for one of your size" they told me. What they didn't tell me is this
little thing can fire as fast as i can pull the trigger, it has a 5 round burst when
i flick the switch on the reverse side of the grip, it recharges this burst over time
too, so i can use it for free if im sparing with it. Other weapons aren't that comfortable
to hold for me but i don't complain about it, however something thats comfortable AND
it's got some stopping power behind it? I'm not gonna say no.