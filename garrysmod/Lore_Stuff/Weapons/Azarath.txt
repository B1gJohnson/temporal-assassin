
(KITS LOG)

The red line across the cosmos, a collective of Azathoth and Shubnigurath. Azarath.

This thing is as light as a feather, i can pull the string back with little to no issues
despite my lack upper body strength.

Any arrows hitting a surface will release what i can only assume is a cosmic violent flesh
eating parasite, i'm glad it doesn't hurt me if anything.

Releasing the string at just the right time gives the arrows an incredible power and
launches them much faster, hitting things where it hurts with this 'Perfect Shot'
creates an even larger cosmic flesh eating cloud than usual too.

Putting some magic into the weapon seems to work too, it takes quite a lot but it turns the
arrows into a large pulling force and freezes anyone caught in them for a good couple of
seconds, the normal shots dont seem to freeze while i'm shifting temporality either, but the
Magic ones do, i guess it let's me setup traps of some kind.

PS:
If i focus a little i can turn the arrow into a temporal arrow, it lets me freeze almost anything
i want, once the effect is over it'll return to its natural movement, along with anything else that
hit it during the freeze time.