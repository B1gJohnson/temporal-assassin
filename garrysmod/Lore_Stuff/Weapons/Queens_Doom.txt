
(KITS LOG)

This thing is lovely, explosive 10 second timed bolts that dig into anything i fire them at. I can
detonate them at will too and the more bolts i attach the more crazy the explosion will be in the
end, It obliterates almost anything with about 4 bolts total, maybe 5 if i want to be sure.

I never really was too crazy about crossbows during my time with weaponry, i always found them to be inferior
to bullets, Why use a steel rod with a pointed tip when you can use a peice of lead or metal with a pointed tip
that travels marginally faster? However put on an explosive tip that digs into flesh that you can't even rip out
without causing yourself incredible pain and you can blow it up at will? You're going to make me want to use a 
crossbow.

PS:
Theres a strange idea i had recently, the adhesive on the tip of the bolts to keep the explosive on is
actually re-useable, taking the tip off of one of the bolts, recasing it to a larger size and then sticking
that onto the end of another base bolt actually works. It lets me fire off basically 2 queens doom bolts at
the same time, however the added weight of the bigger explosive shell and powder has the bolt itself drop
much faster after firing, it's also a little heavier so i can't reload the crossbow as fast as single bolts.
Overall it outputs more technical "bolts per second" than if i was using single bolts anyway, but if i miss
my target i'm wasting extra ammunition, gotta watch my shots.