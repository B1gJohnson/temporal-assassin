
(KITS LOG)

This... 'Thing', it comes from a dark place, i can hear it when i bring it close, some
kind of dreaded force hell bent on destroying everything it can find. However that's just
it's origins, the weapon itself is a sniper rifle with an incredibly powerful blast, i don't
know how it achieves this but aiming down the scope for a short while makes the whispers louder
and louder until eventually i can hear something, this seems to give the weapon a burst of incredible
power, anything i hit with it while charged seems to create a sort of vortex with a sigil, anything near
the vortex isn't long for this world, i have to hit a living entity for it to work though.

The look of this thing is out of this world, i can't even begin to fathom...