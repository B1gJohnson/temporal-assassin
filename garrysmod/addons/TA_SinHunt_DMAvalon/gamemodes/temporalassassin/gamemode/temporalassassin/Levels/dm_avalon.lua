		
if CLIENT then return end

util.AddNetworkString("SIN_UpdateCount")
util.AddNetworkString("SIN_AskForGoal")
util.AddNetworkString("SIN_SinOnMap")

function SIN_AskForGoal_LIB( len, pl )

	net.Start("SIN_UpdateCount")
	net.WriteUInt(LAST_SIN_COUNT,6)
	net.WriteUInt(SIN_COUNT_GOAL,6)
	net.Broadcast()

end
net.Receive("SIN_AskForGoal",SIN_AskForGoal_LIB)

GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true

GLOB_NO_FENIC = false
GLOB_NO_SINS = true

GLOB_SINHUNTER_SPAWN1 = false
GLOB_SINHUNTER_SPAWN2 = false
GLOB_SINHUNTER_SPAWNNPCS = 0
GLOB_SINHUNTER_ENEMYCOUNT = 0
GLOB_SINFUL_ENEMY_COUNT = 0

SIN_SPAWN_SPEED = 5
SIN_SPAWN_MAX = 40
SIN_ENEMIES_KILLED = 0
SIN_SINSPAWN_THRESHOLD = 20

local SPAWN_DIFFICULTIES = {}
SPAWN_DIFFICULTIES[1] = {5,30,20}
SPAWN_DIFFICULTIES[2] = {4,40,25}
SPAWN_DIFFICULTIES[3] = {3,50,30}
SPAWN_DIFFICULTIES[4] = {3,50,30}
SPAWN_DIFFICULTIES[5] = {2.5,60,35}
SPAWN_DIFFICULTIES[6] = {2,60,35}
SPAWN_DIFFICULTIES[7] = {1.5,60,35}
SPAWN_DIFFICULTIES[8] = {1.5,70,40}
SPAWN_DIFFICULTIES[9] = {1,80,45}
SPAWN_DIFFICULTIES[10] = {1,100,50}
SPAWN_DIFFICULTIES[11] = {1,120,50}
SPAWN_DIFFICULTIES[12] = {1,120,50} --[[ Just incase more difficulties ARE added ]]
SPAWN_DIFFICULTIES[13] = {1,120,50}
SPAWN_DIFFICULTIES[14] = {1,120,50}

GLOB_SINTYPE = math.random(1,5)

--[[
This defines what enemies will spawn for this sin hunter mission.

1 = Combine
2 = Zombies
3 = Antlions
4 = HL:S Humans
5 = HL:S Aliens

You can also define it as GLOB_SINTYPE = math.random(1,5) to make it random everytime its played
]]

local SIN_CLASSES = {}

SIN_CLASSES[1] = {
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s"}

if IsMounted("ep2") then
	SIN_CLASSES[1] = {
	"npc_metropolice",
	"npc_combine_s",
	"npc_metropolice",
	"npc_combine_s",
	"npc_metropolice",
	"npc_combine_s"}
end

SIN_CLASSES[2] = {
"npc_zombie",
"npc_zombie",
"npc_zombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_poisonzombie"}

SIN_CLASSES[3] = {
"npc_antlion"}

if IsMounted("ep2") then
	SIN_CLASSES[3] = {
	"npc_antlion",
	"npc_antlion",
	"npc_antlion",
	"npc_antlion_worker"}
end

SIN_CLASSES[4] = SIN_CLASSES[1]
SIN_CLASSES[5] = SIN_CLASSES[2]

if IsMounted("hl1") then

	SIN_CLASSES[4] = {
	"monster_human_grunt"}
	
	SIN_CLASSES[5] = {
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullsquid",
	"monster_houndeye",
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullchicken",
	"monster_houndeye"}
	
end

local BIGSIN_CLASSES = {}

BIGSIN_CLASSES[1] = {
"npc_combine_s"}

if IsMounted("ep2") then
	BIGSIN_CLASSES[1] = {
	"npc_hunter"}
end

BIGSIN_CLASSES[2] = {
"npc_poisonzombie"}

if IsMounted("ep2") or IsMounted("episodic") then
	BIGSIN_CLASSES[2] = {
	"npc_zombine"}
end

BIGSIN_CLASSES[3] = {
"npc_antlionguard"}
BIGSIN_CLASSES[4] = BIGSIN_CLASSES[1]
BIGSIN_CLASSES[5] = BIGSIN_CLASSES[2]

if IsMounted("hl1") then
	BIGSIN_CLASSES[4] = {
	"monster_human_assassin"}
	
	BIGSIN_CLASSES[5] = {
	"monster_alien_slave"}
end

local SIN_WEAPONS = {}
SIN_WEAPONS["npc_metropolice"] = {
"weapon_pistol",
"weapon_smg1",
"weapon_stunstick"}
SIN_WEAPONS["npc_combine_s"] = {
"weapon_smg1",
"weapon_ar2",
"weapon_shotgun"}

function SetAsRespawningItem( ent, del )

	if not ent then return end
	if not ent.ITEM_USE then return end
	
	local POS,ANG,MDL,USE = ent:GetPos(), ent:GetAngles(), ent:GetModel(), ent.ITEM_USE
	
	if not del then del = 30 end
	
	ent.OnRemove = function()
	
		timer.Simple(del,function()
		
			local N_ITEM = CreateItemPickup( POS, ANG, MDL, USE )
			CreatePointParticle( "Doll_EvilSpawn_Activate", POS, POS, Angle(0,0,0), Angle(0,0,0), 1 )
			sound.Play( Sound("TemporalAssassin/Doll/ExtraSpawn_Item.wav"), POS, 70, math.random(99,101), 0.7 )
			SetAsRespawningItem(N_ITEM,del)
			
		end)
	
	end

end

LAST_SIN_COUNT = 0
SIN_COUNT_GOAL = 4

function CanWeSpawnHere(pos)

	local TR = {}
	TR.start = pos
	TR.endpos = pos
	TR.mask = MASK_NPCSOLID
	TR.filter = {}
	table.Add(TR.filter,GLOB_TRIGGER_ENTS)
	TR.mins = Vector(-16,-16,0)
	TR.maxs = Vector(16,16,72)
	local Trace = util.TraceHull(TR)
	
	if Trace.Hit then return false end
	
	return true

end

function EnemySpawns_Think()

	local CT = CurTime()
	
	if SIN_ENEMIES_KILLED and SIN_ENEMIES_KILLED > SIN_SINSPAWN_THRESHOLD then
		SIN_ENEMIES_KILLED = 0
		Create_SinfulBoi()
	end
	
	net.Start("SIN_SinOnMap")
	if GLOB_SINFUL_ENEMY_COUNT and GLOB_SINFUL_ENEMY_COUNT > 0 then
		net.WriteBool(true)
	else
		net.WriteBool(false)
	end
	net.Broadcast()
	
	if GLOB_SIN_DEVOURED_COUNT and GLOB_SIN_DEVOURED_COUNT > LAST_SIN_COUNT then
	
		LAST_SIN_COUNT = GLOB_SIN_DEVOURED_COUNT
		
		local SIN_LEFT = (SIN_COUNT_GOAL - LAST_SIN_COUNT)
		
		net.Start("SIN_UpdateCount")
		net.WriteUInt(LAST_SIN_COUNT,6)
		net.WriteUInt(SIN_COUNT_GOAL,6)
		net.Broadcast()
		
		if SIN_LEFT <= 0 and not SIN_GAME_ENDED then
			SIN_GAME_ENDED = true
			END_SIN_HUNTER()			
		end
		
	end
	
	if GLOB_SINHUNTER_SPAWNNPCS and CT >= GLOB_SINHUNTER_SPAWNNPCS and GLOB_SINHUNTER_STARTED then
	
		GLOB_SINHUNTER_SPAWNNPCS = CT + SIN_SPAWN_SPEED
	
		local SPAWN_POS
	
		if GLOB_SINHUNTER_SPAWN2 then
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_INSIDE) + Vector(0,0,5)
		else
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_OUTSIDE) + Vector(0,0,5)
		end
		
		if SPAWN_POS and GLOB_SINHUNTER_ENEMYCOUNT < SIN_SPAWN_MAX and GLOB_SINTYPE and SIN_CLASSES[GLOB_SINTYPE] then
		
			local CLASS = table.Random(SIN_CLASSES[GLOB_SINTYPE])
			local WEP = false
			
			if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
				WEP = table.Random(SIN_WEAPONS[CLASS])
			end
			
			if CanWeSpawnHere(SPAWN_POS) then
				CreateSnapNPC( CLASS, SPAWN_POS, Angle(0,math.random(-360,360),0), WEP, false )
				GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT + 1
			end
		
		end
	
	end

end
timer.Create("EnemySpawns_ThinkTimer", 0.5, 0, EnemySpawns_Think)

function Set_EnemySpawns_1()

	print("Outdoors of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = true
	GLOB_SINHUNTER_SPAWN2 = false

end

function Set_EnemySpawns_2()

	print("Inside of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = true

end

function START_SIN_HUNTER()

	RemoveAllTeleporters()
	
	Set_EnemySpawns_2()

	--[[ These are the triggers that enables Outside map spawns ]]
	CreateTrigger( Vector(-126.904,-235.952,176.031), Vector(-77.816,-77.816,0), Vector(77.816,77.816,100.245), Set_EnemySpawns_1, true )
	CreateTrigger( Vector(-284.602,1177.926,96.031), Vector(-91.245,-91.245,0), Vector(91.245,91.245,130.125), Set_EnemySpawns_1, true )
	CreateTrigger( Vector(-128.135,-317.021,344.031), Vector(-55.876,-55.876,0), Vector(55.876,55.876,114.919), Set_EnemySpawns_1, true )
	CreateTrigger( Vector(-127.293,-181.027,596.031), Vector(-33.024,-33.024,0), Vector(33.024,33.024,72.602), Set_EnemySpawns_1, true )
	CreateTrigger( Vector(-128.792,-289.656,472.031), Vector(-90.847,-90.847,0), Vector(90.847,90.847,183.558), Set_EnemySpawns_1, true )

	--[[ These are the triggers that enables Inside map spawns ]]
	CreateTrigger( Vector(-127.837,-442.046,344.031), Vector(-58.23,-58.23,0), Vector(58.23,58.23,100.107), Set_EnemySpawns_2, true )
	CreateTrigger( Vector(-129.332,-376.859,172.198), Vector(-63.573,-63.573,0), Vector(63.573,63.573,102.626), Set_EnemySpawns_2, true )
	CreateTrigger( Vector(-289.008,1383.029,80.714), Vector(-75.649,-75.649,0), Vector(75.649,75.649,150.023), Set_EnemySpawns_2, true )
	CreateTrigger( Vector(-128.053,-86.108,604.901), Vector(-48.697,-48.697,0), Vector(48.697,48.697,56.668), Set_EnemySpawns_2, true )
	CreateTrigger( Vector(-127.145,-453.867,467.043), Vector(-65.704,-65.704,0), Vector(65.704,65.704,188.699), Set_EnemySpawns_2, true )
	
	GLOB_SINHUNTER_STARTED = true
	GLOB_SINHUNTER_SPAWNNPCS = CurTime() + 20
	
	local DIFF = GetDifficulty2()

	if SPAWN_DIFFICULTIES and SPAWN_DIFFICULTIES[DIFF] then
		SIN_SPAWN_SPEED = SPAWN_DIFFICULTIES[DIFF][1]
		SIN_SPAWN_MAX = SPAWN_DIFFICULTIES[DIFF][2]
		SIN_SINSPAWN_THRESHOLD = SPAWN_DIFFICULTIES[DIFF][3]
	end
	
end

function END_SIN_HUNTER()

	GLOB_SINHUNTER_STARTED = false

	for _,v in pairs (ents.GetAll()) do
	
		if v and IsValid(v) then
		
			if v:IsNPC() then
				v:Remove() --[[ Lets remove all the npcs ]]
			end
		
		end
	
	end
	
	GLOB_SINFUL_ENEMY_COUNT = 0
	net.Start("SIN_SinOnMap")
	net.WriteBool(false)
	net.Broadcast()
	
	CreateTeleporter( "START_1", Vector(-1981.184,-439.236,1671.709), "START_2", Vector(-107.871,1850.553,92.082) )
	
	local GAME_END_TEXT = {
	""}

	MakeGameEnd( GAME_END_TEXT, 3, Vector(-1981.991,-420.065,1671.678), Vector(-58.951,-58.951,0), Vector(58.951,58.951,100.812) )
	
end

function Create_SinfulBoi()

	local SPAWNS_OUTSIDE = {
	Vector(-136.3,152.84,1130.443)}
	
	local SPAWNS_INSIDE = {
	Vector(-156.978,668.934,119.414)}
	
	local SPAWN_POINT
	
	if GLOB_SINHUNTER_SPAWN2 then
		SPAWN_POINT = table.Random(SPAWNS_INSIDE)
	elseif GLOB_SINHUNTER_SPAWN1 then
		SPAWN_POINT = table.Random(SPAWNS_OUTSIDE)
	end
	
	if not SPAWN_POINT then return end
	if not GLOB_SINTYPE then return end
	if not BIGSIN_CLASSES then return end
	if not BIGSIN_CLASSES[GLOB_SINTYPE] then return end
	
	local CLASS = table.Random(BIGSIN_CLASSES[GLOB_SINTYPE])
	local WEP = false
			
	if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
		WEP = table.Random(SIN_WEAPONS[CLASS])
	end
	
	GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT + 1
	
	local SIN_BOI = CreateSnapNPC( CLASS, SPAWN_POINT, Angle(0,math.random(-360,360),0), WEP, false )
			
	timer.Simple(0.1,function()
		if SIN_BOI and IsValid(SIN_BOI) then
			SIN_BOI:SetHealth( SIN_BOI:Health()*2 )
			SIN_BOI:CreateSinner(true)
			SIN_BOI:SetModelScale(1.2,0) --[[ Lets make sin bois a bit bigger in scale ]]
			if GLOB_SINTYPE == 1 then
				SIN_BOI:MakeCombineShielder()
			end
		end
	end)

end

function SinHunter_NPCDeath( npc, att, inf )

	if not npc.SinHealth then
		SIN_ENEMIES_KILLED = SIN_ENEMIES_KILLED + 1
		GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT - 1
	elseif npc.SinHealth then
		GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT - 1
	end

end
hook.Add("OnNPCKilled","SinHunter_NPCDeathHook",SinHunter_NPCDeath)

function SINHUNTER_DEATH_RESET()

	if #player.GetAll() <= 1 then

		GLOB_NO_SAVEDATA = true
		GLOB_SAFE_GAMEEND = true
		
		GLOB_NO_FENIC = false
		GLOB_NO_SINS = true
		
		GLOB_SINHUNTER_SPAWN1 = false
		GLOB_SINHUNTER_SPAWN2 = false
		GLOB_SINHUNTER_SPAWNNPCS = 0
		GLOB_SINHUNTER_ENEMYCOUNT = 0
		GLOB_SINFUL_ENEMY_COUNT = 0
		
		SIN_SPAWN_SPEED = 5
		SIN_SPAWN_MAX = 40
		SIN_ENEMIES_KILLED = 0
		SIN_SINSPAWN_THRESHOLD = 20
		
		GLOB_SIN_DEVOURED_COUNT = 0
		LAST_SIN_COUNT = 0
		SIN_COUNT_GOAL = 4
		
		GLOB_SINHUNTER_STARTED = false
		GLOB_SINHUNTER_SPAWNNPCS = false
		SIN_GAME_ENDED = false
		
	end

end
hook.Add("PlayerDeath","SINHUNTER_DEATH_RESETHOOK",SINHUNTER_DEATH_RESET)

function MAP_LOADED_FUNC()

	GLOB_NO_SAVEDATA = true
	GLOB_SAFE_GAMEEND = true
	
	GLOB_NO_FENIC = false
	GLOB_NO_SINS = true
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = false
	GLOB_SINHUNTER_SPAWNNPCS = 0
	GLOB_SINHUNTER_ENEMYCOUNT = 0
	GLOB_SINFUL_ENEMY_COUNT = 0
	
	SIN_SPAWN_SPEED = 5
	SIN_SPAWN_MAX = 40
	SIN_ENEMIES_KILLED = 0
	SIN_SINSPAWN_THRESHOLD = 20
	
	GLOB_SIN_DEVOURED_COUNT = 0
	LAST_SIN_COUNT = 0
	SIN_COUNT_GOAL = 4
	
	GLOB_SINHUNTER_STARTED = false
	GLOB_SINHUNTER_SPAWNNPCS = false
	SIN_GAME_ENDED = false
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("info_player_deathmatch")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	RemoveAllEnts("ambient_generic")
	RemoveAllEnts("prop_physics")
	RemoveAllEnts("prop_physics_respawnable")
	RemoveAllEnts("prop_physics_multiplayer")
	
	local SPAWN_P1 = CreateProp(Vector(-1982.687,81.875,1723.531), Angle(0,0,0), "models/props_wasteland/cargo_container01b.mdl", true)
	SPAWN_P1:SetMaterial("models/props_combine/tprings_globe")
	
	local SPAWN_P2 = CreateProp(Vector(-1982.531,-296.625,1722.688), Angle(-0.044,180,-0.044), "models/props_wasteland/cargo_container01b.mdl", true)
	SPAWN_P2:SetMaterial("models/props_combine/tprings_globe")
	
	CreatePlayerSpawn( Vector(-1986.715,239.197,1672.652), Angle(0,-90,0) )
	
	CreateClothesLocker( Vector(-1942.049,123.504,1672.63), Angle(0,180.981,0) )
	
	Create_InfiniteGrenades( Vector(-1934.79, -16.06, 1672.62), Angle(0, 170.949, 0) )
	Create_PistolWeapon( Vector(-1981.728, -144.704, 1671.903), Angle(0, 90.902, 0) )
	Create_PistolAmmo( Vector(-1955.248, -167.456, 1671.867), Angle(0, 113.474, 0) )
	Create_PistolAmmo( Vector(-1982.269, -171.824, 1671.883), Angle(0, 88.812, 0) )
	Create_PistolAmmo( Vector(-2010.877, -159.176, 1671.912), Angle(0, 59.552, 0) )
	Create_PistolAmmo( Vector(-1948.367, -132.869, 1671.888), Angle(0, 128.104, 0) )
	Create_PistolAmmo( Vector(-2010.354, -131.557, 1671.932), Angle(0, 354.03, 0) )
	Create_Armour100( Vector(-1956.059, -213.667, 1671.834), Angle(0, 106.577, 0) )
	Create_SpiritSphere( Vector(-2000.212, -216.437, 1671.863), Angle(0, 73.346, 0) )
	Create_VolatileBattery( Vector(-1978.337, -255.691, 1671.818), Angle(0, 90.902, 0) )
	CreateProp(Vector(-409.187,499.844,415.969), Angle(-10.811,117.686,19.951), "models/temporalassassin/doll/unknown.mdl", true)
	
	Create_CrossbowWeapon( Vector(890.935, 1393.569, 604.414), Angle(0, 224.871, 0) )
	Create_KingSlayerWeapon( Vector(333.93, -117.803, 1056.011), Angle(0, 209.088, 0) )
	Create_RevolverWeapon( Vector(-722.816, 1195.481, 342.539), Angle(0, 207.941, 0) )
	Create_FalanranWeapon( Vector(-773.825, -157.102, 718.031), Angle(0, 46.175, 0) )
	Create_Backpack( Vector(-499.933, 211.997, 1343.743), Angle(0, 355.071, 0) )
	Create_SMGWeapon( Vector(-127.98, -464.724, 344.031), Angle(0, 90.585, 0) )
	Create_M6GWeapon( Vector(135.667, -169.98, 468.031), Angle(0, 270.845, 0) )
	Create_RifleWeapon( Vector(-125.1, -165.946, 596.031), Angle(0, 271.992, 0) )
	Create_ManaCrystal100( Vector(-127.994, -368.096, 724.032), Angle(0, 269.484, 0) )
	Create_ShotgunWeapon( Vector(-481.262, -199.662, 176.031), Angle(0, 359.879, 0) )
	Create_SuperShotgunWeapon( Vector(-442.935, -199.292, 176.031), Angle(0, 359.461, 0) )
	Create_Armour( Vector(-483.04, -280.102, 176.031), Angle(0, 3.641, 0) )
	Create_Armour( Vector(-486.932, -105.323, 176.031), Angle(0, 353.818, 0) )
	Create_ReikoriKey( Vector(12.188, 703.551, 370.091), Angle(0, 181.817, 0) )
	Create_EldritchArmour( Vector(-526.096, 86.206, 728.031), Angle(0, 42.165, 0) )
	Create_RaikiriWeapon( Vector(965.85, 675.089, 361.863), Angle(0, 187.078, 0) )

	local HEALING = {
	Create_Medkit25( Vector(778.254, -1002.577, 71.219), Angle(0, 143.95, 0) ),
	Create_Medkit25( Vector(611.625, 500.748, 80.075), Angle(0, 24.51, 0) ),
	Create_Medkit25( Vector(-99.399, 1387.217, 64.457), Angle(0, 106.126, 0) ),
	Create_Medkit25( Vector(-894.732, 248.171, 89.509), Angle(0, 180.321, 0) ),
	Create_Medkit25( Vector(-286.963, 904.912, 96.031), Angle(0, 88.779, 0) ),
	Create_Medkit10( Vector(135.631, 658.824, 208.031), Angle(0, 296.943, 0) ),
	Create_Medkit10( Vector(164.533, 650.955, 208.031), Angle(0, 282.522, 0) ),
	Create_Medkit10( Vector(-441.882, 655.447, 208.031), Angle(0, 225.464, 0) ),
	Create_Medkit10( Vector(-474.36, 652.459, 208.031), Angle(0, 258.695, 0) ),
	Create_ManaCrystal( Vector(-158.978, 192.165, 152.031), Angle(0, 176.349, 0) ),
	Create_ManaCrystal( Vector(-450.803, -244.533, 344.031), Angle(0, 86.27, 0) ),
	Create_Medkit25( Vector(-304.686, -240.269, 344.031), Angle(0, 136.221, 0) ),
	Create_ManaCrystal( Vector(-81.342, 2.922, 472.031), Angle(0, 271.128, 0) ),
	Create_ManaCrystal( Vector(-654.38, 1113.584, 520.8), Angle(0, 277.189, 0) ),
	Create_Medkit25( Vector(-672.585, 1065.857, 519.254), Angle(0, 341.561, 0) ),
	Create_Medkit25( Vector(895.929, 118.281, 1056), Angle(0, 189.093, 0) ),
	Create_ManaCrystal( Vector(820.122, -132.093, 1056), Angle(0, 135.798, 0) )}
	
	local AMMO_ENTS = {
	Create_PistolAmmo( Vector(350.633, -123.559, 604.031), Angle(0, 169.903, 0) ),
	Create_PistolAmmo( Vector(352.295, -159.016, 604.031), Angle(0, 145.241, 0) ),
	Create_PistolAmmo( Vector(323.568, -182.512, 604.031), Angle(0, 119.116, 0) ),
	Create_PistolAmmo( Vector(277.69, -183.127, 604.031), Angle(0, 84.84, 0) ),
	Create_CrossbowAmmo( Vector(822.173, 1362.283, 604.414), Angle(0, 247.861, 0) ),
	Create_CrossbowAmmo( Vector(823.197, 1323.635, 604.414), Angle(0, 230.723, 0) ),
	Create_CrossbowAmmo( Vector(871.355, 1318.91, 604.414), Angle(0, 217.556, 0) ),
	Create_KingSlayerAmmo( Vector(280.489, -88.828, 1056.011), Angle(0, 258.83, 0) ),
	Create_KingSlayerAmmo( Vector(302.001, -144.922, 1056.011), Angle(0, 214.522, 0) ),
	Create_KingSlayerAmmo( Vector(339.991, -179.409, 1056.011), Angle(0, 173.558, 0) ),
	Create_RevolverAmmo( Vector(-732.864, 1227.474, 342.4), Angle(0, 195.192, 0) ),
	Create_RevolverAmmo( Vector(-753.973, 1248.107, 338.487), Angle(0, 250.054, 0) ),
	Create_RevolverAmmo( Vector(-751.828, 1226.613, 337.376), Angle(0, 211.285, 0) ),
	Create_SMGAmmo( Vector(-72.443, -436.933, 344.031), Angle(0, 138.655, 0) ),
	Create_SMGAmmo( Vector(-107.26, -430.511, 344.031), Angle(0, 116.501, 0) ),
	Create_SMGAmmo( Vector(-145.796, -432.851, 344.031), Angle(0, 69.476, 0) ),
	Create_SMGAmmo( Vector(-184.4, -440.271, 344.031), Angle(0, 43.56, 0) ),
	Create_RifleAmmo( Vector(-189.184, -172.067, 596.031), Angle(0, 338.245, 0) ),
	Create_RifleAmmo( Vector(-67.402, -166.887, 596.031), Angle(0, 208.874, 0) ),
	Create_ShotgunAmmo( Vector(-480.728, -239.93, 176.031), Angle(0, 11.584, 0) ),
	Create_ShotgunAmmo( Vector(-436.698, -241.571, 176.031), Angle(0, 23.915, 0) ),
	Create_ShotgunAmmo( Vector(-431.235, -157.808, 176.031), Angle(0, 338.562, 0) ),
	Create_ShotgunAmmo( Vector(-481.47, -150.028, 176.031), Angle(0, 325.499, 0) )}
	
	local AMMO_S_ENTS = {
	Create_M6GAmmo( Vector(134.928, -119.874, 468.031), Angle(0, 270.845, 0) ),
	Create_M6GAmmo( Vector(135.266, -142.813, 468.031), Angle(0, 270.845, 0) )}
	
	for _,v in pairs (HEALING) do
		SetAsRespawningItem(v,30)
	end
	
	for _,v in pairs (AMMO_ENTS) do
		SetAsRespawningItem(v,60)
	end
	
	for _,v in pairs (AMMO_S_ENTS) do
		SetAsRespawningItem(v,120)
	end

	CreateTeleporter( "START_1", Vector(-1981.184,-439.236,1671.709), "START_2", Vector(-107.871,1850.553,92.082) )
	
	--[[ The trigger to start the mode ]]
	CreateTrigger( Vector(-115.61,1861.571,91.251), Vector(-235.42,-235.42,0), Vector(235.42,235.42,161.279), START_SIN_HUNTER )
	
	for _,v in pairs (ents.GetAll()) do
	
		if IsValid(v) then
		
			if v:IsProp() then
			
				if v.SetHealth then
					v:SetHealth(5000000)
				end

				if IsValid(v:GetPhysicsObject()) then
					v:GetPhysicsObject():EnableMotion(false)
					v:GetPhysicsObject():AddGameFlag(FVPHYSICS_CONSTRAINT_STATIC)
				end
				
			end
			
		end
		
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

SINHUNTER_SPAWNS_INSIDE = {
Vector(-190.626,-254.693,472.031),
Vector(-194.96,-331.463,472.031),
Vector(-111.337,-343.556,472.031),
Vector(-56.304,-316.839,472.031),
Vector(-68.892,-267.158,472.031),
Vector(-121.296,-233.401,472.031),
Vector(-76.385,-176.313,472.031),
Vector(-51.769,-98.397,472.031),
Vector(-95.613,-14.579,472.031),
Vector(-191.2,-35.55,472.031),
Vector(-139.446,-4.199,344.039),
Vector(-70.293,-2.579,344.039),
Vector(36.592,-2.449,344.039),
Vector(125.35,-10.565,344.039),
Vector(154.359,-100.801,344.039),
Vector(173.57,-185.415,344.039),
Vector(196.123,-238.319,344.039),
Vector(-59.491,-112.83,344.039),
Vector(-62.903,-203.434,344.039),
Vector(-61.777,-286.474,344.039),
Vector(-146.745,-310.716,344.039),
Vector(-202.874,-310.573,344.039),
Vector(-203.128,-227.989,344.039),
Vector(-216.21,-143.692,344.039),
Vector(-266.445,-140.535,344.039),
Vector(-294.544,-207.911,344.039),
Vector(-351.166,-259.09,344.039),
Vector(-431.146,-244.172,344.039),
Vector(-467.868,-180.02,344.039),
Vector(-457.355,-126.457,344.039),
Vector(-442.191,-45.063,344.039),
Vector(-329.455,-15.069,294.031),
Vector(-270.502,-13.964,254.031),
Vector(-161.099,-13.262,212.031),
Vector(-57.634,-13.168,212.031),
Vector(35.04,-9.521,212.031),
Vector(122.621,-6.794,212.031),
Vector(139.354,-81.498,194.031),
Vector(135.882,-164.726,176.031),
Vector(128.733,-244.139,176.031),
Vector(40.32,-230.832,176.031),
Vector(-45.982,-181.448,176.031),
Vector(-143.246,-142.181,176.031),
Vector(-242.735,-127.167,176.031),
Vector(-356.973,-121.45,176.031),
Vector(-451.089,-161.607,176.031),
Vector(-425.218,-232.593,176.031),
Vector(-327.997,-244.276,176.031),
Vector(-224.756,-251.758,176.031),
Vector(-142.485,-248.827,176.031),
Vector(-372.042,201.743,208.031),
Vector(-480.56,350.032,208.031),
Vector(-477.696,566.327,208.031),
Vector(-149.818,78.442,212.031),
Vector(79.165,195.148,208.031),
Vector(165.915,328.557,208.031),
Vector(193.61,467.278,208.031),
Vector(168.941,587.61,208.031),
Vector(60.406,400.268,80.031),
Vector(-65.666,382.854,72.031),
Vector(-167.412,384.635,72.031),
Vector(-283.034,379.247,72.031),
Vector(-410.812,348.791,80.031),
Vector(-474.262,397.201,80.031),
Vector(-470.655,485.043,80.031),
Vector(-429.03,569.426,80.031),
Vector(-369.273,643.305,80.031),
Vector(-42.984,707.898,80.031),
Vector(26.543,645.616,80.031),
Vector(55.277,546.935,80.031),
Vector(72.326,444.378,80.031),
Vector(-36.893,502.96,72.031),
Vector(-59.976,868.456,98.523),
Vector(-51.643,979.035,96.714),
Vector(-51.612,1101.8,96.729),
Vector(-138.448,955.305,98.884),
Vector(-246.284,956.649,98.214),
Vector(-321.515,1039.843,116.591),
Vector(-305.963,1128.734,105.516),
Vector(-286.006,1217.942,96.031)}

SINHUNTER_SPAWNS_OUTSIDE = {
Vector(-632.776,-723.104,67.536),
Vector(-721.436,-791.735,69.148),
Vector(-840.43,-881.549,65.778),
Vector(-919.553,-793.673,72.681),
Vector(-909.386,-745.677,71.823),
Vector(-801.06,-643.576,67.427),
Vector(-777.815,-548.167,66.982),
Vector(-886.737,-502.858,65.978),
Vector(-994.63,-402.684,67.224),
Vector(-1008.246,-335.776,67.938),
Vector(-937.521,-220.448,64.572),
Vector(-964.283,-117.355,65.289),
Vector(-1050.892,-12.75,68.224),
Vector(-910.258,84.111,70.753),
Vector(-917.573,137.238,77.536),
Vector(-1029.943,186.405,74.513),
Vector(-1062.43,266.47,79.845),
Vector(-1008.989,319.641,89.421),
Vector(-924.72,356.635,101.554),
Vector(-970.431,414.03,96.867),
Vector(-1033.966,512.958,79.832),
Vector(-992.339,620.19,72.61),
Vector(-904.388,683.851,84.703),
Vector(-887.06,810.134,76.584),
Vector(-971.717,920.699,66.057),
Vector(-1030.824,1033.894,69.253),
Vector(-1029.19,1147.243,78.395),
Vector(-957.536,1192.73,71.972),
Vector(-868.535,1146.843,66.92),
Vector(-845.201,1041.911,67.875),
Vector(-772.109,1117.849,78.237),
Vector(-735.82,1232.17,85.356),
Vector(-774.375,1336.201,66.783),
Vector(-808.123,1442.609,64.639),
Vector(-804.399,1543.915,67.075),
Vector(-753.762,1643.059,69.732),
Vector(-684.96,1717.246,74.798),
Vector(-599.766,1765.457,84.252),
Vector(-513.103,1775.424,76.525),
Vector(-509.405,1601.018,66.006),
Vector(-516.605,1506.081,67.525),
Vector(-463.46,1430.585,71.49),
Vector(-354.515,1440.114,72.747),
Vector(-284.144,1516.99,65.03),
Vector(-192.412,1565.007,68.492),
Vector(-95.262,1593.261,70.826),
Vector(-50.285,1695.017,75.443),
Vector(-97.694,1787.604,87.161),
Vector(-40.699,1763.561,85.628),
Vector(51.304,1724.247,85.666),
Vector(118.27,1634.08,90.53),
Vector(174.214,1545.294,100.346),
Vector(277.795,1547.916,106.186),
Vector(375.839,1604.028,98.472),
Vector(465.459,1652.251,111.887),
Vector(569.794,1638.526,125.026),
Vector(641.745,1550.57,134.6),
Vector(648.136,1454.754,122.32),
Vector(610.035,1365.477,105.14),
Vector(576.986,1277.272,106.823),
Vector(606.588,1230.293,113.061),
Vector(692.86,1170.63,110.995),
Vector(806.139,1165.223,114.579),
Vector(887.828,1206.333,123.812),
Vector(969.536,1243.572,120.515),
Vector(1023.771,1129.092,115.638),
Vector(1039.894,1025.249,108.092),
Vector(1061.153,913.366,99.574),
Vector(1083.817,826.926,96.446),
Vector(1104.481,716.505,91.505),
Vector(1104.279,614.262,85.935),
Vector(1086.885,508.845,78.646),
Vector(1062.522,399.307,71.752),
Vector(1020.138,309.847,71.988),
Vector(922.437,286.187,73.897),
Vector(841.505,356.936,93.291),
Vector(764.661,479.888,85.218),
Vector(686.933,561.3,82.893),
Vector(679.76,665.369,81.161),
Vector(694.573,760.194,82.134),
Vector(984.097,103.045,75.554),
Vector(909.199,101.778,83.002),
Vector(891.555,-1.217,81.566),
Vector(949.677,-100.136,74.726),
Vector(990.262,-194.1,79.026),
Vector(952.241,-288.651,71.944),
Vector(848.21,-293.794,66.424),
Vector(748.627,-247.161,78.975),
Vector(656.731,-182.219,99.608),
Vector(550.84,-188.206,109.707),
Vector(495.701,-296.631,88.671),
Vector(507.126,-417.37,87.065),
Vector(567.166,-509.812,73.792),
Vector(668.246,-560.278,69.88),
Vector(764.311,-605.964,66.387),
Vector(834.258,-672.256,65.409),
Vector(838.549,-779.24,65.017),
Vector(775.837,-857.334,66.119),
Vector(674.58,-893.641,67.118),
Vector(567.508,-861.666,68.562),
Vector(467.645,-809.2,71.508),
Vector(380.547,-856.674,73.551),
Vector(367.933,-951.493,70.385),
Vector(413.256,-1050.748,71.247),
Vector(478.12,-1127.546,76.634),
Vector(467.053,-1217.003,83.481),
Vector(360.714,-1231.148,78.811),
Vector(271.802,-1186.005,74.276),
Vector(217.534,-1101.351,71.033),
Vector(174.5,-987.233,72.415),
Vector(107.088,-911.15,75.259),
Vector(2.848,-894.838,74.115),
Vector(-59.431,-965.412,70.229),
Vector(-68.137,-1067.319,66.127),
Vector(-102.883,-1158.644,65.599),
Vector(-202.719,-1215.824,79.208),
Vector(-297.157,-1186.988,79.123),
Vector(-352.746,-1083.235,68.329),
Vector(-343.282,-990.152,66.344),
Vector(-353.033,-875.478,67.283),
Vector(-431.411,-802.661,66.605),
Vector(-546.477,-810.202,67.194),
Vector(-620.262,-882.414,70.216),
Vector(-711.212,-942.037,69.2),
Vector(-816.045,-890.923,66.515),
Vector(-853.343,-778.353,67.778),
Vector(-838.37,-679.78,66.739),
Vector(-852.842,-578.138,66.46),
Vector(-935.382,-535.536,70.549),
Vector(511.727,-516.917,86.116),
Vector(449.882,-425.059,92.003),
Vector(349.081,-430.782,96.106),
Vector(303.36,-531.633,96.852),
Vector(221.552,-614.807,105.851),
Vector(116.939,-629.975,130.574),
Vector(18.434,-608.79,151.768),
Vector(-95.158,-586.635,168.463),
Vector(-205.72,-564.174,181.454),
Vector(-293.146,-516.802,203.422),
Vector(-380.092,-454.629,226.345),
Vector(-475.95,-412.273,247.445),
Vector(-574.522,-379.921,278.988),
Vector(-662.143,-318.866,301.757),
Vector(-687.749,-206.399,305.315),
Vector(-649.582,-121.77,330.779),
Vector(-677.344,-32.017,363.946),
Vector(-742.721,51.394,370.735),
Vector(-702.774,152.973,394.804),
Vector(-669.161,246.262,424.001),
Vector(-663.584,345.523,448.487),
Vector(-662.346,444.175,469.151),
Vector(-670.966,542.501,480.457),
Vector(-645.451,651.855,489.797),
Vector(-573.179,719.129,482.845),
Vector(-538.494,816.526,480.889),
Vector(-580.536,921.523,483.63),
Vector(-594.418,1019.524,491.942),
Vector(-521.916,1110.38,483.218),
Vector(-425.667,1137.882,465.951),
Vector(-324.843,1092.009,451.726),
Vector(-251.432,1009.972,433.864),
Vector(-152.307,977.828,408.921),
Vector(-57.918,1022.706,378.12),
Vector(28.929,1087.42,375.546),
Vector(127.193,1094.725,345.641),
Vector(217.908,1031.584,309.944),
Vector(275.488,957.928,302.498),
Vector(365.556,937.194,295.085),
Vector(455.721,877.746,300.689),
Vector(452.519,782.747,298.242),
Vector(407.348,698.215,273.827),
Vector(365.527,612.979,260.747),
Vector(338.622,518.045,243.953),
Vector(354.322,426.198,238.004),
Vector(390.833,345.907,230.517),
Vector(394.264,257.825,227.454),
Vector(400.966,156.224,202.704),
Vector(433.483,70.323,180.534),
Vector(486.285,-10.371,150.192),
Vector(557.638,-69.485,131.143),
Vector(-340.54,706.43,503.159),
Vector(-229.295,691.802,539.057),
Vector(-119.13,685.913,563.717),
Vector(-13.205,677.047,601.382),
Vector(83.945,651.354,607.468),
Vector(206.615,629.794,616.92),
Vector(247.113,741.543,615.589),
Vector(583.392,1063.347,612.098),
Vector(657.877,1160.34,604.414),
Vector(685.289,1205.658,618.042),
Vector(805.145,1268.156,604.414),
Vector(852.193,1341.884,604.414),
Vector(878.604,1408.272,604.414),
Vector(923.5,1394.612,604.414),
Vector(254.108,615.415,617.142),
Vector(251.428,545.902,621.176),
Vector(176.74,479.409,609.059),
Vector(124.642,409.055,615.342),
Vector(179.039,357.788,603.545),
Vector(272.517,336.164,614.902),
Vector(306.858,279.817,616.03),
Vector(246.055,199.603,600.404),
Vector(157.162,181.742,595.112),
Vector(87.164,171.912,614.035),
Vector(86.848,140.003,607.992),
Vector(175.104,73.966,605.223),
Vector(226.72,-7.183,603.749),
Vector(175.068,-74.874,598.015),
Vector(77.429,-78.228,598.088),
Vector(-6.069,-38.245,615.132),
Vector(-98.968,-28.067,630.005),
Vector(-192.478,-63.643,625.83),
Vector(-296.927,-61.94,654.446),
Vector(-368.398,10.899,676.092),
Vector(-380.38,107.123,712.588),
Vector(-365.506,211.047,726.516),
Vector(-375.515,308.344,725.162),
Vector(-342.125,414.854,735.64),
Vector(-305.317,488.56,736.242),
Vector(-255.409,462.533,738.166),
Vector(-174.237,413.516,761.084),
Vector(-81.456,431.903,787.061),
Vector(17.104,461.503,835.561),
Vector(96.864,404.184,871.001),
Vector(197.393,352.715,913.81),
Vector(283.931,386.583,957.421),
Vector(387.228,403.216,1004.856),
Vector(475.739,348.515,1035.129),
Vector(563.717,380.382,1051.497),
Vector(677.467,367.583,1057.095),
Vector(753.741,296.326,1059.91),
Vector(787.623,201.383,1056.574),
Vector(777.648,86.226,1056.031),
Vector(757.213,-31.179,1056.031),
Vector(716.489,-119.511,1056.031),
Vector(633.729,-191.108,1056.031),
Vector(532.765,-223.847,1056.031),
Vector(445.049,-184.494,1055.843),
Vector(419.585,-82.912,1056.069),
Vector(435.332,7.356,1056.346),
Vector(425.219,113.397,1056.096),
Vector(342.575,175.846,1057.034),
Vector(251.67,146.867,1057.034),
Vector(165.519,87.029,1060.36),
Vector(42.385,115.913,1057.108),
Vector(-9.413,202.504,1056.962),
Vector(-97.291,276.465,1064.44),
Vector(-209.94,260.522,1063.866),
Vector(-265.623,162.848,1060.527),
Vector(-254.369,92.731,1060.33)}

function NPC_KillBox()

	for _,v in pairs (GLOBAL_NPCS) do
	
		--[[ We need to do this otherwise npcs could stack and fall off the edges and leave the player with nothing to fight ]]
		if v and IsValid(v) and v:GetPos().z <= -100 then
			v:TakeDamage(5000)
		end
	
	end

end
timer.Create("NPC_AvalonKillBox", 5, 0, NPC_KillBox)