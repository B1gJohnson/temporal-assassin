
DEV_GAMEOVER = true

local PORTAL_PLACEMENT = false

game.AddParticles( "particles/magenta_vaniya.pcf" )
PrecacheParticleSystem("Vaniya_EndPortal")
PrecacheParticleSystem("Vaniya_EndPortalSmall")
PrecacheParticleSystem("Vaniya_EndLaser")

function Vaniya_PlacePortalEffect_LIB( len )

	local POS = net.ReadVector()
	local DI_EEEEEEEEEEEEEEE = net.ReadBool()
	
	if DI_EEEEEEEEEEEEEEE then
		PORTAL_PLACEMENT = false
	return end
	
	PORTAL_PLACEMENT = POS

	timer.Create("THE_PORTAL_IS_HERE", 0.9, 0, function()
		if PORTAL_PLACEMENT then
			ParticleEffect( "Vaniya_EndPortal", PORTAL_PLACEMENT, Angle(0,0,0) )
		end
	end)

end
net.Receive("Vaniya_PlacePortalEffect",Vaniya_PlacePortalEffect_LIB)

timer.Create("AH_SHIT_LASER_TEST", 0.1, 0, function()

	if LocalPlayer() and IsValid(LocalPlayer()) then
	
		if not LocalPlayer().LOAD_PARTICLES then
			LocalPlayer().LOAD_PARTICLES = true
			game.AddParticles( "particles/magenta_vaniya.pcf" )
			PrecacheParticleSystem("Vaniya_EndPortal")
			PrecacheParticleSystem("Vaniya_EndPortalSmall")
			PrecacheParticleSystem("Vaniya_EndLaser")
			RunConsoleCommand("Vaniya_ParticleBullshit")
		end

		if LocalPlayer().END_LASER_LMAO then

			if SUPER_PARTICLE_BOOTYBLASTER and IsValid(SUPER_PARTICLE_BOOTYBLASTER) then
				SUPER_PARTICLE_BOOTYBLASTER:StopEmission(false,false,false)
			end
			
			SUPER_PARTICLE_BOOTYBLASTER = CreateParticleSystem( LocalPlayer(), "Vaniya_EndLaser", PATTACH_ABSORIGIN, 0, Vector(0,0,0) )
			if SUPER_PARTICLE_BOOTYBLASTER then
				SUPER_PARTICLE_BOOTYBLASTER:SetControlPoint( 0, Vector(-1891.841,487.034,-2450.5) )
				SUPER_PARTICLE_BOOTYBLASTER:SetControlPoint( 1, Vector(-1891.841,487.034,-103.901) )
				SUPER_PARTICLE_BOOTYBLASTER:StartEmission(false)
			end
			
		end
		
	end
		
end)

--[[ This is the code for antonio's visage - Magenta ]]

local CONTRACTOR_MSG_NUM = 0

local ANTONIO_MESSAGE_TIME = 0
local ANTONIO_MESSAGE_ALPHA = 0
local ANTONIO_MESSAGE = false
local CUSTOM_TIMED_STRING = "ANTONIO_Contractor_Timed"

local Font_Table = {
	font 		= "Victoria Typewriter",
	size 		= GUI_X(10),
	weight 		= 800,
	antialias 	= true,
	additive 	= false,
	blursize	= 0,
	scanlines	= 0
}
surface.CreateFont( "ANTONIO_TXT", Font_Table )

local Font_Table = {
	font 		= "Victoria Typewriter",
	size 		= GUI_X(10),
	weight 		= 800,
	antialias 	= true,
	additive 	= false,
	blursize	= 6,
	scanlines	= 0
}
surface.CreateFont( "ANTONIO_TXT2", Font_Table )

local CONTRACTOR_MSG_NUM = 0

function ANTONIO_CancelMessages()

	ANTONIO_MESSAGE_TIME = 0
	ANTONIO_MESSAGE_ALPHA = 0
	ANTONIO_MESSAGE = false
	
	for loop = 1,CONTRACTOR_MSG_NUM do
		timer.Destroy("ANTONIO_Contractor_Timed"..loop)
	end

end
hook.Add("TA_CancelContractors","ANTONIO_CancelMessagesHook",ANTONIO_CancelMessages)

local ANTONIO_CommunicateSounds = {
Sound("TA_Antonio/Antonio_Com.wav")}

local ANTONIO_CommunicateSounds2 = {
Sound("TA_Antonio/Antonio_Com_NoVisage.wav")}

function AntonioMessage_LIB( len )

	local STR = net.ReadString()
	local DRAW_VISAGE = net.ReadBool()
	local PL = LocalPlayer()
	if not IsValid(PL) then return end
	
	local CT = UnPredictedCurTime()
	local DRAW_TIME = ContractorMessage_DrawTime(STR)
	CONTRACTOR_MSG_NUM = CONTRACTOR_MSG_NUM + 1
	
	if ((not GLOB_CONT_MSG_DELAY) or (GLOB_CONT_MSG_DELAY and CT >= GLOB_CONT_MSG_DELAY)) then
	
		GLOB_CONT_MSG_DELAY = CT + DRAW_TIME
		
		if not DRAW_VISAGE then
			PL:EmitSound( table.Random(ANTONIO_CommunicateSounds2), 80, 100, 0.7, CHAN_STATIC )
		else
			PL:EmitSound( table.Random(ANTONIO_CommunicateSounds), 80, 100, 0.6, CHAN_STATIC )
		end
		
		DRAW_CONTRACTOR_VISUALS = DRAW_VISAGE
		if DRAW_VISAGE then
			PL.Memory_Visuals = CurTime() + DRAW_TIME + 0.5
		end
		ANTONIO_MESSAGE = STR
		print(STR)
		ANTONIO_MESSAGE_ALPHA = 0
		ANTONIO_MESSAGE_TIME = CT + (DRAW_TIME-0.5)
		
	else
		
		local TIME = (GLOB_CONT_MSG_DELAY - CT)
		
		timer.Create(tostring(CUSTOM_TIMED_STRING..CONTRACTOR_MSG_NUM),TIME,1,function()
			DRAW_CONTRACTOR_VISUALS = DRAW_VISAGE
			if not DRAW_VISAGE then
				PL:EmitSound( table.Random(ANTONIO_CommunicateSounds2), 80, 100, 0.7, CHAN_STATIC )
			else
				PL:EmitSound( table.Random(ANTONIO_CommunicateSounds), 80, 100, 0.6, CHAN_STATIC )
			end
			ANTONIO_MESSAGE = STR
			print(STR)
			ANTONIO_MESSAGE_ALPHA = 0
			ANTONIO_MESSAGE_TIME = UnPredictedCurTime() + (DRAW_TIME-0.5)
		end)

		GLOB_CONT_MSG_DELAY = GLOB_CONT_MSG_DELAY + DRAW_TIME
		
	end
	
end
net.Receive("AntonioMessage",AntonioMessage_LIB)

local CUR_MAP = game.GetMap()

ANTONIO_MAT1 = Material("TA_Antonio/Antonio_Base2.png")
ANTONIO_MAT2 = Material("TA_Antonio/Antonio_Visage.png")

function HUD_DrawAntonio_Message(CT,RFT)

	if CT <= ANTONIO_MESSAGE_TIME then
		ANTONIO_MESSAGE_ALPHA = math.Clamp(ANTONIO_MESSAGE_ALPHA + RFT*300,0,255)
	else
		ANTONIO_MESSAGE_ALPHA = math.Clamp(ANTONIO_MESSAGE_ALPHA - RFT*500,0,255)
	end
	
	if ANTONIO_MESSAGE_ALPHA > 0 and ANTONIO_MESSAGE then
	
		if DRAW_CONTRACTOR_VISUALS then
	
			local SCALE_X = math.sin(CT*3)*10
			local SCALE_Y = math.sin(CT*2.4 + 5.1434)*30
			local MOVE_X = math.sin(CT*3)*3
			local MOVE_Y = math.sin(CT*2.8 + 3.1892)*3
			
			local SCW, SCH = ScrW()*1.1, ScrH()*1.015
			
			surface.SetMaterial(ANTONIO_MAT1)
			surface.SetDrawColor(155,155,155,ANTONIO_MESSAGE_ALPHA*0.3 + math.Rand(-100,100)*0.05)
			surface.DrawTexturedRectRotated( SCW*0.48 + GUI_X(MOVE_X), SCH*0.5 + GUI_Y(MOVE_Y), SCW*1.2 + GUI_X(SCALE_X), SCH*1.2 + GUI_Y(SCALE_Y), math.sin(CT*3)*2 )
			surface.DrawTexturedRectRotated( SCW*0.48 + GUI_X(MOVE_X), SCH*0.5 + GUI_Y(MOVE_Y), SCW*1.3 + GUI_X(SCALE_X), SCH*1.3 + GUI_Y(SCALE_Y), math.sin(CT*2.2 + 431.12314)*2.5 )
			
			surface.SetMaterial(ANTONIO_MAT2)
			surface.SetDrawColor(255,255,255,ANTONIO_MESSAGE_ALPHA*0.5 + math.Rand(-100,100)*0.05)
			surface.DrawTexturedRectRotated( SCW*0.425 + GUI_X(MOVE_X*0.2), SCH*0.5 + GUI_Y(MOVE_Y*0.2), SCW + GUI_X(SCALE_X*0.25), SCH + GUI_Y(SCALE_Y*0.25), 0 )
			
		end
	
		local TEXT_SIN = math.sin(CT*4)*(10*(ANTONIO_MESSAGE_ALPHA/255))
		
		for loop = -3,3 do
			draw.SimpleText( ANTONIO_MESSAGE, "ANTONIO_TXT2", (ScrW()*0.5) + GUI_X(loop*4), (ScrH()*0.375), Color(80,40,10,ANTONIO_MESSAGE_ALPHA*0.3 + TEXT_SIN), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			draw.SimpleText( ANTONIO_MESSAGE, "ANTONIO_TXT2", (ScrW()*0.5), (ScrH()*0.375) + GUI_Y(loop*3), Color(80,40,10,ANTONIO_MESSAGE_ALPHA*0.3 + TEXT_SIN), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		end
		
		DrawShadowText( ANTONIO_MESSAGE, "ANTONIO_TXT", ScrW()*0.5, ScrH()*0.375, Color(244,178,63,ANTONIO_MESSAGE_ALPHA), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "ANTONIO_TXT2", Color(60,60,60,ANTONIO_MESSAGE_ALPHA*0.25 + TEXT_SIN) )
	
	end

end

function Antonio_DrawHud()

	local CT = UnPredictedCurTime()
	local RFT = ViewModel_AnimationTime()
	
	if HUD_DrawAntonio_Message then
		HUD_DrawAntonio_Message(CT,RFT)
	end

end
hook.Add("HUDPaintBackground","Antonio_DrawHudHook",Antonio_DrawHud)