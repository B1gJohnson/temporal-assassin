
if CLIENT then return end

file.CreateDir("TemporalAssassin/VaniyaData/")
local FILE_PATH = "TemporalAssassin/VaniyaData/"

game.AddParticles( "particles/magenta_vaniya.pcf" )
PrecacheParticleSystem("Vaniya_EndPortal")
PrecacheParticleSystem("Vaniya_EndPortalSmall")
PrecacheParticleSystem("Vaniya_EndLaser")

util.AddNetworkString("Vaniya_PlacePortalEffect")

function Vaniya_ParticleBullshit_COM(ply,com,arg)
game.AddParticles( "particles/magenta_vaniya.pcf" )
PrecacheParticleSystem("Vaniya_EndPortal")
PrecacheParticleSystem("Vaniya_EndPortalSmall")
PrecacheParticleSystem("Vaniya_EndLaser")
end
concommand.Add("Vaniya_ParticleBullshit",Vaniya_ParticleBullshit_COM)

--[[ if Die is true, the portal is removed ]]
function Vaniya_PortalEffect( pos, die )

	--[[ Didn't pass a position, Don't do it! ]]
	if not pos then return end
	if not die then die = false end

	net.Start("Vaniya_PlacePortalEffect")
	net.WriteVector( pos )
	net.WriteBool(die)
	net.Broadcast()

end

GLOB_NOSNAPRANDOM = true
GLOB_NO_SAVEEXIT = true
GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true
GLOB_NO_MINIBOSSES  = true

GLOB_NO_FENIC = true
GLOB_FORCE_REITER = true

local cable01 = Material( "cable/TASVaniya_CableLine01" )
local cable02 = Material( "cable/TASVaniya_CableLine02" )
local cable03 = Material( "cable/TASVaniya_CableLine03" )
local cableAPC = Material( "cable/TASVaniya_CableLineAPC" )

local statuehint01 = Material( "models/props/StatueHints/statuehint01" )
local statuehint02 = Material( "models/props/StatueHints/statuehint02" )
local statuehint03 = Material( "models/props/StatueHints/statuehint03" )

function MAP_LOADED_FUNC()

	// Map Load
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("game_text")

	--[[ This is so if the player respawns after eating shit the laser effect is removed to save on frames ]]
	for _,v in pairs (player.GetAll()) do
		if v and IsValid(v) then
			v:SetSharedBool("END_LASER_LMAO",false)
		end
	end
	
	--[[ Set the portal effect to nothing here, again just incase the player eats shit and does the die. ]]
	Vaniya_PortalEffect( Vector(0,0,0), true )

	CreatePlayerSpawn(Vector(406.61,-4491.707,-2019.969), Angle(0,135.414,0))
	
	--[[ T0 Tutorial Messages
	MESSAGE:AddMessage( "Hint_T0Statue1", "Reiterpallasch is a dual-purpose weapon, capable of both gun attacks and melee attacks. [HOLD SPECIAL ABILITY] to toggle the gun mode, which has several different attack modes [TAP SPECIAL ABILITY] to suit different situations", 1, Vector(32,-4076,-1998), Angle(0,0,0), Color(0,0,0), Color(0,0,0) )
	MESSAGE:AddMessage( "Hint_T0Statue2", "Reiterpallasch's Gun is powered by blood. Blood is obtained either by striking your enemies in melee, or yourself [HOLD RELOAD]. Filling Blood also fills Ashen, which will supercharge Reiterpallasch's Gun Mode by pressing [HOLD RELOAD] when full", 1, Vector(70,-4010,-1998), Angle(0,0,0), Color(0,0,0), Color(0,0,0) )
	MESSAGE:AddMessage( "Hint_T0Statue3", "Reiterpallasch can stab [DEFAULT: PRIMARY FIRE] or slash [DEFAULT: RELOAD]. Experiment using these two in combination to take down enemies more effectively.", 1, Vector(-6,-4010,-1998), Angle(0,0,0), Color(0,0,0), Color(0,0,0) )
	]]
	
	function TASVaniya_TutorialMessage01()
		statuehint01:SetTexture ( "$basetexture", "models/props/StatueHints/Statuehints01a_Diffuse" )
		MESSAGE:AddMessage( "Hint_T0Statue1", "Reiterpallasch is a dual-purpose weapon, capable of both gun attacks and melee attacks. [HOLD SPECIAL ABILITY] to toggle the gun mode, which has several different attack modes [TAP SPECIAL ABILITY] to suit different situations", 1, Vector(-21,-4043,-1998), Angle(0,0,0), Color(0,0,0), Color(0,0,0) )
	end
	
	function TASVaniya_TutorialMessage02()
		statuehint02:SetTexture ( "$basetexture", "models/props/StatueHints/Statuehints01a_Diffuse" )
		MESSAGE:AddMessage( "Hint_T0Statue2", "Reiterpallasch's Gun is powered by blood. Blood is obtained either by striking your enemies in melee, or yourself [HOLD RELOAD]. Filling Blood also fills Ashen, which will supercharge Reiterpallasch's Gun Mode by pressing [HOLD RELOAD] when full", 1, Vector(-1,-3969,-1998), Angle(0,0,0), Color(0,0,0), Color(0,0,0) )
	end
	
	function TASVaniya_TutorialMessage03()
		statuehint03:SetTexture ( "$basetexture", "models/props/StatueHints/Statuehints01a_Diffuse" )
		MESSAGE:AddMessage( "Hint_T0Statue3", "Reiterpallasch can stab [DEFAULT: PRIMARY FIRE] or slash [DEFAULT: RELOAD]. Experiment using these two in combination to take down enemies more effectively.", 1, Vector(-74,-3989,-1998), Angle(0,0,0), Color(0,0,0), Color(0,0,0) )
	end
	
	function VaniyaProgression()
		if file.Exists( tostring(FILE_PATH.."VaniyaProgress3.txt"), "DATA" ) then
			TASVaniya_TutorialMessage01()
			TASVaniya_TutorialMessage02()
			TASVaniya_TutorialMessage03()
			
		elseif file.Exists( tostring(FILE_PATH.."VaniyaProgress2.txt"), "DATA" ) then
			file.Write( tostring(FILE_PATH.."VaniyaProgress3.txt"), "Vaniya Progress Tracker" )
			
			TASVaniya_TutorialMessage01()
			TASVaniya_TutorialMessage02()
			TASVaniya_TutorialMessage03()
			
		elseif file.Exists( tostring(FILE_PATH.."VaniyaProgress1.txt"), "DATA" ) then
			file.Write( tostring(FILE_PATH.."VaniyaProgress2.txt"), "Vaniya Progress Tracker" )
			
			TASVaniya_TutorialMessage01()
			TASVaniya_TutorialMessage02()
			
		else
			file.Write( tostring(FILE_PATH.."VaniyaProgress1.txt"), "Vaniya Progress Tracker" )
			
			TASVaniya_TutorialMessage01()	
		end	
	end
	
	function VaniyaProgressionRead()
		if file.Exists( tostring(FILE_PATH.."VaniyaProgress3.txt"), "DATA" ) then
			TASVaniya_TutorialMessage01()
			TASVaniya_TutorialMessage02()
			TASVaniya_TutorialMessage03()
			
		elseif file.Exists( tostring(FILE_PATH.."VaniyaProgress2.txt"), "DATA" ) then
			TASVaniya_TutorialMessage01()
			TASVaniya_TutorialMessage02()
			
		elseif file.Exists( tostring(FILE_PATH.."VaniyaProgress1.txt"), "DATA" ) then
			TASVaniya_TutorialMessage01()
			
		else
			file.Write( tostring(FILE_PATH.."VaniyaProgress1.txt"), "Vaniya Progress Tracker" )
			TASVaniya_TutorialMessage01()
		end
	end

	// Lanterns & Equipment
	SetupLantern( Vector(16.375,-4139.444,-2008.742), Angle(0,135,0), "00", "Assassin's Dream", Vector(114.439,-4465.681,-2019.969), false, false )
	SetupLantern( Vector(4929.699,392.265,-839.969), Angle(0,3.235,0), "01", "Old Barn", Vector(4872.353,500,-839.969), true, false, true, true, false )
	SetupLantern( Vector(1519.989,268.39,-943.969), Angle(0,133.317,0), "02", "Fountain's End", Vector(1562.428,227.631,-943.969), false, false, false, false, false )
	SetupLantern( Vector(7281.702,1703.345,-775.969), Angle(0,-128.038,0), "03", "Damned Hole", Vector(7335.791,1755.009,-775.969), false, false, false, false, false )
	SetupLantern( Vector(-1686.019,4927.671,-647.333), Angle(0,-162.755,0), "04", "Forsaken Tree", Vector(-1592.374,4966.037,-643.461), false, false, false, false, false )
	
	// Attic Secret
	function TASVaniya_AtticRest()
		CreateRestingPoint( Vector(-777.216,1594.595,208.031), Angle(0,181.472,0) )
	end
	
	TASVaniya_IfAtticSnapped = file.Exists("temporalassassin/lanternmemory/tas_vaniya/05.txt", "DATA")
	
	SetupLantern( Vector(-718.265,1514.051,208.031), Angle(0,-30.17,0), "05", "Attic Respute", Vector(-713.116,1647.829,208.031), false, TASVaniya_AtticRest, false, true, true )

	
	// Messages
	
	// Intro	
	function TASVaniya_MSG_Intro()
		ANTONIO_TALK("Well if it isn't the Wilde one, good to see you're finally here.",1)
		ANTONIO_TALK("I'm Antonio, welcome to this little respite B' got for us, get yourself setup.",2)
		ANTONIO_TALK("When you're ready, you can use that little lantern to transport out. Should still be connected.",2)
	end
	
	function TASVaniya_MSG_WeaponPickup()
		ANTONIO_TALK("Huh, so this is the surprise? B' said he found something interesting in the area.",1)
		ANTONIO_TALK("It reminds me of a weapon I got too familiar with once, the Reiterpallasch.",2)
		ANTONIO_TALK("Give it a spin then, I'm not one for weapons and I know you love expanding your arsenal.",2)
	end
		
	function TASVaniya_MSG_ChurchLook()
		GLOB_TutorialHad = true
		ANTONIO_TALK("If you guessed then you got it right, that church is where I need you.",1)
		ANTONIO_TALK("There's something that needs to be destroyed inside before I can move people in.",2)
		ANTONIO_TALK("They locked it up tight though, follow the cables and see if you can find something.",2)
	end
		
	if GLOB_TutorialHad then
	
		function TASVaniya_MSG_Intro()
			--[[ print("I've been disabled!") ]]
		end
		
		function TASVaniya_MSG_WeaponPickup()
			--[[ print("I've been disabled!") ]]
		end
		
		function TASVaniya_MSG_ChurchLook()
			--[[ print("I've been disabled!") ]]
		end
		
	end
	
	//IntroEnd
	function TASVaniya_MSG_A1_Clear()
		ANTONIO_TALK("What a spectacle, I can see why B' likes you.",1)
	end
	
	function TASVaniya_MSG_A2_GateDeact()
		ANTONIO_TALK("Terminal barrier's gone, though I question how that APC got there.",1)
	end
	
	function TASVaniya_MSG_A2_Trap()
		ANTONIO_TALK("A Malfunction occurred, standby.",1)
	end
	
	function TASVaniya_MSG_A2_Clear()
		ANTONIO_TALK("Alright getting the door open now, glad to see some undead won't stop you either.",1)
	end
	
	function TASVaniya_MSG_A3_Clear()
		ANTONIO_TALK("The way you work, you'd be an ace in no time..",1)
		ANTONIO_TALK("Ah well, I think that strider left the door open, check it out.",2)
	end
	
	function TASVaniya_MSG_C2_Gen()
		ANTONIO_TALK("There's the generator.. Structurally sound. Try going down.",1)
	end
	
	function TASVaniya_MSG_C2_Tutorial()
		ANTONIO_TALK("That green energy, break the containment on that and it'll destablize the whole thing.",1)
		ANTONIO_TALK("You might make a dent with that weapon if you power it up first, maybe a powered shotgun blast?",2)
	end
	
	function TASVaniya_MSG_C2_Tutorial2()
		ANTONIO_TALK("The weapon is absorbing blood, maybe its doing something else with it. Try flicking it around.",1)
		ANTONIO_TALK("Maybe it'll trigger something and you can use that powered blast.",1)
	end
	
	function TASVaniya_MSG_C2_GoodJobYouFuckedItUp()
		ANTONIO_TALK("Good, the current is getting unstable.",1)
		ANTONIO_TALK("They sent reinforcements, finish the mission at all costs.",2)
	end
	
	function TASVaniya_MSG_C2_Final()
		ANTONIO_TALK("Amazing work! Portal's coming up.",1)
	end
	
	// ButtonPress
	function TASVaniya_MSG_Button1()
		ANTONIO_TALK("Triple lock system, figures. Follow the other cables.",1)
		ANTONIO_TALK("By the way, my boys dropped off some supplies incase you're hurting.",2)
		ANTONIO_TALK("You can find a lantern to return to the respite and get them, or to any other lanterns you lit.",2)
	end
	
	function TASVaniya_MSG_Button2()
		ANTONIO_TALK("One more lock to go.",1)
	end
	
	function TASVaniya_MSG_Button3()
		ANTONIO_TALK("That's all three, locks are undone. Time to go ransack a church.",1)
	end
	
	// Second Reiterpallasch
	function TASVaniya_MSG_ReiterSecond()
		ANTONIO_TALK("It got Badges? They appear to be unlocking the potential of the weapon.",1)
		ANTONIO_TALK("The flaming blood does remind me of a group that used similar methods and weaponry to fight.",2)
		ANTONIO_TALK("They're probably related in some astral sense.",2)
	end

	// Buttons Extra
	function TASVaniya_Button01()
		cable01:SetTexture ( "$basetexture", "cable/Combine_Cable01_Unlit" )
		VaniyaProgression()
	end
	
	function TASVaniya_Button02()
		cable02:SetTexture ( "$basetexture", "cable/Combine_Cable01_Unlit" )
		VaniyaProgression()
	end
	
	function TASVaniya_Button03()
		cable03:SetTexture ( "$basetexture", "cable/Combine_Cable01_Unlit" )
		VaniyaProgression()
	end
	
	function TASVaniya_CableAPC()
		cableAPC:SetTexture ( "$basetexture", "cable/Combine_Cable01_Unlit" )
	end
	
	// This is triggered by a trigger in the Assassin's Dream, and is used to reset several items all at once every time the level is spawned.
	function TASVaniya_Reset()
		cable01:SetTexture ( "$basetexture", "cable/Combine_Cable01_Lit" )
		cable02:SetTexture ( "$basetexture", "cable/Combine_Cable01_Lit" )
		cable03:SetTexture ( "$basetexture", "cable/Combine_Cable01_Lit" )
		cableAPC:SetTexture ( "$basetexture", "cable/Combine_Cable01_Lit" )
		VaniyaProgressionRead()
		if TASVaniya_IfAtticSnapped then
			TASVaniya_AtticRest()
		end
	end
	
	// T0
	CreateClothesLocker( Vector(113.543,-4050.776,-2015.5), Angle(-3, 238, 0) )
	Create_FalanranWeapon( Vector(30.715, -4139.639, -2008.326), Angle(-2.633, -117.168, 0.069) )

	function TASVaniya_B1ItemsSpawn()
		CreateSupplyBox( Vector(-183.578,-4208.469,-2019.969), Angle(0,-26.755,0), {"Health","Armour","Armour"} )
		CreateSupplyBox( Vector(-183.695,-4148.913,-2019.969), Angle(0,-26.755,0) )
	end
		
	function TASVaniya_B2ItemsSpawn()
		CreateSupplyBox( Vector(253.174,-4132.135,-2019.969), Angle(0,-26.755,0), {"Health","Health","Armour"} )
		CreateSupplyBox( Vector(251,-2035,-1050), Angle(0,-26.755,0) )
	end
	
	function TASVaniya_B3ItemsSpawn()
		CreateSupplyBox( Vector(58.289,-4342.888,-2019.969), Angle(0,-26.755,0), {"Health","Armour","Armour"} )
		CreateSupplyBox( Vector(50.289,-4300.888,-1950), Angle(0,-26.755,0) )
		CreateSupplyBox( Vector(75.289,-4380.888,-1900), Angle(0,-26.755,0) )
	end
	
	--[[ Experamental and dumb tutorial idea
	MESSAGE:AddMessage( "Hint_T0Statue", "Reiterpallasch can stab [Default: Left Click/Primary Fire] or slash [Default: R/Reload]. It uses a blood meter filled by blood of either yourself [Press and Hold Slash], or your enemies. This blood powers your weapon's gun [Hold Special Ability] which has several modes [Tap Special Ability] to suit different situations", 4, Vector(32.7,-4073.5,-1998), Angle(-90,0,0), Color(255,255,255), Color(100,90,90) )
	]]
	

	

	// T1
	
	// Outside
	Create_Armour( Vector(4781.771, -114.391, -839.969), Angle(0, 143.615, 0) )
	Create_Armour5( Vector(4812.54, -91.364, -839.969), Angle(0, 78.644, 0) )
	Create_Armour5( Vector(4785.864, -66.546, -839.969), Angle(0, 62.29, 0) )
	Create_PortableMedkit( Vector(3985.807, -535.706, -839.969), Angle(0, 47.713, 0) )
	Create_Beer( Vector(4139.768, -539.104, -839.969), Angle(0, 99.353, 0) )
	Create_Beer( Vector(4147.209, -523.006, -839.969), Angle(0, 103.619, 0) )
	Create_Beer( Vector(4154.53, -534.809, -839.969), Angle(0, 105.308, 0) )
	Create_Bread( Vector(5403.859, -215.865, -831.969), Angle(0, 253.418, 0) )
	Create_Beer( Vector(5413.342, -242.266, -831.969), Angle(0, 249.862, 0) )
	Create_Beer( Vector(5398.133, -246.369, -831.969), Angle(0, 252.706, 0) )
	
	CreateSupplyBox( Vector(5389.37,311.781,-799.554), Angle(0,-26.755,0), {"Health","Armour","Armour"} )
	
	// Inside	
	Create_CorzoHat( Vector(5197.593, -261.011, -831.969), Angle(0, 283.104, 0) )
	Create_Armour5( Vector(4284.489, -236.295, -831.969), Angle(0, 354.742, 0) )
	Create_Armour5( Vector(4284.327, -286.259, -831.969), Angle(0, 6.829, 0) )
	Create_Armour5( Vector(4283.121, -350.975, -831.969), Angle(0, 21.405, 0) )
	Create_Armour5( Vector(4283.922, -404.584, -831.969), Angle(0, 31.804, 0) )
	Create_Armour5( Vector(4290.758, -459.128, -831.969), Angle(0, 41.225, 0) )
	
	MESSAGE:AddMessage( "Hint_T1Precision", "Reiterpallasch is a precision weapon. It's capable of Headshots and critical damage to weak spots with thrusting attacks, similar to a gun.", 1, Vector(4896,-96,-832), Angle(6,123.69,0), Color(50,255,175), Color(100,255,175) )
	
	MESSAGE:AddMessage( "Hint_T1Transform", "Transforming [HOLD SPECIAL ATTACK] right after a gun or thrusting attack performs a Transform Attack.", 1, Vector(4433.828,-473.732,-831.969), Angle(0,-90,0), Color(255,175,50), Color(255,50,0) )

	// Path
	Create_Beer( Vector(3603.555, -2596.061, -849.805), Angle(0, 355.964, 0) )
	Create_Beer( Vector(3519.266, -2594.048, -858.419), Angle(0, 354.364, 0) )
	Create_Beer( Vector(3434.659, -2586.385, -859.768), Angle(0, 353.209, 0) )
	Create_Beer( Vector(3353.485, -2567.79, -861.19), Angle(0, 349.565, 0) )
	Create_Beer( Vector(2964.955, -2326.779, -841.144), Angle(0, 317.746, 0) )
	Create_Beer( Vector(2978.896, -2322.45, -841.516), Angle(0, 315.169, 0) )

	// T1BR
	function TASVaniya_T1BREnemySpawn()
		CreateSnapNPC("npc_zombie",Vector(156.51928710938,-1543.3681640625,-815.96875),Angle(0,9.7461853027344,0),"false",false)
		CreateSnapNPC("npc_zombie",Vector(735.13507080078,-1556.2828369141,-815.96875),Angle(0,167.54425048828,0),"false",true)
	end
	
	MESSAGE:AddMessage( "Hint_T1REPEL", "Shooting an enemy with Reiterpallasch right before their melee attack would hit you will repel them, freezing them and allowing you to press [USE KEY] to deal a large burst of damage", 1, Vector(123.66,-1408.018,-943.969), Angle(0,49.834,0), Color(50,175,255), Color(100,225,175) )
	
	Create_UpgradeStaminaSmall( Vector(47.624, -1411.353, -903.969), Angle(0, 12.875, 0) )
	Create_UpgradeStaminaSmall( Vector(46.441, -1386.58, -903.969), Angle(0, 303.904, 0) )
	
	// A1P - Plaza
	Create_Armour( Vector(3238.868, -955.009, -959.969), Angle(0, 90.616, 0) )
	Create_Armour5( Vector(3238.868, -889.528, -959.969), Angle(0, 90.527, 0) )
	Create_Armour5( Vector(3238.868, -814.071, -959.969), Angle(0, 90.172, 0) )
	Create_Armour5( Vector(3238.868, -762.212, -959.969), Angle(0, 88.483, 0) )
	
	Create_Beer( Vector(2950, 428.295, -614.457), Angle(0, 88.217, 0) )
	Create_Beer( Vector(2950, 338.67, -567.181), Angle(0, 87.15, 0) )
	Create_Beer( Vector(2950, 244.33, -517.418), Angle(0, 87.594, 0) )
	Create_Beer( Vector(2950, 136.425, -460.5), Angle(0, 88.661, 0) )
	Create_Beer( Vector(2950, 6.665, -392.054), Angle(0, 94.794, 0) )
	
	Create_Medkit10( Vector(513.649, -248.971, -831.969), Angle(0, 343.084, 0) )
	Create_Medkit10( Vector(532.457, -252.097, -831.969), Angle(0, 342.196, 0) )
	Create_Armour5( Vector(523.394, -268.197, -831.969), Angle(0, 348.506, 0) )
	
	Create_ManaCrystal( Vector(2685.94, -118.985, -895.969), Angle(0, 117.471, 0) )
	Create_ManaCrystal( Vector(2617.571, -123.83, -895.969), Angle(0, 99.339, 0) )
	Create_ManaCrystal( Vector(2545.171, -124.554, -895.969), Angle(0, 78.808, 0) )
	Create_ManaCrystal( Vector(2485.764, -125.503, -895.969), Angle(0, 63.787, 0) )
	Create_ManaCrystal( Vector(2417.674, -128.071, -895.969), Angle(0, 50.632, 0) )
	Create_Armour5( Vector(2650.596, 342.485, -891.969), Angle(0, 251.323, 0) )
	Create_Armour5( Vector(2635.273, 349.773, -891.969), Angle(0, 254.879, 0) )
	Create_Armour5( Vector(2642.04, 337.463, -891.969), Angle(0, 252.746, 0) )
	
	Create_Beer( Vector(1443.28, 135.302, -927.776), Angle(0, 20.805, 0) )
	Create_Beer( Vector(1438.675, 135.821, -916.582), Angle(0, 315.833, 0) )
	Create_Beer( Vector(1434.179, 136.449, -928.068), Angle(0, 316.455, 0) )
	
	TASVaniya_A1FountainCrystal = Create_ManaCrystal100( Vector(1471.781, 319.959, -680), Angle(0, 186.246, 0) )
	TASVaniya_A1FountainCrystal:SetCollisionBounds( Vector(-48,-48,-32), Vector(48,48,64) )
	
	// A1P2 - Plaza Building
	Create_Armour5( Vector(621.992, 236.693, -823.969), Angle(0, 99.982, 0) )
	Create_Armour5( Vector(619.868, 279.997, -823.969), Angle(0, 102.204, 0) )
	Create_Armour5( Vector(618.519, 321.193, -823.969), Angle(0, 106.47, 0) )
	Create_Bread( Vector(287.939, 289.422, -703.969), Angle(0, 319.96, 0) )
	Create_Beer( Vector(308.19, 286.667, -703.969), Angle(0, 313.65, 0) )
	Create_Beer( Vector(300.997, 281.831, -703.969), Angle(0, 318.493, 0) )
	Create_Armour5( Vector(129.28, 667.848, -955.969), Angle(0, 81.229, 0) )
	Create_Armour5( Vector(129.298, 713.334, -955.969), Angle(0, 76.429, 0) )
	Create_Armour5( Vector(157.583, 745.438, -955.969), Angle(0, 100.427, 0) )
	Create_Armour5( Vector(204.271, 751.155, -955.969), Angle(0, 142.556, 0) )
	Create_Medkit10( Vector(528.591, 691.838, -823.969), Angle(0, 237.479, 0) )
	Create_Medkit10( Vector(511.057, 694.426, -823.969), Angle(0, 191.084, 0) )

	function TASVaniya_A1EnemySpawn()
		local TAS_DifficultyLevel = GetDifficulty2()
		CreateSnapNPC("npc_metropolice",Vector(711.44305419922,-88.122299194336,-823.96875),Angle(0,2,0),"weapon_357",false)
		CreateSnapNPC("npc_metropolice",Vector(1316.9471435547,758.28100585938,-959.96875),Angle(0,456,0),"weapon_smg1",false)
		CreateSnapNPC("npc_metropolice",Vector(610.07086181641,771.46606445313,-959.96875),Angle(0,0,0),"weapon_shotgun",false)
		CreateSnapNPC("npc_combine_s",Vector(2329.6948242188,8.6079177856445,-895.96875),Angle(0,180,0),"weapon_shotgun",false)
		CreateSnapNPC("npc_combine_s",Vector(2424.2827148438,63.711311340332,-895.96875),Angle(0,180,0),"weapon_smg1",false)
		CreateSnapNPC("npc_combine_s",Vector(2266.3864746094,64.223510742188,-895.96875),Angle(0,180,0),"weapon_smg1",false)
		CreateSnapNPC("npc_metropolice",Vector(662.25903320313,273.75970458984,-959.96875),Angle(0,357,0),"weapon_stunstick",false)
		CreateSnapNPC("npc_metropolice",Vector(771.82275390625,279.4596862793,-959.96875),Angle(0,357,0),"weapon_stunstick",false)
		CreateSnapNPC("npc_metropolice",Vector(710.33184814453,240.03161621094,-959.96875),Angle(0,358,0),"weapon_stunstick",false)
		CreateSnapNPC("npc_metropolice",Vector(1886.0034179688,814.48223876953,-959.96875),Angle(0,144,0),"weapon_pistol",false)
		CreateSnapNPC("npc_metropolice",Vector(548.99389648438,831.0869140625,-959.96875),Angle(0,0,0),"weapon_pistol",false)
		CreateSnapNPC("npc_metropolice",Vector(462.86364746094,782.87493896484,-959.96875),Angle(0,0,0),"weapon_pistol",false)
		CreateSnapNPC("npc_metropolice",Vector(747.39019775391,114.9969329834,-823.96875),Angle(0,357,0),"weapon_pistol",false)
		CreateSnapNPC("npc_metropolice",Vector(747.31823730469,26.503631591797,-823.96875),Angle(0,0,0),"weapon_pistol",false)
		CreateSnapNPC("npc_metropolice",Vector(707.23937988281,818.27270507813,-959.96875),Angle(0,330,0),"weapon_smg1",false)
		CreateSnapNPC("npc_combine_s",Vector(2491.232421875,585.66198730469,-639.96875),Angle(0,270,0),"weapon_smg1",false) // BalconySMG1
		CreateSnapNPC("npc_metropolice",Vector(769.28179931641,-118.57230377197,-703.96875),Angle(0,0,0),"weapon_smg1",false)
		CreateSnapNPC('npc_combine_s',Vector(2076,-212,-320),Angle(0,180,0),'tnpc_grenadelauncher',false) //BuldingGrenade
		if TAS_DifficultyLevel > 5 then
			CreateSnapNPC('npc_combine_s',Vector(688,-24,-440),Angle(0,0,0),'tnpc_grenadelauncher',false) // ChurchGrenade
			CreateSnapNPC("npc_metropolice",Vector(771.22613525391,112.94702148438,-703.96875),Angle(0,357,0),"weapon_smg1",false) //BalconySMG2
		end
	end
	
	// A1B - Heavy Ambush
	function A1BuildingHeavySpawn()
		CreateNPC("npc_heavycombine",Vector(2944,-160,-1018),Angle(0,270,0),"tnpc_crystalrifle",false)
	end

	// A1B - Building
	Create_Armour5( Vector(2984.396, -674.662, -447.969), Angle(0, 91.47, 0) )
	Create_Armour5( Vector(2984.12, -634.349, -447.969), Angle(0, 92.003, 0) )
	Create_Armour5( Vector(2983.342, -601.333, -447.969), Angle(0, 92.448, 0) )
	Create_Armour5( Vector(2983.583, -751.339, -447.969), Angle(0, 259.4, 0) )
	Create_Armour5( Vector(2985.563, -795.995, -447.969), Angle(0, 251.667, 0) )
	Create_Armour5( Vector(2988.719, -841.668, -447.969), Angle(0, 224.026, 0) )
	Create_Medkit25( Vector(2062.051, -999.93, -447.969), Angle(0, 56.184, 0) )
	Create_EldritchArmour10( Vector(2037.356, -538.75, -443.969), Angle(0, 16.188, 0) )
	Create_EldritchArmour10( Vector(2038.894, -505.564, -443.969), Angle(0, 2.856, 0) )

	// A1B F2 - Building Floor 2
	Create_Armour5( Vector(2302.963, -1039.879, -575.969), Angle(0, 116.477, 0) )
	Create_Armour5( Vector(2241.913, -1038.875, -575.969), Angle(0, 96.123, 0) )
	Create_Armour( Vector(2184.133, -1038.778, -575.969), Angle(0, 74.081, 0) )
	Create_Armour5( Vector(2111.831, -1039.978, -575.982), Angle(0, 59.326, 0) )
	Create_Armour5( Vector(2049.298, -1040.875, -575.933), Angle(0, 33.551, 0) )
	
	Create_Armour5( Vector(2127.547, -670.895, -559.969), Angle(0, 192.149, 0) )
	Create_Armour5( Vector(2276.753, -672.151, -560.107), Angle(0, 213.836, 0) )
	
	Create_Beer( Vector(2321.681, -394.06, -703.969), Angle(0, 316.581, 0) )
	Create_Beer( Vector(2221.2, -314.288, -703.969), Angle(0, 320.225, 0) )
	Create_Beer( Vector(2114.525, -258.472, -703.969), Angle(0, 326.536, 0) )
	Create_Beer( Vector(2275.038, -480.622, -671.969), Angle(0, 285.473, 0) )
	
	Create_Medkit10( Vector(3095.134, -116.296, -575.969), Angle(0, 239.579, 0) )
	Create_Medkit10( Vector(3078.648, -105.128, -575.969), Angle(0, 245.623, 0) )
	Create_Medkit25( Vector(3086.542, -144.917, -575.969), Angle(0, 237.002, 0) )
	
	function TASVaniya_A1B2Cops()
		CreateSnapNPC("npc_metropolice",Vector(2192.2497558594,-488.78134155273,-703.96875),Angle(0,352.72814941406,0),"weapon_pistol",false)
		CreateSnapNPC("npc_metropolice",Vector(2189.8103027344,-598.11529541016,-703.96875),Angle(0,0.99375915527344,0),"weapon_pistol",false)
		CreateSnapNPC("npc_metropolice",Vector(2590.85546875,-330.50396728516,-575.96875),Angle(0,314.15365600586,0),"weapon_smg1",false)
		CreateSnapNPC("npc_metropolice",Vector(2290.8605957031,-428.39056396484,-575.96875),Angle(0,314.15365600586,0),"weapon_smg1",false)
		CreateSnapNPC("npc_metropolice",Vector(3075.5729980469,-169.76544189453,-703.96875),Angle(0,257.49243164063,0),"weapon_smg1",false)
		CreateSnapNPC("npc_metropolice",Vector(2112.373046875,-969.16729736328,-703.96875),Angle(0,2.8597412109375,0),"weapon_smg1",false)
	end
	
	function TASVaniya_A1B3Stalkers()
		CreateNPC("npc_combine_s",Vector(2670.9528808594,-818.36730957031,-447.96875),Angle(0,118.22651672363,0),"weapon_smg1",false)
		CreateNPC("npc_combine_s",Vector(2632.0251464844,-843.57537841797,-447.96875),Angle(0,171.02122497559,0),"weapon_smg1",false)
		CreateNPC("npc_combine_s",Vector(2983.3828125,-785.09625244141,-447.96875),Angle(0,274.06903076172,0),"weapon_smg1",false)
	end

	// Secret Grave
	Create_UpgradeMana( Vector(-3364.698,651.748,-290), Angle(0, 33.477, 0) )
	CreateSecretArea( Vector(-3364.698,651.748,-290), Vector(-64, -64, -64), Vector(64, 64, 64), "Essence of life, from death", Secret_Find )

	// Dumpster Secret
	CreateSecretArea( Vector(2224, 3450, -900), Vector(-32, -32, 0), Vector(32, 32, 64), "Who'd throw this away?", Secret_Find )
	Create_ShadeCloak( Vector(2224, 3450, -900), Angle(0, 45, 0) )

	// A2 Tunnel
	Create_ManaCrystal( Vector(3774.323, 1894.701, -955.969), Angle(0, 11.973, 0) )
	Create_ManaCrystal( Vector(3878.039, 1895.856, -955.969), Angle(0, 14.728, 0) )
	Create_ManaCrystal( Vector(4010.755, 1893.489, -955.969), Angle(0, 21.66, 0) )

	// A2 Secret
	Create_FalanranWeapon( Vector(8560, 3904, -935.969), Angle(0, 214.787, 0) )
	Create_UpgradeStaminaSmall( Vector(8560, 3872, -935.969), Angle(0, 185.279, 0) )
	Create_UpgradeStaminaSmall( Vector(8560, 3936, -935.969), Angle(0, 232.474, 0) )
	
	CreateSecretArea( Vector(8576.024, 3895.554, -935.969), Vector(-64, -64, 0), Vector(64, 64, 64), "Reiterpallasch feels empowered now", Secret_Find )

	MESSAGE:AddMessage( "Hint_A2Hive", "Hive Zombies explode in a hail of hornets shortly after death, instantly after death if dealt explosive or extreme damage. Unless they're on fire or disintegrated.", 1, Vector(6669.79,2119.838,-966.5), Angle(0,-152.623,0), Color(255,175,50), Color(255,50,0) )

	// A2 - Zombie Infestation
	function TASVaniya_A2ZombieWandering()
	
		local TAS_DifficultyLevel = GetDifficulty2()
		
		CreateNPC("npc_zombie",Vector(6604.6088867188,3294.6730957031,-967.96875),Angle(0,175.8251953125,0),"false",false)
		CreateNPC("npc_zombie",Vector(6460.4243164063,3580.0231933594,-967.96875),Angle(0,225.51092529297,0),"false",false)
		CreateNPC("npc_zombie",Vector(6627.1206054688,2720.673828125,-967.45721435547),Angle(0,211.28828430176,0),"false",false)
		CreateNPC("npc_poisonzombie",Vector(6282.8969726563,3292.4611816406,-839.96875),Angle(0,181.11555480957,0),"false",false)
		
		if TAS_DifficultyLevel > 5 then
			CreateNPC("npc_fastzombie",Vector(6292.3896484375,2952.2578125,-967.96875),Angle(0,180.67105102539,0),"false",false)
			CreateNPC("npc_fastzombie",Vector(6913.3203125,3437.5478515625,-967.96875),Angle(0,175.8251953125,0),"false",false)
		end
		
		if TAS_DifficultyLevel > 10 then
			CreateNPC("npc_zombie",Vector(5992.5346679688,2104.4965820313,-967.96875),Angle(0,172.9248046875,0),"false",false)
			CreateNPC("npc_zombie",Vector(6723.4145507813,2446.0510253906,-967.96875),Angle(0,61.074794769287,0),"false",false)
			CreateNPC("npc_zombie",Vector(5793.03125,4061.2243652344,-964.84002685547),Angle(0,172.9248046875,0),"false",false)
			CreateNPC("npc_poisonzombie",Vector(6682.0942382813,2976.5676269531,-967.96875),Angle(0,172.9248046875,0),"false",false)
			CreateNPC("npc_fastzombie",Vector(6924.8032226563,2550.9479980469,-967.96875),Angle(0,122.09093475342,0),"false",false)
			CreateNPC("npc_fastzombie",Vector(5851.2075195313,2170.6342773438,-967.96875),Angle(0,172.9248046875,0),"false",false)
		end
		
	end

	function TASVaniya_A2HiveZombies()
	
		local TAS_DifficultyLevel = GetDifficulty2()
		
		CreateNPC("npc_hivezombie",Vector(8368,3904,-760),Angle(0,248,0),"false",false)
		
		if TAS_DifficultyLevel > 6 then
			CreateNPC("npc_hivezombie",Vector(8455.41015625,3767.5187988281,-775.96875),Angle(0,-135.94311523438,0),"false",false)
		end
		
		if TAS_DifficultyLevel > 14 then
			CreateNPC("npc_hivezombie",Vector(8091.0463867188,3810.4655761719,-775.96875),Angle(0,-51.684783935547,0),"false",false)
		end
		
	end
	
	function TASVaniya_A2ZombieTrapSpawn()
	
		local ZSpawn1 = "npc_zombie"
		
		if not IsMounted("ep2") and not IsMounted("episodic") then
			ZSpawn1 = "npc_zombie"
		else
			ZSpawn1 = "npc_zombine"
		end
		
		CreateNPC(ZSpawn1,Vector(7616,3200,-752),Angle(0,214,0),"false",true)
		
	end

	
	function TASVaniya_A2ItemsSpawn()
	
		Create_Armour( Vector(6284.785, 3236.744, -839.969), Angle(0, 180.943, 0) )
		Create_Armour5( Vector(6283.796, 3296.736, -839.969), Angle(0, 180.943, 0) )
		Create_Armour5( Vector(6282.932, 3349.229, -839.969), Angle(0, 180.943, 0) )
		Create_Armour5( Vector(6281.82, 3416.72, -839.969), Angle(0, 180.943, 0) )
		Create_Bread( Vector(6518.226, 3578.986, -775.969), Angle(0, 271.511, 0) )
		Create_Bread( Vector(6585.704, 3580.766, -775.969), Angle(0, 271.511, 0) )
		Create_Beer( Vector(6638.187, 3582.15, -775.969), Angle(0, 271.511, 0) )
		Create_Beer( Vector(6690.67, 3583.535, -775.969), Angle(0, 271.511, 0) )
		Create_Medkit10( Vector(7041.811, 3925.444, -967.969), Angle(0, 292.789, 0) )
		Create_Medkit10( Vector(7081.691, 3922.836, -967.969), Angle(0, 279.279, 0) )
		Create_Medkit10( Vector(6972.627, 3938.345, -967.969), Angle(0, 267.992, 0) )
		Create_Medkit10( Vector(6928.509, 3939.066, -967.969), Angle(0, 282.879, 0) )
		Create_Medkit25( Vector(6239.942, 2756.283, -775.969), Angle(0, 86.587, 0) )
		Create_Beer( Vector(7119.497, 2926.343, -811.194), Angle(0, 173.104, 0) )
		Create_Beer( Vector(7069.063, 2939.26, -825.143), Angle(0, 174.704, 0) )
		Create_Beer( Vector(7021.866, 2949.696, -838.082), Angle(0, 176.481, 0) )
		Create_Beer( Vector(6979.042, 2965.961, -850.293), Angle(0, 181.636, 0) )
		Create_ManaCrystal( Vector(6300.464, 2751.692, -775.969), Angle(0, 273.205, 0) )
		Create_ManaCrystal( Vector(6168.099, 2755.832, -775.969), Angle(0, 272.138, 0) )
		Create_Medkit25( Vector(7113.607, 2468.176, -775.969), Angle(0, 300.58, 0) )
		Create_Medkit10( Vector(7129.15, 2464.23, -775.969), Angle(0, 287.959, 0) )
		
		CreateSnapNPC("npc_exhumed",Vector(7664,3072,-938),Angle(0,180,0),"false",false)
	end
	
	Create_Beer( Vector(7445.748, 1950.813, -775.969), Angle(0, 109.021, 0) )
	Create_Beer( Vector(7439.714, 2001.643, -775.969), Angle(0, 111.954, 0) )
	Create_Beer( Vector(7438.374, 2054.482, -775.969), Angle(0, 118.443, 0) )
	Create_Beer( Vector(7439.01, 2100.838, -775.969), Angle(0, 128.842, 0) )
	Create_GrenadeAmmo( Vector(7450.446, 2619.015, -735.969), Angle(0, 359.974, 0) )
	//Create_GrenadeAmmo( Vector(7451.697, 2612.818, -735.969), Angle(0, 4.551, 0) )
	Create_GrenadeAmmo( Vector(7443.197, 2619.219, -735.969), Angle(0, 359.841, 0) )
	Create_GrenadeAmmo( Vector(7450.74, 3094.832, -741.576), Angle(0, 183.027, 0) )
	Create_Medkit25( Vector(8222.136, 1698.559, -775.969), Angle(0, 139.258, 0) )
	Create_Medkit10( Vector(8190.365, 1694.825, -775.969), Angle(0, 132.325, 0) )
	Create_Medkit10( Vector(8190.945, 1708.314, -775.969), Angle(0, 135.258, 0) )

	
	// A2 Hive
	Create_Armour( Vector(8611.784, 3999.337, -775.969), Angle(0, 210.451, 0) )
	Create_Armour5( Vector(8625.971, 3979.586, -775.969), Angle(0, 206.273, 0) )
	Create_Armour5( Vector(8598.779, 3969.314, -775.969), Angle(0, 206.807, 0) )
	Create_EldritchArmour( Vector(8101.213, 3995.391, -775.969), Angle(0, 307.241, 0) )
	Create_Medkit10( Vector(8238.569, 4040.828, -775.969), Angle(0, 293.198, 0) )
	Create_Medkit10( Vector(8257.396, 4048.803, -775.969), Angle(0, 288.043, 0) )
	Create_Beer( Vector(8465.855, 2882.331, -903.969), Angle(0, 262.247, 0) )
	Create_Beer( Vector(8462.108, 2866.765, -903.969), Angle(0, 262.78, 0) )

	// A2 - Outside Trap
	function TASVaniya_A2OutsideTrap()
	
		local A2Set1 = "weapon_ar2"
		local A2Set2 = "weapon_357"
		local A2Set3 = "weapon_smg1"
		local TAS_DifficultyLevel = GetDifficulty2()
		
		if TAS_DifficultyLevel > 8 then
			A2Set1 = "weapon_357"
			else
			A2Set1 = "weapon_pistol"
		end
		
		CreateNPC("npc_metropolice",Vector(2624,2336,-764),Angle(0,0,0),A2Set1,false)
		CreateNPC("npc_metropolice",Vector(2624,1920,-764),Angle(0,0,0),A2Set1,false)
		CreateNPC("npc_combine_s",Vector(2432,2112,-919),Angle(0,0,0),"weapon_smg1",false)
		
		if TAS_DifficultyLevel > 8 then
			CreateNPC("npc_combine_s",Vector(1604,2225,-857),Angle(0,0,0),"tnpc_sniperrifle",false)
		end
		
	end
	
	// A3 - Tree Yard
	Create_Bread( Vector(-588.899, 4114.91, 0.031), Angle(0, 268.469, 0) )
	Create_Bread( Vector(-498.931, 4112.505, 0.031), Angle(0, 268.469, 0) )
	Create_Beer( Vector(-408.964, 4110.1, 0.031), Angle(0, 268.469, 0) )
	Create_Beer( Vector(-289.007, 4106.893, 0.031), Angle(0, 268.469, 0) )
	Create_Beer( Vector(-344.845, 4105.708, 0.031), Angle(0, 279.49, 0) )
	
	Create_Beer( Vector(518.12, 5465.828, -635), Angle(0, 229.273, 0) )
	Create_Beer( Vector(527.531, 5461.123, -635), Angle(0, 227.407, 0) )
	Create_Armour( Vector(511.953, 5479.783, -635), Angle(0, 231.762, 0) )
	
	Create_Medkit50( Vector(-744.567, 6019.094, 0.031), Angle(0, 5.74, 0) )
	
	Create_ManaCrystal( Vector(-2342.655, 6033.479, 0.031), Angle(0, 127.239, 0) )
	Create_ManaCrystal( Vector(-2386.371, 6030.188, 0.031), Angle(0, 105.997, 0) )
	Create_ManaCrystal( Vector(-2430.602, 6030.872, 0.031), Angle(0, 80.488, 0) )
	
	Create_Armour5( Vector(-3024.5, 4573.299, 2.834), Angle(0, 78.444, 0) )
	Create_Armour5( Vector(-3045.835, 4503.832, 2.833), Angle(0, 77.2, 0) )
	Create_Armour5( Vector(-3063.291, 4441.868, 2.834), Angle(0, 76.667, 0) )
	Create_Armour( Vector(-3083.602, 4367.762, 2.834), Angle(0, 76.311, 0) )
	
	Create_Beer( Vector(-573.514, 5027.354, 0.031), Angle(0, 89.554, 0) )
	Create_Beer( Vector(-453.518, 5026.425, 0.031), Angle(0, 89.554, 0) )
	Create_Bread( Vector(-371.02, 5025.786, 0.031), Angle(0, 89.554, 0) )
	Create_Bread( Vector(-236.024, 5024.74, 0.031), Angle(0, 89.554, 0) )
	Create_Beer( Vector(-661.672, 5028.551, 0.031), Angle(0, 85.466, 0) )
	
	Create_Armour( Vector(1444.188, 5454.32, -639.969), Angle(0, 209.134, 0) )
	Create_Armour5( Vector(1429.308, 5453.125, -639.969), Angle(0, 210.023, 0) )
	
	Create_Medkit10( Vector(-2749.414, 3774.059, 0.031), Angle(0, 271.35, 0) )
	Create_Medkit10( Vector(-2804.914, 3777.672, 0.031), Angle(0, 280.682, 0) )
	Create_Medkit10( Vector(-2865.695, 3777.795, 0.031), Angle(0, 290.281, 0) )
	
	Create_MaskOfShadows( Vector(-2217.215, 5945.096, 374.731), Angle(0, 311.524, 0) )
	
	Create_Medkit25( Vector(-3.324, 3933.886, -511.969), Angle(0, 348.142, 0) )
	Create_Medkit10( Vector(26.464, 3937.146, -511.969), Angle(0, 346.009, 0) )
	Create_Medkit10( Vector(22.552, 3922.883, -511.969), Angle(0, 349.386, 0) )
	
	Create_Armour( Vector(460.279, 4426.292, -510.969), Angle(0, 189.491, 0) )
	Create_Armour5( Vector(441.543, 4428.72, -510.969), Angle(0, 191.713, 0) )
	
	Create_Bread( Vector(464.383, 4407.968, -510.969), Angle(0, 182.825, 0) )
	Create_Bread( Vector(448.076, 4395.457, -510.969), Angle(0, 178.203, 0) )
	
	Create_Bread( Vector(-2185.038, 5972.684, 0.031), Angle(0, 276.096, 0) )
	Create_Bread( Vector(-2182.182, 5939.639, 0.031), Angle(0, 276.718, 0) )
	Create_Bread( Vector(-2179.247, 5910.064, 0.031), Angle(0, 277.696, 0) )
	
	// A3 Hint
	MESSAGE:AddMessage( "Hint_A3BloodRefill", "Holding Slash [Default: RELOAD KEY] will cause you to cut yourself and add blood to Reiterpallasch's blood gauge using your own Health. However with a full Bone Marrow Gauge you will instead perform an Ash Flourish.", 1, Vector(444,5509,-639), Angle(0,-30,0), Color(200,20,10), Color(255,200,150) )
	
	// A3 Vent Mega Secret
	Create_MegaSphere( Vector(-380.036, 4494.533, 16.031), Angle(0, 268.113, 0) )
	Create_EldritchArmour( Vector(-394.783, 4492.375, 16.031), Angle(0, 278.236, 0) )
	Create_EldritchArmour10( Vector(-391.52, 4468.148, 16.082), Angle(0, 278.414, 0) )
	CreateSecretArea( Vector(-380.036, 4494.533, 16.031), Vector(-64, -64, 0), Vector(64, 64, 64), "Hm, such a suspicous spot for a Yokai", Secret_Find )
	
	// Danger Damage
	Create_QuadDamage( Vector(1729.141, 5071.726, -255.969), Angle(0, 186.377, 0) )
	
	// A3 - Hunter Mounted
	function TASVaniya_A3HunterSpawn()
	
		if not IsMounted("ep2") then
			CreateNPC("npc_combine_s",Vector(1451.1798095703,5057.7416992188,-653),Angle(0,169.11837768555,0),"tnpc_flechetteshotgun",false)
			CreateNPC("npc_combine_s",Vector(1461.5278320313,5163.80078125,-650.72094726563),Angle(0,169.11837768555,0),"tnpc_flechetteshotgun",false)
			CreateNPC("npc_combine_s",Vector(1545.3255615234,5078.8095703125,-652.10778808594),Angle(0,169.11837768555,0),"weapon_ar2",false)
			CreateNPC("npc_combine_s",Vector(1587.859375,5146.3740234375,-649.80102539063),Angle(0,176.89535522461,0),"weapon_smg1",false)
			CreateNPC("npc_combine_s",Vector(1588.2510986328,5015.3549804688,-648.41625976563),Angle(0,176.89535522461,0),"weapon_smg1",false)
		else
			CreateNPC("npc_hunter",Vector(1437.544921875,5017.6865234375,-652.46002197266),Angle(0,167.56297302246,0),"false",false)
			CreateNPC("npc_hunter",Vector(1442.9639892578,5226.1752929688,-645.80535888672),Angle(0,190.4051361084,0),"false",false)
		end
		
	end
	
	// A3 - Arena Combines
	function TASVaniya_A3CombineSpawn()
	
		CreateSnapNPC("npc_combine_s",Vector(-3001.8864746094,4433.7177734375,-639.96875),Angle(0,4.1457672119141,0),"tnpc_flechettemg",false)
		CreateSnapNPC("npc_combine_s",Vector(-2979.3454589844,4339.3251953125,-639.96875),Angle(0,4.1457672119141,0),"tnpc_flechettemg",false)
		CreateSnapNPC("npc_combine_s",Vector(-2902.1796875,4474.0776367188,-639.96875),Angle(0,4.1457672119141,0),"tnpc_grenadelauncher",false)
		CreateSnapNPC("npc_combine_s",Vector(-3086.5324707031,4375.0561523438,-639.96875),Angle(0,4.1457672119141,0),"tnpc_grenadelauncher",false)
		CreateSnapNPC("npc_combine_s",Vector(-2917.0485839844,4316.3515625,-639.96875),Angle(0,4.1457672119141,0),"tnpc_grenadelauncher",false)
		CreateSnapNPC("npc_combine_s",Vector(-2129.1787109375,6164.45703125,0.03125),Angle(0,310.05426025391,0),"weapon_smg1",false)
		CreateSnapNPC("npc_combine_s",Vector(-2260.2568359375,6143.3828125,0.03125),Angle(0,310.05426025391,0),"weapon_smg1",false)
		CreateSnapNPC("npc_combine_s",Vector(-2860.4982910156,3990.5139160156,0.03125),Angle(0,42.044982910156,0),"weapon_smg1",false)
		CreateSnapNPC("npc_combine_s",Vector(-2747,3982.3334960938,0.03125),Angle(0,42.044982910156,0),"weapon_shotgun",false)
		CreateSnapNPC("npc_combine_s",Vector(-2920,4395.02734375,-639.96875),Angle(0,6.715087890625,0),"weapon_shotgun",false)
		CreateSnapNPC("npc_metropolice",Vector(-571,4919.6923828125,0.03125),Angle(0,145.48442077637,0),"weapon_pistol",false)
		CreateSnapNPC("npc_metropolice",Vector(-432,4952.259765625,0.03125),Angle(0,95.08943939209,0),"weapon_pistol",false)
		CreateSnapNPC("npc_metropolice",Vector(-359,4784.8583984375,128.03125),Angle(0,84.112602233887,0),"weapon_pistol",false)
		CreateSnapNPC("npc_combine_s",Vector(322,4473,-639.96875),Angle(0,149.27136230469,0),"tnpc_grenadelauncher",false)
		CreateSnapNPC("npc_combine_s",Vector(-524,3614.4067382813,-639.96875),Angle(0,276.33453369141,0),"weapon_ar2",false)
		CreateSnapNPC("npc_combine_s",Vector(-590,3559.6115722656,-639.96875),Angle(0,276.33453369141,0),"weapon_ar2",false)
		CreateSnapNPC("npc_combine_s",Vector(-693.35290527344,4380.341796875,-639.96875),Angle(0,176.12231445313,0),"weapon_ar2",false)
		CreateSnapNPC("npc_combine_s",Vector(-699.48901367188,4308.1552734375,-639.96875),Angle(0,176.12231445313,0),"weapon_shotgun",false)
		CreateSnapNPC("npc_combine_s",Vector(-693.01171875,4218.435546875,-639.96875),Angle(0,176.12231445313,0),"weapon_shotgun",false)
		
		CreateSnapNPC("npc_combine_s",Vector(-303,4952.3369140625,0.03125),Angle(0,84.112602233887,0),"tnpc_grenadelauncher",false) //RPGer 
		
	end
	
	// A3 - Strider Difficulty Buff
	function TASVaniya_A3StriderDifficulty()
	
		local A3StriderFight = ents.FindByName('A3_Strider_Template')
		local TAS_DifficultyLevel = GetDifficulty2()
		local A3StriderHealth = 1500
		
		if TAS_DifficultyLevel <  5 then
			A3StriderHealth = 1500
		elseif TAS_DifficultyLevel < 8 then
			A3StriderHealth = 2000
		elseif TAS_DifficultyLevel < 10 then
			A3StriderHealth = 2500
		else
			A3StriderHealth = 3000
		end
		
		for k, v in ipairs ( A3StriderFight ) do
			v.FAKE_HEALTH = A3StriderHealth
			v:SetMaxHealth ( A3StriderHealth ) 
			v:SetHealth ( A3StriderHealth )	
		end
		
	end
	
	// A3 - Metrocops Console
	function TASVaniya_A3MetrocopSpawn()
	
		CreateNPC("npc_metropolice",Vector(776.11041259766,5951.4760742188,-127.96875),Angle(0,41.697662353516,0),"weapon_pistol",false)
		CreateNPC("npc_metropolice",Vector(694.69830322266,6138.9116210938,-127.96875),Angle(0,41.697662353516,0),"weapon_pistol",false)
		CreateNPC("npc_metropolice",Vector(859.1689453125,5894.390625,-127.96875),Angle(0,41.697662353516,0),"weapon_smg1",false)
		CreateNPC("npc_metropolice",Vector(792.96734619141,5972.4057617188,0.03125),Angle(0,21.877243041992,0),"weapon_pistol",false)
		
	end
	
	// C1
	Create_Bread( Vector(-2106.683, 1333.409, -287.969), Angle(0, 21.287, 0) )
	Create_Bread( Vector(-2098.115, 1360.554, -287.969), Angle(0, 8.133, 0) )
	Create_Medkit50( Vector(-2080.514, 1605.456, -287.969), Angle(0, 278.719, 0) )
	Create_Armour( Vector(-1073.056, 1887.767, -127.969), Angle(0, 1.502, 0) )
	Create_Armour5( Vector(-1051.28, 1888.494, -127.969), Angle(0, 1.413, 0) )
	Create_Armour5( Vector(-1027.254, 1889.324, -127.969), Angle(0, 1.235, 0) )
	Create_ManaCrystal( Vector(-1405.962, 1599.696, 192.031), Angle(0, 358.603, 0) )
	Create_ManaCrystal( Vector(-1408.588, 1691.878, 192.031), Angle(0, 326.34, 0) )
	Create_ManaCrystal( Vector(-1407.645, 1508.883, 192.031), Angle(0, 31.489, 0) )
	CreateSecretArea( Vector(-776.911,1588.858,208.031), Vector(-64, -64, 0), Vector(64, 64, 64), "I feel like I can return here...", Secret_Find )
	
	// C1 - Heavy Ambush
	function C1HeavyCombineSpawn()
		CreateSnapNPC("npc_heavycombine",Vector(-1536,1456,-312),Angle(0,0,0),"tnpc_crystalrifle",false)
		CreateSnapNPC("npc_heavycombine",Vector(-1536,1744,-312),Angle(0,0,0),"tnpc_crystalrifle",false)
	end

	// C2
	MESSAGE:AddMessage( "Hint_C1Ashen", "While the Bone Marrow Gauge next to the Blood Gauge is full, Press and hold Slash [Default: RELOAD KEY] to perform an Ash Flourish which buffs and augments Reiterpallasch's shots.", 1, Vector(-2213.685,1151.213,-318.5), Angle(0,-210,0), Color(50,175,255), Color(0,150,255) )

	function C2ShielderCreator()
	
		local C2Shielder1 = ents.FindByName('C2_ShielderArm1_Shield')
		local C2Shielder2 = ents.FindByName('C2_ShielderArm2_Shield')
		
		for k,v in pairs (C2Shielder1) do 
			v:MakeCombineShielder()
		end
		
		for k,v in pairs (C2Shielder2) do 
			v:MakeCombineShielder()
		end
		
	end
	
	C2ShielderCreator()
	
	// Secret Bell
	CreateSecretArea( Vector(-785.024, 1589.534, 448.031), Vector(-50, -50, 0), Vector(50, 50, 50), "Ding Dong!", Secret_Find )
	Create_UpgradeStamina( Vector(-785.024, 1589.534, 448.031), Angle(0, 14.105, 0) )
	Create_UpgradeManaSmall( Vector(-803.226, 1566.553, 448.031), Angle(0, 30.726, 0) )
	Create_UpgradeManaSmall( Vector(-761.879, 1606.932, 448.031), Angle(0, 325.399, 0) )
	
	// C1 Secret Floor
	Create_GrenadeAmmo( Vector(-1965.611, 1525.068, -319.969), Angle(0, 276.429, 0) )
	Create_GrenadeAmmo( Vector(-1956.044, 1534.586, -319.969), Angle(0, 271.274, 0) )
	Create_GrenadeAmmo( Vector(-1957.133, 1515.074, -319.969), Angle(0, 272.163, 0) )
	Create_EldritchArmour10( Vector(-1921.864, 1537.811, -319.969), Angle(0, 255.364, 0) )
	Create_EldritchArmour( Vector(-1938.333, 1522.38, -319.969), Angle(0, 261.853, 0) )
	
	--[[ Hello, This is the trigger that makes the end beam particle effect active. ]]
	local FUCKIN_BEAM = function(self,ply)
		ply:SetSharedBool("END_LASER_LMAO",true)
	end
	CreateTrigger( Vector(-1947.29,1299.106,-319.969), Vector(-1947.29,1299.106,-319.969), Vector(-1064.126,1903.969,184.671), FUCKIN_BEAM, false, Angle(0,0,0) )
	
	// C2 - Arena
	Create_Medkit25( Vector(-893.517, 1222.782, -3007.969), Angle(0, 216.829, 0) )
	Create_Medkit10( Vector(-901.876, 1202.928, -3007.969), Angle(0, 214.874, 0) )
	Create_Medkit10( Vector(-910.237, 1207.819, -3007.969), Angle(0, 216.474, 0) )
	Create_Bread( Vector(-2184.192, 365.729, -3007.969), Angle(0, 103.685, 0) )
	Create_Bread( Vector(-2191.727, 463.546, -3007.969), Angle(0, 105.374, 0) )
	Create_Bread( Vector(-2184.632, 570.559, -3007.969), Angle(0, 109.996, 0) )
	Create_Bread( Vector(-2169.456, 645.629, -3007.969), Angle(0, 116.128, 0) )
	Create_Armour( Vector(-1602.542, 355.13, -3007.969), Angle(0, 93.109, 0) )
	Create_Armour5( Vector(-1623.059, 428.428, -3007.969), Angle(0, 90.175, 0) )
	Create_Armour5( Vector(-1601.948, 448.378, -3007.969), Angle(0, 94.175, 0) )
	Create_Armour5( Vector(-1619.941, 509.941, -3007.969), Angle(0, 90.975, 0) )
	Create_ManaCrystal( Vector(-889.43, 314.801, -2879.969), Angle(0, 206.253, 0) )
	Create_ManaCrystal( Vector(-893.401, 291.316, -2879.969), Angle(0, 197.898, 0) )
	Create_ManaCrystal( Vector(-903.509, 303.654, -2879.969), Angle(0, 204.564, 0) )
	Create_Turkey( Vector(-1180.38, 908.702, -2760.299), Angle(0, 152.747, 0) )
	Create_Beer( Vector(-2654.659, -40.128, -3007.969), Angle(0, 350.861, 0) )
	Create_Beer( Vector(-2654.359, -75.789, -3007.969), Angle(0, 355.66, 0) )
	Create_Beer( Vector(-2627.798, -65.526, -3007.969), Angle(0, 353.883, 0) )
	Create_Beer( Vector(-2522.902, -155.387, -3007.969), Angle(0, 9.437, 0) )
	Create_PortableMedkit( Vector(-2234.465, 1462.283, -319.969), Angle(0, 245.805, 0) )
	Create_PortableMedkit( Vector(-2237.472, 1488.445, -319.969), Angle(0, 220.296, 0) )
	Create_Armour200( Vector(-2288.711, 1492.79, -319.969), Angle(0, 274.424, 0) )

	Create_Beer( Vector(-1499.846, 481.427, -3007.969), Angle(0, 250.054, 0) )
	Create_Beer( Vector(-1462.839, 563.791, -3007.969), Angle(0, 124.644, 0) )
	Create_Beer( Vector(-1378.405, 600.712, -3007.969), Angle(0, 71.405, 0) )
	Create_Beer( Vector(-1287.777, 567.968, -3007.969), Angle(0, 35.498, 0) )
	Create_Beer( Vector(-1258.942, 481.223, -3007.969), Angle(0, 357.546, 0) )
	Create_Beer( Vector(-1290.222, 393.722, -3007.969), Angle(0, 325.905, 0) )
	Create_Beer( Vector(-1375.661, 357.819, -3007.969), Angle(0, 290.264, 0) )
	Create_Beer( Vector(-1462.942, 391.537, -3007.969), Angle(0, 229.025, 0) )

	// C2 - Backup
	function C2_NPCBackup()
	
		local TAS_DifficultyLevel = GetDifficulty2()
		local CSet1 = "weapon_smg1"
		local CSet2 = "weapon_smg1"
		
		if TAS_DifficultyLevel >  5 then
			CSet1 = "tnpc_flechettemg"
		else
			CSet1 = "weapon_smg1"
		end
		
		if TAS_DifficultyLevel >  10 then
			CSet2 = "weapon_shotgun"
		else
			CSet2 = "weapon_smg1"
		end
		
		//CreateSnapNPC("npc_combine_s",Vector(-2192.3061523438,-635.44616699219,-3007.96875),Angle(0,73.994407653809,0),CSet1,false)
		CreateSnapNPC("npc_combine_s",Vector(-2140.3918457031,-718.50939941406,-3007.96875),Angle(0,73.994407653809,0),CSet1,false)
		//CreateSnapNPC("npc_combine_s",Vector(-1991.5537109375,-711.80804443359,-3007.96875),Angle(0,92.659217834473,0),CSet2,false)
		CreateSnapNPC("npc_combine_s",Vector(-1930.265625,-637.15625,-3007.96875),Angle(0,92.659217834473,0),CSet2,false)
		CreateSnapNPC("npc_combine_s",Vector(-2117.2099609375,-555.26751708984,-3007.96875),Angle(0,84.83763885498,0),"tnpc_flechetteshotgun",false)
		CreateSnapNPC("npc_combine_s",Vector(-2012.8570556641,-559.90618896484,-3007.96875),Angle(0,89.281700134277,0),"tnpc_flechetteshotgun",false)
		CreateSnapNPC("npc_heavycombine",Vector(-2112,-736,-3001),Angle(0,90,0),"tnpc_crystalrifle",false)
		
	end

	function C2_GeneratorIsDead()
	
		for _,v in pairs (player.GetAll()) do
			if v and IsValid(v) then
				v:SetSharedBool("END_LASER_LMAO",false)
			end
		end
		
	end

	// Game End
	local GAME_END_TXT = {
	"[Operation : Sunday Stroll (SUCCESS)]",
	"[The Genesis Anti-Orbit Cannon's generator confirmed destroyed.]",
	"[Asset successfully exfiltrated, O.E. sent in for cleanup.]",
	"",
	"",
	"You did good, Wilde",
	"Hell better than good, I'd offer you a top position in O.E. if that Agency",
	"and B' weren't already holding on to ya, that's just how it goes sometimes.",
	"But atleast you still got something out of this, that weapon is a beauty.",
	"Try to take good care of it, it's seen its fair share of monsters,",
	"and it clearly needs a loving touch after being abandoned for so long.",
	"",
	"",
	"Maybe I'll get another request for you in the future",
	"but until then, keep on honing those skills. - Antonio"}
	
	function GameEndPortal()
	
		Vaniya_PortalEffect(Vector(-1888,-736,-2928), false)
		
		local UNLOCK_R = function(self,ply)
			ply:Unlock_TA_LoreFile( "REITER_UNLOCK", "" )
			file.Delete("temporalassassin/lanternmemory/tas_vaniya/05.txt")
		end
		CreateTrigger( Vector(-1888,-736,-2928), Vector(-33,-33,-33), Vector(33,33,33), UNLOCK_R )
		
		MakeGameEnd( GAME_END_TXT, 48, Vector(-1888,-736,-2928), Vector(-32,-32,-32), Vector(32,32,32) )
		
	end
	
	--[[ Hello it me, magneto, this is a quick little check to make sure the plug and func brush can't be unfrozen via weapon damage and thus hard crashing the game. ]]
	
	for XX,CHECK in pairs (ents.FindInSphere(Vector(8117.116,3875.138,-708.783),25)) do
		if CHECK and IsValid(CHECK) then
			CHECK.Super_Freeze = true
		end
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

--[[ I cleaned up some stuff! - Magenta ]]

--[[ Stuff for Antonio's Visage ]]

util.AddNetworkString("AntonioMessage")

function ANTONIO_TALK( str, type )

	if not type then type = 1 end
	
	net.Start("AntonioMessage")
	net.WriteString(str)
	
	if type == 2 then
		net.WriteBool(false)
	else
		net.WriteBool(true)
	end
	
	net.Broadcast()

end