To install, put the TAS_VaniyaBloodBorne folder into your Addons folder.

----

Level by Nessinby
Custom Assets made by Nessinby.
Custom Particles by Magenta Iris.
Programming by Magenta Iris
Special thanks to: Corvus, Alex, Lazy Shadow, Lt. Grey, & Moogleh for feedback and testing!
Everything else curtousy of Valve Corporation.