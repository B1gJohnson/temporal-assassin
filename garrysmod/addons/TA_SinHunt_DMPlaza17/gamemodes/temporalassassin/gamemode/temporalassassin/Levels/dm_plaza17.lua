		
if CLIENT then return end

util.AddNetworkString("SIN_UpdateCount")
util.AddNetworkString("SIN_AskForGoal")
util.AddNetworkString("SIN_SinOnMap")

function SIN_AskForGoal_LIB( len, pl )

	net.Start("SIN_UpdateCount")
	net.WriteUInt(LAST_SIN_COUNT,6)
	net.WriteUInt(SIN_COUNT_GOAL,6)
	net.Broadcast()

end
net.Receive("SIN_AskForGoal",SIN_AskForGoal_LIB)

GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true

GLOB_NO_FENIC = false
GLOB_NO_SINS = true

GLOB_SINHUNTER_SPAWN1 = false
GLOB_SINHUNTER_SPAWN2 = false
GLOB_SINHUNTER_SPAWNNPCS = 0
GLOB_SINHUNTER_ENEMYCOUNT = 0
GLOB_SINFUL_ENEMY_COUNT = 0

SIN_SPAWN_SPEED = 5
SIN_SPAWN_MAX = 40
SIN_ENEMIES_KILLED = 0
SIN_SINSPAWN_THRESHOLD = 20

local SPAWN_DIFFICULTIES = {}
SPAWN_DIFFICULTIES[1] = {5,25,15}
SPAWN_DIFFICULTIES[2] = {4,30,20}
SPAWN_DIFFICULTIES[3] = {3,40,25}
SPAWN_DIFFICULTIES[4] = {3,40,25}
SPAWN_DIFFICULTIES[5] = {2.5,50,30}
SPAWN_DIFFICULTIES[6] = {2,50,30}
SPAWN_DIFFICULTIES[7] = {1.5,50,30}
SPAWN_DIFFICULTIES[8] = {1.5,60,35}
SPAWN_DIFFICULTIES[9] = {1,70,40}
SPAWN_DIFFICULTIES[10] = {1,85,45}
SPAWN_DIFFICULTIES[11] = {1,85,45}
SPAWN_DIFFICULTIES[12] = {1,85,45} --[[ Just incase more difficulties ARE added ]]
SPAWN_DIFFICULTIES[13] = {1,85,45}
SPAWN_DIFFICULTIES[14] = {1,85,45}

GLOB_SINTYPE = math.random(1,5)
--[[
This defines what enemies will spawn for this sin hunter mission.

1 = Combine
2 = Zombies
3 = Antlions
4 = HL:S Humans
5 = HL:S Aliens

You can also define it as GLOB_SINTYPE = math.random(1,5) to make it random everytime its played
]]

local SIN_CLASSES = {}

SIN_CLASSES[1] = {
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s"}

if IsMounted("ep2") then
	SIN_CLASSES[1] = {
	"npc_metropolice",
	"npc_combine_s",
	"npc_metropolice",
	"npc_combine_s",
	"npc_metropolice",
	"npc_combine_s"}
end

SIN_CLASSES[2] = {
"npc_zombie",
"npc_zombie",
"npc_zombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_poisonzombie"}

SIN_CLASSES[3] = {
"npc_antlion"}

if IsMounted("ep2") then
	SIN_CLASSES[3] = {
	"npc_antlion",
	"npc_antlion",
	"npc_antlion",
	"npc_antlion_worker"}
end

SIN_CLASSES[4] = SIN_CLASSES[1]
SIN_CLASSES[5] = SIN_CLASSES[2]

if IsMounted("hl1") then

	SIN_CLASSES[4] = {
	"monster_human_grunt"}
	
	SIN_CLASSES[5] = {
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullsquid",
	"monster_houndeye",
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullchicken",
	"monster_houndeye"}
	
end

local BIGSIN_CLASSES = {}

BIGSIN_CLASSES[1] = {
"npc_combine_s"}

if IsMounted("ep2") then
	BIGSIN_CLASSES[1] = {
	"npc_hunter"}
end

BIGSIN_CLASSES[2] = {
"npc_poisonzombie"}

if IsMounted("ep2") or IsMounted("episodic") then
	BIGSIN_CLASSES[2] = {
	"npc_zombine"}
end

BIGSIN_CLASSES[3] = {
"npc_antlionguard"}
BIGSIN_CLASSES[4] = BIGSIN_CLASSES[1]
BIGSIN_CLASSES[5] = BIGSIN_CLASSES[2]

if IsMounted("hl1") then
	BIGSIN_CLASSES[4] = {
	"monster_human_assassin"}
	
	BIGSIN_CLASSES[5] = {
	"monster_alien_slave"}
end

local SIN_WEAPONS = {}
SIN_WEAPONS["npc_metropolice"] = {
"weapon_pistol",
"weapon_smg1",
"weapon_stunstick"}
SIN_WEAPONS["npc_combine_s"] = {
"weapon_smg1",
"weapon_ar2",
"weapon_shotgun"}

function SetAsRespawningItem( ent, del )

	if not ent then return end
	if not ent.ITEM_USE then return end
	
	local POS,ANG,MDL,USE = ent:GetPos(), ent:GetAngles(), ent:GetModel(), ent.ITEM_USE
	
	if not del then del = 30 end
	
	ent.OnRemove = function()
	
		timer.Simple(del,function()
		
			local N_ITEM = CreateItemPickup( POS, ANG, MDL, USE )
			CreatePointParticle( "Doll_EvilSpawn_Activate", POS, POS, Angle(0,0,0), Angle(0,0,0), 1 )
			sound.Play( Sound("TemporalAssassin/Doll/ExtraSpawn_Item.wav"), POS, 70, math.random(99,101), 0.7 )
			SetAsRespawningItem(N_ITEM,del)
			
		end)
	
	end

end

LAST_SIN_COUNT = 0
SIN_COUNT_GOAL = 5

function CanWeSpawnHere(pos)

	local TR = {}
	TR.start = pos
	TR.endpos = pos
	TR.mask = MASK_NPCSOLID
	TR.filter = {}
	table.Add(TR.filter,GLOB_TRIGGER_ENTS)
	TR.mins = Vector(-16,-16,0)
	TR.maxs = Vector(16,16,72)
	local Trace = util.TraceHull(TR)
	
	if Trace.Hit then return false end
	
	return true

end

function EnemySpawns_Think()

	local CT = CurTime()
	
	if SIN_ENEMIES_KILLED and SIN_ENEMIES_KILLED > SIN_SINSPAWN_THRESHOLD then
		SIN_ENEMIES_KILLED = 0
		Create_SinfulBoi()
	end
	
	net.Start("SIN_SinOnMap")
	if GLOB_SINFUL_ENEMY_COUNT and GLOB_SINFUL_ENEMY_COUNT > 0 then
		net.WriteBool(true)
	else
		net.WriteBool(false)
	end
	net.Broadcast()
	
	if GLOB_SIN_DEVOURED_COUNT and GLOB_SIN_DEVOURED_COUNT > LAST_SIN_COUNT then
	
		LAST_SIN_COUNT = GLOB_SIN_DEVOURED_COUNT
		
		local SIN_LEFT = (SIN_COUNT_GOAL - LAST_SIN_COUNT)
		
		net.Start("SIN_UpdateCount")
		net.WriteUInt(LAST_SIN_COUNT,6)
		net.WriteUInt(SIN_COUNT_GOAL,6)
		net.Broadcast()
		
		if SIN_LEFT <= 0 and not SIN_GAME_ENDED then
			SIN_GAME_ENDED = true
			END_SIN_HUNTER()			
		end
		
	end
	
	if GLOB_SINHUNTER_SPAWNNPCS and CT >= GLOB_SINHUNTER_SPAWNNPCS and GLOB_SINHUNTER_STARTED then
	
		GLOB_SINHUNTER_SPAWNNPCS = CT + SIN_SPAWN_SPEED
	
		local SPAWN_POS
	
		if GLOB_SINHUNTER_SPAWN2 then
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_FRONT) + Vector(0,0,5)
		else
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_BACK) + Vector(0,0,5)
		end
		
		if SPAWN_POS and GLOB_SINHUNTER_ENEMYCOUNT < SIN_SPAWN_MAX and GLOB_SINTYPE and SIN_CLASSES[GLOB_SINTYPE] then
		
			local CLASS = table.Random(SIN_CLASSES[GLOB_SINTYPE])
			local WEP = false
			
			if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
				WEP = table.Random(SIN_WEAPONS[CLASS])
			end
			
			if CanWeSpawnHere(SPAWN_POS) then
				CreateSnapNPC( CLASS, SPAWN_POS, Angle(0,math.random(-360,360),0), WEP, false )
				GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT + 1
			end
		
		end
	
	end

end
timer.Create("EnemySpawns_ThinkTimer", 0.5, 0, EnemySpawns_Think)

function Set_EnemySpawns_1()

	print("Back of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = true
	GLOB_SINHUNTER_SPAWN2 = false

end

function Set_EnemySpawns_2()

	print("Front of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = true

end

function START_SIN_HUNTER()

	RemoveAllTeleporters()
	
	--[[ This is the trigger that enables Back side map spawns ]]
	CreateTrigger( Vector(3946.578,-3646.885,-5394.692), Vector(-4665.483,-4665.483,0), Vector(4665.483,4665.483,7323.375), Set_EnemySpawns_1, true )
	
	--[[ This is the trigger that enables Front side map spawns ]]
	CreateTrigger( Vector(3924.24,6453.617,-6800.092), Vector(-5403.918,-5403.918,0), Vector(5403.918,5403.918,7825.285), Set_EnemySpawns_2, true )
	
	GLOB_SINHUNTER_STARTED = true
	GLOB_SINHUNTER_SPAWNNPCS = CurTime() + 20
	
	local DIFF = GetDifficulty2()

	if SPAWN_DIFFICULTIES and SPAWN_DIFFICULTIES[DIFF] then
		SIN_SPAWN_SPEED = SPAWN_DIFFICULTIES[DIFF][1]
		SIN_SPAWN_MAX = SPAWN_DIFFICULTIES[DIFF][2]
		SIN_SINSPAWN_THRESHOLD = SPAWN_DIFFICULTIES[DIFF][3]
	end
	
end

function END_SIN_HUNTER()

	GLOB_SINHUNTER_STARTED = false

	for _,v in pairs (ents.GetAll()) do
	
		if v and IsValid(v) then
		
			if v:IsNPC() then
				v:Remove() --[[ Lets remove all the npcs ]]
			end
		
		end
	
	end
	
	GLOB_SINFUL_ENEMY_COUNT = 0
	net.Start("SIN_SinOnMap")
	net.WriteBool(false)
	net.Broadcast()
	
	CreateTeleporter( "START_1", Vector(-1461.839,3686.379,138.114), "START_2", Vector(2731.987,1291.845,-1071.969) )
	
	local GAME_END_TEXT = {
	""}

	MakeGameEnd( GAME_END_TEXT, 3, Vector(-1463.581,3689.685,138.085), Vector(-103.137,-103.137,0), Vector(103.137,103.137,117.14) )
	
end

function Create_SinfulBoi()

	local SPAWNS_BACK = {
	Vector(3886.608,2542.564,-702.965),
	Vector(4945.72,1793.242,-683.568),
	Vector(2984.198,1697.49,-685.432)}
	
	local SPAWNS_FRONT = {
	Vector(3910.287,152.19,-691.907),
	Vector(4795.973,383.248,-714.093),
	Vector(3061.629,16.781,-952.36)}
	
	local SPAWN_POINT
	
	if GLOB_SINHUNTER_SPAWN2 then
		SPAWN_POINT = table.Random(SPAWNS_FRONT)
	elseif GLOB_SINHUNTER_SPAWN1 then
		SPAWN_POINT = table.Random(SPAWNS_BACK)
	end
	
	if not SPAWN_POINT then return end
	if not GLOB_SINTYPE then return end
	if not BIGSIN_CLASSES then return end
	if not BIGSIN_CLASSES[GLOB_SINTYPE] then return end
	
	local CLASS = table.Random(BIGSIN_CLASSES[GLOB_SINTYPE])
	local WEP = false
			
	if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
		WEP = table.Random(SIN_WEAPONS[CLASS])
	end
	
	GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT + 1
	
	local SIN_BOI = CreateSnapNPC( CLASS, SPAWN_POINT, Angle(0,math.random(-360,360),0), WEP, false )
			
	timer.Simple(0.1,function()
		if SIN_BOI and IsValid(SIN_BOI) then
			SIN_BOI:SetHealth( SIN_BOI:Health()*2 )
			SIN_BOI:CreateSinner(true)
			SIN_BOI:SetModelScale(1.2,0) --[[ Lets make sin bois a bit bigger in scale ]]
			if GLOB_SINTYPE == 1 then
				SIN_BOI:MakeCombineShielder()
			end
		end
	end)

end

function SinHunter_NPCDeath( npc, att, inf )

	if not npc.SinHealth then
		SIN_ENEMIES_KILLED = SIN_ENEMIES_KILLED + 1
		GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT - 1
	elseif npc.SinHealth then
		GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT - 1
	end

end
hook.Add("OnNPCKilled","SinHunter_NPCDeathHook",SinHunter_NPCDeath)

function SINHUNTER_DEATH_RESET()

	if #player.GetAll() <= 1 then

		GLOB_NO_SAVEDATA = true
		GLOB_SAFE_GAMEEND = true
		
		GLOB_NO_FENIC = false
		GLOB_NO_SINS = true
		
		GLOB_SINHUNTER_SPAWN1 = false
		GLOB_SINHUNTER_SPAWN2 = false
		GLOB_SINHUNTER_SPAWNNPCS = 0
		GLOB_SINHUNTER_ENEMYCOUNT = 0
		GLOB_SINFUL_ENEMY_COUNT = 0
		
		SIN_SPAWN_SPEED = 5
		SIN_SPAWN_MAX = 40
		SIN_ENEMIES_KILLED = 0
		SIN_SINSPAWN_THRESHOLD = 20
		
		GLOB_SIN_DEVOURED_COUNT = 0
		LAST_SIN_COUNT = 0
		SIN_COUNT_GOAL = 5
		
		GLOB_SINHUNTER_STARTED = false
		GLOB_SINHUNTER_SPAWNNPCS = false
		SIN_GAME_ENDED = false
		
	end

end
hook.Add("PlayerDeath","SINHUNTER_DEATH_RESETHOOK",SINHUNTER_DEATH_RESET)

function MAP_LOADED_FUNC()

	GLOB_NO_SAVEDATA = true
	GLOB_SAFE_GAMEEND = true
	
	GLOB_NO_FENIC = false
	GLOB_NO_SINS = true
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = false
	GLOB_SINHUNTER_SPAWNNPCS = 0
	GLOB_SINHUNTER_ENEMYCOUNT = 0
	GLOB_SINFUL_ENEMY_COUNT = 0
	
	SIN_SPAWN_SPEED = 5
	SIN_SPAWN_MAX = 40
	SIN_ENEMIES_KILLED = 0
	SIN_SINSPAWN_THRESHOLD = 20
	
	GLOB_SIN_DEVOURED_COUNT = 0
	LAST_SIN_COUNT = 0
	SIN_COUNT_GOAL = 5
	
	GLOB_SINHUNTER_STARTED = false
	GLOB_SINHUNTER_SPAWNNPCS = false
	SIN_GAME_ENDED = false
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("info_player_deathmatch")
	RemoveAllEnts("info_player_rebel")
	RemoveAllEnts("info_player_combine")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	RemoveAllEnts("ambient_generic")
	RemoveAllEnts("prop_physics_respawnable")
	
	local SP_1 = CreateProp(Vector(-1465.75,4206.063,189.063), Angle(-0.044,-0.044,0), "models/props_wasteland/cargo_container01b.mdl", true)
	local SP_2 = CreateProp(Vector(-1466.656,3826.063,189), Angle(0,180,0), "models/props_wasteland/cargo_container01b.mdl", true)
	SP_1:SetMaterial("models/props_combine/stasisshield_sheet")
	SP_2:SetMaterial("models/props_combine/stasisshield_sheet")
	
	CreatePlayerSpawn( Vector(-1464.474,4354.592,137.971), Angle(0,-90,0) )
	
	CreateClothesLocker( Vector(-1417.844,4283.656,137.813), Angle(0,181.383,0) )
	
	Create_PistolWeapon( Vector(-1468.204, 4042.057, 137.975), Angle(0, 90.473, 0) )
	Create_PistolAmmo( Vector(-1434.2, 4050.959, 137.979), Angle(0, 115.135, 0) )
	Create_PistolAmmo( Vector(-1456.399, 4071.737, 137.976), Angle(0, 103.222, 0) )
	Create_PistolAmmo( Vector(-1480.644, 4073.574, 137.974), Angle(0, 77.097, 0) )
	Create_PistolAmmo( Vector(-1498.127, 4062.557, 141.005), Angle(0, 64.871, 0) )
	Create_InfiniteGrenades( Vector(-1427.563, 4112.421, 137.979), Angle(0, 177.73, 0) )
	Create_Armour100( Vector(-1493.404, 3998.36, 138.115), Angle(0, 73.753, 0) )
	Create_Backpack( Vector(-1419.355, 4007.542, 138.116), Angle(0, 177.835, 0) )
	Create_EldritchArmour( Vector(-1456.453, 3996.766, 138.115), Angle(0, 96.534, 0) )
	Create_VolatileBattery( Vector(-1434.428, 3945.314, 138.115), Angle(0, 106.984, 0) )
	Create_FalanrathThorn( Vector(-1483.071, 3935.063, 138.115), Angle(0, 71.454, 0) )
	Create_CorzoHat( Vector(-1461.028, 3887.684, 138.115), Angle(0, 87.547, 0) )
	
	Create_ShotgunWeapon( Vector(3700.313, -441.135, -1039.969), Angle(0, 93.068, 0) )
	Create_SMGWeapon( Vector(2491.017, 859.068, -767.969), Angle(0, 1.323, 0) )
	Create_RevolverWeapon( Vector(3870.372, 2655.806, -767.969), Angle(0, 269.781, 0) )
	Create_CrossbowWeapon( Vector(3903.861, 1855.897, -527.969), Angle(0, 81.681, 0) )
	Create_FoolsOathWeapon( Vector(3564.862, 1367.013, -399.969), Angle(0, 90.776, 0) )
	Create_RifleWeapon( Vector(6340.162, 982.506, -759.969), Angle(0, 180.749, 0) )
	Create_SuperShotgunWeapon( Vector(5517.505, 898.042, -751.969), Angle(0, 1.432, 0) )
	Create_MegaSphere( Vector(6281.605, 273.726, -764.775), Angle(0, 176.258, 0) )
	Create_KingSlayerWeapon( Vector(4765.779, 725.729, -751.969), Angle(0, 268.218, 0) )
	Create_M6GWeapon( Vector(4894.569, -88.19, -557.186), Angle(0, 187.024, 0) )
	Create_PortableMedkit( Vector(2644.754, -718.573, -767.969), Angle(0, 91.09, 0) )
	Create_PortableMedkit( Vector(2702.961, -717.54, -767.969), Angle(0, 91.09, 0) )
	Create_PortableMedkit( Vector(2758.134, -716.491, -767.969), Angle(0, 91.09, 0) )
	Create_ManaCrystal100( Vector(3125.973, -667.985, -767.969), Angle(0, 174.899, 0) )
	Create_FalanranWeapon( Vector(3325.368, -535.584, -1024), Angle(0, 178.452, 0) )
	Create_ManaCrystal100( Vector(3105.626, 981.113, -763.969), Angle(0, 42.708, 0) )
	Create_EldritchArmour10( Vector(3620.796, 1029.51, -763.969), Angle(0, 126.517, 0) )
	Create_EldritchArmour10( Vector(3622.503, 1075.067, -763.969), Angle(0, 159.121, 0) )
	Create_EldritchArmour10( Vector(3539.02, 1074.582, -763.969), Angle(0, 29.541, 0) )
	Create_EldritchArmour10( Vector(3538.573, 1023.652, -763.969), Angle(0, 63.817, 0) )
	Create_SpiritSphere( Vector(3581.006, 1049.222, -763.969), Angle(0, 89.733, 0) )
	Create_ManaCrystal100( Vector(3471.881, 735.897, -766.969), Angle(0, 266.542, 0) )
	Create_FrontlinerWeapon( Vector(4448.634, 689.985, -511.969), Angle(0, 179.078, 0) )
	Create_Armour100( Vector(3612.101, 998.959, -515.969), Angle(0, 0.699, 0) )
	Create_Backpack( Vector(3723.789, 1121.532, -515.969), Angle(0, 270.411, 0) )
	Create_PortableMedkit( Vector(3086.937, 1201.278, -461.971), Angle(0, 280.756, 0) )
	Create_PortableMedkit( Vector(3086.195, 1231.043, -461.971), Angle(0, 280.547, 0) )
	Create_ShadeCloak( Vector(4276.681, 858.496, -759.969), Angle(0, 269.888, 0) )
	Create_HarbingerWeapon( Vector(4739.479, 156.527, -538.969), Angle(0, 182.836, 0) )
	Create_RaikiriWeapon( Vector(6253.739, 273.587, -767.618), Angle(0, 180.705, 0) )
	
	Create_MaskOfShadows( Vector(2769.095, 1567.625, -639.969), Angle(0, 179.6, 0) )
	Create_InvulnSphere( Vector(5503.159, 898.216, -503.969), Angle(0, 181.374, 0) )
	Create_OverdriveSphere( Vector(3138.454, 176.945, -895.969), Angle(0, 268.472, 0) )
		
	local HEALING = {
	Create_Medkit10( Vector(4029.344, 553.293, -763.969), Angle(0, 112.976, 0) ),
	Create_Medkit10( Vector(3991.05, 550.27, -763.969), Angle(0, 84.343, 0) ),
	Create_Medkit25( Vector(4430.205, 655.314, -766.969), Angle(0, 91.551, 0) ),
	Create_Armour( Vector(4422.38, 1018.216, -641.969), Angle(0, 272.545, 0) ),
	Create_Armour5( Vector(4021.017, 900.971, -515.969), Angle(0, 269.201, 0) ),
	Create_Armour5( Vector(4021.859, 992.245, -515.969), Angle(0, 269.619, 0) ),
	Create_Armour5( Vector(4022.139, 1066.278, -515.969), Angle(0, 269.828, 0) ),
	Create_Armour5( Vector(4022.164, 1154.026, -515.969), Angle(0, 270.037, 0) ),
	Create_Armour5( Vector(4022.126, 1243.762, -515.969), Angle(0, 270.037, 0) ),
	Create_Armour5( Vector(3385.115, 1245.56, -515.969), Angle(0, 89.461, 0) ),
	Create_Armour5( Vector(3384.904, 1151.45, -515.969), Angle(0, 90.088, 0) ),
	Create_Armour5( Vector(3384.72, 1057.693, -515.969), Angle(0, 89.879, 0) ),
	Create_Armour5( Vector(3384.519, 951.118, -515.969), Angle(0, 89.879, 0) ),
	Create_Armour5( Vector(3384.328, 860.378, -515.969), Angle(0, 89.879, 0) ),
	Create_Armour( Vector(3767.599, 1133.766, -763.969), Angle(0, 358.711, 0) ),
	Create_Medkit25( Vector(4472.48, 471.031, -759.969), Angle(0, 270.723, 0) ),
	Create_Medkit25( Vector(3035.828, 183.216, -767.969), Angle(0, 359.549, 0) ),
	Create_Beer( Vector(3159.977, 981.65, -515.969), Angle(0, 0.178, 0) ),
	Create_Medkit25( Vector(3550.331, -39.026, -999.969), Angle(0, 273.853, 0) ),
	Create_Medkit25( Vector(3600.404, -38.031, -999.969), Angle(0, 254.207, 0) ),
	Create_ManaCrystal( Vector(3781.351, 151.43, -1022.724), Angle(0, 195.167, 0) ),
	Create_ManaCrystal( Vector(3795.746, 107.211, -1020.727), Angle(0, 158.383, 0) ),
	Create_Medkit10( Vector(2538.384, 1681.204, -767.969), Angle(0, 177.821, 0) ),
	Create_Medkit10( Vector(2540.234, 1454.22, -767.969), Angle(0, 183.255, 0) ),
	Create_Medkit25( Vector(3565.828, 2695.553, -767.969), Angle(0, 268.736, 0) ),
	Create_Medkit25( Vector(4159.527, 2693.734, -767.969), Angle(0, 268.736, 0) ),
	Create_Medkit10( Vector(3903.561, 1772.599, -751.969), Angle(0, 268.109, 0) ),
	Create_Medkit10( Vector(3902.874, 1938.556, -751.969), Angle(0, 94.328, 0) ),
	Create_Medkit25( Vector(5696.664, 1543.342, -767.969), Angle(0, 271.144, 0) ),
	Create_Medkit10( Vector(6023.085, 1481.929, -751.969), Angle(0, 177.094, 0) ),
	Create_Medkit10( Vector(6024.126, 1331.842, -751.969), Angle(0, 189.843, 0) ),
	Create_ManaCrystal( Vector(6020.543, 1408.729, -751.969), Angle(0, 181.692, 0) ),
	Create_Medkit25( Vector(2537.357, -719.678, -767.969), Angle(0, 90.045, 0) ),
	Create_Medkit25( Vector(2843.273, -719.316, -767.969), Angle(0, 91.508, 0) ),
	Create_Medkit25( Vector(4084.241, 1144.975, -763.969), Angle(0, 181.168, 0) ),
	Create_Medkit10( Vector(3759.745, 1100.92, -763.969), Angle(0, 28.807, 0) ),
	Create_Medkit10( Vector(3758.555, 1162.102, -763.969), Angle(0, 333.421, 0) ),
	Create_Medkit25( Vector(4020.142, 839.328, -515.969), Angle(0, 90.569, 0) ),
	Create_Medkit10( Vector(3602.105, 958.282, -515.969), Angle(0, 0.49, 0) ),
	Create_Medkit10( Vector(3602.635, 1040.52, -515.969), Angle(0, 354.011, 0) ),
	Create_Beer( Vector(3208.474, 944.667, -515.969), Angle(0, 16.062, 0) ),
	Create_Beer( Vector(3204.197, 1030.087, -515.969), Angle(0, 347.638, 0) ),
	Create_Beer( Vector(3246.198, 988.891, -515.969), Angle(0, 353.699, 0) ),
	Create_Bread( Vector(3179.973, 983.732, -515.969), Angle(0, 0.387, 0) ),
	Create_Bread( Vector(3229.183, 987.667, -512.937), Angle(0, 352.444, 0) ),
	Create_Bread( Vector(3205.719, 962.167, -512.937), Angle(0, 98.407, 0) ),
	Create_Bread( Vector(3206.984, 1009.336, -515.969), Angle(0, 78.134, 0) )}
	
	local AMMO_ENTS = {
	Create_PistolAmmo( Vector(2996.859, 2142.603, -763.969), Angle(0, 328.092, 0) ),
	Create_PistolAmmo( Vector(3006.468, 2157.127, -760.937), Angle(0, 315.134, 0) ),
	Create_PistolAmmo( Vector(3025.861, 2167.646, -763.969), Angle(0, 298.623, 0) ),
	Create_KingSlayerAmmo( Vector(4696.473, 730.19, -751.969), Angle(0, 293.716, 0) ),
	Create_KingSlayerAmmo( Vector(4843.109, 734.339, -751.969), Angle(0, 254.424, 0) ),
	Create_ShotgunAmmo( Vector(5511.382, 854.933, -751.969), Angle(0, 316.705, 0) ),
	Create_ShotgunAmmo( Vector(5511.319, 932.996, -751.969), Angle(0, 51.384, 0) ),
	Create_RifleAmmo( Vector(6304.802, 940.575, -759.969), Angle(0, 142.502, 0) ),
	Create_RifleAmmo( Vector(6306.06, 1036.648, -759.969), Angle(0, 201.858, 0) ),
	Create_CrossbowAmmo( Vector(3987.172, 1855.132, -751.969), Angle(0, 359.335, 0) ),
	Create_CrossbowAmmo( Vector(3816.683, 1856.873, -751.969), Angle(0, 180.747, 0) ),
	Create_ShotgunAmmo( Vector(3749.069, -443.211, -1039.969), Angle(0, 108.952, 0) ),
	Create_ShotgunAmmo( Vector(3646.551, -446.935, -1039.969), Angle(0, 94.949, 0) ),
	Create_SMGAmmo( Vector(2490.156, 806.992, -767.969), Angle(0, 43.333, 0) ),
	Create_SMGAmmo( Vector(2486.407, 916.978, -767.969), Angle(0, 316.806, 0) ),
	Create_GrenadeAmmo( Vector(2585.161, 1623.802, -767.969), Angle(0, 225.264, 0) ),
	Create_GrenadeAmmo( Vector(2590.106, 1565.319, -767.969), Angle(0, 185.658, 0) ),
	Create_GrenadeAmmo( Vector(2590.785, 1498.607, -767.969), Angle(0, 166.326, 0) ),
	Create_RevolverAmmo( Vector(3840.124, 2636.464, -767.969), Angle(0, 292.562, 0) ),
	Create_RevolverAmmo( Vector(3904.353, 2629.595, -767.969), Angle(0, 242.193, 0) )}
	
	local AMMO_S_ENTS = {
	Create_FoolsOathAmmo( Vector(3564.315, 1388.437, -399.969), Angle(0, 0.358, 0) ),
	Create_M6GAmmo( Vector(4875.86, -87.662, -556.463), Angle(-1.254, -179.039, 0) ),
	Create_FrontlinerAmmo( Vector(4412.73, 691.064, -511.969), Angle(0, 179.705, 0) ),
	Create_HarbingerAmmo( Vector(4763.204, 21.561, -538.969), Angle(0, 179.283, 0) )}
	
	for _,v in pairs (HEALING) do
		SetAsRespawningItem(v,30)
	end
	
	for _,v in pairs (AMMO_ENTS) do
		SetAsRespawningItem(v,60)
	end
	
	for _,v in pairs (AMMO_S_ENTS) do
		SetAsRespawningItem(v,120)
	end

	CreateTeleporter( "START_1", Vector(-1461.839,3686.379,138.114), "START_2", Vector(2731.987,1291.845,-1071.969) )
	
	--[[ The trigger to start the mode ]]
	CreateTrigger( Vector(2670.02,1292.099,-1071.969), Vector(-184.269,-184.269,0), Vector(184.269,184.269,521.728), START_SIN_HUNTER )
	
	for _,v in pairs (ents.GetAll()) do
	
		if IsValid(v) then
		
			if v:IsProp() then

				if IsValid(v:GetPhysicsObject()) then
					v:GetPhysicsObject():EnableMotion(false)
					v:GetPhysicsObject():AddGameFlag(FVPHYSICS_CONSTRAINT_STATIC)
				end
				
			end
			
		end
		
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

SINHUNTER_SPAWNS_FRONT = {
Vector(3002.115,927.563,-767.969),
Vector(2888.06,914.663,-769.969),
Vector(2774.85,915.736,-774.91),
Vector(2616.674,930.158,-769.982),
Vector(2499.814,899.408,-767.969),
Vector(2553.575,785.056,-769.969),
Vector(2632.935,768.539,-775.138),
Vector(2740.243,764.066,-774.086),
Vector(2841.303,760.931,-766.06),
Vector(2979.157,746.717,-767.969),
Vector(2982.916,646.839,-759.969),
Vector(2979.18,552.521,-759.969),
Vector(2977.295,458.977,-759.969),
Vector(3075.372,440.295,-759.969),
Vector(3188.357,445.323,-759.969),
Vector(3324.583,450.723,-759.969),
Vector(3438.404,450.958,-767.12),
Vector(3536.965,451.801,-759.969),
Vector(3657.486,452.056,-759.969),
Vector(3782.546,449.762,-759.969),
Vector(3909.576,448.022,-759.969),
Vector(4044.233,441.144,-759.969),
Vector(4174.259,425.169,-759.969),
Vector(4291.082,411.871,-761.969),
Vector(4400.51,404.481,-759.969),
Vector(4534.098,400.094,-767.969),
Vector(4668.712,411.006,-759.969),
Vector(4742.834,508.638,-767.969),
Vector(4839.345,629.574,-767.969),
Vector(4935.704,585.827,-759.969),
Vector(4901.538,446.262,-759.969),
Vector(4919.055,326.117,-759.969),
Vector(5028.86,303.73,-767.969),
Vector(5153.553,302.656,-767.969),
Vector(5291.901,301.849,-767.969),
Vector(5417.671,308.341,-759.969),
Vector(5528.167,364.232,-759.969),
Vector(5612.781,490.971,-759.969),
Vector(5619.502,630.666,-759.969),
Vector(5582.493,761.855,-759.969),
Vector(5603.464,895.42,-759.969),
Vector(5709.78,948.231,-767.969),
Vector(5828.107,949.621,-767.969),
Vector(5962.343,930.502,-769.095),
Vector(6038.088,902.599,-764.843),
Vector(5843.628,856.959,-767.956),
Vector(5791.71,754.408,-770.851),
Vector(5858.592,659.788,-766.789),
Vector(5910.875,554.473,-768.884),
Vector(5905.528,436.543,-768.962),
Vector(5943.251,307.223,-767.969),
Vector(6048.226,255.29,-767.969),
Vector(6208.542,262.052,-767.969),
Vector(5891.449,909.926,-766.923),
Vector(6049.221,912.408,-764.002),
Vector(4496.866,85.333,-759.454),
Vector(4690.414,83.906,-728.248),
Vector(5011.277,194.781,-749.93),
Vector(4063.395,452.117,-759.969),
Vector(3903.375,341.065,-767.969),
Vector(3907.432,204.174,-767.969),
Vector(3932.071,44.671,-767.969),
Vector(3993.641,-73.282,-767.969),
Vector(4083.577,-200.632,-759.969),
Vector(3932.6,-244.042,-759.969),
Vector(3735.812,-223.796,-815.169),
Vector(3605.466,-220.339,-847.969),
Vector(3585.289,-118.571,-847.969),
Vector(3726.63,-106.918,-847.969),
Vector(3605.364,82.464,-1026.393),
Vector(3388.43,-1.311,-1023.959),
Vector(3417.813,-156.867,-1023.301),
Vector(3316.507,-215.865,-1019.089),
Vector(2910.697,-268.525,-1023.969),
Vector(2895.434,-426.601,-1023.969),
Vector(3026.097,-534.348,-1028.969),
Vector(3201.959,-547.255,-1029.054),
Vector(3031.882,-369.18,-1020.361),
Vector(3146.217,-375.112,-1021.441),
Vector(3330.416,-379.602,-1020.953),
Vector(3471.425,-379.283,-1023.187),
Vector(3636.42,-380.779,-1039.969),
Vector(3707.629,-271.887,-1039.969),
Vector(3774.468,-97.669,-1039.969),
Vector(3699.741,58.483,-1029.554),
Vector(3484.774,107.39,-895.969),
Vector(3511.36,194.969,-895.969),
Vector(3195.993,182.475,-767.969),
Vector(3051.165,266.673,-767.969),
Vector(3304.983,557.222,-766.969),
Vector(2863.295,265.612,-1023.969),
Vector(2841.591,387.558,-1023.969),
Vector(2843.526,512.292,-1023.969),
Vector(2822.137,673.124,-1023.969),
Vector(2834.585,830.017,-1023.969),
Vector(2914.287,923.635,-1021.969),
Vector(2976.554,1004.793,-1023.969),
Vector(2856.721,991.646,-1036.077),
Vector(2736.853,979.532,-1071.969),
Vector(2648.24,925.454,-1071.969),
Vector(2631.562,789.643,-1071.969),
Vector(2701.516,626.496,-1071.969),
Vector(2733.215,469.868,-1071.969),
Vector(2711.589,320.064,-1071.969),
Vector(2720.226,170.422,-1071.969),
Vector(2733.642,39.096,-1071.969),
Vector(2723.343,-100.875,-1071.969),
Vector(2714.077,-231.022,-1071.969),
Vector(2737.218,-366.324,-1071.969),
Vector(2741.227,-508.319,-1071.969),
Vector(2728.229,-642.869,-1071.969),
Vector(2601.651,-647.768,-1048.606),
Vector(2506.552,-544.194,-1023.969),
Vector(2493.12,-453.196,-1023.969),
Vector(2497.277,-331.66,-1023.969),
Vector(2499.946,-183.911,-1023.969),
Vector(2502.138,-48.368,-1023.969),
Vector(2504.136,79.77,-1023.969),
Vector(2499.823,222.66,-1023.969),
Vector(2493.155,365.469,-1023.969),
Vector(2507.412,529.969,-1021.544),
Vector(2520.044,663.602,-1023.969),
Vector(2520.916,789.456,-1023.969),
Vector(2521.262,905.165,-1023.969),
Vector(2590.616,482.206,-1029.145),
Vector(2875.159,-247.224,-1023.969),
Vector(2853.945,-459.849,-1023.969),
Vector(3030.178,-664.667,-767.969),
Vector(2958.596,-658.622,-767.969),
Vector(2876.665,-655.061,-767.969),
Vector(2790.631,-653.905,-767.969),
Vector(2708.324,-653.876,-767.969),
Vector(2628.05,-654.165,-767.969),
Vector(2540.324,-654.584,-767.969),
Vector(2458.521,-466.206,-751.969),
Vector(2458.937,-323.476,-751.969),
Vector(2457.32,-181.687,-751.969),
Vector(2459.493,-46.893,-751.969),
Vector(2461.42,98.741,-751.969),
Vector(2462.04,232.364,-751.969),
Vector(2460.839,353.218,-751.969),
Vector(2458.975,476.388,-751.969),
Vector(2458.711,602.22,-751.969),
Vector(2458.977,732.454,-751.969),
Vector(2458.391,846.43,-751.969),
Vector(3054.001,-325.524,-767.969),
Vector(3135.617,-320.452,-767.969),
Vector(3146.417,-406.099,-767.969),
Vector(3086.493,-435.966,-767.969),
Vector(3022.259,-426.036,-767.969),
Vector(3019.667,-351.624,-767.969),
Vector(4225.01,679.277,-765.969),
Vector(4098.575,681.612,-763.969),
Vector(3953.309,849.746,-766.969),
Vector(3858.557,855.856,-766.969),
Vector(3747.064,845.203,-766.969),
Vector(3633.945,849.293,-766.969),
Vector(3534.653,849.147,-766.969),
Vector(3443.758,844.55,-766.969),
Vector(3318.475,868.502,-766.969),
Vector(3235.6,936.954,-763.969),
Vector(3173.325,996.982,-763.969),
Vector(3318.146,978.003,-763.969),
Vector(3193.01,879.074,-766.969),
Vector(3171.943,862.22,-895.969),
Vector(3292.621,931.821,-895.969),
Vector(3258.501,997.721,-895.969),
Vector(3670.383,767.199,-763.969),
Vector(3772.733,978.616,-763.969),
Vector(3855.467,992.403,-763.969),
Vector(4061.578,982.238,-763.969),
Vector(4191.275,699.815,-765.969),
Vector(4306.099,740.72,-765.969),
Vector(4134.954,827.869,-766.969),
Vector(4145.656,952.657,-766.969),
Vector(4222.118,987.479,-766.969),
Vector(4306.222,977.543,-763.969)}

SINHUNTER_SPAWNS_BACK = {
Vector(4445.88,1051.266,-640.969),
Vector(4308.045,1050.247,-590.678),
Vector(4151.204,1050.638,-511.969),
Vector(4012.664,1048.13,-515.969),
Vector(4026.241,1143.767,-515.969),
Vector(4020.019,1240.679,-515.969),
Vector(3955.929,1273.828,-515.969),
Vector(3864.017,1231.667,-515.969),
Vector(3838.897,1135.924,-515.969),
Vector(3860.759,1068.8,-515.969),
Vector(3774.472,1070.256,-515.969),
Vector(3674.365,1067.395,-515.969),
Vector(3563.826,1067.613,-515.969),
Vector(3481.078,1092.966,-515.969),
Vector(3671.613,1122.969,-515.969),
Vector(3827.138,1253.704,-515.969),
Vector(3745.04,1274.187,-515.969),
Vector(3653.532,1268.899,-515.969),
Vector(3571.673,1247.4,-515.969),
Vector(3475.726,1235.568,-515.969),
Vector(3396.049,1178.296,-515.969),
Vector(3379.266,1110.733,-515.969),
Vector(3925.757,1393.93,-519.969),
Vector(3833.252,1387.432,-519.969),
Vector(3709.288,1380.123,-519.969),
Vector(3541.428,1382.886,-519.969),
Vector(3338.927,1386.998,-519.969),
Vector(3160.943,1388.759,-519.969),
Vector(3003.087,1368.183,-519.969),
Vector(2993.595,1233.421,-519.969),
Vector(3237.572,1276.065,-515.969),
Vector(3231.146,1170.411,-515.969),
Vector(3131.125,1079.38,-515.969),
Vector(3180.331,1077.76,-515.969),
Vector(3280.702,1073.98,-515.969),
Vector(3142.709,1221.335,-515.969),
Vector(3163.514,1265.089,-515.969),
Vector(2866.678,1181.958,-767.969),
Vector(2745.432,1194.082,-767.969),
Vector(2591.096,1215.708,-767.969),
Vector(2483.493,1298.899,-767.969),
Vector(2470.182,1448.146,-767.969),
Vector(2533.454,1554.295,-767.969),
Vector(2508.263,1640.708,-767.969),
Vector(2524.859,1780.11,-767.969),
Vector(2663.975,1800.958,-767.969),
Vector(2812.535,1721.522,-767.969),
Vector(2838.863,1577.994,-767.969),
Vector(2846.137,1431.927,-767.969),
Vector(2986.604,1383.025,-759.969),
Vector(2990.596,1472.661,-775.969),
Vector(2973.583,1590.277,-775.969),
Vector(3022.14,1738.405,-775.969),
Vector(3142.699,1832.844,-767.969),
Vector(3222.286,1987.442,-775.969),
Vector(3134.235,2103.411,-767.969),
Vector(3168.996,2245.035,-767.969),
Vector(3299.576,2321.493,-767.969),
Vector(3406.001,2438.026,-767.969),
Vector(3507.544,2559.166,-767.969),
Vector(3650.439,2587.545,-767.969),
Vector(3786.269,2556.782,-767.969),
Vector(3921.583,2572.855,-767.969),
Vector(4097.478,2602.842,-767.969),
Vector(4232.482,2553.986,-767.969),
Vector(4370.015,2491.247,-767.969),
Vector(4500.809,2392.468,-767.969),
Vector(4536.205,2236.13,-767.969),
Vector(4457.796,2092.615,-775.969),
Vector(4301.546,2066.88,-775.969),
Vector(4154.879,2095.844,-767.969),
Vector(4013.395,2117.788,-767.969),
Vector(3878.054,2112.624,-767.969),
Vector(3708.542,2074.673,-767.969),
Vector(3635.55,1985.881,-767.969),
Vector(3737.455,1844.276,-751.969),
Vector(3865.153,1792.932,-751.969),
Vector(3897.57,1622.954,-759.969),
Vector(3927.315,1500.347,-775.969),
Vector(4098.303,1514.165,-775.969),
Vector(4240.097,1602.248,-767.969),
Vector(4225.221,1755.791,-767.969),
Vector(4170.746,1912.011,-767.969),
Vector(4271.688,2043.067,-775.969),
Vector(4305.08,2237.814,-775.969),
Vector(4450.406,2339.256,-767.969),
Vector(4609.896,2271.751,-767.969),
Vector(4728.91,2190.942,-767.969),
Vector(4843.794,2064.831,-767.969),
Vector(4896.36,1889.363,-767.969),
Vector(4862.609,1721.053,-767.969),
Vector(4761.379,1553.327,-767.969),
Vector(4623.023,1488.825,-767.969),
Vector(4681.464,1355.031,-759.969),
Vector(4739.567,1191.629,-767.969),
Vector(4863.593,1176.882,-767.969),
Vector(4907.252,1319.199,-759.969),
Vector(4936.563,1478.544,-759.969),
Vector(4985.042,1598.798,-767.969),
Vector(5036.417,1718.635,-767.969),
Vector(5067.244,1841.935,-767.969),
Vector(5015.933,1990.473,-767.969),
Vector(4896.587,2166.708,-767.969),
Vector(4763.348,2275.16,-767.969),
Vector(4608.547,2313.473,-767.969),
Vector(4453.105,2376.468,-767.969),
Vector(4309.333,2510.103,-767.969),
Vector(4161.016,2585.61,-767.969),
Vector(3943.26,2587.741,-767.969),
Vector(3160.607,2117.191,-767.969),
Vector(3041.256,2093.867,-767.969),
Vector(2920.807,2009.817,-767.969),
Vector(2904.046,1850.771,-767.969),
Vector(2993.393,1677.639,-775.969),
Vector(3102.437,1563.272,-767.969),
Vector(3500.333,1270.478,-758.65),
Vector(3453.051,1162.055,-763.969),
Vector(3503.127,1082.832,-763.969),
Vector(3608.954,1076.22,-763.969),
Vector(3688.546,1177.057,-763.969),
Vector(3725.482,1250.868,-763.969),
Vector(3765.641,1237.123,-763.969),
Vector(3812.994,1141.909,-763.969),
Vector(3911.742,1088.871,-763.969),
Vector(4012.907,1154.358,-763.969),
Vector(3996.357,1258.008,-763.969),
Vector(3500.696,1134.986,-763.969),
Vector(3306.941,1205.338,-763.969),
Vector(3221.404,1125.217,-763.969),
Vector(3125.321,1118.854,-763.969),
Vector(3107.971,1230.315,-763.969),
Vector(5201.743,1589.849,-767.969),
Vector(5262.473,1498.096,-767.969),
Vector(5335.212,1480.227,-759.969),
Vector(5370.777,1595.612,-767.969),
Vector(5456.608,1619.559,-767.969),
Vector(5536.742,1545.685,-767.969),
Vector(5583.368,1418.652,-759.969),
Vector(5680.294,1385.16,-767.969),
Vector(5784.383,1455.297,-770.652),
Vector(5871.473,1528.539,-770.89),
Vector(5966.713,1553.557,-768.14),
Vector(5965.838,1459.677,-765.381),
Vector(5912.873,1360.404,-768.206),
Vector(5773.229,1294.433,-767.529),
Vector(5649.779,1160.331,-758.962),
Vector(5603.278,1049.044,-759.969),
Vector(5754.731,1050.203,-767.969),
Vector(5851.994,1068.109,-769.376),
Vector(6023.791,1574.499,-757.534),
Vector(4394.009,1334.638,-757.969),
Vector(4394.697,1190.559,-765.969),
Vector(4369.189,1113.296,-765.969),
Vector(4234.755,1130.049,-765.969),
Vector(4146.431,1070.384,-766.969),
Vector(4284.036,1048.285,-766.969),
Vector(2821.438,1225.02,-1023.969),
Vector(2816.154,1114.073,-1023.969),
Vector(2676.606,1098.568,-1071.969),
Vector(2592.162,1142.118,-1071.969),
Vector(2580.593,1254.895,-1071.969),
Vector(2650.346,1334.911,-1071.969),
Vector(2782.524,1327.959,-1071.969)}