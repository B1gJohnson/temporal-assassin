local meta = FindMetaTable( "Entity" ) if (!meta) then return end local TriggerEntities = { trigger_autosave = true, trigger_changelevel = true, trigger_finale = true, trigger_gravity = true, trigger_hurt = true, trigger_impact = true, trigger_look = true, trigger_multiple = true, trigger_once = true, trigger_physics_trap = true, trigger_playermovement = true, trigger_proximity = true, trigger_push = true, trigger_remove = true, trigger_rpgfire = true, trigger_soundscape = true, trigger_serverragdoll = true, trigger_soundscape = true, trigger_teleport = true, trigger_transition = true, trigger_vphysics_motion = true, trigger_waterydeath = true, trigger_weapon_dissolve = true, trigger_weapon_strip = true, trigger_wind = true, func_occluder = true, func_precipitation = true, func_smokevolume = true, func_vehicleclip = true, func_areaportal = true, func_areaportalwindow = true, func_dustcloud = true, point_hurt = true, point_clientcommand = true, point_servercommand = true, ambient_generic = true, env_generic = true, env_steam = true, env_shake = true, _firesmoke = true, npc_maker = true, npc_template_maker = true, env_smokestack = true, spotlight_end = true, assault_assaultpoint = true, color_correction_volume = true, weapon_crowbar = true, weapon_stunstick = true, weapon_pistol = true, weapon_357 = true, weapon_smg1 = true, weapon_ar2 = true, weapon_shotgun = true, weapon_crossbow = true, weapon_rpg = true, weapon_bugbait = true, weapon_slam = true } function meta:IsTrigger() if TriggerEntities[self:GetClass()] then return true end return false end
 
--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]