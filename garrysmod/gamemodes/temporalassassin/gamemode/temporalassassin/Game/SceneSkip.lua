 GLOB_SCENE_SKIP_AVAILABLE = false if SERVER then util.AddNetworkString("Skip_Available") util.AddNetworkString("Skip_Activated") function SetSceneSkipAvailable( bool ) GLOB_SCENE_SKIP_AVAILABLE = bool net.Start("Skip_Available") net.WriteBool(bool) net.Broadcast() if not bool then SkipScene_Cancel() end end function SkipScene_Start() if TEMPORAL_SLOW_ON then return end if not GLOB_SCENE_SKIP_AVAILABLE then return end TEMPORAL_SLOW_SPEED = 0.5 TEMPORAL_SLOW_TARGET = 6 TEMPORAL_SKIPPING_SCENE = true net.Start("Skip_Activated") net.WriteBool(true) net.Broadcast() end function SkipScene_Cancel() if TEMPORAL_SLOW_ON then return end TEMPORAL_SLOW_SPEED = 0.5 TEMPORAL_SLOW_TARGET = 1 TEMPORAL_SKIPPING_SCENE = false net.Start("Skip_Activated") net.WriteBool(false) net.Broadcast() end function SkipScene_KeyPress( ply, key ) local CT = CurTime() if key == IN_USE then ply.SkipScene_Activate = CT + 1 end end hook.Add("KeyPress","SkipScene_KeyPressHook",SkipScene_KeyPress) function SkipScene_KeyRelease( ply, key ) if key == IN_USE then ply.SkipScene_Activate = false SkipScene_Cancel() end end hook.Add("KeyRelease","SkipScene_KeyReleaseHook",SkipScene_KeyRelease) function SkipScene_Think( ply ) local CT = CurTime() if ply.SkipScene_Activate then if CurTime() >= ply.SkipScene_Activate then ply.SkipScene_Activate = false SkipScene_Start() end end end hook.Add("PlayerPostThink","SkipScene_ThinkHook",SkipScene_Think) return end function Skip_Available_LIB( len ) local B = net.ReadBool() GLOB_SCENE_SKIP_AVAILABLE = B end net.Receive("Skip_Available",Skip_Available_LIB) function Skip_Activated_LIB( len ) local B = net.ReadBool() TEMPORAL_SKIPPING_SCENE = B end net.Receive("Skip_Activated",Skip_Activated_LIB) function SkipScene_HUD() if GLOB_SCENE_SKIP_AVAILABLE then local SINE = math.sin(UnPredictedCurTime()*10)*50 local SINE2 = math.sin(UnPredictedCurTime()*4)*3 DrawShadowText( tostring("- Hold USE to skip scene -"), "HUD_LevelDetails", ScrW()*0.5, ScrH()*0.09 + GUI_Y(SINE2), Color(120,220,120,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "HUD_LevelDetails2", Color(250,110,110,105+SINE) ) end end hook.Add("HUDPaint","SkipScene_HUDHook",SkipScene_HUD)

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]