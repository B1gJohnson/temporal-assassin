 local RPlayer = debug.getregistry().Player local REntity = debug.getregistry().Entity PrecacheParticleSystem("HeatGauge_LoopFX") if SERVER then util.AddNetworkString("LoadTemporalHeat") file.CreateDir("TemporalAssassin/Resources/") function RPlayer:SaveTemporalHeat() local ID = self:UniqueID() local S_NAME = tostring(ID.."_Heat") file.Write( "TemporalAssassin/Resources/"..S_NAME..".txt", tostring(TAS:GetTemporalHeat(self)) ) end function LoadTemporalHeat_LIB( len, pl ) local ID = pl:UniqueID() local S_NAME = tostring(ID.."_Heat") local FILE = tostring("TemporalAssassin/Resources/"..S_NAME..".txt") if file.Exists( FILE, "DATA" ) then TAS:SetTemporalHeat( pl, tonumber( file.Read( FILE, "DATA" ) ) ) else TAS:SetTemporalHeat( pl, 0 ) end TAS:TemporalHeatRegenCall( pl ) TAS:TemporalHeatSend( pl ) end net.Receive("LoadTemporalHeat",LoadTemporalHeat_LIB) end function TAS:TemporalHeatSend( ply ) TAS:TemporalHeatCallback(ply) if CLIENT then return end ply:SetSharedFloat( "HeatGauge_Amount", TAS:GetTemporalHeat(ply) ) end function TAS:TemporalHeatRegenCall( ply ) TAS:TemporalHeatCallback( ply ) end function TAS:TemporalHeatCallback( ply ) if not ply.HeatGauge_Amount then ply.HeatGauge_Amount = 0 end end function TAS:GetMaxTemporalHeat( ply ) TAS:TemporalHeatCallback(ply) return 100 end function TAS:GetTemporalHeat( ply ) TAS:TemporalHeatCallback(ply) return ply.HeatGauge_Amount end function TAS:SetTemporalHeat( ply, amount ) if CLIENT then return end TAS:TemporalHeatCallback(ply) ply.HeatGauge_Amount = amount TAS:TemporalHeatSend(ply) end function TAS:GiveTemporalHeat( ply, amount ) if CLIENT then return end if ply.HeatGauge_Activated then amount = amount*0.15 end amount = amount*0.5 TAS:TemporalHeatCallback(ply) ply.HeatGauge_Amount = math.Clamp( ply.HeatGauge_Amount + amount, 0, TAS:GetMaxTemporalHeat(ply) ) TAS:TemporalHeatSend(ply) end function TAS:TakeTemporalHeat( ply, amount ) if CLIENT then return end if ply.WHITECRYSTAL_ACTIVE and CurTime() <= ply.WHITECRYSTAL_ACTIVE then amount = 0 end amount = amount*0.5 TAS:TemporalHeatCallback(ply) ply.HeatGauge_Amount = math.Clamp( ply.HeatGauge_Amount - amount, 0, TAS:GetMaxTemporalHeat(ply) ) TAS:TemporalHeatSend(ply) TAS:TemporalHeatRegenCall(ply) end if CLIENT then return end function TAS_TemporalHeatRegenThink(ply) local CT = CurTime() if not ply.HeatGauge_Generation then ply.HeatGauge_Generation = CT end if ply.HeatGauge_Amount and ply.HeatGauge_Amount >= 100 and not ply.BurstGauge_Activated and TAS:GetSuit(ply) == 15 then if CT >= ply.HeatGauge_Generation then ply.HeatGauge_Generation = CT + 0.5 TAS:GiveTemporalBurst(ply,0.1) end end end hook.Add("PlayerPostThink","TAS_TemporalHeatRegenThinkHook",TAS_TemporalHeatRegenThink) 

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]