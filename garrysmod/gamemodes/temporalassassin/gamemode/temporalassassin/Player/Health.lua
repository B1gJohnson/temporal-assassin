 local RPlayer = debug.getregistry().Player local REntity = debug.getregistry().Entity function RPlayer:GiveHealth( num, custommax ) local HP = self:Health() local MAXHP = self:GetMaxHealth() if not custommax then custommax = MAXHP end if TAS:GetSuit(self) == 23 and custommax > MAXHP*2 then custommax = MAXHP*2 end if custommax then MAXHP = custommax end if TAS:GetSuit(self) == 2 and math.Rand(0,100) <= 5 then if math.Rand(1,100) <= 50 then num = num*0.5 else num = num*2 end end if HP < MAXHP then self:SetHealth( math.Clamp( HP + num, 0, MAXHP ) ) end end function RPlayer:TakeHealth( num, dontkill ) local MAX_LOW = 0 if dontkill then MAX_LOW = 1 end local HP = self:Health() self:SetHealth( math.Clamp( HP - num, MAX_LOW, 50000 ) ) if self:Health() <= 0 and self:Alive() then self:Kill() end end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]