 GLOB_FOOTSOUND_NULL = false function AddFootstepTable( table_name, soundbase, numbers ) _G[table_name] = {} for loop = 1,numbers do local FILE = tostring(soundbase..loop..".wav") GLOB_FOOTSOUND_NULL = Sound(FILE) table.insert( _G[table_name], Sound(FILE) ) end end AddFootstepTable( "Footsteps_Default", "TemporalAssassin/Player/Footsteps/Footstep_Concrete", 8 ) AddFootstepTable( "Footsteps_Concrete", "TemporalAssassin/Player/Footsteps/Footstep_Concrete", 8 ) AddFootstepTable( "Footsteps_Carpet", "TemporalAssassin/Player/Footsteps/Footstep_Carpet", 8 ) AddFootstepTable( "Footsteps_Dirt", "TemporalAssassin/Player/Footsteps/Footstep_Dirt", 6 ) AddFootstepTable( "Footsteps_Grass", "TemporalAssassin/Player/Footsteps/Footstep_Dirt", 6 ) AddFootstepTable( "Footsteps_Glass", "TemporalAssassin/Player/Footsteps/Footstep_Glass", 8 ) AddFootstepTable( "Footsteps_Gravel", "TemporalAssassin/Player/Footsteps/Footstep_Gravel", 6 ) AddFootstepTable( "Footsteps_Metal", "TemporalAssassin/Player/Footsteps/Footstep_NewMetal", 7 ) AddFootstepTable( "Footsteps_MetalGrate", "TemporalAssassin/Player/Footsteps/Footstep_NewChain", 9 ) AddFootstepTable( "Footsteps_MetalVent", "TemporalAssassin/Player/Footsteps/Footstep_NewVent", 7 ) AddFootstepTable( "Footsteps_Snow", "TemporalAssassin/Player/Footsteps/Footstep_Snow", 8 ) AddFootstepTable( "Footsteps_Stone", "TemporalAssassin/Player/Footsteps/Footstep_ConcreteDirty", 8 ) AddFootstepTable( "Footsteps_Water", "TemporalAssassin/Player/Footsteps/Footstep_NewWater", 10 ) AddFootstepTable( "Footsteps_Puddle", "TemporalAssassin/Player/Footsteps/Footstep_NewWater", 10 ) AddFootstepTable( "Footsteps_Wood", "TemporalAssassin/Player/Footsteps/Footstep_NewWood", 9 ) AddFootstepTable( "Footsteps_Sand", "TemporalAssassin/Player/Footsteps/Footstep_Sand", 8 ) AddFootstepTable( "Footsteps_Mud", "TemporalAssassin/Player/Footsteps/Footstep_Puddle", 6 ) AddFootstepTable( "Footsteps_Tile", "TemporalAssassin/Player/Footsteps/Footstep_Tile", 6 ) AddFootstepTable( "Footsteps_Rubber", "TemporalAssassin/Player/Footsteps/Footstep_Marble", 9 ) AddFootstepTable( "Footsteps_Ladder", "TemporalAssassin/Player/Footsteps/Footstep_Ladder", 6 )

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]