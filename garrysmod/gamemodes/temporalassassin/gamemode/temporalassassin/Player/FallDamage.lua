 local RPlayer = debug.getregistry().Player function RPlayer:TakeFallDamage( amount ) local CT = CurTime() local GROUND = self:GetGroundEntity() local Dam = amount local WORLD = Entity(0) local DMG = DamageInfo() DMG:SetDamageType(DMG_FALL) DMG:SetInflictor( WORLD ) DMG:SetAttacker( WORLD ) DMG:SetDamage( Dam ) DMG:SetDamageForce( Vector(0,0,-50000) ) self:TakeDamageInfo( DMG ) end function NULL_FallDamage( ply, speed ) return 0 end hook.Add("GetFallDamage","NULL_FallDamageHook",NULL_FallDamage) function RPlayer:FallImpactDamage( VEL ) if self:InSafeZone() then return end local TR = {} TR.start = self:GetPos() + Vector(0,0,1) TR.endpos = TR.start - Vector(0,0,40) TR.filter = self TR.mask = MASK_PLAYERSOLID TR.mins = Vector(-16,-16,-0.5) TR.maxs = Vector(16,16,0.5) local Trace = util.TraceHull(TR) if Trace.Hit and not Trace.HitWorld and IsValid(Trace.Entity) then local DMG = DamageInfo() DMG:SetDamage( (VEL*-0.15) ) DMG:SetAttacker(self) DMG:SetInflictor(self) DMG:SetDamageType(DMG_FALL) DMG:SetDamagePosition(self:GetPos()) DMG:SetDamageForce( Vector(0,0,-50000) ) Trace.Entity:TakeDamageInfo(DMG) end end function DAMAGE_FallDamage( ply ) local CT = CurTime() if ply:KeyDown(4) then ExtraLength = 32 else ExtraLength = 0 end local GENT = ply:GetGroundEntity() local DMG_MUL = 1 local WORLD = Entity(0) local VELOCITY = ply:GetVelocity().z local speed = VELOCITY*-1 if VELOCITY <= -750 then ply:FallImpactDamage( VELOCITY ) end end hook.Add("OnPlayerHitGround","DAMAGE_FallDamageHook",DAMAGE_FallDamage)

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]