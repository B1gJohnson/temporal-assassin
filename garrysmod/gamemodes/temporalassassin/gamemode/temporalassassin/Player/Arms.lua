 local RPlayer = debug.getregistry().Player local REntity = debug.getregistry().Entity local ArmModel = Model("models/temporalassassin/arms/arms_1.mdl") function TA_GetArms() return ArmModel end function TA_CreateArms( hide1, hide2 ) local PL = LocalPlayer() if not IsValid(TA_Arms) then local VM = (TA_VIEWMODEL1 or PL:GetViewModel()) TA_Arms = ClientsideModel( ArmModel, RENDERGROUP_VIEWMODEL ) TA_Arms:SetPos( VM:GetPos() or Vector(0,0,0) ) TA_Arms:SetAngles( VM:GetAngles() or Angle(0,0,0) ) TA_Arms:SetRenderMode( RENDERMODE_TRANSALPHA ) TA_Arms:AddEffects( EF_BONEMERGE ) TA_Arms:AddEffects( EF_BONEMERGE_FASTCULL ) TA_Arms:SetSkin(0) TAS:SetArmMaterials() TA_Arms:SetNoDraw(true) TA_Arms:SetParent( VM ) TA_Arms:Spawn() TA_Arms:Activate() end end function TA_RemoveArms() if IsValid(TA_Arms) then TA_Arms:Remove() TA_Arms = nil end end function CLArmsThink() local PL = LocalPlayer() if not IsValid(PL) then return end if not PL:Alive() then TA_RemoveArms() return end local WEP = PL:GetActiveWeapon() if not IsValid(WEP) then TA_RemoveArms() return end if TA_Arms and IsValid(TA_Arms) then if WEP.TA_Arms_BG then TA_Arms:SetBodygroup(1,WEP.TA_Arms_BG) else TA_Arms:SetBodygroup(1,0) end end if not TA_LastWeapon then TA_LastWeapon = WEP:GetClass() end if TA_LastWeapon and TA_LastWeapon != WEP:GetClass() then TA_RemoveArms() TA_LastWeapon = WEP:GetClass() end if WEP.TA_Arms and not TA_Arms then TA_CreateArms( 1, 1 ) end local VM = (TA_VIEWMODEL1 or PL:GetViewModel()) local Remove1 = ( false ) local Remove11 = ( false ) local POS1 = VM:GetPos().x local POS2 = (IsValid(TA_Arms) and TA_Arms:GetPos().x) local Remove2 = ( IsValid(TA_Arms) and ( (POS1 >= POS2+400) or (POS1 <= POS2-400) ) ) if ( Remove1 or Remove2 ) and (TA_Arms) then TA_RemoveArms() end if TA_Arms then if IsValid(TA_Arms) and TA_Arms:GetParent() != VM then TA_Arms:SetParent(VM) end end end timer.Create("CLArmsThink_Timer",0.2,0,CLArmsThink)

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]