 if not GLOB_NPC_VOICEDATA then GLOB_NPC_VOICEDATA = {} end local FOLDER = "TemporalAssassin/NPC/Phantom/" GLOB_NPC_VOICEDATA["Phantom"] = {} GLOB_NPC_VOICEDATA["Phantom"]["DeathFunction"] = function( npc, att, inf ) npc:PlayNPCVoice( "Die", 1, 1, {160,math.random(98,102),1,CHAN_ITEM}, true ) end GLOB_NPC_VOICEDATA["Phantom"]["Die"] = { Sound(FOLDER.."Phantom_Death.wav")}

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]