 local RPlayer = debug.getregistry().Player local REntity = debug.getregistry().Entity function NPCX_LagNPCCreation( ent ) timer.Simple(0.1,function() if IsValid(ent) and (ent:IsNPC() or ent:IsPlayer()) then ent.Max_Health = (ent:GetInternalVariable("max_health") or ent:Health()) end end) end hook.Add("OnEntityCreated","NPCX_LagNPCCreationHook",NPCX_LagNPCCreation) function NPCX_LagInitEntity() for _,v in pairs (ents.GetAll()) do if IsValid(v) and (v:IsNPC() or v:IsPlayer()) then v.Max_Health = 50 timer.Simple(0.1,function() if IsValid(v) then v.Max_Health = (v:GetInternalVariable("max_health") or v:Health()) end end) end end end hook.Add("InitPostEntity","NPCX_LagInitEntityHook",NPCX_LagInitEntity) function REntity:SetAccuracy( x ) if not self.GetActiveWeapon then return end if not IsValid(self:GetActiveWeapon()) then return end if not x then x = 2 end if x < 0 then x = 0 end if x > 5 then x = 5 end self:SetCurrentWeaponProficiency(x) end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]