
"TA.RIFLE_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/FALSniper/FAL_Draw.wav"
}

"TA.RIFLE_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/FALSniper/FAL_Holster.wav"
}

"TA.RIFLE_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/FALSniper/FAL_MagOut.wav"
}

"TA.RIFLE_MagIn_Rattle"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/FALSniper/FAL_MagIn_Rustle.wav"
}

"TA.RIFLE_MagIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/FALSniper/FAL_MagIn.wav"
}

"TA.RIFLE_GrabMag"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/FALSniper/FAL_GrabMag.wav"
}

"TA.RIFLE_BoltBack"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/FALSniper/FAL_BoltBack.wav"
}

"TA.RIFLE_BoltForward"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/FALSniper/FAL_BoltForward.wav"
}

// Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
// created by myself and is used (hopefully, sorry in advance) under the copyright law
// of recreational purposes without profit.

// The credits for all of those belong to their original authors and creators who's work being
// in public domain has allowed me to make this game.

// Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
// My Animations and Sound Effects may be repurposed for personal projects but please let me
// know before hand, Credit where credit is due of course.

// However my Programming and Particle Effects should not be repurposed or used under any other context.
// Please respect my choice on this matter.

// Magenta Iris © 2018-2022