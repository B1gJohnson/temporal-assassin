
"TA.Omen_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Omen/Draw.wav"
}

"TA.Omen_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Omen/Holster.wav"
}

"TA.Omen_HolsterGrab"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/LongHorn/Grab_Weapon4.wav"
}

"TA.Omen_MagRelease"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Omen/Mag_Release.wav"
}

"TA.Omen_MagOut_Base"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Omen/Mag_Out_Base.wav"
}

"TA.Omen_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Omen/Mag_Out.wav"
}

"TA.Omen_MagIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Omen/Mag_In.wav"
}

"TA.Omen_GrabMag"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/LongHorn/GrabMag.wav"
}

"TA.Omen_BoltBack"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Omen/Bolt_Back.wav"
}

"TA.Omen_BoltForward"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Omen/Bolt_Forward.wav"
}