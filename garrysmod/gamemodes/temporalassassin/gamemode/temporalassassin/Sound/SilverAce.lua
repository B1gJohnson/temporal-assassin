
"TA.SilverAce_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/SilverAce/Draw.wav"
}

"TA.SilverAce_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SilverAce/Holster.wav"
}

"TA.SilverAce_ReloadClose"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
		"wave"			"TemporalAssassin/Weapons/SilverAce/Reload_Close.wav"
		"wave"			"TemporalAssassin/Weapons/SilverAce/Reload_Close2.wav"
	}
}

"TA.SilverAce_ReloadStart"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SilverAce/Reload_Start.wav"
}

"TA.SilverAce_ReloadBeforeOpen"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SilverAce/Reload_BeforeOpen.wav"
}

"TA.SilverAce_ReloadOpen"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SilverAce/Reload_Open.wav"
}

"TA.SilverAce_ReloadEnd"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SilverAce/Reload_End.wav"
}

"TA.SilverAce_GrabWeapon"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SilverAce/Grab_Weapon.wav"
}