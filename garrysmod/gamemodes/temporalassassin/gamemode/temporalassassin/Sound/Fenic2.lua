
"TA.Fenic2_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Fenic/Fenic2_Draw.wav"
}

"TA.Fenic2_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic2_Holster.wav"
}

"TA.Fenic2_MagOut_Click"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic2_MagOut_Click.wav"
}

"TA.Fenic2_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic2_MagOut.wav"
	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic2_MagOut2.wav"
	}
}

"TA.Fenic2_GrabMag"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic_GrabMag.wav"
}

"TA.Fenic2_MagIn_Slide"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic2_MagIn_Slide.wav"
}

"TA.Fenic2_MagIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic2_MagIn.wav"
	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic2_MagIn2.wav"
	}
}

"TA.Fenic2_SlideRelease"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic2_SlideRelease.wav"
}

// Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
// created by myself and is used (hopefully, sorry in advance) under the copyright law
// of recreational purposes without profit.

// The credits for all of those belong to their original authors and creators who's work being
// in public domain has allowed me to make this game.

// Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
// My Animations and Sound Effects may be repurposed for personal projects but please let me
// know before hand, Credit where credit is due of course.

// However my Programming and Particle Effects should not be repurposed or used under any other context.
// Please respect my choice on this matter.

// Magenta Iris © 2018-2022