
"TA.GrisMK2_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/GrisMK2/Draw.wav"
}

"TA.GrisMK2_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/GrisMK2/Holster.wav"
}

"TA.GrisMK2_MagOut_Click"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/GrisMK2/MagOut_Click.wav"
}

"TA.GrisMK2_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/GrisMK2/Mag_Out1.wav"
	"wave"			"TemporalAssassin/Weapons/GrisMK2/Mag_Out2.wav"
	"wave"			"TemporalAssassin/Weapons/GrisMK2/Mag_Out3.wav"
	}
}

"TA.GrisMK2_GrabMag"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Fenic/Fenic_GrabMag.wav"
}

//"TA.GrisMK2_MagIn_Slide"
//{
//	"channel"		"CHAN_STATIC"
//	"volume"		"1.0"
//	"pitch"			"98,102"
//	"soundlevel"	"SNDLEVEL_45dB"
//
//	"wave"			"TemporalAssassin/Weapons/GrisMK2/MagIn_Slide.wav"
//}

"TA.GrisMK2_MagIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/GrisMK2/Mag_In1.wav"
	"wave"			"TemporalAssassin/Weapons/GrisMK2/Mag_In2.wav"
	}
}

"TA.GrisMK2_SlideRelease"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/GrisMK2/SlideRelease.wav"
}