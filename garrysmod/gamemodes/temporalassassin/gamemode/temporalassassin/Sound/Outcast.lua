
"TA.Outcast_Draw"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"	"TemporalAssassin/Weapons/Outcast/Draw1.wav"
	"wave"	"TemporalAssassin/Weapons/Outcast/Draw2.wav"
	"wave"	"TemporalAssassin/Weapons/Outcast/Draw3.wav"
	}
}

"TA.Outcast_Draw2"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Outcast/Draw_P2.wav"
}

"TA.Outcast_FidgetClose"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Outcast/Fidget_Close.wav"
}

"TA.Outcast_FidgetOpen"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Outcast/Fidget_Open.wav"
}

"TA.Outcast_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Outcast/Holster.wav"
}

"TA.Outcast_GunOpen"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Outcast/Gun_Open.wav"
}

"TA.Outcast_GunClose"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"	"TemporalAssassin/Weapons/Outcast/Gun_Close1.wav"
	"wave"	"TemporalAssassin/Weapons/Outcast/Gun_Close2.wav"
	"wave"	"TemporalAssassin/Weapons/Outcast/Gun_Close3.wav"
	}
}

"TA.Outcast_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"	"TemporalAssassin/Weapons/Outcast/Mag_Out1.wav"
	"wave"	"TemporalAssassin/Weapons/Outcast/Mag_Out2.wav"
	"wave"	"TemporalAssassin/Weapons/Outcast/Mag_Out3.wav"
	}
}

"TA.Outcast_MagIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"	"TemporalAssassin/Weapons/Outcast/Mag_In1.wav"
	"wave"	"TemporalAssassin/Weapons/Outcast/Mag_In2.wav"
	}
}