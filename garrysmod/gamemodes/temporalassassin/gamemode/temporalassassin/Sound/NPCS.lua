
"NPC.FastZombie_FallOver"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_60dB"

	"wave"	"TemporalAssassin/NPC/FZombie_FallOver.wav"
}

"NPC.Combine_FallOver"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_60dB"

	"wave"	"TemporalAssassin/NPC/Combine_FallOver.wav"
}

"NPC.MetroPolice_FallOver"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_60dB"

	"wave"	"TemporalAssassin/NPC/MetroPolice_FallOver.wav"
}

"NPC.HeavyCombine_PunchStart"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_80dB"

	"wave"	"TemporalAssassin/NPC/HeavyCombine/Punch_Start.wav"
}

"NPC.HeavyCombine_Punch"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_80dB"

	"wave"	"TemporalAssassin/NPC/HeavyCombine/Punch.wav"
}

"NPC.HeavyCombine_Step"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_60dB"

	"rndwave"
	{
		"wave"	")TemporalAssassin/NPC/HeavyCombine/Footstep1.wav"
		"wave"	")TemporalAssassin/NPC/HeavyCombine/Footstep2.wav"
		"wave"	")TemporalAssassin/NPC/HeavyCombine/Footstep3.wav"
		"wave"	")TemporalAssassin/NPC/HeavyCombine/Footstep4.wav"
		"wave"	")TemporalAssassin/NPC/HeavyCombine/Footstep5.wav"
		"wave"	")TemporalAssassin/NPC/HeavyCombine/Footstep6.wav"
	}
}

"NPC.FireSoul_Step"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_60dB"

	"rndwave"
	{
		"wave"	")TemporalAssassin/NPC/FireSoul/Footstep1.wav"
		"wave"	")TemporalAssassin/NPC/FireSoul/Footstep2.wav"
		"wave"	")TemporalAssassin/NPC/FireSoul/Footstep3.wav"
		"wave"	")TemporalAssassin/NPC/FireSoul/Footstep4.wav"
	}
}