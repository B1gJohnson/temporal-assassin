
"TA.Cruentus2_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Cruentus/Cruentus2_Draw.wav"
}

"TA.Cruentus2_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus2_Holster.wav"
}

"TA.Cruentus2_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus2_MagOut.wav"
}

"TA.Cruentus2_GrabMag"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"96,104"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus2_GrabMagazine.wav"
}

"TA.Cruentus2_MagIn"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus2_MagIn.wav"
}

"TA.Cruentus2_PlatesUnlock"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"95,105"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus2_PlatesUnlock.wav"
}

"TA.Cruentus2_PlatesLock"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"95,105"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus2_PlatesLock.wav"
}

"TA.Cruentus2_PlatesOpen"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"95,105"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus2_PlatesOpen.wav"
}

"TA.Cruentus2_PlatesClose"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"95,105"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus2_PlatesClose.wav"
}