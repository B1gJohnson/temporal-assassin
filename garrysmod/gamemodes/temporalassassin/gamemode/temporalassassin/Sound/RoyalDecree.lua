
"TA.RoyalDecree_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/RoyalDecree/Draw.wav"
}

"TA.RoyalDecree_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/Holster.wav"
}

"TA.RoyalDecree_ReleaseStart"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/ReleaseStart.wav"
}

"TA.RoyalDecree_Release"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/Release.wav"
}

"TA.RoyalDecree_BarrelSmoke"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/BarrelSmoke.wav"
}

"TA.RoyalDecree_GrabShells"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/GrabShells.wav"
}

"TA.RoyalDecree_ShellsIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/ShellsIn.wav"
}

"TA.RoyalDecree_ShellsIn2"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/ShellsIn2.wav"
}

"TA.RoyalDecree_GrabWeapon"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/GrabWeapon.wav"
}

"TA.RoyalDecree_CloseWeapon"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/CloseWeapon.wav"
}

"TA.RoyalDecree_FidgetIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/FidgetIn.wav"
}

"TA.RoyalDecree_FidgetWispSpawn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.6"
	"pitch"			"104,106"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/FidgetWispSpawn.wav"
}

"TA.RoyalDecree_FidgetWispNotice"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.6"
	"pitch"			"104,106"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoyalDecree/FidgetWispLeave.wav"
}