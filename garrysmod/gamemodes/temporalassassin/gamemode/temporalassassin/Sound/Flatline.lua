
"TA.Flatline_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Flatline/Draw.wav"
}

"TA.Flatline_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Holster.wav"
}

"TA.Flatline_MechBack"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Mech_Back.wav"
}

"TA.Flatline_MechForward"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Mech_Forward.wav"
}

"TA.Flatline_Fidget1"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Fidget_1.wav"
}

"TA.Flatline_Fidget2"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Fidget_2.wav"
}

"TA.Flatline_Fidget3"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Fidget_3.wav"
}

"TA.Flatline_ReloadStart"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Flatline/Reload_Start1.wav"
	"wave"			"TemporalAssassin/Weapons/Flatline/Reload_Start2.wav"
	"wave"			"TemporalAssassin/Weapons/Flatline/Reload_Start3.wav"
	"wave"			"TemporalAssassin/Weapons/Flatline/Reload_Start4.wav"
	"wave"			"TemporalAssassin/Weapons/Flatline/Reload_Start5.wav"
	}
}

"TA.Flatline_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Mag_Out.wav"
}

"TA.Flatline_MagIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Mag_In.wav"
}

"TA.Flatline_GrabWeapon"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Flatline/Grab_Weapon1.wav"
	"wave"			"TemporalAssassin/Weapons/Flatline/Grab_Weapon2.wav"
	"wave"			"TemporalAssassin/Weapons/Flatline/Grab_Weapon3.wav"
	}
}

"TA.Flatline_BoltBack"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Bolt_Back.wav"
}

"TA.Flatline_BoltForward"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Flatline/Bolt_Forward.wav"
}