
"TA.SummonDragon_Portal"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_PortalOpen.wav"
}

"TA.SummonDragon_PortalLight"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_PortalOpenLight.wav"
}

"TA.SummonDragon_DBite"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Bite1.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Bite2.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Bite3.wav"
	}
}

"TA.SummonDragon_DComplain"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Complain.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Complain2.wav"
	}
}

"TA.SummonDragon_DComplainNoTarget"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_ComplainNoTarget.wav"
}

"TA.SummonDragon_DHuff"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Huff.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Huff2.wav"
	}
}

"TA.SummonDragon_DPurr"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.6"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Purr.wav"
}

"TA.SummonDragon_PetScratch"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Scratch1.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_Scratch2.wav"
	}
}

"TA.SummonDragon_PetPat"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_PatPat1.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_PatPat2.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Dragon_PatPat3.wav"
	}
}

// Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
// created by myself and is used (hopefully, sorry in advance) under the copyright law
// of recreational purposes without profit.

// The credits for all of those belong to their original authors and creators who's work being
// in public domain has allowed me to make this game.

// Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
// My Animations and Sound Effects may be repurposed for personal projects but please let me
// know before hand, Credit where credit is due of course.

// However my Programming and Particle Effects should not be repurposed or used under any other context.
// Please respect my choice on this matter.

// Magenta Iris © 2018-2022