
"TA.SnakeCharmer_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_Draw.wav"
}

"TA.SnakeCharmer_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_Holster.wav"
}

"TA.SnakeCharmer_BarrelUp"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_BarrelUp.wav"
}

"TA.SnakeCharmer_BarrelDown"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_BarrelDown.wav"
}

"TA.SnakeCharmer_Release"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_Release.wav"
}

"TA.SnakeCharmer_ShellsIn"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_ShellsIn.wav"
}

"TA.SnakeCharmer_ShellsInStruggle"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_ShellsInStruggle.wav"
}

"TA.SnakeCharmer_ShellsOut"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_ShellsOut.wav"
}

"TA.SnakeCharmer_ShellsGrab"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_ShellsGrab.wav"
}

"TA.SnakeCharmer_GrabBarrel"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_GrabBarrel.wav"
}