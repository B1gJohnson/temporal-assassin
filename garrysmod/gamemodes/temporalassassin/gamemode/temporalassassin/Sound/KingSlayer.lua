
"TA.KingSlayer_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/KingSlayer/KingSlayer_Draw.wav"
}

"TA.KingSlayer_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/KingSlayer/KingSlayer_Holster.wav"
}

"TA.KingSlayer_BarrelUp"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/KingSlayer/KingSlayer_BarrelUp.wav"
}

"TA.KingSlayer_BarrelDown"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/KingSlayer/KingSlayer_BarrelDown.wav"
}

"TA.KingSlayer_ShellGrab"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/KingSlayer/KingSlayer_GrabShell1.wav"
	"wave"			"TemporalAssassin/Weapons/KingSlayer/KingSlayer_GrabShell2.wav"
	}
}

"TA.KingSlayer_ShellIn"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/KingSlayer/KingSlayer_ShellIn.wav"
}

"TA.KingSlayer_GrabBarrel"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/SnakeCharmer/SnakeCharmer_GrabBarrel.wav"
}

// Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
// created by myself and is used (hopefully, sorry in advance) under the copyright law
// of recreational purposes without profit.

// The credits for all of those belong to their original authors and creators who's work being
// in public domain has allowed me to make this game.

// Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
// My Animations and Sound Effects may be repurposed for personal projects but please let me
// know before hand, Credit where credit is due of course.

// However my Programming and Particle Effects should not be repurposed or used under any other context.
// Please respect my choice on this matter.

// Magenta Iris © 2018-2022