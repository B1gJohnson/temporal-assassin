
"TA.Wolf_DrawSpin"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/RoaringWolf/Wolf_DrawSpin.wav"
}

"TA.Wolf_DrawGrab"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/RoaringWolf/Wolf_DrawCatch.wav"
}

"TA.Wolf_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_Holster.wav"
}

"TA.Wolf_CylinderRelease"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_CylinderRelease.wav"
}

"TA.Wolf_CylinderOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_CylinderOut.wav"
}

"TA.Wolf_BulletsOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_BulletsOut.wav"
}

"TA.Wolf_GrabLoader"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_GrabLoader.wav"
}

"TA.Wolf_LoaderSettle"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_LoaderSettle1.wav"
	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_LoaderSettle2.wav"
	}
}

"TA.Wolf_LoaderIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_LoaderIn.wav"
}

"TA.Wolf_LoaderTwist"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_LoaderTwist.wav"
}

"TA.Wolf_LoaderOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_LoaderOut.wav"
}

"TA.Wolf_CylinderIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_CylinderIn.wav"
}

"TA.Wolf_HammerBack"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_HammerCock1.wav"
	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_HammerCock2.wav"
	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_HammerCock3.wav"
	}
}

"TA.WolfSE_CylinderOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_SE_Open.wav"
}

"TA.WolfSE_LoaderIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_SE_Reload.wav"
}

"TA.WolfSE_CylinderIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/RoaringWolf/Wolf_SE_Close.wav"
}

// Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
// created by myself and is used (hopefully, sorry in advance) under the copyright law
// of recreational purposes without profit.

// The credits for all of those belong to their original authors and creators who's work being
// in public domain has allowed me to make this game.

// Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
// My Animations and Sound Effects may be repurposed for personal projects but please let me
// know before hand, Credit where credit is due of course.

// However my Programming and Particle Effects should not be repurposed or used under any other context.
// Please respect my choice on this matter.

// Magenta Iris © 2018-2022