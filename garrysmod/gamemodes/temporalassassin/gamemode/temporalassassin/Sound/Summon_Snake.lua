
"TA.SummonSnake_View"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_SummonView.wav"
}

"TA.SummonSnake_PortalOpenFast"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_PortalOpenFast1.wav"
}

"TA.SummonSnake_ShortHiss1"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_ShortHiss1.wav"
}

"TA.SummonSnake_Scratch"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch1.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch2.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch3.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch4.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch5.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch6.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch7.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch8.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch9.wav"
	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_Scratch10.wav"
	}
}

"TA.SummonSnake_ShortHiss4"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_ShortHiss4.wav"
}

"TA.SummonSnake_PortalCloseFast"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Summon/Reikori/Reikori_PortalCloseFast1.wav"
}

// Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
// created by myself and is used (hopefully, sorry in advance) under the copyright law
// of recreational purposes without profit.

// The credits for all of those belong to their original authors and creators who's work being
// in public domain has allowed me to make this game.

// Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
// My Animations and Sound Effects may be repurposed for personal projects but please let me
// know before hand, Credit where credit is due of course.

// However my Programming and Particle Effects should not be repurposed or used under any other context.
// Please respect my choice on this matter.

// Magenta Iris © 2018-2022