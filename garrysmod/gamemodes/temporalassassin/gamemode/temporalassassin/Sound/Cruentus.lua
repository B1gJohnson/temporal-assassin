
"TA.Cruentus_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/Cruentus/Cruentus_Draw.wav"
}

"TA.Cruentus_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus_Holster.wav"
}

"TA.Cruentus_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus_MagOut.wav"
}

"TA.Cruentus_GrabMag"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"96,104"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus_GrabMagazine.wav"
}

"TA.Cruentus_MagIn_Rustle"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus_MagIn_Rustle.wav"
}

"TA.Cruentus_MagIn"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"97,103"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/Cruentus/Cruentus_MagIn.wav"
}