
"TA.ReikoriRifle_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Draw.wav"
}

"TA.ReikoriRifle_Holster"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Holster.wav"
}

"TA.ReikoriRifle_GrabWeapon"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Grab_Weapon.wav"
}

"TA.ReikoriRifle_FidgetStart"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Fidget_Start.wav"
}

"TA.ReikoriRifle_FidgetTransition"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Fidget_Transition.wav"
}

"TA.ReikoriRifle_FidgetDrawBullet"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Fidget_DrawBullet.wav"
}

"TA.ReikoriRifle_FidgetBulletTurn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Fidget_BulletTurn.wav"
}

"TA.ReikoriRifle_FidgetBulletHolster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Fidget_BulletHolster.wav"
}

"TA.ReikoriRifle_FidgetReturn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Fidget_Return.wav"
}

"TA.ReikoriRifle_ReloadStart"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Reload_Start.wav"
}

"TA.ReikoriRifle_ReloadGrabBullets"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Reload_GrabBullets.wav"
}

"TA.ReikoriRifle_ReloadPlaceBullets"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Reload_PlaceBullets.wav"
}

"TA.ReikoriRifle_ReloadEnd"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Fidget_Start.wav"
}

"TA.ReikoriRifle_ReloadReturn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/ReikoriRifle/Fidget_Return.wav"
}