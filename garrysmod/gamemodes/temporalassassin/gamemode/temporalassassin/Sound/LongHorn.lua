
"TA.LongHorn_Draw"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/LongHorn/Draw.wav"
}

"TA.LongHorn_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/LongHorn/Holster.wav"
}

"TA.LongHorn_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/LongHorn/Mag_Out.wav"
}

"TA.LongHorn_MagIn_Rattle"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/LongHorn/Mag_In_Settle.wav"
}

"TA.LongHorn_MagIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/LongHorn/Mag_In.wav"
}

"TA.LongHorn_GrabMag"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/LongHorn/GrabMag.wav"
}

"TA.LongHorn_GripWeapon"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/LongHorn/Grab_Weapon1.wav"
	"wave"			"TemporalAssassin/Weapons/LongHorn/Grab_Weapon2.wav"
	"wave"			"TemporalAssassin/Weapons/LongHorn/Grab_Weapon3.wav"
	"wave"			"TemporalAssassin/Weapons/LongHorn/Grab_Weapon4.wav"
	"wave"			"TemporalAssassin/Weapons/LongHorn/Grab_Weapon5.wav"
	}
}


// Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
// created by myself and is used (hopefully, sorry in advance) under the copyright law
// of recreational purposes without profit.

// The credits for all of those belong to their original authors and creators who's work being
// in public domain has allowed me to make this game.

// Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
// My Animations and Sound Effects may be repurposed for personal projects but please let me
// know before hand, Credit where credit is due of course.

// However my Programming and Particle Effects should not be repurposed or used under any other context.
// Please respect my choice on this matter.

// Magenta Iris © 2018-2022