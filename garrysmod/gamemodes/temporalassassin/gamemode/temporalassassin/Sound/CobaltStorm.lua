
"TA.CobaltStorm_Draw"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"	"TemporalAssassin/Weapons/CobaltStorm/Draw.wav"
}

"TA.CobaltStorm_Holster"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Holster.wav"
}

"TA.CobaltStorm_ReloadStart"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Reload_Start1.wav"
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Reload_Start2.wav"
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Reload_Start3.wav"
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Reload_Start4.wav"
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Reload_Start5.wav"
	}
}

"TA.CobaltStorm_ReleasePush"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Release_Push.wav"
}

"TA.CobaltStorm_ReleasePull"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Release_Pull.wav"
}

"TA.CobaltStorm_CylinderOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Cylinder_Out.wav"
}

"TA.CobaltStorm_CylinderIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Cylinder_In.wav"
}

"TA.CobaltStorm_MagOut"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Mag_Out.wav"
}

"TA.CobaltStorm_GrabMag"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Mag_Grab.wav"
}

"TA.CobaltStorm_MagPlace"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Mag_Place.wav"
}

"TA.CobaltStorm_MagInSlide"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Mag_Slide.wav"
}

"TA.CobaltStorm_MagIn"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Mag_In.wav"
}

"TA.CobaltStorm_GrabWeapon"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"pitch"			"98,102"
	"soundlevel"	"SNDLEVEL_45dB"

	"rndwave"
	{
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Grab_Weapon1.wav"
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Grab_Weapon2.wav"
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Grab_Weapon3.wav"
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Grab_Weapon4.wav"
	"wave"			"TemporalAssassin/Weapons/CobaltStorm/Grab_Weapon5.wav"
	}
}