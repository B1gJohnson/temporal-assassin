 local RPlayer = debug.getregistry().Player local REntity = debug.getregistry().Entity function RPlayer:HasLoreFile( name ) local PL = self local STRING = string.lower(tostring("player_lore_"..name)) local STRING2 = string.lower(tostring("player_lore_retry_"..name)) local CT = CurTime() if PL[STRING2] and CT < PL[STRING2] then return false end if not PL[STRING] and file.Exists("temporalassassin/lore/"..name..".txt","DATA") then PL[STRING] = true return true elseif PL[STRING] then return true else PL[STRING2] = CT + 0.2 return false end end if SERVER then util.AddNetworkString("TA_UnlockLore") function RPlayer:Unlock_TA_LoreFile( name, nicetxt ) if not nicetxt then nicetxt = "Lore File Unlocked: [UNDEFINED]" end net.Start("TA_UnlockLore") net.WriteString(string.lower(name)) net.WriteString(nicetxt) net.Send(self) end return end file.CreateDir("temporalassassin/lore/") function TA_UnlockLore_LIB( len, pl ) local NAME = net.ReadString() local NICETXT = net.ReadString() Unlock_TA_LoreFile( NAME, NICETXT ) end net.Receive("TA_UnlockLore",TA_UnlockLore_LIB) function TA_HasLoreFile( name ) if DEV_ALL_LORE then return true end local PL = LocalPlayer() local STRING = tostring("player_lore_"..name) --[[ Lets make it so it falls back to using a players saved variable once we know the file exists so we're not calling file.Exists every frame. ]] if not PL[STRING] and file.Exists("temporalassassin/lore/"..name..".txt","DATA") then PL[STRING] = true return true elseif PL[STRING] then return true else return false end end function Unlock_TA_LoreFile( name, nicetxt ) if not nicetxt then nicetxt = "Lore File Unlocked: [UNDEFINED]" end name = string.lower(name) if not TA_HasLoreFile(name) then file.Write("temporalassassin/lore/"..name..".txt","THE_LORE") if TEMPORAL_AddBattleLog then TEMPORAL_AddBattleLog(nicetxt) end end end Unlock_TA_LoreFile("weapon_arcblade_1","Lore File Unlocked: A.R.C Blade #1") Unlock_TA_LoreFile("weapon_arcblade_2","Lore File Unlocked: A.R.C Blade #2") Unlock_TA_LoreFile("weapon_arcblade_3","Lore File Unlocked: A.R.C Blade #3") Unlock_TA_LoreFile("weapon_fenic_1","Lore File Unlocked: S.I.S Fenic #1") Unlock_TA_LoreFile("weapon_fenic_2","Lore File Unlocked: S.I.S Fenic #2") Unlock_TA_LoreFile("weapon_fenic_3","Lore File Unlocked: S.I.S Fenic #3") Unlock_TA_LoreFile("weapon_griseus_1","Lore File Unlocked: Griseus Special MK.II #1") Unlock_TA_LoreFile("weapon_photoop_1","Lore File Unlocked: DI2 Photo-Op #1")

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]