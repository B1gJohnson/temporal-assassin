 local RPlayer = debug.getregistry().Player local REntity = debug.getregistry().Entity function HealthScale_Think() local PL = LocalPlayer() local HP = PL:Health() local CT = UnPredictedCurTime() if LAST_HEALTH and HP < LAST_HEALTH then HEALTH_RED_VIS = 0.35 PLAYER_LAST_SHOT = CT + 1.5 end LAST_HEALTH = HP end hook.Add("Think","HealthScale_ThinkHook",HealthScale_Think) function TEMPORAL_Health_ViewOverlay() if gui.IsGameUIVisible() then return end end hook.Add("RenderScreenspaceEffects","TEMPORAL_Health_ViewOverlayHook",TEMPORAL_Health_ViewOverlay)
 
--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]