
if CLIENT then return end

if not file.Exists("TemporalAssassin/Coast_7_OffShoot.txt","DATA") then
	LEVEL_STRINGS["d2_coast_07"] = {"d2_coast_08",Vector(3328.0402832031,3914.5185546875,1536.03125), Vector(-107.4291229248,-107.4291229248,0), Vector(107.4291229248,107.4291229248,131.6201171875)}
else
	LEVEL_STRINGS["d2_coast_07"] = {"d2_coast_09",Vector(8160.0278320313,-7897.9370117188,2048.03125), Vector(-503.60241699219,-503.60241699219,0), Vector(503.60241699219,503.60241699219,191.9375)}
end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("env_fade")
	RemoveAllEnts("trigger_hurt")
	
	if file.Exists("TemporalAssassin/Coast_7_OffShoot.txt","DATA") then
	
		CreatePlayerSpawn( Vector(3329,5050,1536.1), Angle(0,90,0) )
		
		for _,v in pairs (ents.GetAll()) do
		
			if IsValid(v) then
				
				if v:GetName() == "bridge_field_02" then
					v:Fire("Disable")
				elseif (v:IsNPC() or v:IsNextBot()) then
					v:Remove()
				end
				
			end
			
		end
		
		if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
		else
		
		end
		
	else

		CreatePlayerSpawn( Vector(-6408,6111,1600.1), Angle(0,0,0) )
	
		if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
		
			Create_Medkit25( Vector(-1327.5179443359, 8902.42578125, 1696.03125), Angle(0, 91.879745483398, 0) )
			Create_PortableMedkit( Vector(606.18902587891, 7333.2392578125, 1521.7332763672), Angle(0, 269.05972290039, 0) )
			Create_ManaCrystal100( Vector(1519.9528808594, 6035.4575195313, 1529.5827636719), Angle(0, 125.69946289063, 0) )
			Create_KingSlayerAmmo( Vector(552.90386962891, 5736.1567382813, 1540.03125), Angle(0, 1.3993072509766, 0) )
			Create_KingSlayerAmmo( Vector(548.10296630859, 5664.3979492188, 1540.03125), Angle(0, 0.95930480957031, 0) )
			Create_SuperShotgunWeapon( Vector(597.62084960938, 5705.390625, 1540.03125), Angle(0, 2.4992523193359, 0) )
			Create_ShotgunAmmo( Vector(549.0205078125, 5703.119140625, 1540.03125), Angle(0, 0.95916748046875, 0) )
			Create_Medkit25( Vector(1155.0958251953, 5248.916015625, 1584.03125), Angle(0, 180.99914550781, 0) )
			Create_Medkit10( Vector(736.67944335938, 5280.19921875, 1606.03125), Angle(0, 88.959457397461, 0) )
			Create_Medkit10( Vector(714.07873535156, 5280.08203125, 1606.03125), Angle(0, 64.319320678711, 0) )
			Create_PistolAmmo( Vector(614.63720703125, 5543.9536132813, 1568.03125), Angle(0, 268.99871826172, 0) )
			Create_PistolAmmo( Vector(614.37292480469, 5505.6865234375, 1568.03125), Angle(0, 268.55871582031, 0) )
			Create_RevolverWeapon( Vector(813.46569824219, 5096.6000976563, 1696.03125), Angle(0, 76.858879089355, 0) )
			Create_PistolWeapon( Vector(1122.8702392578, 5320.3149414063, 1696.03125), Angle(0, 165.95892333984, 0) )
			Create_PistolAmmo( Vector(1122.0076904297, 5354.5258789063, 1696.03125), Angle(0, 195.21891784668, 0) )
			
		else
		
			CreateClothesLocker( Vector(-1147.991,8546.87,16.217), Angle(0,283.773,0) )
			
			Create_Medkit25( Vector(-1434.399, 9053.445, 1696.031), Angle(0, 322.227, 0) )
			Create_Medkit25( Vector(-1257.08, 9054.867, 1696.031), Angle(0, 225.983, 0) )
			Create_Armour( Vector(-1123.974, 9054.732, 1696.031), Angle(0, 167.776, 0) )
			Create_Medkit25( Vector(416.544, 6747.02, 1568.031), Angle(0, 290.042, 0) )
			Create_Medkit10( Vector(555.556, 6754.513, 1568.031), Angle(0, 294.431, 0) )
			Create_Medkit10( Vector(583.848, 6751.438, 1568.031), Angle(0, 244.689, 0) )
			Create_Armour5( Vector(715.363, 6461.348, 1568.031), Angle(0, 88.566, 0) )
			Create_Armour5( Vector(715.921, 6387.862, 1568.031), Angle(0, 90.238, 0) )
			Create_Armour5( Vector(691.746, 6320.127, 1568.031), Angle(0, 73.727, 0) )
			Create_VolatileBattery( Vector(425.949, 6289.867, 1568.031), Angle(0, 2.875, 0) )
			Create_RevolverWeapon( Vector(579.378, 6181.438, 1568.031), Angle(0, 99.433, 0) )
			Create_GrenadeAmmo( Vector(544.465, 6169.681, 1568.031), Angle(0, 72.264, 0) )
			Create_SMGWeapon( Vector(555.669, 6374.023, 1568.031), Angle(0, 278.23, 0) )
			Create_SMGAmmo( Vector(527.734, 6404.021, 1568.031), Angle(0, 296.831, 0) )
			Create_FalanranWeapon( Vector(-1034.043, 8477.565, 11.327), Angle(0, 5.074, 0) )
			Create_KingSlayerWeapon( Vector(586.536, 5785.15, 1540.031), Angle(0, 272.279, 0) )
			Create_Medkit25( Vector(837.651, 5608.975, 1540.031), Angle(0, 114.902, 0) )
			Create_CrossbowWeapon( Vector(720.282, 5136.398, 1568.031), Angle(0, 210.622, 0) )
			Create_CrossbowAmmo( Vector(725.003, 5084.415, 1568.031), Angle(0, 162.134, 0) )
			Create_ManaCrystal100( Vector(967.018, 7238.191, 1569.329), Angle(0, 279.174, 0) )
			Create_PortableMedkit( Vector(667.984, 7169.528, 1526.422), Angle(0, 85.747, 0) )
			Create_EldritchArmour10( Vector(1855.292, 9301.909, 1698.908), Angle(0, 116.262, 0) )
			Create_EldritchArmour10( Vector(1830.933, 9299.111, 1698.698), Angle(0, 86.166, 0) )
			Create_EldritchArmour10( Vector(1840.123, 9314.859, 1697.457), Angle(0, 95.362, 0) )
			
			CreateSecretArea( Vector(-1030.017,8475.415,15.844), Vector(-83.419,-83.419,0), Vector(83.419,83.419,123.3), "FOUND THE TELEKINETIC SWORD FALANRAN" )
			CreateSecretArea( Vector(1841.633,9304.061,1703.438), Vector(-28.462,-28.462,0), Vector(28.462,28.462,33.971) )
		
		end
		
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)