		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["02_uvalno_undertower"] = {"03_uvalno_night", Vector(-2638.521,3161.553,-191.969), Vector(-310.033,-310.033,0), Vector(310.033,310.033,301.516)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-3555.399,-2312.131,121.219),Angle(0,-179.504,0))
	
	Create_Bread( Vector(-5653.782, 49.003, 142.154), Angle(45, -359.12, 0) )
	Create_Bread( Vector(-5657.839, 48.897, 143.588), Angle(-40.227, 3.022, -3.953) )
	Create_RevolverWeapon( Vector(-5458.699, 21.969, 155.164), Angle(0, 275.157, 0) )
	Create_Beer( Vector(-5518.566, 348.098, 151.049), Angle(0, -1.352, 0) )
	Create_Beer( Vector(-5526.949, 348.059, 151.035), Angle(0, 142.013, 0) )

	local VEH = ents.Create("prop_vehicle_jeep")
	VEH:SetModel("models/vehicle.mdl")
	VEH:SetKeyValue( "vehiclescript", "scripts/vehicles/jalopy.txt" )
	VEH:SetPos( Vector(-3626.969,-2244.344,124.344) )
	VEH:SetAngles( Angle(0.264,92.856,4.526) )
	VEH:Spawn()
	VEH:Activate()
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)
	
function NoShooty()

	for _,v in pairs (player.GetAll()) do
		
		if v then
			v:SetSharedBool("Safety_Zone",true)
		end
		
	end
	
end
timer.Create("NoShooty_ThisMap", 1, 0, NoShooty)
	