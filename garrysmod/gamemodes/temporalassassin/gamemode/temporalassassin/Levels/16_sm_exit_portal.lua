		
if CLIENT then return end

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("env_fade")
	RemoveAllEnts("env_sprite")
	
	for _,v in pairs (ents.GetAll()) do 
	
		if IsValid(v) and v:GetClass() == "game_text" then
		
			if string.find( string.lower(v:GetKeyValues()["message"]),"auto save" ) then
				v:SetKeyValue("message","")
			end
			
			if string.find( string.lower(v:GetKeyValues()["message"]),"go back" ) then
				v:SetKeyValue("message","")
			end
			
			if string.find( string.lower(v:GetKeyValues()["message"]),"forgot" ) then
				v:SetKeyValue("message","")
			end
			
			if string.find( string.lower(v:GetKeyValues()["message"]),"forget" ) then
				v:SetKeyValue("message","")
			end
			
			if string.find( string.lower(v:GetKeyValues()["message"]),"download data" ) then
				v:SetKeyValue("message","")
			end
			
		end
	
	end
	
	CreatePlayerSpawn(Vector(13735.400390625,-2834,1539.4888916016),Angle(0,-7,0))
	
	Create_MegaSphere( Vector(14492.237304688, -2612.9392089844, 1808.03125), Angle(0, 210.99534606934, 0) )
	Create_VolatileBattery( Vector(14433.008789063, -2611.4233398438, 1808.03125), Angle(0, 251.69548034668, 0) )
	Create_Backpack( Vector(3897.6279296875, 2180.01953125, 800.03125), Angle(0, 142.5240020752, 0) )
	Create_Armour100( Vector(3838.1809082031, 2180.8293457031, 800.03125), Angle(0, 73.663879394531, 0) )
	Create_Armour200( Vector(3567.9311523438, 2900.9155273438, 1076.3082275391), Angle(0, 91.123001098633, 0) )
	Create_EldritchArmour10( Vector(3730.7062988281, 2730.6484375, 960.03125), Angle(0, 194.30209350586, 0) )
	Create_EldritchArmour10( Vector(3730.6062011719, 2704.5910644531, 960.03125), Angle(0, 169.2220916748, 0) )
	Create_FalanranWeapon( Vector(3394.0905761719, 2805.8005371094, 960.03125), Angle(0, 348.60192871094, 0) )
	Create_KingSlayerAmmo( Vector(3410.6516113281, 2738.0578613281, 960.03125), Angle(0, 23.142105102539, 0) )
	Create_KingSlayerAmmo( Vector(3457.8034667969, 2744.9641113281, 960.03125), Angle(0, 36.012176513672, 0) )
	Create_KingSlayerWeapon( Vector(3453.3249511719, 2798.0744628906, 960.03125), Angle(0, 336.72186279297, 0) )
	Create_CrossbowWeapon( Vector(3486.6728515625, 2773.0178222656, 960.03125), Angle(0, 344.20190429688, 0) )
	Create_SpiritEye1( Vector(3667.435546875, 2811.3005371094, 960.03125), Angle(0, 184.26188659668, 0) )
	Create_ShotgunAmmo( Vector(3546.9821777344, 3421.7067871094, 358.03125), Angle(0, 190.38566589355, 0) )
	Create_SMGAmmo( Vector(3545.8208007813, 3389.03125, 358.03125), Angle(0, 167.94567871094, 0) )
	Create_RevolverWeapon( Vector(3522.7353515625, 3418.6672363281, 358.03125), Angle(0, 191.70565795898, 0) )
	Create_RevolverWeapon( Vector(3504.7268066406, 3392.7565917969, 358.03125), Angle(0, 161.34567260742, 0) )
	Create_Armour5( Vector(3934.8608398438, 2601.4401855469, 254.03125), Angle(0, 108.98561096191, 0) )
	Create_Armour5( Vector(3894.3815917969, 2599.8395996094, 254.03125), Angle(0, 70.485557556152, 0) )
	Create_Armour5( Vector(3854.916015625, 2606.57421875, 254.03125), Angle(0, 40.78547668457, 0) )
	Create_Beer( Vector(3849.9975585938, 2739.6076660156, 254.03125), Angle(0, 308.82513427734, 0) )
	Create_Beer( Vector(3857.5080566406, 2679.5080566406, 254.03125), Angle(0, 340.06530761719, 0) )
	Create_Beer( Vector(3850.1411132813, 2639.2067871094, 254.03125), Angle(0, 16.585479736328, 0) )
	Create_Bread( Vector(3894.2846679688, 2673.806640625, 254.03125), Angle(0, 53.985504150391, 0) )
	Create_Beer( Vector(2741.2658691406, 3887.2355957031, -807.96875), Angle(0, 88.22550201416, 0) )
	Create_Beer( Vector(2740.0068359375, 3817.1879882813, -799.96875), Angle(0, 88.665504455566, 0) )
	Create_Beer( Vector(2740.1120605469, 3761.5510253906, -1031.96875), Angle(0, 90.865516662598, 0) )
	Create_Backpack( Vector(3344.1918945313, 3243.1091308594, -1159.96875), Angle(0, 118.8416595459, 0) )
	Create_EldritchArmour10( Vector(3308.2353515625, 3238.84765625, -1159.96875), Angle(0, 95.081535339355, 0) )
	Create_EldritchArmour10( Vector(3277.6257324219, 3243.2502441406, -1159.96875), Angle(0, 73.521423339844, 0) )
	Create_HarbingerAmmo( Vector(3944.970703125, 1947.5017089844, -1918.96875), Angle(0, 110.28073883057, 0) )
	Create_Backpack( Vector(3944.8432617188, 2026.9432373047, -1918.96875), Angle(0, 230.18072509766, 0) )
	Create_Backpack( Vector(3946.5417480469, 1987.0782470703, -1918.96875), Angle(0, 177.60075378418, 0) )
	Create_SMGWeapon( Vector(3911.4143066406, 2006.8306884766, -1959.96875), Angle(0, 204.88075256348, 0) )
	Create_SuperShotgunWeapon( Vector(3781.9538574219, 1964.0083007813, -1959.96875), Angle(0, 56.820541381836, 0) )
	Create_RevolverWeapon( Vector(3917.685546875, 1961.4858398438, -1959.96875), Angle(0, 150.54057312012, 0) )
	Create_CrossbowWeapon( Vector(3884.9106445313, 1992.6833496094, -1959.96875), Angle(0, 159.34059143066, 0) )
	Create_KingSlayerWeapon( Vector(3885.1594238281, 1942.7576904297, -1959.96875), Angle(0, 157.14056396484, 0) )
	Create_Medkit25( Vector(3882.0541992188, 2089.1967773438, -2519.96875), Angle(0, 270.81985473633, 0) )
	Create_Medkit25( Vector(3929.6882324219, 2089.8818359375, -2519.96875), Angle(0, 270.59985351563, 0) )
	Create_Medkit25( Vector(3931.1745605469, 1629.4909667969, -2519.96875), Angle(0, 90.119979858398, 0) )
	Create_Medkit25( Vector(3886.0729980469, 1626.7263183594, -2519.96875), Angle(0, 89.019973754883, 0) )
	Create_Armour( Vector(4551.73828125, 1589.4523925781, -2599.96875), Angle(0, 247.84251403809, 0) )
	Create_Backpack( Vector(4486.275390625, 1591.0648193359, -2599.96875), Angle(0, 284.58255004883, 0) )
	Create_PistolAmmo( Vector(4470.2119140625, 1435.9195556641, -2599.96875), Angle(0, 52.482223510742, 0) )
	Create_PistolAmmo( Vector(4493.6157226563, 1430.8612060547, -2599.96875), Angle(0, 67.442306518555, 0) )
	Create_M6GAmmo( Vector(4556.0102539063, 1433.0167236328, -2599.96875), Angle(0, 111.88235473633, 0) )
	Create_M6GAmmo( Vector(4521.8813476563, 1431.9572753906, -2599.96875), Angle(0, 87.462249755859, 0) )
	Create_WhisperAmmo( Vector(4451.3276367188, 1476.8151855469, -2581.46875), Angle(0, 91.562797546387, 0) )
	Create_RevolverAmmo( Vector(4450.7280273438, 1509.6420898438, -2581.46875), Angle(0, 91.78279876709, 0) )
	Create_RevolverAmmo( Vector(4451.578125, 1530.7072753906, -2581.46875), Angle(0, 93.322807312012, 0) )
	Create_ShotgunAmmo( Vector(4450.0888671875, 1558.2349853516, -2581.46875), Angle(0, 93.542808532715, 0) )
	Create_ShotgunWeapon( Vector(4489.0205078125, 2154.4853515625, -2599.96875), Angle(0, 9.4229888916016, 0) )
	Create_KingSlayerWeapon( Vector(4493.4331054688, 2244.2961425781, -2599.96875), Angle(0, 323.14733886719, 0) )
	Create_ManaCrystal100( Vector(4559.5625, 2204.3962402344, -2599.96875), Angle(0, 8.0273742675781, 0) )
	Create_SpiritSphere( Vector(5194.5932617188, 1859.6606445313, -2599.96875), Angle(0, 270.78704833984, 0) )
	Create_ManaCrystal( Vector(5196.6206054688, 1686.1817626953, -2599.96875), Angle(0, 270.78704833984, 0) )
	Create_ManaCrystal( Vector(5426.9663085938, 1857.1641845703, -2599.96875), Angle(0, 356.58724975586, 0) )
	Create_ManaCrystal( Vector(5192.3920898438, 2062.9921875, -2599.96875), Angle(0, 90.967315673828, 0) )
	Create_ManaCrystal( Vector(4942.8745117188, 1859.0186767578, -2599.96875), Angle(0, 180.28721618652, 0) )
	Create_EldritchArmour10( Vector(5921.6206054688, 1547.8696289063, -2559.96875), Angle(0, 35.638214111328, 0) )
	Create_EldritchArmour10( Vector(5945.4736328125, 1526.0163574219, -2559.96875), Angle(0, 54.888320922852, 0) )
	Create_EldritchArmour10( Vector(5947.9877929688, 1555.39453125, -2559.96875), Angle(0, 43.008255004883, 0) )
	Create_Backpack( Vector(5920.650390625, 2161.7124023438, -2559.96875), Angle(0, 324.10833740234, 0) )
	Create_Bread( Vector(5958.1171875, 2184.3955078125, -2527.46875), Angle(0, 293.30816650391, 0) )
	Create_Bread( Vector(5997.7973632813, 2186.5383300781, -2527.46875), Angle(0, 257.00805664063, 0) )
	Create_Medkit25( Vector(6336.2763671875, 2561.5654296875, -2559.96875), Angle(0, 255.53065490723, 0) )
	Create_Medkit25( Vector(6296.82421875, 2561.2036132813, -2559.96875), Angle(0, 277.31069946289, 0) )
	Create_Medkit25( Vector(6117.5717773438, 2553.9846191406, -2559.96875), Angle(0, 316.03063964844, 0) )
	Create_Medkit25( Vector(6158.5219726563, 2557.7734375, -2559.96875), Angle(0, 279.73046875, 0) )
	Create_PortableMedkit( Vector(6114.1796875, 2436.0964355469, -2559.96875), Angle(0, 14.550659179688, 0) )
	Create_PortableMedkit( Vector(6112.712890625, 2469.2119140625, -2559.96875), Angle(0, 359.15057373047, 0) )
	Create_SpiritSphere( Vector(6341.8969726563, 2472.2905273438, -2559.96875), Angle(0, 176.6908416748, 0) )
	Create_Armour100( Vector(6177.1499023438, 2446.3645019531, -2559.96875), Angle(0, 53.380783081055, 0) )
	Create_EldritchArmour( Vector(4973.1875, 1619.1540527344, -2367.96875), Angle(0, 326.95233154297, 0) )
	Create_Backpack( Vector(5033.330078125, 1622.3627929688, -2367.96875), Angle(0, 298.57217407227, 0) )
	Create_Backpack( Vector(5084.7114257813, 1619.1575927734, -2367.96875), Angle(0, 255.89208984375, 0) )
	
	local B_FUNC = function()
		Create_EldritchArmour( Vector(3387.8400878906, 2435.9467773438, -1959.96875), Angle(0, 186.16172790527, 0) )
		Create_EldritchArmour( Vector(3392.8393554688, 2388.6960449219, -1959.96875), Angle(0, 160.64172363281, 0) )
		Create_MegaSphere( Vector(3361.6635742188, 2414.8317871094, -1959.96875), Angle(0, 171.86170959473, 0) )
	end
	
	AddSecretButton( Vector(3262.6320800781,2319.9484863281,-1908.9554443359), B_FUNC )
	
	CreateSecretArea( Vector(14470.626953125,-2606.2536621094,1808.03125), Vector(-98.170112609863,-98.170112609863,0), Vector(98.170112609863,98.170112609863,85.458618164063) )
	CreateSecretArea( Vector(3567.3447265625,2906.73828125,1080.8125), Vector(-28.344675064087,-28.344675064087,0), Vector(28.344675064087,28.344675064087,52.118408203125) )
	
	local GAME_END_TXT = {
	"After bapping some teleporter coordinate stuff and doing some cool ass",
	"slide into the portal platform, Kit got scrimble scrambled back to her home.",
	"",
	"",
	"Taking a load off on the couch, covered in blood she contemplates if",
	"that was honestly worth her time...",
	"",
	"",
	"No, No not really, But congrats on beating Strider Mountain."}	
	
	MakeGameEnd( GAME_END_TXT, 30, Vector(4099.4052734375,2709.2175292969,-1895.96875), Vector(-52.959899902344,-52.959899902344,0), Vector(52.959899902344,52.959899902344,110.27624511719) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)