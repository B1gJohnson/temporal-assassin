		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["14_uvalno_houseyard"] = {"15_uvalno_villa", Vector(695.277,-1540.201,324.031), Vector(-56.218,-56.218,0), Vector(56.218,56.218,140.369)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-729,734,95.975),Angle(0,-10.384,0))
	
	Create_Backpack( Vector(-331.571, 510.396, 122.196), Angle(0, 259.729, 0) )
	Create_Beer( Vector(289.922, 827.428, 132.295), Angle(0, 189.057, 0) )
	Create_Bread( Vector(311.294, 824.33, 132.026), Angle(0, 254.265, 0) )
	Create_Medkit10( Vector(563.969, 828.359, 147.004), Angle(-76.229, 72.345, 93.081) )
	Create_Beer( Vector(296.958, -355.815, 249.938), Angle(0, 342.697, 0) )
	Create_Beer( Vector(292.641, -380.249, 251.1), Angle(0, 356.425, 0) )
	Create_ShotgunAmmo( Vector(573.657, 492.724, 121.401), Angle(0, 244.137, 0) )
	Create_SuperShotgunWeapon( Vector(610.226, 496.137, 120.888), Angle(0, 274.761, 0) )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)