	
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["15_uvalno_villa"] = {"16_uvalno_undertower", Vector(-3390.158,-2301.969,112.905), Vector(-308.33,-308.33,0), Vector(308.33,308.33,500.083)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-132,1660,-91.5), Angle(0,-74.688,0))
	
	CreateClothesLocker(Vector(-857.312,146.156,-33.5), Angle(0,-28.301,-90))
	
	Create_Armour100( Vector(2114.823, -168.905, -78.795), Angle(-12.269, 175.173, -41.079) )
	Create_Beer( Vector(2347.373, -253.329, -42.994), Angle(0, 179.824, 0) )
	Create_Medkit25( Vector(2325.756, -259.959, -42.324), Angle(0, 103.123, 0) )
	Create_Bread( Vector(2285.433, -254.558, -42.969), Angle(0, 57.099, 0) )
	Create_SuperShotgunWeapon( Vector(683.422, 90.928, 122.148), Angle(79.39, 91.717, -83.474) )
	Create_Medkit25( Vector(-1062.136, 698.315, 138.246), Angle(0, 259.923, 0) )
	Create_PortableMedkit( Vector(-1089.201, 697.747, 138.031), Angle(0, 282.627, 0) )
	Create_SMGWeapon( Vector(-795.045, -1209.197, -54.818), Angle(4.941, 12.582, 164.64) )
	Create_SMGAmmo( Vector(-813.57, -1247.097, -45.749), Angle(1.005, 3.295, 168.845) )
	Create_Medkit25( Vector(-908.279, -1097.452, -103.969), Angle(0, 195.251, 0) )
	Create_Armour5( Vector(-824.581, -1115.589, -34.762), Angle(16.931, -98.171, -70.306) )
	Create_Armour5( Vector(-841.178, -1122.66, -34.591), Angle(-4.889, 143.356, -67.605) )
	
	local OBJECTS = {}
	table.insert(OBJECTS, CreateProp(Vector(2351,-199.937,-36.875), Angle(0.264,104.766,90.352), "models/weapons/c_models/c_frying_pan/c_frying_pan.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2376.281,-175.344,-32.781), Angle(-0.132,165.938,0.088), "models/props_c17/metalpot001a.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2346.781,-205.281,-106.781), Angle(-10.195,-173.145,5.581), "models/player/gibs/heavygib007.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2350,-148.094,-15.5), Angle(-41.968,-114.434,-151.523), "models/player/gibs/heavygib005.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2126.375,-141.187,-108.375), Angle(4.219,169.453,-42.759), "models/player/gibs/heavygib006.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2041.188,-146.5,-76.594), Angle(-84.99,-164.839,-22.808), "models/player/gibs/heavygib004.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(1992.219,-152.156,-71.094), Angle(-71.719,-102.437,-86.221), "models/player/gibs/heavygib002.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2001.125,-127.031,-68.812), Angle(-30.762,71.104,90.747), "models/player/gibs/heavygib001.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2070.156,-117.062,-67.406), Angle(0.396,149.194,-69.17), "models/weapons/w_models/w_minigun.mdl",true))
	table.insert(OBJECTS, CreateProp(Vector(2118.313,-189.062,-50.781), Angle(-0.571,78.267,-2.197), "models/props_junk/sawblade001a.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2352.344,-245.937,-41.562), Angle(0.132,38.804,89.78), "models/workshop_partner/weapons/c_models/c_sd_cleaver/c_sd_cleaver.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2310.438,-250,-41.469), Angle(2.329,-119.619,82.529), "models/weapons/w_models/w_bonesaw.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(2078,-164.375,-49.5), Angle(36.167,-169.893,157.456), "models/weapons/w_models/w_fireaxe.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(-707.156,312.469,-0.437), Angle(20.303,-21.577,-82.09), "models/player/items/medic/coh_medichat.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(-702.969,303,3.125), Angle(0.044,6.548,-92.285), "models/weapons/c_models/c_switchblade/c_switchblade.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(-1158.312,502.875,154), Angle(0,94.922,180), "models/weapons/w_models/w_knife.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(-1163.094,499.469,141.125), Angle(0.044,-179.297,0.044), "models/props_junk/cardboard_box003a.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(-1160.312,524.438,132.125), Angle(-88.682,-126.606,98.525), "models/weapons/w_models/w_cigarette_case.mdl", true))
	table.insert(OBJECTS, CreateProp(Vector(-1106.560425,512.283569,110.200684), Angle(-64.889,160.052,-43.616), "models/weapons/c_models/c_unarmed_combat/c_unarmed_combat.mdl", true))

	OBJECTS[3]:SetSkin(1)
	OBJECTS[4]:SetSkin(1)
	OBJECTS[5]:SetSkin(1)
	OBJECTS[6]:SetSkin(1)
	OBJECTS[7]:SetSkin(1)
	OBJECTS[8]:SetSkin(1)
	OBJECTS[11]:SetSkin(1)
	OBJECTS[14]:SetSkin(1)
	OBJECTS[17]:SetHealth(99999)
	OBJECTS[19]:SetSkin(1)
	
	CreateProp(Vector(682.688,71.281,99.563), Angle(0.088,-97.031,0.088), "models/props_interiors/furniture_chair03a.mdl", true)

	--[[ If I have to go through the horseshit of having to figure out something myself before asking someone, I will genuinely fucking run amok. - Lt. Grey ]]

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)