
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_hurt")
	
	CreatePlayerSpawn( Vector(12021,9465,545), Angle(0,111,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_SpiritSphere( Vector(656.24389648438, 8960.5341796875, 640.03125), Angle(0, 359.67077636719, 0) )
		Create_M6GWeapon( Vector(642.86358642578, 9021.03125, 640.03125), Angle(0, 357.47088623047, 0) )
		Create_M6GAmmo( Vector(681.96710205078, 9020.892578125, 640.03125), Angle(0, 356.15087890625, 0) )
		Create_M6GAmmo( Vector(643.95703125, 9064.2314453125, 640.03125), Angle(0, 353.73089599609, 0) )
		Create_M6GAmmo( Vector(597.5556640625, 9024.3642578125, 640.03125), Angle(0, 353.73077392578, 0) )
		Create_M6GAmmo( Vector(604.02850341797, 9066.0751953125, 640.03125), Angle(0, 345.15069580078, 0) )
		Create_M6GAmmo( Vector(682.46630859375, 9060.580078125, 640.03125), Angle(0, 334.15063476563, 0) )
		
		CreateSecretArea( Vector(648.71240234375,9006.349609375,643.0625), Vector(-96.334800720215,-96.334800720215,0), Vector(96.334800720215,96.334800720215,92.011169433594) )
		
	else
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)