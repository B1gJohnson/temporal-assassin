		
if CLIENT then return end

LEVEL_STRINGS["islandlockdown3"] = { "islandlockdown4", Vector(529.46,-1203.015,-183.969), Vector(-91.965,-91.965,0), Vector(91.965,91.965,117.139) }

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-87.729,-71.631,-56.969),Angle(0,90,0))
	
	Create_InvulnSphere( Vector(871.748, 1053.018, -311.969), Angle(0, 93.433, 0) )
	Create_Medkit25( Vector(239.979, 998.112, -183.969), Angle(0, 252.493, 0) )
	Create_SMGAmmo( Vector(490.683, 356.012, -311.969), Angle(0, 49.873, 0) )
	Create_ShotgunAmmo( Vector(553.667, 354.104, -311.969), Angle(0, 92.773, 0) )
	Create_RevolverWeapon( Vector(524.075, 355.051, -311.969), Angle(0, 83.313, 0) )
	Create_PistolWeapon( Vector(491.183, 386.776, -311.969), Angle(0, 46.573, 0) )
	Create_RifleAmmo( Vector(546.172, 382.422, -311.969), Angle(0, 78.253, 0) )
	Create_CrossbowAmmo( Vector(520.13, 387.882, -311.969), Angle(0, 60.213, 0) )
	Create_KingSlayerAmmo( Vector(488.581, 422.09, -311.969), Angle(0, 28.533, 0) )
	Create_CrossbowWeapon( Vector(543.212, 431.691, -311.969), Angle(0, 68.573, 0) )
	Create_Armour( Vector(803.766, 1855.527, -295.768), Angle(0, 221.203, 0) )
	Create_Armour( Vector(555.11, 1856.07, -295.768), Angle(0, 303.703, 0) )
	Create_Medkit25( Vector(616.6, 1856.597, -295.768), Angle(0, 283.023, 0) )
	Create_Medkit25( Vector(743.844, 1856.546, -295.768), Angle(0, 245.843, 0) )
	Create_Medkit10( Vector(663.776, 1863.267, -311.969), Angle(0, 260.583, 0) )
	Create_Medkit10( Vector(697.38, 1863.638, -311.969), Angle(0, 288.303, 0) )
	Create_ManaCrystal( Vector(634.988, 3031.896, -276.618), Angle(0, 296.548, 0) )
	Create_ManaCrystal( Vector(661.753, 3031.966, -276.618), Angle(0, 262.888, 0) )
	Create_Backpack( Vector(1011.463, 1503.81, -311.969), Angle(0, 252.351, 0) )
	Create_Medkit25( Vector(1909.944, 1047.025, -311.969), Angle(0, 172.711, 0) )
	Create_Medkit25( Vector(1902.37, 1092.37, -311.969), Angle(0, 196.691, 0) )
	Create_SpiritSphere( Vector(1728.641, 605.701, -311.969), Angle(0, 273.251, 0) )
	Create_KingSlayerAmmo( Vector(489.376, 288.018, -311.969), Angle(0, 333.113, 0) )
	Create_CrossbowAmmo( Vector(485.992, 133.822, -311.969), Angle(0, 23.493, 0) )
	Create_Medkit25( Vector(1520.641, -514.754, -311.969), Angle(0, 17.333, 0) )
	Create_RevolverAmmo( Vector(1524.836, -361.962, -311.969), Angle(0, 316.172, 0) )
	Create_PistolWeapon( Vector(1515.709, -404.125, -311.969), Angle(0, 335.753, 0) )
	Create_PistolAmmo( Vector(685.139, -153.365, -311.969), Angle(0, 187.613, 0) )
	Create_M6GAmmo( Vector(676.128, -237.514, -311.969), Angle(0, 143.613, 0) )
	Create_ManaCrystal( Vector(683.252, -194.882, -311.969), Angle(0, 179.913, 0) )
	
	Create_ShotgunAmmo( Vector(1462.114, -691.447, -183.969), Angle(0, 274.14, 0) )
	Create_Medkit10( Vector(1429.009, -682.436, -183.969), Angle(0, 290.64, 0) )
	Create_Beer( Vector(527.992, -850.64, -183.969), Angle(0, 87.235, 0) )
	Create_Beer( Vector(527.738, -930.858, -183.969), Angle(0, 88.775, 0) )
	Create_Beer( Vector(527.791, -1026.818, -183.969), Angle(0, 89.215, 0) )
	Create_VolatileBattery( Vector(-95.239, 1251.574, -183.969), Angle(0, 335.312, 0) )	
	
	CreateSecretArea( Vector(887.872,1055.364,-311.969), Vector(-39.754,-39.754,0), Vector(39.754,39.754,77.862) )
	CreateSecretArea( Vector(1742.28,590.048,-311.969), Vector(-41.489,-41.489,0), Vector(41.489,41.489,60.606) )
	CreateSecretArea( Vector(-76.855,1250.116,-179.437), Vector(-41.386,-41.386,0), Vector(41.386,41.386,59.122) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)