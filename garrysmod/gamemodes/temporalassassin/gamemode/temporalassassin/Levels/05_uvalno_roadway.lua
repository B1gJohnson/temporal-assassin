		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["05_uvalno_roadway"] = {"06_ov_hrabova", Vector(-3049.788,10912.127,-131.969), Vector(-183.498,-183.498,0), Vector(183.498,183.498,218.128)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-8964.427,-10272.503,-47.264), Angle(0,-56.752,0))
	
	Create_Armour5( Vector(-2484.216, 8368.355, -127.944), Angle(2.611, -2.406, -6.602) )
	Create_Armour5( Vector(-2493.18, 8369.485, -127.531), Angle(-18.281, 5.72, -1.61) )
	Create_PistolAmmo( Vector(-2494.541, 8350.64, -123.085), Angle(52.701, 162.654, -13.88) )
	Create_EldritchArmour10( Vector(-2494.852, 8331.399, -127.575), Angle(-86.546, -101.77, 139.49) )

	local VEH = ents.Create("prop_vehicle_jeep")
	VEH:SetModel("models/vehicle.mdl")
	VEH:SetKeyValue( "vehiclescript", "scripts/vehicles/jalopy.txt" )
	VEH:SetPos( Vector(-9113.719,-10324.281,-45.875) )
	VEH:SetAngles( Angle(-0.264,-151.919,2.021) )
	VEH:Spawn()
	VEH:Activate()
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)