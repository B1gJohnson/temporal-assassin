
if SERVER then return end

--[[
This is an example on how to put text on the screen for using locked doors with a net
message, you can find the net message run times in KEYCARD_EXAMPLE.lua
]]

local KEY_MESSAGE = false
local KEY_MESSAGETIME = 0

local Font_Table = {
	font 		= "Arial",
	size 		= GUI_X(35),
	weight 		= 300,
	antialias 	= true,
	additive 	= false,
	blursize	= 0,
	scanlines	= 0
}
surface.CreateFont( "KeyCard_Font", Font_Table )

function KEY_ScreenMessage_LIB( len )

	local STR = net.ReadString()
	
	KEY_MESSAGE = STR
	KEY_MESSAGETIME = UnPredictedCurTime() + 3

end
net.Receive("KEY_ScreenMessage",KEY_ScreenMessage_LIB)

function KEY_HUDPaint()

	local CT = UnPredictedCurTime()
	local PL = LocalPlayer()
	
	if KEY_MESSAGE and CT <= KEY_MESSAGETIME then
		draw.SimpleText( KEY_MESSAGE, "KeyCard_Font", ScrW()*0.5, ScrH()*0.3, Color(255,200,37,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )	
	end

end
hook.Add("HUDPaint","KEY_HUDPaintHook",KEY_HUDPaint)