
if CLIENT then return end

local FILES, DIRECTOR = file.Find("gamemodes/temporalassassin/gamemode/temporalassassin/levels/*_addhub.lua","GAME","nameasc")

for loop = 1,#FILES do

	local INC_S = FILES[loop]
	AddCSLuaFile(INC_S)
	print("Adding "..INC_S)
	
end

util.AddNetworkString("HUB_LoreObjects")
util.AddNetworkString("HUB_CreateAbyssWisp")
util.AddNetworkString("Reset_HubSleep")

PrecacheParticleSystem("Training_Portal")
PrecacheParticleSystem("Eldritch_Essence")
PrecacheParticleSystem("HUB_Abyss_Beacon")

local EDAX_POSITIONS = {
{Vector(-237.625,768.969,-43.344), Angle(4.713,-95.96,80.013)},
{Vector(-633.344,588.688,-0.094), Angle(56.239,105.414,-167.805)},
{Vector(-934.406,973.938,-4.156), Angle(10.124,-179.088,103.667)},
{Vector(-1199.031,408.594,-63.281), Angle(-5.251,-150.853,-0.39)},
{Vector(-896.062,237.938,-43.031), Angle(17.831,-178.583,70.922)},
{Vector(-963.406,266.125,-38.156), Angle(6.713,172.37,18.452)},
{Vector(-1204.562,582.563,-58.469), Angle(-8.937,-14.145,177.149)},
{Vector(-506.344,913.469,-58.625), Angle(-5.18,-108.853,176.957)},
{Vector(-503.156,754.219,-58.531), Angle(-7.652,30.948,177.039)},
{Vector(-832,576.688,-63.281), Angle(-5.142,45.846,-0.341)},
{Vector(-1027.937,583.25,-63.219), Angle(-6.383,-153.325,-0.412)}}

local LORE_OBJECTS = {}
LORE_OBJECTS["weapon_raikiri_1"] = function() CreateProp(Vector(-571.437,641.469,-59.344), Angle(15.029,127.408,4.169), "models/temporalassassin/items/weapon_raikiri.mdl", true) end
LORE_OBJECTS["weapon_falanran_1"] = function() local FAL = CreateProp(Vector(-211.5,575.75,11.844), Angle(35.09,-10.311,-120.745), "models/temporalassassin/items/weapon_falanran.mdl", true);FAL:SetModelScale(1.2,0) end
LORE_OBJECTS["weapon_hammer_1"] = function() local HS = CreateProp(Vector(-542.062,935.656,-6.75), Angle(-11.635,-90.939,179.72), "models/temporalassassin/items/weapon_hellsteel.mdl", true);HS:SetModelScale(1.15,0) end
LORE_OBJECTS["REITER_UNLOCK"] = function() local R = CreateProp(Vector(-515.031,509.063,-55.281), Angle(15.469,-47.763,-18.842), "models/temporalassassin/items/weapon_reiterpallasch.mdl", true);R:SetModelScale(1.05,0) end
LORE_OBJECTS["weapon_reiterpallasch_6"] = function() CreateProp(Vector(-506.25,520.469,-29.812), Angle(1.181,120.114,1.159), "models/temporalassassin/props/swordhunterbadge.mdl", true);CreateProp(Vector(-505.9,516,-29.9), Angle(0.181,150.114,1.159), "models/temporalassassin/props/cainhurstbadge.mdl", true) end
LORE_OBJECTS["weapon_fenic_1"] = function() CreateProp(Vector(-968.1875,816,-24.5625), Angle(2.5048828125,-104.150390625,-2.109375), "models/temporalassassin/items/weapon_fenic.mdl", true);CreateProp(Vector(-308.21875,882.5,-28.78125), Angle(-0.703125,-111.357421875,-7.4267578125), "models/temporalassassin/items/fenic_ammo.mdl", true) end
LORE_OBJECTS["weapon_griseus_1"] = function() CreateProp(Vector(-926.562,344.969,-30.719), Angle(59.381,-146.486,5.581), "models/temporalassassin/items/weapon_grismk2.mdl", true) CreateProp(Vector(-933.812,333.313,-33.469), Angle(-5.12,-147.975,0.143), "models/temporalassassin/items/grismk2_ammo.mdl", true);CreateProp(Vector(-418.844,506,-34.094), Angle(-32.289,74.57,-64.16), "models/temporalassassin/items/weapon_griseus.mdl", true);CreateProp(Vector(-414.656,517.813,-29.281), Angle(3.098,-157.703,178.726), "models/temporalassassin/items/griseus_ammo.mdl", true) end
LORE_OBJECTS["weapon_photoop_1"] = function() CreateProp(Vector(-1124.219,937.813,-32.844), Angle(-0.659,-39.771,-179.604), "models/temporalassassin/items/weapon_photoop.mdl", true);CreateProp(Vector(-1135.75,932.063,-33.375), Angle(8.701,49.966,-173.057), "models/temporalassassin/items/photoop_ammo.mdl", true) end
LORE_OBJECTS["weapon_roaringwolf_1"] = function() CreateProp(Vector(-1060.6875,316.6875,-42.5625), Angle(11.953125,100.634765625,2.373046875), "models/temporalassassin/items/weapon_roaringwolf.mdl", true);CreateProp(Vector(-604.375,571.34375,-37.4375), Angle(-0.3515625,-9.228515625,0.3955078125), "models/temporalassassin/items/revolver_ammo.mdl", true) end
LORE_OBJECTS["weapon_cobaltstorm_1"] = function() CreateProp(Vector(-1097.219,308.094,-40.062), Angle(68.203,171.87,116.807), "models/temporalassassin/items/weapon_cobaltstorm.mdl", true);CreateProp(Vector(-1099.719,304.781,-42.312), Angle(-1.67,-133.11,-1.318), "models/temporalassassin/items/cobaltstorm_ammo.mdl", true) end
LORE_OBJECTS["weapon_outcast_1"] = function() CreateProp(Vector(-1191.625,387.25,-29.344), Angle(1.609,28.872,-0.027), "models/temporalassassin/items/weapon_outcast.mdl", true);CreateProp(Vector(-1210.031,423.625,-29.469), Angle(-0.176,103.189,-0.291), "models/temporalassassin/items/outcast_ammo.mdl", true);CreateProp(Vector(-1211.312,428.344,-27.437), Angle(-13.574,-102.469,-88.995), "models/temporalassassin/items/outcast_ammo.mdl", true) end
LORE_OBJECTS["weapon_m6g_1"] = function() CreateProp(Vector(-724.4375,433.84375,-35.25), Angle(3.1640625,-150.2490234375,0.52734375), "models/temporalassassin/items/weapon_m6g.mdl", true);CreateProp(Vector(-593.1875,556.40625,-37.1875), Angle(-0.6591796875,16.3916015625,1.9775390625), "models/temporalassassin/items/m6g_ammo.mdl", true) end
LORE_OBJECTS["weapon_ebonyivory_1"] = function() CreateProp(Vector(-815.562,781.813,-28.687), Angle(-1.939,12.662,0.643), "models/temporalassassin/items/weapon_ebonyivory.mdl", true) end
LORE_OBJECTS["weapon_snakecharmer_1"] = function() CreateProp(Vector(-1077.219,550.031,-30.375), Angle(-14.81,-171.167,-156.665), "models/temporalassassin/items/weapon_supershotgun.mdl", true);CreateProp(Vector(-626,599.03125,-37.53125), Angle(-0.0439453125,96.591796875,-0.52734375), "models/temporalassassin/items/shotgun_ammo.mdl", true) end
LORE_OBJECTS["weapon_royaldecree_1"] = function() CreateProp(Vector(-1195.156,301.469,-37.281), Angle(-0.005,132.638,0), "models/temporalassassin/items/weapon_royaldecree.mdl", true) end
LORE_OBJECTS["weapon_gungnir_1"] = function() CreateProp(Vector(-828.594,492.906,-31.031), Angle(-3.433,-29.031,-81.123), "models/temporalassassin/items/weapon_gungnir.mdl", true) end
LORE_OBJECTS["weapon_cruentus_1"] = function() CreateProp(Vector(-1214,878.78125,-44.40625), Angle(-13.4033203125,75.498046875,74.53125), "models/temporalassassin/items/weapon_shotgun.mdl", true);CreateProp(Vector(-644.59375,600.1875,-37.65625), Angle(0.3076171875,161.279296875,-0.17578125), "models/temporalassassin/items/shotgun_ammo.mdl", true) end
LORE_OBJECTS["weapon_demonsbane_1"] = function() CreateProp(Vector(-232.469,832.594,-55.656), Angle(-6.213,169.53,-179.978), "models/temporalassassin/items/weapon_demonsbane.mdl", true) end
LORE_OBJECTS["weapon_foolsoath_1"] = function() CreateProp(Vector(-1212.156,689.875,-39.312), Angle(25.181,175.518,-88.594), "models/temporalassassin/items/weapon_foolsoath.mdl", true);CreateProp(Vector(-1211.812,786.906,-12.187), Angle(0.044,-87.451,-0.22), "models/temporalassassin/items/foolsoath_ammo.mdl", true);CreateProp(Vector(-1180.312,880.125,-36.094), Angle(-0.439,26.323,0.308), "models/temporalassassin/items/foolsoath_ammo.mdl", true) end
LORE_OBJECTS["weapon_autoshotgun_1"] = function()
EDAX_SPAWN_PROC = true
local RAND = table.Random(EDAX_POSITIONS)
G_EDAX = CreateProp(RAND[1], RAND[2], "models/temporalassassin/items/weapon_autoshotgun.mdl", true)
CreateProp(Vector(-235.375,763.25,-63.656), Angle(-0.967,-130.133,5.581), "models/temporalassassin/items/autoshotgun_ammo.mdl", true)
end
LORE_OBJECTS["weapon_gearakarme_1"] = function() CreateProp(Vector(-875.281,864.406,-45.5), Angle(12.843,58.881,-75.026), "models/temporalassassin/items/weapon_smg.mdl", true);CreateProp(Vector(-319.03125,884.6875,-29.28125), Angle(0.3076171875,-48.3837890625,1.93359375), "models/temporalassassin/items/smg_ammo.mdl", true) end
LORE_OBJECTS["weapon_omen_1"] = function() CreateProp(Vector(-1140.062,620.219,-40.406), Angle(-0.511,119.059,2.236), "models/temporalassassin/items/omen_ammo.mdl", true);CreateProp(Vector(-1079.562,631.031,-32.187), Angle(-4.796,179.357,-20.984), "models/temporalassassin/items/weapon_omen.mdl", true) end
LORE_OBJECTS["weapon_flatline_1"] = function() CreateProp(Vector(-1028,274.656,-44.437), Angle(-6.938,106.326,62.303), "models/temporalassassin/items/weapon_flatline.mdl", true);CreateProp(Vector(-1017.656,262.281,-31.406), Angle(-0.753,174.452,1.022), "models/temporalassassin/items/flatline_ammo.mdl", true) end
LORE_OBJECTS["weapon_longhorn_1"] = function() CreateProp(Vector(-723.25,766.938,-39.562), Angle(2.813,-68.467,70.752), "models/temporalassassin/items/weapon_rifle.mdl", true);CreateProp(Vector(-606.375,557.375,-37.5625), Angle(-1.0546875,10.1953125,0.791015625), "models/temporalassassin/items/rifle_ammo.mdl", true) end
LORE_OBJECTS["weapon_reikoririfle_1"] = function() CreateProp(Vector(-695.406,490.5,-7.687), Angle(0,-179.984,0), "models/props_c17/trappropeller_lever.mdl", true);CreateProp(Vector(-723.562,490.313,-9), Angle(-0.005,-179.995,0.011), "models/props_c17/trappropeller_lever.mdl", true);CreateProp(Vector(-710.594,488.25,-8.562), Angle(84.968,-19.49,-108.166), "models/temporalassassin/items/weapon_reikoririfle.mdl", true);CreateProp(Vector(-649.344,558.75,-38.344), Angle(-0.016,-134.44,0.011), "models/temporalassassin/items/reikoririfle_ammo.mdl", true) end
LORE_OBJECTS["weapon_siamesespectre_1"] = function() local SS = CreateProp(Vector(-1218.937,532.625,-33.719), Angle(11.656,91.703,-96.092), "models/temporalassassin/items/weapon_siamesespectre.mdl", true);SS:SetModelScale(1.1,0);CreateProp(Vector(-1142.469,580.125,-41.25), Angle(-0.961,-150.656,3.131), "models/temporalassassin/items/siamesespectre_ammo.mdl", true) end
LORE_OBJECTS["weapon_harbinger_1"] = function() CreateProp(Vector(-198.65625,531.65625,5.8), Angle(-72.509765625,68.37890625,-34.3212890625), "models/temporalassassin/items/weapon_harbinger.mdl", true);CreateProp(Vector(-216.281,500.031,-24.5), Angle(-0.17,149.771,0.297), "models/temporalassassin/items/harbinger_ammo.mdl", true) end
LORE_OBJECTS["weapon_lstar_1"] = function() CreateProp(Vector(-425.406,628.563,-42.594), Angle(-0.818,-89.583,-68.099), "models/temporalassassin/items/weapon_lstar.mdl", true);CreateProp(Vector(-425.656,571.406,-61.312), Angle(1.055,-34.464,0.467), "models/temporalassassin/items/lstar_ammo.mdl", true) end
LORE_OBJECTS["weapon_kingslayer_1"] = function() CreateProp(Vector(-720.78125,745.96875,-44.09375), Angle(3.427734375,-142.1630859375,75.9814453125), "models/temporalassassin/items/weapon_kingslayer.mdl", true);CreateProp(Vector(-592.125,572.28125,-37.40625), Angle(-0.3955078125,-14.7216796875,0.2197265625), "models/temporalassassin/items/kingslayer_ammo.mdl", true) end
LORE_OBJECTS["weapon_silverace_1"] = function() CreateProp(Vector(-852.094,710.469,-63.562), Angle(0.236,-176.468,0.066), "models/temporalassassin/items/silverace_ammo.mdl", true);CreateProp(Vector(-847.594,727.813,-42.75), Angle(20.369,35.349,-79.266), "models/temporalassassin/items/weapon_silverace.mdl", true) end
LORE_OBJECTS["weapon_queensdoom_1"] = function() CreateProp(Vector(-728.875,724.25,-8.6), Angle(1,13,-95), "models/temporalassassin/items/weapon_crossbow.mdl", true);CreateProp(Vector(-599.8125,509.84375,-35.9375), Angle(-87.3193359375,-104.94140625,-175.4736328125), "models/temporalassassin/items/crossbow_ammo.mdl", true);CreateProp(Vector(-736.656,733.688,5.4), Angle(0,0,0), "models/props_c17/trappropeller_lever.mdl", true);CreateProp(Vector(-723.5,733.875,5.55), Angle(0,0,0), "models/props_c17/trappropeller_lever.mdl", true) end
LORE_OBJECTS["weapon_azarath_1"] = function() CreateProp(Vector(-830.125,713.375,-41.594), Angle(0,-90,0), "models/temporalassassin/items/weapon_azarath.mdl", true);CreateProp(Vector(-831.344,711.938,-16.462), Angle(0,90,0), "models/props_c17/trappropeller_lever.mdl", true);CreateProp(Vector(-831.375,714,13.244), Angle(0,90,0), "models/props_c17/trappropeller_lever.mdl", true) end
LORE_OBJECTS["weapon_frontliner_1"] = function() CreateProp(Vector(-695.062,806.719,-14), Angle(23.555,126.87,-100.415), "models/temporalassassin/items/weapon_frontliner.mdl", true);CreateProp(Vector(-675.812,803.844,-52.281), Angle(18.984,116.279,-70.796), "models/temporalassassin/items/frontliner_ammo.mdl", true);CreateProp(Vector(-681.312,796.563,-50.75), Angle(8.042,165.278,-100.415), "models/temporalassassin/items/frontliner_ammo.mdl", true) end
LORE_OBJECTS["weapon_anthrakia_1"] = function() local ANTH = CreateProp(Vector(-587.562,489.031,-65.375), Angle(-5.257,-157.961,18.182), "models/temporalassassin/items/weapon_anthrakia.mdl", true);ANTH:SetModelScale(1.25,0) end
LORE_OBJECTS["weapon_whisper_1"] = function() CreateProp(Vector(-955.875,986.96875,28.78125), Angle(2.1533203125,3.603515625,-43.330078125), "models/temporalassassin/items/weapon_whisper.mdl", true);CreateProp(Vector(-920.125,992.344,37.344), Angle(41.484,-85.825,13.228), "models/temporalassassin/items/whisper_ammo.mdl", true) end
LORE_OBJECTS["weapon_hergift_1"] = function() CreateProp(Vector(-1186.34375,302.15625,-65.59375), Angle(-71.7626953125,-65.4345703125,-2.4169921875), "models/temporalassassin/items/hergift.mdl", true) end
LORE_OBJECTS["weapon_grenade_1"] = function() CreateProp(Vector(-1202.531,375.313,-27.25), Angle(5.449,-89.297,67.808), "models/temporalassassin/enemyweapons/grenade.mdl", true);CreateProp(Vector(-1196.656,370.594,-27.187), Angle(-29.092,124.893,8.965), "models/temporalassassin/enemyweapons/grenade.mdl", true) end
LORE_OBJECTS["weapon_falanrath_1"] = function() CreateProp(Vector(-349.34375,886.5625,-28.75), Angle(-28.30078125,109.0283203125,85.7373046875), "models/temporalassassin/items/falanrath_thorn.mdl", true) end
LORE_OBJECTS["weapon_reikori_1"] = function() CreateProp(Vector(-428.812,582.188,-42.344), Angle(8.481,141.899,-89.912), "models/temporalassassin/items/reikori_key.mdl", true) end
LORE_OBJECTS["item_backpack_1"] = function() CreateProp(Vector(-454.594,922.25,-63.219), Angle(-1.659,-91.555,-0.203), "models/temporalassassin/items/backpack.mdl", true);CreateProp(Vector(-232.531,821.094,-32.562), Angle(-89.511,167.948,26.098), "models/temporalassassin/props/cards/datharon_card.mdl", true);CreateProp(Vector(-233.719,835.844,-32.531), Angle(-89.791,-169.64,-17.661), "models/temporalassassin/props/cards/gift_card.mdl", true);CreateProp(Vector(-231.5,846.969,-32.5), Angle(-89.077,-167.162,36.425), "models/temporalassassin/props/cards/kit_card.mdl", true);CreateProp(Vector(-230,822,-7.675), Angle(0,-145.344,0), "models/temporalassassin/props/totem/totem.mdl", true) end
LORE_OBJECTS["item_corzoshat_1"] = function() CreateProp(Vector(-1008.155823,820.239502,-24.768751), Angle(0.000,-114.041,0.000), "models/temporalassassin/items/corzohat.mdl", true) end
LORE_OBJECTS["item_volatilebattery_1"] = function() CreateProp(Vector(-927.09375,390.59375,-33.6875), Angle(-0.703125,-165.673828125,-0.263671875), "models/temporalassassin/items/volatilebattery.mdl", true) end
LORE_OBJECTS["item_reinforcedcombatrig_1"] = function() CreateProp(Vector(-926.40625,465.28125,-63.3125), Angle(-1.0107421875,-174.4189453125,-0.0439453125), "models/temporalassassin/items/armour_200.mdl", true) end
LORE_OBJECTS["item_smallmanacrystal_1"] = function() CreateProp(Vector(-640.875,559.59375,-39.03125), Angle(10.6787109375,-40.078125,39.111328125), "models/temporalassassin/items/manacrystal_20.mdl", true) end
LORE_OBJECTS["item_largemanacrystal_1"] = function() CreateProp(Vector(-636.6875,495.46875,-40.46875), Angle(16.6552734375,171.8701171875,35.15625), "models/temporalassassin/items/manacrystal_100.mdl", true) end
LORE_OBJECTS["item_portablemedkit_1"] = function() CreateProp(Vector(-752.844,285.719,-10.187), Angle(89.995,-90.016,180), "models/temporalassassin/items/portable_medkit.mdl", true) end
LORE_OBJECTS["item_beer_1"] = function() CreateProp(Vector(-936.1875,984.125,-24.75), Angle(1.0546875,-8.26171875,0.615234375), "models/temporalassassin/items/beer_5.mdl", true);CreateProp(Vector(-895.75,813.53125,-24.5625), Angle(-1.8017578125,-64.3798828125,0.7470703125), "models/temporalassassin/items/beer_5.mdl", true);CreateProp(Vector(-889.34375,813,-24.53125), Angle(0.87890625,-41.484375,1.0986328125), "models/temporalassassin/items/beer_5.mdl", true) end
LORE_OBJECTS["item_turkey_1"] = function() CreateProp(Vector(-873.875,985.219,-24.375), Angle(-0.192,94.471,0.17), "models/temporalassassin/items/turkey_35.mdl", true) end
LORE_OBJECTS["item_healthinhaler_1"] = function() CreateProp(Vector(-358.65625,884.9375,-63.625), Angle(0.087890625,-120.2783203125,-2.5048828125), "models/temporalassassin/items/medkit_10.mdl", true) end
LORE_OBJECTS["item_medkit_1"] = function() CreateProp(Vector(-433.90625,924.21875,-63.375), Angle(6.240234375,-122.783203125,-0.7470703125), "models/temporalassassin/items/medkit_25.mdl", true) end
LORE_OBJECTS["item_medkit50_1"] = function() CreateProp(Vector(-233.094,624.563,-49.156), Angle(0,-87.198,80.195), "models/temporalassassin/items/medkit_50.mdl", true) end
LORE_OBJECTS["item_armourshard_1"] = function() CreateProp(Vector(-697.71875,782.34375,-34.46875), Angle(0.087890625,66.884765625,1.669921875), "models/temporalassassin/items/armour_5.mdl", true) end
LORE_OBJECTS["item_armourpieces_1"] = function() CreateProp(Vector(-701.46875,754.84375,-55.0625), Angle(5.0537109375,-17.05078125,1.40625), "models/temporalassassin/items/armour.mdl", true) end
LORE_OBJECTS["item_eldritcharmourshard_1"] = function() CreateProp(Vector(-696.031,765.813,-28.406), Angle(0.797,-12.047,-1.148), "models/temporalassassin/items/eldritcharmour_10.mdl", true) end
LORE_OBJECTS["item_eldritcharmourset_1"] = function() CreateProp(Vector(-700.844,775.813,-55), Angle(-0.357,32.003,0.06), "models/temporalassassin/items/eldritcharmour.mdl", true) end
LORE_OBJECTS["item_sinfragment_1"] = function() CreateProp(Vector(-480.781,567,-43.125), Angle(16.249,151.644,20.275), "models/temporalassassin/items/sinfragment.mdl", true);CreateProp(Vector(-483.344,563.781,-42.469), Angle(-74.18,58.129,179.555), "models/temporalassassin/items/sinfragment.mdl", true) end
LORE_OBJECTS["item_armourfragment_1"] = function() CreateProp(Vector(-467.187,626.5,-39.094), Angle(1.077,-23,-3.153), "models/temporalassassin/items/armour_1.mdl", true);CreateProp(Vector(-467.312,619.156,-39.5), Angle(0.352,-172.667,-0.731), "models/temporalassassin/items/armour_1.mdl", true) end

function Reset_Story_Stuff( len, pl )

	pl:StripWeapon("temporal_lilcosmo")

	for _,v in pairs (ents.GetAll()) do
	
		if v and IsValid(v) and v.GetClass and (v:GetClass() == "temporal_pickup" or v:GetClass() == "temporal_locker" or v:GetClass() == "temporal_trigger" or v:GetClass() == "temporal_lantern") then
			v:Remove()
		end
	
	end
	
	if GLOB_COSMICREALM then
		for _,v in pairs (GLOB_COSMICREALM) do
			if v and IsValid(v) then
				v:Remove()
			end
		end
	end
	
	if G_EGG and IsValid(G_EGG) then
		G_EGG:Remove()
	end
	
	if G_COSMO and IsValid(G_COSMO) then
		G_COSMO:Remove()
	end
	
	if G_KNIFE and IsValid(G_KNIFE) then
		G_KNIFE:Remove()
	end
	
	HUB_DOLL_MESSAGE_FORCE = false
	
	if HUB_DOLL and IsValid(HUB_DOLL) then
		HUB_DOLL:Remove()
	end
	
	if GLOB_GRAMAPHONE and IsValid(GLOB_GRAMAPHONE) then
		GLOB_GRAMAPHONE:Remove()
	end
	
	RemoveCosmicRealmEntrance("BEDROOM_DOOR")
	RemoveCosmicRealmEntrance("KITCHEN_DOOR")
	
	local WHITE_SPACE = CheckWhiteSpace()
	if WHITE_SPACE then
		pl:SetSharedFloat("HubSleep",999999999999)
		timer.Simple(0.5,function()
			if pl and IsValid(pl) then
				pl:SetSharedBool("WhiteSpace_Transition",true)
				pl:SetSharedFloat("Gift_WhiteOut",0)
				pl:Unlock_TA_LoreFile( "WHITESPACE_LEVELLOAD", "" )
			end
		end)
		file.Write("temporalassassin/lore/whitespace_oldmap.txt","ta_hub")
		file.Write("temporalassassin/lore/whitespace_slept.txt","true bay beeeeeee")
		timer.Simple(2.5,function()
			game.SetTimeScale(1)
			RunConsoleCommand("changelevel","whitespace")
		end)
	end
	
	timer.Simple(0.1,function()
		Load_Story_Stuff(pl)
	end)
	
end
net.Receive("Reset_HubSleep",Reset_Story_Stuff)

function Load_Story_Stuff(pl)

	local HAS_HL2_LORE = (pl:HasLoreFile("lore_mthuulra_1") and pl:HasLoreFile("lore_mthuulra_2") and pl:HasLoreFile("lore_mthuulra_3") and pl:HasLoreFile("lore_mthuulra_4") and pl:HasLoreFile("lore_mthuulra_5") and pl:HasLoreFile("lore_mthuulra_6"))
	local STORY_BEAT_1 = (pl:HasLoreFile("lore_malum_char1") and pl:HasLoreFile("lore_binderoutcast") and pl:HasLoreFile("lore_binderoutcast2") and pl:HasLoreFile("lore_mthuulra_char") and pl:HasLoreFile("lore_mthuulra_char2"))
	local CHALLENGES_COMPLETE = (pl:HasLoreFile("lore_mthuulra_char") and pl:HasLoreFile("lore_mthuulra_char2"))
	local WARNING_TALK = false
	
	if not pl:HasPocketWatch() then
	
		if CHALLENGES_COMPLETE or HAS_HL2_LORE then
			Create_PocketWatch( Vector(-1142.142,342.488,-62.717) )
			WARNING_TALK = true
		end
		
	end
	
	--[[ If we have the pocket watch and all challenge shit is complete, Wait 5 seconds after lore objects spawn, then have B
	tell us he's really gay and that we should go touch his butt ]]
	if (CHALLENGES_COMPLETE or HAS_HL2_LORE) and pl:HasPocketWatch() then

		if not pl:HasLoreFile("WHITESPACE_GOTFRAGMENT") then
	
			timer.Simple(5,function()
				if pl and IsValid(pl) then
					pl:Unlock_TA_LoreFile( "WHITESPACE_STARTED", "" )
					B_TALK("Kit, I'm detecting some weird aura in the dimension we use for training.")
					B_TALK("Mind taking a look for me?",2)
				end
			end)
			
		elseif pl:HasLoreFile("WHITESPACE_GOTGUN") and not pl:HasLoreFile("WHITESPACE_FINISHED") then
	
			timer.Simple(5,function()
				if pl and IsValid(pl) then
					pl:Unlock_TA_LoreFile( "WHITESPACE_FINISHED", "" )
					B_TALK("I see... You found Him. I thought I lost that thing to the void long ago.")
					B_TALK("Guess he just wanted to go soul searching. Well I suppose you can keep it then.",2)
					B_TALK("As much as I'd like to have it back, I think it'd be safe with you.",2)
					B_TALK("Just try not to indulge in gluttony too much.",2)
				end
			end)
			
		end
		
	end
	
	if pl:HasLoreFile("pet_unlocked") then
	
		G_COSMO = ents.Create("hub_pet")
		G_COSMO:SetPos( Vector(-884.771,734.102,-63.969) )
		G_COSMO:Spawn()
		G_COSMO:Activate()
		
		pl:SetAchievementProgress( "COSMO_PET", 5 )
		net.Start("Achievement_ForceSave")
		net.Send(pl)
	
	elseif not pl:HasLoreFile("pet_unlocked") and pl:HasLoreFile("pet_acquired") then
	
		G_EGG = CreateProp(Vector(-881.878174,332.734375,-61.141956), Angle(0,0.071,0), "models/temporalassassin/hub/pet_egg.mdl", true)
	
		if not pl:HasLoreFile("pet_acquired_1") then
			G_EGG:SetModelScale(0.4,0)
			pl:Unlock_TA_LoreFile( "pet_acquired_1", "" )
			pl:SetAchievementProgress( "COSMO_PET", 1 )
			net.Start("Achievement_ForceSave")
			net.Send(pl)
		elseif not pl:HasLoreFile("pet_acquired_2") then
			G_EGG:SetModelScale(0.7,0)
			pl:Unlock_TA_LoreFile( "pet_acquired_2", "" )
			pl:SetAchievementProgress( "COSMO_PET", 2 )
			net.Start("Achievement_ForceSave")
			net.Send(pl)
		elseif not pl:HasLoreFile("pet_acquired_3") then
			G_EGG:SetModelScale(0.9,0)
			pl:Unlock_TA_LoreFile( "pet_acquired_3", "" )
			pl:SetAchievementProgress( "COSMO_PET", 3 )
			net.Start("Achievement_ForceSave")
			net.Send(pl)
		elseif not pl:HasLoreFile("pet_acquired_4") then
			G_EGG:SetModelScale(1.1,0)
			pl:Unlock_TA_LoreFile( "pet_acquired_4", "" )
			pl:Unlock_TA_LoreFile( "pet_unlocked", "" )
			pl:SetAchievementProgress( "COSMO_PET", 4 )
			net.Start("Achievement_ForceSave")
			net.Send(pl)
		end
	
	end
	
	if HasAchievement("ECHO_COAT_1") and HasAchievement("SHATTER_COAT_1") and HasAchievement("SHATTER_COAT_2") and HasAchievement("JESTERS_COAT_1") and HasAchievement("JESTERS_COAT_2") then
	
		for _,v in pairs (ents.FindByClass("prop_physics")) do
		
			if v and IsValid(v) then
			
				if v:GetModel() == "models/props_urban/plastic_icechest_lid001.mdl" then
					v:Remove()
				end
				
				if v:GetModel() == "models/props_urban/plastic_icechest001.mdl" then
					v:Remove()
				end
				
			end
			
		end
		
		GLOB_GRAMAPHONE = CreateProp(Vector(-588.875,624.125,-30), Angle(0,176.067,0), "models/temporalassassin/props/gramaphone.mdl", true)
	
	end
	
	if pl:HasLoreFile("lore_dagonspotted") then
	
		if pl:HasLoreFile("abyss_1") then
		
			if pl:HasLoreFile("mirror_complete") then
			
				GLOB_COSMICREALM = {
				CreateCosmicRealm( Vector(-1112.38,354.684,-63.969), Vector(-124.727,-124.727,0), Vector(124.727,124.727,127.938) ),
				CreateCosmicRealm( Vector(-1041.024,353.092,-63.969), Vector(-128.577,-128.577,0), Vector(128.577,128.577,125.571) )}
				
				CreateCosmicRealmEntrance( Vector(-1001.025,481.452,-63.969), Angle(0,90,0), "BEDROOM_DOOR", 0 )
				
			else
			
				GLOB_COSMICREALM = {
				CreateCosmicRealm( Vector(-1660.324,492.394,-208.969), Vector(-743.611,-743.611,0), Vector(743.611,743.611,588.292) ),
				CreateCosmicRealm( Vector(-1181.562,842.836,-63.969), Vector(-345.592,-345.592,0), Vector(345.592,345.592,263.95) )}
				
				CreateCosmicRealmEntrance( Vector(-836.525,616.118,-63.969), Angle(0,-180,0), "KITCHEN_DOOR", 1 )
				
			end
		
		else
	
			GLOB_COSMICREALM = {
			CreateCosmicRealm( Vector(-1112.38,354.684,-63.969), Vector(-124.727,-124.727,0), Vector(124.727,124.727,127.938) ),
			CreateCosmicRealm( Vector(-1041.024,353.092,-63.969), Vector(-128.577,-128.577,0), Vector(128.577,128.577,125.571) )}
			
			CreateCosmicRealmEntrance( Vector(-1001.025,481.452,-63.969), Angle(0,90,0), "BEDROOM_DOOR", 0 )
			
		end
		
		local MALUM_REACTION_REALM = function( self, ply )
		
			if not ply:HasLoreFile("lore_malum_reactrealm") then
			
				ply:Unlock_TA_LoreFile( "lore_malum_reactrealm", "Ohno" )
				
				timer.Simple(2,function()
					net.Start("MalumMessage")
					net.WriteString("I don't like this, I can feel there's a presence here.")
					net.Broadcast()
					net.Start("MalumMessage")
					net.WriteString("You feel it too... Right?")
					net.Broadcast()
				end)
				
			end
		
		end
		
		CreateTrigger( Vector(-1004.937,433.227,-63.969), Vector(-45.763,-45.763,0), Vector(45.763,45.763,125.851), MALUM_REACTION_REALM )
	
	end
	
	--[[ If the player has all of the challenge map stuff complete, and they have malums dagger, we can advance the plot ]]
	if STORY_BEAT_1 then
	
		HUB_DOLL_MESSAGE_FORCE = true
		HUB_DOLL = CreateProp(Vector(-1189.687,302.188,-26.406), Angle(7.245,-95.762,-101.426), "models/props_borealis/door_wheel001a.mdl", true)
		HUB_DOLL:SetNoDraw(true)
		
		local HAS_WOLF_COAT = false
			
		if file.Exists("TemporalAssassin/Achievements/WOLF_COAT_1.txt","DATA") and file.Exists("TemporalAssassin/Achievements/WOLF_COAT_2.txt","DATA") then
			HAS_WOLF_COAT = true
		end
		
		if not pl:HasLoreFile("weapon_outcast_1") then
		
			local FUNC = function( self, ply )
	
				if IsValid(ply) then

					if not ply:Alive() then return end
					
					CreateClothesLocker( Vector(-883.95318603516,370.75631713867,-63.96875), Angle(0,0.072494506835938,0) )
					
					local AMMO = "outcast_ammo"
					local MAX = 12
					local GIVE_AMOUNT = 6
					local TXT = "..."
					local SND = Sound("TemporalAssassin/Weapons/Outcast/Get_Weapon.wav")	
				
					if ply.ITEM_BACKPACK then
						MAX = MAX*2
					end
					
					ply:Give("temporal_outcast")
					ply:GiveAmmo2( GIVE_AMOUNT, AMMO )
					ply:SetSharedFloat("Item_ScreenFlash",CurTime() + 0.15)
					ply:EmitSound( SND, 80, math.random(98,102), 0.7, CHAN_BODY )
					hook.Call("TA_ItemPickup",false,ply,self)
					self:Remove()
					STATS.AddStat( "Item_Pickups", ply, 1 )
					
					RunConsoleCommand("TA_DEV_Forced_CobaltStorm",0)
					RunConsoleCommand("TA_DEV_Forced_Outcast",1)
					ply:Unlock_TA_LoreFile( "weapon_outcast_1", "The Outcast is in your hands now, Take care of it." )
					
					ply:SetAchievementProgress( "DIFFERENT_KINDA_GIFT", 1 )
					net.Start("Achievement_ForceSave")
					net.Send(ply)
					
					timer.Simple(2,function()
						net.Start("MalumMessage")
						net.WriteString("I haven't seen this thing in ages...")
						net.Broadcast()
						
						net.Start("MalumMessage2")
						net.WriteString("She used it a very long time ago when we started working together.")
						net.Broadcast()
						
						net.Start("MalumMessage2")
						net.WriteString("It's her reminder of better times, but i suppose she's giving it to you now.")
						net.Broadcast()
						
						net.Start("MalumMessage2")
						net.WriteString("Please take care of it Kid, for her to give something like this to someone?")
						net.Broadcast()
						
						net.Start("MalumMessage2")
						net.WriteString("I've never seen it, let alone with my own eyes.")
						net.Broadcast()
					end)
					
				end
			
			end
			
			local OC = CreateItemPickup( Vector(-382.49, 902.099, -41.298), Angle(0, 263.628, 0), "models/temporalassassin/items/weapon_outcast.mdl", FUNC )
			OC.Custom_Override = true
			
			WARNING_TALK = true
			
		elseif not pl:HasLoreFile("trinket_eyeofchari") and HAS_WOLF_COAT then
		
			--[[ We can assume the player has a simple simple idea of who c'hari is if they have the wolf coat ]]
			if HAS_WOLF_COAT then
			
				local CHARI_TEXT = {
				"Thank you for accepting my Gift, Cub.",
				"I know that the Child of the Stars and Sea can be very straight forward.",
				"Do not blame her, take it from an old one like myself.",
				"You are part of her family, by blood or otherwise.",
				"I hope you find use of this trinket, Be it a reminder that i shall help.",
				"After the feats you have achieved i believe that you deserve it."}
				
				local FUNC = function(self,ply)

					if not ply:Alive() then return end
					
					local SND = Sound("TemporalAssassin/Items/Eye_Of_Chari.wav")	
				
					ply:SetSharedFloat("Item_ScreenFlash",CurTime() + 0.15)
					ply:EmitSound( SND, 80, math.random(98,102), 0.7, CHAN_BODY )
					hook.Call("TA_ItemPickup",false,ply,self)
					self:Remove()
					STATS.AddStat( "Item_Pickups", ply, 1 )
					
					RunConsoleCommand("TA_DEV_Forced_CobaltStorm",0)
					RunConsoleCommand("TA_DEV_Forced_Outcast",1)
					ply:Unlock_TA_LoreFile( "trinket_eyeofchari", "" )
					
					ply:SetAchievementProgress( "CHARIS_GUIDANCE", 1 )
					net.Start("Achievement_ForceSave")
					net.Send(ply)
				
					self:Remove()
					
					timer.Simple(1,function()
						for loop = 1,6 do
							if loop >= 2 then
								net.Start("ChariMessage2")
							else
								net.Start("ChariMessage")
							end
							net.WriteString(CHARI_TEXT[loop])
							net.Send(ply)
						end
					end)
				
				end
				
				CreateItemPickup( Vector(-916.082,815.785,-24.969), Angle(0,-92.977,0), "models/temporalassassin/items/eye_of_chari.mdl", FUNC )
			
				WARNING_TALK = true
				
			end
			
			CreateClothesLocker( Vector(-883.95318603516,370.75631713867,-63.96875), Angle(0,0.072494506835938,0) )
		
		elseif pl:HasLoreFile("weapon_outcast_1") and not pl:HasLoreFile("abyss_1") then
		
			local ABYSS_WISP = function()
			
				net.Start("HUB_CreateAbyssWisp")
				local PL = player.GetAll()[1]
				net.Broadcast(PL)
			
			end
			
			WARNING_TALK = true
			
			CreateTrigger( Vector(-899.306,655.417,-63.969), Vector(-258.863,-258.863,0), Vector(258.863,258.863,127.938), ABYSS_WISP )
			
			CreateClothesLocker( Vector(-883.95318603516,370.75631713867,-63.96875), Angle(0,0.072494506835938,0) )
		
		elseif pl:HasLoreFile("weapon_outcast_1") and pl:HasLoreFile("abyss_1") and not pl:HasLoreFile("weapon_royaldecree_1") then
		
			local LORE_BOOK = CreateProp(Vector(-952.219,786.875,-28.687), Angle(-0.22,-5.142,-0.132), "models/temporalassassin/items/eldritch_lore.mdl", true)
			
			local FUNC = function( self, ply )
	
				if IsValid(ply) then

					if not ply:Alive() then return end
					
					CreateClothesLocker( Vector(-883.95318603516,370.75631713867,-63.96875), Angle(0,0.072494506835938,0) )
					
					local AMMO = "cruentus_ammo"
					local MAX = 40
					local GIVE_AMOUNT = 8
					local TXT = "..."
					local SND = Sound("TemporalAssassin/Weapons/RoyalDecree/Pickup_Weapon.wav")	
				
					if ply.ITEM_BACKPACK then
						MAX = MAX*2
					end
					
					ply:Give("temporal_royaldecree")
					ply:GiveAmmo2( GIVE_AMOUNT, AMMO )
					ply:SetSharedFloat("Item_ScreenFlash",CurTime() + 0.15)
					ply:EmitSound( SND, 80, math.random(98,102), 0.7, CHAN_BODY )
					hook.Call("TA_ItemPickup",false,ply,self)
					self:Remove()
					STATS.AddStat( "Item_Pickups", ply, 1 )
					
					RunConsoleCommand("TA_DEV_Forced_RoyalDecree",1)
					RunConsoleCommand("TA_DEV_Forced_Gungnir",0)
					ply:Unlock_TA_LoreFile( "weapon_royaldecree_1", "A Shotgun...?" )
					ply:Unlock_TA_LoreFile( "lore_malum_abyss", "There's a note next to it, seems to be from Malum." )
					
					ply:SetAchievementProgress( "MALUM_ABYSS", 1 )
					net.Start("Achievement_ForceSave")
					net.Send(ply)
					
					if LORE_BOOK and IsValid(LORE_BOOK) then
						LORE_BOOK:Remove()
					end
					
					timer.Simple(2,function()
						B_TALK("Oh? Fox, I see you found Malum's little gift.")
						B_TALK("I'd suggest reading it up whenever you can, I assume it's important.",2)
						B_TALK("Though I'm surprised you're not shaking or muttering insane phrases.",2)
						B_TALK("Especially with what you came into contact with in that dark place.",2)
						B_TALK("I suppose you've gotten resistant to all this after all..",2)
						B_TALK("Well either way, it seems Malum's off for a bit, I'm sure he'll be fine.",2)
					end)
					
				end
			
			end
			
			local OC = CreateItemPickup( Vector(-926.12, 814.521, -24.969), Angle(0, 270.209, 0), "models/temporalassassin/items/weapon_royaldecree.mdl", FUNC )
			OC.Custom_Override = true
			
			WARNING_TALK = true
		
		else
		
			CreateClothesLocker( Vector(-883.95318603516,370.75631713867,-63.96875), Angle(0,0.072494506835938,0) )
			
		end
		
	elseif pl:HasLoreFile("lore_malum_char1") and pl:HasLoreFile("lore_binderoutcast") and not pl:HasLoreFile("lore_binderoutcast2") then
		G_KNIFE = CreateProp(Vector(-1189.687,302.188,-26.406), Angle(7.245,-95.762,-101.426), "models/temporalassassin/props/malum_knife.mdl", true)
		G_KNIFE:SetModelScale(1.3,0)
		WARNING_TALK = true
		CreateClothesLocker( Vector(-883.95318603516,370.75631713867,-63.96875), Angle(0,0.072494506835938,0) )
	elseif not pl:HasLoreFile("lore_malum_char1") and STATS.GetStat("Levels_Completed",pl) >= 3 then
		G_KNIFE = CreateProp(Vector(-395.25,901.25,-32.781), Angle(-2.758,-178.715,-92.67), "models/temporalassassin/props/malum_knife.mdl", true)
		G_KNIFE:SetModelScale(1.3,0)
		WARNING_TALK = true
		CreateClothesLocker( Vector(-883.95318603516,370.75631713867,-63.96875), Angle(0,0.072494506835938,0) )
		HUB_DOLL = CreateProp(Vector(-1197.84375,294.75,-36.84375), Angle(-0.263671875,12.8759765625,0.52734375), "models/temporalassassin/doll/unknown.mdl", true)
	else
		CreateClothesLocker( Vector(-883.95318603516,370.75631713867,-63.96875), Angle(0,0.072494506835938,0) )
		HUB_DOLL = CreateProp(Vector(-1197.84375,294.75,-36.84375), Angle(-0.263671875,12.8759765625,0.52734375), "models/temporalassassin/doll/unknown.mdl", true)
	end
	
	if pl:HasLoreFile("pet_unlocked") and pl:HasLoreFile("item_mthuulrapocketwatch_1") and pl:HasLoreFile("WHITESPACE_FINISHED") and pl:HasLoreFile("weapon_outcast_1") and pl:HasLoreFile("abyss_1") and pl:HasLoreFile("weapon_royaldecree_1") and pl:HasLoreFile("trinket_eyeofchari") and not pl:HasLoreFile("mirror_complete") then
		pl:SetSharedBool("MIRROR_HUB",true)
		WARNING_TALK = true
	end
	
	SetupLantern( Vector(-632.73,449.71,-63.969), Angle(0,59.225,0), "HUB_LANTERN", "My Apartment", Vector(-581.951,435.889,-63.969), false, false, true, true, true )
	
	if STATS.GetStat("Levels_Completed",pl) >= 20 then
	
		SetupLantern( Vector(3357.031,-1127.969,-856.969), Angle(0,0,0), "HUB_GOTO_VANIYA", "Vaniya", Vector(8810.969,-1127.969,-856.969), true, false, true, true, true )
	
		local VANIYA_TRIGGER = function(self,ply)
			RunConsoleCommand("changelevel","tas_vaniya")
		end
		CreateTrigger( Vector(8810.969,-1127.969,-856.969), Vector(-32,-32,-32), Vector(32,32,32), VANIYA_TRIGGER )
		
		local B_REMINDER = function(self,ply)
		
			if not ply:HasLoreFile("vaniya_reminder") and not ply:HasLoreFile("REITER_UNLOCK") then
				ply:Unlock_TA_LoreFile( "vaniya_reminder", "" )
				B_TALK("Fox, I was given a special request that I think you'll fit nicely.")
				B_TALK("If you're interested just go up to the lantern in your hallway and think real hard.",2)
			end
			
		end
		CreateTrigger( Vector(-398.548,361.944,-63.969), Vector(-398.548,361.944,-63.969), Vector(-201.643,478.572,63.969), B_REMINDER, false, Angle(0,0,0) )
		
	end
	
	if EDAX_SPAWN_PROC then
	
		if G_EDAX and IsValid(G_EDAX) then
			G_EDAX:Remove()
			G_EDAX = nil
		end
		
		local RAND = table.Random(EDAX_POSITIONS)
		G_EDAX = CreateProp(RAND[1], RAND[2], "models/temporalassassin/items/weapon_autoshotgun.mdl", true)
	
	end
	
	if WARNING_TALK then
		KIT_TALK("Something feels different, I should look around.")
	end

end

function HUB_LoreObjects_LIB( len, pl )

	if LORE_OBJ_SPAWNED then return end
	
	local OBJS = net.ReadTable()
	
	local SF = CreateProp(Vector(-602.219,523.75,-3.5), Angle(1.461,-64.655,-26.702), "models/temporalassassin/props/superfly.mdl", true)
	SF:SetModelScale(0.5,0)
	SF:SetCollisionGroup( COLLISION_GROUP_WORLD )
	
	for _,v in pairs (OBJS) do
	
		if LORE_OBJECTS and LORE_OBJECTS[v] then
			local FUNC = LORE_OBJECTS[v]
			
			timer.Simple(1 + (_*0.1),function()
				FUNC()
			end)
		end
	
	end
	
	pl:SetSharedFloat("TIME_YOU_PLAYED", STATS.GetStat("TimePlayed",pl))
	
	Load_Story_Stuff(pl)
	
	LORE_OBJ_SPAWNED = true

end
net.Receive("HUB_LoreObjects",HUB_LoreObjects_LIB)

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	if file.Exists("temporalassassin/lore/whitespace_slept.txt","DATA") then
		CreatePlayerSpawn(Vector(-1013.453,323.778,-63.969),Angle(0,89,0))
		file.Delete("temporalassassin/lore/whitespace_slept.txt","DATA")
	else
		CreatePlayerSpawn(Vector(-287.74142456055,511.68069458008,-63.96875),Angle(0,90,0))
	end

	CreateProp(Vector(-702.594,747.688,7.75), Angle(10.107,-27.378,-86.265), "models/temporalassassin/items/phial.mdl", true)
	
	HUB_SIDEDOOR = GetObjectFromTrace( Vector(-235.005,418.981,6.684), Vector(-182.527,420.263,7.518), MASK_SHOT )
	HUB_SINDOOR = GetObjectFromTrace( Vector(-421.133,385.699,6.437), Vector(-422.17,325.933,11.634), MASK_SHOT )
	HUB_CHRONODOOR = GetObjectFromTrace( Vector(-644.7,374.528,-7.969), Vector(-646.468,322.108,-10.266), MASK_SHOT )
	
	for _,v in pairs (ents.GetAll()) do
	
		if IsValid(v) then
		
			if v.GetModel then
			
				if v:GetModel() == "models/nt/props_office/officechair.mdl" then
					v:Remove()
				end
				
				if v:GetModel() == "models/nt/props_office/officechair02.mdl" then
					v:Remove()
				end
				
			end
			
			if v:IsProp() then
			
				if v.SetHealth then
					v:SetHealth(5000000)
				end

				if IsValid(v:GetPhysicsObject()) then
					v:GetPhysicsObject():EnableMotion(false)
					v:GetPhysicsObject():AddGameFlag(FVPHYSICS_CONSTRAINT_STATIC)
				end
				
			end
			
		end
		
	end
	
	CreateCampaignSelector( Vector(-536.646484375,289.46243286133,-63.778602600098), Vector(-48.093795776367,-48.093795776367,0), Vector(48.093795776367,48.093795776367,96.239700317383) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

local MISSING_MTHUULRA_MSGS = {
{"BMessage","Not a single trace from what I can see. She doesn't want to be found, Fox."},
{"BMessage","It's different without Her presence looming over us. I'll have to keep looking."},
{"BMessage","We'll have to look out for you now, Though I have a feeling she's not forgotten."},
{"TwinsMessage","Wait, Mthuulra's gone...? And without telling you anything, Kit?"},
{"TwinsMessage",".. She's been gone for a while. It's... Weird, without her around."},
{"TwinsMessage","... We're here if you need someone to talk to, Wilde. We're worried for you."},
{"ChariMessage","Your Mother has gone missing? I shall keep my eyes open, Cub."},
{"ChariMessage","Stand fast Cub, we shall find her together."},
{"GriseusMessage","Uh... Mthuulra? Where... Where did she go? Kit?"},
{"GriseusMessage","This is... eheheh... erh... unusual... She'd let us know, right? Right?"},
{"GriseusMessage","I'm... feeling both relieved but also very much nervous. Why?"},
{"GriseusMessage","I... oh no... I hope that isn't the result of my less, legitimate dealings."},
{"GriseusMessage","Jesus Christ, I hope to god I haven't been messing with pears too much."},
{"GriseusMessageP","So much for keeping the family together, huh, Wilde?"},
{"GriseusMessageP","I knew it was a matter of time until she abandoned you. Again."},
{"GriseusMessageP","She's gone? How many parents have you lost by now?"},
{"AkaneMessage","I need the Her word on this new prototype, do you know where she is?"},
{"AkaneMessage","It is becoming increasingly harder to maintain order in the Agency without her guidance."},
{"CorzoMessage","Kind of strange without her here isn't it."},
{"CorzoMessage","I don't know where she's gone, she gives me all the wrong kinds of vibes."},
{"CorzoMessage","She'll be back Kid, s'call it a feeling y'hear?"},
{"AgencyMessage","Hey Kit, Zio here, know where she went? I got a handful of sin to fix here.",{250,250,250,255},{50,50,50,255}},
{"AgencyMessage","Good Morning Kit, everything okay today?",{50,255,50,255},{255,0,0,255}},
{"MalumMessage","She'll be back Kid, I'm worried too y'know."},
{"MalumMessage","Just stay focused on you, kid. She'll figure it out..."},
{"MalumMessage","Oh Stardust, not again..."},
{"BMessage","Not a single trace from what I can see. She doesn't want to be found, Fox."},
{"BMessage","It's different without Her presence looming over us. I'll have to keep looking."},
{"BMessage","We'll have to look out for you now, Though I have a feeling she's not forgotten."},
{"TwinsMessage","Wait, Mthuulra's gone...? And without telling you anything, Kit?"},
{"TwinsMessage",".. She's been gone for a while. It's... Weird, without her around."},
{"TwinsMessage","... We're here if you need someone to talk to, Wilde. We're worried for you."},
{"ChariMessage","Your Mother has gone missing? I shall keep my eyes open, Cub."},
{"ChariMessage","Stand fast Cub, we shall find her together."},
{"GriseusMessage","Uh... Mthuulra? Where... Where did she go? Kit?"},
{"GriseusMessage","This is... eheheh... erh... unusual... She'd let us know, right? Right?"},
{"GriseusMessage","I'm... feeling both relieved but also very much nervous. Why?"},
{"GriseusMessage","I... oh no... I hope that isn't the result of my less, legitimate dealings."},
{"GriseusMessage","Jesus Christ, I hope to god I haven't been messing with pears too much."},
{"GriseusMessageP","So much for keeping the family together, huh, Wilde?"},
{"GriseusMessageP","I knew it was a matter of time until she abandoned you. Again."},
{"GriseusMessageP","She's gone? How many parents have you lost by now?"},
{"AkaneMessage","I need the Her word on this new prototype, do you know where she is?"},
{"AkaneMessage","It is becoming increasingly harder to maintain order in the Agency without her guidance."},
{"CorzoMessage","Kind of strange without her here isn't it."},
{"CorzoMessage","I don't know where she's gone, she gives me all the wrong kinds of vibes."},
{"CorzoMessage","She'll be back Kid, s'call it a feeling y'hear?"},
{"AgencyMessage","Hey Kit, Zio here, know where she went? I got a handful of sin to fix here.",{250,250,250,255},{50,50,50,255}},
{"AgencyMessage","Good Morning Kit, everything okay today?",{50,255,50,255},{255,0,0,255}},
{"MalumMessage","She'll be back Kid, I'm worried too y'know."},
{"MalumMessage","Just stay focused on you, kid. She'll figure it out..."},
{"MalumMessage","Oh Stardust, not again..."},
{"BMessage","Not a single trace from what I can see. She doesn't want to be found, Fox."},
{"BMessage","It's different without Her presence looming over us. I'll have to keep looking."},
{"BMessage","We'll have to look out for you now, Though I have a feeling she's not forgotten."},
{"TwinsMessage","Wait, Mthuulra's gone...? And without telling you anything, Kit?"},
{"TwinsMessage",".. She's been gone for a while. It's... Weird, without her around."},
{"TwinsMessage","... We're here if you need someone to talk to, Wilde. We're worried for you."},
{"ChariMessage","Your Mother has gone missing? I shall keep my eyes open, Cub."},
{"ChariMessage","Stand fast Cub, we shall find her together."},
{"GriseusMessage","Uh... Mthuulra? Where... Where did she go? Kit?"},
{"GriseusMessage","This is... eheheh... erh... unusual... She'd let us know, right? Right?"},
{"GriseusMessage","I'm... feeling both relieved but also very much nervous. Why?"},
{"GriseusMessage","I... oh no... I hope that isn't the result of my less, legitimate dealings."},
{"GriseusMessage","Jesus Christ, I hope to god I haven't been messing with pears too much."},
{"GriseusMessageP","So much for keeping the family together, huh, Wilde?"},
{"GriseusMessageP","I knew it was a matter of time until she abandoned you. Again."},
{"GriseusMessageP","She's gone? How many parents have you lost by now?"},
{"AkaneMessage","I need the Her word on this new prototype, do you know where she is?"},
{"AkaneMessage","It is becoming increasingly harder to maintain order in the Agency without her guidance."},
{"CorzoMessage","Kind of strange without her here isn't it."},
{"CorzoMessage","I don't know where she's gone, she gives me all the wrong kinds of vibes."},
{"CorzoMessage","She'll be back Kid, s'call it a feeling y'hear?"},
{"AgencyMessage","Hey Kit, Zio here, know where she went? I got a handful of sin to fix here.",{250,250,250,255},{50,50,50,255}},
{"AgencyMessage","Good Morning Kit, everything okay today?",{50,255,50,255},{255,0,0,255}},
{"MalumMessage","She'll be back Kid, I'm worried too y'know."},
{"MalumMessage","Just stay focused on you, kid. She'll figure it out..."},
{"MalumMessage","Oh Stardust, not again..."},
{"MalumMessage","Stardust..."}}

local MISSING_MTHUULRA_MSGS2 = {
{"BMessage","Not a single trace from what I can see. She doesn't want to be found, Fox."},
{"BMessage","It's different without Her presence looming over us. I'll have to keep looking."},
{"BMessage","We'll have to look out for you now, Though I have a feeling she's not forgotten."},
{"BMessage","I got a bad feeling Fox."},
{"TwinsMessage","Wait, Mthuulra's gone...? And without telling you anything, Kit?"},
{"TwinsMessage",".. She's been gone for a while. It's... Weird, without her around."},
{"TwinsMessage","... We're here if you need someone to talk to, Wilde. We're worried for you."},
{"ChariMessage","Your Mother has gone missing? I shall keep my eyes open, Cub."},
{"ChariMessage","Stand fast Cub, we shall find her together."},
{"ChariMessage","You felt that too yes, Cub?"},
{"GriseusMessage","Uh... Mthuulra? Where... Where did she go? Kit?"},
{"GriseusMessage","This is... eheheh... erh... unusual... She'd let us know, right? Right?"},
{"GriseusMessage","I'm... feeling both relieved but also very much nervous. Why?"},
{"GriseusMessage","I... oh no... I hope that isn't the result of my less, legitimate dealings."},
{"GriseusMessage","Jesus Christ, I hope to god I haven't been messing with pears too much."},
{"GriseusMessageP","So much for keeping the family together, huh, Wilde?"},
{"GriseusMessageP","I knew it was a matter of time until she abandoned you. Again."},
{"GriseusMessageP","She's gone? How many parents have you lost by now?"},
{"AkaneMessage","I need the Her word on this new prototype, do you know where she is?"},
{"AkaneMessage","It is becoming increasingly harder to maintain order in the Agency without her guidance."},
{"CorzoMessage","Kind of strange without her here isn't it."},
{"CorzoMessage","I don't know where she's gone, she gives me all the wrong kinds of vibes."},
{"CorzoMessage","She'll be back Kid, s'call it a feeling y'hear?"},
{"CorzoMessage","Trouble is afoot Kid, i can feel it comin'."},
{"AgencyMessage","Hey Kit, Zio here, know where she went? I got a handful of sin to fix here.",{250,250,250,255},{50,50,50,255}},
{"AgencyMessage","Good Morning Kit, everything okay today?",{50,255,50,255},{255,0,0,255}},
{"MalumMessage","She'll be back Kid, I'm worried too y'know."},
{"MalumMessage","Just stay focused on you, kid. She'll figure it out..."},
{"MalumMessage","Oh Stardust, not again..."},
{"MalumMessage","So you've seen the visions too, huh kid? Haunting isn't it..."},
{"BMessage","Not a single trace from what I can see. She doesn't want to be found, Fox."},
{"BMessage","It's different without Her presence looming over us. I'll have to keep looking."},
{"BMessage","We'll have to look out for you now, Though I have a feeling she's not forgotten."},
{"BMessage","I got a bad feeling Fox."},
{"TwinsMessage","Wait, Mthuulra's gone...? And without telling you anything, Kit?"},
{"TwinsMessage",".. She's been gone for a while. It's... Weird, without her around."},
{"TwinsMessage","... We're here if you need someone to talk to, Wilde. We're worried for you."},
{"ChariMessage","Your Mother has gone missing? I shall keep my eyes open, Cub."},
{"ChariMessage","Stand fast Cub, we shall find her together."},
{"ChariMessage","You felt that too yes, Cub?"},
{"GriseusMessage","Uh... Mthuulra? Where... Where did she go? Kit?"},
{"GriseusMessage","This is... eheheh... erh... unusual... She'd let us know, right? Right?"},
{"GriseusMessage","I'm... feeling both relieved but also very much nervous. Why?"},
{"GriseusMessage","I... oh no... I hope that isn't the result of my less, legitimate dealings."},
{"GriseusMessage","Jesus Christ, I hope to god I haven't been messing with pears too much."},
{"GriseusMessageP","So much for keeping the family together, huh, Wilde?"},
{"GriseusMessageP","I knew it was a matter of time until she abandoned you. Again."},
{"GriseusMessageP","She's gone? How many parents have you lost by now?"},
{"AkaneMessage","I need the Her word on this new prototype, do you know where she is?"},
{"AkaneMessage","It is becoming increasingly harder to maintain order in the Agency without her guidance."},
{"CorzoMessage","Kind of strange without her here isn't it."},
{"CorzoMessage","I don't know where she's gone, she gives me all the wrong kinds of vibes."},
{"CorzoMessage","She'll be back Kid, s'call it a feeling y'hear?"},
{"CorzoMessage","Trouble is afoot Kid, i can feel it comin'."},
{"AgencyMessage","Hey Kit, Zio here, know where she went? I got a handful of sin to fix here.",{250,250,250,255},{50,50,50,255}},
{"AgencyMessage","Good Morning Kit, everything okay today?",{50,255,50,255},{255,0,0,255}},
{"MalumMessage","She'll be back Kid, I'm worried too y'know."},
{"MalumMessage","Just stay focused on you, kid. She'll figure it out..."},
{"MalumMessage","Oh Stardust, not again..."},
{"MalumMessage","So you've seen the visions too, huh kid? Haunting isn't it..."},
{"BMessage","Not a single trace from what I can see. She doesn't want to be found, Fox."},
{"BMessage","It's different without Her presence looming over us. I'll have to keep looking."},
{"BMessage","We'll have to look out for you now, Though I have a feeling she's not forgotten."},
{"BMessage","I got a bad feeling Fox."},
{"TwinsMessage","Wait, Mthuulra's gone...? And without telling you anything, Kit?"},
{"TwinsMessage",".. She's been gone for a while. It's... Weird, without her around."},
{"TwinsMessage","... We're here if you need someone to talk to, Wilde. We're worried for you."},
{"ChariMessage","Your Mother has gone missing? I shall keep my eyes open, Cub."},
{"ChariMessage","Stand fast Cub, we shall find her together."},
{"ChariMessage","You felt that too yes, Cub?"},
{"GriseusMessage","Uh... Mthuulra? Where... Where did she go? Kit?"},
{"GriseusMessage","This is... eheheh... erh... unusual... She'd let us know, right? Right?"},
{"GriseusMessage","I'm... feeling both relieved but also very much nervous. Why?"},
{"GriseusMessage","I... oh no... I hope that isn't the result of my less, legitimate dealings."},
{"GriseusMessage","Jesus Christ, I hope to god I haven't been messing with pears too much."},
{"GriseusMessageP","So much for keeping the family together, huh, Wilde?"},
{"GriseusMessageP","I knew it was a matter of time until she abandoned you. Again."},
{"GriseusMessageP","She's gone? How many parents have you lost by now?"},
{"AkaneMessage","I need the Her word on this new prototype, do you know where she is?"},
{"AkaneMessage","It is becoming increasingly harder to maintain order in the Agency without her guidance."},
{"CorzoMessage","Kind of strange without her here isn't it."},
{"CorzoMessage","I don't know where she's gone, she gives me all the wrong kinds of vibes."},
{"CorzoMessage","She'll be back Kid, s'call it a feeling y'hear?"},
{"CorzoMessage","Trouble is afoot Kid, i can feel it comin'."},
{"AgencyMessage","Hey Kit, Zio here, know where she went? I got a handful of sin to fix here.",{250,250,250,255},{50,50,50,255}},
{"AgencyMessage","Good Morning Kit, everything okay today?",{50,255,50,255},{255,0,0,255}},
{"MalumMessage","She'll be back Kid, I'm worried too y'know."},
{"MalumMessage","Just stay focused on you, kid. She'll figure it out..."},
{"MalumMessage","Oh Stardust, not again..."},
{"MalumMessage","So you've seen the visions too, huh kid? Haunting isn't it..."},
{"MalumMessage","Stardust..."}}

local MISSING_MTHUULRA_MSGS3 = {
{"BMessage","Malums working on it Fox, take a load off for a while."},
{"BMessage","We'll just have to wait for Malum to figure something out."},
{"BMessage","I have a feeling Malum will find her, even if he has to beat his inner demons."},
{"ChariMessage","The Shadow Binder himself is looking for Her, not long now, Cub."},
{"ChariMessage","He will find her, part of him is as you say, part of her."},
{"TwinsMessage","We haven't heard from Malum for a while, is he doing okay?"},
{"TwinsMessageA","I have not seen Malum for a bit, he's managing right?"},
{"MalumMessage","I'm still figuring it out, this may take a while kid..."},
{"MalumMessage","Try not to think about what you saw in there, okay?"},
{"SeamstressMessage","B' has informed me of what happened, I do hope that Malum will be okay."},
{"SeamstressMessage","If you see Malum at all please let him know that I am always available to talk."},
{"SeamstressMessage","Try not to worry Dear, Malum can do this, we just have to put our faith in him."},
{"SeamstressMessage","He will find Her, Dear. I am a little more concerned on how you are feeling."},
{"LeviathanMessage","he may search. for decades to come."},
{"LeviathanMessage","dagon may return. if the stars align."},
{"LeviathanMessage","i fought to protect her. now it seems. it is his turn."},
{"LeviathanMessage","elders collapse. it still hurts. my screams are silent."},
{"LeviathanMessage","i am a part of you. it may be that one day. you will learn new things."}}

local DOLL_MSGS5_DIFFICULTY = {}
DOLL_MSGS5_DIFFICULTY[1] = {
"Are you playing on Basement Dweller P_NAMER? What a shame.",
"Basement Dweller, Really P_NAMER?",
"The difficulty you've selected matches you P_NAMER, i should know."}
DOLL_MSGS5_DIFFICULTY[2] = {
"Hometown Hero huh, You must be new P_NAMER.",
"Hero of your Hometown, that's you right P_NAMER?"}
DOLL_MSGS5_DIFFICULTY[3] = {
"Coast to Coast, You a traveller P_NAMER?",
"I hope the water isn't too deep for you P_NAMER."}
DOLL_MSGS5_DIFFICULTY[4] = {
"Trotting around the globe, Big boots you have there P_NAMER.",
"Globe Trotting is rather dangerous y'know P_NAMER."}
DOLL_MSGS5_DIFFICULTY[5] = {
"Queen of the Galaxy? Try not to take my job P_NAMER.",
"Space Travel isn't as easy as you might think P_NAMER.",
"Got a rocket ship for where you're going, P_NAMER?"}
DOLL_MSGS5_DIFFICULTY[6] = {
"Breaking Time and Space might get you killed P_NAMER, Leave it to the professionals.",
"If you break time and space too much you're gonna have to pay for it P_NAMER, and the VAT."}
DOLL_MSGS5_DIFFICULTY[7] = {
"Hehe, Good luck out there P_NAMER.",
"Is it truely impossible, P_NAMER?"}
DOLL_MSGS5_DIFFICULTY[8] = {
"I have my eye on you, P_NAMER.",
"I warned you P_NAMER, you can always turn back."}
DOLL_MSGS5_DIFFICULTY[9] = {
"The Cosmos beckons P_NAMER, come join us.",
"Perish P_NAMER, PERISH!"}
DOLL_MSGS5_DIFFICULTY[10] = {
"HAHAHAHAHAHAHAHAHAHA",
"HAAAAAAAAAAAHAHAHAHA",
"P_NAMER P_NAMER oh art thou EXPUNGED FROM EXISTENCE"}
DOLL_MSGS5_DIFFICULTY[11] = {
"...",
"Why? P_NAMER what the fuck",
"P_NAMER Stop, Go back holy shit"}
DOLL_MSGS5_DIFFICULTY[12] = {
"Your hunger grows huh?"}
DOLL_MSGS5_DIFFICULTY[13] = {
"Embrace it kiddo."}
DOLL_MSGS5_DIFFICULTY[14] = {
"Maybe you really are my Daughter."}

HUB_DOLL_MESSAGE_COUNT = 0

ESMIRE_GRAMA_MSG = {
{ {"SeamstressMessage","How are you, Dear?",true} },
{ {"SeamstressMessage","Is there something on your mind?",true} },
{ {"SeamstressMessage","Feel free to visit my shop some time, Miss Wilde.",true} },
{ {"SeamstressMessage","Would you like to schedule a session?",true} },
{ {"SeamstressMessage","It appears that B' is up to his tricks again.",true}, {"BMessage","Bird up.",true} },
{ {"SeamstressMessage","It is nice to take a break now and then.",true} },
{ {"SeamstressMessage","No need for clothing repairs, I would assume?",true} },
{ {"SeamstressMessage","I am glad you enjoy my work, Dear.",true} },
{ {"SeamstressMessage","Such a colorful group, the Agency... So many opinions and morals.",true}, {"SeamstressMessage","So much to pity.",false} },
{ {"SeamstressMessage","Your preference of music in the field is quite interesting.",true} } }

function Doll_HubSpeak()
	
	local ent = HUB_DOLL
	local ent2 = GLOB_GRAMAPHONE
	
	if HUB_SPEAK_DELAY and CurTime() < HUB_SPEAK_DELAY then return end
	
	if GLOBAL_GRAMAPHONE_ON then
	
		for _,v in pairs (player.GetAll()) do
		
			if v:EyePos():Distance(ent2:GetPos()) <= 70 then
				local RANDOM = table.Random(ESMIRE_GRAMA_MSG)
				
				for xx,kk in pairs (RANDOM) do
					net.Start(kk[1])
					net.WriteString(kk[2])
					net.WriteBool(kk[3])
					net.Broadcast()
				end
				
			end
			
		end
	
	end
	
	if not IsValid(HUB_DOLL) then return end

	for _,v in pairs (player.GetAll()) do
	
		if v:EyePos():Distance(ent:GetPos()) <= 70 then
		
			if HUB_DOLL_MESSAGE_FORCE then
			
				local RANDOM = table.Random(MISSING_MTHUULRA_MSGS)
				
				if v:HasLoreFile("lore_malum_abyss") and v:HasLoreFile("weapon_royaldecree_1") then
					RANDOM = table.Random(MISSING_MTHUULRA_MSGS3)
				elseif v:HasLoreFile("lore_dagonspotted") then
					RANDOM = table.Random(MISSING_MTHUULRA_MSGS2)
				end
				
				net.Start( RANDOM[1] )
				net.WriteString( RANDOM[2] )
				
				if RANDOM[1] == "AgencyMessage" then
					net.WriteTable( RANDOM[3] )
					net.WriteTable( RANDOM[4] )
				end
				
				net.Broadcast()
			
			return end
			
		end
	
	end

end
timer.Create("Doll_SpeakToKit", 30, 0, Doll_HubSpeak)

local DOOR_USE_DELAY = 0
util.AddNetworkString("HUB_Gramaphone_On")
util.AddNetworkString("Mirror_SwapLevel")

function Mirror_SwapLevel_LIB( len, pl )

	pl:Unlock_TA_LoreFile( "kit_mirror", "" )

end
net.Receive("Mirror_SwapLevel",Mirror_SwapLevel_LIB)

function Player_UseSideDoor( ply, ent )

	if ply and IsValid(ply) and CurTime() >= DOOR_USE_DELAY then
	
		DOOR_USE_DELAY = CurTime() + 0.5
		
		if GLOB_GRAMAPHONE and IsValid(ent) and ent == GLOB_GRAMAPHONE then
			
			if not GLOBAL_GRAMAPHONE_ON then
			
				GLOBAL_GRAMAPHONE_ON = true
				
				if not ply:HasLoreFile("esmire_gramaphone_1") then
				
					ply:Unlock_TA_LoreFile( "esmire_gramaphone_1", "" )
					ply:SetAchievementProgress( "ECHO_COAT_2", 1 )
					net.Start("Achievement_ForceSave")
					net.Send(ply)
					
					HUB_SPEAK_DELAY = CurTime() + 30
					
					timer.Simple(0.75,function()
						SEAMSTRESS_TALK("Hello there, Dear! I am glad you are such an avid collector of my works.")
						SEAMSTRESS_TALK("Please feel free to put a record on if you wish to converse. I am always willing to listen.",2)
						SEAMSTRESS_TALK("And, to thank you for your work, I am giving you a new coat with my own magic essence.",2)
						SEAMSTRESS_TALK("It will allow you to utilize my Echo magic, if somewhat limited. I do so hope you enjoy it. Farewell, Miss Wilde.",2)
					end)
				
				end
				
			else
				GLOBAL_GRAMAPHONE_ON = false
			end
			
			net.Start("HUB_Gramaphone_On")
			net.WriteBool(GLOBAL_GRAMAPHONE_ON)
			net.Send(ply)
			
		end

		if HUB_SIDEDOOR and IsValid(HUB_SIDEDOOR) and IsValid(ent) and ent == HUB_SIDEDOOR then
			net.Start("HUB_CampaignSelect")
			net.WriteFloat(2)
			net.Send(ply)
			ply:ConCommand("-use")
		end

		if HUB_SINDOOR and IsValid(HUB_SINDOOR) and IsValid(ent) and ent == HUB_SINDOOR then
			net.Start("HUB_CampaignSelect")
			net.WriteFloat(3)
			net.Send(ply)
			ply:ConCommand("-use")
		end

		if HUB_CHRONODOOR and IsValid(HUB_CHRONODOOR) and IsValid(ent) and ent == HUB_CHRONODOOR then
			net.Start("HUB_CampaignSelect")
			net.WriteFloat(4)
			net.Send(ply)
			ply:ConCommand("-use")
		end
	
	end

end
hook.Add("PlayerUse","Player_UseSideDoorHook",Player_UseSideDoor)