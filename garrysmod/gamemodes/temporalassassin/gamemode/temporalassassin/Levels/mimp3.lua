		
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_teleport")
	RemoveAllEnts("env_fade")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(4281.99,-3236.98,-1155.961),Angle(0,90,0))
	CreateClothesLocker( Vector(4510.128,-1680.538,-1119.969), Angle(0,141.836,0) )
	
	CreateProp(Vector(4172.625,-1763.562,-1116.25), Angle(0.088,-21.313,0.264), "models/props_junk/propane_tank001a.mdl", true)
	CreateProp(Vector(4186.375,-1767.531,-1116.25), Angle(-0.088,-99.756,0.132), "models/props_junk/propane_tank001a.mdl", true)
	CreateProp(Vector(4208.719,-1778.406,-1106.469), Angle(-28.037,128.496,0.088), "models/props_lab/lockerdoorleft.mdl", true)
	CreateProp(Vector(4185.844,-1771.187,-1117.719), Angle(-0.044,-50.361,-0.044), "models/props_junk/pushcart01a.mdl", true)
	CreateProp(Vector(4174.438,-1754.781,-1116.25), Angle(-0.132,-88.11,0.044), "models/props_junk/propane_tank001a.mdl", true)
	CreateProp(Vector(4182.063,-1748.219,-1116.25), Angle(-0.132,-94.043,0.044), "models/props_junk/propane_tank001a.mdl", true)
	CreateProp(Vector(4185.656,-1790.5,-1118.969), Angle(52.119,-140.801,-33.091), "models/combine_helicopter/helicopter_bomb01.mdl", true)
	CreateProp(Vector(4407.063,-1890.594,-1133.937), Angle(0.308,-93.076,0.132), "models/props_junk/propane_tank001a.mdl", true)
	CreateProp(Vector(4347.563,-1890.75,-1133.906), Angle(0.132,84.463,-0.176), "models/props_junk/propane_tank001a.mdl", true)
	
	Create_FalanranWeapon( Vector(4288.598, -3402.606, -1155.969), Angle(0, 89.952, 0) )
	Create_PortableMedkit( Vector(4565.954, -1322.248, -1119.969), Angle(0, 265.356, 0) )
	Create_Backpack( Vector(4458.591, -1518.295, -1119.969), Angle(0, 0.836, 0) )
	Create_ManaCrystal100( Vector(4417.436, -1316.661, -1119.969), Angle(0, 267.996, 0) )
	Create_InfiniteGrenades( Vector(4614.581, -1507.158, -1119.969), Angle(0, 171.116, 0) )
	Create_SuperShotgunWeapon( Vector(4362.235, -1061.014, -1055.969), Angle(0, 346.456, 0) )
	Create_CrossbowWeapon( Vector(4309.782, -1105.823, -1055.969), Angle(0, 8.236, 0) )
	Create_ShotgunWeapon( Vector(4384.763, -1103.252, -1055.969), Angle(0, 14.396, 0) )
	Create_SMGWeapon( Vector(4403.339, -1066.789, -1055.969), Angle(0, 342.496, 0) )
	Create_RifleWeapon( Vector(4333.255, -1062.427, -1055.969), Angle(0, 348.216, 0) )
	Create_RevolverWeapon( Vector(4423.428, -1109.363, -1055.969), Angle(0, 22.976, 0) )
	Create_PistolWeapon( Vector(4434.856, -1062.727, -1055.969), Angle(0, 349.756, 0) )
	Create_KingSlayerWeapon( Vector(4278.545, -1061.041, -1055.969), Angle(0, 348.436, 0) )
	Create_AnthrakiaWeapon( Vector(4328.815, -1483.784, -767.969), Angle(0, 90.295, 0) )
	Create_MegaSphere( Vector(4590.862, -1381.92, -1119.969), Angle(0, 222.515, 0) )
	Create_FalanrathThorn( Vector(3107.046, -925.899, -4351.969), Angle(0, 181.575, 0) )
	Create_ManaCrystal100( Vector(3223.801, -928.097, -4351.969), Angle(0, 179.595, 0) )
	Create_Medkit10( Vector(3404.554, -645.611, -4357.969), Angle(0, 358.955, 0) )
	Create_Medkit25( Vector(3433.459, -383.892, -4355.969), Angle(0, 0.495, 0) )
	Create_Medkit10( Vector(3402.943, -122.586, -4357.969), Angle(0, 359.835, 0) )
	Create_Armour5( Vector(3400.949, -242.575, -4357.969), Angle(0, 359.615, 0) )
	Create_Armour5( Vector(3401.507, -242.579, -4354.937), Angle(0, 359.615, 0) )
	Create_Armour5( Vector(3402.066, -242.583, -4351.906), Angle(0, 359.615, 0) )
	Create_Armour5( Vector(3405.228, -524.989, -4357.969), Angle(0, 0.275, 0) )
	Create_Armour5( Vector(3405.762, -524.986, -4354.937), Angle(0, 0.275, 0) )
	Create_Armour5( Vector(3406.297, -524.983, -4351.906), Angle(0, 0.275, 0) )
	Create_ManaCrystal( Vector(3433.8, -748.945, -4355.969), Angle(0, 2.695, 0) )
	Create_ManaCrystal( Vector(3433.465, -19.112, -4355.969), Angle(0, 3.135, 0) )
	Create_PortableMedkit( Vector(3325.265, -734.245, -4351.969), Angle(0, 126.803, 0) )
	Create_PortableMedkit( Vector(3331.82, -19.892, -4351.969), Angle(0, 240.103, 0) )
	Create_GrenadeAmmo( Vector(3326.685, -186.147, -4351.969), Angle(0, 186.423, 0) )
	Create_GrenadeAmmo( Vector(3333.035, -585.787, -4351.969), Angle(0, 178.943, 0) )
	Create_Medkit25( Vector(3322.759, -334.238, -4351.969), Angle(0, 132.523, 0) )
	Create_Medkit25( Vector(3324.12, -430.917, -4351.969), Angle(0, 231.523, 0) )
	Create_EldritchArmour10( Vector(3331.299, -102.281, -4351.969), Angle(0, 185.103, 0) )
	Create_EldritchArmour10( Vector(3334.45, -254.395, -4351.969), Angle(0, 171.023, 0) )
	Create_EldritchArmour10( Vector(3331.676, -512.725, -4351.969), Angle(0, 184.443, 0) )
	Create_EldritchArmour10( Vector(3335.064, -665.505, -4351.969), Angle(0, 179.823, 0) )
	Create_EldritchArmour( Vector(2889.117, -1472.181, -4879.969), Angle(0, 308.971, 0) )
	Create_EldritchArmour( Vector(2918.825, -1471.12, -4879.969), Angle(0, 231.311, 0) )
	Create_SpiritSphere( Vector(2903.863, -1480.126, -4879.969), Angle(0, 268.601, 0) )
	Create_Armour100( Vector(1402.905, -1181.146, -4703.969), Angle(0, 310.84, 0) )
	Create_HarbingerAmmo( Vector(1476.11, -1186.683, -4703.969), Angle(0, 224.93, 0) )
	Create_Beer( Vector(549.74, -1534.864, -4719.969), Angle(0, 0.31, 0) )
	Create_Beer( Vector(705.952, -1534.634, -4719.969), Angle(0, 0.53, 0) )
	Create_Beer( Vector(834.948, -1533.754, -4719.969), Angle(0, 0.75, 0) )
	Create_Beer( Vector(976.77, -1535.125, -4719.969), Angle(0, 359.65, 0) )
	Create_Beer( Vector(1116.284, -1534.495, -4719.969), Angle(0, 0.75, 0) )
	Create_ShotgunAmmo( Vector(173.065, -1637.371, -4863.969), Angle(0, 98.178, 0) )
	Create_SMGAmmo( Vector(139.327, -1634.862, -4863.969), Angle(0, 94.218, 0) )
	Create_GrenadeAmmo( Vector(70.231, -1642.341, -4863.969), Angle(0, 49.341, 0) )
	Create_ManaCrystal( Vector(17.855, -1519.486, -4863.969), Angle(0, 353.242, 0) )
	Create_ManaCrystal( Vector(44.881, -1518.87, -4863.969), Angle(0, 348.401, 0) )
	Create_Armour( Vector(33.694, -1386.946, -4863.969), Angle(0, 325.081, 0) )
	
	local ITEMS_PART_2 = function()
	
		Create_Medkit10( Vector(2533.165, 224.485, -4897.969), Angle(0, 180.163, 0) )
		Create_Medkit10( Vector(2533.381, 81.076, -4897.969), Angle(0, 179.063, 0) )
		Create_GrenadeAmmo( Vector(2328.458, 538.273, -5697.969), Angle(0, 35.843, 0) )
		Create_Armour( Vector(2357.474, 535.413, -5697.969), Angle(0, 63.123, 0) )
		Create_SuperShotgunWeapon( Vector(2344.932, -220.132, -5697.969), Angle(0, 282.807, 0) )
		Create_Beer( Vector(2337.848, -932.663, -5695.969), Angle(0, 271.587, 0) )
		Create_Beer( Vector(2337.799, -805.969, -5695.969), Angle(0, 270.487, 0) )
		Create_Beer( Vector(2338.164, -703.362, -5695.969), Angle(0, 269.167, 0) )
		Create_Beer( Vector(2333.73, -591.902, -5695.969), Angle(0, 273.127, 0) )
		Create_Beer( Vector(2334.341, -476.16, -5695.969), Angle(0, 269.167, 0) )
		Create_Beer( Vector(2336.898, -378.904, -5695.969), Angle(0, 268.507, 0) )
		Create_Beer( Vector(2337.664, -1028.906, -5695.969), Angle(0, 85.827, 0) )
		Create_M6GAmmo( Vector(1924.303, -636.256, -5697.969), Angle(0, 289.627, 0) )
		Create_PortableMedkit( Vector(1854.93, -706.377, -5697.969), Angle(0, 355.847, 0) )
		Create_RevolverAmmo( Vector(1506.333, -1056.914, -5697.969), Angle(0, 248.707, 0) )
		Create_PistolWeapon( Vector(1481.68, -1059.977, -5697.969), Angle(0, 268.727, 0) )
		Create_Medkit25( Vector(1056.351, -603.972, -5793.969), Angle(0, 267.407, 0) )
		Create_Medkit25( Vector(1473.36, -185.125, -5793.969), Angle(0, 268.947, 0) )
		Create_ManaCrystal100( Vector(1536.343, -257.857, -5793.969), Angle(0, 190.407, 0) )
	
	end
	
	CreateTrigger( Vector(1202.539,-923.861,-4815.969), Vector(-96.846,-96.846,0), Vector(96.846,96.846,135.62), ITEMS_PART_2 )
	
	local ITEMS_PART_3 = function()
	
		Create_Backpack( Vector(3536.579, 400.989, -4079.969), Angle(0, 129.759, 0) )
		Create_Bread( Vector(3472.463, 391.669, -4079.969), Angle(0, 78.059, 0) )
		Create_Bread( Vector(3494.612, 405.287, -4079.969), Angle(0, 157.039, 0) )
		Create_Medkit25( Vector(3364.194, 405.886, -4079.969), Angle(0, 29.879, 0) )
		Create_Armour( Vector(3420.316, 403.331, -4079.969), Angle(0, 75.419, 0) )
		Create_Medkit25( Vector(3611.694, -497.861, -4082.969), Angle(0, 182.8, 0) )
		Create_Armour( Vector(4209.506, -685.796, -3871.969), Angle(0, 0.12, 0) )
		Create_Armour( Vector(4209.044, -183.886, -3871.969), Angle(0, 269.7, 0) )
		Create_Medkit25( Vector(4207.165, -427.173, -3871.969), Angle(0, 95.16, 0) )
		Create_PortableMedkit( Vector(4209.954, 11.596, -3231.969), Angle(0, 350.659, 0) )
		Create_Medkit25( Vector(4219.864, -28.913, -3232.969), Angle(0, 13.099, 0) )
		Create_KingSlayerAmmo( Vector(4250.954, -22.22, -3232.969), Angle(0, 13.539, 0) )
		Create_CrossbowAmmo( Vector(4271.955, 9.652, -3231.969), Angle(0, 340.979, 0) )
		
		local GAME_END_TXT = {
		"Before being able to continue Kit got the call to just ollie out back to her",
		"appartment, her job apparently already done.",
		"",
		"",
		"No second guesses, a shrug was had and a good ole beer was drank",
		"back at her place.",
		"",
		"",
		"That's kinda it really, Not much else to add."}	
	
		MakeGameEnd( GAME_END_TXT, 28, Vector(4347.37,352.034,112.031), Vector(-103.091,-103.091,0), Vector(103.091,103.091,299.099) )
	
	end
	
	CreateTrigger( Vector(3201.595,755.231,-4895.969), Vector(-81.165,-81.165,0), Vector(81.165,81.165,176.457), ITEMS_PART_3 )
	
	local SEC_BUTTON_1 = function()
	
		local DOOR = GetObjectFromTrace( Vector(4290.187,-3266.969,-1099.969), Vector(4291.567,-3295.031,-1099.969), MASK_SHOT )
		
		if DOOR and IsValid(DOOR) then
			DOOR:Fire("Unlock")
		end
	
	end
	
	AddSecretButton( Vector(4378.507,-1895.969,-1097.138), SEC_BUTTON_1 )
	
	CreateSecretArea( Vector(4328.628,-1472.653,-765.852), Vector(-52.148,-52.148,0), Vector(52.148,52.148,61.821) )
	CreateSecretArea( Vector(2905.385,-1473.092,-4875.437), Vector(-43.076,-43.076,0), Vector(43.076,43.076,88.74) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)