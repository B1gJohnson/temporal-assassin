
if CLIENT then return end

function MAP_LOADED_FUNC()	

	RemoveAllEnts("info_player_start")
	
	CreatePlayerSpawn( Vector(528.625,-1193.25,-3647.5), Angle(0,-60,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_KingSlayerAmmo( Vector(852.26251220703, -764.74694824219, -3647.96875), Angle(0, 268.99932861328, 0) )
		Create_KingSlayerAmmo( Vector(816.45166015625, -764.12145996094, -3647.96875), Angle(0, 268.99932861328, 0) )
		Create_KingSlayerAmmo( Vector(773.29016113281, -763.36743164063, -3647.96875), Angle(0, 268.99932861328, 0) )
		Create_KingSlayerAmmo( Vector(737.47930908203, -762.74194335938, -3647.96875), Angle(0, 268.99932861328, 0) )
		Create_Armour200( Vector(796.2919921875, -931.07647705078, -3647.96875), Angle(0, 75.839210510254, 0) )
		Create_SpiritSphere( Vector(856.14532470703, -931.19482421875, -3647.96875), Angle(0, 110.81927490234, 0) )
		Create_VolatileBattery( Vector(733.25170898438, -863.45074462891, -3647.96875), Angle(0, 359.27908325195, 0) )
		Create_CorzoHat( Vector(799.33935546875, -844.61999511719, -3647.96875), Angle(0, 11.599212646484, 0) )
		Create_ShotgunWeapon( Vector(449.52117919922, -1632.5743408203, -4799.96875), Angle(0, 178.27827453613, 0) )
		Create_ShotgunAmmo( Vector(416.54977416992, -1590.4747314453, -4799.96875), Angle(0, 269.13824462891, 0) )
		Create_ShotgunAmmo( Vector(322.88171386719, -1589.0656738281, -4799.96875), Angle(0, 269.13824462891, 0) )
		Create_ShotgunAmmo( Vector(287.06927490234, -1588.5268554688, -4799.96875), Angle(0, 269.13824462891, 0) )
		Create_ShotgunAmmo( Vector(372.19995117188, -1591.8312988281, -4799.96875), Angle(0, 268.33801269531, 0) )
		Create_PistolAmmo( Vector(293.36419677734, -1680.3059082031, -4799.96875), Angle(0, 89.258140563965, 0) )
		Create_PistolAmmo( Vector(330.25430297852, -1678.357421875, -4799.96875), Angle(0, 89.918144226074, 0) )
		Create_PistolAmmo( Vector(377.78277587891, -1677.0593261719, -4799.96875), Angle(0, 83.31810760498, 0) )
		Create_PistolAmmo( Vector(427.32238769531, -1675.8334960938, -4799.96875), Angle(0, 113.89819335938, 0) )
		Create_Beer( Vector(1612.6949462891, 324.28170776367, -5099.7407226563), Angle(0, 358.38140869141, 0) )
		Create_Beer( Vector(1602.3017578125, 339.70712280273, -5097.9565429688), Angle(0, 347.82135009766, 0) )
		Create_Beer( Vector(1631.4835205078, 326.65954589844, -5098.1240234375), Angle(0, 355.08135986328, 0) )
		Create_Beer( Vector(1617.3751220703, 341.91928100586, -5097.8466796875), Angle(0, 346.06134033203, 0) )
		Create_Beer( Vector(1632.4616699219, 337.30249023438, -5096.0927734375), Angle(0, 346.72131347656, 0) )
		Create_Beer( Vector(1648.4851074219, 325.07135009766, -5096.8950195313), Angle(0, 352.44134521484, 0) )
		Create_Beer( Vector(1649.9348144531, 344.03894042969, -5095.5961914063), Angle(0, 337.70129394531, 0) )
		Create_Beer( Vector(1616.642578125, 357.69219970703, -5095.3706054688), Angle(0, 337.70129394531, 0) )
		Create_Beer( Vector(1630.5638427734, 361.90679931641, -5094.9296875), Angle(0, 332.42126464844, 0) )
		
		CreateSecretArea( Vector(956.95697021484,-837.5029296875,-3647.96875), Vector(-124.62196350098,-124.62196350098,0), Vector(124.62196350098,124.62196350098,167.9375) )
	
	else
	
		CreateProp(Vector(861.344,-954.156,-3599.594), Angle(-0.044,90.044,-0.044), "models/props_interiors/vendingmachinesoda01a.mdl", true)
		CreateProp(Vector(774.25,-961.25,-3625.562), Angle(0,89.868,0), "models/props_c17/furniturewashingmachine001a.mdl", true)
		
		Create_ShotgunWeapon( Vector(446.35, -1632.075, -4799.969), Angle(0, 178.813, 0) )
		Create_ShotgunAmmo( Vector(432.567, -1590.843, -4799.969), Angle(0, 195.115, 0) )
		Create_GrenadeAmmo( Vector(430.544, -1675.735, -4799.969), Angle(0, 160.212, 0) )
		Create_GrenadeAmmo( Vector(411.432, -1653.39, -4799.969), Angle(0, 167.318, 0) )
		Create_SpiritSphere( Vector(-320.553, -545.69, -5120), Angle(0, 182.677, 0) )
		Create_Armour( Vector(-391.773, -544.918, -5120), Angle(0, 160.732, 0) )
		Create_Armour( Vector(-389.692, -612.894, -5120), Angle(0, 139.937, 0) )
		Create_Beer( Vector(1088.878, -608.809, -4927.969), Angle(0, 173.899, 0) )
		Create_Beer( Vector(1178.202, -614.905, -4927.969), Angle(0, 172.436, 0) )
		Create_Beer( Vector(1285.011, -625.771, -4911.969), Angle(0, 173.481, 0) )
		Create_FoolsOathWeapon( Vector(1664.121, 361.191, -5088.917), Angle(0, -0.615, 73.81) )
		Create_VolatileBattery( Vector(1660.688, 343.927, -5094.763), Angle(0, 357.082, 0) )
		Create_ManaCrystal100( Vector(1629.036, 358.737, -5095.401), Angle(0, 255.612, 0) )
		Create_InfiniteGrenades( Vector(2569.021, -420.463, -5355.754), Angle(0, 143.064, 0) )
		Create_EldritchArmour10( Vector(2546.662, -378.955, -5360.955), Angle(-20.338, 16.007, -18.302) )
		Create_EldritchArmour10( Vector(2541.362, -381.411, -5362.539), Angle(-53.992, 34.754, -32.006) )
		Create_KingSlayerAmmo( Vector(-2069.46, 1150.494, -4855.969), Angle(0, 180.269, 0) )
		Create_CrossbowAmmo( Vector(-2104.93, 1151.225, -4855.969), Angle(0, 181.105, 0) )
		Create_WhiteCrystal( Vector(1033.251, -1258.579, -3647.969), Angle(0, 178.208, 0) )

		local SEC_BUTTON_1 = function()
			Create_AnthrakiaWeapon( Vector(810.504, -817.98, -3647.969), Angle(0, 272.608, 0) )
			Create_EldritchArmour10( Vector(771.993, -777.819, -3647.969), Angle(0, 284.939, 0) )
			Create_EldritchArmour10( Vector(774.59, -853.701, -3647.969), Angle(0, 300.405, 0) )
			Create_EldritchArmour10( Vector(839.694, -856.628, -3647.969), Angle(0, 237.914, 0) )
			Create_EldritchArmour10( Vector(845.886, -780.371, -3647.969), Angle(0, 277.833, 0) )
		end
		
		AddSecretButton( Vector(808.255,-975.969,-3592.357), SEC_BUTTON_1 )
		
		CreateSecretArea( Vector(-362.524,-576.934,-5120), Vector(-75.929,-75.929,0), Vector(75.929,75.929,87.519) )
		CreateSecretArea( Vector(2560.467,-405.681,-5360.611), Vector(-46.981,-46.981,0), Vector(46.981,46.981,38.875) )
		CreateSecretArea( Vector(1036.219,-1258.281,-3647.969), Vector(-91.517,-91.517,0), Vector(91.517,91.517,149.04) )
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)