
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	
	CreatePlayerSpawn( Vector(-10660,4812,-511), Angle(0,-90,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_RevolverAmmo( Vector(2942.2849121094, -1149.4738769531, 226.10809326172), Angle(0, 349.54336547852, 0) )
		Create_PistolAmmo( Vector(2941.4851074219, -1209.3885498047, 226.10809326172), Angle(0, 30.683258056641, 0) )
		Create_ShotgunAmmo( Vector(2941.0373535156, -1193.1590576172, 226.10809326172), Angle(0, 14.843170166016, 0) )
		Create_SMGAmmo( Vector(2941.3208007813, -1170.0290527344, 226.10809326172), Angle(0, 348.88302612305, 0) )
		Create_KingSlayerAmmo( Vector(3097.5678710938, -1127.2143554688, 192.03125), Angle(0, 270.78308105469, 0) )
		Create_KingSlayerAmmo( Vector(3136.6867675781, -1124.1225585938, 192.03125), Angle(0, 270.78302001953, 0) )
		Create_Armour100( Vector(1228.8214111328, -3213.4975585938, -63.96875), Angle(0, 137.10746765137, 0) )
		Create_Medkit25( Vector(1148.5378417969, -3128.2189941406, -63.968753814697), Angle(0, 310.10739135742, 0) )
		Create_MegaSphere( Vector(11003.3125, 1341.9152832031, -300.080078125), Angle(0, 227.30102539063, 0) )
		Create_AnthrakiaWeapon( Vector(10998.063476563, 1218.4014892578, -279.57720947266), Angle(0, 253.26095581055, 0) )
		Create_Backpack( Vector(10914.080078125, 1270.4130859375, -284.25720214844), Angle(0, 358.20074462891, 0) )
		Create_Backpack( Vector(10929.028320313, 1227.0588378906, -282.98504638672), Angle(0, 32.520904541016, 0) )
		Create_Backpack( Vector(10927.185546875, 1325.1733398438, -289.12014770508), Angle(0, 319.04071044922, 0) )
		
		CreateSecretArea( Vector(11081.75390625,1278.0494384766,-299.28594970703), Vector(-182.98420715332,-182.98420715332,0), Vector(182.98420715332,182.98420715332,185.28224182129) )
		
	else
		
		Create_AnthrakiaWeapon( Vector(11012.032, 1322.008, -290.353), Angle(0, 12.453, 0) )
		Create_Backpack( Vector(10989.135, 1233.404, -296.534), Angle(0, 345.606, 0) )
		Create_FoolsOathAmmo( Vector(11016.599, 1153.464, -286.642), Angle(-12.749, -51.197, -5.53) )
		Create_M6GWeapon( Vector(10944.304, 1289.587, -291.438), Angle(10.868, -42.294, -10.58) )
		Create_ShotgunWeapon( Vector(2939.158, -1361.29, 192.031), Angle(0, 354.401, 0) )
		Create_ShotgunAmmo( Vector(2962.116, -1386.341, 192.031), Angle(0, 22.407, 0) )
		Create_Medkit25( Vector(2941.556, -1289.854, 192.031), Angle(0, 309.884, 0) )
		Create_Medkit10( Vector(3495.078, -1226.347, 160.031), Angle(0, 285.013, 0) )
		Create_Beer( Vector(3612.67, -1136.594, 160.031), Angle(0, 236.316, 0) )
		Create_Beer( Vector(3564.61, -1125.574, 160.031), Angle(0, 260.351, 0) )
		Create_PortableMedkit( Vector(2950.531, -1701.298, 160.031), Angle(0, 55.846, 0) )
		Create_PistolAmmo( Vector(2974.885, -1171.559, 192.031), Angle(0, 304.865, 0) )
		Create_PistolAmmo( Vector(2954.11, -1189.118, 192.031), Angle(0, 319.496, 0) )
		Create_SMGAmmo( Vector(2958.654, -1216.984, 192.031), Angle(0, 331.514, 0) )
		Create_SMGAmmo( Vector(2964.287, -1248.063, 192.031), Angle(0, 351.473, 0) )
		Create_Armour100( Vector(1182.991, -3807.267, -63.969), Angle(0, 123.245, 0) )
		Create_Bread( Vector(1054.805, -3772.548, -63.969), Angle(0, 58.664, 0) )
		Create_WhisperAmmo( Vector(2476.215, -2461.75, -441.272), Angle(0, 294.833, 0) )
		Create_WhisperAmmo( Vector(2472.059, -2505.55, -442.555), Angle(0, 323.049, 0) )
			
		CreateSecretArea( Vector(11150.285,1266.688,-291.67), Vector(-240.709,-240.709,0), Vector(240.709,240.709,278.215) )
		CreateSecretArea( Vector(2477.287,-2480.736,-441.827), Vector(-49.744,-49.744,0), Vector(49.744,49.744,46.755) )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)