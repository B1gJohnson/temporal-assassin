		
if CLIENT then return end

local VORTS = {}

LEVEL_STRINGS["ep2_outland_02"] = {"ep2_outland_03",Vector(-1441.1573486328,-11223.352539063,-516.70751953125), Vector(-145.27781677246,-145.27781677246,0), Vector(145.27781677246,145.27781677246,225.38262939453)}

function EXTRA_FAIL_STATE( npc, att, inf )

	if IsValid(npc) and ( npc:GetClass() == "npc_alyx" or npc:GetClass() == "npc_vortigaunt" ) then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRA_FAIL_STATEHook",EXTRA_FAIL_STATE)

function HELP_ME_NOT_EAT_SHIT_MTHUULRA( ent, dmginfo )

	if ent and IsValid(ent) and ent:IsNPC() then
	
		if ent.GetClass then
		
			if (ent:GetClass() == "npc_citizen" or ent:GetClass() == "npc_vortigaunt") then
				dmginfo:ScaleDamage(0.1)
			end
		
		end
	
	end

end
hook.Add("EntityTakeDamage","HELP_ME_NOT_EAT_SHIT_MTHUULRAHook",HELP_ME_NOT_EAT_SHIT_MTHUULRA)

NPC_NO_DUPES = true

function MAP_LOADED_FUNC()

	NPC_NO_DUPES = true
	
	RemoveAllEnts("trigger_playermovement")
	RemoveAllEnts("trigger_changelevel")
	
	Create_Medkit25( Vector(-2719.8666992188, -8953.6416015625, -715.96875), Angle(0, 275.77947998047, 0) )
	Create_Medkit25( Vector(-2666.3381347656, -8920.771484375, -715.96875), Angle(0, 260.15939331055, 0) )
	Create_Medkit25( Vector(-2625.4575195313, -8959.8994140625, -715.96875), Angle(0, 245.41931152344, 0) )
	Create_Medkit25( Vector(-2672.2133789063, -8950.6044921875, -715.96875), Angle(0, 260.37936401367, 0) )
	Create_PortableMedkit( Vector(-2738.150390625, -8982.87890625, -715.96875), Angle(0, 283.91934204102, 0) )
	Create_PortableMedkit( Vector(-2741.4604492188, -9013.7353515625, -715.96875), Angle(0, 288.97937011719, 0) )
	Create_SMGWeapon( Vector(-2697.0190429688, -9001.041015625, -715.96875), Angle(0, 267.63934326172, 0) )
	Create_SMGAmmo( Vector(-2652.83203125, -8981.28515625, -715.96875), Angle(0, 251.35931396484, 0) )
	Create_SMGAmmo( Vector(-2627.7075195313, -8994.4013671875, -715.96875), Angle(0, 240.79925537109, 0) )
	Create_SMGAmmo( Vector(-2665.0112304688, -9021.4501953125, -715.96875), Angle(0, 250.69931030273, 0) )
	Create_SMGAmmo( Vector(-2638.9594726563, -9029.267578125, -715.96875), Angle(0, 237.27926635742, 0) )
	Create_PistolAmmo( Vector(-2731.6752929688, -9051.8017578125, -715.96875), Angle(0, 291.17932128906, 0) )
	Create_PistolAmmo( Vector(-2704.9604492188, -9042.2099609375, -715.96875), Angle(0, 271.81924438477, 0) )
	Create_PistolAmmo( Vector(-2675.5649414063, -9056.8662109375, -715.96875), Angle(0, 249.37911987305, 0) )
	Create_ShotgunAmmo( Vector(-3405.1186523438, -9257.322265625, -651.96875), Angle(0, 323.95947265625, 0) )
	Create_ShotgunAmmo( Vector(-3416.0771484375, -9305.3701171875, -651.96875), Angle(0, 352.77966308594, 0) )
	Create_ShotgunAmmo( Vector(-3406.0224609375, -9357.3505859375, -651.96875), Angle(0, 25.779830932617, 0) )
	Create_ShotgunAmmo( Vector(-3388.4467773438, -9326.609375, -651.96875), Angle(0, 8.1797485351563, 0) )
	Create_ShotgunAmmo( Vector(-3384.6799316406, -9288.0068359375, -651.96875), Angle(0, 334.73959350586, 0) )
	Create_RevolverAmmo( Vector(-3414.1821289063, -9193.9765625, -651.96875), Angle(0, 325.71960449219, 0) )
	Create_RevolverAmmo( Vector(-3417.5092773438, -9227.07421875, -651.96875), Angle(0, 342.87969970703, 0) )
	Create_RevolverWeapon( Vector(-3381.5385742188, -9222.2119140625, -651.96875), Angle(0, 329.89959716797, 0) )
	Create_SuperShotgunWeapon( Vector(-3400.9780273438, -9410.462890625, -651.96875), Angle(0, 44.479568481445, 0) )
	Create_ManaCrystal100( Vector(-3338.0466308594, -9834.98828125, -651.96875), Angle(0, 87.443229675293, 0) )
	Create_ManaCrystal100( Vector(-3388.5441894531, -9788.201171875, -651.96875), Angle(0, 62.803184509277, 0) )
	Create_Armour( Vector(-3395.8403320313, -9725.5615234375, -651.96875), Angle(0, 47.403106689453, 0) )
	Create_Armour( Vector(-3409.9709472656, -9677.7216796875, -651.96875), Angle(0, 19.902954101563, 0) )
	Create_Armour100( Vector(-3403.4951171875, -9610.8515625, -651.96875), Angle(0, 336.34271240234, 0) )
	Create_SMGAmmo( Vector(-3363.7849121094, -9735.759765625, -651.96875), Angle(0, 65.442749023438, 0) )
	Create_SMGAmmo( Vector(-3330.9445800781, -9728.1611328125, -651.96875), Angle(0, 84.362808227539, 0) )
	Create_SMGAmmo( Vector(-3365.685546875, -9694.974609375, -651.96875), Angle(0, 48.722625732422, 0) )
	Create_SMGAmmo( Vector(-3330.1567382813, -9686.54296875, -651.96875), Angle(0, 79.742630004883, 0) )
	Create_ShotgunAmmo( Vector(-3371.6728515625, -9646.0791015625, -651.96875), Angle(0, 359.88250732422, 0) )
	Create_ShotgunAmmo( Vector(-3333.1171875, -9646.671875, -651.96875), Angle(0, 2.7425231933594, 0) )
	Create_ShotgunAmmo( Vector(-3351.0322265625, -9609.6953125, -651.96875), Angle(0, 2.3021697998047, 0) )
	Create_RifleAmmo( Vector(-3412.7785644531, -9562.9638671875, -651.96875), Angle(0, 9.122314453125, 0) )
	Create_RifleAmmo( Vector(-3385.5288085938, -9578.880859375, -651.96875), Angle(0, 25.842407226563, 0) )
	Create_RifleAmmo( Vector(-3392.2341308594, -9537.91015625, -651.96875), Angle(0, 351.30221557617, 0) )
	Create_Backpack( Vector(-2707.7600097656, -9483.4462890625, -87.202812194824), Angle(0, 216.68551635742, 0) )
	Create_CrossbowWeapon( Vector(-3251.9836425781, -9351.13671875, -899.96875), Angle(0, 20.921752929688, 0) )
	Create_CrossbowAmmo( Vector(-3282.6437988281, -9249.7060546875, -899.96875), Angle(0, 331.64169311523, 0) )
	Create_CrossbowAmmo( Vector(-3262.9323730469, -9298.7177734375, -899.96875), Angle(0, 351.00180053711, 0) )
	Create_CrossbowAmmo( Vector(-3225.0844726563, -9259.439453125, -899.96875), Angle(0, 318.66162109375, 0) )
	Create_CrossbowAmmo( Vector(-3216.5910644531, -9312.7939453125, -899.96875), Angle(0, 358.04183959961, 0) )
	Create_HarbingerWeapon( Vector(-3402.1125488281, -9446.3212890625, 112.03125), Angle(0, 2.2392730712891, 0) )
	Create_Armour100( Vector(-3406.978, -9481.728, -651.969), Angle(0, 359.319, 0) )
	Create_GrenadeAmmo( Vector(-3422.861, -9450.651, -651.969), Angle(0, 340.404, 0) )
	Create_RevolverAmmo( Vector(-3390.467, -9153.839, -651.969), Angle(0, 290.662, 0) )
	Create_SMGAmmo( Vector(-2570.294, -8959.721, -715.969), Angle(0, 237.056, 0) )
	Create_SMGAmmo( Vector(-2589.83, -9006.277, -715.969), Angle(0, 231.622, 0) )
	Create_SMGAmmo( Vector(-2620.045, -9055.59, -715.969), Angle(0, 220.963, 0) )
	Create_ShadeCloak( Vector(-3561.02, -9445.404, 112.031), Angle(0, 3.813, 0) )
	
	CreateProp(Vector(-3080.8125,-9993.28125,-453.84375), Angle(1.0546875,45.0439453125,-1.0546875), "models/temporalassassin/doll/unknown.mdl", true)
	
	CreateSecretArea( Vector(-3384.0625,-9444.16796875,113.26389312744), Vector(-62.83374786377,-62.83374786377,0), Vector(62.83374786377,62.83374786377,111.98162078857) )
	CreateSecretArea( Vector(-2710.7312011719,-9484.923828125,-82.656242370605), Vector(-12.141107559204,-12.141107559204,0), Vector(12.141107559204,12.141107559204,30.724414825439) )
	
	local FIX_TURRETS = function(self,ply)
		
		for _,v in pairs (ents.GetAll()) do
		
			if v and IsValid(v) and v.GetClass and (v:GetClass() == "npc_turret_floor") then
				v:Remove()
			end
		
		end
		
	end
	CreateTrigger( Vector(-1969.458,-8694.789,-507.149), Vector(-1969.458,-8694.789,-507.149), Vector(-2106,-8446.699,-465.106), FIX_TURRETS, false, Angle(0,0,0) )
	
	for _,v in pairs (ents.FindByClass("npc_vortigaunt")) do
	
		if v and IsValid(v) then
			table.insert(VORTS,v)
		end
	
	end
	
	VORT_END_CHECKED = false

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

function MAP_OnEntityCreated( ent )

	timer.Simple(0,function()
	
		if ent and IsValid(ent) then
		
			if ent.GetClass and ent:GetClass() == "npc_vortigaunt" and not table.HasValue(VORTS,ent) then
				table.insert(VORTS,ent)
			end
		
		end
	
	end)

end
hook.Add("OnEntityCreated","MAP_OnEntityCreatedHook",MAP_OnEntityCreated)

local GEN_POS = Vector(-708.678,-9544.523,-711.59)

timer.Create("Vort_End_Checker", 1, 0, function()

	if not VORT_END_CHECKED then

		if VORTS and #VORTS > 0 then
		
			for _,v in pairs (VORTS) do
			
				if v and IsValid(v) then
					if v:GetPos():DistanceFix( GEN_POS ) <= 150 then
						VORT_END_CHECKED = true
						CreateTeleporter( "P1", Vector(-669.801,-9597.574,-715.969), "P2", Vector(-501.906,-9594.605,-715.969) )
						break
					end
				else
					table.remove(VORTS,_)
				end
			
			end
		
		end
		
	end

end)