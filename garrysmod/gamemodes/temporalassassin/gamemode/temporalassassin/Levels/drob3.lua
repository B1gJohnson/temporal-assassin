
if CLIENT then return end

NPC_NO_DUPES = true

function MAP_LOADED_FUNC()	

	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("game_text")
	RemoveAllEnts("ambient_generic")
	
	for _,v in pairs (ents.FindByClass("func_door")) do
		v:SetSaveValue("noise1","")
		v:SetSaveValue("noise2","")
	end
	
	local READD_DUPES = function()
		NPC_NO_DUPES = false
	end
	
	CreateTrigger( Vector(-7415.049,2102.624,-1727.969), Vector(-211.677,-211.677,0), Vector(211.677,211.677,272.194), READD_DUPES )
	
	Create_Backpack( Vector(-7465.479, 1831.525, -1731.969), Angle(0, 271.045, 0) )
	Create_Backpack( Vector(-7465.621, 1779.918, -1731.969), Angle(0, 271.463, 0) )
	Create_Backpack( Vector(-7464.479, 1721.618, -1731.969), Angle(0, 271.672, 0) )
	Create_Backpack( Vector(-7465.339, 1661.541, -1731.969), Angle(0, 273.135, 0) )
	Create_Backpack( Vector(-7463.484, 1624.888, -1731.969), Angle(0, 273.135, 0) )
	Create_Backpack( Vector(-7466.013, 1586.924, -1731.969), Angle(0, 269.164, 0) )
	Create_Backpack( Vector(-7466.757, 1541.733, -1731.969), Angle(0, 269.164, 0) )
	Create_Backpack( Vector(-7467.413, 1496.837, -1731.969), Angle(0, 269.164, 0) )
	Create_Backpack( Vector(-7467.914, 1465.141, -1731.969), Angle(0, 269.164, 0) )
	Create_Backpack( Vector(-7469.183, 1432.414, -1731.969), Angle(0, 270.209, 0) )
	Create_MegaSphere( Vector(-7400.531, 1826.616, -1731.969), Angle(0, 275.434, 0) )
	Create_EldritchArmour( Vector(-7400.188, 1788.775, -1731.969), Angle(0, 271.254, 0) )
	Create_EldritchArmour( Vector(-7399.369, 1740.023, -1731.969), Angle(0, 271.463, 0) )
	Create_FoolsOathWeapon( Vector(-7404.668, 1690.146, -1731.969), Angle(0, 270.418, 0) )
	Create_ReikoriKey( Vector(-7403.387, 1638.902, -1731.969), Angle(0, 270, 0) )
	Create_RevolverWeapon( Vector(-7405.105, 1587.002, -1731.969), Angle(0, 271.045, 0) )
	Create_PistolWeapon( Vector(-7404.494, 1553.603, -1731.969), Angle(0, 271.045, 0) )
	Create_SuperShotgunWeapon( Vector(-7402.961, 1520.073, -1731.969), Angle(0, 270.209, 0) )
	Create_RifleWeapon( Vector(-7402.601, 1486.624, -1731.969), Angle(0, 270, 0) )
	Create_SMGWeapon( Vector(-7402.548, 1458.535, -1731.969), Angle(0, 271.463, 0) )
	Create_FrontlinerWeapon( Vector(-7405.231, 1428, -1731.969), Angle(0, 269.791, 0) )
	Create_CrossbowWeapon( Vector(-7402.746, 1721.673, -1731.969), Angle(0, 267.91, 0) )
	Create_M6GWeapon( Vector(-7403.837, 1442.447, -1728.937), Angle(0, 260.386, 0) )
	Create_HarbingerWeapon( Vector(-7404.859, 1474.47, -1728.937), Angle(0, 264.148, 0) )
	Create_KingSlayerWeapon( Vector(-7407.058, 1505.301, -1728.937), Angle(0, 267.492, 0) )
	Create_ManaCrystal100( Vector(-7407.556, 1535.206, -1728.937), Angle(0, 269.164, 0) )
	
	local GAME_END_TXT = {
	"The road ahead is clear, the robot intact as it walks off",
	"into the horizon, a job well done and all that.",
	"",
	"Armistice will be happy, the company gets paid and blood lust",
	"has been satiated for now.",
	"",
	"",
	"",
	"Who knows what'll happen next in Temporal Assassin"}	
	
	MakeGameEnd( GAME_END_TXT, 30, Vector(-3130.336,-1995.238,-5087.969), Vector(-400,-400,-100), Vector(400,400,400) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)