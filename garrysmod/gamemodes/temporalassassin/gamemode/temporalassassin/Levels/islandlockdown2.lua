		
if CLIENT then return end

LEVEL_STRINGS["islandlockdown2"] = { "islandlockdown3", Vector(5791.202,3043.977,255.031), Vector(-66.171,-66.171,0), Vector(66.171,66.171,129.986) }

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(3321.749,3897.25,576.031),Angle(0,0,0))
	
	CreateProp(Vector(3054.094,5340.75,500.25), Angle(0,0.396,0), "models/props_trainstation/tracksign07.mdl", true)
	
	Create_Medkit25( Vector(3739.858, 4017.007, 576.031), Angle(0, 270.474, 0) )
	Create_Medkit25( Vector(3682.344, 4014.271, 576.031), Angle(0, 270.254, 0) )
	Create_Armour100( Vector(3930.857, 3867.615, 576.031), Angle(0, 162.674, 0) )
	Create_KingSlayerWeapon( Vector(3934.524, 3957.005, 576.031), Angle(0, 181.594, 0) )
	Create_KingSlayerAmmo( Vector(3906.996, 3913.594, 576.031), Angle(0, 171.474, 0) )
	Create_SpiritEye1( Vector(3243.233, 4001.708, 576.031), Angle(0, 330.094, 0) )
	Create_PortableMedkit( Vector(3712.371, 4014.969, 576.031), Angle(0, 269.154, 0) )
	Create_ShotgunWeapon( Vector(3633.554, 5525.621, 576.031), Angle(0, 270.254, 0) )
	Create_CrossbowWeapon( Vector(3563.374, 5198.159, 576.031), Angle(0, 28.834, 0) )
	Create_CrossbowAmmo( Vector(3544.07, 5244.532, 576.031), Angle(0, 0.234, 0) )
	Create_ShotgunAmmo( Vector(3580.896, 5529.223, 576.031), Angle(0, 257.824, 0) )
	Create_Bread( Vector(3922.653, 5373.148, 324.031), Angle(0, 327.014, 0) )
	Create_Beer( Vector(3911.96, 5341.466, 324.031), Angle(0, 347.254, 0) )
	Create_Beer( Vector(3953.549, 5374.277, 324.031), Angle(0, 312.934, 0) )
	Create_SMGWeapon( Vector(3943.666, 5344.452, 324.031), Angle(0, 306.774, 0) )
	Create_ManaCrystal( Vector(4434.226, 5325.108, 324.031), Angle(0, 3.489, 0) )
	Create_ManaCrystal( Vector(4435.798, 5365.672, 324.031), Angle(0, 347.429, 0) )
	Create_ShotgunAmmo( Vector(4398.698, 5114.128, 324.031), Angle(0, 49.328, 0) )
	Create_M6GAmmo( Vector(4686.732, 5369.204, 324.031), Angle(0, 245.948, 0) )
	Create_RevolverAmmo( Vector(4628.587, 5374.639, 324.031), Angle(0, 280.489, 0) )
	Create_PistolWeapon( Vector(4732.63, 5197.241, 452.24), Angle(0, 181.928, 0) )
	Create_PistolAmmo( Vector(4733.609, 5147.5, 452.24), Angle(0, 149.588, 0) )
	Create_Armour( Vector(4824.854, 5329.23, 608.031), Angle(0, 270.455, 0) )
	Create_PortableMedkit( Vector(4870.351, 5319.781, 608.031), Angle(0, 244.935, 0) )
	Create_Armour100( Vector(4870.437, 5207.488, 473.83), Angle(0, 138.094, 0) )
	Create_RifleAmmo( Vector(4685.81, 4182.88, 323.948), Angle(0, 21.15, 0) )
	Create_Armour5( Vector(4734.186, 4182.494, 323.948), Angle(0, 37.87, 0) )
	Create_Armour5( Vector(4785.224, 4182.723, 323.948), Angle(0, 72.41, 0) )
	Create_Armour5( Vector(4759.374, 4182.093, 323.948), Angle(0, 75.49, 0) )
	Create_SMGWeapon( Vector(4188.79, 3609.764, 324.072), Angle(0, 92.912, 0) )
	Create_Medkit25( Vector(3777.522, 2280.946, 512.031), Angle(0, 187.864, 0) )
	Create_Medkit25( Vector(3775.244, 2238.281, 512.031), Angle(0, 170.044, 0) )
	Create_RevolverAmmo( Vector(3663.29, 2314.252, 554.201), Angle(0, 268.165, 0) )
	Create_RifleAmmo( Vector(3718.281, 2313.239, 554.201), Angle(0, 226.585, 0) )
	Create_PistolAmmo( Vector(3687.523, 2312.652, 554.201), Angle(0, 271.465, 0) )
	Create_KingSlayerAmmo( Vector(4764.567, 2885.087, 512.031), Angle(0, 247.114, 0) )
	Create_KingSlayerAmmo( Vector(4709.292, 2881.609, 512.031), Angle(0, 287.374, 0) )
	Create_CrossbowAmmo( Vector(4563.431, 3255.103, 512.031), Angle(0, 139.973, 0) )
	Create_CrossbowAmmo( Vector(4593.746, 3252.791, 512.031), Angle(0, 66.713, 0) )
	Create_CrossbowWeapon( Vector(4946.687, 3133.239, 255.031), Angle(0, 89.499, 0) )
	Create_Armour( Vector(5740.932, 2566.469, 255.031), Angle(0, 189.599, 0) )
	Create_Medkit25( Vector(5745.485, 2609.767, 255.031), Angle(0, 186.519, 0) )
	Create_Beer( Vector(5819.415, 2716.362, 323.031), Angle(0, 177.939, 0) )
	Create_Beer( Vector(5908.431, 2715.762, 323.031), Angle(0, 179.479, 0) )
	Create_Beer( Vector(5909.366, 2821.931, 323.031), Angle(0, 269.019, 0) )
	Create_Beer( Vector(5910.563, 2896.09, 323.031), Angle(0, 270.119, 0) )
	Create_MegaSphere( Vector(3865.668, 3849.682, 776.031), Angle(0, 90.875, 0) )
	
	local B_FUNC = function()
		Create_VolatileBattery( Vector(3256.347, 5341.452, 447.031), Angle(0, 180.669, 0) )
		Create_WhisperAmmo( Vector(3248.055, 5307.126, 447.031), Angle(0, 161.969, 0) )
		Create_WhisperAmmo( Vector(3255.399, 5381.279, 447.031), Angle(0, 200.689, 0) )
	end
	
	AddSecretButton( Vector(3041.502,5346.966,472.624), B_FUNC )
	
	CreateSecretArea( Vector(3866.631,3832.644,776.031), Vector(-52.132,-52.132,0), Vector(52.132,52.132,59.316) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)