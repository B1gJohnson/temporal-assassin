
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	CreatePlayerSpawn( Vector(-2462,3030.875,961), Angle(0,0,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_KingSlayerAmmo( Vector(-2274.5168457031, 2017.3845214844, 960.03125), Angle(0, 179.99989318848, 0) )
		Create_Medkit10( Vector(-1810.8050537109, 1469.2628173828, 960.03125), Angle(0, 180.00001525879, 0) )
		Create_SMGAmmo( Vector(-1815.552734375, 1644.0107421875, 960.03125), Angle(0, 166.580078125, 0) )
		Create_SMGAmmo( Vector(-1815.0085449219, 1605.8443603516, 960.03125), Angle(0, 172.29988098145, 0) )
		Create_PortableMedkit( Vector(-1284.3430175781, 1252.3695068359, 1088.03125), Angle(0, 54.019599914551, 0) )
		Create_Beer( Vector(-1271.6771240234, 1630.3616943359, 1088.03125), Angle(0, 277.83963012695, 0) )
		Create_Beer( Vector(-1228.6890869141, 1630.9704589844, 1088.03125), Angle(0, 249.01954650879, 0) )
		Create_Beer( Vector(-1181.2260742188, 1634.5848388672, 1088.03125), Angle(0, 227.45950317383, 0) )
		Create_PistolAmmo( Vector(-1110.576171875, 1224.5792236328, 960.03125), Angle(0, 346.11950683594, 0) )
		Create_PistolAmmo( Vector(-1108.3095703125, 1183.8493652344, 960.03125), Angle(0, 14.719604492188, 0) )
		Create_PistolWeapon( Vector(-804.64453125, 1090.3996582031, 960.03125), Angle(0, 157.05966186523, 0) )
		Create_Medkit25( Vector(-800.48199462891, 1175.7559814453, 960.03125), Angle(0, 180.37969970703, 0) )
		Create_Medkit10( Vector(-149.20178222656, 652.56176757813, 960.03125), Angle(0, 112.03925323486, 0) )
		Create_Medkit10( Vector(-149.39730834961, 690.47808837891, 960.03125), Angle(0, 129.63931274414, 0) )
		Create_Armour100( Vector(349.53527832031, -483.89904785156, 960.03125), Angle(0, 65.179168701172, 0) )
		Create_Armour100( Vector(-90.401489257813, -274.35119628906, 960.03125), Angle(0, 111.81956481934, 0) )
		Create_SMGAmmo( Vector(665.970703125, 609.39031982422, 960.03125), Angle(0, 359.92700195313, 0) )
		Create_SMGAmmo( Vector(720.71405029297, 609.32061767578, 960.03125), Angle(0, 359.92700195313, 0) )
		Create_SMGAmmo( Vector(781.14105224609, 609.24383544922, 960.03125), Angle(0, 359.92700195313, 0) )
		Create_SMGAmmo( Vector(838.52972412109, 609.17083740234, 960.03125), Angle(0, 359.92700195313, 0) )
		Create_SMGAmmo( Vector(910.74749755859, 609.07897949219, 960.03125), Angle(0, 359.92700195313, 0) )
		Create_SMGAmmo( Vector(978.74542236328, 609.11328125, 960.03125), Angle(0, 359.70700073242, 0) )
		Create_RevolverAmmo( Vector(796.87097167969, 669.83972167969, 960.03125), Angle(0, 92.106948852539, 0) )
		Create_RevolverAmmo( Vector(742.37512207031, 671.58520507813, 960.03125), Angle(0, 63.506858825684, 0) )
		Create_RevolverAmmo( Vector(990.07586669922, 677.69854736328, 960.03125), Angle(0, 106.6268157959, 0) )
		Create_RevolverAmmo( Vector(934.841796875, 674.85034179688, 960.03125), Angle(0, 74.066719055176, 0) )
		Create_ManaCrystal100( Vector(160.34608459473, 256.70123291016, 992.03125), Angle(0, 89.466758728027, 0) )
		Create_SpiritSphere( Vector(128.0588684082, 1041.6917724609, 1088.03125), Angle(0, 261.68258666992, 0) )
		
	else
	
		CreateProp(Vector(-459.219,75.344,1216.438), Angle(0.044,25.752,0), "models/temporalassassin/doll/unknown.mdl", true)
		
		Create_Medkit25( Vector(-2219.515, 2341.015, 960.031), Angle(0, 359.74, 0) )
		Create_Medkit10( Vector(-2217.656, 2310.749, 960.031), Angle(0, 23.567, 0) )
		Create_SuperShotgunWeapon( Vector(-2062.139, 2757.278, 960.031), Angle(0, 138.726, 0) )
		Create_ShotgunAmmo( Vector(-2060.455, 2795.649, 960.031), Angle(0, 189.513, 0) )
		Create_KingSlayerAmmo( Vector(-2051.6, 1958.841, 960.031), Angle(0, 159.417, 0) )
		Create_GrenadeAmmo( Vector(-1924.839, 1543.556, 960.031), Angle(0, 77.698, 0) )
		Create_RevolverAmmo( Vector(-1888.227, 1537.066, 960.031), Angle(0, 119.707, 0) )
		Create_MegaSphere( Vector(127.233, 1024.804, 1088.031), Angle(0, 82.816, 0) )
		Create_FoolsOathAmmo( Vector(418.372, 999.513, 960.031), Angle(0, 129.293, 0) )
		Create_SMGWeapon( Vector(415.759, 928.307, 960.031), Angle(0, 190.869, 0) )
		Create_SMGAmmo( Vector(390.779, 962.812, 960.031), Angle(0, 197.348, 0) )
		Create_SMGAmmo( Vector(369.462, 922.115, 960.031), Angle(0, 145.83, 0) )
		Create_GrenadeAmmo( Vector(-90.526, 995.646, 960.031), Angle(0, 310.832, 0) )
		Create_GrenadeAmmo( Vector(-109.571, 976, 960.031), Angle(0, 331.628, 0) )
		Create_PistolAmmo( Vector(-102.179, 927.183, 960.031), Angle(0, 357.335, 0) )
		Create_PistolAmmo( Vector(-80.133, 947.19, 960.031), Angle(0, 340.615, 0) )
		Create_PistolAmmo( Vector(-58.64, 978.663, 960.031), Angle(0, 310.728, 0) )
		Create_ShotgunWeapon( Vector(-47.217, 914.059, 960.031), Angle(0, 301.532, 0) )
		Create_CrossbowWeapon( Vector(-91.731, 863.678, 960.031), Angle(0, 344.377, 0) )
		Create_CrossbowAmmo( Vector(-51.505, 863.281, 960.031), Angle(0, 325.567, 0) )
		Create_KingSlayerWeapon( Vector(11.089, 973.395, 960.031), Angle(0, 291.5, 0) )
		Create_Armour100( Vector(625.93, 158.964, 960.031), Angle(0, 163.383, 0) )
		Create_Medkit25( Vector(627.002, 335.792, 960.031), Angle(0, 182.193, 0) )
		Create_Medkit25( Vector(623.757, 497.691, 960.031), Angle(0, 180.103, 0) )
		Create_Bread( Vector(731.336, 677.706, 960.031), Angle(0, 262.031, 0) )
		Create_Bread( Vector(799.126, 653.58, 960.031), Angle(0, 209.781, 0) )
		Create_M6GAmmo( Vector(991.032, 634.05, 960.031), Angle(0, 196.927, 0) )
		Create_PortableMedkit( Vector(1006.005, 595.608, 960.031), Angle(0, 169.862, 0) )
		Create_PortableMedkit( Vector(975.37, 598.894, 960.031), Angle(0, 167.772, 0) )
		Create_FoolsOathWeapon( Vector(-461.823, 166.524, 1216.031), Angle(0, 354.409, 0) )
		Create_Beer( Vector(285.491, 95.507, 992.031), Angle(0, 169.546, 0) )
		Create_Beer( Vector(293.026, 55.565, 992.031), Angle(0, 139.45, 0) )
		Create_VolatileBattery( Vector(419.48, 329.044, 992.031), Angle(0, 188.147, 0) )
		Create_SpiritEye2( Vector(131.017, 531.321, 960.031), Angle(0, 269.03, 0) )
		
		CreateSecretArea( Vector(-427.534,119.134,1216.031), Vector(-88.124,-88.124,0), Vector(88.124,88.124,134.466) )
	
	end
	
	for _,v in pairs (ents.FindByClass("npc_turret_floor")) do
	
		if v and IsValid(v) then
			v.FAKE_HEALTH = 999999
			v:AddRelationship("player D_LI 99")
		end
	
	end
	
	local ALLOW_SCENESKIP = function()
		SetSceneSkipAvailable(true)
	end
	
	local STOP_SCENESKIP = function()
		SetSceneSkipAvailable(false)
	end
	
	CreateTrigger( Vector(-580.889,784.09,928.031), Vector(-122.318,-122.318,0), Vector(122.318,122.318,243.417), ALLOW_SCENESKIP )
	CreateTrigger( Vector(-188.744,558.699,928.031), Vector(-67.429,-67.429,0), Vector(67.429,67.429,247.219), STOP_SCENESKIP )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

function NOKILL_TURRETS( ent )

	timer.Simple(0.1,function()
	
		if ent and IsValid(ent) then
		
			if ent:GetClass() == "npc_turret_floor" then
				ent.FAKE_HEALTH = 999999
				ent:AddRelationship("player D_LI 99")
				ent:AddRelationship("npc_alyx D_LI 99")
			end
		
		end
	
	end)

end
hook.Add("OnEntityCreated","NOKILL_TURRETSHook",NOKILL_TURRETS)

local DONT_DIE_LMAO = {
npc_eli = true,
npc_kleiner = true,
npc_kliener = true,
npc_magnusson = true,
npc_mossman = true}

function STOP_NPC_DEATH( ent, dmginfo )

	if ent and IsValid(ent) and ent.GetClass then

		local CLASS = ent:GetClass()
		
		if DONT_DIE_LMAO and DONT_DIE_LMAO[CLASS] then
			ent:SetHealth(50000)
			dmginfo:SetDamage(0)
			dmginfo:ScaleDamage(0)
		end
		
	end

end
hook.Add("EntityTakeDamage","XORDER_STOP_NPC_DEATHHook",STOP_NPC_DEATH)