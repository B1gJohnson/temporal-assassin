		
if CLIENT then return end

--[[
- IMPORTANT -
When making stuff for a map, make sure you are playing on Normal Difficulty so item spawns are not doubled
by accident and the map doesnt spawn extra enemies

REMEMBER to turn on TA_DEV_DeveloperMode 1

Be sure to check out example_islandescape_cl.lua too!
]]

--[[
This is used to define where the end of the map is, and what it should link to.

LEVEL_STRINGS["MAP_HERE"] = {"MAP_TO_GOTO", POS, MIN, MAX}

You can use TA_DEV_MakeSecretArea to help with defining a map end trigger, Just take the vector data from inside the function and paste it instead.

For example up top would change from what it is to a proper one by using the data from this

CreateSecretArea( Vector(739.698,56.263,-143.969), Vector(-101.053,-101.053,0), Vector(101.053,101.053,89.739) )
^ We copy the vector data only into our LEVEL_STRINGS data
LEVEL_STRINGS["MAP_HERE"] = {"MAP_TO_GOTO", Vector(739.698,56.263,-143.969), Vector(-101.053,-101.053,0), Vector(101.053,101.053,89.739)}

REMEMBER AND NOTE, IF THE LEVEL YOU ARE MAKING IS A FINAL MAP, YOU DO NOT NEED A LEVEL_STRINGS SETUP, USE THE END OF EPISODE/GAME STUFF AT THE BOTTOM OF HERE
]]

LEVEL_STRINGS["islandescape"] = {"islandescape2", Vector(1686.922,1204.033,268.031), Vector(-176.629,-176.629,0), Vector(176.629,176.629,125.46)}

--[[
GLOB_NO_FENIC when set to true like below means that kit will start the map without the S.I.S fenic
]]
GLOB_NO_FENIC = true

--[[
If you wish to force a specific weapon variant you can use these globals.

It is IMPORTANT that you only enable one of each variant, else it might not work or not force itself to the weapon you want.

Pistols : GLOB_FORCE_FENIC = true / GLOB_FORCE_DI2PHOTO = true / GLOB_FORCE_GRISSPECIAL = true
Revolvers : GLOB_FORCE_WOLF = true / GLOB_FORCE_COBALT = true
Shotguns : GLOB_FORCE_CRUENTUS = true / GLOB_FORCE_DEMONSBANE = true
Bows : GLOB_FORCE_QUEENSDOOM = true / GLOB_FORCE_AZARATH = true
Power-Automatic : GLOB_FORCE_HARBINGER = true / GLOB_FORCE_LSTAR = true

GLOB_STARTHURT = 42
^ This will make kit spawn with the health defined above, this cannot be a value of 0 or stuff kinda breaks, the max value is 300

GLOB_NOSNAPRANDOM = true
^ This makes it so enemy spawns aren't randomized when duped, this means normal zombies won't snap into poison zombies and so on so forth
]]

--[[
You can use these globals for extra options to do with saves/loading

GLOB_NO_SAVEEXIT, if set to true (GLOB_NO_SAVEEXIT = true), the map will not save the players inventory/next level when they enter the Next Level trigger
GLOB_NO_SAVEDATA, if set to true (GLOB_NO_SAVEDATA = true), the map will not load the players save data from the current campaign they where on or if any save data exists (weapon/ammo/health/armour/hats/medkits data)
GLOB_SAFE_GAMEEND, if set to true (GLOB_SAFE_GAMEEND = true), Chapter End/Credit Roll chapter ends will not delete the players save data, this is ofcourse, used in the new sin hunter lua too
]]

--[[
This is our function that we hook into InitPostEntity
This is ran once the map loads and before the player spawns, keep this as it is and just modify the inside
of the function for whatever you might need.
]]
function MAP_LOADED_FUNC()

	--[[
	The stuff below here marked with file.Delete will cause kit's inventory and the players
	save data to be deleted upon map loading, this is used on FIRST MAPS ONLY, since this is
	basically the start of a campaign, we want the player to start with the base equipment.
	
	This can also be used to delete the players inventory during a midway reset point like in
	HL:Source's garbage crusher scene
	]]
	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end
	
	--[[
	Here we remove all the entities we'de want to get rid of from the base map to make it easier to use
	in TA, we remove the normal spawn point and define a custom one below using CreatePlayerSpawn
	
	info_player_start to remove the default spawn points
	trigger_changelevel to get rid of any left over change level triggers, just incase
	trigger_hurt so kit doesn't die to pit falls or base gordon stuff
	game_text to make it so no text pops up from the map
	]]
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	--[[
	We make the players spawn here, we make this by standing where we want the player to start
	and use CLGetPos and CLGetAngles
	]]
	CreatePlayerSpawn(Vector(160,-296,-15),Angle(0,120,0))
	
	--[[
	Make a clothes locker for the player to change their suit at, i tend to place these every 1/3rd or 1/4th of a campaign
	depending on its length, just incase the player wants to change it up, however they can always change it up at the HUB
	before loading their game so this isn't quite necessary, spawning a Clothes Locker from entities will create a line of
	code for you to copy and paste into files like this
	]]
	CreateClothesLocker( Vector(1461.797,-230.412,360.134), Angle(0,317.507,0) )
	
	--[[
	Heres the main meat of the level, the item placements.
	
	To add item placements for a map, be sure to disable ai with ai_disabled 1
	Pop open kits phone and use the chat cheats
	immune
	gimmegimme
	fullrestore
	
	This makes it so you cannot pickup items that you place (except backpacks and hats to the cap) and
	will help you place items without making mistakes, If you place an item in a wrong place you can hold
	F2 and context menu delete them. This isn't possible with some items and will need to be picked up by
	kit or ignored when you create the list.
	
	To be safe, be sure to report your item list and paste it in your level lua periodically.
	
	You can print a list of all current items on the map for your level item spawn by using TA_DEV_GenerateItemTable
	and copying the printed list into here, this was done for the below items too
	]]
	Create_Armour100( Vector(82.456, -305.343, 7.495), Angle(0, 26.374, 0) )
	Create_RevolverWeapon( Vector(83.306, -250.416, 7.495), Angle(0, 318.614, 0) )
	Create_RevolverAmmo( Vector(88.145, -267.292, 7.495), Angle(0, 339.074, 0) )
	Create_RevolverAmmo( Vector(87.785, -285.628, 7.495), Angle(0, 18.674, 0) )
	Create_EldritchArmour10( Vector(469.117, -8.244, 7.83), Angle(0, 121.669, 0) )
	Create_EldritchArmour10( Vector(442.028, -9.147, 7.83), Angle(0, 86.248, 0) )
	Create_EldritchArmour10( Vector(412.612, -8.309, 7.83), Angle(0, 51.048, 0) )
	Create_SpiritEye1( Vector(127.183, -86.285, -15.969), Angle(0, 93.588, 0) )
	Create_Backpack( Vector(-52.853, -18.836, -15.969), Angle(0, 145.368, 0) )
	Create_Medkit25( Vector(-383.251, 410.408, 112.031), Angle(0, 90.328, 0) )
	Create_Medkit25( Vector(-484.667, 411.121, 112.031), Angle(0, 90.108, 0) )
	Create_Medkit10( Vector(-392.892, 370.507, 112.031), Angle(0, 271.028, 0) )
	Create_Medkit10( Vector(-370.197, 366.678, 112.031), Angle(0, 256.948, 0) )
	Create_Beer( Vector(403.527, -305.576, 240.031), Angle(0, 153.857, 0) )
	Create_Beer( Vector(384.391, -304.361, 240.031), Angle(0, 148.357, 0) )
	Create_Beer( Vector(367.767, -304.122, 240.031), Angle(0, 140.877, 0) )
	Create_Beer( Vector(351.924, -303.985, 240.031), Angle(0, 130.317, 0) )
	Create_Armour( Vector(1374.29, -276.73, 368.031), Angle(0, 211.495, 0) )
	Create_CorzoHat( Vector(4844.865, -4188.778, 683.419), Angle(0, 142.526, 0) )
	Create_SuperShotgunWeapon( Vector(2330.584, -3984.929, 369.185), Angle(0, 270.206, 0) )
	Create_ShotgunAmmo( Vector(2330.516, -4010.271, 369.175), Angle(0, 270.646, 0) )
	Create_PistolWeapon( Vector(719.338, -570.874, 368.031), Angle(0, 0.217, 0) )
	Create_PistolAmmo( Vector(724.84, -589.768, 368.031), Angle(0, 15.837, 0) )
	Create_PistolAmmo( Vector(730.291, -555.845, 368.031), Angle(0, 328.097, 0) )
	Create_Medkit25( Vector(1146.334, -467.346, 360.184), Angle(0, 274.615, 0) )
	Create_Medkit25( Vector(1201.982, -469.366, 360.293), Angle(0, 262.075, 0) )
	Create_PistolAmmo( Vector(1044.723, 310.349, 391.83), Angle(0, 129.072, 0) )
	Create_PistolWeapon( Vector(993.779, 310.049, 391.83), Angle(0, 60.432, 0) )
	Create_Medkit25( Vector(901.328, -78.227, 368.031), Angle(0, 94.172, 0) )
	Create_PortableMedkit( Vector(979.267, -70.918, 391.83), Angle(0, 157.532, 0) )
	Create_PortableMedkit( Vector(1039.406, 399.764, 368.031), Angle(0, 213.272, 0) )
	Create_RevolverAmmo( Vector(430, 1079.969, 432.031), Angle(0, 262.552, 0) )
	Create_Medkit10( Vector(400.122, 1081.874, 432.031), Angle(0, 270.032, 0) )
	Create_Medkit25( Vector(98.698, 236.918, 464.031), Angle(0, 89.318, 0) )
	Create_Medkit10( Vector(-97.994, 1386.932, 240.031), Angle(0, 290.758, 0) )
	Create_Medkit10( Vector(-79.597, 1375.922, 240.031), Angle(0, 239.718, 0) )
	Create_PistolWeapon( Vector(206.797, 1404.68, 464.031), Angle(0, 159.384, 0) )
	Create_Medkit10( Vector(212.604, 1471.413, 464.031), Angle(0, 206.024, 0) )
	Create_RevolverAmmo( Vector(285.515, 370.15, 464.031), Angle(0, 194.32, 0) )
	Create_Medkit10( Vector(282.787, 280.286, 464.031), Angle(0, 158.68, 0) )
	Create_VolatileBattery( Vector(45.464, 743.041, 464.031), Angle(0, 230.92, 0) )
	
	--[[
	This is a function that was made up for a secret button to run when its pressed
	You can create items or props with this by just taking them out of your item list
	]]
	local B_FUNC = function()
		Create_MegaSphere( Vector(268.864, 405.462, 256.031), Angle(0, 238.396, 0) )
		Create_SMGWeapon( Vector(241.411, 386.144, 256.031), Angle(0, 276.236, 0) )
		Create_SMGAmmo( Vector(218.557, 362.4, 256.031), Angle(0, 244.116, 0) )
		Create_SMGAmmo( Vector(267.03, 364.239, 256.031), Angle(0, 217.496, 0) )
	end
	
	--[[
	This creates a secret button at the specified position that runs out function above
	that we called B_FUNC, a button can be pressed multiple times, but will only run the function once
	
	There are more arguments to this than what is shown here, the arguments are as follows
	
	AddSecretButton( PositionOfButton, FunctionToRun, CustomText, SecretSound, ButtonClickSound )
	
	You can find other secret sounds in content/sound/temporalassassin/secrets/
	]]
	AddSecretButton( Vector(272.065,229.194,280.485), B_FUNC )
	
	--[[
	Here we make the secret button hint prop, you can do this yourself to make custom props or anything like that
	by looking at an object and using CLGetObject, the output is copied to your clipboard automatically
	]]
	CreateProp(Vector(278.656,257.938,266.594), Angle(-20.127,179.473,0), "models/props_trainstation/tracksign10.mdl", true)
	
	--[[
	We make secret areas here, these can be easily created with the TA_DEV_MakeSecretArea command.
	
	Using the command once shows a white helper gizmo for you to choose where the base of the secret area is.
	Using the command a second time locks the gizmo in place and allows you to pull the secret area out and up/down to
	define its size and what not.
	Using the command a final time will stop the gizmo and box drawing and will output the code for that secret area, it
	is copied to your clipboard automatically for easy pasting.
	
	There are more arguments to this than what is shown here, the arguments are as follows
	
	CreateSecretArea( PositionOfSecretTrigger, OBBMinsOfTrigger, OBBMaxsOfTrigger, CustomText, CustomSound )
	
	You can find other secret sounds in content/sound/temporalassassin/secrets/
	]]
	CreateSecretArea( Vector(-61.697,-14.477,-11.437), Vector(-21.103,-21.103,0), Vector(21.103,21.103,53.912) )
	CreateSecretArea( Vector(411.677,33.09,-15.969), Vector(-63.931,-63.931,0), Vector(63.931,63.931,104.899) )
	CreateSecretArea( Vector(4839.675,-4187.213,682.996), Vector(-79.843,-79.843,0), Vector(79.843,79.843,67.973) )
	CreateSecretArea( Vector(2329.974,-3997.203,373.688), Vector(-47.995,-47.995,0), Vector(47.995,47.995,67.528) )
	CreateSecretArea( Vector(41.529,738.798,468.563), Vector(-18.739,-18.739,0), Vector(18.739,18.739,54.611) )
	
	--[[
	You can make custom triggers if you like, theres a commented out example below that simply
	spawns some enemies when you touch the trigger, you can also make triggers infinite, making them
	trigger everytime the player passes through them.
	
	The arguments for CreateTrigger are as follows.
	
	The last argument, if set to true means the trigger will stay forever until you remove it yourself.
	
	CreateTrigger( TriggerPos, OBBMins, OBBMaxs, FunctionToRun, DontRemoveOnTrigger )
	
	You can spawn custom npcs in two ways, One way is normal with no audio que or delay, and the other is
	with the high difficulty style Snap spawn.
	
	CanDuplicate if true means this enemy can spawn snap dupes from higher difficultys
	
	CreateNPC( NPCClass, Position, Angle, Weapon, CanDuplicate )
	CreateSnapNPC( NPCClass, Position, Angle, Weapon, CanDuplicate )
	]]
	
	--[[
	local NPC_SPAWNS_1 = function()
		CreateNPC( "npc_combine_s", Vector(411.677,33.09,-15.969), Angle(0,127,0), "weapon_smg1", true )
	end
	
	CreateTrigger( Vector(2329.974,-3997.203,373.688), Vector(-47.995,-47.995,0), Vector(47.995,47.995,67.528), NPC_SPAWNS_1, false )
	]]
	
	--[[
	You can create Teleporters if you would like by using the function below, Please make sure that
	every teleporter has a unique name, or else bad stuff starts to happen.
	
	You can use CLGetPos for teleporter locations
	
	CreateTeleporter( NameOfPortal1, Portal1Position, NameOfPortal2, Poratl2Position )
	]]
	
	--[[
	This is where we would setup our Episode/Game end point if this was the final level in our string of levels.
	
	It is setup like shown below (without the --[[]] ofcourse), each line in the table is text that will show up
	on the black screen, they scroll from bottom to top and can be infinitely long.
	
	The arguments for the game ending functions are as follows
	
	MakeGameEnd( GameEndTextTable, SecondsUntilGoingToHub, PositionOfTrigger, MinOBBsOfTrigger, MaxsOBBsOfTrigger )
	]]
	
	--[[
	local GAME_END_TXT = {
	"You finished the example lua by reading it and stuff, Congrats.",
	"",
	"However learning how to make all this work out in the end is",
	"something else entirely, no?",
	"",
	"",
	"I wish you the best of luck, and i look forward to playing anything",
	"you can come up with.",
	"",
	"Thanks so much for playing my game",
	"Stay safe - Magenta Iris"}	
	
	MakeGameEnd( GAME_END_TXT, 40, Vector(-12447,11748,-3006), Vector(-43,-43,0), Vector(43,43,93) )
	]]
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)