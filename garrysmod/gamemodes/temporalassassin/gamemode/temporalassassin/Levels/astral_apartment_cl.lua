
if SERVER then return end

DEV_GAMEOVER = true
GLOB_MUSIC_SILENCE = true
TEMPORAL_NO_SLOW = true
TEMPORAL_NO_STOP = true

if MAP_NICENAMES then
	MAP_NICENAMES["astral_apartment"] = "?????"
end

function MAP_HintParticle_LIB( len )

	local VEC = net.ReadVector()
	
	ParticleEffect( "AstralApartment_Hint", VEC, Angle(0,0,0) )

end
net.Receive("MAP_HintParticle",MAP_HintParticle_LIB)

function AstralApartment_Think()

	local PL = LocalPlayer()
	
	if PL and IsValid(PL) then
	
		if not PL.AstralApartment_Music then
			PL.AstralApartment_Music = CreateSound( PL, "TemporalAssassin/Maps/AstralApartment_Loop.mp3" )
			PL.AstralApartment_Music:PlayEx(0.225,100)
			net.Start("Astral_Loaded")
			net.SendToServer()
		else
		
			PL.AstralApartment_Music:Play()
			
			if PL.STOP_ASTRAL_MUSIC then
				PL.AstralApartment_Music:ChangeVolume( 0, 0 )
			else
				
				if PL.Memory_Visuals and CurTime() <= PL.Memory_Visuals then
					PL.AstralApartment_Music:ChangeVolume( 0.14, 0 )
				else
					PL.AstralApartment_Music:ChangeVolume( 0.225, 0 )
				end
				
			end
			
		end
	
	end

end
hook.Add("Think","AstralApartment_ThinkHook",AstralApartment_Think)

function Astral_MomArms_LIB( len )

	local BL = net.ReadBool()
	
	if BL then
		ScreenParticleLoop( "AstralApartment_MthuulraArms", "ASTRAL_ARMS", true )
	else
		ScreenParticleLoop( "AstralApartment_MthuulraArms", "ASTRAL_ARMS", false )
	end

end
net.Receive("Astral_MomArms",Astral_MomArms_LIB)

local ALPHA = 0
local DOLL_MESSAGE = "Temporal Guard + Reload with 150% Temporal Mana & The Pocket Watch"

function RELEASE_DATHARON_HUD()

	local CT = CurTime()
	local PL = LocalPlayer()
	
	if PL and IsValid(PL) then
	
		if PL.RELEASE_DATHARON_HINT and CT >= PL.RELEASE_DATHARON_HINT then
		
			local RFT = ViewModel_AnimationTime()*50
		
			ALPHA = math.Clamp(ALPHA + RFT,0,150)
			
			if PL.Gift_WhiteOut then
				hook.Remove("HUDPaint","RELEASE_DATHARON_HUDHook")
			end

			local SINEX1, SINEY1 = math.sin(CT*2)*4, math.sin(CT*1.7 + 0.44)*4
			local SINEX2, SINEY2 = math.sin(CT*1.9+0.2)*3.5, math.sin(CT*1.35 + 0.62)*3.5
			local SINEX3, SINEY3 = math.sin(CT*2.5+0.4)*3.19, math.sin(CT*1.9 + 0.87)*3.24
			draw.SimpleText( DOLL_MESSAGE, "DOLL_TXT2", ScrW()*0.5 + GUI_X(SINEX1), ScrH()*0.3 + GUI_X(SINEY1), Color(100,100,100,ALPHA*0.01), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			draw.SimpleText( DOLL_MESSAGE, "DOLL_TXT2", ScrW()*0.5 + GUI_X(SINEX2), ScrH()*0.3 + GUI_X(SINEY2), Color(100,100,100,ALPHA*0.01), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			draw.SimpleText( DOLL_MESSAGE, "DOLL_TXT2", ScrW()*0.5 + GUI_X(SINEX3), ScrH()*0.3 + GUI_X(SINEY3), Color(100,100,100,ALPHA*0.01), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
			DrawShadowText( DOLL_MESSAGE, "DOLL_TXT", ScrW()*0.5, ScrH()*0.3, Color(10,10,10,ALPHA), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "DOLL_TXT2", Color(100,100,100,ALPHA) )	
			
		end
	
	end
	
end
hook.Add("HUDPaint","RELEASE_DATHARON_HUDHook",RELEASE_DATHARON_HUD)

function MEMORY_VIS()

	local MEMORY_VISION = 0
	
	local PL = LocalPlayer()
	if PL.Memory_Visuals and CurTime() <= PL.Memory_Visuals then
		MEMORY_VISION = 4
	end
	
	local MEMORY_S = Variable_SmoothOut("Memory_Vision_Smooth", MEMORY_VISION, 1, true, 0.01, 5000)

	if MEMORY_S > 0 then
		
		local CMOD = {}
		CMOD["$pp_colour_addr"] = 0
		CMOD["$pp_colour_addg"] = 0
		CMOD["$pp_colour_addb"] = 0
		CMOD["$pp_colour_brightness"] = 0
		CMOD["$pp_colour_contrast"] = 1
		CMOD["$pp_colour_colour"] = 1 - (MEMORY_S/4)
		CMOD["$pp_colour_mulr"] = 0
		CMOD["$pp_colour_mulg"] = 0
		CMOD["$pp_colour_mulb"] = 0
		
		DrawColorModify(CMOD)
		
		if MEMORY_S > 0 then
			DrawSobel( 30 - (MEMORY_S*7) )
		end
		
	end

end
hook.Add("RenderScreenspaceEffects","MEMORY_VIS_HOOK",MEMORY_VIS)