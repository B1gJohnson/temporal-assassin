		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true
GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(3119.661,3691.389,-896.071), Angle(0,-90.36,0))
	
	Create_Medkit25( Vector(217.645, 3132.33, -831.768), Angle(0, 166.201, 0) )
	Create_Beer( Vector(2353.389, 1897.004, -643.653), Angle(0, 158.073, 0) )
	Create_Beer( Vector(2336.325, 1898.745, -643.969), Angle(0, 175.497, 0) )
	Create_Beer( Vector(2371.847, 1896.902, -643.969), Angle(0, 167.753, 0) )
	Create_Beer( Vector(2380.657, 1897.031, -643.896), Angle(0, 171.024, 0) )
	Create_SuperShotgunWeapon( Vector(2372.974, -434.465, -631.608), Angle(10.606, -170.874, -49.134) )
	Create_ShotgunAmmo( Vector(2374.482, -399.101, -607.943), Angle(16.212, -4.916, 8.384) )
	Create_Beer( Vector(511.614, -221.806, -461.969), Angle(0, 17.192, 0) )
	Create_Beer( Vector(517.454, -278.193, -461.969), Angle(0, 64.888, 0) )
	Create_Beer( Vector(322.821, -237.603, -461.969), Angle(0, 173.832, 0) )
	Create_Beer( Vector(335.079, -264.097, -461.969), Angle(0, 14.024, 0) )
	Create_Bread( Vector(513.072, -247.507, -461.969), Angle(0, 52.04, 0) )
	Create_Beer( Vector(370.207, 107.158, -461.969), Angle(0, 275.64, 0) )
	Create_Beer( Vector(356.818, 173.256, -461.969), Angle(0, 97.528, 0) )
	Create_Beer( Vector(182.936, 113.269, -465.969), Angle(0, 282.496, 0) )
	Create_Beer( Vector(192.529, 163.111, -465.969), Angle(0, 85.112, 0) )
	Create_Beer( Vector(514.818, -812.922, -435.969), Angle(0, 185.784, 0) )
	Create_Beer( Vector(320.63, -815.925, -435.677), Angle(0, 352.36, 0) )
	Create_RifleWeapon( Vector(-614.883, -35.946, -633.282), Angle(-7.61, 99.66, 1.713) )
	Create_RifleAmmo( Vector(-611.887, -29.671, -631.678), Angle(0, 98.841, 0) )
	Create_VolatileBattery( Vector(-12.512, 657.115, -794.491), Angle(0, -133.592, 90) )
	Create_Bread( Vector(-284.14, 187.815, -467.76), Angle(0, 175.921, 0) )
	Create_Bread( Vector(-273.592, 220.62, -470.311), Angle(0, 204.081, 0) )
	Create_Beer( Vector(-331.735, 234.687, -466.969), Angle(0, 333.441, 0) )
	Create_Beer( Vector(-320.59, 245.29, -463.937), Angle(0, 7.585, 0) )
	Create_Beer( Vector(-346.936, 245.857, -466.969), Angle(0, 329.041, 0) )
	Create_Beer( Vector(-404.576, 153.955, -465.969), Angle(0, 180.065, 0) )
	Create_PortableMedkit( Vector(-280.522, 159.019, -468.893), Angle(0, 123.73, 0) )
	Create_Medkit25( Vector(-277.736, 170.633, -493.792), Angle(0, 178.642, 0) )
	Create_Armour100( Vector(-414.867, 302.031, -351.969), Angle(0, 165.346, 0) )
	Create_Medkit10( Vector(-508.592, 342.374, -315.75), Angle(-86.276, 45.756, -131.591) )
	Create_Medkit25( Vector(-536.607, 350.057, -317.368), Angle(0, 283.178, 0) )
	Create_SMGWeapon( Vector(-588.845, 253.34, -311.136), Angle(0, 353.49, 0) )
	Create_SMGAmmo( Vector(-566.473, 260.353, -311.377), Angle(0, -233.981, 0) )
	Create_PistolWeapon( Vector(290.844, 2935.752, 70.111), Angle(0, 152.243, 0) )
	Create_PistolAmmo( Vector(275.868, 2925.733, 70.047), Angle(0, 224.835, 0) )
	Create_FrontlinerWeapon( Vector(324.723, 2749.021, 743.02), Angle(-16.653, 29.038, -67.87) )
	Create_FrontlinerAmmo( Vector(353.277, 2736.18, 760.285), Angle(0, 109.899, 0) )
	Create_FrontlinerAmmo( Vector(378.16, 2757.653, 760.183), Angle(0, 152.051, 0) )
	Create_Medkit25( Vector(369.071, 2761.807, 712.33), Angle(0, 44.875, 0) )
	Create_Medkit25( Vector(-57.703, 977.427, -799.969), Angle(0, 248.01, 0) )
	Create_Beer( Vector(-84.791, 963.969, -799.361), Angle(0, 325.977, 0) )
	Create_Backpack( Vector(-26.178, 974.71, -799.962), Angle(0, 253.114, 0) )
	
	local GAME_END_TXT = {
	"After porting out of that sorry situation, Griseus wanted to apologise",
    "for what happened, however Kit was having none of it, she just went back",
    "to her appartment to chill out and watch TV with a smirk on her face.",
    "",
    "She secretly enjoyed it, her job is basically killing after all...",
    "",
    "",
    "Not like Griseus can complain, she did her job and then some, despite how",
    "it was meant to be a vacation. Who knows where she'll end up next."}
	
	MakeGameEnd( GAME_END_TXT, 35, Vector(1033.932,1005.031,-10815.969), Vector(-150,-150,0), Vector(150,150,200) )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)