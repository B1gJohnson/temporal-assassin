
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	CreatePlayerSpawn( Vector(-4102.75,-3522.25,128), Angle(0,76,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then

		Create_KingSlayerWeapon( Vector(-3114.3854980469, -3131.1625976563, 170.03125), Angle(0, 179.47828674316, 0) )
		Create_KingSlayerAmmo( Vector(-3113.8908691406, -3076.1508789063, 128.03125), Angle(0, 181.4582824707, 0) )
		Create_KingSlayerAmmo( Vector(-3114.7927246094, -3116.9670410156, 128.03125), Angle(0, 179.25828552246, 0) )
		Create_RevolverAmmo( Vector(-3431.6979980469, -3168.4108886719, 128.03125), Angle(0, 21.958190917969, 0) )
		Create_RevolverWeapon( Vector(-3396.6508789063, -3171.1584472656, 128.03125), Angle(0, 81.578193664551, 0) )
		Create_ShotgunWeapon( Vector(-3312.2351074219, -3041.3527832031, 128.03125), Angle(0, 270.77813720703, 0) )
		Create_Medkit10( Vector(-3236.5354003906, -4553.4975585938, 192.03125), Angle(0, 107.23865509033, 0) )
		Create_Medkit10( Vector(-3290.6811523438, -4550.7182617188, 192.03125), Angle(0, 73.358573913574, 0) )
		Create_SMGWeapon( Vector(-2622.599609375, -3456.8395996094, 248.20654296875), Angle(0, 206.01844787598, 0) )
		Create_Medkit25( Vector(-1128.0443115234, -4031.59765625, 320.03125), Angle(0, 181.78842163086, 0) )
		Create_SuperShotgunWeapon( Vector(-1209.0581054688, -3647.1079101563, 320.03125), Angle(0, 179.3683013916, 0) )
		Create_PistolAmmo( Vector(-2013.5927734375, -5361.4287109375, 362.03125), Angle(0, 14.726821899414, 0) )
		Create_PistolAmmo( Vector(-2011.9512939453, -5338.9580078125, 362.03125), Angle(0, 354.48669433594, 0) )
		Create_Armour( Vector(-2012.5166015625, -5322.1484375, 320.03125), Angle(0, 332.48663330078, 0) )
		Create_SpiritSphere( Vector(-1055.9105224609, -4945.0615234375, 320.03125), Angle(0, 85.997550964355, 0) )
		
		CreateSecretArea( Vector(-1057.6665039063,-4937.12890625,320.03125), Vector(-21.085548400879,-21.085548400879,0), Vector(21.085548400879,21.085548400879,61.499938964844) )
		
		CreateProp(Vector(7602.09375,-2257.21875,2056.90625), Angle(5.009765625,-96.6796875,0.9228515625), "models/temporalassassin/doll/unknown.mdl", true)
		
	else
	
		Create_Armour( Vector(-3113.127, -3155.594, 170.031), Angle(0, 151.468, 0) )
		Create_Medkit25( Vector(-3111.79, -3111.718, 170.031), Angle(0, 171.532, 0) )
		Create_Medkit25( Vector(-3113.75, -3120.175, 128.031), Angle(0, 162.336, 0) )
		Create_Bread( Vector(-3425.247, -3165.403, 128.031), Angle(0, 358.168, 0) )
		Create_Beer( Vector(-3412.221, -3175.172, 128.031), Angle(0, 41.432, 0) )
		Create_ManaCrystal100( Vector(-3412.557, -3053.402, 128.031), Angle(0, 319.294, 0) )
		Create_RevolverWeapon( Vector(-3227.528, -4567.532, 192.031), Angle(0, 112.7, 0) )
		Create_RevolverAmmo( Vector(-3272.034, -4567.563, 192.031), Angle(0, 78.842, 0) )
		Create_Medkit10( Vector(-2845.16, -4271.437, 256.031), Angle(0, 81.462, 0) )
		Create_Medkit10( Vector(-2861.945, -4248.748, 256.031), Angle(0, 68.713, 0) )
		Create_ShotgunWeapon( Vector(-2795.093, -4261.272, 256.031), Angle(0, 88.986, 0) )
		Create_SuperShotgunWeapon( Vector(-2793.388, -4234.972, 256.031), Angle(0, 90.031, 0) )
		Create_VolatileBattery( Vector(-1056.832, -4961.99, 320.031), Angle(0, 90.672, 0) )
		Create_SpiritSphere( Vector(-1055.288, -4929.028, 320.031), Angle(0, 86.701, 0) )
		Create_FoolsOathAmmo( Vector(-38.461, -6405.02, 170.031), Angle(0, 68.616, 0) )
		Create_FalanranWeapon( Vector(-104.509, -6350.785, 128.031), Angle(0, 172.828, 0) )
		Create_KingSlayerAmmo( Vector(-219.378, -6405.739, 128.031), Angle(0, 61.849, 0) )
		Create_KingSlayerWeapon( Vector(-170.093, -6402.2, 128.031), Angle(0, 112.218, 0) )
		Create_RifleWeapon( Vector(-464.391, -5422.996, 128.031), Angle(0, 295.613, 0) )
		Create_RifleAmmo( Vector(-491.074, -5469.196, 128.031), Angle(0, 348.49, 0) )
		Create_PistolAmmo( Vector(-20.565, -5264.506, 192.031), Angle(0, 212.849, 0) )
		Create_PistolAmmo( Vector(-16.585, -5296.434, 192.031), Angle(0, 176.692, 0) )
		Create_Armour100( Vector(-1059.126, -5277.283, 320.031), Angle(0, 88.095, 0) )
		Create_Beer( Vector(-2014.573, -5362.646, 362.031), Angle(0, 48.594, 0) )
		Create_Beer( Vector(-2012.891, -5346.945, 362.031), Angle(0, 22.051, 0) )
	
		CreateSecretArea( Vector(-1050.402,-4903.123,320.031), Vector(-25.157,-25.157,0), Vector(25.157,25.157,70.634) )
		
		CreateProp(Vector(7602.09375,-2257.21875,2056.90625), Angle(5.009765625,-96.6796875,0.9228515625), "models/temporalassassin/doll/unknown.mdl", true)
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)