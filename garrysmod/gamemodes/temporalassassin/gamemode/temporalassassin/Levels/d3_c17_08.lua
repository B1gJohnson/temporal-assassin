	
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	CreatePlayerSpawn( Vector(-2242,-3239.375,232.125), Angle(0,90,0) )
	
	CreateProp(Vector(-2239.437,-3035.937,318.656), Angle(-89.473,-10.811,-79.233), "models/props_c17/playground_teetertoter_seat.mdl", false)
	
	local MTHUULRA_HELP_START = function()
		
		local MTHUUL_HELP = function()
		
			net.Start("DollMessage")
			net.WriteString("Really kiddo...")
			net.Broadcast()
			
			timer.Simple(4,function()
				net.Start("DollMessage")
				net.WriteString("Fine, here")
				net.Broadcast()
				
				CreateTeleporter( "H1", Vector(-753.429,-1444.679,-215.969), "H2", Vector(-612.959,-1436.245,-215.969) )
			end)
		
		end
		timer.Create("Mthuulra_Help_Timer", 30, 1, MTHUUL_HELP)
	
	end
	
	local MTHUULRA_HELP_END = function()
		timer.Remove("Mthuulra_Help_Timer")
	end
	
	timer.Simple(1,function()
	
		local E1 = GetEntityByID(2171)
		local E2 = GetEntityByID(1743)
		
		if E1 and IsValid(E1) then
			E1:Remove()
		end
		
		if E2 and IsValid(E2) then
			E2:Remove()
		end
		
	end)
	
	CreateTrigger( Vector(-763.639,-1445.283,-215.969), Vector(-56.395,-56.395,0), Vector(56.395,56.395,127.938), MTHUULRA_HELP_START )
	CreateTrigger( Vector(-596.856,-1422.819,-215.969), Vector(-80.627,-80.627,0), Vector(80.627,80.627,127.938), MTHUULRA_HELP_END )

	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_SpiritSphere( Vector(-3506.2060546875, -3375.7316894531, 467.43496704102), Angle(0, 355.29992675781, 0) )
		Create_PistolAmmo( Vector(-1812.2423095703, -3000.8674316406, 104.03125), Angle(0, 88.660285949707, 0) )
		Create_PistolAmmo( Vector(-1852.4361572266, -2999.8298339844, 104.03125), Angle(0, 64.020225524902, 0) )
		Create_SMGAmmo( Vector(-1811.7235107422, -2952.6853027344, 104.03125), Angle(0, 87.780227661133, 0) )
		Create_SMGAmmo( Vector(-1849.3466796875, -2950.6594238281, 104.03125), Angle(0, 43.780136108398, 0) )
		Create_RevolverAmmo( Vector(-1835.3770751953, -2976.1076660156, 104.03125), Angle(0, 68.200248718262, 0) )
		Create_Medkit10( Vector(-1369.9758300781, -2064.8486328125, 113.63583374023), Angle(0, 210.40466308594, 0) )
		Create_Medkit10( Vector(-1369.1927490234, -2104.0593261719, 114.10816955566), Angle(0, 190.82467651367, 0) )
		Create_PortableMedkit( Vector(-1555.7478027344, -1974.8571777344, 125.69971466064), Angle(0, 52.364486694336, 0) )
		Create_M6GAmmo( Vector(-1544.1837158203, -1309.9073486328, 109.41588592529), Angle(0, 294.44454956055, 0) )
		Create_VolatileBattery( Vector(-1888.2229003906, -1388.3771972656, 104.03125), Angle(0, 331.40444946289, 0) )
		Create_SMGAmmo( Vector(-1882.4840087891, -1486.6284179688, 104.03125), Angle(0, 11.664596557617, 0) )
		Create_SMGAmmo( Vector(-1882.7335205078, -1443.8587646484, 104.03125), Angle(0, 352.52447509766, 0) )
		Create_SMGAmmo( Vector(-1845.7777099609, -1481.2197265625, 104.03125), Angle(0, 12.984588623047, 0) )
		Create_SMGAmmo( Vector(-1841.3525390625, -1444.5791015625, 104.03125), Angle(0, 349.44448852539, 0) )
		Create_Armour100( Vector(-2588.4934082031, -1958.5372314453, 104.03125), Angle(0, 102.37724304199, 0) )
		Create_Medkit25( Vector(-2635.8823242188, -1962.8709716797, 104.03125), Angle(0, 83.237243652344, 0) )
		Create_Medkit25( Vector(-2694.2116699219, -1947.1328125, 104.03125), Angle(0, 55.077102661133, 0) )
		Create_PortableMedkit( Vector(-2575.6018066406, -1894.2216796875, 104.03125), Angle(0, 117.99725341797, 0) )
		Create_MegaSphere( Vector(-1195.2890625, -1119.4008789063, 264.03125), Angle(0, 0.73606872558594, 0) )
		Create_Backpack( Vector(-1206.2047119141, -1865.4379882813, 104.03125), Angle(0, 3.7361297607422, 0) )
		Create_ShotgunAmmo( Vector(-918.05883789063, -1866.0469970703, -55.968746185303), Angle(0, 172.77578735352, 0) )
		Create_ShotgunAmmo( Vector(-916.76275634766, -1846.3594970703, -55.968746185303), Angle(0, 187.07579040527, 0) )
		Create_Armour( Vector(-881.51678466797, -1437.7408447266, -55.96875), Angle(0, 177.17572021484, 0) )
		Create_Medkit25( Vector(-1238.5430908203, -1439.7957763672, -215.96875), Angle(0, 0.611572265625, 0) )
		Create_Backpack( Vector(-1139.7283935547, -1813.7197265625, -391.96875), Angle(0, 352.88122558594, 0) )
		Create_Backpack( Vector(-1144.2404785156, -1844.9311523438, -391.96875), Angle(0, 0.1412353515625, 0) )
		Create_Backpack( Vector(-1138.1740722656, -1874.8247070313, -391.96875), Angle(0, 22.801345825195, 0) )
		Create_PistolAmmo( Vector(-850.11779785156, -1537.2092285156, -215.96875), Angle(0, 291.44638061523, 0) )
		Create_PistolAmmo( Vector(-818.39855957031, -1537.2854003906, -215.96875), Angle(0, 267.68634033203, 0) )
		Create_RevolverAmmo( Vector(-468.43008422852, -1390.9794921875, -207.96875), Angle(0, 5.5866241455078, 0) )
		Create_RevolverAmmo( Vector(-439.92666625977, -1374.646484375, -207.96875), Angle(0, 354.36657714844, 0) )
		Create_KingSlayerAmmo( Vector(223.64727783203, -2224.4711914063, -127.96875), Angle(0, 90.567169189453, 0) )
		Create_KingSlayerAmmo( Vector(190.98120117188, -2204.64453125, -127.96875), Angle(0, 68.787055969238, 0) )
		Create_KingSlayerAmmo( Vector(221.84588623047, -2183.7626953125, -127.96875), Angle(0, 89.247108459473, 0) )
		Create_Armour200( Vector(1367.8054199219, -1851.9072265625, -319.96875), Angle(0, 89.795745849609, 0) )
		Create_Medkit25( Vector(1388.3980712891, -1820.9729003906, -319.96875), Angle(0, 103.2158203125, 0) )
		Create_Medkit25( Vector(1344.44140625, -1822.8012695313, -319.96875), Angle(0, 74.83570098877, 0) )
		Create_PortableMedkit( Vector(1367.5550537109, -1794.3637695313, -319.96875), Angle(0, 89.355781555176, 0) )
		Create_SpiritEye2( Vector(2439.3708496094, -886.96051025391, -527.96875), Angle(0, 268.00537109375, 0) )
		Create_MegaSphere( Vector(2438.8181152344, -907.38336181641, -527.96875), Angle(0, 270.20538330078, 0) )
		Create_Armour( Vector(805.95892333984, -933.79541015625, -304), Angle(0, 126.77096557617, 0) )
		Create_Beer( Vector(272.80514526367, -1188.1156005859, -287.96875), Angle(0, 197.83102416992, 0) )
		Create_Beer( Vector(274.37414550781, -1231.8354492188, -287.96875), Angle(0, 157.791015625, 0) )
		Create_Beer( Vector(272.38873291016, -1277.8583984375, -287.96875), Angle(0, 129.63098144531, 0) )
		Create_Beer( Vector(268.85693359375, -1329.4580078125, -287.96875), Angle(0, 114.23094940186, 0) )
		Create_SuperShotgunWeapon( Vector(1264.0059814453, -844.37603759766, -364.19519042969), Angle(0, 268.45056152344, 0) )
		Create_ShotgunWeapon( Vector(1263.0844726563, -884.54217529297, -361.68478393555), Angle(0, 268.89056396484, 0) )
		Create_Beer( Vector(1197.3547363281, 167.99032592773, -239.96875), Angle(0, 242.05030822754, 0) )
		Create_Beer( Vector(1165.0866699219, 172.79321289063, -239.96875), Angle(0, 264.93041992188, 0) )
		Create_Beer( Vector(1125.4711914063, 168.92492675781, -239.96875), Angle(0, 293.53051757813, 0) )
		Create_Beer( Vector(1094.0704345703, 177.10852050781, -239.96875), Angle(0, 307.61059570313, 0) )
		Create_Beer( Vector(1059.1706542969, 167.92401123047, -239.96875), Angle(0, 323.23065185547, 0) )
		Create_Medkit25( Vector(1680.9864501953, 464.25457763672, 64.03125), Angle(0, 179.57055664063, 0) )
		Create_Armour( Vector(1414.1552734375, -1030.6945800781, 64.03125), Angle(0, 89.150566101074, 0) )
		Create_Medkit25( Vector(2088.4216308594, -62.723003387451, 624.03125), Angle(0, 194.0205078125, 0) )
		Create_Medkit25( Vector(2083.4011230469, -130.35034179688, 624.03125), Angle(0, 164.32051086426, 0) )
		Create_RevolverWeapon( Vector(2049.2841796875, -98.465606689453, 624.03125), Angle(0, 177.96051025391, 0) )
		Create_SMGAmmo( Vector(2014.3371582031, -122.57311248779, 624.03125), Angle(0, 159.26051330566, 0) )
		Create_SMGAmmo( Vector(2013.6954345703, -68.581237792969, 624.03125), Angle(0, 197.98051452637, 0) )
		Create_SMGAmmo( Vector(1987.2277832031, -94.081184387207, 624.03125), Angle(0, 178.40051269531, 0) )
		
		CreateSecretArea( Vector(-1188.0667724609,-1119.3073730469,264.03125), Vector(-57.589889526367,-57.589889526367,0), Vector(57.589889526367,57.589889526367,114.12869262695) )
		CreateSecretArea( Vector(1367.3050537109,-1793.1069335938,-315.4375), Vector(-40.699638366699,-40.699638366699,0), Vector(40.699638366699,40.699638366699,41.293975830078) )
		CreateSecretArea( Vector(2441.1804199219,-914.67736816406,-527.96875), Vector(-57.160167694092,-57.160167694092,0), Vector(57.160167694092,57.160167694092,110.4577331543) )
		
	else
	
		CreateProp(Vector(1235.813,-1034.25,-517.687), Angle(-0.22,99.141,-0.352), "models/temporalassassin/doll/unknown.mdl", true)
	
		Create_Medkit25( Vector(-1831.597, -2989.073, 104.031), Angle(0, 80.1, 0) )
		Create_EldritchArmour10( Vector(-1542.921, -3203.271, 232.031), Angle(0, 28.895, 0) )
		Create_EldritchArmour10( Vector(-1501.453, -3201.863, 232.031), Angle(0, 41.435, 0) )
		Create_RifleAmmo( Vector(-1670.832, -2383.767, 166.026), Angle(0, 348.032, 0) )
		Create_RifleAmmo( Vector(-1668.496, -2419.055, 164.784), Angle(0, 19.174, 0) )
		Create_Medkit10( Vector(-1667.776, -2694.877, 161.386), Angle(0, 349.076, 0) )
		Create_Medkit25( Vector(-1664.662, -2732.987, 157.035), Angle(0, 15.62, 0) )
		Create_SpiritSphere( Vector(-2683.223, -1763.075, 104.031), Angle(0, 305.982, 0) )
		Create_Armour( Vector(-1266.498, -1465.855, 112.031), Angle(0, 104.607, 0) )
		Create_ReikoriKey( Vector(-1165.661, -1122.358, 264.031), Angle(0, 1.259, 0) )
		Create_Armour200( Vector(-1212.886, -1154.388, 264.031), Angle(0, 5.439, 0) )
		Create_Medkit25( Vector(-1216.653, -1088.677, 264.031), Angle(0, 337.224, 0) )
		Create_Backpack( Vector(-1131.688, -1829.262, -391.969), Angle(0, 332.835, 0) )
		Create_Backpack( Vector(-1133.276, -1866.736, -391.969), Angle(0, 357.079, 0) )
		Create_InfiniteGrenades( Vector(-539.546, -1551.511, -215.969), Angle(0, 189.98, 0) )
		Create_Medkit25( Vector(-199.273, -1722.474, -287.969), Angle(0, 121.637, 0) )
		Create_Medkit25( Vector(-157.814, -1564.598, -287.969), Angle(0, 187.89, 0) )
		Create_MegaSphere( Vector(-329.767, -1395.752, 367.716), Angle(0, 355.504, 0) )
		Create_SpiritSphere( Vector(210.034, -2214.117, -127.969), Angle(0, 91.322, 0) )
		Create_Medkit25( Vector(84.864, -2065.696, -127.969), Angle(0, 8.767, 0) )
		Create_Medkit25( Vector(87.61, -2025.371, -127.969), Angle(0, 340.551, 0) )
		Create_CrossbowAmmo( Vector(-513.437, -1861.039, -287.969), Angle(0, 330.311, 0) )
		Create_CrossbowAmmo( Vector(-484.089, -1862.524, -287.969), Angle(0, 318.606, 0) )
		Create_KingSlayerAmmo( Vector(-519.836, -1948.255, -287.969), Angle(0, 26.636, 0) )
		Create_RifleAmmo( Vector(-480.941, -1940.025, -287.969), Angle(0, 36.773, 0) )
		Create_SuperShotgunWeapon( Vector(-446.69, -1902.706, -287.969), Angle(0, 2.287, 0) )
		Create_ShotgunWeapon( Vector(-468.303, -1904.995, -287.969), Angle(0, 3.541, 0) )
		Create_FoolsOathAmmo( Vector(1390.214, -1865.268, -319.969), Angle(0, 19.815, 0) )
		Create_FoolsOathAmmo( Vector(1345.52, -1864.587, -319.969), Angle(0, -27.21, 0) )
		Create_EldritchArmour( Vector(1368.202, -1865.801, -319.969), Angle(0, 89.333, 0) )
		Create_KingSlayerWeapon( Vector(955.388, -1411.7, -287.969), Angle(0, 184.691, 0) )
		Create_KingSlayerAmmo( Vector(919.428, -1446.005, -287.969), Angle(0, 171.106, 0) )
		Create_RifleAmmo( Vector(1685.292, -1401.962, -287.969), Angle(0, 195.559, 0) )
		Create_PistolAmmo( Vector(1681.787, -1467.087, -287.969), Angle(0, 156.894, 0) )
		Create_PistolAmmo( Vector(1686.117, -1436.129, -287.969), Angle(0, 175.286, 0) )
		Create_FoolsOathAmmo( Vector(1659.272, -1456.892, -287.969), Angle(0, 67.312, 0) )
		Create_ManaCrystal100( Vector(1262.895, -1035.703, -352.237), Angle(0, 264.84, 0) )
		Create_InfiniteGrenades( Vector(1131.376, -1080.367, -383.969), Angle(0, 289.502, 0) )
		Create_Backpack( Vector(2420.693, -857.553, -527.969), Angle(0, 278.109, 0) )
		Create_Backpack( Vector(2454.989, -859.88, -527.969), Angle(0, 264.524, 0) )
		Create_MegaSphere( Vector(2437.763, -902.681, -527.969), Angle(0, 272.048, 0) )
		Create_EldritchArmour( Vector(2439.198, -880.696, -527.969), Angle(0, 271.003, 0) )
		Create_Beer( Vector(1199.034, 173.615, -239.969), Angle(0, 252.193, 0) )
		Create_Beer( Vector(1152.248, 167.767, -239.969), Angle(0, 271.421, 0) )
		Create_Beer( Vector(1067.224, 169.087, -239.969), Angle(0, 290.649, 0) )
		Create_Beer( Vector(1105.661, 166.803, -239.969), Angle(0, 272.675, 0) )
		Create_InfiniteGrenades( Vector(1443.736, -1023.785, 64.031), Angle(0, 119.895, 0) )
		Create_RevolverWeapon( Vector(1109.143, -1028.181, 64.031), Angle(0, 89.279, 0) )
		Create_RevolverAmmo( Vector(1078.069, -1002.435, 64.031), Angle(0, 67.334, 0) )
		Create_RevolverAmmo( Vector(1114.164, -990.671, 64.031), Angle(0, 93.25, 0) )
		Create_SpiritEye1( Vector(1251.114, -923.178, 64.031), Angle(0, 142.574, 0) )
		Create_Armour100( Vector(903.519, 613.593, 400.031), Angle(0, 350.579, 0) )
		Create_Medkit25( Vector(889.35, 575.88, 400.031), Angle(0, 359.044, 0) )
		Create_Medkit25( Vector(915.469, 534.222, 400.031), Angle(0, 22.974, 0) )
		Create_RifleWeapon( Vector(1555.049, 586.17, 400.031), Angle(0, 180.56, 0) )
		Create_RifleAmmo( Vector(1568.342, 527.249, 400.031), Angle(0, 152.554, 0) )
		Create_SMGWeapon( Vector(1514.484, 584.432, 400.031), Angle(0, 171.991, 0) )
		Create_SMGAmmo( Vector(1533.417, 524.488, 400.031), Angle(0, 139.596, 0) )
		Create_SMGAmmo( Vector(1537.326, 547.991, 400.031), Angle(0, 151.823, 0) )
		Create_MaskOfShadows( Vector(1533.602, -907.365, 80.031), Angle(0, 97.796, 0) )
		Create_Medkit25( Vector(1021.171, -46.746, 944.031), Angle(0, 181.503, 0) )
		Create_Bread( Vector(1754.553, 106.334, 768.031), Angle(0, 227.901, 0) )
		Create_Bread( Vector(1757.214, 58.789, 768.031), Angle(0, 207.523, 0) )
		Create_FalanrathThorn( Vector(1359.397, -726.681, 64.031), Angle(0, 173.877, 0) )
	
		CreateSecretArea( Vector(-1163.274,-1122.305,266.157), Vector(-50.635,-50.635,0), Vector(50.635,50.635,99.464) )
		CreateSecretArea( Vector(-1121.262,-1848.185,-391.969), Vector(-38.682,-38.682,0), Vector(38.682,38.682,34.738) )
		CreateSecretArea( Vector(-332.069,-1396.61,367.686), Vector(-85.365,-85.365,0), Vector(85.365,85.365,94.662) )
		CreateSecretArea( Vector(1366.234,-1863.597,-315.437), Vector(-36.693,-36.693,0), Vector(36.693,36.693,43.406) )
		CreateSecretArea( Vector(2437.242,-898.719,-524.846), Vector(-40.243,-40.243,0), Vector(40.243,40.243,108.815) )
		
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)