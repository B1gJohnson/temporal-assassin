
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	
	CreatePlayerSpawn( Vector(4517.4565429688,-2744.173828125,-3767.96875), Angle(0,90,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Backpack( Vector(3599.833984375, -2218.5344238281, -3903.96875), Angle(0, 269.93161010742, 0) )
		Create_VolatileBattery( Vector(3578.7290039063, -2217.9692382813, -3903.96875), Angle(0, 269.05166625977, 0) )
		Create_Armour( Vector(4574.7744140625, -1676.4632568359, -3807.96875), Angle(0, 173.79153442383, 0) )
		Create_PortableMedkit( Vector(4581.5029296875, -1628.2698974609, -3807.96875), Angle(0, 180.17155456543, 0) )
		Create_PortableMedkit( Vector(3583.0336914063, -1709.8399658203, -3695.96875), Angle(0, 25.23860168457, 0) )
		Create_PortableMedkit( Vector(3608.7385253906, -1710.2150878906, -3695.96875), Angle(0, 32.27864074707, 0) )
		Create_PortableMedkit( Vector(3580.0849609375, -1689.3413085938, -3695.96875), Angle(0, 14.898544311523, 0) )
		Create_M6GAmmo( Vector(3600.1687011719, -1691.9307861328, -3695.96875), Angle(0, 10.938385009766, 0) )
		Create_Beer( Vector(2701.4409179688, -1171.5146484375, -3839.96875), Angle(0, 282, 0) )
		Create_Beer( Vector(2692.4936523438, -1174.4217529297, -3839.96875), Angle(0, 294, 0) )
		Create_Beer( Vector(2684.3464355469, -1179.1254882813, -3839.96875), Angle(0, 306, 0) )
		Create_Beer( Vector(2677.35546875, -1185.4204101563, -3839.96875), Angle(0, 318, 0) )
		Create_Beer( Vector(2671.8256835938, -1193.03125, -3839.96875), Angle(0, 330, 0) )
		Create_Beer( Vector(2667.9992675781, -1201.6254882813, -3839.96875), Angle(0, 342, 0) )
		Create_Beer( Vector(2666.0434570313, -1210.8275146484, -3839.96875), Angle(0, 354, 0) )
		Create_Beer( Vector(2666.0434570313, -1220.2349853516, -3839.96875), Angle(0, 366, 0) )
		Create_Beer( Vector(2667.9992675781, -1229.4370117188, -3839.96875), Angle(0, 378, 0) )
		Create_Beer( Vector(2671.8256835938, -1238.03125, -3839.96875), Angle(0, 390, 0) )
		Create_Beer( Vector(2677.35546875, -1245.6420898438, -3839.96875), Angle(0, 402, 0) )
		Create_Beer( Vector(2684.3464355469, -1251.9370117188, -3839.96875), Angle(0, 414, 0) )
		Create_Beer( Vector(2692.4936523438, -1256.6407470703, -3839.96875), Angle(0, 426, 0) )
		Create_Beer( Vector(2701.4409179688, -1259.5478515625, -3839.96875), Angle(0, 438, 0) )
		Create_Beer( Vector(2710.796875, -1260.53125, -3839.96875), Angle(0, 450, 0) )
		Create_Beer( Vector(2720.1528320313, -1259.5478515625, -3839.96875), Angle(0, 462, 0) )
		Create_Beer( Vector(2729.1000976563, -1256.6407470703, -3839.96875), Angle(0, 474, 0) )
		Create_Beer( Vector(2737.2473144531, -1251.9370117188, -3839.96875), Angle(0, 486, 0) )
		Create_Beer( Vector(2744.23828125, -1245.6420898438, -3839.96875), Angle(0, 498, 0) )
		Create_Beer( Vector(2749.7680664063, -1238.03125, -3839.96875), Angle(0, 510, 0) )
		Create_Beer( Vector(2753.5944824219, -1229.4370117188, -3839.96875), Angle(0, 522, 0) )
		Create_Beer( Vector(2755.5502929688, -1220.2349853516, -3839.96875), Angle(0, 534, 0) )
		Create_Beer( Vector(2755.5502929688, -1210.8275146484, -3839.96875), Angle(0, 546, 0) )
		Create_Beer( Vector(2753.5944824219, -1201.6254882813, -3839.96875), Angle(0, 558, 0) )
		Create_Beer( Vector(2749.7680664063, -1193.03125, -3839.96875), Angle(0, 570, 0) )
		Create_Beer( Vector(2744.23828125, -1185.4204101563, -3839.96875), Angle(0, 582, 0) )
		Create_Beer( Vector(2737.2473144531, -1179.1254882813, -3839.96875), Angle(0, 594, 0) )
		Create_Beer( Vector(2729.1000976563, -1174.4217529297, -3839.96875), Angle(0, 606, 0) )
		Create_Beer( Vector(2720.1528320313, -1171.5146484375, -3839.96875), Angle(0, 618, 0) )
		Create_Beer( Vector(2710.796875, -1170.53125, -3839.96875), Angle(0, 630, 0) )
		Create_ShotgunAmmo( Vector(456.36215209961, -1631.8240966797, -3499.96875), Angle(0, 88.596694946289, 0) )
		Create_ShotgunAmmo( Vector(457.74649047852, -1573.5765380859, -3499.96875), Angle(0, 88.376693725586, 0) )
		Create_ShotgunAmmo( Vector(456.52368164063, -1515.2821044922, -3499.96875), Angle(0, 88.376678466797, 0) )
		Create_KingSlayerAmmo( Vector(358.59747314453, -607.77111816406, -3487.96875), Angle(0, 270.08483886719, 0) )
		Create_KingSlayerAmmo( Vector(329.36059570313, -659.49566650391, -3487.96875), Angle(0, 0.72488403320313, 0) )
		Create_Armour200( Vector(263.58972167969, -1145.3530273438, -3267.96875), Angle(0, 270.91424560547, 0) )
		Create_SuperShotgunWeapon( Vector(1305.8065185547, -477.22943115234, -3517.96875), Angle(0, 86.334121704102, 0) )
		Create_ShotgunAmmo( Vector(1319.7326660156, -444.57611083984, -3517.96875), Angle(0, 100.41421508789, 0) )
		Create_ShotgunAmmo( Vector(1294.5296630859, -444.96090698242, -3517.96875), Angle(0, 71.154067993164, 0) )
		
		CreateSecretArea( Vector(3568.5891113281,-2245.7019042969,-3903.96875), Vector(-59.591701507568,-59.591701507568,0), Vector(59.591701507568,59.591701507568,147.77807617188) )
		CreateSecretArea( Vector(3597.2834472656,-1698.9342041016,-3692.9375), Vector(-28.979955673218,-28.979955673218,0), Vector(28.979955673218,28.979955673218,57.009765625) )
		CreateSecretArea( Vector(263.74035644531,-1148.6727294922,-3264.9375), Vector(-35.041397094727,-35.041397094727,0), Vector(35.041397094727,35.041397094727,91.477783203125) )
		
		CreateProp(Vector(2686.75,-793.15625,-3482.1875), Angle(30.5859375,-96.8115234375,-3.4716796875), "models/temporalassassin/doll/unknown.mdl", true)
		
	else
	
		Create_Backpack( Vector(3603.43, -2214.95, -3903.969), Angle(0, 270.421, 0) )
		Create_RifleWeapon( Vector(3558.455, -2218.37, -3903.969), Angle(0, 270.212, 0) )
		Create_RifleAmmo( Vector(4554.694, -1691.145, -3807.969), Angle(0, 209.392, 0) )
		Create_Armour( Vector(4555.212, -1619.252, -3807.969), Angle(0, 195.598, 0) )
		Create_Bread( Vector(4555.762, -1511.264, -3807.969), Angle(0, 171.563, 0) )
		Create_Beer( Vector(4562.018, -1456.215, -3839.969), Angle(0, 216.916, 0) )
		Create_Beer( Vector(4539.169, -1491.113, -3839.969), Angle(0, 196.747, 0) )
		Create_Medkit10( Vector(3875.508, -1068.729, -3783.969), Angle(0, 20.665, 0) )
		Create_Medkit10( Vector(3891.68, -1047.13, -3820.923), Angle(-79.997, -8.597, -79.89) )
		Create_ShotgunAmmo( Vector(3923.874, -1001.027, -3818.068), Angle(-79.683, 97.778, -0.11) )
		Create_Beer( Vector(2746.448, -1171.271, -3839.969), Angle(0, 266.441, 0) )
		Create_Beer( Vector(2758.903, -1218.071, -3839.969), Angle(0, 245.541, 0) )
		Create_Beer( Vector(2734.302, -1260.599, -3839.969), Angle(0, 252.02, 0) )
		Create_Beer( Vector(2670.842, -1164.402, -3839.969), Angle(0, 166.539, 0) )
		Create_Beer( Vector(2658.277, -1218.599, -3839.969), Angle(0, 106.974, 0) )
		Create_Beer( Vector(2674.639, -1269.806, -3839.969), Angle(0, 108.437, 0) )
		Create_Armour5( Vector(2562.901, -982.76, -3839.969), Angle(0, 77.821, 0) )
		Create_Armour5( Vector(2556.778, -1041.268, -3807.969), Angle(0, 82.628, 0) )
		Create_Armour5( Vector(2554.791, -1091.865, -3775.969), Angle(0, 87.226, 0) )
		Create_SpiritSphere( Vector(2523.749, -1435.213, -3775.969), Angle(0, 269.683, 0) )
		Create_GrenadeAmmo( Vector(2089.161, -1420.51, -3839.969), Angle(0, 255.16, 0) )
		Create_Medkit25( Vector(1439.575, -1695.954, -3775.969), Angle(0, 21.812, 0) )
		Create_SMGAmmo( Vector(1559.302, -1824.382, -3775.969), Angle(0, 7.181, 0) )
		Create_SMGAmmo( Vector(1589.416, -1831.881, -3775.969), Angle(0, 15.333, 0) )
		Create_SMGWeapon( Vector(1577.343, -1849.528, -3752.914), Angle(0, 15.331, -74.46) )
		Create_ShotgunAmmo( Vector(1561.407, -1769.499, -3775.969), Angle(0, 350.671, 0) )
		Create_ShotgunAmmo( Vector(1551.266, -1740.712, -3775.969), Angle(0, 329.561, 0) )
		Create_SuperShotgunWeapon( Vector(1623.377, -1739.455, -3751.495), Angle(17.969, 105.198, -83.41) )
		Create_Armour( Vector(444.197, -1641.581, -3499.969), Angle(0, 77.549, 0) )
		Create_Armour( Vector(467.246, -1623.134, -3499.969), Angle(0, 99.912, 0) )
		Create_Armour5( Vector(444.105, -1614.856, -3499.969), Angle(0, 69.607, 0) )
		Create_Armour5( Vector(467.924, -1645.942, -3499.969), Angle(0, 86.118, 0) )
		Create_RevolverAmmo( Vector(363.841, -583.57, -3457.969), Angle(0, 304.416, 0) )
		Create_RevolverAmmo( Vector(349.255, -583.716, -3457.969), Angle(0, 326.361, 0) )
		Create_Medkit25( Vector(664.51, -1095.559, -3775.969), Angle(0, 175.6, 0) )
		Create_ShotgunWeapon( Vector(1516.803, -1082.954, -3775.969), Angle(0, 113.945, 0) )
		Create_M6GAmmo( Vector(1471.528, -1077.733, -3775.969), Angle(0, 77.684, 0) )
		Create_M6GAmmo( Vector(1457.168, -1048.526, -3775.969), Angle(0, 51.35, 0) )
		Create_PistolAmmo( Vector(1249.716, -547.395, -3775.969), Angle(0, 294.937, 0) )
		Create_PistolAmmo( Vector(1305.937, -545.562, -3775.969), Angle(0, 256.272, 0) )
		Create_Bread( Vector(1279.544, -570.686, -3775.969), Angle(0, 276.963, 0) )
		Create_Armour5( Vector(268.846, -601.086, -3647.969), Angle(0, 205.694, 0) )
		Create_Armour5( Vector(236.24, -651.698, -3647.969), Angle(0, 151.145, 0) )
		Create_Beer( Vector(266.981, -654.02, -3647.969), Angle(0, 162.013, 0) )
		Create_Beer( Vector(238.536, -610.324, -3647.969), Angle(0, 212.173, 0) )
		Create_RifleAmmo( Vector(256.09, -632.087, -3647.969), Angle(0, 181.032, 0) )
		Create_CrossbowAmmo( Vector(792.726, -814.496, -3487.969), Angle(0, 187.616, 0) )
		Create_KingSlayerAmmo( Vector(791.869, -677.413, -3487.969), Angle(0, 191.587, 0) )
		Create_SpiritEye2( Vector(270.078, -766.08, -3263.969), Angle(0, 0.77, 0) )
		
		CreateSecretArea( Vector(3584.396,-2257.429,-3903.969), Vector(-43.199,-43.199,0), Vector(43.199,43.199,103.92) )
		CreateSecretArea( Vector(2525.191,-1464.724,-3775.969), Vector(-26.6,-26.6,0), Vector(26.6,26.6,127.938) )
		CreateSecretArea( Vector(454.66,-1629.322,-3495.437), Vector(-35.93,-35.93,0), Vector(35.93,35.93,43.406) )
		CreateSecretArea( Vector(305.821,-763.977,-3263.969), Vector(-56.285,-56.285,0), Vector(56.285,56.285,80.277) )
		
		local SC_PROP_1 = CreateProp(Vector(3626.313,-1106.062,-3624.594), Angle(-0.044,179.912,-0.044), "models/props_wasteland/barricade001a.mdl", true)
		local SC_PROP_2 = CreateProp(Vector(3762.219,-1105.406,-3624.625), Angle(0,179.956,0), "models/props_wasteland/barricade001a.mdl", true)
		SC_PROP_1:SetHealth(99999)
		SC_PROP_2:SetHealth(99999)
		
		local SECRET_CHASE_1 = function()
		
			local DOLL_CHASE_1 = CreateProp(Vector(2547.469,-1708.125,-3775.469), Angle(0.044,54.009,0), "models/temporalassassin/doll/unknown.mdl", true)
			
			local SECRET_CHASE_2 = function()
			
				CreateParticleEffect( "Eldritch_Disappear", DOLL_CHASE_1:GetBoxCenter(), Angle(0,0,0), 1 )
				sound.Play( "TemporalAssassin/Doll/Doll_Teleported.wav", DOLL_CHASE_1:GetBoxCenter(), 140, 100, 1, CHAN_STATIC )
				
				timer.Simple(0,function()
					if IsValid(DOLL_CHASE_1) then
						DOLL_CHASE_1:SetPos( Vector(2547.469,-1708.125,-50000.375) )
						DOLL_CHASE_1:Remove()
					end
				end)
				
				local SC_PROP_3 = CreateProp(Vector(2538.125,-1732.906,-3764.656), Angle(0,-90,-90), "models/props_trainstation/tracksign03.mdl", true)
				
				local SECRET_CHASE_3 = function()
				
					local DOLL_CHASE_2 = CreateProp(Vector(858.688,-988.469,-3775.562), Angle(0.088,-53.965,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
					
					local SECRET_CHASE_4 = function()
					
						CreateParticleEffect( "Eldritch_Disappear", DOLL_CHASE_2:GetBoxCenter(), Angle(0,0,0), 1 )
						sound.Play( "TemporalAssassin/Doll/Doll_Teleported.wav", DOLL_CHASE_2:GetBoxCenter(), 140, 100, 1, CHAN_STATIC )
				
						timer.Simple(0,function()
							if IsValid(DOLL_CHASE_2) then
								DOLL_CHASE_2:SetPos( Vector(858.688,-988.469,-50000.375) )
								DOLL_CHASE_2:Remove()
							end
						end)
						
						local SC_PROP_4 = CreateProp(Vector(861.469,-932.062,-3698.031), Angle(0,-90,0), "models/props_trainstation/tracksign02.mdl", true)
						
						local SECRET_CHASE_5 = function()
						
							net.Start("DollMessage")
							net.WriteString("Okay Kiddo, You win, he's around here somewhere.")
							net.Broadcast()
							
							Create_FalanrathThorn( Vector(217.472, -1086.877, -3775.969), Angle(0, 6.699, 0) )							
							CreateSecretArea( Vector(217,-1086,-3775), Vector(-60,-60,0), Vector(60,60,60), "YOU FOUND A SUPER SECRET" )
						
						end
						
						AddSecretButton( Vector(862.087,-960.031,-3750.985), SECRET_CHASE_5 )
					
					end
				
					CreateTrigger( Vector(867.473,-997.902,-3775.969), Vector(-99.773,-99.773,0), Vector(99.773,99.773,129.959), SECRET_CHASE_4 )
				
				end
				
				AddSecretButton( Vector(2544.856,-1727.969,-3752.13), SECRET_CHASE_3 )
			
			end
			
			CreateTrigger( Vector(2634.344,-1658.29,-3841.979), Vector(-102.213,-102.213,0), Vector(102.213,102.213,197.007), SECRET_CHASE_2 )
		
		end
		
		AddSecretButton( Vector(3697.538,-1088.031,-3584.204), SECRET_CHASE_1 )
	
	end
	
	timer.Simple(5,function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_3.txt","DATA") then
			Create_LoreItem( Vector(2484.508,-1628.85,-3775.969), Angle(0,177.179,0), "lore_mthuulra_3", "Lore File Unlocked: Mthuulra #3" )
		end
	end)
	
	local REMIND_LORE = function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_3.txt","DATA") then
			net.Start("DollMessage")
			net.WriteString("There is another book here past this point")
			net.Broadcast()
		end
	end
	
	CreateTrigger( Vector(2711.607,-1288.518,-3839.969), Vector(-223.314,-223.314,0), Vector(223.314,223.314,326.853), REMIND_LORE )
	
	CreateRestingPoint( Vector(4469.988,-3042.695,-4023.969), Angle(0,3.015,0) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)