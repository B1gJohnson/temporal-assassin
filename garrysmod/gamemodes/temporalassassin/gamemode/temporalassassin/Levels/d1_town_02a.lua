
if CLIENT then return end

function MAP_LOADED_FUNC()
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then

		CreateSecretArea( Vector(-4978.115234375,1028.9399414063,-3259.4375), Vector(-56.780921936035,-56.780921936035,0), Vector(56.780921936035,56.780921936035,79.133056640625) )
		Create_FalanranWeapon( Vector(-4982.0419921875, 1040.4422607422, -3263.96875), Angle(0, 304.39965820313, 0) )
		
		Create_ShotgunAmmo( Vector(-7399.7592773438, 815.05310058594, -3408), Angle(0, 28.227966308594, 0) )
		Create_ShotgunAmmo( Vector(-7406.9130859375, 847.06640625, -3408), Angle(0, 7.9878540039063, 0) )
		Create_ShotgunAmmo( Vector(-7393.17578125, 835.00360107422, -3408), Angle(0, 17.887893676758, 0) )
		Create_ShotgunAmmo( Vector(-7371.2841796875, 809.49395751953, -3408), Angle(0, 42.528030395508, 0) )
		Create_ShotgunAmmo( Vector(-7369.3510742188, 830.85162353516, -3408), Angle(0, 28.667953491211, 0) )
		Create_Medkit25( Vector(-7564.2958984375, 1071.0421142578, -3408), Angle(0, 263.20947265625, 0) )
		Create_Medkit25( Vector(-7671.4096679688, 1104.7181396484, -3408), Angle(0, 287.18951416016, 0) )
		Create_RevolverAmmo( Vector(-8233.884765625, 989.01251220703, -3355.96875), Angle(0, 359.78955078125, 0) )
		Create_RevolverAmmo( Vector(-8168.802734375, 988.99334716797, -3355.96875), Angle(0, 359.56951904297, 0) )
		Create_SMGAmmo( Vector(-8236.3662109375, 1068.1784667969, -3355.96875), Angle(0, 339.10968017578, 0) )
		Create_SMGAmmo( Vector(-8169.166015625, 1070.177734375, -3355.96875), Angle(0, 307.64947509766, 0) )
		Create_KingSlayerAmmo( Vector(-8235.470703125, 903.66516113281, -3355.96875), Angle(0, 359.78952026367, 0) )
		Create_KingSlayerAmmo( Vector(-8173.8134765625, 904.58825683594, -3355.96875), Angle(0, 358.90951538086, 0) )
		Create_CrossbowWeapon( Vector(-7582.1376953125, 417.65957641602, -3375.96875), Angle(0, 269.50439453125, 0) )
		Create_CrossbowAmmo( Vector(-7581.7006835938, 361.04302978516, -3375.96875), Angle(0, 268.62438964844, 0) )
		Create_CrossbowAmmo( Vector(-7587.9458007813, 349.01177978516, -3375.2084960938), Angle(0, 275.66442871094, 0) )
		Create_CrossbowAmmo( Vector(-7578.0502929688, 337.06649780273, -3375.96875), Angle(0, 262.02438354492, 0) )
		
	else
	
		Create_FoolsOathAmmo( Vector(-7158.937, 1095.41, -3408), Angle(0, -10.709, 0) )
		Create_FoolsOathAmmo( Vector(-7178.422, 1081.236, -3408), Angle(0, 48.647, 0) )
		Create_ShotgunAmmo( Vector(-7211.43, 1094.113, -3408), Angle(0, 83.889, 0) )
		Create_ShotgunAmmo( Vector(-7231.859, 1104.179, -3408), Angle(0, 135.303, 0) )
		Create_SuperShotgunWeapon( Vector(-7237.606, 1139.786, -3408), Angle(0, 58.182, 0) )
		Create_RifleAmmo( Vector(-7188.068, 1116.32, -3408), Angle(0, 69.468, 0) )
		Create_SMGWeapon( Vector(-7169.472, 1149.899, -3408), Angle(0, 75.947, 0) )
		Create_SMGAmmo( Vector(-7133.216, 1085.586, -3408), Angle(0, 102.072, 0) )
		Create_SMGAmmo( Vector(-7115.745, 1117.161, -3408), Angle(0, 117.12, 0) )
		Create_SMGAmmo( Vector(-7152.897, 1121.217, -3408), Angle(0, 93.294, 0) )
		Create_CrossbowAmmo( Vector(-7115.787, 1157.225, -3408), Angle(0, 67.587, 0) )
		Create_CrossbowAmmo( Vector(-7223.891, 1172.425, -3408), Angle(0, 58.6, 0) )
		Create_CrossbowWeapon( Vector(-7167.747, 1206.725, -3408), Angle(0, 81.59, 0) )
		Create_Armour( Vector(-7585.917, 421.485, -3375.969), Angle(0, 270.628, 0) )
		Create_Armour( Vector(-7584.529, 348.035, -3375.969), Angle(0, 270.001, 0) )
		Create_Medkit25( Vector(-7583.596, 256.061, -3408), Angle(0, 269.792, 0) )
		Create_RevolverWeapon( Vector(-8232.841, 903.2, -3355.969), Angle(0, 4.68, 0) )
		Create_PistolWeapon( Vector(-8236.689, 991.68, -3355.969), Angle(0, 359.036, 0) )
		Create_ShotgunWeapon( Vector(-8237.439, 1066.712, -3355.969), Angle(0, 2.172, 0) )
		Create_RevolverAmmo( Vector(-8204.51, 903.425, -3355.969), Angle(0, 358.2, 0) )
		Create_RevolverAmmo( Vector(-8171.638, 903.168, -3355.969), Angle(0, 4.262, 0) )
		Create_PistolAmmo( Vector(-8208.143, 988.76, -3355.969), Angle(0, 359.664, 0) )
		Create_PistolAmmo( Vector(-8168.508, 989.213, -3355.969), Angle(0, 359.037, 0) )
		Create_ShotgunAmmo( Vector(-8208.916, 1067.583, -3355.969), Angle(0, 278.572, 0) )
		Create_ShotgunAmmo( Vector(-8166.455, 1071.829, -3355.969), Angle(0, 35.402, 0) )
		Create_Backpack( Vector(-8252.216, -397.561, -3408), Angle(0, 284.84, 0) )
		Create_Medkit25( Vector(-7592.975, -424.173, -3408), Angle(0, 102.177, 0) )
		Create_Medkit10( Vector(-7648.584, -394.072, -3408), Angle(0, 61.318, 0) )
		Create_Medkit10( Vector(-7686.584, -401.893, -3408), Angle(0, 44.598, 0) )
		Create_Bread( Vector(-7639.95, -417.921, -3408), Angle(0, 73.231, 0) )
	
		CreateSecretArea( Vector(-8247.872,-403.666,-3403.469), Vector(-28.092,-28.092,0), Vector(28.092,28.092,97) )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)