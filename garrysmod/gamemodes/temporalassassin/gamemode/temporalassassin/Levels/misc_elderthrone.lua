
if CLIENT then return end

local MINIMUM_SKILL = 9

if GetConVarNumber("ta_sv_skill") < MINIMUM_SKILL then
	RunConsoleCommand("ta_sv_skill",MINIMUM_SKILL)
end

GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true
HUD_DONTDRAW_POCKETWATCH = true

function MAP_LOADED_FUNC()
	
	GLOB_ELDER_LOAD = false
	GLOB_NO_SAVEDATA = true
	GLOB_SAFE_GAMEEND = true
	GLOB_SUPER_DIE = false
	GLOB_NO_SKIP1 = false
	GLOB_NO_SPECIALS = false
	GLOB_FUN_STARTED = false
	GLOB_FUNWAVE = false
	GLOB_NEXTWAVE_FUNCTION = nil
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

function MAP_INSTANCE_1()

	GLOB_SUPER_DIE = true
	GLOB_NO_SKIP1 = true
	
	CreatePlayerSpawn(Vector(13818.507,-3.049,-813.969),Angle(0,-180,0))

	local EQ_1 = CreateClothesLocker( Vector(13823.815,-419.417,-847.742), Angle(0,88.824,0) )
	local EQ_2 = CreateEquipmentLocker( Vector(13823.737,400.515,-846), Angle(0,268.457,0) )
	Create_ShadeCloak( Vector(13441.151, -3.269, -887.969), Angle(0, 0, 0) )
	
	local WARNING_1 = function()
	
		net.Start("DollMessage")
		net.WriteString("You shouldn't be here kiddo, Leave...")
		net.Broadcast()
	
		if EQ_1 and EQ_2 and IsValid(EQ_1) and IsValid(EQ_2) then
			EQ_1:Remove()
			EQ_2:Remove()
		end
		
		timer.Simple(3,function()
		
			CreateSnapNPC("npc_combine_s",Vector(10559.985351563,-664.54998779297,-895.96875),Angle(0,359.99987792969,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10559.985351563,-713.29998779297,-895.96875),Angle(0,359.99987792969,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10559.985351563,-615.79998779297,-895.96875),Angle(0,359.99987792969,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10511.235351563,-664.54986572266,-895.96875),Angle(0,359.99987792969,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10511.235351563,-615.79986572266,-895.96875),Angle(0,359.99987792969,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10511.235351563,-713.29986572266,-895.96875),Angle(0,359.99987792969,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10462.485351563,-664.54974365234,-895.96875),Angle(0,359.99987792969,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10462.485351563,-615.79974365234,-895.96875),Angle(0,359.99987792969,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10462.485351563,-713.29974365234,-895.96875),Angle(0,359.99987792969,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10679.279296875,-439.97122192383,-895.96875),Angle(0,1.6717834472656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10680.701171875,-488.7004699707,-895.96875),Angle(0,1.6717834472656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10677.857421875,-391.24197387695,-895.96875),Angle(0,1.6717834472656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10630.549804688,-441.39346313477,-895.96875),Angle(0,1.6717834472656,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10629.127929688,-392.66421508789,-895.96875),Angle(0,1.6717834472656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10631.971679688,-490.12271118164,-895.96875),Angle(0,1.6717834472656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10581.8203125,-442.81567382813,-895.96875),Angle(0,1.6717834472656,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10580.3984375,-394.08642578125,-895.96875),Angle(0,1.6717834472656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10583.2421875,-491.544921875,-895.96875),Angle(0,1.6717834472656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(11025.5234375,-727.09503173828,-895.96875),Angle(0,16.928482055664,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(11039.71875,-773.73266601563,-895.96875),Angle(0,16.928482055664,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(11011.328125,-680.45739746094,-895.96875),Angle(0,16.928482055664,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10978.885742188,-741.28997802734,-895.96875),Angle(0,16.928482055664,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10964.690429688,-694.65234375,-895.96875),Angle(0,16.928482055664,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10993.081054688,-787.92761230469,-895.96875),Angle(0,16.928482055664,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10932.248046875,-755.48486328125,-895.96875),Angle(0,16.928482055664,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10918.052734375,-708.84722900391,-895.96875),Angle(0,16.928482055664,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10946.443359375,-802.12249755859,-895.96875),Angle(0,16.928482055664,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(11027.033203125,700.33532714844,-895.96875),Angle(0,335.12789916992,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(11006.529296875,656.10693359375,-895.96875),Angle(0,335.12789916992,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(11047.537109375,744.56372070313,-895.96875),Angle(0,335.12789916992,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10982.8046875,720.83929443359,-895.96875),Angle(0,335.12789916992,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(11003.30859375,765.06768798828,-895.96875),Angle(0,335.12789916992,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10962.30078125,676.61090087891,-895.96875),Angle(0,335.12789916992,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10938.576171875,741.34326171875,-895.96875),Angle(0,335.12789916992,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10959.080078125,785.57165527344,-895.96875),Angle(0,335.12789916992,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10918.072265625,697.11486816406,-895.96875),Angle(0,335.12789916992,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10606.88671875,432.87066650391,-895.96875),Angle(0,356.23693847656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10603.6875,384.22576904297,-895.96875),Angle(0,356.23693847656,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10610.0859375,481.51556396484,-895.96875),Angle(0,356.23693847656,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10558.2421875,436.07015991211,-895.96875),Angle(0,356.23693847656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10561.44140625,484.71505737305,-895.96875),Angle(0,356.23693847656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10555.04296875,387.42526245117,-895.96875),Angle(0,356.23693847656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10509.596679688,439.26965332031,-895.96875),Angle(0,356.23693847656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10512.795898438,487.91455078125,-895.96875),Angle(0,356.23693847656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10506.397460938,390.62475585938,-895.96875),Angle(0,356.23693847656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10384.276367188,810.23754882813,-895.96875),Angle(0,356.23693847656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10381.077148438,761.59265136719,-895.96875),Angle(0,356.23693847656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10387.475585938,858.88244628906,-895.96875),Angle(0,356.23693847656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10335.631835938,813.43707275391,-895.96875),Angle(0,356.23693847656,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(10338.831054688,862.08197021484,-895.96875),Angle(0,356.23693847656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10332.432617188,764.79217529297,-895.96875),Angle(0,356.23693847656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10286.986328125,816.63653564453,-895.96875),Angle(0,356.23693847656,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(10290.185546875,865.28143310547,-895.96875),Angle(0,356.23693847656,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(10283.787109375,767.99163818359,-895.96875),Angle(0,356.23693847656,0),"weapon_shotgun",false)
		
		end)
	
	end
	
	CreateTrigger( Vector(12256.351,-1.085,-895.969), Vector(-997.247,-997.247,0), Vector(997.247,997.247,3607.758), WARNING_1 )
	
	local FINAL_5 = function(self,ply)
	
		if ply:HasLoreFile("lore_mthuulra_char") then
		
			net.Start("DollMessage")
			net.WriteString("Why come back here again? You already have it.")
			net.Broadcast()
			
		else
		
			net.Start("DollMessage")
			net.WriteString("Just take it and leave, head back to your place.")
			net.Broadcast()
			
			Create_LoreItem( Vector(7167.33,1.622,14592.031), Angle(0,180,0), "lore_mthuulra_char", "Lore File Unlocked: Mthuulra (Outer-God)" )
			
		end
	
	end
	
	local WARNING_4 = function()
	
		GLOB_DOLL_OBJ = CreateProp(Vector(7302.75,2.438,14592.375), Angle(0,180,0), "models/temporalassassin/doll/unknown.mdl", true)
		local B_BOX = CreateProp(Vector(7301.375,4.813,14612.25), Angle(0,179.868,0), "models/props_junk/wood_crate001a.mdl", true)
		B_BOX:SetHealth(50000)
		B_BOX:SetPhysicsWeight(5000)
		B_BOX:SetNoDraw(true)
		
		CreateTrigger( Vector(7140.634,8.308,14592.031), Vector(-310.796,-310.796,0), Vector(310.796,310.796,161.438), FINAL_5 )
	
		timer.Simple(2,function()
		
			CreateSnapNPC("npc_combine_s",Vector(8174.904296875,-2.1821329593658,7424.03125),Angle(0,359.92083740234,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8174.8369140625,-50.93208694458,7424.03125),Angle(0,359.92083740234,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8174.9716796875,46.567821502686,7424.03125),Angle(0,359.92083740234,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8126.154296875,-2.1147751808167,7424.03125),Angle(0,359.92083740234,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8126.2216796875,46.635177612305,7424.03125),Angle(0,359.92083740234,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8126.0869140625,-50.864730834961,7424.03125),Angle(0,359.92083740234,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(8077.404296875,-2.0474174022675,7424.03125),Angle(0,359.92083740234,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8077.4716796875,46.702537536621,7424.03125),Angle(0,359.92083740234,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8077.3369140625,-50.797370910645,7424.03125),Angle(0,359.92083740234,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(8551.4013671875,-152.12139892578,7168.03125),Angle(0,10.788818359375,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8560.52734375,-200.00967407227,7168.03125),Angle(0,10.788818359375,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(8542.275390625,-104.2331161499,7168.03125),Angle(0,10.788818359375,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8503.5126953125,-161.24688720703,7168.03125),Angle(0,10.788818359375,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(8494.38671875,-113.35860443115,7168.03125),Angle(0,10.788818359375,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8512.638671875,-209.13516235352,7168.03125),Angle(0,10.788818359375,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8455.625,-170.37239074707,7168.03125),Angle(0,10.788818359375,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8446.4990234375,-122.48410797119,7168.03125),Angle(0,10.788818359375,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8464.7509765625,-218.26068115234,7168.03125),Angle(0,10.788818359375,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(8489.0908203125,145.66680908203,7168.03125),Angle(0,352.81466674805,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8482.9931640625,97.299652099609,7168.03125),Angle(0,352.81466674805,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(8495.1884765625,194.03396606445,7168.03125),Angle(0,352.81466674805,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(8440.7236328125,151.76443481445,7168.03125),Angle(0,352.81466674805,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8446.8212890625,200.13159179688,7168.03125),Angle(0,352.81466674805,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8434.6259765625,103.39727783203,7168.03125),Angle(0,352.81466674805,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8392.3564453125,157.86206054688,7168.03125),Angle(0,352.81466674805,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8398.4541015625,206.2292175293,7168.03125),Angle(0,352.81466674805,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8386.2587890625,109.49490356445,7168.03125),Angle(0,352.81466674805,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7479.322265625,-601.67999267578,7424.03125),Angle(0,274.85729980469,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7430.7475585938,-605.80786132813,7424.03125),Angle(0,274.85729980469,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7527.8969726563,-597.55212402344,7424.03125),Angle(0,274.85729980469,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7475.1943359375,-553.10510253906,7424.03125),Angle(0,274.85729980469,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(7523.7690429688,-548.97723388672,7424.03125),Angle(0,274.85729980469,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7426.6196289063,-557.23297119141,7424.03125),Angle(0,274.85729980469,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7471.06640625,-504.53015136719,7424.03125),Angle(0,274.85729980469,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(7519.6411132813,-500.40225219727,7424.03125),Angle(0,274.85729980469,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(7422.4916992188,-508.65805053711,7424.03125),Angle(0,274.85729980469,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6438.6997070313,-462.89453125,7424.03125),Angle(0,220.93482971191,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6406.7587890625,-426.06610107422,7424.03125),Angle(0,220.93482971191,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6470.640625,-499.72296142578,7424.03125),Angle(0,220.93482971191,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6475.5283203125,-430.95352172852,7424.03125),Angle(0,220.93482971191,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6507.4692382813,-467.78198242188,7424.03125),Angle(0,220.93482971191,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6443.5874023438,-394.12506103516,7424.03125),Angle(0,220.93482971191,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6512.3564453125,-399.01251220703,7424.03125),Angle(0,220.93482971191,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6544.2973632813,-435.84094238281,7424.03125),Angle(0,220.93482971191,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6480.4155273438,-362.18408203125,7424.03125),Angle(0,220.93482971191,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6494.2329101563,421.07583618164,7424.03125),Angle(0,122.70490264893,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6535.2543945313,447.41607666016,7424.03125),Angle(0,122.70490264893,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6453.2114257813,394.73559570313,7424.03125),Angle(0,122.70490264893,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6520.5732421875,380.05444335938,7424.03125),Angle(0,122.70490264893,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6479.5517578125,353.71420288086,7424.03125),Angle(0,122.70490264893,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6561.5947265625,406.39468383789,7424.03125),Angle(0,122.70490264893,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6546.9135742188,339.03305053711,7424.03125),Angle(0,122.70490264893,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6505.8920898438,312.69281005859,7424.03125),Angle(0,122.70490264893,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6587.9350585938,365.37329101563,7424.03125),Angle(0,122.70490264893,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(7531.505859375,552.57806396484,7424.03125),Angle(0,51.644943237305,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7569.7348632813,522.32708740234,7424.03125),Angle(0,51.644943237305,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7493.2768554688,582.82904052734,7424.03125),Angle(0,51.644943237305,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(7501.2548828125,514.34924316406,7424.03125),Angle(0,51.644943237305,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(7463.0258789063,544.60021972656,7424.03125),Angle(0,51.644943237305,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7539.4838867188,484.09826660156,7424.03125),Angle(0,51.644943237305,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7471.00390625,476.12048339844,7424.03125),Angle(0,51.644943237305,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7432.7749023438,506.37145996094,7424.03125),Angle(0,51.644943237305,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7509.2329101563,445.86950683594,7424.03125),Angle(0,51.644943237305,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(5140.787109375,-20.705194473267,7168.03125),Angle(0,0.85836791992188,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(5141.517578125,-69.449722290039,7168.03125),Angle(0,0.85836791992188,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(5140.056640625,28.039335250854,7168.03125),Angle(0,0.85836791992188,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(5092.0424804688,-21.435508728027,7168.03125),Angle(0,0.85836791992188,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(5091.3120117188,27.309020996094,7168.03125),Angle(0,0.85836791992188,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(5092.7729492188,-70.180038452148,7168.03125),Angle(0,0.85836791992188,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(5043.2978515625,-22.165821075439,7168.03125),Angle(0,0.85836791992188,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(5042.5673828125,26.578708648682,7168.03125),Angle(0,0.85836791992188,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(5044.0283203125,-70.910354614258,7168.03125),Angle(0,0.85836791992188,0),"weapon_smg1",false)
		
		end)
	
	end

	local WARNING_3 = function()
	
		timer.Simple(2,function()
		
			CreateSnapNPC("npc_combine_s",Vector(8008.5893554688,-704.83764648438,2560.03125),Angle(0,61.78434753418,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8051.5463867188,-727.88623046875,2560.03125),Angle(0,61.78434753418,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(7965.6323242188,-681.7890625,2560.03125),Angle(0,61.78434753418,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7985.5405273438,-747.794921875,2560.03125),Angle(0,61.78434753418,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7942.5834960938,-724.74633789063,2560.03125),Angle(0,61.78434753418,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(8028.4975585938,-770.84350585938,2560.03125),Angle(0,61.78434753418,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7962.4921875,-790.75213623047,2560.03125),Angle(0,61.78434753418,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7919.53515625,-767.70355224609,2560.03125),Angle(0,61.78434753418,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(8005.44921875,-813.80072021484,2560.03125),Angle(0,61.78434753418,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6671.9291992188,-764.41766357422,2560.03125),Angle(0,150.81823730469,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6695.6987304688,-721.85516357422,2560.03125),Angle(0,150.81823730469,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6648.1596679688,-806.98016357422,2560.03125),Angle(0,150.81823730469,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6714.4916992188,-788.18725585938,2560.03125),Angle(0,150.81823730469,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6690.7221679688,-830.74975585938,2560.03125),Angle(0,150.81823730469,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6738.2612304688,-745.62475585938,2560.03125),Angle(0,150.81823730469,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6757.0541992188,-811.95690917969,2560.03125),Angle(0,150.81823730469,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6733.2846679688,-854.51940917969,2560.03125),Angle(0,150.81823730469,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6780.8237304688,-769.39440917969,2560.03125),Angle(0,150.81823730469,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6635.8623046875,853.25756835938,2560.03125),Angle(0,72.861274719238,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6682.4477539063,838.8916015625,2560.03125),Angle(0,72.861274719238,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6589.2768554688,867.62353515625,2560.03125),Angle(0,72.861274719238,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6621.4965820313,806.67236328125,2560.03125),Angle(0,72.861274719238,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6574.9111328125,821.03833007813,2560.03125),Angle(0,72.861274719238,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6668.08203125,792.30639648438,2560.03125),Angle(0,72.861274719238,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6607.1303710938,760.08715820313,2560.03125),Angle(0,72.861274719238,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6560.544921875,774.453125,2560.03125),Angle(0,72.861274719238,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6653.7158203125,745.72119140625,2560.03125),Angle(0,72.861274719238,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7644.7333984375,565.60339355469,2560.03125),Angle(0,352.39630126953,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7638.2827148438,517.28204345703,2560.03125),Angle(0,352.39630126953,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7651.1840820313,613.92474365234,2560.03125),Angle(0,352.39630126953,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7596.412109375,572.05401611328,2560.03125),Angle(0,352.39630126953,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7602.8627929688,620.37536621094,2560.03125),Angle(0,352.39630126953,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(7589.9614257813,523.73266601563,2560.03125),Angle(0,352.39630126953,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(7548.0908203125,578.50463867188,2560.03125),Angle(0,352.39630126953,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7554.5415039063,626.82598876953,2560.03125),Angle(0,352.39630126953,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7541.6401367188,530.18328857422,2560.03125),Angle(0,352.39630126953,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7017.3002929688,8.7671413421631,2560.03125),Angle(0,359.39819335938,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(7016.7880859375,-39.980171203613,2560.03125),Angle(0,359.39819335938,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(7017.8125,57.514450073242,2560.03125),Angle(0,359.39819335938,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6968.5532226563,9.279182434082,2560.03125),Angle(0,359.39819335938,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6969.0654296875,58.02649307251,2560.03125),Angle(0,359.39819335938,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6968.041015625,-39.468128204346,2560.03125),Angle(0,359.39819335938,0),"weapon_ar2",false)
			CreateSnapNPC("npc_combine_s",Vector(6919.8056640625,9.7912225723267,2560.03125),Angle(0,359.39819335938,0),"weapon_smg1",false)
			CreateSnapNPC("npc_combine_s",Vector(6920.3178710938,58.53853225708,2560.03125),Angle(0,359.39819335938,0),"weapon_shotgun",false)
			CreateSnapNPC("npc_combine_s",Vector(6919.2934570313,-38.956089019775,2560.03125),Angle(0,359.39819335938,0),"weapon_smg1",false)
			
			CreateTrigger( Vector(8414.257,24.424,7168.031), Vector(-4457.898,-4457.898,0), Vector(4457.898,4457.898,1224.438), WARNING_4 )
		
		end)
	
	end
	
	local WARNING_2 = function()
	
		net.Start("DollMessage")
		net.WriteString("This place is not for you, stop while you can...")
		net.Broadcast()
		
		timer.Simple(7,function()
			GLOB_NO_SKIP1 = false
			net.Start("DollMessage")
			net.WriteString("If you wish to continue, then so be it. You only need look up")
			net.Broadcast()
			CreateTrigger( Vector(7128.266,-0.832,2364.419), Vector(-3719.987,-3719.987,0), Vector(3719.987,3719.987,4916.997), WARNING_3 )
		end)
	
	end
	
	CreateTrigger( Vector(7156.555,-4.988,-255.969), Vector(-828.547,-828.547,0), Vector(828.547,828.547,2272.906), WARNING_2 )
	
	local BLOCK_5 = function( self, ply )
		if ply and IsValid(ply) and ply:IsPlayer() then
			ply:SetLocalVelocity(Vector(0,0,0))
			local P1, P2 = CreateTeleporter( "STALL_PROGRESS_1", ply:GetPos(), "STALL_PROGRESS_2", Vector(13818.507,-3.049,-812.969) )
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
		end
	end
	
	local BLOCKER_5 = CreateTrigger( Vector(8532.488,-853.791,-2096.292), Vector(-13410.874,-13410.874,0), Vector(13410.874,13410.874,235.294), BLOCK_5, true )

end

function MAP_INSTANCE_2()

	GLOB_NO_SPECIALS = true
	GLOB_FUN_STARTED = false
	GLOB_NEXTWAVE_FUNCTION = nil

	CreatePlayerSpawn( Vector(7153.82,-3.031,14592.031), Angle(0,0,0) )

	local EQ_1 = CreateEquipmentLocker( Vector(7224.114,115.672,14592.031), Angle(0,239.994,0) )
	local EQ_2 = CreateClothesLocker( Vector(7222.39,-106.5,14592.031), Angle(0,119.61,0) )
	Create_ShadeCloak( Vector(7150.03, 133.755, 14592.031), Angle(0, 235.869, 0) )
	
	GLOB_DOLL_OBJ2 = CreateProp(Vector(16000,-40,13299), Angle(0.044,178.682,0), "models/temporalassassin/doll/unknown.mdl", true)
	GLOB_DOLL_OBJ2:SetModelScale(100,0)
	
	local OBS_1 = CreateProp(Vector(7146.625,742.406,2557.75), Angle(0,0,0), "models/hunter/plates/plate16x24.mdl", true)
	local OBS_2 = CreateProp(Vector(7168.063,-730.812,2557.531), Angle(0,-0.044,0), "models/hunter/plates/plate16x24.mdl", true)
	local OBS_3 = CreateProp(Vector(6534.813,94.281,2555.375), Angle(-0.044,16.172,0), "models/hunter/plates/plate24x32.mdl", true)
	local OBS_4 = CreateProp(Vector(6545.313,-151.687,2556.563), Angle(0,-17.534,0), "models/hunter/plates/plate24x32.mdl", true)
	local OBS_5 = CreateProp(Vector(6050.281,3,2681.313), Angle(59.326,179.561,-179.341), "models/hunter/plates/plate6x6.mdl", true)
	local OBS_6 = CreateProp(Vector(6071.906,1.5,2807.719), Angle(0,0,0), "models/hunter/plates/plate6x6.mdl", true)
	OBS_1:SetMaterial("models/props_c17/frostedglass_01a")
	OBS_2:SetMaterial("models/props_c17/frostedglass_01a")
	OBS_3:SetMaterial("models/props_c17/frostedglass_01a")
	OBS_4:SetMaterial("models/props_c17/frostedglass_01a")
	OBS_5:SetMaterial("models/props_c17/frostedglass_01a")
	OBS_6:SetMaterial("models/props_c17/frostedglass_01a")
	
	local START_FUN5 = function( self, ply )
		
		timer.Simple(4,function()
		
			local GD = GLOB_DOLL_OBJ2
			
			GLOB_DOLL_OBJ2 = false
			GD:SetPos( Vector(0,0,-36000) )
			GD:Remove()
		
			GLOB_DOLL_OBJ = CreateProp(Vector(7304.906,-0.187,-255.594), Angle(0.044,179.341,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
			local BLOCK_P = CreateProp(Vector(7306.031,0.031,-235.594), Angle(0,-179.297,0), "models/props_junk/wood_crate001a.mdl", true)
			BLOCK_P:SetNoDraw(true)
			BLOCK_P:SetHealth(50000)
			
			if ply and IsValid(ply) and ply:IsPlayer() then
				ply:SetLocalVelocity(Vector(0,0,0))
				local P1, P2 = CreateTeleporter( "FUN_PORTAL_1", ply:GetPos(), "FUN_PORTAL_2", Vector(6623.037,-3.988,-255.969) )
				timer.Simple(0.1,function()
					if IsValid(P1) and IsValid(P2) then
						P1:Remove()
						P2:Remove()
					end
				end)
			end
			
		end)
		
		timer.Simple(5,function()
			net.Start("DollMessage")
			net.WriteString("That was thoroughly entertaining.")
			net.Broadcast()
		end)
		
		timer.Simple(10,function()
		
			local TEXT = "Here, take it and head home."
			
			if ply:HasLoreFile("lore_mthuulra_char2") then
				TEXT = "You already have it, Either way the entertainment was welcome."
			else
				Create_LoreItem( Vector(7116.822,2.004,-255.969), Angle(0,180,0), "lore_mthuulra_char2", "Lore File Unlocked: The Elders Collapse" )
			end
		
			net.Start("DollMessage")
			net.WriteString(TEXT)			
			net.Broadcast()
			
		end)
	
	end
	
	local FUN_WAVE4 = function()
	
		GLOB_NEXTWAVE_FUNCTION = function()
			CreateTrigger( Vector(9142.255,-320.031,-2229.338), Vector(-10819.734,-10819.734,0), Vector(10819.734,10819.734,5987.745), START_FUN5 )
		end
		
		if IsMounted("ep2") then
		
			GLOB_FUNWAVE = {
			CreateSnapNPC("npc_hunter",Vector(9941.525390625,320.38778686523,-383.96875),Angle(0,202.57182312012,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(9805.3427734375,456.93392944336,-383.96875),Angle(0,202.57182312012,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(9598.51171875,551.85174560547,-383.96875),Angle(0,247.57182312012,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(9393.8564453125,601.18664550781,-383.96875),Angle(0,269.24270629883,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(9147.2978515625,508.19946289063,-383.96875),Angle(0,301.15411376953,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(9911.18359375,-266.05725097656,-383.96875),Angle(0,155.96461486816,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(9829.7734375,-527.79827880859,-383.96875),Angle(0,127.33165740967,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(9562.9462890625,-687.23394775391,-383.96875),Angle(0,99.534706115723,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(9304.46875,-701.47906494141,-383.96875),Angle(0,81.769737243652,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(9039.5078125,-615.80456542969,-383.96875),Angle(0,54.181785583496,0),"false",false),
			CreateSnapNPC("npc_combine_s",Vector(10381.828125,305.38723754883,-127.96875),Angle(0,187.52380371094,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(10270.60546875,327.61410522461,-127.96875),Angle(0,187.52380371094,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(10269.685546875,435.6217956543,-127.96875),Angle(0,179.16380310059,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(10405.201171875,-305.18655395508,-127.96875),Angle(0,176.86480712891,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(10269.7734375,-311.87582397461,-127.96875),Angle(0,176.86480712891,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(10265.40234375,-447.48010253906,-127.96875),Angle(0,178.7458190918,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(12935.796875,252.56530761719,-895.96875),Angle(0,179.37283325195,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(13080.91796875,5.7649579048157,-895.96875),Angle(0,179.37283325195,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(12923.692382813,-319.0813293457,-895.96875),Angle(0,178.5368347168,0),"weapon_ar2",false),
			CreateSnapNPC("npc_metropolice",Vector(12751.967773438,135.17517089844,-895.96875),Angle(0,182.298828125,0),"weapon_pistol",false),
			CreateSnapNPC("npc_metropolice",Vector(12751.123046875,-158.09748840332,-895.96875),Angle(0,181.88082885742,0),"weapon_pistol",false),
			CreateSnapNPC("npc_metropolice",Vector(12613.875,-4.2679309844971,-895.96875),Angle(0,179.99983215332,0),"weapon_stunstick",false),
			CreateSnapNPC("npc_combine_s",Vector(12438.76171875,241.86346435547,-895.96875),Angle(0,187.00132751465,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(12277.432617188,-8.6163530349731,-895.96875),Angle(0,180.4178314209,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(12469.799804688,-246.14668273926,-895.96875),Angle(0,175.08833312988,0),"weapon_smg1",false),
			CreateSnapNPC("npc_metropolice",Vector(12093.122070313,114.95874023438,-895.96875),Angle(0,181.25382995605,0),"weapon_smg1",false),
			CreateSnapNPC("npc_metropolice",Vector(12110.05078125,-158.17974853516,-895.96875),Angle(0,175.61082458496,0),"weapon_smg1",false),
			CreateSnapNPC("npc_metropolice",Vector(11895.541992188,-2.6259074211121,-895.96875),Angle(0,177.07382202148,0),"weapon_pistol",false),
			CreateSnapNPC("npc_combine_s",Vector(11754.626953125,246.86288452148,-895.96875),Angle(0,185.53833007813,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(11725.537109375,-246.33337402344,-895.96875),Angle(0,169.34083557129,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(11605.77734375,-6.3531579971313,-895.96875),Angle(0,181.88084411621,0),"weapon_smg1",false),
			CreateSnapNPC("npc_hunter",Vector(11366.479492188,177.62770080566,-895.96875),Angle(0,186.26991271973,0),"false",false),
			CreateSnapNPC("npc_hunter",Vector(11378.977539063,-132.56115722656,-895.96875),Angle(0,170.17691040039,0),"false",false)}
		
		else
	
			GLOB_FUNWAVE = {
			CreateSnapNPC("npc_metropolice",Vector(10260.97265625,-307.62307739258,-127.96875),Angle(0,170.28385925293,0),"weapon_smg1",false),
			CreateSnapNPC("npc_metropolice",Vector(10261.950195313,-443.52987670898,-127.96875),Angle(0,182.82385253906,0),"weapon_pistol",false),
			CreateSnapNPC("npc_metropolice",Vector(10381.793945313,-321.69000244141,-127.96875),Angle(0,118.76538848877,0),"weapon_pistol",false),
			CreateSnapNPC("npc_metropolice",Vector(10407.543945313,316.53067016602,-127.96875),Angle(0,193.1693572998,0),"weapon_smg1",false),
			CreateSnapNPC("npc_metropolice",Vector(10261.473632813,321.83200073242,-127.96875),Angle(0,197.34936523438,0),"weapon_smg1",false),
			CreateSnapNPC("npc_metropolice",Vector(10277.0703125,441.15954589844,-127.96875),Angle(0,164.32736206055,0),"weapon_pistol",false),
			CreateSnapNPC("npc_combine_s",Vector(10121.024414063,587.36181640625,-383.96875),Angle(0,218.35391235352,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(10054.846679688,878.26306152344,-383.96875),Angle(0,230.2668762207,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(9896.4072265625,1131.6853027344,-383.96875),Angle(0,243.64286804199,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9514.8662109375,1050.8228759766,-383.96875),Angle(0,243.64286804199,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(9001.26953125,755.29559326172,-383.96875),Angle(0,309.47766113281,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9210.6083984375,1098.7470703125,-383.96875),Angle(0,309.47766113281,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9242.9951171875,661.95788574219,-383.96875),Angle(0,295.47463989258,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(9603.8623046875,687.60778808594,-383.96875),Angle(0,295.47463989258,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(9832.26171875,836.61352539063,-383.96875),Angle(0,248.03170776367,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9817.298828125,492.65161132813,-383.96875),Angle(0,215.95025634766,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(10045.528320313,147.51614379883,-383.96875),Angle(0,183.86875915527,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(10064.68359375,-166.90106201172,-383.96875),Angle(0,176.97175598145,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9919.71484375,-425.73907470703,-383.96875),Angle(0,117.72027587891,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(9950.7060546875,-725.85626220703,-383.96875),Angle(0,117.72027587891,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9687.17578125,-806.49786376953,-383.96875),Angle(0,117.72027587891,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9377.6630859375,-806.91436767578,-383.96875),Angle(0,78.417274475098,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(9059.12109375,-611.71917724609,-383.96875),Angle(0,65.365867614746,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9289.3525390625,-534.62475585938,-383.96875),Angle(0,65.365867614746,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9688.140625,-525.46740722656,-383.96875),Angle(0,65.365867614746,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9478.298828125,-660.14697265625,-383.96875),Angle(0,95.98429107666,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(9089.240234375,-877.15222167969,-383.96875),Angle(0,51.153884887695,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(9939.5478515625,-1081.9907226563,-383.96875),Angle(0,107.3748626709,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(9409.8486328125,1419.2277832031,-383.96875),Angle(0,273.11184692383,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(9442.509765625,-1261.3325195313,-383.96875),Angle(0,100.47789764404,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(11690.346679688,238.22146606445,-895.96875),Angle(0,182.40592956543,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(11961.607421875,165.27241516113,-895.96875),Angle(0,182.40592956543,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(12095.706054688,262.24041748047,-895.96875),Angle(0,182.40592956543,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(12314.8203125,157.60580444336,-895.96875),Angle(0,177.70343017578,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(12472.598632813,278.0071105957,-895.96875),Angle(0,177.70343017578,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(12727.846679688,134.81353759766,-895.96875),Angle(0,177.70343017578,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(11652.934570313,-174.54156494141,-895.96875),Angle(0,179.68891906738,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(11880.58984375,-306.69400024414,-895.96875),Angle(0,175.29992675781,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(12033.78515625,-190.75233459473,-895.96875),Angle(0,175.29992675781,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(12331.296875,-324.38177490234,-895.96875),Angle(0,175.29992675781,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(12494.87109375,-206.21441650391,-895.96875),Angle(0,175.29992675781,0),"weapon_smg1",false),
			CreateSnapNPC("npc_combine_s",Vector(12807.015625,-313.86367797852,-895.96875),Angle(0,175.29992675781,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(12993.150390625,-162.7281036377,-895.96875),Angle(0,182.19692993164,0),"weapon_ar2",false),
			CreateSnapNPC("npc_combine_s",Vector(12983.79296875,277.01123046875,-895.96875),Angle(0,182.19692993164,0),"weapon_ar2",false)}
			
		end
	
	end
	
	local START_FUN4 = function( self, ply )
		
		timer.Simple(5,function()
			net.Start("DollMessage")
			net.WriteString("Almost.")
			net.Broadcast()
		end)
		
		timer.Simple(10,function()
		
			if ply and IsValid(ply) and ply:IsPlayer() then
				ply:SetLocalVelocity(Vector(0,0,0))
				local P1, P2 = CreateTeleporter( "FUN_PORTAL_1", ply:GetPos(), "FUN_PORTAL_2", Vector(9390.189,-2.011,-255.968) )
				timer.Simple(0.1,function()
					if IsValid(P1) and IsValid(P2) then
						P1:Remove()
						P2:Remove()
					end
				end)
			end
			
		end)
		
		timer.Simple(14,function()
			FUN_WAVE4()
		end)
	
	end
	
	local FUN_WAVE3 = function()
	
		GLOB_NEXTWAVE_FUNCTION = function()
			CreateTrigger( Vector(7128.194,12.447,2560.031), Vector(-2891.966,-2891.966,0), Vector(2891.966,2891.966,1035.003), START_FUN4 )
		end
	
		if IsMounted("ep2") then
		
			GLOB_FUNWAVE = {
			CreateSnapNPC("npc_antlion_worker",Vector(8064.4091796875,-1257.9466552734,2656.03125),Angle(0,110.65423583984,0),"false",false),
			CreateSnapNPC("npc_antlion_worker",Vector(7908.0185546875,-1340.8720703125,2656.03125),Angle(0,112.01273345947,0),"false",false),
			CreateSnapNPC("npc_antlion_worker",Vector(7904.5869140625,1340.9741210938,2656.03125),Angle(0,243.99362182617,0),"false",false),
			CreateSnapNPC("npc_antlion_worker",Vector(8065.9599609375,1258.3345947266,2656.03125),Angle(0,242.94860839844,0),"false",false),
			CreateSnapNPC("npc_antlion_worker",Vector(6436.5004882813,1345.1882324219,2656.03125),Angle(0,300.84124755859,0),"false",false),
			CreateSnapNPC("npc_antlion_worker",Vector(6283.150390625,1256.9133300781,2656.03125),Angle(0,286.97555541992,0),"false",false),
			CreateSnapNPC("npc_antlion_worker",Vector(6426.8090820313,-1336.0362548828,2656.03125),Angle(0,67.074783325195,0),"false",false),
			CreateSnapNPC("npc_antlion_worker",Vector(6275.4389648438,-1256.7309570313,2656.03125),Angle(0,66.071731567383,0),"false",false),
			CreateSnapNPC("npc_antlionguard",Vector(7891.0502929688,545.06402587891,2560.03125),Angle(0,215.25811767578,0),"false",false),
			CreateSnapNPC("npc_antlionguard",Vector(7954.3413085938,-594.56066894531,2560.03125),Angle(0,147.33312988281,0),"false",false)}
		
		else
		
			GLOB_FUNWAVE = {
			CreateSnapNPC("npc_antlion",Vector(8064.4091796875,-1257.9466552734,2656.03125),Angle(0,110.65423583984,0),"false",false),
			CreateSnapNPC("npc_antlion",Vector(7908.0185546875,-1340.8720703125,2656.03125),Angle(0,112.01273345947,0),"false",false),
			CreateSnapNPC("npc_antlion",Vector(7904.5869140625,1340.9741210938,2656.03125),Angle(0,243.99362182617,0),"false",false),
			CreateSnapNPC("npc_antlion",Vector(8065.9599609375,1258.3345947266,2656.03125),Angle(0,242.94860839844,0),"false",false),
			CreateSnapNPC("npc_antlion",Vector(6436.5004882813,1345.1882324219,2656.03125),Angle(0,300.84124755859,0),"false",false),
			CreateSnapNPC("npc_antlion",Vector(6283.150390625,1256.9133300781,2656.03125),Angle(0,286.97555541992,0),"false",false),
			CreateSnapNPC("npc_antlion",Vector(6426.8090820313,-1336.0362548828,2656.03125),Angle(0,67.074783325195,0),"false",false),
			CreateSnapNPC("npc_antlion",Vector(6275.4389648438,-1256.7309570313,2656.03125),Angle(0,66.071731567383,0),"false",false),
			CreateSnapNPC("npc_antlionguard",Vector(7891.0502929688,545.06402587891,2560.03125),Angle(0,215.25811767578,0),"false",false),
			CreateSnapNPC("npc_antlionguard",Vector(7954.3413085938,-594.56066894531,2560.03125),Angle(0,147.33312988281,0),"false",false)}
			
		end
		
	end
	
	local START_FUN3 = function( self, ply )
		
		timer.Simple(5,function()
			net.Start("DollMessage")
			net.WriteString("...")
			net.Broadcast()
		end)
		
		timer.Simple(10,function()
		
			if ply and IsValid(ply) and ply:IsPlayer() then
				ply:SetLocalVelocity(Vector(0,0,0))
				local P1, P2 = CreateTeleporter( "FUN_PORTAL_1", ply:GetPos(), "FUN_PORTAL_2", Vector(7150.044,2.679,2560.031) )
				timer.Simple(0.1,function()
					if IsValid(P1) and IsValid(P2) then
						P1:Remove()
						P2:Remove()
					end
				end)
			end
			
		end)
		
		timer.Simple(14,function()
			FUN_WAVE3()
		end)
	
	end
	
	local FUN_WAVE2 = function()
	
		GLOB_NEXTWAVE_FUNCTION = function()
			CreateTrigger( Vector(7097.839,1.147,4736.031), Vector(-1737.621,-1737.621,0), Vector(1737.621,1737.621,694.856), START_FUN3 )
		end
		
		if IsMounted("episodic") or IsMounted("ep2") then
		
			GLOB_FUNWAVE = {
			CreateSnapNPC("npc_fastzombie",Vector(8479.1748046875,12.203016281128,4896.03125),Angle(0,178.89869689941,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8281.1328125,507.10913085938,4896.03125),Angle(0,196.97729492188,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8333.1767578125,387.36367797852,4896.03125),Angle(0,221.53475952148,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8448.5810546875,97.832908630371,4896.03125),Angle(0,186.35264587402,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8451.185546875,-88.188819885254,4896.03125),Angle(0,174.31829833984,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8357.10546875,-376.50424194336,4896.03125),Angle(0,153.19171142578,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8287.51171875,-485.98123168945,4896.03125),Angle(0,149.42971801758,0),"false",false),
			CreateSnapNPC("npc_poisonzombie",Vector(8284.73046875,-278.90768432617,4871.4916992188),Angle(0,154.86370849609,0),"false",false),
			CreateSnapNPC("npc_poisonzombie",Vector(8314.076171875,-197.65840148926,4870.0825195313),Angle(0,164.58218383789,0),"false",false),
			CreateSnapNPC("npc_poisonzombie",Vector(8329.12890625,169.2283782959,4870.1870117188),Angle(0,187.0496673584,0),"false",false),
			CreateSnapNPC("npc_poisonzombie",Vector(8277.0205078125,267.24960327148,4869.7998046875),Angle(0,206.06867980957,0),"false",false),
			CreateSnapNPC("npc_zombine",Vector(8052.9072265625,480.99603271484,4864.03125),Angle(0,198.54495239258,0),"false",false),
			CreateSnapNPC("npc_zombine",Vector(8049.0078125,-500.84417724609,4864.03125),Angle(0,154.02807617188,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8314.693359375,-53.542373657227,4866.9931640625),Angle(0,181.4070892334,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8322.375,62.489948272705,4867.9536132813),Angle(0,179.73509216309,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8385.9091796875,7.4646224975586,4875.8955078125),Angle(0,178.89909362793,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8223.9814453125,425.59136962891,4873.0659179688),Angle(0,185.58712768555,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8244.3720703125,-399.33932495117,4873.9741210938),Angle(0,154.80836486816,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8097.6040039063,-85.016563415527,4839.857421875),Angle(0,178.16484069824,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8040.1796875,-273.12573242188,4840.5620117188),Angle(0,161.86285400391,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8084.5551757813,-171.03277587891,4839.7275390625),Angle(0,170.64085388184,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7830.1606445313,-75.255294799805,4806.4267578125),Angle(0,180.25485229492,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7821.3818359375,-162.34687805176,4806.2880859375),Angle(0,171.39303588867,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7796.0170898438,-252.5798034668,4808.7573242188),Angle(0,161.77403259277,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7739.451171875,-355.22192382813,4808.1015625),Angle(0,145.35185241699,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7894.3046875,-480.88079833984,4835.3120117188),Angle(0,163.22132873535,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7780.265625,97.057464599609,4800.189453125),Angle(0,179.0008392334,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7713.6376953125,213.19755554199,4800.03125),Angle(0,195.92984008789,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7654.2475585938,287.28039550781,4800.03125),Angle(0,206.37983703613,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7606.8642578125,427.21319580078,4800.03125),Angle(0,195.74792480469,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7534.142578125,511.99639892578,4800.03125),Angle(0,221.72427368164,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(7965.8017578125,184.68060302734,4832.03125),Angle(0,179.00088500977,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7975.5244140625,88.123092651367,4832.03125),Angle(0,166.46430969238,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7915.1640625,267.58190917969,4832.03125),Angle(0,201.36389160156,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8026.9946289063,246.63333129883,4837.2583007813),Angle(0,192.58589172363,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8060.2885742188,125.81098175049,4835.1928710938),Angle(0,187.15188598633,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7803.7856445313,200.56874084473,4806.4775390625),Angle(0,196.55686950684,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7816.2602539063,140.0924987793,4804.6889648438),Angle(0,182.73965454102,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7726.3188476563,-108.4977722168,4800.03125),Angle(0,179.83685302734,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7727.4047851563,-196.55009460449,4800.03125),Angle(0,159.33993530273,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7968.8500976563,-113.78382110596,4832.03125),Angle(0,173.77584838867,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7945.5922851563,-206.81669616699,4832.03125),Angle(0,168.13285827637,0),"false",false),
			CreateSnapNPC("npc_zombine",Vector(7566.5283203125,79.973945617676,4773.4721679688),Angle(0,181.19232177734,0),"false",false),
			CreateSnapNPC("npc_zombine",Vector(7563.517578125,-80.598564147949,4773.095703125),Angle(0,176.80331420898,0),"false",false)}
			
		else
		
			GLOB_FUNWAVE = {
			CreateSnapNPC("npc_fastzombie",Vector(8479.1748046875,12.203016281128,4896.03125),Angle(0,178.89869689941,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8281.1328125,507.10913085938,4896.03125),Angle(0,196.97729492188,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8333.1767578125,387.36367797852,4896.03125),Angle(0,221.53475952148,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8448.5810546875,97.832908630371,4896.03125),Angle(0,186.35264587402,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8451.185546875,-88.188819885254,4896.03125),Angle(0,174.31829833984,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8357.10546875,-376.50424194336,4896.03125),Angle(0,153.19171142578,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8287.51171875,-485.98123168945,4896.03125),Angle(0,149.42971801758,0),"false",false),
			CreateSnapNPC("npc_poisonzombie",Vector(8284.73046875,-278.90768432617,4871.4916992188),Angle(0,154.86370849609,0),"false",false),
			CreateSnapNPC("npc_poisonzombie",Vector(8314.076171875,-197.65840148926,4870.0825195313),Angle(0,164.58218383789,0),"false",false),
			CreateSnapNPC("npc_poisonzombie",Vector(8329.12890625,169.2283782959,4870.1870117188),Angle(0,187.0496673584,0),"false",false),
			CreateSnapNPC("npc_poisonzombie",Vector(8277.0205078125,267.24960327148,4869.7998046875),Angle(0,206.06867980957,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8314.693359375,-53.542373657227,4866.9931640625),Angle(0,181.4070892334,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8322.375,62.489948272705,4867.9536132813),Angle(0,179.73509216309,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8385.9091796875,7.4646224975586,4875.8955078125),Angle(0,178.89909362793,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8223.9814453125,425.59136962891,4873.0659179688),Angle(0,185.58712768555,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8244.3720703125,-399.33932495117,4873.9741210938),Angle(0,154.80836486816,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8097.6040039063,-85.016563415527,4839.857421875),Angle(0,178.16484069824,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8040.1796875,-273.12573242188,4840.5620117188),Angle(0,161.86285400391,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(8084.5551757813,-171.03277587891,4839.7275390625),Angle(0,170.64085388184,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7830.1606445313,-75.255294799805,4806.4267578125),Angle(0,180.25485229492,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7821.3818359375,-162.34687805176,4806.2880859375),Angle(0,171.39303588867,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7796.0170898438,-252.5798034668,4808.7573242188),Angle(0,161.77403259277,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7739.451171875,-355.22192382813,4808.1015625),Angle(0,145.35185241699,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7894.3046875,-480.88079833984,4835.3120117188),Angle(0,163.22132873535,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7780.265625,97.057464599609,4800.189453125),Angle(0,179.0008392334,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7713.6376953125,213.19755554199,4800.03125),Angle(0,195.92984008789,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7654.2475585938,287.28039550781,4800.03125),Angle(0,206.37983703613,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7606.8642578125,427.21319580078,4800.03125),Angle(0,195.74792480469,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7534.142578125,511.99639892578,4800.03125),Angle(0,221.72427368164,0),"false",false),
			CreateSnapNPC("npc_fastzombie",Vector(7965.8017578125,184.68060302734,4832.03125),Angle(0,179.00088500977,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7975.5244140625,88.123092651367,4832.03125),Angle(0,166.46430969238,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7915.1640625,267.58190917969,4832.03125),Angle(0,201.36389160156,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8026.9946289063,246.63333129883,4837.2583007813),Angle(0,192.58589172363,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(8060.2885742188,125.81098175049,4835.1928710938),Angle(0,187.15188598633,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7803.7856445313,200.56874084473,4806.4775390625),Angle(0,196.55686950684,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7816.2602539063,140.0924987793,4804.6889648438),Angle(0,182.73965454102,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7726.3188476563,-108.4977722168,4800.03125),Angle(0,179.83685302734,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7727.4047851563,-196.55009460449,4800.03125),Angle(0,159.33993530273,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7968.8500976563,-113.78382110596,4832.03125),Angle(0,173.77584838867,0),"false",false),
			CreateSnapNPC("npc_zombie",Vector(7945.5922851563,-206.81669616699,4832.03125),Angle(0,168.13285827637,0),"false",false)}
			
		end
	
	end
	
	local START_FUN2 = function( self, ply )
		
		timer.Simple(5,function()
			net.Start("DollMessage")
			net.WriteString("We're not done yet.")
			net.Broadcast()
		end)
		
		timer.Simple(10,function()
		
			if ply and IsValid(ply) and ply:IsPlayer() then
				ply:SetLocalVelocity(Vector(0,0,0))
				local P1, P2 = CreateTeleporter( "FUN_PORTAL_1", ply:GetPos(), "FUN_PORTAL_2", Vector(6981,-0.364,4736.031) )
				timer.Simple(0.1,function()
					if IsValid(P1) and IsValid(P2) then
						P1:Remove()
						P2:Remove()
					end
				end)
			end
			
		end)
		
		timer.Simple(14,function()
			FUN_WAVE2()
		end)
	
	end
	
	local FUN_WAVE1 = function()
	
		GLOB_NEXTWAVE_FUNCTION = function()
			CreateTrigger( Vector(5540.816,-3.343,7168.031), Vector(-4214.312,-4214.312,0), Vector(4214.313,4214.313,1992.404), START_FUN2 )
		end
	
		GLOB_FUNWAVE = {
		CreateSnapNPC("npc_combine_s",Vector(8174.5029296875,-0.44976258277893,7424.03125),Angle(0,180.98426818848,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(8060.4594726563,-154.63055419922,7424.03125),Angle(0,152.35127258301,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(8057.2783203125,130.61862182617,7424.03125),Angle(0,152.35127258301,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(7951.35546875,-10.888938903809,7424.03125),Angle(0,177.74478149414,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(7839.1259765625,208.66163635254,7424.03125),Angle(0,187.67227172852,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(7806.9228515625,-194.64282226563,7424.03125),Angle(0,180.04377746582,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(8074.1000976563,-9.4937858581543,7424.03125),Angle(0,181.40228271484,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(5105.00390625,-2.8414437770844,7168.03125),Angle(0,0.51547241210938,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(5243.208984375,-202.19418334961,7168.03125),Angle(0,0.51547241210938,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(5275.2705078125,262.33883666992,7168.03125),Angle(0,0.51547241210938,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(5440.5346679688,12.478685379028,7168.03125),Angle(0,0.51547241210938,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(4947.169921875,218.55317687988,7168.03125),Angle(0,0.51547241210938,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(4914.7236328125,-205.70808410645,7168.03125),Angle(0,0.51547241210938,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(5114.0107421875,-492.17279052734,7168.03125),Angle(0,0.51547241210938,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(5083.3393554688,477.68402099609,7168.03125),Angle(0,0.51547241210938,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(5449.81640625,-417.23696899414,7168.03125),Angle(0,0.51547241210938,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(5483.6669921875,450.55645751953,7168.03125),Angle(0,0.51547241210938,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(5541.6186523438,-261.91110229492,7168.03125),Angle(0,359.26141357422,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(5532.1953125,257.25534057617,7168.03125),Angle(0,358.84344482422,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(5284.6010742188,18.736507415771,7168.03125),Angle(0,24.132522583008,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(8534.54296875,-5.1952118873596,7168.03125),Angle(0,179.31240844727,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(8547.548828125,184.10716247559,7168.03125),Angle(0,179.31240844727,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(8552.1689453125,-219.53718566895,7168.03125),Angle(0,179.31240844727,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(8465.7548828125,-138.05018615723,7168.03125),Angle(0,179.31240844727,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(8466.6923828125,96.782913208008,7168.03125),Angle(0,179.31240844727,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(7163.21484375,-906.3037109375,7424.03125),Angle(0,96.237319946289,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(7454.9697265625,-724.080078125,7424.03125),Angle(0,91.848327636719,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(6780.8627929688,-724.56304931641,7424.03125),Angle(0,95.923820495605,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(7597.0185546875,-393.85803222656,7424.03125),Angle(0,128.94583129883,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(7624.458984375,378.06677246094,7424.03125),Angle(0,190.28729248047,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(7529.0385742188,730.86828613281,7424.03125),Angle(0,235.28729248047,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(7176.4404296875,912.8564453125,7424.03125),Angle(0,258.21221923828,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(6822.3989257813,738.57263183594,7424.03125),Angle(0,272.21520996094,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(6420.5590820313,549.9404296875,7424.03125),Angle(0,310.46215820313,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(6267.0629882813,-468.54846191406,7424.03125),Angle(0,348.18682861328,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(6176.0185546875,6.6330451965332,7424.03125),Angle(0,1.8761138916016,0),"weapon_smg1",false),
		CreateSnapNPC("npc_combine_s",Vector(8017.5668945313,-390.78048706055,8320.03125),Angle(0,95.19213104248,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(8009.5888671875,393.58312988281,8320.03125),Angle(0,207.42510986328,0),"weapon_ar2",false),
		CreateSnapNPC("npc_combine_s",Vector(8926.439453125,0.71692132949829,7669.7543945313),Angle(0,169.49440002441,0),"weapon_ar2",false)}
	
	end
	
	local START_FUN1 = function( self, ply )
	
		net.Start("DollMessage")
		net.WriteString("You're back once again.")
		net.Broadcast()
		
		timer.Simple(5,function()
			net.Start("DollMessage")
			net.WriteString("I know what you're here for, more of course.")
			net.Broadcast()
		end)
		
		timer.Simple(10,function()
			net.Start("DollMessage")
			net.WriteString("So be it, if that is your wish.")
			net.Broadcast()
		end)
		
		timer.Simple(15,function()
			net.Start("DollMessage")
			net.WriteString("Entertain Me.")
			net.Broadcast()
		end)
		
		timer.Simple(18,function()
		
			if EQ_1 and EQ_2 and IsValid(EQ_1) and IsValid(EQ_2) then
				EQ_1:Remove()
				EQ_2:Remove()
			end
		
			if ply and IsValid(ply) and ply:IsPlayer() then
				ply:SetLocalVelocity(Vector(0,0,0))
				local P1, P2 = CreateTeleporter( "FUN_PORTAL_1", ply:GetPos(), "FUN_PORTAL_2", Vector(6275.335,1.893,8608.031) )
				timer.Simple(0.1,function()
					if IsValid(P1) and IsValid(P2) then
						P1:Remove()
						P2:Remove()
					end
				end)
			end
			
		end)
		
		timer.Simple(22,function()
			FUN_WAVE1()
		end)
	
	end
	
	local BLOCK_1 = function( self, ply )
		if ply and IsValid(ply) and ply:IsPlayer() then
			if not GLOB_FUN_STARTED then
				GLOB_FUN_STARTED = true
				CreateTrigger( Vector(7156.54,0.348,14592.031), Vector(-368.576,-368.576,0), Vector(368.576,368.576,167.113), START_FUN1 )
			end
			ply:SetLocalVelocity(Vector(0,0,0))
			local P1, P2 = CreateTeleporter( "STALL_PROGRESS_1", ply:GetPos(), "STALL_PROGRESS_2", Vector(7109.736,4.518,14592.031) )
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
		end
	end
	
	local BLOCKER_1 = CreateTrigger( Vector(7360.031,-6.663,14450.314), Vector(-9305.39,-9305.39,-200), Vector(9305.39,9305.39,19.48), BLOCK_1, true )
	
	local END_BLOCKER_1 = function()
		BLOCKER_1:Remove()
	end
	
	CreateTrigger( Vector(6262.808,-2.903,8605.735), Vector(-185.991,-185.991,0), Vector(185.991,185.991,281.881), END_BLOCKER_1 )
	
	local BLOCK_2 = function( self, ply )
		if ply and IsValid(ply) and ply:IsPlayer() then
			ply:SetLocalVelocity(Vector(0,0,0))
			local P1, P2 = CreateTeleporter( "STALL_PROGRESS_1", ply:GetPos(), "STALL_PROGRESS_2", Vector(6262.476,1.951,8608.031) )
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
		end
	end
	
	local BLOCKER_2 = CreateTrigger( Vector(7459.319,-878.375,6886.741), Vector(-9276.502,-9276.502,-200), Vector(9276.502,9276.502,46.053), BLOCK_2, true )
	
	local END_BLOCKER_2 = function()
		BLOCKER_2:Remove()
	end
	
	CreateTrigger( Vector(6993.958,-3.661,4736.031), Vector(-556.122,-556.122,0), Vector(556.122,556.122,373.157), END_BLOCKER_2 )
	
	local BLOCK_3 = function( self, ply )
		if ply and IsValid(ply) and ply:IsPlayer() then
			ply:SetLocalVelocity(Vector(0,0,0))
			local P1, P2 = CreateTeleporter( "STALL_PROGRESS_1", ply:GetPos(), "STALL_PROGRESS_2", Vector(7180.395,5.66,4946.32) )
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
		end
	end
	
	local BLOCKER_3 = CreateTrigger( Vector(6499.656,-384.031,4258.27), Vector(-12766.355,-12766.355,-200), Vector(12766.355,12766.355,46.621), BLOCK_3, true )
	
	local END_BLOCKER_3 = function()
		BLOCKER_3:Remove()
	end
	
	CreateTrigger( Vector(7165.411,-5.393,2560.031), Vector(-1091.543,-1091.543,0), Vector(1091.543,1091.543,808.654), END_BLOCKER_3 )
	
	local BLOCK_4 = function( self, ply )
		if ply and IsValid(ply) and ply:IsPlayer() then
			ply:SetLocalVelocity(Vector(0,0,0))
			local P1, P2 = CreateTeleporter( "STALL_PROGRESS_1", ply:GetPos(), "STALL_PROGRESS_2", Vector(7165.337,-20.737,3012.031) )
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
		end
	end

	local BLOCKER_4 = CreateTrigger( Vector(8576.031,-264.802,1606.905), Vector(-12507.077,-12507.077,-200), Vector(12507.077,12507.077,69.981), BLOCK_4, true )
	
	local END_BLOCKER_4 = function()
		BLOCKER_4:Remove()
	end
	
	CreateTrigger( Vector(9407.19,0.31,-255.969), Vector(-873.578,-873.578,0), Vector(873.578,873.578,770.626), END_BLOCKER_4 )
	
	local BLOCK_5 = function( self, ply )
		if ply and IsValid(ply) and ply:IsPlayer() then
			ply:SetLocalVelocity(Vector(0,0,0))
			local P1, P2 = CreateTeleporter( "STALL_PROGRESS_1", ply:GetPos(), "STALL_PROGRESS_2", Vector(9954.439,-11.648,590.452) )
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
		end
	end
	
	local BLOCKER_5 = CreateTrigger( Vector(8532.488,-853.791,-2096.292), Vector(-13410.874,-13410.874,0), Vector(13410.874,13410.874,235.294), BLOCK_5, true )
	
	timer.Create("OhNo_NPCDead", 5, 0, function()
	
		if BLOCKER_1 and IsValid(BLOCKER_1) then
		
			for _,v in pairs (GLOBAL_NPCS) do
			
				if v and IsValid(v) and not v.IsDead then
				
					if v:GetPos().z <= 14450 then
						v:TakeDamage(5000)
					end
				
				end
			
			end
		
		elseif BLOCKER_2 and IsValid(BLOCKER_2) then
		
			for _,v in pairs (GLOBAL_NPCS) do
			
				if v and IsValid(v) and not v.IsDead then
				
					if v:GetPos().z <= 6886 then
						v:TakeDamage(5000)
					end
				
				end
			
			end
		
		elseif BLOCKER_3 and IsValid(BLOCKER_3) then
		
			for _,v in pairs (GLOBAL_NPCS) do
			
				if v and IsValid(v) and not v.IsDead then
				
					if v:GetPos().z <= 4258 then
						v:TakeDamage(5000)
					end
				
				end
			
			end
		
		elseif BLOCKER_4 and IsValid(BLOCKER_4) then
		
			for _,v in pairs (GLOBAL_NPCS) do
			
				if v and IsValid(v) and not v.IsDead then
				
					if v:GetPos().z <= 1606 then
						v:TakeDamage(5000)
					end
				
				end
			
			end
		
		elseif BLOCKER_5 and IsValid(BLOCKER_5) then
		
			for _,v in pairs (GLOBAL_NPCS) do
			
				if v and IsValid(v) and not v.IsDead then
				
					if v:GetPos().z <= -2096 then
						v:TakeDamage(5000)
					end
				
				end
			
			end
			
		end
	
	end)
	
end

function MAP_INSTANCE_3( haslore )

	GLOB_NO_SPECIALS = true
	GLOB_FUN_STARTED = false
	GLOB_NEXTWAVE_FUNCTION = nil

	CreatePlayerSpawn( Vector(6836.976,-1.004,-255.969), Angle(0,0,0) )
	CreateClothesLocker( Vector(7275.845,-170.309,-255.969), Angle(0,139.034,0) )
	
	CreateProp(Vector(7348,-66.719,-255.594), Angle(0.104,157.396,-0.038), "models/temporalassassin/doll/unknown.mdl", true)
	
	local STUFF_1
	
	if haslore then
	
		local KN = CreateProp(Vector(7335.594,235.844,-251.187), Angle(14.409,146.656,-82.458), "models/temporalassassin/props/malum_knife.mdl", true)
		KN:SetPhysicsWeight(5000)
		
		STUFF_1 = function()
			net.Start("MalumMessage")
			net.WriteString("I tried kid, Just head home.")
			net.Broadcast()
		end
		
	else
		
		STUFF_1 = function()
			net.Start("DollMessage")
			net.WriteString("")
			net.Broadcast()
		end
	
		local KN = CreateProp(Vector(7353.469,-53.375,-251.062), Angle(14.431,46.077,-82.463), "models/temporalassassin/props/malum_knife.mdl", true)
		KN:SetPhysicsWeight(5000)
		
	end
	
	local P1 = CreateProp(Vector(7456.406,-18.594,476.031), Angle(89.995,-0.027,180), "models/hunter/plates/plate32x32.mdl", true)
	local P2 = CreateProp(Vector(7001.281,287.813,458.188), Angle(89.995,122.86,180), "models/hunter/plates/plate32x32.mdl", true)
	local P3 = CreateProp(Vector(7022.094,-280.344,444.156), Angle(89.978,-125.145,180), "models/hunter/plates/plate32x32.mdl", true)
	local P4 = CreateProp(Vector(6996.531,5,1133.781), Angle(0.005,179.984,-0.011), "models/hunter/plates/plate32x32.mdl", true)
	local P5 = CreateProp(Vector(7354.938,-68.969,-231.937), Angle(0,4.318,180), "models/props_wasteland/laundry_basket001.mdl", true)
	
	P1:SetNoDraw(true)
	P2:SetNoDraw(true)
	P3:SetNoDraw(true)
	P4:SetNoDraw(true)
	P5:SetNoDraw(true)
	
	CreateTrigger( Vector(7341.356,0.52,-255.969), Vector(-322.009,-322.009,0), Vector(322.009,322.009,99.467), STUFF_1 )
	
	CreateCosmicStatue( Vector(7377.412,417.536,-255.969), Angle(0,-106.903,0), "lore_binderoutcast", "Faint whispers fill your mind..." )

end

function MAP_PLAYERSPAWN( ply )

	if not GLOB_ELDER_LOAD then
	
		GLOB_ELDER_LOAD = true

		RemoveAllEnts("info_player_start")
		RemoveAllEnts("info_player_terrorist")
		RemoveAllEnts("info_player_counterterrorist")
		RemoveAllEnts("trigger_changelevel")
		RemoveAllEnts("trigger_hurt")
		RemoveAllEnts("game_text")

		if ply:HasLoreFile("lore_mthuulra_char2") then
			GLOB_NO_SAVEDATA = true
			GLOB_SAFE_GAMEEND = true
			GLOB_SUPER_DIE = false
			GLOB_NO_SKIP1 = false
			GLOB_NO_SPECIALS = false
			GLOB_FUN_STARTED = false
			GLOB_FUNWAVE = false
			GLOB_NEXTWAVE_FUNCTION = nil
			MAP_INSTANCE_3(ply:HasLoreFile("lore_binderoutcast"))
			timer.Simple(0.1,function()
				ply:Spawn()
			end)		
		elseif ply:HasLoreFile("lore_mthuulra_char") then
			GLOB_NO_SAVEDATA = true
			GLOB_SAFE_GAMEEND = true
			GLOB_SUPER_DIE = false
			GLOB_NO_SKIP1 = false
			GLOB_NO_SPECIALS = false
			GLOB_FUN_STARTED = false
			GLOB_FUNWAVE = false
			GLOB_NEXTWAVE_FUNCTION = nil
			MAP_INSTANCE_2()
			timer.Simple(0.1,function()
				ply:Spawn()
			end)
		else
			GLOB_NO_SAVEDATA = true
			GLOB_SAFE_GAMEEND = true
			GLOB_SUPER_DIE = false
			GLOB_NO_SKIP1 = false
			GLOB_NO_SPECIALS = false
			GLOB_FUN_STARTED = false
			GLOB_FUNWAVE = false
			GLOB_NEXTWAVE_FUNCTION = nil
			MAP_INSTANCE_1()
			timer.Simple(0.1,function()
				ply:Spawn()
			end)
		end
		
	end

end
hook.Add("PlayerSpawn","MAP_PLAYERSPAWNHOOK",MAP_PLAYERSPAWN)

function DOLL_LOOKER()

	if GLOB_DOLL_OBJ2 and IsValid(GLOB_DOLL_OBJ2) and IsValid(player.GetAll()[1]) then
	
		local PLAYER = player.GetAll()[1]
		local POS = GLOB_DOLL_OBJ2:GetPos()
		POS.z = PLAYER:GetPos().z - 1500
		
		local ANG = (PLAYER:GetPos() - (POS + Vector(0,0,1500))):Angle()
		ANG:RotateAroundAxis( ANG:Right(),-20 )
		
		GLOB_DOLL_OBJ2:SetPos( POS )
		GLOB_DOLL_OBJ2:SetAngles( ANG )
	
	end

	if GLOB_DOLL_OBJ and IsValid(GLOB_DOLL_OBJ) and IsValid(player.GetAll()[1]) then
	
		local PLAYER = player.GetAll()[1]
		local ANG = (PLAYER:GetPos() - GLOB_DOLL_OBJ:GetPos()):Angle()
		
		GLOB_DOLL_OBJ:SetAngles( Angle(0,ANG.y,0) )
	
	end

end
hook.Add("Think","DOLL_LOOKERHook",DOLL_LOOKER)

function FUNWAVES_OnNPCKilled( npc, att, inf )

	if GLOB_FUNWAVE then
	
		for _,v in pairs (GLOB_FUNWAVE) do
		
			if v and IsValid(v) and v == npc then
				table.remove(GLOB_FUNWAVE,_)
			end
		
		end
		
		if table.Count(GLOB_FUNWAVE) <= 0 and GLOB_NEXTWAVE_FUNCTION then
			GLOB_NEXTWAVE_FUNCTION()
			GLOB_NEXTWAVE_FUNCTION = nil
		end
	
	end

end
hook.Add("OnNPCKilled","FUNWAVES_OnNPCKilledHook",FUNWAVES_OnNPCKilled)

timer.Create("FuckOff_NoSkipping", 1, 0, function()

	if GetConVarNumber("ta_sv_skill") < MINIMUM_SKILL then
		RunConsoleCommand("ta_sv_skill",MINIMUM_SKILL)
	end
	
	if GetConVarNumber("ta_dev_developermode") < 1 then
	
		local PLAYERS = player.GetAll()
		
		for _,v in pairs (PLAYERS) do
		
			v:SetMoveType(MOVETYPE_WALK)
			v:SetBackpack(false)
			
			TAS:SetArmour2(v,0)
			
			if GLOB_NO_SPECIALS then
			
				TAS:TakeTemporalMana( v, 500 )
				TAS:TakeTemporalBurst( v, 500 )
			
				for xx,mm in pairs (v:GetWeapons()) do
					if IsValid(mm) and mm.TA_MaxAmmunition and not mm.AMMO_MOD then
						mm.AMMO_MOD = true
						mm.TA_MaxAmmunition = mm.TA_MaxAmmunition*0.5
						mm:SetSharedFloat("TA_MaxAmmunition",mm.TA_MaxAmmunition)
						mm:SetSharedBool("AMMO_MOD",true)
					end
				end
				
			end
		
			if TAS:GetSuit(v) == 5 then
			
				local SUIT = 0
			
				if SUIT then
				
					v.Assassin_CurrentSuit = SUIT
					v:SetSharedFloat("Assassin_CurrentSuit",SUIT)
					
					if SUIT == 16 then
						v:SetMaxHealth(50)
					else
						v:SetMaxHealth(100)
					end
					
				end
			
				v:ApplyPlayerMaterials()
				
			end
		
			v:GodDisable()
			v.GOD_ON = false
			
			if GLOB_NO_SKIP1 and v:GetPos().z > 2160 then
				v:SetPos(Vector(9387.136,-6.055,-255.968))
			end
		
		end
		
	end

end)

function ElderThrone_EntityTakeDamage( ent, dmginfo )

	if ent:IsPlayer() then
		dmginfo:ScaleDamage(4.5)
	end

end
hook.Add("EntityTakeDamage","ElderThrone_EntityTakeDamageHook",ElderThrone_EntityTakeDamage)