		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["08_ov_pension"] = {"09_ov_firm_hq", Vector(4051.492,3527.38,-79.969), Vector(-83.225,-83.225,0), Vector(83.225,83.225,139.585)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-2560.149,-1341.736,-88.015), Angle(0,31.056,0))
	
	Create_Medkit25( Vector(1226.539, -901.54, -87.999), Angle(0, 96.118, 0) )
	Create_Medkit10( Vector(1141.159, -637.872, -86.287), Angle(-90, 50.456, 0) )
	Create_Armour100( Vector(1276.024, -907.457, -79.228), Angle(-90, -50.896, -180) )
	Create_SMGAmmo( Vector(1124.08, -668.344, -87.773), Angle(0, 5.134, 0) )
	Create_SMGAmmo( Vector(1125.978, -676.58, -87.687), Angle(0, 4.782, 0) )
	Create_PistolAmmo( Vector(1144.6, -630.717, -87.21), Angle(7.136, 399.619, 0) )
	Create_SMGWeapon( Vector(342.098, 483.458, 33.173), Angle(0, 269.312, 0) )
	Create_Bread( Vector(36.213, 954.002, 9.891), Angle(0, 92.616, 0) )
	Create_Bread( Vector(13.946, 948.286, 8.588), Angle(0, 40.432, 0) )
	Create_Bread( Vector(44.709, 941.009, 9.425), Angle(0, 104.144, 0) )
	Create_Beer( Vector(47.226, 955.42, 32.114), Angle(0, 108.016, 0) )
	Create_Beer( Vector(36.33, 955.665, 33.155), Angle(0, -232.712, 0) )
	Create_Medkit10( Vector(373.493, -97.003, 0.112), Angle(0, 293.952, 0) )
	Create_Armour5( Vector(487.209, -103.334, 10.664), Angle(0, 126.32, 0) )
	Create_Armour5( Vector(487.473, -91.52, 10.579), Angle(0, 61.375, 0) )
	Create_ShotgunWeapon( Vector(-606.456, 410.48, -157.501), Angle(0, 282.687, 0) )
	Create_Beer( Vector(-618.451, 442.11, -156.95), Angle(0, 108.623, 0) )
	Create_Beer( Vector(-601.264, 453.722, -156.821), Angle(0, 111.263, 0) )
	Create_Beer( Vector(-615.363, 466.475, -157.82), Angle(0, 111.439, 0) )
	Create_Beer( Vector(-614.237, 523.009, -155.863), Angle(0, 149.631, 0) )
	Create_Beer( Vector(-604.371, 558.834, -155.863), Angle(0, 101.759, 0) )
	Create_Beer( Vector(-622.13, 574.379, -155.863), Angle(0, 104.575, 0) )
	Create_Beer( Vector(-620.741, 618.497, -155.863), Angle(0, 127.807, 0) )
	Create_Bread( Vector(-590.531, 616.835, -156.152), Angle(0, 16.223, 0) )
	Create_GrenadeAmmo( Vector(26.508, 311.021, -127.537), Angle(0, 165.903, 0) )
	Create_GrenadeAmmo( Vector(19.334, 304.24, -125.813), Angle(0, 150.767, 0) )
	Create_Beer( Vector(-312.568, 1095.588, 49.031), Angle(0, 5.568, 0) )
	Create_Beer( Vector(-287.013, 1106.303, 37.031), Angle(0, 40.591, 0) )
	Create_Bread( Vector(-109.521, 1117.043, 37.031), Angle(0, 179.887, 0) )
	Create_Armour100( Vector(-48.957, 1180.031, 160.261), Angle(0, 63.023, 0) )
	Create_Medkit25( Vector(-46.798, 1384.139, 200.031), Angle(0, 314.351, 0) )
	Create_PortableMedkit( Vector(110.688, 1380.853, 200.031), Angle(0, 218.783, 0) )
	Create_RifleWeapon( Vector(-528.556, 630.826, 200.336), Angle(-11.398, -185.939, -0.136) )
	Create_ShotgunAmmo( Vector(-530.271, 490.029, 200.2), Angle(0, 117.151, 0) )
	Create_RevolverWeapon( Vector(521.925, -254.031, 188.293), Angle(0, 123.919, 0) )
	Create_Beer( Vector(-24.652, 19.94, 200.031), Angle(0, 295.335, 0) )
	Create_Bread( Vector(-44.068, 15.213, 200.031), Angle(0, 291.727, 0) )
	Create_RevolverAmmo( Vector(150.791, 1101.866, 198.031), Angle(0, 309.502, 0) )
	Create_Medkit10( Vector(482.91, 550.198, 378.031), Angle(0, 225.007, 0) )
	Create_Beer( Vector(351.395, 977.047, 379.831), Angle(0, 4.655, 0) )
	Create_Beer( Vector(352.887, 966.164, 379.831), Angle(0, 21.198, 0) )

	local GR_B1 = function()
		Create_CorzoHat( Vector(175.047, 403.884, 336.031), Angle(0, 5.117, 0) )
	end
	
	local OBJECTS = {}
	table.insert(OBJECTS,CreateProp(Vector(197.25,447.219,364.844), Angle(0.044,-65.259,0), "models/items/plate.mdl", true))
	table.insert(OBJECTS,CreateProp(Vector(170.094,424.531,358.594), Angle(-1.318,41.221,55.327), "models/temporalassassin/props/corzo_cintia1901.mdl", true))
	table.insert(OBJECTS,CreateProp(Vector(160.531,352.094,393.469), Angle(2.021,0.044,0.923), "models/props_gameplay/towel_rack.mdl", true))

	AddSecretButton(Vector(173.031,410.432,392.031), GR_B1, "TODAY A HAT, TOMMOROW ANOTHER ONE (Password: Terminus Est)", "TemporalAssassin/Secrets/Secret_Find2.wav")
	
	local VEH = ents.Create("prop_vehicle_jeep")
	VEH:SetModel("models/vehicle.mdl")
	VEH:SetKeyValue( "vehiclescript", "scripts/vehicles/jalopy.txt" )
	VEH:SetPos( Vector(-2402.625,-1347.281,-87.594) )
	VEH:SetAngles( Angle(0.439,-19.819,1.802) )
	VEH:Spawn()
	VEH:Activate()
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)