
if CLIENT then return end

GLOB_SCENE_SKIP_AVAILABLE = true

function MAP_LOADED_FUNC()

	CreateClothesLocker( Vector(-6570.478,-986.315,0.031), Angle(0,20.499,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Medkit25( Vector(-7077.013671875, -1329.2960205078, 0.03125), Angle(0, 180.55659484863, 0) )
		Create_Medkit25( Vector(-7093.21484375, -1273.9086914063, 0.03125), Angle(0, 179.23658752441, 0) )
		Create_Medkit25( Vector(-7112.7705078125, -1226.7604980469, 0.031246185302734), Angle(0, 233.79670715332, 0) )
		Create_PortableMedkit( Vector(-7085.9555664063, -1522.3920898438, 0.031253814697266), Angle(0, 116.09664154053, 0) )
		Create_PortableMedkit( Vector(-7088.0874023438, -1490.6049804688, 0.03125), Angle(0, 125.11668395996, 0) )
		Create_SpiritEye2( Vector(-7230.369140625, -1435.7442626953, 0.03125), Angle(0, 63.956596374512, 0) )
		Create_Backpack( Vector(-7140.9833984375, -1523.4029541016, 0.031246185302734), Angle(0, 90.576614379883, 0) )
		Create_Backpack( Vector(-7141.2661132813, -1495.2957763672, 0.03125), Angle(0, 90.576614379883, 0) )
		Create_Armour200( Vector(-6372.5278320313, -1569.1481933594, 184.03125), Angle(0, 188.91693115234, 0) )
		Create_Beer( Vector(-6399.7719726563, -1559.9930419922, 184.03125), Angle(0, 203.87692260742, 0) )
		Create_Beer( Vector(-6400.1557617188, -1576.3927001953, 184.03125), Angle(0, 185.39692687988, 0) )
		Create_Beer( Vector(-6419.8994140625, -1558.9940185547, 184.03125), Angle(0, 219.0569152832, 0) )
		Create_Beer( Vector(-6420.0922851563, -1573.0578613281, 184.03125), Angle(0, 196.17691040039, 0) )
		Create_Beer( Vector(-6441.8740234375, -1560.3298339844, 184.03125), Angle(0, 229.39694213867, 0) )
		Create_Beer( Vector(-6441.888671875, -1575.2208251953, 184.03125), Angle(0, 200.13688659668, 0) )
		
		CreateSecretArea( Vector(-6408.009765625,-1590.9416503906,188.5625), Vector(-39.400886535645,-39.400886535645,0), Vector(39.400886535645,39.400886535645,89.418762207031) )
		
	else
	
		CreateProp(Vector(-6400.156,-686.906,15.406), Angle(-0.132,94.307,0), "models/props_lab/filecabinet02.mdl", true)
		CreateProp(Vector(-6400.312,-687.031,45.75), Angle(-0.044,90.264,-0.044), "models/props_lab/filecabinet02.mdl", true)
		local P1 = CreateProp(Vector(-6395.875,-691.437,69.938), Angle(0.22,-1.055,0.176), "models/props_lab/desklamp01.mdl", true)
		local P2 = CreateProp(Vector(-6371.719,-586.281,14.969), Angle(-0.352,-86.792,0.132), "models/props_combine/breenbust.mdl", true)
		P2:SetHealth(50000)
	
		Create_ShadeCloak( Vector(-7186.244, -1355.17, 0.031), Angle(0, 268.582, 0) )
		CreateEquipmentLocker( Vector(-7124.406,-1657.906,12.031), Angle(0,90,0) )
		
		Create_Beer( Vector(-6653.75, -1244.827, 30.002), Angle(0, 174.852, 0) )
		Create_Medkit25( Vector(-6415.528, -1461.212, 0.031), Angle(0, 154.376, 0) )
		Create_Medkit25( Vector(-6413.107, -1395.086, 0.031), Angle(0, 201.609, 0) )
		Create_Medkit25( Vector(-6435.638, -1413.728, 0.031), Angle(0, 189.697, 0) )
		Create_Medkit25( Vector(-6439.039, -1445.826, 0.031), Angle(0, 148.106, 0) )
		
		local S_BUTTON1 = function()
		
			if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_6.txt","DATA") then
				Create_LoreItem( Vector(-6551.772,-597.453,0.031), Angle(0,-9.021,0), "lore_mthuulra_6", "Lore File Unlocked: Mthuulra #6" )
			end
		
			CreateParticleEffect( "Doll_EvilSpawnItem", Vector(-6493.205,-444.403,37.781), Angle(0,0,0), 1 )
			CreateProp(Vector(-6492.625,-392.625,21.656), Angle(-4.79,-84.771,0), "models/temporalassassin/doll/unknown.mdl", true)
			local SP1 = CreateProp(Vector(-6489.531,-389.562,19.875), Angle(-0.044,-86.836,0), "models/props_c17/bench01a.mdl", true)
			SP1:SetHealth(5000)
			local SP2 = CreateProp(Vector(-6457.969,-394.094,0.344), Angle(0.396,170.024,0.571), "models/maxofs2d/camera.mdl", true)
			local SP3 = CreateProp(Vector(-6466.25,-387.281,21.719), Angle(-4.482,-80.42,-0.879), "models/temporalassassin/items/weapon_photoop.mdl", true)
			local SP4 = CreateProp(Vector(-6522.094,-389.812,21.125), Angle(-2.461,-32.124,-1.582), "models/temporalassassin/items/weapon_griseus.mdl", true)
			local SP5 = CreateProp(Vector(-6527.875,-394.187,0.969), Angle(-25.972,157.72,-35.2), "models/temporalassassin/items/phial.mdl", true)
			
			Create_CorzoHat( Vector(-6411.896, -395.022, 0.031), Angle(0, 239.64, 0) )
			
			local E_COAT_CHECK = function(self,ply)
			
				if TAS:GetSuit(ply) == 12 or ply.Eldritch_Buff then
				
					net.Start("GriseusMessage")
					net.WriteString("Her unshackled bloodlust is starting to worry me.")
					net.Broadcast()
					
					timer.Simple(5,function()
						net.Start("TwinsMessageA")
						net.WriteString("... I gotta agree, especially after seeing her in action like this.")
						net.Broadcast()
					end)
					
					timer.Simple(10,function()
						net.Start("DollMessage")
						net.WriteString("My daughter is not of your concern")
						net.Broadcast()
					end)
					
					timer.Simple(15,function()
						net.Start("GriseusMessage")
						net.WriteString("And what about that enabling prick with a hat?")
						net.Broadcast()
					end)
					
					timer.Simple(20,function()
						net.Start("TwinsMessageA")
						net.WriteString("We're under the impression that he's teaching her his ways, though we're not certain.")
						net.Broadcast()
					end)
					
					timer.Simple(25,function()
						net.Start("DollMessage")
						net.WriteString("Enough, his travels are his own, we shall see in time")
						net.Broadcast()
					end)
				
				end
			
			end
			
			CreateTrigger( Vector(-6491.839,-394.068,0.031), Vector(-58.017,-58.017,0), Vector(58.017,58.017,71.342), E_COAT_CHECK )
		
		end
		
		AddSecretButton( Vector(-6369.175,-703.969,54.566), S_BUTTON1, "A VEIL HAS BEEN LIFTED", "TemporalAssassin/Secrets/Secret_Spooky.wav" )
		
		CreateSecretArea( Vector(-7157.423,-1654.657,12.031), Vector(-89.129,-89.129,0), Vector(89.129,89.129,184.312) )
		
		local REMIND_LORE = function()
			if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_6.txt","DATA") then
				net.Start("DollMessage")
				net.WriteString("I have the last book around here somewhere")
				net.Broadcast()
				net.Start("DollMessage")
				net.WriteString("Find me and it is yours")
				net.Broadcast()
			end
		end
		
		CreateTrigger( Vector(-6467.286,-1107.125,0.031), Vector(-72.745,-72.745,0), Vector(72.745,72.745,120.107), REMIND_LORE )
	
	end
	
	local FUCK_DOORS_START = function()
	
		timer.Create("FIX_THE_DOORS", 155, 1, function()
		
			local DOOR1 = GetEntityByID(1509)
			local DOOR2 = GetEntityByID(1510)
			
			if IsValid(DOOR1) and IsValid(DOOR2) then
				DOOR1:Fire("Unlock")
				DOOR2:Fire("Unlock")
				DOOR1:Fire("Open")
				DOOR2:Fire("Open")
			end
			
			local BRUSH = GetEntityByID(1722)
			
			if IsValid(BRUSH) then
				BRUSH:Remove()
			end
			
		end)
		
	end
	CreateTrigger( Vector(-7188.439,-1177.35,10.031), Vector(-102.843,-102.843,0), Vector(102.843,102.843,55.1), FUCK_DOORS_START )
	
	local STOP_SCENESKIP = function()
		SetSceneSkipAvailable(false)
	end
	
	CreateTrigger( Vector(-6472.142,-982.947,0.031), Vector(-75.466,-75.466,0), Vector(75.466,75.466,105.441), STOP_SCENESKIP )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)