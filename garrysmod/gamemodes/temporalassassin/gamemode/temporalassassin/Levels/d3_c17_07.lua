	
if CLIENT then return end

function EXTRA_FAIL_STATE( npc, att, inf )

	if IsValid(npc) and ( npc:GetClass() == "npc_alyx" ) then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRA_FAIL_STATEHook",EXTRA_FAIL_STATE)

NPC_NO_DUPES = true

function MAP_DO_RESET( ply )

	NPC_NO_DUPES = true

end
hook.Add("PlayerDeath","MAP_DO_RESETHook",MAP_DO_RESET)

function MAP_LOADED_FUNC()

	NPC_NO_DUPES = true

	RemoveAllEnts("info_player_start")
	
	CreatePlayerSpawn( Vector(4431,1205,280.4), Angle(0,0,0) )
	
	local RE_ENABLE_DIFFICULTY = function()
		NPC_NO_DUPES = false
	end
	
	CreateTrigger( Vector(4746.419,664.914,128.031), Vector(-113.093,-113.093,0), Vector(113.093,113.093,119.938), RE_ENABLE_DIFFICULTY )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Medkit25( Vector(4056.0676269531, 1140.626953125, 152.03125), Angle(0, 270.27947998047, 0) )
		Create_Beer( Vector(4056.0871582031, 1106.8829345703, 144.03125), Angle(0, 269.3994140625, 0) )
		Create_Beer( Vector(4056.4831542969, 1095.7857666016, 136.03125), Angle(0, 268.95941162109, 0) )
		Create_Beer( Vector(4056.029296875, 1081.9857177734, 128.03125), Angle(0, 269.17944335938, 0) )
		Create_Beer( Vector(4055.8662109375, 1070.5856933594, 120.03125), Angle(0, 269.17944335938, 0) )
		Create_Beer( Vector(4055.7890625, 1057.5731201172, 112.03125), Angle(0, 268.95941162109, 0) )
		Create_Beer( Vector(4055.5773925781, 1045.9151611328, 104.03125), Angle(0, 268.95941162109, 0) )
		Create_Beer( Vector(4055.1525878906, 1033.3862304688, 96.03125), Angle(0, 234.41940307617, 0) )
		Create_Medkit25( Vector(4200.4721679688, 916.35491943359, 128.03125), Angle(0, 321.14874267578, 0) )
		Create_Medkit25( Vector(4602.6645507813, 927.87280273438, 128.03125), Angle(0, 232.84870910645, 0) )
		Create_RevolverAmmo( Vector(4191.6455078125, 649.71313476563, 128.03125), Angle(0, 15.04850769043, 0) )
		Create_RevolverAmmo( Vector(4190.0024414063, 691.93829345703, 128.03125), Angle(0, 345.78833007813, 0) )
		Create_SuperShotgunWeapon( Vector(5125.7807617188, 671.62542724609, 128.03125), Angle(0, 179.82830810547, 0) )
		Create_ShotgunAmmo( Vector(5156.4599609375, 698.02264404297, 128.03125), Angle(0, 195.88830566406, 0) )
		Create_ShotgunAmmo( Vector(5156.16015625, 651.10327148438, 128.03125), Angle(0, 167.28831481934, 0) )
		Create_Medkit10( Vector(4753.728515625, 1252.26953125, 128.03125), Angle(0, 358.64547729492, 0) )
		Create_Medkit10( Vector(4759.5297851563, 1198.4213867188, 129.21890258789), Angle(0, 6.5653381347656, 0) )
		Create_PortableMedkit( Vector(4945.1254882813, 903.88116455078, 151.42124938965), Angle(0, 123.6053237915, 0) )
		Create_SMGAmmo( Vector(4240.2075195313, 1276.4976806641, 128.03125), Angle(0, 91.04613494873, 0) )
		Create_SMGAmmo( Vector(4239.6870117188, 1304.9653320313, 128.03125), Angle(0, 91.04613494873, 0) )
		Create_SMGAmmo( Vector(4239.1337890625, 1335.2918701172, 128.03125), Angle(0, 91.04613494873, 0) )
		Create_SMGAmmo( Vector(4334.1494140625, 1280.1821289063, 128.03125), Angle(0, 91.926055908203, 0) )
		Create_SMGAmmo( Vector(4331.8305664063, 1322.6843261719, 128.03125), Angle(0, 91.266052246094, 0) )
		Create_SMGAmmo( Vector(4331.4013671875, 1356.9595947266, 128.03125), Angle(0, 91.486053466797, 0) )
		Create_Medkit25( Vector(4434.1499023438, 1284.0935058594, 128.03125), Angle(0, 90.386016845703, 0) )
		Create_SMGWeapon( Vector(4345.0390625, 1535.4997558594, 128.03125), Angle(0, 269.62585449219, 0) )
		Create_ShotgunWeapon( Vector(4344.5654296875, 1492.7961425781, 128.03125), Angle(0, 269.84582519531, 0) )
		Create_PistolWeapon( Vector(4243.7700195313, 1539.9848632813, 128.03125), Angle(0, 269.62567138672, 0) )
		Create_SuperShotgunWeapon( Vector(4242.728515625, 1507.3520507813, 128.03125), Angle(0, 270.28567504883, 0) )
		Create_KingSlayerWeapon( Vector(4433.0112304688, 1332.3399658203, 128.03125), Angle(0, 90.545478820801, 0) )
		Create_VolatileBattery( Vector(4235.7802734375, 1638.1223144531, 128.03125), Angle(0, 248.28497314453, 0) )
		Create_Medkit25( Vector(5147.8330078125, 1616.7703857422, 128.03125), Angle(0, 181.70457458496, 0) )
		Create_Armour( Vector(4704.1328125, 1337.3825683594, 0.031246185302734), Angle(0, 99.508316040039, 0) )
		Create_PistolAmmo( Vector(4647.095703125, 1329.2492675781, 0.03125), Angle(0, 66.068237304688, 0) )
		Create_SpiritEye1( Vector(4680.4438476563, 1237.8721923828, 0.03125), Angle(0, 269.65808105469, 0) )
		Create_ShotgunAmmo( Vector(5257.1215820313, 1361.9249267578, 36.03125), Angle(0, 45.398071289063, 0) )
		Create_ShotgunAmmo( Vector(5258.0361328125, 1388.3104248047, 36.03125), Angle(0, 23.39794921875, 0) )
		Create_ShotgunAmmo( Vector(5258.0776367188, 1411.9226074219, 36.03125), Angle(0, 353.91778564453, 0) )
		Create_ShotgunAmmo( Vector(5257.833984375, 1441.3421630859, 36.03125), Angle(0, 322.23764038086, 0) )
		Create_SMGAmmo( Vector(5257.9345703125, 1470.1004638672, 36.03125), Angle(0, 352.15759277344, 0) )
		Create_SMGAmmo( Vector(5255.6958007813, 1494.7592773438, 36.03125), Angle(0, 334.11749267578, 0) )
		Create_SMGAmmo( Vector(5258.6977539063, 1515.7434082031, 36.03125), Angle(0, 318.05740356445, 0) )
		Create_Armour200( Vector(5214.3090820313, 1402.9636230469, 0.03125), Angle(0, 101.05776977539, 0) )
		Create_Medkit25( Vector(5448.1372070313, 1450.8787841797, 0.03125), Angle(0, 180.61814880371, 0) )
		Create_Medkit25( Vector(5445.8266601563, 1365.7927246094, 0.03125), Angle(0, 90.858108520508, 0) )
		Create_RevolverAmmo( Vector(5309.3061523438, 1626.9344482422, 2.0312538146973), Angle(0, 291.49826049805, 0) )
		Create_RevolverAmmo( Vector(5323.3056640625, 1627.2393798828, 2.0312538146973), Angle(0, 273.8981628418, 0) )
		Create_RevolverAmmo( Vector(5371.2036132813, 1627.7119140625, 2.03125), Angle(0, 251.45819091797, 0) )
		Create_M6GAmmo( Vector(5407.568359375, 1642.0866699219, 0.031253814697266), Angle(0, 238.03811645508, 0) )
		Create_PistolAmmo( Vector(5284.9086914063, 1645.1875, 0.03125), Angle(0, 281.15826416016, 0) )
		Create_PistolAmmo( Vector(5250.2924804688, 1648.7950439453, 0.03125), Angle(0, 300.95834350586, 0) )
		Create_PistolAmmo( Vector(5225.1108398438, 1643.7459716797, 0.03125), Angle(0, 313.71838378906, 0) )
		Create_PistolAmmo( Vector(5195.4345703125, 1642.2203369141, 0.03125), Angle(0, 323.61846923828, 0) )
		Create_PortableMedkit( Vector(5191.666015625, 1522.1229248047, 0.031253814697266), Angle(0, 21.258743286133, 0) )
		Create_PortableMedkit( Vector(5223.8510742188, 1528.7016601563, 0.03125), Angle(0, 24.998764038086, 0) )
		Create_RevolverWeapon( Vector(6298.0078125, 1582.3143310547, 40.03125), Angle(0, 150.02301025391, 0) )
		Create_RevolverAmmo( Vector(6300.9091796875, 1487.5129394531, 40.03125), Angle(0, 212.28298950195, 0) )
		Create_ShotgunWeapon( Vector(6395.9506835938, 1477.4208984375, 40.03125), Angle(0, 302.48303222656, 0) )
		Create_SMGWeapon( Vector(6395.3916015625, 1597.7028808594, 40.03125), Angle(0, 39.503005981445, 0) )

		CreateSecretArea( Vector(4219.1513671875,1631.7211914063,132.5625), Vector(-39.474590301514,-39.474590301514,0), Vector(39.474590301514,39.474590301514,55.679244995117) )
		CreateSecretArea( Vector(4679.0986328125,1154.9016113281,0.03125), Vector(-47.030044555664,-47.030044555664,0), Vector(47.030044555664,47.030044555664,119.9375) )
		
	else

		CreateClothesLocker( Vector(5266.044,1644.264,0.031), Angle(0,267.453,0) )
	
		Create_Medkit25( Vector(4699.83, 811.293, 280.031), Angle(0, 99.432, 0) )
		Create_Medkit25( Vector(4657.79, 810.749, 280.031), Angle(0, 73.725, 0) )
		Create_InfiniteGrenades( Vector(4522.757, 750.752, 280.031), Angle(0, 129.946, 0) )
		Create_Armour100( Vector(4197.962, 646.936, 128.031), Angle(0, 23.669, 0) )
		Create_FoolsOathAmmo( Vector(4185.951, 701.877, 128.031), Angle(0, 257.61, 0) )
		Create_Medkit25( Vector(5148.773, 687.253, 128.031), Angle(0, 186.68, 0) )
		Create_PortableMedkit( Vector(5163.006, 643.447, 128.031), Angle(0, 166.825, 0) )
		Create_CrossbowWeapon( Vector(4677.551, 933.303, 280.031), Angle(0, 90.021, 0) )
		Create_ShotgunWeapon( Vector(4676.875, 968.81, 280.031), Angle(0, 89.603, 0) )
		Create_FoolsOathWeapon( Vector(4676.355, 995.416, 280.031), Angle(0, 88.976, 0) )
		Create_FrontlinerWeapon( Vector(4677.518, 1025.285, 280.031), Angle(0, 90.021, 0) )
		Create_SMGWeapon( Vector(4678.182, 1052.909, 280.031), Angle(0, 89.812, 0) )
		Create_KingSlayerWeapon( Vector(4678.588, 1077.992, 280.031), Angle(0, 90.23, 0) )
		Create_RifleWeapon( Vector(4678.707, 1096.289, 280.031), Angle(0, 90.648, 0) )
		Create_RevolverWeapon( Vector(4678.202, 1116.435, 280.031), Angle(0, 90.021, 0) )
		Create_PistolWeapon( Vector(4677.582, 1136.576, 280.031), Angle(0, 89.603, 0) )
		Create_SuperShotgunWeapon( Vector(4677.611, 1154.5, 280.031), Angle(0, 89.812, 0) )
		Create_M6GWeapon( Vector(4677.39, 1177.988, 280.031), Angle(0, 89.812, 0) )
		Create_SpiritSphere( Vector(4242.767, 1417.758, 128.031), Angle(0, 358.688, 0) )
		Create_ShotgunAmmo( Vector(5091.267, 1224.018, 128.031), Angle(0, 152.196, 0) )
		Create_ShotgunAmmo( Vector(5096.509, 1246.168, 128.031), Angle(0, 170.588, 0) )
		Create_SMGAmmo( Vector(5070.262, 1241.472, 128.031), Angle(0, 159.302, 0) )
		Create_FrontlinerAmmo( Vector(5104.316, 1138.94, 128.031), Angle(0, 160.974, 0) )
		Create_PistolAmmo( Vector(5084.455, 746.55, 128.031), Angle(0, 106.007, 0) )
		Create_PistolAmmo( Vector(5106.867, 745.64, 128.031), Angle(0, 118.443, 0) )
		Create_PistolAmmo( Vector(5106.985, 776.817, 128.031), Angle(0, 130.251, 0) )
		Create_Armour( Vector(5101.322, 817.674, 128.031), Angle(0, 178.739, 0) )
		Create_Armour5( Vector(5086.973, 845.758, 128.031), Angle(0, 211.761, 0) )
		Create_Armour5( Vector(5089.408, 884.691, 128.031), Angle(0, 235.169, 0) )
		Create_EldritchArmour10( Vector(4905.307, 915.253, 128.031), Angle(0, 267.355, 0) )
		Create_EldritchArmour10( Vector(4936.746, 889.374, 128.031), Angle(0, 215.314, 0) )
		Create_SMGAmmo( Vector(4866.275, 1092.651, 128.031), Angle(0, 207.999, 0) )
		Create_RevolverAmmo( Vector(4844.775, 1095.021, 128.031), Angle(0, 221.584, 0) )
		Create_RevolverAmmo( Vector(4847.997, 1081.574, 128.031), Angle(0, 206.536, 0) )
		Create_VolatileBattery( Vector(4228.935, 1641.862, 128.031), Angle(0, 297.869, 0) )
		Create_ManaCrystal100( Vector(5128.316, 1614.617, 128.031), Angle(0, 180.62, 0) )
		Create_HarbingerAmmo( Vector(4596.12, 1279.964, 128.031), Angle(0, 87.305, 0) )
		Create_Beer( Vector(4608.048, 1687.104, 128.031), Angle(0, 19.798, 0) )
		Create_Beer( Vector(4604.828, 1759.234, 128.031), Angle(0, 327.966, 0) )
		Create_Bread( Vector(4612.663, 1721.07, 128.031), Angle(0, 352.837, 0) )
		Create_Backpack( Vector(4785.046, 1815.365, 0.031), Angle(0, 180.519, 0) )
		Create_Medkit25( Vector(4967.427, 1289.744, 0.031), Angle(0, 89.706, 0) )
		Create_M6GAmmo( Vector(5024.535, 1088.327, 0.031), Angle(0, 67.97, 0) )
		Create_M6GAmmo( Vector(5049.622, 1099.765, 0.031), Angle(0, 92.841, 0) )
		Create_CorzoHat( Vector(4682.01, 1222.056, 0.031), Angle(0, 264.843, 0) )
		Create_InfiniteGrenades( Vector(5206.183, 1632.966, 0.031), Angle(0, 308.105, 0) )
		Create_Backpack( Vector(5224.768, 1396.321, 0.031), Angle(0, 113.633, 0) )
		Create_Backpack( Vector(5223.005, 1502.087, 0.031), Angle(0, 227.956, 0) )
		Create_Backpack( Vector(5230.442, 1464.764, 0.031), Angle(0, 184.066, 0) )
		Create_Backpack( Vector(5228.65, 1427.824, 0.031), Angle(0, 182.603, 0) )
		Create_Backpack( Vector(5192.622, 1395.653, 0.031), Angle(0, 86.254, 0) )
		Create_HarbingerWeapon( Vector(5365.234, 1409.153, 0.031), Angle(0, 90.746, 0) )
		Create_WhisperWeapon( Vector(5319.087, 1544.324, 0.031), Angle(0, 80.923, 0) )
		Create_Medkit25( Vector(6101.382, 1425.755, 0.031), Angle(0, 180.3, 0) )
		Create_Medkit25( Vector(6101.704, 1364.014, 0.031), Angle(0, 180.3, 0) )
		Create_Medkit25( Vector(6102.044, 1299.181, 0.031), Angle(0, 180.3, 0) )
		Create_Medkit25( Vector(6102.371, 1236.904, 0.031), Angle(0, 180.3, 0) )
		Create_Medkit25( Vector(6100.213, 1639.516, 0.031), Angle(0, 179.882, 0) )
		Create_Medkit25( Vector(6101.76, 1699.368, 0.031), Angle(0, 181.136, 0) )
		Create_Medkit25( Vector(6100.646, 1755.616, 0.031), Angle(0, 181.136, 0) )
		Create_Medkit25( Vector(6099.337, 1821.675, 0.031), Angle(0, 181.136, 0) )
		Create_Armour100( Vector(6247.958, 1536.972, -3.969), Angle(0, 187.406, 0) )
		Create_Armour100( Vector(6350.166, 1642.227, -3.969), Angle(0, 93.565, 0) )
		Create_Armour100( Vector(6352.269, 1425.859, -3.969), Angle(0, 269.961, 0) )
		Create_EldritchArmour( Vector(6142.079, 1889.527, 0.031), Angle(0, 94.401, 0) )
		Create_EldritchArmour( Vector(6145.672, 1185.575, 0.031), Angle(0, 267.662, 0) )
		Create_OverdriveSphere( Vector(6712.606, 1536.282, 0.031), Angle(0, 357.741, 0) )
		Create_MaskOfShadows( Vector(6528.926, 903.956, 0.031), Angle(0, 35.988, 0) )
		Create_MaskOfShadows( Vector(6343.615, 2254.012, 0.031), Angle(0, 177.06, 0) )
		Create_WhiteCrystal( Vector(6350.405, 1533.956, 291.407), Angle(0, 0, 0) )
	
		CreateSecretArea( Vector(4227.187,1638.408,132.563), Vector(-18.281,-18.281,0), Vector(18.281,18.281,33.55) )
		CreateSecretArea( Vector(4595.836,1282.321,132.563), Vector(-17.335,-17.335,0), Vector(17.335,17.335,20.845) )
		CreateSecretArea( Vector(4681.962,1225.189,0.031), Vector(-44.076,-44.076,0), Vector(44.076,44.076,73.859) )
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)