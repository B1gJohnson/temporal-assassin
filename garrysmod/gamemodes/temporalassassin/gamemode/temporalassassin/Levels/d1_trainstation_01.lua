		
if CLIENT then return end

GLOB_SCENE_SKIP_AVAILABLE = true

function MAP_LOADED_FUNC()
	
	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		CreateClothesLocker( Vector(-5061.7607421875,-579.75256347656,-31.96875), Angle(0,269.79577636719,0) )

		Create_Beer( Vector(-3430.9768066406, -340.26797485352, 1.8351058959961), Angle(0, 165.61981201172, 0) )
		Create_Beer( Vector(-3402.7353515625, -340.86901855469, 1.8351020812988), Angle(0, 119.63980102539, 0) )
		Create_WhisperWeapon( Vector(-4096.03125, -652.78729248047, 366.19924926758), Angle(0, 181.0750579834, 0) )
		
		CreateSecretArea( Vector(-4096.03125,-652.66967773438,364.41262817383), Vector(-10.476146697998,-10.476146697998,0), Vector(10.476146697998,10.476146697998,3.5561218261719) )
		
		CreateProp(Vector(-4744.48828125,-2035.71875,384.29711914063), Angle(2.3919124603271,-0.81752490997314,-0.13180360198021), "models/temporalassassin/doll/unknown.mdl", true)
		CreateProp(Vector(4058.21875,-4063.3125,-489.84375), Angle(0,-90,0), "models/temporalassassin/doll/unknown.mdl", true)
		local DOLL3 = CreateProp(Vector(-830.75,2311.6875,-546.625), Angle(8.525390625,119.3994140625,1.2744140625), "models/temporalassassin/doll/unknown.mdl", true)
		DOLL3:SetCollisionGroup( COLLISION_GROUP_WEAPON )
	
	else
	
		CreateClothesLocker( Vector(-5061.7607421875,-579.75256347656,-31.96875), Angle(0,269.79577636719,0) )
		
		CreateProp(Vector(4058.21875,-4063.3125,-489.84375), Angle(0,-90,0), "models/temporalassassin/doll/unknown.mdl", true)
		
		local DOLL3 = CreateProp(Vector(-830.75,2311.6875,-546.625), Angle(8.525390625,119.3994140625,1.2744140625), "models/temporalassassin/doll/unknown.mdl", true)
		DOLL3:SetCollisionGroup( COLLISION_GROUP_WEAPON )
		
		CreateProp(Vector(-3651.219,-1163.625,45.406), Angle(1.143,-66.753,154.907), "models/temporalassassin/items/weapon_rifle.mdl", true)
		CreateProp(Vector(-3659.562,-1160.187,43.281), Angle(55.283,178.462,58.843), "models/temporalassassin/items/weapon_m6g.mdl", true)
		local GARBAGE = CreateProp(Vector(-3673.187,-1169.5,46.5), Angle(44.165,107.974,78.706), "models/props_trainstation/trashcan_indoor001b.mdl", true)
		
		Create_EldritchArmour10( Vector(-4801.308, -2628.097, 352.031), Angle(0, 99.719, 0) )
		Create_EldritchArmour10( Vector(-4823.214, -2626.359, 352.031), Angle(0, 75.057, 0) )
		Create_Armour( Vector(-4809.844, -2612.402, 353.664), Angle(0, 91.972, 0) )
		Create_RevolverWeapon( Vector(-6083.428, -2023.521, -103.969), Angle(0, 307.637, 0) )
		Create_PistolAmmo( Vector(-6094.333, -2061.376, -103.969), Angle(0, 26.013, 0) )
		Create_PistolAmmo( Vector(-6070.153, -2078.283, -103.969), Angle(0, 91.848, 0) )
		Create_Armour5( Vector(-3884.424, -1365.848, -24.969), Angle(0, 191.121, 0) )
		Create_Armour5( Vector(-3883.03, -1378.14, -24.969), Angle(0, 173.565, 0) )
		Create_RevolverAmmo( Vector(-3882.431, -1212.843, -24.969), Angle(0, 203.452, 0) )
		Create_CorzoHat( Vector(-4317.5, 254.462, -31.969), Angle(0, 1.229, 0) )
		Create_SMGWeapon( Vector(-3555.108, -1173.784, 32.031), Angle(0, 92.661, 0) )
		Create_GrenadeAmmo( Vector(-3522.668, -1151.915, 32.031), Angle(0, 115.024, 0) )
		Create_Beer( Vector(-3429.781, -340.902, 1.835), Angle(0, 80.223, 0) )
		Create_Beer( Vector(-3403.491, -340.619, 1.835), Angle(0, 114.917, 0) )
		
		local SecretButton_1 = function()
			CreateTeleporter( "Secret1", Vector(-3616.745,-697.87,32.031), "Secret2", Vector(-3193.724,79.722,-63.969) )
		end
		
		CreateProp(Vector(-3236.719,300.75,-5.469), Angle(0,-90,0), "models/props_c17/gravestone_cross001b.mdl", true)
		CreateProp(Vector(-3127.594,300.656,-5.5), Angle(0.044,-90.044,0), "models/props_c17/gravestone_cross001b.mdl", true)
		
		AddSecretButton( Vector(-3187.562,319.969,-8.871), SecretButton_1 )
		CreateSecretArea( Vector(-4812.252,-2613.367,356.563), Vector(-31.751,-31.751,0), Vector(31.751,31.751,110.155) )
		CreateSecretArea( Vector(-6079.985,-2047.828,-99.437), Vector(-73.041,-73.041,0), Vector(73.041,73.041,67.098) )
		CreateSecretArea( Vector(-4321.587,253.404,-31.969), Vector(-75.914,-75.914,0), Vector(75.914,75.914,106.074) )
		
		--[[ Safety Teleporter ]]
		
		local Create_SafetyPortals = function()
			CreateTeleporter( "Safe1", Vector(-4090.95,-206.87,-31.969), "Safe2", Vector(-4300.383,-515.742,-31.969) )
		end
		
		CreateTrigger( Vector(-4225.244,-354.982,-31.969), Vector(-54.574,-54.574,0), Vector(54.574,54.574,114.402), Create_SafetyPortals )
		
	end
	
	timer.Simple(5,function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_1.txt","DATA") then
			Create_LoreItem( Vector(-3700.169,-1104.614,32.031), Angle(0,57.852,0), "lore_mthuulra_1", "Lore File Unlocked: Mthuulra #1" )
		end
	end)
	
	for _,v in pairs (ents.FindInSphere(Vector(-3882.789,-2539.858,23.869),10)) do
		if v and IsValid(v) and v:IsProp() then
			v:Remove()
		end
	end
	
	timer.Simple(0.1,function()
		GLOB_PHONERING_PROP = CreateProp(Vector(-3874.469,-2540.531,23.031), Angle(0,-89.956,0), "models/props_c17/briefcase001a.mdl", true)
		GLOB_PHONERING_PROP:SetPhysicsWeight(5000)
		GLOB_PHONERING_PROP:SetNoDraw(true)
	end)
	
	local PHONE_TRIG = function()
	
		GLOB_PHONERING_PROP.RING_RING = CreateSound( GLOB_PHONERING_PROP, "TemporalAssassin/Secrets/Secret_PhoneHint.wav" )
		GLOB_PHONERING_PROP.RING_RING:Play()
	
		timer.Create("Phone_RingRing_D1", 3.6, 0, function()
		
			if GLOB_PHONERING_PROP.RING_RING then
				GLOB_PHONERING_PROP.RING_RING:Stop()
				GLOB_PHONERING_PROP.RING_RING = nil
			end
			
			timer.Simple(0.1,function()
				GLOB_PHONERING_PROP.RING_RING = CreateSound( GLOB_PHONERING_PROP, "TemporalAssassin/Secrets/Secret_PhoneHint.wav" )
				GLOB_PHONERING_PROP.RING_RING:Play()
			end)
			
		end)
	
	end
	
	CreateTrigger( Vector(-4038.382,-2319.202,-31.969), Vector(-164.366,-164.366,0), Vector(164.366,164.366,248.395), PHONE_TRIG )
	
	local STOP_SCENESKIP = function()
		SetSceneSkipAvailable(false)
	end
	
	local ALLOW_SCENESKIP = function()
		SetSceneSkipAvailable(true)
	end
	
	CreateTrigger( Vector(-5498.72,-1858.511,16.031), Vector(-200,-200,-200), Vector(200,200,200), STOP_SCENESKIP )
	CreateTrigger( Vector(-3501.405,-359.238,-31.969), Vector(-87.581,-87.581,0), Vector(87.581,87.581,111.038), ALLOW_SCENESKIP )
	CreateTrigger( Vector(-3465.17,-59.855,-31.969), Vector(-65.2,-65.2,0), Vector(65.2,65.2,107.938), STOP_SCENESKIP )
	
	local KILL_CITIZENS = function()
	
		for _,v in pairs (ents.FindInSphere(Vector(-4370.333,-920.652,-28.142),200)) do
			if IsValid(v) and v:IsNPC() and v:GetClass() == "npc_citizen" then
				v:Remove()
			end
		end
	
	end
	
	CreateTrigger( Vector(-4483.24,-919.069,-31.969), Vector(-35.087,-35.087,0), Vector(35.087,35.087,140.899), KILL_CITIZENS )
	
	local REMIND_LORE = function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_1.txt","DATA") then
			net.Start("DollMessage")
			net.WriteString("If you can find all 6 of my books from here on out through this time line")
			net.Broadcast()
			net.Start("DollMessage")
			net.WriteString("I'll be sure to compensate you for them kiddo")
			net.Broadcast()
			net.Start("DollMessage")
			net.WriteString("I know there is one around here somewhere past this point")
			net.Broadcast()
		end
	end
	
	CreateTrigger( Vector(-3894.839,-511.095,-31.969), Vector(-45.881,-45.881,0), Vector(45.881,45.881,154.481), REMIND_LORE )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

local CALEB_TALK = {
Sound("TemporalAssassin/Secrets/Caleb_Cameo.wav"),
Sound("TemporalAssassin/Secrets/Caleb_Cameo2.wav"),
Sound("TemporalAssassin/Secrets/Caleb_Cameo3.wav")}

function Caleb_PhoneCameo( ply, key )

	if key == IN_USE then
	
		local TR = {}
		TR.start = ply:EyePos()
		TR.endpos = TR.start + ply:EyeAngles():Forward()*30
		TR.mask = MASK_PLAYERSOLID
		TR.filter = {ply}
		TR.mins = Vector(-0.5,-0.5,-0.5)
		TR.maxs = Vector(0.5,0.5,0.5)
		local Trace = util.TraceHull(TR)
		
		local POS = Vector(-3882.789,-2539.858,23.869)
		
		if Trace.Hit and Trace.HitPos:Distance(POS) <= 30 and not PHONE_DONE then
		
			PHONE_DONE = true
			
			timer.Remove("Phone_RingRing_D1")
			
			timer.Simple(0.1,function()
				if GLOB_PHONERING_PROP and GLOB_PHONERING_PROP.RING_RING then
					GLOB_PHONERING_PROP.RING_RING:Stop()
					GLOB_PHONERING_PROP.RING_RING = nil
				end
			end)
			
			sound.Play( Sound("TemporalAssassin/Secrets/Secret_Phone.wav"), POS, 80, 100, 0.8 )
			
			timer.Simple( 0.8, function()
				sound.Play( table.Random(CALEB_TALK), POS, 80, 100, 0.8 )
			end)
		
		end
	
	end

end
hook.Add("KeyPress","Caleb_PhoneCameoHook",Caleb_PhoneCameo)