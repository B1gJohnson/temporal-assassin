
if CLIENT then return end

GLOB_REPLACE_CRUENTUS_DEMON = true

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("trigger_changelevel")
	
	Create_Backpack( Vector(1745.4429931641, 4402.0361328125, -3427.96875), Angle(0, 229.76315307617, 0) )
	Create_SpiritSphere( Vector(1951.4787597656, 4464.4306640625, -3427.96875), Angle(0, 205.34310913086, 0) )
	Create_Backpack( Vector(-267.45498657227, 4866.375, -3913.3103027344), Angle(0, 319.38513183594, 0) )
	Create_Backpack( Vector(-341.85006713867, 4877.0478515625, -3911.8208007813), Angle(0, 315.64483642578, 0) )
	
	local GAME_END_TXT = {
	"Kit found herself infront of some dude with a suitcase, weirdo",
	"kinda look to him too. After feeding him to her pet dragon she",
	"decided its time to get at LEAST a months worth of rest before",
	"doing this kinda thing again.",
	"",
	"",
	"The company needs to chill with all this alien nonsense after all.",
	"",
	"",
	"I have no idea where Kit will end up next time in Temporal Assassin, Do you?"}	
	
	MakeGameEnd( GAME_END_TXT, 35, Vector(-12447.494140625,11748.103515625,-3006.46875), Vector(-43.696384429932,-43.696384429932,0), Vector(43.696384429932,43.696384429932,93.459228515625) )
	
	local SAFETY_ON = function(self,ply)
		ply:SetSharedBool("Safety_Zone",true)	
	end
	
	CreateTrigger( Vector(-14626,15884,-6275.5), Vector(-150,-150,-100), Vector(150,150,200), SAFETY_ON )
	
	local GMAN_DIE = function()
	
		for _,v in pairs (ents.FindByClass("monster_gman")) do
		
			if v and IsValid(v) then
				v:Remove()
			end
			
		end
		
	end
	
	CreateTrigger( Vector(-12467,11747,-3005.891), Vector(-150,-150,-100), Vector(150,150,200), GMAN_DIE )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)