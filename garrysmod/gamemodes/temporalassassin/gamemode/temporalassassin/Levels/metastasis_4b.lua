if CLIENT then return end

GLOB_CONTRACTOR_TWINS = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_teleport")
	RemoveAllEnts("trigger_hurt")

	CreatePlayerSpawn(Vector(0,-315.607,-677.407), Angle(0,90,0))
	
	CreateClothesLocker( Vector(1046.432,-247.643,-583.969), Angle(0,180.672,0) )
	
	CreateProp(Vector(1014.531,2110.75,-363.812), Angle(0,-137.285,0), "models/props_junk/gnome.mdl", true)
	CreateProp(Vector(-1620.437,1459.469,282.5), Angle(-12.129,-114.17,7.383), "models/maxofs2d/camera.mdl", true) 
	CreateProp(Vector(-1601.062,1457.656,270.313), Angle(-73.564,90.923,-2.725), "models/props/cs_militia/footlocker01_open.mdl", true)
	CreateProp(Vector(2305.938,-461.187,41.219), Angle(-17.271,-53.218,-7.954), "models/gibs/helicopter_brokenpiece_06_body.mdl", true)
	CreateProp(Vector(2210.344,-240.406,19.094), Angle(-2.856,135.571,88.33), "models/gibs/helicopter_brokenpiece_04_cockpit.mdl", true)
	CreateProp(Vector(2279.438,-694.062,-14.031), Angle(-11.206,-178.989,91.23), "models/gibs/helicopter_brokenpiece_05_tailfan.mdl", true)
	CreateProp(Vector(735.563,1823.125,307.625), Angle(-0.22,178.989,0), "models/props_trainstation/tracksign03.mdl", true)
	CreateProp(Vector(737.906,-2651.25,67.625), Angle(-6.515,-179.363,-0.698), "models/combine_helicopter.mdl", true)
	
	Create_SMGWeapon( Vector(56.43, 153.904, -671.949), Angle(0, -45, 0) )
	Create_SMGAmmo( Vector(124.012, 156.917, -671.969), Angle(0, -90, 0) )
	Create_GrenadeAmmo( Vector(1132.591, -512.297, -591.998), Angle(0, 158.83, 0) )
	Create_RifleWeapon( Vector(1099.842, -616.771, -591.954), Angle(0, -90, 0) )
	Create_RifleAmmo( Vector(1112.845, -631.919, -591.969), Angle(0, -49.27, 0) )
	Create_PortableMedkit( Vector(812.751, -620.019, -591.969), Angle(0, 0, 0) )
	Create_PortableMedkit( Vector(812.031, -599.502, -591.969), Angle(0, 0, 0) )
	Create_Medkit25( Vector(808.624, -562.527, -591.969), Angle(0, 0, 0) )
	Create_Armour5( Vector(1136.148, -550.761, -591.916), Angle(0, 180, 0) )
	Create_Armour5( Vector(1157.019, -537.126, -591.546), Angle(0, -135, 0) )
	Create_Medkit25( Vector(899.434, -265.566, -583.969), Angle(0, 0, 0) )
	Create_Medkit10( Vector(899.504, -224.222, -583.969), Angle(0, 0, 0) )
	Create_Medkit10( Vector(912.495, -209.896, -583.969), Angle(0, -45, 0) )
	Create_GrenadeAmmo( Vector(957.716, 126.703, -347.999), Angle(0, 316.049, 0) )
	Create_GrenadeAmmo( Vector(983.431, 86.767, -347.991), Angle(0, 42.094, 0) )
	Create_EldritchArmour10( Vector(1099.445, 468.478, -591.988), Angle(0, 90, 0) )
	Create_EldritchArmour10( Vector(1122.168, 468.39, -591.994), Angle(0, 90, 0) )
	Create_PistolWeapon( Vector(917.606, 92.519, -347.969), Angle(0, 134.724, 0) )
	Create_PistolAmmo( Vector(911.876, 99.553, -347.988), Angle(0, 123.364, 0) )
	Create_Armour( Vector(600.068, 622.839, -227.969), Angle(0, 274.664, 0) )
	Create_Armour( Vector(668.52, 629.043, -227.969), Angle(0, 266.137, 0) )
	Create_Medkit25( Vector(631.982, 643.474, -227.969), Angle(0, 270.705, 0) )
	Create_Armour5( Vector(448.21, 16.704, -229.969), Angle(0, 273.824, 0) )
	Create_Armour5( Vector(448.892, 114.075, -233.936), Angle(0, 269.152, 0) )
	Create_RevolverWeapon( Vector(367.659, 144.079, -343.969), Angle(0, 0, 0) )
	Create_RevolverAmmo( Vector(382.772, 149.968, -343.969), Angle(0, 268.231, 0) )
	Create_RevolverAmmo( Vector(380.95, 132.59, -343.969), Angle(0, 249.825, 0) )
	Create_ShotgunWeapon( Vector(298.984, 20.889, -343.969), Angle(0, 135, 0) )
	Create_ShotgunAmmo( Vector(277.371, 25.397, -343.993), Angle(0, 135, 0) )
	Create_ShotgunAmmo( Vector(288.204, 33.765, -343.987), Angle(0, 90, 0) )
	Create_GrenadeAmmo( Vector(260.15, 75.201, -343.969), Angle(0, 358.761, 0) )
	Create_Armour100( Vector(299.08, 147.675, -343.969), Angle(0, -90, 0) )
	Create_Backpack( Vector(398.98, -165.491, -227.969), Angle(0, 0.214, 0) )
	Create_Armour( Vector(401.289, -195.454, -227.969), Angle(0, 0, 0) )
	Create_Armour( Vector(402.477, -141.734, -227.969), Angle(0, 0, 0) )
	Create_KingSlayerWeapon( Vector(173.251, 1344.369, -343.964), Angle(0, -90, 0) )
	Create_KingSlayerAmmo( Vector(140.031, 1309.442, -343.969), Angle(0, 0, 0) )
	Create_KingSlayerAmmo( Vector(189.125, 1325.921, -343.983), Angle(0, -90, 0) )
	Create_Medkit10( Vector(315.123, 1280.84, -343.969), Angle(0, 179.87, 0) )
	Create_Medkit10( Vector(315.025, 1237.674, -343.969), Angle(0, 179.87, 0) )
	Create_ManaCrystal( Vector(448.437, 1640.141, -351.969), Angle(0, 104.713, 0) )
	Create_ManaCrystal( Vector(448.244, 1264.154, -351.969), Angle(0, 69.285, 0) )
	Create_ManaCrystal( Vector(896.28, 1264.179, -351.969), Angle(0, 165.505, 0) )
	Create_ManaCrystal( Vector(959.858, 1639.714, -351.969), Angle(0, 130.111, 0) )
	Create_Beer( Vector(376.732, 1775.282, -343.969), Angle(0, 0, 0) )
	Create_Beer( Vector(375.223, 1815.11, -343.969), Angle(0, 0, 0) )
	Create_Beer( Vector(374.836, 1794.973, -343.969), Angle(0, 0, 0) )
	Create_Bread( Vector(470.581, 1848.605, -343.969), Angle(0, -135, 0) )
	Create_Bread( Vector(511.205, 1828.091, -343.969), Angle(0, -90, 0) )
	Create_RifleWeapon( Vector(677.894, 1594.142, -343.469), Angle(0, 2.485, 0) )
	Create_RifleAmmo( Vector(634.811, 1580.031, -351.826), Angle(0, 135, 0) )
	Create_RifleAmmo( Vector(643.31, 1601.172, -351.969), Angle(0, 90, 0) )
	Create_SMGWeapon( Vector(900.577, 1471.003, -343.469), Angle(0, 135, 0) )
	Create_SMGAmmo( Vector(867.057, 1433.244, -343.969), Angle(0, 0, 0) )
	Create_SMGAmmo( Vector(868.116, 1407.03, -343.969), Angle(0, 0, 0) )
	Create_FoolsOathWeapon( Vector(623.731, 1195.5, -227.984), Angle(0, 90, 0) )
	Create_FoolsOathAmmo( Vector(645.223, 1208.874, -227.969), Angle(0, 0, 0) )
	Create_FoolsOathAmmo( Vector(622.433, 1209.224, -227.905), Angle(0, 0, 0) )
	Create_Armour( Vector(419.419, 998.49, -227.969), Angle(0, 90, 0) )
	Create_Armour5( Vector(354.174, 1021.001, -227.969), Angle(0, 142.174, 0) )
	Create_Armour5( Vector(308.589, 1080.421, -223.969), Angle(0, 123.673, 0) )
	Create_Armour5( Vector(260.93, 1130.833, -223.969), Angle(0, 112.634, 0) )
	Create_Beer( Vector(859.288, 991.778, -103.969), Angle(0, 180.098, 0) )
	Create_Beer( Vector(861.297, 1076.569, -103.969), Angle(0, -180, 0) )
	Create_Beer( Vector(604.324, 1076.731, 16.031), Angle(0, 0.151, 0) )
	Create_Beer( Vector(601.703, 990.963, 16.031), Angle(0, 0.584, 0) )
	Create_Beer( Vector(857.658, 992.124, 136.031), Angle(0, 179.072, 0) )
	Create_Beer( Vector(858.831, 1080.215, 136.031), Angle(0, 181.336, 0) )
	Create_Beer( Vector(602.81, 1076.466, 256.031), Angle(0, 358.362, 0) )
	Create_Beer( Vector(599.516, 993.012, 256.031), Angle(0, 2.062, 0) )
	Create_SuperShotgunWeapon( Vector(1101.723, 1076.481, 256.031), Angle(0, -135, 0) )
	Create_ShotgunAmmo( Vector(1082.143, 1059.152, 256.008), Angle(0, 135, 0) )
	Create_ShotgunAmmo( Vector(1087.597, 1017.265, 256.031), Angle(0, -135, 0) )
	Create_Armour100( Vector(707.969, 2247.703, 272.031), Angle(0, 180, 0) )
	Create_Medkit25( Vector(706.656, 2194.133, 272.031), Angle(0, 180, 0) )
	Create_CrossbowWeapon( Vector(446.663, 2297.142, 272.031), Angle(0, -45, 0) )
	Create_CrossbowAmmo( Vector(418.961, 2269.782, 272.031), Angle(0, -135, 0) )
	Create_CrossbowAmmo( Vector(434.919, 2243.348, 272.031), Angle(0, -45, 0) )
	Create_Medkit25( Vector(371.127, 1823.969, 384.027), Angle(0, -90, 0) )
	Create_Medkit25( Vector(313.315, 1823.969, 384.839), Angle(0, -90, 0) )
	Create_ShotgunAmmo( Vector(389.994, 1699.737, 384.031), Angle(0, -180, 0) )
	Create_ShotgunAmmo( Vector(389.993, 1673.503, 384.031), Angle(0, 180, 0) )
	Create_ShotgunWeapon( Vector(389.969, 1685.117, 410.111), Angle(-2.806, -96.693, 67.627) )
	Create_SMGWeapon( Vector(321.989, 1586.665, 384.155), Angle(0, 135, 0) )
	Create_SMGAmmo( Vector(309.948, 1607.499, 384.031), Angle(0, 180, 0) )
	Create_SMGAmmo( Vector(296.266, 1592.08, 384.031), Angle(0, -45, 0) )
	Create_RifleWeapon( Vector(64.758, 2024.139, 228.01), Angle(0, 135, 0) )
	Create_RifleAmmo( Vector(85.707, 2095.116, 228.031), Angle(0, -135, 0) )
	Create_RifleAmmo( Vector(60.911, 2086.166, 228.031), Angle(0, -90, 0) )
	Create_Medkit10( Vector(85.954, 2202.811, 228.031), Angle(0, -90, 0) )
	Create_Medkit10( Vector(109.402, 2186.649, 228.031), Angle(0, -135, 0) )
	Create_PortableMedkit( Vector(-29.333, 2177.402, 228.031), Angle(0, -45, 0) )
	Create_PortableMedkit( Vector(-12.631, 2203.862, 228.031), Angle(0, -45, 0) )
	Create_ShotgunAmmo( Vector(-31.268, 3048.759, 223.602), Angle(0, 0, 0) )
	Create_ShotgunAmmo( Vector(-31.247, 3079.063, 223.602), Angle(0, 359.961, 0) )
	Create_ShotgunAmmo( Vector(-31.23, 3105.578, 223.602), Angle(0, 359.961, 0) )
	Create_SMGAmmo( Vector(-30.042, 3179.167, 223.602), Angle(0, 0, 0) )
	Create_SMGAmmo( Vector(-30.201, 3207.619, 223.602), Angle(0, 0.322, 0) )
	Create_SMGAmmo( Vector(-30.361, 3236.107, 223.602), Angle(0, 0, 0) )
	Create_PistolAmmo( Vector(-31.63, 2601.448, 223.602), Angle(0, 45, 0) )
	Create_PistolAmmo( Vector(-31.992, 2630.028, 224.02), Angle(0, 44.39, 0) )
	Create_PistolAmmo( Vector(-31.088, 2657.329, 223.81), Angle(0, 44.894, 0) )
	Create_RifleAmmo( Vector(-31.428, 2733.017, 223.636), Angle(0, 0, 0) )
	Create_RifleAmmo( Vector(-30.621, 2759.521, 223.602), Angle(0, 0, 0) )
	Create_RifleAmmo( Vector(-30.621, 2787.616, 223.602), Angle(0, 0, 0) )
	Create_Medkit25( Vector(-53.648, 3208.263, 208.031), Angle(0, 179.79, 0) )
	Create_Medkit25( Vector(-54.122, 3078.769, 208.031), Angle(0, 179.79, 0) )
	Create_Medkit25( Vector(-55.293, 2758.917, 208.031), Angle(0, 179.79, 0) )
	Create_Medkit25( Vector(-55.759, 2631.918, 208.031), Angle(0, 179.79, 0) )
	Create_OverdriveSphere( Vector(-42.599, 2913.993, 208.031), Angle(0, 357.908, 0) )
	Create_WhisperWeapon( Vector(559.742, 2146.96, 592.031), Angle(0, 89.384, 0) )
	Create_WhisperAmmo( Vector(488.899, 2147.733, 592.031), Angle(0, -180, 0) )
	Create_WhisperAmmo( Vector(632.467, 2148.693, 592.031), Angle(0, 0, 0) )
	Create_Armour200( Vector(156.029, 1437.675, 239.005), Angle(0, 90, 0) )
	Create_Armour( Vector(312.8, 1188.762, -343.969), Angle(0, 147.169, 0) )
	Create_Armour5( Vector(315.808, 1259.305, -343.969), Angle(0, 178.116, 0) )
	Create_ManaCrystal( Vector(-482.39, 481.38, 408.031), Angle(0, 152.408, 0) )
	Create_ManaCrystal( Vector(-214.917, 186.782, 408.031), Angle(0, 120.353, 0) )
	Create_ManaCrystal( Vector(-287.649, -197.256, 408.031), Angle(0, 77.977, 0) )
	Create_ManaCrystal( Vector(-580.754, -357.812, 409.734), Angle(0, 24.97, 0) )
	Create_ManaCrystal( Vector(-938.28, -271.32, 408.031), Angle(0, 336.231, 0) )
	Create_ManaCrystal( Vector(-1094.262, 47.541, 408.031), Angle(0, 291.08, 0) )
	Create_ManaCrystal( Vector(-907.274, 396.57, 408.031), Angle(0, 224.95, 0) )
	Create_SMGWeapon( Vector(-1736.269, 1517.474, 236.571), Angle(0, -180, 0) )
	Create_SMGAmmo( Vector(-1758.234, 1544.105, 236.835), Angle(0, -135, 0) )
	Create_SMGAmmo( Vector(-1754.969, 1529.932, 239.393), Angle(0, 42.654, 0) )
	Create_Medkit25( Vector(-1786.824, 1566.537, 236.44), Angle(0, -135, 0) )
	Create_Armour100( Vector(-2071.141, 1546.575, 261.207), Angle(0, -45, 0) )
	Create_PortableMedkit( Vector(-2049.756, 1562.018, 255.658), Angle(0, 299.873, 0) )
	Create_PortableMedkit( Vector(-2032.358, 1578.129, 250.27), Angle(0, 329.72, 0) )
	Create_AnthrakiaWeapon( Vector(-3167.501, 1805.131, 268.262), Angle(0, 64.112, 0) )
	Create_HarbingerAmmo( Vector(-3044.868, 1677.58, 269.3), Angle(0, 129.558, 0) )
	Create_HarbingerAmmo( Vector(-3106.265, 1610.733, 276.695), Angle(0, 145.756, 0) )
	Create_ReikoriKey( Vector(-3213.342, 1631.77, 257.123), Angle(0, 13.588, 0) )
	Create_ManaCrystal( Vector(-3687.503, -420.979, 232.031), Angle(0, 10.343, 0) )
	Create_ManaCrystal( Vector(-3625.119, -327.014, 232.031), Angle(0, 15.784, 0) )
	Create_ManaCrystal( Vector(-3727.025, -284.421, 232.031), Angle(0, 14.208, 0) )
	Create_ManaCrystal( Vector(-3780.236, -374.203, 232.031), Angle(0, 78.101, 0) )
	Create_ManaCrystal100( Vector(-3713.342, -356.117, 362.58), Angle(0, 78.825, 0) )
	Create_RifleWeapon( Vector(-2115.16, -1919.834, 173.463), Angle(0, -135, 45) )
	Create_RifleAmmo( Vector(-2083.637, -1897.218, 148.031), Angle(0, 90, 0) )
	Create_RifleAmmo( Vector(-2109.842, -1896.331, 148.005), Angle(0, 135, 0) )
	Create_Medkit10( Vector(-2215.078, -1691.209, 148.031), Angle(0, 137.483, 0) )
	Create_Medkit10( Vector(-2133.872, -1768.611, 148.031), Angle(0, 314.571, 0) )
	Create_Medkit10( Vector(-2229.857, -1775.287, 148.031), Angle(0, 225.89, 0) )
	Create_Medkit10( Vector(-2127.836, -1681.27, 148.031), Angle(0, 45.389, 0) )
	Create_Armour100( Vector(-2288.304, -1897.19, 148.031), Angle(0, 45, 0) )
	Create_Armour100( Vector(-2347.633, -1836.653, 148.031), Angle(0, 45, 0) )
	Create_Armour( Vector(-2321.116, -1875.206, 148.031), Angle(0, 45, 0) )
	Create_Armour5( Vector(-2260.016, -1916.138, 148.031), Angle(0, 45, 0) )
	Create_Armour5( Vector(-2362.874, -1813.903, 148.031), Angle(0, 45, 0) )

	local AL_B1 = function()
		Create_MegaSphere( Vector(1208.365, 1611.535, -343.969), Angle(0, 140.694, 0) )
		Create_CorzoHat( Vector(1131.249, 1637.86, -343.969), Angle(0, 176.049, 0) )
	end

	AddSecretButton(Vector(543.969,1445.737,-288.751), AL_B1)

	local TWINS_HINT = function(self,ply)
	
		net.Start("TwinsMessage")
		net.WriteString("Pipes blocking your way, huh..? Perhaps overloading them is the idea.")
		net.Broadcast()
	end

	local TWINS_HINT2 = function(self,ply)
	
		net.Start("TwinsMessage")
		net.WriteString("More pipes... Look around. There must be more valves to use.")
		net.Broadcast()
	
	end

	local TWINS_HINT3 = function(self,ply)
	
		net.Start("TwinsMessage")
		net.WriteString("Guess hell ran out of space. Don't slow down, Wilde. Leave none standing.")
		net.Broadcast()
		
	end

	CreateTrigger( Vector(1167.936,235.249,-594.562), Vector(-49.805,-49.805,0), Vector(49.805,49.805,104.597), TWINS_HINT )
	CreateTrigger( Vector(451.819,857.929,-256.177), Vector(-52.423,-52.423,0), Vector(52.423,52.423,98.117), TWINS_HINT2 )
	CreateTrigger( Vector(1022.772,1219.583,256.031), Vector(-73.368,-73.368,0), Vector(73.368,73.368,130.004), TWINS_HINT3 )

	CreateSecretArea( Vector(1110.729,469.425,-587.437), Vector(-21.634,-21.634,0), Vector(21.634,21.634,21.415) )
	CreateSecretArea( Vector(561.424,2146.889,595.669), Vector(-72.962,-72.962,0), Vector(72.962,72.962,87.906) )
	CreateSecretArea( Vector(154.646,1442.897,243.531), Vector(-28.388,-28.388,0), Vector(28.388,28.388,120.352) )
	CreateSecretArea( Vector(-3131.715,1692.669,255.324), Vector(-177.288,-177.288,0), Vector(177.288,177.288,220.492) )

	local GAME_END_TEXT = {
		
		"As kit reached the final part of the beach, an eerie silence befell the island.",
		"Kit paid no mind to this, merely approaching the helicopter ahead of her.",
		"",
		"Waiting for her at said helicopter was one half of the Twins,",
		"whom had been communicating with her for most of the mission.",
		"",
		"'Well done, Wilde. You shouldn't linger for long, we're erasing this place completely.'",
		"Kit merely nodded, returning the pistol she had been issued for the mission to her contractor.",
		"",
		"After a not-long-at-all trip, kit was back home in her apartment, crashing on the couch,",
		"beer in hand and ready to relax, awaiting the next contract, from whoever and whenever.",
		"",
		"'ありがと, Temporal Assassin, and until next time...'",}

	MakeGameEnd( GAME_END_TEXT, 32, Vector(133.031,-2197.791,85.075), Vector(-217.687,-217.687,0), Vector(217.687,217.687,474.123) )

	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)