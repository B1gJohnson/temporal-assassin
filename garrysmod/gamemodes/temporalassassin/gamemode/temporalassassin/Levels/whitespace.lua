
if CLIENT then return end

PrecacheParticleSystem("WhiteSpace_RG")
PrecacheParticleSystem("WhiteSpace_RGTurned")
PrecacheParticleSystem("Training_StarFragment")

util.AddNetworkString("WhiteSpace_Loaded")

function WhiteSpace_Loaded_LIB( len, pl )

	if pl and pl == player.GetAll()[1] then
		MAP_LOADED_FUNC(pl)
	end

end
net.Receive("WhiteSpace_Loaded",WhiteSpace_Loaded_LIB)

function WhiteSpace_Death( ply )

	if GLOB_RG and IsValid(GLOB_RG) then
		GLOB_RG:Remove()
		GLOB_RG = false
	end

end
hook.Add("PlayerDeath","WhiteSpace_DeathHook",WhiteSpace_Death)

function MAP_LOADED_FUNC(pl)
	
	GLOB_RG = CreateParticleEffect( "WhiteSpace_RG", Vector(4110.975,-8000,-1058.685), Angle(0,90,0) )
	
	if pl:HasLoreFile("WHITESPACE_LEVELLOAD") and not pl:HasLoreFile("WHITESPACE_GOTGUN") then

		local FUNC = function( self, ply )

			if IsValid(ply) then

				if not ply:Alive() then return end
				
				local TXT = "Looks like some kind of Shotgun, I should get out of here..."
				local SND = Sound("TemporalAssassin/Weapons/Autoshotgun/GetWeapon.wav")	
				
				net.Start("TEMPORAL_AddLog")
				net.WriteString(TXT)
				net.Send(ply)
				ply:Give("temporal_autoshotgun")
				ply:GiveAmmo2( 64, "autoshotgun_ammo" )
				RunConsoleCommand("TA_DEV_Forced_AutoShotgun",1)
				ply:Unlock_TA_LoreFile( "weapon_autoshotgun_1", "" )
				ply:SetSharedFloat("Item_ScreenFlash",CurTime() + 0.15)
				ply:EmitSound( SND, 100, math.random(98,102), 1, CHAN_BODY )
				self:Remove()
				
				ply:EmitSound("TemporalAssassin/Maps/WhiteSpace_Noticed.wav",100,100,1,CHAN_STATIC)
				
				timer.Simple(0.2,function()
					GLOB_RG:Remove()
				end)
				
				GLOB_RG2 = CreateParticleEffect( "WhiteSpace_RGTurned", Vector(4110.975,-8000,-1058.685), Angle(0,90,0) )
			
				ply:SetAchievementProgress( "WHITE_SPACE", 1 )
				net.Start("Achievement_ForceSave")
				net.Send(ply)
					
				ply:Unlock_TA_LoreFile( "WHITESPACE_GOTGUN", "" )
				ply:SetSharedBool("RGStare",true)
				
				timer.Simple(5,function()
				
					if file.Exists("temporalassassin/lore/whitespace_slept.txt","DATA") then
					
						timer.Simple(2.5,function()
							RunConsoleCommand("changelevel","ta_hub")
						end)
						
					else
				
						local POS1 = ply:GetPos() + Vector(-500,-500,-500)
						local POS2 = ply:GetPos() + Vector(500,500,500)
						local PL = player.GetAll()[1]
						local END_P = ply:GetPos()
						local END_P1 = END_P + Vector(-500,-500,-500)
						local END_P2 = END_P + Vector(500,500,500)
						LEVEL_SWAP = {END_P1,END_P2}
						
						if file.Exists("temporalassassin/lore/whitespace_oldmap.txt","DATA") then
							LEVEL_TOSWAP = file.Read("temporalassassin/lore/whitespace_oldmap.txt","DATA")
						else
							LEVEL_TOSWAP = "ta_hub"
						end
						
						RemoveAllEnts("temporal_changelevel")
						CreateChangeLevel( END_P, Vector(-500,-500,-500), Vector(500,500,500), LEVEL_TOSWAP )
					
					end
					
				end)
				
			end
		
		end
		
		local WEAPON = CreateItemPickup( Vector(3947.864,-1791.46,-2047.969), Angle(0, 90, 0), "models/temporalassassin/items/weapon_autoshotgun.mdl", FUNC )
		WEAPON.Custom_Override = true
		
		CreateParticleEffect( "Training_StarFragment", Vector(4159.036,10790.203,-2047.969+50), Angle(0,0,0), 999999 )
		
	end

end