		
if CLIENT then return end

util.AddNetworkString("SIN_UpdateCount")
util.AddNetworkString("SIN_AskForGoal")
util.AddNetworkString("SIN_SinOnMap")

function SIN_AskForGoal_LIB( len, pl )

	net.Start("SIN_UpdateCount")
	net.WriteUInt(LAST_SIN_COUNT,6)
	net.WriteUInt(SIN_COUNT_GOAL,6)
	net.Broadcast()

end
net.Receive("SIN_AskForGoal",SIN_AskForGoal_LIB)

GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true

GLOB_NO_FENIC = false
GLOB_NO_SINS = true

GLOB_SINHUNTER_SPAWN1 = false
GLOB_SINHUNTER_SPAWN2 = false
GLOB_SINHUNTER_SPAWNNPCS = 0
GLOB_SINHUNTER_ENEMYCOUNT = 0
GLOB_SINFUL_ENEMY_COUNT = 0

SIN_SPAWN_SPEED = 5
SIN_SPAWN_MAX = 40
SIN_ENEMIES_KILLED = 0
SIN_SINSPAWN_THRESHOLD = 20

local SPAWN_DIFFICULTIES = {}
SPAWN_DIFFICULTIES[1] = {5,30,20}
SPAWN_DIFFICULTIES[2] = {4,40,25}
SPAWN_DIFFICULTIES[3] = {3,50,30}
SPAWN_DIFFICULTIES[4] = {3,50,30}
SPAWN_DIFFICULTIES[5] = {2.5,60,35}
SPAWN_DIFFICULTIES[6] = {2,60,35}
SPAWN_DIFFICULTIES[7] = {1.5,60,35}
SPAWN_DIFFICULTIES[8] = {1.5,70,40}
SPAWN_DIFFICULTIES[9] = {1,80,45}
SPAWN_DIFFICULTIES[10] = {1,100,50}
SPAWN_DIFFICULTIES[11] = {1,120,50}
SPAWN_DIFFICULTIES[12] = {1,120,50} --[[ Just incase more difficulties ARE added ]]
SPAWN_DIFFICULTIES[13] = {1,120,50}
SPAWN_DIFFICULTIES[14] = {1,120,50}

GLOB_SINTYPE = math.random(1,5)
--[[
This defines what enemies will spawn for this sin hunter mission.

1 = Combine
2 = Zombies
3 = Antlions
4 = HL:S Humans
5 = HL:S Aliens

You can also define it as GLOB_SINTYPE = math.random(1,5) to make it random everytime its played
]]

local SIN_CLASSES = {}

SIN_CLASSES[1] = {
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s"}

SIN_CLASSES[2] = {
"npc_zombie",
"npc_zombie",
"npc_zombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_poisonzombie"}

SIN_CLASSES[3] = {
"npc_antlion"}

if IsMounted("ep2") then
	SIN_CLASSES[3] = {
	"npc_antlion",
	"npc_antlion",
	"npc_antlion",
	"npc_antlion_worker"}
end

SIN_CLASSES[4] = SIN_CLASSES[1]
SIN_CLASSES[5] = SIN_CLASSES[2]

if IsMounted("hl1") then

	SIN_CLASSES[4] = {
	"monster_human_grunt"}
	
	SIN_CLASSES[5] = {
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullsquid",
	"monster_houndeye",
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullchicken",
	"monster_houndeye"}
	
end

local BIGSIN_CLASSES = {}

BIGSIN_CLASSES[1] = {
"npc_combine_s"}

if IsMounted("ep2") then
	BIGSIN_CLASSES[1] = {
	"npc_hunter"}
end

BIGSIN_CLASSES[2] = {
"npc_poisonzombie"}

if IsMounted("ep2") or IsMounted("episodic") then
	BIGSIN_CLASSES[2] = {
	"npc_zombine"}
end

BIGSIN_CLASSES[3] = {
"npc_antlionguard"}
BIGSIN_CLASSES[4] = BIGSIN_CLASSES[1]
BIGSIN_CLASSES[5] = BIGSIN_CLASSES[2]

if IsMounted("hl1") then
	BIGSIN_CLASSES[4] = {
	"monster_human_assassin"}
	
	BIGSIN_CLASSES[5] = {
	"monster_alien_slave"}
end

local SIN_WEAPONS = {}
SIN_WEAPONS["npc_metropolice"] = {
"weapon_pistol",
"weapon_smg1",
"weapon_stunstick"}
SIN_WEAPONS["npc_combine_s"] = {
"weapon_smg1",
"weapon_ar2",
"weapon_shotgun"}

function SetAsRespawningItem( ent, del )

	if not ent then return end
	if not ent.ITEM_USE then return end
	
	local POS,ANG,MDL,USE = ent:GetPos(), ent:GetAngles(), ent:GetModel(), ent.ITEM_USE
	
	if not del then del = 30 end
	
	ent.OnRemove = function()
	
		timer.Simple(del,function()
		
			local N_ITEM = CreateItemPickup( POS, ANG, MDL, USE )
			CreatePointParticle( "Doll_EvilSpawn_Activate", POS, POS, Angle(0,0,0), Angle(0,0,0), 1 )
			sound.Play( Sound("TemporalAssassin/Doll/ExtraSpawn_Item.wav"), POS, 70, math.random(99,101), 0.7 )
			SetAsRespawningItem(N_ITEM,del)
			
		end)
	
	end

end

LAST_SIN_COUNT = 0
SIN_COUNT_GOAL = 5

function CanWeSpawnHere(pos)

	local TR = {}
	TR.start = pos
	TR.endpos = pos
	TR.mask = MASK_NPCSOLID
	TR.filter = {}
	table.Add(TR.filter,GLOB_TRIGGER_ENTS)
	TR.mins = Vector(-16,-16,0)
	TR.maxs = Vector(16,16,72)
	local Trace = util.TraceHull(TR)
	
	if Trace.Hit then return false end
	
	return true

end

function EnemySpawns_Think()

	local CT = CurTime()
	
	if SIN_ENEMIES_KILLED and SIN_ENEMIES_KILLED > SIN_SINSPAWN_THRESHOLD then
		SIN_ENEMIES_KILLED = 0
		Create_SinfulBoi()
	end
	
	net.Start("SIN_SinOnMap")
	if GLOB_SINFUL_ENEMY_COUNT and GLOB_SINFUL_ENEMY_COUNT > 0 then
		net.WriteBool(true)
	else
		net.WriteBool(false)
	end
	net.Broadcast()
	
	if GLOB_SIN_DEVOURED_COUNT and GLOB_SIN_DEVOURED_COUNT > LAST_SIN_COUNT then
	
		LAST_SIN_COUNT = GLOB_SIN_DEVOURED_COUNT
		
		local SIN_LEFT = (SIN_COUNT_GOAL - LAST_SIN_COUNT)
		
		net.Start("SIN_UpdateCount")
		net.WriteUInt(LAST_SIN_COUNT,6)
		net.WriteUInt(SIN_COUNT_GOAL,6)
		net.Broadcast()
		
		if SIN_LEFT <= 0 and not SIN_GAME_ENDED then
			SIN_GAME_ENDED = true
			END_SIN_HUNTER()			
		end
		
	end
	
	if GLOB_SINHUNTER_SPAWNNPCS and CT >= GLOB_SINHUNTER_SPAWNNPCS and GLOB_SINHUNTER_STARTED then
	
		GLOB_SINHUNTER_SPAWNNPCS = CT + SIN_SPAWN_SPEED
	
		local SPAWN_POS
	
		if GLOB_SINHUNTER_SPAWN2 then
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_FRONT) + Vector(0,0,5)
		else
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_BACK) + Vector(0,0,5)
		end
		
		if SPAWN_POS and GLOB_SINHUNTER_ENEMYCOUNT < SIN_SPAWN_MAX and GLOB_SINTYPE and SIN_CLASSES[GLOB_SINTYPE] then
		
			local CLASS = table.Random(SIN_CLASSES[GLOB_SINTYPE])
			local WEP = false
			
			if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
				WEP = table.Random(SIN_WEAPONS[CLASS])
			end
			
			if CanWeSpawnHere(SPAWN_POS) then
				CreateSnapNPC( CLASS, SPAWN_POS, Angle(0,math.random(-360,360),0), WEP, false )
				GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT + 1
			end
		
		end
	
	end

end
timer.Create("EnemySpawns_ThinkTimer", 0.5, 0, EnemySpawns_Think)

function Set_EnemySpawns_1()

	print("Back of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = true
	GLOB_SINHUNTER_SPAWN2 = false

end

function Set_EnemySpawns_2()

	print("Front of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = true

end

function START_SIN_HUNTER()

	RemoveAllTeleporters()
	
	local TSC1, TSC2 = CreateTeleporter( "SEC_1", Vector(-3920.956,490.086,-138.079), "SEC_2", Vector(-1532.809,1102.574,512.031) )
	
	local RemTeleSecret1 = function()
	
		local RemTeleSecret2 = function()
			if TSC1 and TSC2 then
				TSC1:Remove()
				TSC2:Remove()
			end
		end
		
		CreateTrigger( Vector(-1560.514,1115.157,512.031), Vector(-83.19,-83.19,0), Vector(83.19,83.19,130.219), RemTeleSecret2 )
		
	end
	
	CreateTrigger( Vector(-3920.743,516.934,-143.651), Vector(-103.949,-103.949,0), Vector(103.949,103.949,95.298), RemTeleSecret1 )
	
	--[[ This is the trigger that enables Back side map spawns ]]	
	CreateTrigger( Vector(698.552,-254.3,16.031), Vector(-2960.374,-2960.374,0), Vector(2960.374,2960.374,3129.462), Set_EnemySpawns_1, true )
	
	--[[ This is the trigger that enables Front side map spawns ]]
	CreateTrigger( Vector(-4912.031,-648.9,-412.22), Vector(-2577.093,-2577.093,0), Vector(2577.093,2577.093,3610.266), Set_EnemySpawns_2, true )
	
	GLOB_SINHUNTER_STARTED = true
	GLOB_SINHUNTER_SPAWNNPCS = CurTime() + 20
	
	local DIFF = GetDifficulty2()

	if SPAWN_DIFFICULTIES and SPAWN_DIFFICULTIES[DIFF] then
		SIN_SPAWN_SPEED = SPAWN_DIFFICULTIES[DIFF][1]
		SIN_SPAWN_MAX = SPAWN_DIFFICULTIES[DIFF][2]
		SIN_SINSPAWN_THRESHOLD = SPAWN_DIFFICULTIES[DIFF][3]
	end
	
end

function END_SIN_HUNTER()

	GLOB_SINHUNTER_STARTED = false

	for _,v in pairs (ents.GetAll()) do
	
		if v and IsValid(v) then
		
			if v:IsNPC() then
				v:Remove() --[[ Lets remove all the npcs ]]
			end
		
		end
	
	end
	
	GLOB_SINFUL_ENEMY_COUNT = 0
	net.Start("SIN_SinOnMap")
	net.WriteBool(false)
	net.Broadcast()
	
	CreateTeleporter( "END_1", Vector(-1187.882,-564.114,512.031), "END_2", Vector(-192.084,-450.087,490.895) )
	
	local GAME_END_TEXT = {
	""}

	MakeGameEnd( GAME_END_TEXT, 3, Vector(-1222.945,-575.931,512.031), Vector(-84.881,-84.881,0), Vector(84.881,84.881,116.362) )
	
end

function Create_SinfulBoi()

	local SPAWNS_BACK = {
	Vector(-3537.462,-989.659,-117.658),
	Vector(-4041.373,-296.914,-119.195)}
	
	local SPAWNS_FRONT = {
	Vector(137.044,-147.648,200.558),
	Vector(168.138,-611.072,199.738)}
	
	local SPAWN_POINT
	
	if GLOB_SINHUNTER_SPAWN2 then
		SPAWN_POINT = table.Random(SPAWNS_FRONT)
	elseif GLOB_SINHUNTER_SPAWN1 then
		SPAWN_POINT = table.Random(SPAWNS_BACK)
	end
	
	if not SPAWN_POINT then return end
	if not GLOB_SINTYPE then return end
	if not BIGSIN_CLASSES then return end
	if not BIGSIN_CLASSES[GLOB_SINTYPE] then return end
	
	local CLASS = table.Random(BIGSIN_CLASSES[GLOB_SINTYPE])
	local WEP = false
			
	if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
		WEP = table.Random(SIN_WEAPONS[CLASS])
	end
	
	GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT + 1
	
	local SIN_BOI = CreateSnapNPC( CLASS, SPAWN_POINT, Angle(0,math.random(-360,360),0), WEP, false )
			
	timer.Simple(0.1,function()
		if SIN_BOI and IsValid(SIN_BOI) then
			SIN_BOI:SetHealth( SIN_BOI:Health()*2 )
			SIN_BOI:CreateSinner(true)
			SIN_BOI:SetModelScale(1.2,0) --[[ Lets make sin bois a bit bigger in scale ]]
			if GLOB_SINTYPE == 1 then
				SIN_BOI:MakeCombineShielder()
			end
		end
	end)

end

function SinHunter_NPCDeath( npc, att, inf )

	if not npc.SinHealth then
		SIN_ENEMIES_KILLED = SIN_ENEMIES_KILLED + 1
		GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT - 1
	elseif npc.SinHealth then
		GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT - 1
	end

end
hook.Add("OnNPCKilled","SinHunter_NPCDeathHook",SinHunter_NPCDeath)

function SINHUNTER_DEATH_RESET()

	if #player.GetAll() <= 1 then

		GLOB_NO_SAVEDATA = true
		GLOB_SAFE_GAMEEND = true
		
		GLOB_NO_FENIC = false
		GLOB_NO_SINS = true
		
		GLOB_SINHUNTER_SPAWN1 = false
		GLOB_SINHUNTER_SPAWN2 = false
		GLOB_SINHUNTER_SPAWNNPCS = 0
		GLOB_SINHUNTER_ENEMYCOUNT = 0
		GLOB_SINFUL_ENEMY_COUNT = 0
		
		SIN_SPAWN_SPEED = 5
		SIN_SPAWN_MAX = 40
		SIN_ENEMIES_KILLED = 0
		SIN_SINSPAWN_THRESHOLD = 20
		
		GLOB_SIN_DEVOURED_COUNT = 0
		LAST_SIN_COUNT = 0
		SIN_COUNT_GOAL = 5
		
		GLOB_SINHUNTER_STARTED = false
		GLOB_SINHUNTER_SPAWNNPCS = false
		SIN_GAME_ENDED = false
		
	end

end
hook.Add("PlayerDeath","SINHUNTER_DEATH_RESETHOOK",SINHUNTER_DEATH_RESET)

function MAP_LOADED_FUNC()

	GLOB_NO_SAVEDATA = true
	GLOB_SAFE_GAMEEND = true
	
	GLOB_NO_FENIC = false
	GLOB_NO_SINS = true
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = false
	GLOB_SINHUNTER_SPAWNNPCS = 0
	GLOB_SINHUNTER_ENEMYCOUNT = 0
	GLOB_SINFUL_ENEMY_COUNT = 0
	
	SIN_SPAWN_SPEED = 5
	SIN_SPAWN_MAX = 40
	SIN_ENEMIES_KILLED = 0
	SIN_SINSPAWN_THRESHOLD = 20
	
	LAST_SIN_COUNT = 0
	SIN_COUNT_GOAL = 5
	
	GLOB_SIN_DEVOURED_COUNT = 0
	GLOB_SINHUNTER_STARTED = false
	GLOB_SINHUNTER_SPAWNNPCS = false
	SIN_GAME_ENDED = false

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	RemoveAllEnts("ambient_generic")

	CreatePlayerSpawn( Vector(-1238.497,-577.727,512.031), Angle(0,0.90,0) )

	CreateClothesLocker( Vector(-1152.283,-755.773,512.031), Angle(0,90.714,0) )
	
	local B_PROP = CreateProp(Vector(-273.906,-452.062,516.188), Angle(0,179.956,0), "models/props_lab/blastdoor001c.mdl", true)
	B_PROP:SetMaterial("models/props_combine/tprings_globe")
	
	CreateProp(Vector(-3919.312,1168.375,-139.844), Angle(0,-91.362,0), "models/temporalassassin/doll/unknown.mdl", true)
	
	Create_PistolWeapon( Vector(-1056.236, -741.411, 512.031), Angle(0, 90.056, 0) )
	Create_RevolverWeapon( Vector(380.666, -104.041, 128.031), Angle(0, 43.313, 0) )
	Create_ShotgunWeapon( Vector(504.49, 56.616, 16.031), Angle(0, 71.313, 0) )
	Create_RifleWeapon( Vector(-1900.826, 742.877, 368.031), Angle(0, 271.239, 0) )
	Create_PistolWeapon( Vector(-1168.715, 738.26, 224.031), Angle(0, 89.884, 0) )
	Create_FrontlinerWeapon( Vector(-1267.373, 734.232, 368.031), Angle(0, 179.949, 0) )
	Create_SMGWeapon( Vector(-955.483, -955.815, 128.031), Angle(0, 151.074, 0) )
	Create_SuperShotgunWeapon( Vector(-1345.122, -1404.563, 166.639), Angle(0, 267.619, 0) )
	Create_KingSlayerWeapon( Vector(-2220.675, -1619.624, 32.031), Angle(0, 346.054, 0) )
	Create_CrossbowWeapon( Vector(-4081.658, -1411.011, -63.969), Angle(0, 176.909, 0) )
	Create_M6GWeapon( Vector(-4591.306, -189.147, -175.969), Angle(0, 358.769, 0) )
	Create_RevolverWeapon( Vector(497.374, 139.748, 368.031), Angle(0, 157.745, 0) )
	Create_RaikiriWeapon( Vector(-1449.072, 345.569, 512.031), Angle(0, 181.522, 0) )
	
	local HEALING = {
	Create_Medkit25( Vector(703.871, -991.584, 128.031), Angle(0, 179.817, 0) ),
	Create_Medkit10( Vector(365.977, -139.882, 368.031), Angle(0, 46.991, 0) ),
	Create_Medkit25( Vector(-4588.766, -764.141, -175.969), Angle(0, 2.987, 0) ),
	Create_Medkit10( Vector(-3888.569, -51.227, -303.969), Angle(0, 269.652, 0) ),
	Create_Medkit10( Vector(-4203.318, -315.923, -303.969), Angle(0, 0.622, 0) ),
	Create_Medkit25( Vector(716.792, -634.194, 128.031), Angle(0, 180.984, 0) ),
	Create_Medkit25( Vector(-1968.165, -591.592, 128.031), Angle(0, 272.938, 0) ),
	Create_Medkit10( Vector(-3168.041, -696.204, -3.48), Angle(0, 358.941, 0) ),
	Create_Medkit10( Vector(-3371.864, -961.375, -44.188), Angle(0, 89.636, 0) ),
	Create_Medkit25( Vector(-182.233, -1010.496, 16.031), Angle(0, 90.712, 0) ),
	Create_Medkit10( Vector(-4094.594, -116.73, -175.969), Angle(0, 136.027, 0) ),
	Create_Medkit10( Vector(-4074.822, -96.24, -175.969), Angle(0, 136.027, 0) ),
	Create_Medkit10( Vector(-4113.192, -135.937, -175.969), Angle(0, 129.317, 0) ),
	Create_Medkit25( Vector(-2356.044, -1475.504, 32.031), Angle(0, 269.854, 0) ),
	Create_Medkit10( Vector(-1324.051, -1343.433, 183.785), Angle(0, 108.229, 0) ),
	Create_Medkit10( Vector(-1652.21, -995.913, 128.031), Angle(0, 17.098, 0) ),
	Create_Medkit10( Vector(-1653.62, -786.034, 128.031), Angle(0, 326.719, 0) ),
	Create_Medkit10( Vector(-2412.323, -557.138, 128.031), Angle(0, 0.048, 0) ),
	Create_Medkit25( Vector(-801.487, -549.731, 128.031), Angle(0, 50.369, 0) ),
	Create_Medkit10( Vector(720.951, 239.142, 128.031), Angle(0, 177.778, 0) ),
	Create_Medkit25( Vector(-712.225, 731.476, 368.031), Angle(0, 91.564, 0) ),
	Create_Medkit25( Vector(-1349.803, 1100.284, 368.031), Angle(0, 89.529, 0) ),
	Create_Medkit25( Vector(-720.027, 1122.769, 48.031), Angle(0, 91.944, 0) ),
	Create_Medkit25( Vector(-1338.337, 1109.829, 48.031), Angle(0, 89.799, 0) ),
	Create_Medkit25( Vector(-1419.363, 748.536, 48.031), Angle(0, 87.599, 0) ),
	Create_Medkit25( Vector(-1047.646, 1101.622, 224.031), Angle(0, 269.369, 0) ),
	Create_Medkit25( Vector(-860.282, -43.58, 48.031), Angle(0, 270.359, 0) ),
	Create_Medkit25( Vector(-1299.208, -44.025, 48.031), Angle(0, 273.164, 0) ),
	Create_ManaCrystal( Vector(-3278.285, -1865.806, 0.031), Angle(0, 0.359, 0) ),
	Create_ManaCrystal( Vector(-1706.354, -910.464, 128.031), Angle(0, 0.818, 0) ),
	Create_ManaCrystal( Vector(398.376, -914.226, 184.847), Angle(0, 1.428, 0) ),
	Create_ManaCrystal( Vector(-470.363, -894.674, 256.031), Angle(0, 0.632, 0) ),
	Create_ManaCrystal( Vector(-3744.955, -985.172, -303.969), Angle(0, 52.033, 0) ),
	Create_ManaCrystal( Vector(-3273.451, -469.904, -303.969), Angle(0, 228.828, 0) ),
	Create_ManaCrystal( Vector(-3752.217, -468.519, -303.969), Angle(0, 314.353, 0) ),
	Create_ManaCrystal( Vector(-3273.878, -987.294, -303.969), Angle(0, 136.923, 0) ),
	Create_ManaCrystal( Vector(-3523.181, -737.931, -303.969), Angle(0, 0.827, 0) ),
	Create_ManaCrystal( Vector(122.475, -448.947, 384.031), Angle(0, 179.787, 0) )}
	
	local AMMO_ENTS = {
	Create_PistolAmmo( Vector(-1148.05, 725.689, 224.031), Angle(0, 90.324, 0) ),
	Create_PistolAmmo( Vector(-1185.694, 754.774, 227.063), Angle(0, 87.794, 0) ),
	Create_ShotgunAmmo( Vector(537.898, 28.755, 16.031), Angle(0, 44.748, 0) ),
	Create_ShotgunAmmo( Vector(556.145, 46.43, 16.031), Angle(0, 45.078, 0) ),
	Create_RevolverAmmo( Vector(357.56, -78.13, 128.031), Angle(0, 47.768, 0) ),
	Create_SMGAmmo( Vector(430.076, -142.435, 368.031), Angle(0, 72.401, 0) ),
	Create_SMGAmmo( Vector(352.932, -87.994, 368.031), Angle(0, 30.932, 0) ),
	Create_ShotgunAmmo( Vector(464.219, -155.312, 368.031), Angle(0, 74.271, 0) ),
	Create_ShotgunAmmo( Vector(340.922, -57.992, 368.031), Angle(0, 31.867, 0) ),
	Create_GrenadeAmmo( Vector(-9.907, -780.359, 256.031), Angle(0, 226.983, 0) ),
	Create_GrenadeAmmo( Vector(716.991, 240.504, 368.031), Angle(0, 3.713, 0) ),
	Create_GrenadeAmmo( Vector(706.665, -440.825, 16.031), Angle(0, 176.908, 0) ),
	Create_RifleAmmo( Vector(-1840.481, 746.315, 368.031), Angle(0, 271.184, 0) ),
	Create_ShotgunAmmo( Vector(-1834.081, -53.659, 368.031), Angle(0, 1.423, 0) ),
	Create_SMGAmmo( Vector(-1831.42, -39.717, 371.063), Angle(0, 1.313, 0) ),
	Create_CrossbowAmmo( Vector(-1807.26, -62.367, 368.031), Angle(0, 3.128, 0) ),
	Create_RevolverAmmo( Vector(-1809.958, -17.201, 368.031), Angle(0, 6.978, 0) ),
	Create_KingSlayerAmmo( Vector(-1805.202, -33.006, 371.063), Angle(0, 351.358, 0) ),
	Create_SMGAmmo( Vector(-952.446, -807.587, 128.031), Angle(0, 209.869, 0) ),
	Create_SMGAmmo( Vector(-931.731, -843.16, 128.031), Angle(0, 205.359, 0) ),
	Create_ShotgunAmmo( Vector(-1315.517, -1398.869, 166.852), Angle(0, 264.594, 0) ),
	Create_ShotgunAmmo( Vector(-1317.39, -1424.53, 165.893), Angle(0, 263.934, 0) ),
	Create_KingSlayerAmmo( Vector(-2226.356, -1738.863, 32.031), Angle(0, 17.624, 0) ),
	Create_GrenadeAmmo( Vector(-3867.236, -1178.388, -63.969), Angle(0, 238.839, 0) ),
	Create_CrossbowAmmo( Vector(-4073.317, -1543.652, -63.969), Angle(0, 178.009, 0) )}
	
	local AMMO_S_ENTS = {
	Create_FoolsOathAmmo( Vector(-3362.93, -1339.962, -175.969), Angle(0, 178.987, 0) ),
	Create_M6GAmmo( Vector(-4528.227, -401.009, -91.969), Angle(0, 90.398, 0) ),
	Create_M6GAmmo( Vector(-4528.71, -145.711, -91.969), Angle(0, 270.556, 0) )}
	
	for _,v in pairs (HEALING) do
		SetAsRespawningItem(v,30)
	end
	
	for _,v in pairs (AMMO_ENTS) do
		SetAsRespawningItem(v,60)
	end
	
	for _,v in pairs (AMMO_S_ENTS) do
		SetAsRespawningItem(v,120)
	end
	
	Create_Armour( Vector(-1265.578, -743.831, 512.031), Angle(0, 91.045, 0) )
	Create_PistolAmmo( Vector(-1032.758, -755.821, 512.031), Angle(0, 90.661, 0) )
	Create_PistolAmmo( Vector(-1033.53, -723.562, 512.031), Angle(0, 90.441, 0) )
	Create_Beer( Vector(-710.272, -432.195, 512.031), Angle(0, 220.901, 0) )
	Create_Beer( Vector(-762.98, -415.046, 512.031), Angle(0, 194.116, 0) )
	Create_Beer( Vector(-734.51, -413.731, 512.031), Angle(0, 191.476, 0) )
	Create_Beer( Vector(-784.215, -417.018, 512.031), Angle(0, 196.646, 0) )
	Create_Bread( Vector(-732.765, -748.884, 512.031), Angle(0, 110.441, 0) )
	Create_Armour5( Vector(-669.918, -736.401, 512.031), Angle(0, 72.505, 0) )
	Create_Armour5( Vector(-729.337, -691.056, 512.031), Angle(0, 48.975, 0) )
	Create_Armour( Vector(-667.514, -424.251, 512.031), Angle(0, 320.637, 0) )
	Create_PortableMedkit( Vector(-1248.294, -404.911, 512.031), Angle(0, 269.872, 0) )
	Create_GrenadeAmmo( Vector(-367.714, -757.346, 512.031), Angle(0, 90.407, 0) )
	Create_GrenadeAmmo( Vector(-1155.714, -396.137, 512.031), Angle(0, 359.412, 0) )
	Create_EldritchArmour10( Vector(749.193, 158.258, 607.341), Angle(0, 181.247, 0) )
	Create_Bread( Vector(702.422, -940.308, 368.031), Angle(0, 93.027, 0) )
	Create_Bread( Vector(628.251, -942.725, 368.031), Angle(0, 85.382, 0) )
	Create_Bread( Vector(558.856, -993.984, 368.031), Angle(0, 88.627, 0) )
	Create_Armour5( Vector(559.677, -939.599, 368.031), Angle(0, 88.077, 0) )
	Create_Armour5( Vector(670.48, -994.99, 368.031), Angle(0, 91.817, 0) )
	Create_Beer( Vector(723.191, -53.501, 128.031), Angle(0, 181.223, 0) )
	Create_Beer( Vector(724.112, -96.659, 128.031), Angle(0, 181.223, 0) )
	Create_Beer( Vector(725.053, -140.997, 128.031), Angle(0, 180.673, 0) )
	Create_Armour( Vector(747.784, 236.195, 16.031), Angle(0, 43.313, 0) )
	Create_Bread( Vector(740.645, -46.39, 16.031), Angle(0, 181.253, 0) )
	Create_Bread( Vector(741.627, -98.384, 16.031), Angle(0, 180.043, 0) )
	Create_PortableMedkit( Vector(-281.523, -305.598, 16.031), Angle(0, 91.418, 0) )
	Create_Beer( Vector(-803.968, 353.114, 83.915), Angle(0, 0.339, 0) )
	Create_Beer( Vector(-937.809, 352.323, 99.038), Angle(0, 0.339, 0) )
	Create_Beer( Vector(-1063.806, 351.579, 113.275), Angle(0, 0.339, 0) )
	Create_Beer( Vector(-1179.304, 350.897, 126.326), Angle(0, 0.339, 0) )
	Create_Beer( Vector(-1284.301, 350.277, 138.19), Angle(0, 0.339, 0) )
	Create_Armour( Vector(-1270.154, 1113.909, 224.031), Angle(0, 271.074, 0) )
	Create_Backpack( Vector(-1708.065, 1099.909, 448.031), Angle(0, 343.674, 0) )
	Create_CorzoHat( Vector(-723.281, 1104.125, 512.031), Angle(0, 181.234, 0) )
	Create_VolatileBattery( Vector(-1386.591, 347.733, 512.031), Angle(0, 181.289, 0) )
	Create_Backpack( Vector(-1970.443, -50.628, 512.031), Angle(0, 2.209, 0) )
	Create_Armour100( Vector(-1368.509, -55.807, 512.031), Angle(0, 180.574, 0) )
	Create_EldritchArmour10( Vector(-404.273, 560.23, 522.47), Angle(0, 175.514, 0) )
	Create_Armour( Vector(-1665.875, 737.277, 224.031), Angle(0, 271.794, 0) )
	Create_Armour( Vector(-1662.602, 1102.042, 224.031), Angle(0, 257.219, 0) )
	Create_Armour( Vector(-831.522, 722.882, 224.031), Angle(0, 89.664, 0) )
	Create_Beer( Vector(-1030.272, 304.009, 368.031), Angle(0, 185.864, 0) )
	Create_Beer( Vector(-1035.047, 312.803, 371.063), Angle(0, 194.004, 0) )
	Create_Beer( Vector(-1034.837, 293.641, 371.063), Angle(0, 176.954, 0) )
	Create_Beer( Vector(-1088.996, 396.068, 368.031), Angle(0, 267.429, 0) )
	Create_Beer( Vector(-1078.888, 394.182, 371.063), Angle(0, 253.239, 0) )
	Create_Beer( Vector(-1068.671, 389.04, 374.094), Angle(0, 237.344, 0) )
	Create_PortableMedkit( Vector(-1856.056, -50.985, 224.031), Angle(0, 179.074, 0) )
	Create_EldritchArmour10( Vector(-405.826, 159.929, 522.47), Angle(0, 179.568, 0) )
	Create_EldritchArmour10( Vector(-404.119, 914.378, 522.472), Angle(0, 262.918, 0) )
	Create_Armour5( Vector(383.955, -743.358, 16.031), Angle(0, 354.904, 0) )
	Create_Armour5( Vector(190.765, -743.851, 16.031), Angle(0, 51.334, 0) )
	Create_Armour( Vector(-779.826, -1264.398, 128.031), Angle(0, 90.739, 0) )
	Create_Beer( Vector(-1428.542, -1327.414, 128.031), Angle(0, 88.594, 0) )
	Create_Beer( Vector(-1461.716, -1326.6, 128.031), Angle(0, 88.594, 0) )
	Create_Beer( Vector(-1500.728, -1325.643, 128.031), Angle(0, 88.594, 0) )
	Create_Beer( Vector(-1531.136, -1324.897, 128.031), Angle(0, 88.594, 0) )
	Create_Beer( Vector(-1291.983, -1293.888, 128.031), Angle(0, 90.299, 0) )
	Create_Armour( Vector(-2418.205, -783.077, 128.031), Angle(0, 90.578, 0) )
	Create_EldritchArmour10( Vector(-1761.993, -1263.863, 355.129), Angle(0, 92.119, 0) )
	Create_EldritchArmour10( Vector(-1773.518, -1268.391, 358.161), Angle(0, 140.354, 0) )
	Create_SpiritSphere( Vector(-2515.093, -929.498, 32.031), Angle(0, 89.423, 0) )
	Create_PortableMedkit( Vector(-1928.377, -1251.167, 128.031), Angle(0, 270.508, 0) )
	Create_Bread( Vector(-2542.975, -1143.452, 32.031), Angle(0, 269.958, 0) )
	Create_Bread( Vector(-2598.956, -1144.273, 32.031), Angle(0, 269.958, 0) )
	Create_Bread( Vector(-2656.486, -1141.698, 32.031), Angle(0, 269.738, 0) )
	Create_EldritchArmour10( Vector(-3111.66, -1138.433, 255.891), Angle(0, 271.834, 0) )
	Create_PortableMedkit( Vector(-2317.535, -1316.596, 259.218), Angle(0, 271.12, 0) )
	Create_GrenadeAmmo( Vector(-2889.203, -1913.096, -111.969), Angle(0, 269.029, 0) )
	Create_Beer( Vector(-2850.189, -1136.464, -111.969), Angle(0, 269.304, 0) )
	Create_Beer( Vector(-2849.158, -1153.384, -108.937), Angle(0, 267.929, 0) )
	Create_Beer( Vector(-2839.291, -1147.386, -105.906), Angle(0, 258.469, 0) )
	Create_Beer( Vector(-2905.437, -1151.107, -111.969), Angle(0, 271.284, 0) )
	Create_Beer( Vector(-2884.777, -1147.549, -111.969), Angle(0, 278.104, 0) )
	Create_Beer( Vector(-2899.323, -1169.138, -111.642), Angle(0, 268.259, 0) )
	Create_FalanrathThorn( Vector(-3349.974, -1710.615, 226.129), Angle(0, 213.894, 0) )
	Create_Backpack( Vector(-4581.232, -337.897, -175.969), Angle(0, 359.4, 0) )
	Create_EldritchArmour10( Vector(-4293.452, -937.722, 83.156), Angle(0, 91.084, 0) )
	Create_ManaCrystal100( Vector(-3926.996, 355.93, -175.969), Angle(0, 269.567, 0) )
	Create_Backpack( Vector(-3742.424, -340.116, -303.969), Angle(0, 59.522, 0) )
	Create_Armour100( Vector(-3640.879, -359.814, -303.969), Angle(0, 90.707, 0) )
	Create_FoolsOathWeapon( Vector(-3512.187, -1348.786, -175.969), Angle(0, 270.857, 0) )
	Create_FoolsOathAmmo( Vector(-3363.434, -1368.43, -175.969), Angle(0, 178.987, 0) )
	Create_Beer( Vector(-2984.686, -545.993, -294.22), Angle(0, 179.807, 0) )
	Create_Beer( Vector(-3014.042, -545.894, -294.22), Angle(0, 179.807, 0) )
	Create_Beer( Vector(-3032.074, -545.833, -293.061), Angle(0, 179.807, 0) )
	Create_Beer( Vector(-3059.656, -545.74, -294.22), Angle(0, 179.807, 0) )
	Create_Armour( Vector(-3089.74, -620.425, 13.451), Angle(0, 18.851, 0) )
	Create_Armour200( Vector(-2289.334, -13.999, 690.218), Angle(0, 0.462, 0) )
	Create_AnthrakiaWeapon( Vector(-290.462, 564.166, 690.058), Angle(0, 180.612, 0) )
	Create_HarbingerWeapon( Vector(-2286.687, 1026.891, 690.058), Angle(0, 183.392, 0) )
	Create_InfiniteGrenades( Vector(-1378.162, 1414.075, 691.129), Angle(0, 186.747, 0) )
	Create_Bread( Vector(-1650.073, 353.241, 179.52), Angle(0, 306.339, 0) )
	Create_Bread( Vector(-1805.367, 337.396, 197.067), Angle(0, 37.529, 0) )
	Create_Backpack( Vector(-1990.326, 377.94, 217.967), Angle(0, 44.459, 0) )
	Create_EldritchArmour( Vector(-947.071, -307.144, 690.218), Angle(0, 88.739, 0) )
	Create_Armour100( Vector(9.185, -753.123, 16.031), Angle(0, 89.514, 0) )
	Create_ReikoriKey( Vector(-289.351, 1152.481, 690.218), Angle(0, 359.638, 0) )
	Create_MegaSphere( Vector(-3630.902,-719.791,882.277), Angle(0, 180.175, 0) )
	Create_EldritchArmour( Vector(-241.705, -304.658, 256.031), Angle(0, 269.394, 0) )
	Create_PortableMedkit( Vector(711.736, -883.809, 128.031), Angle(0, 344.274, 0) )
	Create_FalanranWeapon( Vector(-2886.51, -1917.574, -108.937), Angle(0, 160.95, 0) )

	CreateTeleporter( "START_1", Vector(-329.495,-615.118,512.031), "START_2", Vector(-94.316,-451.316,469.704) )
	
	--[[ The trigger to start the mode ]]
	CreateTrigger( Vector(-119.038,-451.276,443.554), Vector(-99.187,-99.187,0), Vector(99.187,99.187,197.28), START_SIN_HUNTER )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

SINHUNTER_SPAWNS_FRONT = {
Vector(-1426.516,-1571.061,128.031),
Vector(-1488.122,-1573.312,128.031),
Vector(-1506.3,-1497.169,128.031),
Vector(-1441.205,-1438.466,128.031),
Vector(-1560.106,-1208.515,128.031),
Vector(-1610.199,-1095.704,128.031),
Vector(-1623.266,-941.785,128.031),
Vector(-1531.439,-850.295,128.031),
Vector(-1409.303,-798.926,128.031),
Vector(-1406.462,-679.557,128.031),
Vector(-1455.843,-586.953,128.031),
Vector(-1401.445,-565.248,128.031),
Vector(-1263.852,-557.079,128.031),
Vector(-1108.077,-556.111,128.031),
Vector(-989.731,-605.695,128.031),
Vector(-977.174,-786.098,128.031),
Vector(-968.54,-877.015,128.031),
Vector(-994.55,-1043.205,128.031),
Vector(-936.697,-1115.605,128.031),
Vector(-825.89,-1081.658,128.031),
Vector(-799.956,-953.027,128.031),
Vector(-812.289,-838.415,128.031),
Vector(-1100.662,-797.143,128.031),
Vector(-1145.855,-862.756,128.031),
Vector(-1702.399,-658.754,128.031),
Vector(-1771.107,-728.84,128.031),
Vector(-1864.309,-722.612,128.031),
Vector(-1949.311,-667.375,128.031),
Vector(-654.761,-830.567,84.179),
Vector(-583.1,-947.997,52.828),
Vector(-445.505,-953.07,16.031),
Vector(-353.783,-867.502,16.031),
Vector(-305.536,-837.908,16.031),
Vector(-273.933,-968.664,16.031),
Vector(-213.463,-900.845,16.031),
Vector(-157.786,-767.172,16.031),
Vector(-159.121,-574.449,16.031),
Vector(-184.989,-416.112,16.031),
Vector(-186.012,-250.527,16.031),
Vector(-190.043,-113.391,16.031),
Vector(-194.306,20.665,16.031),
Vector(-183.561,145.351,16.031),
Vector(-68.713,217.214,16.031),
Vector(117.148,-65.989,16.031),
Vector(290.409,-70.228,16.031),
Vector(436.135,-84.201,16.031),
Vector(590.636,-154.119,16.031),
Vector(682.761,-277.218,16.031),
Vector(682.113,-361.666,16.031),
Vector(445.829,-202.369,16.031),
Vector(399.228,-332.422,16.031),
Vector(379.935,-479.932,16.031),
Vector(357.444,-611.858,16.031),
Vector(282.665,-718.298,16.031),
Vector(155.862,-713.412,16.031),
Vector(73.863,-614.572,16.031),
Vector(78.715,-428.81,16.031),
Vector(113.033,-282.147,16.031),
Vector(93.601,-118.681,16.031),
Vector(309.318,197.705,128.031),
Vector(438.615,210.659,128.031),
Vector(540.187,218.241,128.031),
Vector(528.748,82.75,128.031),
Vector(438.632,12.406,128.031),
Vector(381.339,-69.979,128.031),
Vector(462.75,-142.928,128.031),
Vector(580.711,-152.009,128.031),
Vector(647.373,-131.733,128.031),
Vector(669.673,-253.27,128.031),
Vector(610.423,-306.985,128.031),
Vector(572.278,-392.618,128.031),
Vector(594.124,-485.845,128.031),
Vector(645.815,-594.363,128.031),
Vector(661.822,-704.185,128.031),
Vector(622.57,-799.15,128.031),
Vector(599.506,-892.5,128.031),
Vector(236.082,-873.002,256.031),
Vector(142.43,-932.726,256.031),
Vector(14.692,-941.906,256.031),
Vector(-121.63,-939.011,256.031),
Vector(-265.583,-938.958,256.031),
Vector(-411.879,-886.294,256.031),
Vector(-262.403,-807.411,256.031),
Vector(-134.2,-733.549,256.031),
Vector(-119.335,-489.883,256.031),
Vector(-98.375,-399.052,256.031),
Vector(-108.449,-315.073,256.031),
Vector(-127.467,-230.438,256.031),
Vector(-147.116,-152.94,256.031),
Vector(-188.237,-90.534,256.031),
Vector(-212.129,-8.504,256.031),
Vector(-190.711,44.925,256.031),
Vector(-108.608,9.54,256.031),
Vector(-52.988,51.781,256.031),
Vector(-50.743,139.705,256.031),
Vector(83.211,187.596,298.128),
Vector(165.551,94.382,334.152),
Vector(235.271,65.731,364.655),
Vector(281.875,160.057,368.031),
Vector(348.387,116.615,368.031),
Vector(406.966,48.447,368.031),
Vector(497.702,48.669,368.031),
Vector(599.678,107.795,368.031),
Vector(667.973,71.66,368.031),
Vector(616.848,-61.406,368.031),
Vector(554.527,-116.505,368.031),
Vector(535.056,-208.076,368.031),
Vector(577.216,-298.253,368.031),
Vector(641.277,-393.797,368.031),
Vector(686.612,-510.189,368.031),
Vector(684.549,-642.835,368.031),
Vector(644.443,-766.232,368.031),
Vector(675.621,-857.19,368.031),
Vector(459.829,-449.876,368.031),
Vector(246.572,-448.806,384.031),
Vector(121.986,-449.425,384.031),
Vector(-119.335,-489.883,256.031),
Vector(-98.375,-399.052,256.031),
Vector(-108.449,-315.073,256.031),
Vector(-127.467,-230.438,256.031),
Vector(-147.116,-152.94,256.031),
Vector(-188.237,-90.534,256.031),
Vector(-212.129,-8.504,256.031),
Vector(-190.711,44.925,256.031),
Vector(-108.608,9.54,256.031),
Vector(-52.988,51.781,256.031),
Vector(-50.743,139.705,256.031),
Vector(83.211,187.596,298.128),
Vector(165.551,94.382,334.152),
Vector(235.271,65.731,364.655),
Vector(281.875,160.057,368.031),
Vector(348.387,116.615,368.031),
Vector(406.966,48.447,368.031),
Vector(497.702,48.669,368.031),
Vector(599.678,107.795,368.031),
Vector(667.973,71.66,368.031),
Vector(616.848,-61.406,368.031),
Vector(554.527,-116.505,368.031),
Vector(535.056,-208.076,368.031),
Vector(577.216,-298.253,368.031),
Vector(641.277,-393.797,368.031),
Vector(686.612,-510.189,368.031),
Vector(684.549,-642.835,368.031),
Vector(644.443,-766.232,368.031),
Vector(675.621,-857.19,368.031),
Vector(459.829,-449.876,368.031),
Vector(246.572,-448.806,384.031),
Vector(121.986,-449.425,384.031),
Vector(-457.538,-188.064,16.031),
Vector(-579.433,-212.962,16.031),
Vector(-721.245,-247.19,16.031),
Vector(-888.964,-238.574,16.031),
Vector(-1072.896,-204.457,16.031),
Vector(-1231.862,-177.976,16.031),
Vector(-1411.049,-178.003,16.031),
Vector(-1589.779,-184.19,16.031),
Vector(-1738.231,-202.39,16.031),
Vector(-1888.75,-229.033,16.031),
Vector(-2065.32,-245.665,16.031),
Vector(-2192.656,-181.826,16.031),
Vector(-2198.212,-33.891,16.031),
Vector(-2076.423,81.578,16.031),
Vector(-1927.236,115.186,16.031),
Vector(-1774.228,119.456,16.031),
Vector(-1602.348,125.382,16.031),
Vector(-1450.142,127.653,16.031),
Vector(-1303.228,130.42,16.031),
Vector(-1158.858,132.562,16.031),
Vector(-1016.697,166.694,16.031),
Vector(-875.503,178.63,16.031),
Vector(-723.634,143.348,16.031),
Vector(-578.065,138.555,16.031),
Vector(-432.302,196.929,16.031),
Vector(-439.212,338.437,16.031),
Vector(-475.326,464.311,16.031),
Vector(-434.539,614.949,16.031),
Vector(-508.656,747.679,16.031),
Vector(-595.93,849.855,16.031),
Vector(-730.572,906.491,16.031),
Vector(-872.665,944.171,16.031),
Vector(-996.21,971.412,16.031),
Vector(-1122.839,962.685,16.031),
Vector(-1246.823,907.865,16.031),
Vector(-1376.754,874.989,16.031),
Vector(-1494.751,914.568,16.031),
Vector(-1592.864,956.086,16.031),
Vector(-1719.234,944.008,16.031),
Vector(-1859.437,886.689,16.031),
Vector(-1985.475,911.972,16.031),
Vector(-2132.895,945.522,16.031),
Vector(-2205.091,826.499,16.031),
Vector(-2158.376,688.247,16.031),
Vector(-2107.661,554.338,16.031),
Vector(-1989.581,542.056,16.031),
Vector(-1835.339,573.836,16.031),
Vector(-1689.958,566.054,16.031),
Vector(-1540.169,525.091,16.031),
Vector(-1394.322,540.396,16.031),
Vector(-1244.809,584.138,16.031),
Vector(-1112.913,569.781,16.031),
Vector(-988.729,528.455,16.031),
Vector(-858.319,538.787,16.031),
Vector(-2132.805,1104.293,16.031),
Vector(-2153.937,1216.824,16.031),
Vector(-2115.508,1325.918,16.031),
Vector(-2002.983,1363.633,16.031),
Vector(-1869.867,1312.762,16.031),
Vector(-1768.125,1309.901,16.031),
Vector(-1656.52,1344.635,16.031),
Vector(-1556.988,1329.95,16.031),
Vector(-1480.864,1270.822,16.031),
Vector(-1377.663,1257.691,16.031),
Vector(-1310.328,1294.016,16.031),
Vector(-1240.709,1354.823,16.031),
Vector(-1164.086,1383.665,16.031),
Vector(-1063.538,1348.361,16.031),
Vector(-974.348,1276.676,16.031),
Vector(-851.38,1272.772,16.031),
Vector(-754.416,1311.052,16.031),
Vector(-637.849,1348.486,16.031),
Vector(-542.861,1327.685,16.031),
Vector(-476.273,1244.616,16.031),
Vector(-1477.561,1050.614,368.031),
Vector(-1235.333,1052.926,368.031),
Vector(-1110.064,771.778,224.031),
Vector(-1063.99,682.753,224.031),
Vector(-1247.251,686.714,224.031),
Vector(-1512.841,689.134,368.031),
Vector(-1113.918,808.03,368.031),
Vector(-1469.798,804.327,368.031),
Vector(-1176.717,300.631,368.031),
Vector(-1430.798,280.298,368.031),
Vector(-1435.678,403.895,368.031),
Vector(-1211.981,423.78,368.031),
Vector(-1820.276,-116.471,512.031),
Vector(-1501.734,-115.231,512.031),
Vector(-1422.564,0.699,512.031),
Vector(-1823.183,17.636,512.031),
Vector(-1243.829,409.514,512.031),
Vector(-1569.486,287.741,512.031),
Vector(-1251.152,782.603,512.031),
Vector(-902.235,1153.425,512.031),
Vector(-867.242,1039.721,512.031)}

SINHUNTER_SPAWNS_BACK = {
Vector(-2676.579,-901.878,-127.969),
Vector(-2653.221,-782.559,-127.969),
Vector(-2783.753,-736.268,-127.969),
Vector(-2706.54,-574.412,-127.969),
Vector(-2820.54,-605.39,-127.969),
Vector(-3024.785,-889.28,-303.969),
Vector(-3012.858,-752.572,-303.969),
Vector(-3081.592,-652.917,-303.969),
Vector(-3147.281,-548.916,-303.969),
Vector(-3185.082,-686.134,-303.969),
Vector(-3181.709,-839.214,-303.969),
Vector(-3274.986,-905.191,-303.969),
Vector(-3357.518,-981.655,-303.969),
Vector(-3365.141,-1105.151,-303.969),
Vector(-3433.106,-1149.959,-303.969),
Vector(-3538.881,-1154.502,-303.969),
Vector(-3622.041,-1208.803,-303.969),
Vector(-3664.485,-1185.67,-303.969),
Vector(-3641.76,-1061.404,-303.969),
Vector(-3683.704,-941.603,-303.969),
Vector(-3712.234,-809.601,-303.969),
Vector(-3813.557,-763.967,-303.969),
Vector(-3945.455,-770.575,-303.969),
Vector(-4061.47,-817.916,-303.969),
Vector(-4142.851,-844.809,-303.969),
Vector(-4171.598,-749.281,-303.969),
Vector(-4137.501,-651.451,-303.969),
Vector(-4015.318,-580.928,-303.969),
Vector(-4030.577,-495.681,-303.969),
Vector(-4103.07,-363.683,-303.969),
Vector(-4059.055,-285.343,-303.969),
Vector(-3950.572,-267.62,-303.969),
Vector(-3888.793,-350.813,-303.969),
Vector(-3832.771,-423.145,-303.969),
Vector(-3696.175,-442.81,-303.969),
Vector(-3539.471,-458.033,-303.969),
Vector(-3483.302,-401.032,-303.969),
Vector(-3569.518,-279.52,-303.969),
Vector(-3712.902,-224.591,-303.969),
Vector(-3835.504,-112.81,-303.969),
Vector(-3519.613,-119.75,-303.969),
Vector(-3577.015,-80.204,-303.969),
Vector(-3525.782,115.201,-175.969),
Vector(-3432.548,167.921,-175.969),
Vector(-3354.102,235.4,-175.969),
Vector(-3453.331,245.001,-175.969),
Vector(-3549.351,278.236,-175.969),
Vector(-3667.064,279.394,-175.969),
Vector(-3733.132,206.816,-175.969),
Vector(-3769.223,114.42,-175.969),
Vector(-3843.6,54.325,-175.969),
Vector(-3954.061,99.414,-175.969),
Vector(-3993.971,190.353,-175.969),
Vector(-4056.41,277.43,-175.969),
Vector(-4149.097,260.078,-175.969),
Vector(-4204.778,153.612,-175.969),
Vector(-4192.813,62.855,-175.969),
Vector(-4180.732,-47.834,-175.969),
Vector(-4233.371,-121.816,-175.969),
Vector(-4339.406,-105.942,-175.969),
Vector(-4411.909,-30.765,-175.969),
Vector(-4494.93,14.913,-175.969),
Vector(-4429.017,-176.66,-175.969),
Vector(-4432.581,-352.458,-175.969),
Vector(-4346.556,-417.673,-175.969),
Vector(-4301.821,-465.201,-175.969),
Vector(-4384.446,-502.904,-175.969),
Vector(-4403.891,-513.585,-175.969),
Vector(-4348.97,-592.28,-175.969),
Vector(-4391.447,-645.324,-175.969),
Vector(-4487.398,-670.691,-175.969),
Vector(-4567.805,-713.73,-175.969),
Vector(-4519.21,-817.202,-175.969),
Vector(-4439.016,-877.929,-175.969),
Vector(-4288.685,-917.93,-175.969),
Vector(-4139.888,-850.653,-303.969),
Vector(-4341.642,-1222.067,-63.969),
Vector(-4474.279,-1224.762,-63.969),
Vector(-4630.667,-1203.811,-63.969),
Vector(-4680.834,-1272.338,-63.969),
Vector(-4363.877,-1222.714,-63.969),
Vector(-4303.473,-1284.107,-63.969),
Vector(-4243.998,-1388.935,-63.969),
Vector(-4132.384,-1457.11,-63.969),
Vector(-4083.814,-1417.96,-63.969),
Vector(-4078.211,-1262.605,-63.969),
Vector(-4080.658,-1209.458,-63.969),
Vector(-3950.276,-1215.41,-63.969),
Vector(-3868.394,-1240.874,-63.969),
Vector(-3876.018,-1328.388,-63.969),
Vector(-3949.527,-1405.833,-63.969),
Vector(-3983.403,-1527.382,-63.969),
Vector(-3867.828,-1592.06,-63.969),
Vector(-3980.007,-1716.328,-31.969),
Vector(-3969.345,-1854.169,-31.969),
Vector(-3875.76,-1872.985,-31.969),
Vector(-3837.669,-1792.718,-31.969),
Vector(-3675.101,-1859.061,0.031),
Vector(-3580.035,-1892.614,0.031),
Vector(-3532.468,-1826.354,0.031),
Vector(-3454.129,-1787.821,0.031),
Vector(-3364.435,-1785.331,0.031),
Vector(-3319.949,-1870.708,0.031),
Vector(-3189.107,-1803.986,0.031),
Vector(-3226.759,-1686.405,0.031),
Vector(-3198.093,-1457.977,-111.969),
Vector(-3209.531,-1321.714,-111.969),
Vector(-3223.89,-1184.195,-111.969),
Vector(-3052.133,-1293.039,-111.969),
Vector(-2922.419,-1239.305,-111.969),
Vector(-2804.768,-1249.042,-111.969),
Vector(-2884.648,-1357.129,-111.969),
Vector(-2887.518,-1514.416,-111.969),
Vector(-2846.631,-1664.468,-111.969),
Vector(-2933.196,-1726.835,-111.969),
Vector(-3048.07,-1747.611,-111.969),
Vector(-3077.552,-1899.076,-111.969),
Vector(-2488.778,-1782.119,32.031),
Vector(-2476.403,-1603.276,32.031),
Vector(-2350.42,-1595.642,32.031),
Vector(-2309.879,-1711.166,32.031),
Vector(-2292.262,-1830.038,32.031),
Vector(-2194.575,-1900.092,32.031),
Vector(-2078.734,-1868.911,32.031),
Vector(-2018.989,-1779.541,32.031),
Vector(-2006.681,-1670.908,32.031),
Vector(-2066.728,-1591.555,32.031),
Vector(-2165.762,-1622.161,32.031),
Vector(-2187.647,-1709.931,32.031),
Vector(-2455.119,-1528.897,32.031),
Vector(-2522.729,-1493.415,32.031),
Vector(-2607.2,-1487.054,32.031),
Vector(-2632.342,-1394.944,32.031),
Vector(-2631.172,-1288.749,32.031),
Vector(-2615.268,-1229.754,32.031),
Vector(-2506.771,-1267.737,32.031),
Vector(-2398.507,-1280.171,32.031),
Vector(-2359.091,-1179.044,32.031),
Vector(-2364.676,-1078.093,32.031),
Vector(-2354.433,-989.96,32.031),
Vector(-3355.164,-1372.443,-175.969),
Vector(-3366.536,-1461.856,-175.969),
Vector(-3425.502,-1538.387,-175.969),
Vector(-3541.685,-1548.35,-175.969),
Vector(-3588.976,-1488.675,-175.969),
Vector(-3516.578,-1422.163,-175.969),
Vector(-3668.123,-1376.4,-175.969)}