		
if CLIENT then return end

--[[ I don't use a level string auto parse here because i make the map ending myself after the player triggers the return ]]

GLOB_NO_FENIC = true

function MAP_CREATE_ENDING()

	--[[ Make the map ending manually ]]
	local END_P = Vector(-5874.76,-388.495,-1277.859)
	local END_P1 = Vector(-234.804,-234.804,0)
	local END_P2 = Vector(234.804,234.804,436.806)
	LEVEL_SWAP = {END_P1,END_P2}
	LEVEL_TOSWAP = "mimp2"
	RemoveAllEnts("temporal_changelevel")
	CreateChangeLevel( END_P, END_P1, END_P2, "mimp2" )

end

function MAP_LOADED_FUNC()

	--[[ Delete data and stuff since this is a fresh map/save ]]
	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end
	
	--[[ No stuff to get in our way ]]
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("env_fade")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-3462,2049,-2167.961),Angle(0,155,0))
	CreateClothesLocker( Vector(-3624.946,1953.767,-2166.073), Angle(0,270.76,0) )
	
	--[[ Making hint props ]]
	CreateProp(Vector(-4458.562,1971.563,-1054.594), Angle(1.187,98.438,-177.363), "models/props_junk/harpoon002a.mdl", true)
	CreateProp(Vector(-3121.75,3260.938,-1069.281), Angle(10.942,87.188,-8.481), "models/props_combine/breenchair.mdl", true)
	CreateProp(Vector(248.406,4198.094,-915.812), Angle(-6.504,58.359,73.564), "models/weapons/w_alyx_gun.mdl", true)
	CreateProp(Vector(222.938,4183.656,-916.969), Angle(-11.558,-147.48,71.543), "models/weapons/w_pistol.mdl", true)
	CreateProp(Vector(225.75,4198.469,-918.844), Angle(-0.176,-63.457,-0.044), "models/items/boxsrounds.mdl", true)
	CreateProp(Vector(-95.031,5027.719,-150.719), Angle(0.016,83.584,-3.609), "models/props_c17/briefcase001a.mdl", true)
	CreateProp(Vector(-94.094,4966.688,-150.937), Angle(0.016,95.147,0.313), "models/props_c17/briefcase001a.mdl", true)
	local CHAIR = CreateProp(Vector(229.531,4197.906,-914.062), Angle(-0.044,-51.108,0), "models/props_interiors/furniture_couch02a.mdl", true)
	CHAIR:SetHealth(50000)
	
	--[[ Warning Progress trigger and stuff ]]
	local WARN_1 = CreateProp(Vector(-2146.5,1292.75,-1426.594), Angle(-0.615,-46.318,-0.879), "models/props_docks/channelmarker_gib03.mdl", true)
	local WARN_2 = CreateProp(Vector(-2029.625,1421.5,-1425.062), Angle(-0.615,-43.418,1.582), "models/props_docks/channelmarker_gib03.mdl", true)
	WARN_1:SetHealth(500000)
	WARN_2:SetHealth(500000)
	
	local PROGRESS_WARNING_1 = function(self,ply)
		
		if ply and IsValid(ply) and ply:IsPlayer() then
		
			local P1, P2 = CreateTeleporter( "STALL_PROGRESS_1", ply:GetPos(), "STALL_PROGRESS_2", Vector(-2317.108,1616.058,-1436.023) )
			
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
			
		end
	
		net.Start("DollMessage")
		net.WriteString("Not yet kiddo, Try again later. You will know when you see it")
		net.Broadcast()
		
	end
	
	local WARN_TRIGGER = CreateTrigger( Vector(281.394,629.226,-2188.57), Vector(-1921.518,-1921.518,0), Vector(1921.518,1921.518,3251.172), PROGRESS_WARNING_1, true )
	--[[ End of warning progress trigger stuff ]]
	
	--[[ Delete the warning and teleport trigger here ]]
	local DELETE_WARNING = function()
	
		MAP_CREATE_ENDING()
		
		if WARN_TRIGGER and IsValid(WARN_TRIGGER) then
			WARN_TRIGGER:Remove()
		end
		
		--[[ Lets make all these extra items after the player is on their return trip to help save on performance ]]
		Create_ShotgunAmmo( Vector(-3108.082, 2664.328, -959.969), Angle(0, 199.405, 0) )
		Create_RifleAmmo( Vector(-3106.351, 2615.949, -959.969), Angle(0, 145.725, 0) )
		Create_Medkit25( Vector(-3151.744, 2618.399, -959.969), Angle(0, 89.845, 0) )
		Create_Armour( Vector(-3113.645, 2747.272, -934.015), Angle(0, 181.805, 0) )
		Create_Medkit10( Vector(-2722.901, 2801.149, -959.969), Angle(0, 159.145, 0) )
		Create_Medkit25( Vector(-885.209, 491.205, -1728), Angle(0, 232.447, 0) )
		Create_Armour5( Vector(-882.673, 449.11, -1728), Angle(0, 195.047, 0) )
		Create_Armour5( Vector(-883.189, 448.972, -1724.969), Angle(0, 195.047, 0) )
		Create_Armour5( Vector(-883.705, 448.833, -1721.937), Angle(0, 195.047, 0) )
		Create_Armour5( Vector(-884.221, 448.694, -1718.906), Angle(0, 195.047, 0) )
		Create_Armour5( Vector(-930.447, 497.558, -1728), Angle(0, 268.967, 0) )
		Create_Armour5( Vector(-930.464, 496.597, -1724.969), Angle(0, 268.967, 0) )
		Create_Armour5( Vector(-930.481, 495.637, -1721.937), Angle(0, 268.967, 0) )
		Create_Armour5( Vector(-930.499, 494.676, -1718.906), Angle(0, 268.967, 0) )
		Create_Medkit10( Vector(-130.367, 306.139, -1824), Angle(0, 273.863, 0) )
		Create_Medkit10( Vector(-114.545, 271.047, -1824), Angle(0, 256.593, 0) )
		Create_SpiritSphere( Vector(362.071, -62.082, -1664), Angle(0, 359.422, 0) )
		Create_RifleWeapon( Vector(3568.062, -1229.51, -1795.969), Angle(0, 179.769, 0) )
		Create_RifleAmmo( Vector(3558.104, -1186.006, -1795.969), Angle(0, 207.929, 0) )
		Create_RifleAmmo( Vector(3555.831, -1280.284, -1795.969), Angle(0, 128.729, 0) )
		Create_Armour100( Vector(-2671.001, -275.385, -1277.394), Angle(0, 341.504, 0) )
		Create_ManaCrystal100( Vector(-2627.737, -244.19, -1278.369), Angle(0, 281.884, 0) )
		Create_Armour5( Vector(-2580.208, -262.551, -1277.745), Angle(0, 270.884, 0) )
		Create_Armour5( Vector(-2580.2, -263.085, -1274.714), Angle(0, 270.884, 0) )
		Create_Armour5( Vector(-2580.192, -263.62, -1271.683), Angle(0, 270.884, 0) )
		Create_Armour5( Vector(-2580.184, -264.154, -1268.651), Angle(0, 270.884, 0) )
		
	end
	
	local DELETE_WARNING_PRE = function()
		CreateTrigger( Vector(-2975.888,3310.766,-791.969), Vector(-85.648,-85.648,0), Vector(85.648,85.648,129.715), DELETE_WARNING )
	end
	
	CreateTrigger( Vector(0.724,5132.17,1018.031), Vector(-180.428,-180.428,0), Vector(180.428,180.428,133.744), DELETE_WARNING_PRE )
	--[[ End of deleting the warning trigger ]]
	
	--[[ Another item buffer 2 trigger to save on performance ]]
	local ITEM_BUFFER_2 = function()
		Create_SpiritSphere( Vector(-3123.75, 3277.11, -1043.521), Angle(0, 91.071, 0) )
		Create_ManaCrystal( Vector(-1905.391, 3171.677, -896), Angle(0, 157.942, 0) )
		Create_ManaCrystal( Vector(-1964.286, 3174.791, -896), Angle(0, 141.882, 0) )
		Create_Medkit25( Vector(-825.899, 4499.736, -741.765), Angle(0, 359.902, 0) )
		Create_Armour( Vector(-827.455, 4541.738, -741.765), Angle(0, 1.882, 0) )
		Create_Beer( Vector(210.97, 4428.963, -935.969), Angle(0, 261.861, 0) )
		Create_Beer( Vector(196.462, 4423.186, -935.969), Angle(0, 271.981, 0) )
		Create_Beer( Vector(190.885, 4408.912, -935.969), Angle(0, 277.481, 0) )
		Create_Armour5( Vector(235.158, 4445.493, -935.969), Angle(0, 256.141, 0) )
		Create_Armour5( Vector(235.498, 4442.685, -932.937), Angle(0, 255.481, 0) )
		Create_Armour5( Vector(246.96, 4414.745, -935.969), Angle(0, 239.421, 0) )
		Create_Armour5( Vector(245.158, 4411.696, -932.937), Angle(0, 239.421, 0) )
		Create_ShotgunAmmo( Vector(-701.129, 4613.564, -751.969), Angle(0, 43.981, 0) )
		Create_PistolAmmo( Vector(199.678, 4465.922, -675.103), Angle(0, 162.559, 0) )
		Create_GrenadeAmmo( Vector(152.056, 4417.736, -670.983), Angle(-44.11, -98.212, 0) )
		Create_EldritchArmour10( Vector(318.101, 5091.082, -511.969), Angle(0, 281.15, 0) ) --[[ Secret 3 ]]
		Create_EldritchArmour10( Vector(344.018, 5083.023, -511.969), Angle(0, 257.39, 0) ) --[[ Secret 3 ]]
		Create_PortableMedkit( Vector(332.255, 5110.351, -511.969), Angle(0, 276.97, 0) ) --[[ Secret 3 ]]
		Create_RevolverAmmo( Vector(358.406, 5097.925, -511.969), Angle(0, 256.29, 0) ) --[[ Secret 3 ]]
		Create_Medkit25( Vector(-731.634, 4932.497, -768), Angle(0, 1.811, 0) )
		Create_ManaCrystal100( Vector(-38.356, 5427.161, -766.976), Angle(0, 187.351, 0) )
		Create_SMGAmmo( Vector(-698.59, 4640.799, -751.969), Angle(0, 84.831, 0) )
		Create_Medkit10( Vector(-18.16, 4944.231, -703.969), Angle(0, 95.391, 0) )
		Create_Medkit10( Vector(-52.272, 4945.145, -703.969), Angle(0, 60.41, 0) )
		Create_Armour100( Vector(27.189, 4999.731, -447.969), Angle(0, 10.391, 0) )
		Create_SuperShotgunWeapon( Vector(107.276, 5009.364, -159.969), Angle(0, 188.89, 0) )
		Create_Medkit25( Vector(55.853, 5009.506, 296.091), Angle(0, 0.18, 0) )
		Create_Medkit25( Vector(29.603, 5069.118, 1022.031), Angle(0, 181.774, 0) )
		Create_PortableMedkit( Vector(25.656, 5035.832, 1022.031), Angle(0, 142.504, 0) )
		Create_Medkit10( Vector(-53.929, 5074.69, -189.823), Angle(0, 328.123, 0) )
		Create_Armour5( Vector(-27.941, 5090.121, -189.823), Angle(0, 289.403, 0) )
		Create_Armour5( Vector(-27.763, 5089.617, -186.792), Angle(0, 289.403, 0) )
		Create_Armour5( Vector(-27.585, 5089.113, -183.761), Angle(0, 289.403, 0) )
		Create_PortableMedkit( Vector(20.295, 5029.431, -447.969), Angle(0, 43.944, 0) )
		Create_Medkit25( Vector(370.787, 5087.692, -704), Angle(0, 300.984, 0) )
		Create_RifleWeapon( Vector(-810.758, 4664.823, -572.863), Angle(0, 352.164, 0) )
		Create_RifleAmmo( Vector(-805.979, 4637.083, -572.863), Angle(0, 33.744, 0) )
		Create_RifleAmmo( Vector(-800.802, 4712.485, -572.863), Angle(0, 308.384, 0) )
		Create_Armour5( Vector(-743.875, 4445.748, -623.969), Angle(0, 103.844, 0) )
		Create_Armour5( Vector(-744.003, 4446.267, -620.937), Angle(0, 103.844, 0) )
		Create_Armour5( Vector(-744.131, 4446.786, -617.906), Angle(0, 103.844, 0) )
		Create_Armour5( Vector(-744.259, 4447.304, -614.875), Angle(0, 103.844, 0) )
		Create_Medkit10( Vector(-770.156, 4442.957, -623.969), Angle(0, 82.284, 0) )
		Create_Medkit10( Vector(-790.241, 4463.312, -623.969), Angle(0, 63.584, 0) )
	end
	
	CreateTrigger( Vector(-2683.573,2999.897,-791.969), Vector(-47.228,-47.228,0), Vector(47.228,47.228,93.323), ITEM_BUFFER_2 )
	--[[ End of item buffer 2 trigger ]]
	
	--[[ Secret Button 1 Hint ]]
	CreateProp(Vector(-2817.187,2612.938,-910.25), Angle(0,0,0), "models/props_trainstation/tracksign10.mdl", true)
	
	--[[ Secret Button 1 function ]]
	local SEC_BUTTON_1 = function()
		Create_RevolverWeapon( Vector(-2936.624, 3023.35, -765.769), Angle(0, -170.431, -12.65) )
		Create_RevolverAmmo( Vector(-2957.545, 3017.4, -764.959), Angle(80, -119.172, -80.45) )
		Create_PortableMedkit( Vector(-2966.486, 2938.461, -767.933), Angle(0, 203.432, 0) )
		Create_GrenadeAmmo( Vector(-2964.307, 2968.419, -767.474), Angle(0, 177.032, 0) )
	end
	
	--[[ Create secret button 1 ]]
	AddSecretButton( Vector(-3207.969,3020.544,-736.898), SEC_BUTTON_1 )
	
	--[[ Secret Button 2 function ]]
	local SEC_BUTTON_2 = function()
	
		local SECB2_PROP1 = CreateProp(Vector(-8956.469,6531.25,-1679.281), Angle(90,89.956,180), "models/props_lab/blastdoor001c.mdl", true)
		local SECB2_PROP2 = CreateProp(Vector(-8957.469,6536.5,-1676.531), Angle(0,-90,0), "models/props_lab/blastdoor001c.mdl", true)
		local SECB2_PROP3 = CreateProp(Vector(-8955.594,6420.781,-1679.937), Angle(0.22,89.824,-0.044), "models/props_lab/blastdoor001c.mdl", true)
		local SECB2_PROP4 = CreateProp(Vector(-9045.937,6477.469,-1680.844), Angle(0,0,0), "models/props_lab/blastdoor001b.mdl", true)
		local SECB2_PROP5 = CreateProp(Vector(-8867.969,6480.063,-1678.562), Angle(0.527,-179.912,0.044), "models/props_lab/blastdoor001b.mdl", true)
		local SECB2_PROP6 = CreateProp(Vector(-8958.156,6424.313,-1564.75), Angle(90,-90,180), "models/props_lab/blastdoor001c.mdl", true)
	
		SECB2_PROP1:SetMaterial("models/effects/comball_tape")
		SECB2_PROP2:SetMaterial("models/effects/comball_tape")
		SECB2_PROP3:SetMaterial("models/effects/comball_tape")
		SECB2_PROP4:SetMaterial("models/effects/comball_tape")
		SECB2_PROP5:SetMaterial("models/effects/comball_tape")
		SECB2_PROP6:SetMaterial("models/effects/comball_tape")
		
		Create_CorzoHat( Vector(-8912.298, 6479.506, -1674.854), Angle(0, 181.207, 0) )
		
		CreateTeleporter( "SEC_B1", Vector(-8990.395,6479.639,-1674.847), "SEC_B2", Vector(157.674,4992.06,-189.823) )
		
	end
	
	--[[ Make secret button 2 ]]
	AddSecretButton( Vector(-89.969,4998.244,-168.759), SEC_BUTTON_2 )
	
	--[[ Items spawned on map start ]]
	Create_PistolWeapon( Vector(-3536.813, 1810.876, -2173.748), Angle(14.965, 93.931, -54.95) )
	Create_PistolAmmo( Vector(-3553.1, 1858.821, -2173.888), Angle(0.22, 62.556, 21.2) )
	Create_PortableMedkit( Vector(-2927.643, 1800.433, -2092.729), Angle(34.755, 140.139, 0) )
	Create_SMGWeapon( Vector(-3094.639, 2053.112, -2300.973), Angle(10.118, 20.686, 5.05) ) --[[ Secret 1 ]]
	Create_SMGAmmo( Vector(-4478.596, 2158.178, -1075.362), Angle(-79.997, 42.083, -50.75) )
	Create_SMGAmmo( Vector(-4491.046, 2154.787, -1074.37), Angle(-52.355, 118.531, 0) )
	Create_Medkit10( Vector(-3200.408, 1650.933, -1103.969), Angle(0, 291.025, 0) )
	Create_Medkit10( Vector(-3131.186, 1648.426, -1103.969), Angle(0, 243.725, 0) )
	Create_Medkit25( Vector(-2927.056, 2134.591, -1103.969), Angle(0, 183.004, 0) )
	Create_Armour( Vector(-3165.63, 1644.478, -1103.969), Angle(0, 264.924, 0) )
	Create_Armour5( Vector(-3197.352, 1956.609, -1283.969), Angle(0, 343.873, 0) )
	Create_Armour5( Vector(-3196.52, 1956.369, -1280.937), Angle(0, 343.873, 0) )
	Create_Armour5( Vector(-3195.688, 1956.128, -1277.906), Angle(0, 343.873, 0) )
	Create_Armour5( Vector(-3194.856, 1955.888, -1274.875), Angle(0, 343.873, 0) )
	Create_EldritchArmour( Vector(-2299.389, 3118.874, -1132.661), Angle(0, 266.712, 0) ) --[[ Secret 2 ]]
	Create_SuperShotgunWeapon( Vector(-2984.621, 2587.346, -933.502), Angle(17.627, 90.962, 95.4) )
	Create_ShotgunAmmo( Vector(-2978.99, 2599.513, -954.423), Angle(-80, 136.401, 0.3) )
	Create_GrenadeAmmo( Vector(-3116.526, 3083.893, -925.32), Angle(0, 167.657, 0) )
	Create_Beer( Vector(-2784.013, 3237.368, -1050.168), Angle(0, 269.507, 0) )
	Create_Beer( Vector(-3033.765, 3148.564, -1055.969), Angle(0, 62.847, 0) )
	Create_Beer( Vector(-3038.304, 2932.144, -1029.969), Angle(0, 82.647, 0) )
	Create_GrenadeAmmo( Vector(-2989.042, 2647.046, -1061.844), Angle(0, 181.056, 0) )
	Create_GrenadeAmmo( Vector(-2990.637, 2627.265, -1062.293), Angle(0, 234.956, 0) )
	Create_PortableMedkit( Vector(-2997.618, 2970.96, -1102.015), Angle(0, 101.715, 0) )
	Create_Medkit25( Vector(-3049.399, 2969.339, -1102.015), Angle(0, 89.395, 0) )
	Create_PistolAmmo( Vector(-3074.947, 3052.125, -1101.879), Angle(0, -44.149, 0) )
	Create_PistolAmmo( Vector(-3074.855, 3037.117, -1102.015), Angle(0, 0.075, 0) )
	Create_PistolWeapon( Vector(-3071.805, 3064.498, -1096.606), Angle(0, 8.871, 59.05) )
	Create_Armour100( Vector(-3018.715, 3219.927, -791.969), Angle(0, 54.935, 0) )
	Create_Medkit10( Vector(-3098.951, 2953.327, -753.969), Angle(0, 256.971, 0) )
	Create_ShotgunAmmo( Vector(-3111.894, 2875.229, -791.969), Angle(0, 74.071, 0) )
	Create_PistolWeapon( Vector(-2763.622, 2958.12, -791.969), Angle(0, 260.329, 0) )
	Create_Armour5( Vector(-2561.306, 3231.673, -895.969), Angle(0, 0.43, 0) )
	Create_Armour5( Vector(-2560.771, 3231.677, -892.937), Angle(0, 0.43, 0) )
	Create_Armour5( Vector(-2560.237, 3231.681, -889.906), Angle(0, 0.43, 0) )
	Create_Medkit25( Vector(-2553.34, 3292.878, -895.969), Angle(0, 320.39, 0) )
	Create_SMGAmmo( Vector(-2930.259, 3207.427, -791.969), Angle(0, 163.115, 0) )
	Create_RevolverAmmo( Vector(-3169.451, 3189.96, -938.545), Angle(79.997, -122.651, 0) )

	--[[ Make secret areas ]]
	CreateSecretArea( Vector(-3093.056,2052.844,-2296.437), Vector(-30.366,-30.366,0), Vector(30.366,30.366,13.555) )
	CreateSecretArea( Vector(-2283.617,3089.95,-1129.378), Vector(-34.684,-34.684,0), Vector(34.684,34.684,60.803) )
	CreateSecretArea( Vector(-3122.89,3276.873,-1068.262), Vector(-34.036,-34.036,0), Vector(34.036,34.036,85.04) )
	CreateSecretArea( Vector(336.426,5089.61,-507.437), Vector(-33.304,-33.304,0), Vector(33.304,33.304,57.469) )
	CreateSecretArea( Vector(363.785,-33.551,-1664), Vector(-46.61,-46.61,0), Vector(46.61,46.61,121.028) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)