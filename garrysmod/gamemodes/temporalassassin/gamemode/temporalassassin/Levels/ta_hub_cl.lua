
timer.Create("Training_PortalFX", 1, 0, function()
	ParticleEffect( "Training_Portal", Vector(-273.759,850.726,-13.969), Angle(0,0,0) )
end)

file.CreateDir("temporalassassin/levelload/")

SLEEP_SND = {
Sound("TemporalAssassin/Misc/Sleep1.wav"),
Sound("TemporalAssassin/Misc/Sleep2.wav")}

function HUB_DoRest()

	if not LocalPlayer():IsAdmin() then return end
	if REST_DELAY and CurTime() < REST_DELAY then return end
	
	REST_DELAY = CurTime() + 6
	GLOB_MUSIC_SILENCE = true
	GLOBAL_GRAMAPHONE_ON = false
	
	ForceCancelContractorMessages()
	
	LocalPlayer().HubSleep = UnPredictedCurTime() + 5
	LocalPlayer().HubSleepEyes_Lerp = 0
	LocalPlayer().HubSleepEyes = UnPredictedCurTime() + 5.5
	
	if LocalPlayer():HasWeapon("temporal_unarmed") then
		input.SelectWeapon( LocalPlayer():GetWeapon("temporal_unarmed") )
	end
	
	LocalPlayer():DisableControls(true)
	
	timer.Simple(0.5,function()
		LocalPlayer():EmitSound( table.Random(SLEEP_SND), 80, 100, 0.7, CHAN_STATIC )
	end)
	
	if GRAMAPHONE_MUSIC then
		GRAMAPHONE_MUSIC:FadeOut(0.2)
		GRAMAPHONE_MUSIC = nil
	end
	
	timer.Simple(4.5,function()
	
		net.Start("Reset_HubSleep")
		net.SendToServer()
	
	end)
	
	timer.Simple(6,function()
	
		GLOB_MUSIC_SILENCE = false
		LocalPlayer():DisableControls(false)
	
	end)

end

local COLOUR_BOX1 = Color(0,0,0,150)
local COLOUR_BOX2 = Color(5,5,5,240)
local COLOUR_RED = Color(240,140,140,255)
local COLOUR_GREEN = Color(100,255,100,255)
local COLOUR_GREY = Color(100,100,100,100)
local COLOUR_GREY2 = Color(200,200,200,100)
local COLOUR_BLUE = Color(150,242,250,255)
local COLOUR_BLUE2 = Color(20,20,100,255)
local COLOUR_HIGHLIGHT = Color(0,240,40,240)
local COLOUR_WHITE = Color(240,240,240,240)
local COLOUR_GREY3 = Color(220,220,220,200)

CHAPTERSELECT_LOADOUT_DATA = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_1"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_1"]["Health"] = 100

CHAPTERSELECT_LOADOUT_DATA["HL2_2"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_2"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_2"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_2"]["Ammo"] = {
{"fenic_ammo",100}}

CHAPTERSELECT_LOADOUT_DATA["HL2_3"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_3"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_3"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_3"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HL2_3"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_kingslayer",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["HL2_4"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_4"]["Health"] = 125
CHAPTERSELECT_LOADOUT_DATA["HL2_4"]["Armour"] = 125
CHAPTERSELECT_LOADOUT_DATA["HL2_4"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HL2_4"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["HL2_5"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_5"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_5"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_5"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HL2_5"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["HL2_6"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_6"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_6"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_6"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HL2_6"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["EP2_1"] = {}
CHAPTERSELECT_LOADOUT_DATA["EP2_1"]["Health"] = 100

CHAPTERSELECT_LOADOUT_DATA["EP2_2"] = {}
CHAPTERSELECT_LOADOUT_DATA["EP2_2"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["EP2_2"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["EP2_2"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",120}}
CHAPTERSELECT_LOADOUT_DATA["EP2_2"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle"}

CHAPTERSELECT_LOADOUT_DATA["EP2_3"] = {}
CHAPTERSELECT_LOADOUT_DATA["EP2_3"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["EP2_3"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["EP2_3"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["EP2_3"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["EP2_4"] = {}
CHAPTERSELECT_LOADOUT_DATA["EP2_4"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["EP2_4"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["EP2_4"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["EP2_4"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["EP2_5"] = {}
CHAPTERSELECT_LOADOUT_DATA["EP2_5"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["EP2_5"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["EP2_5"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"harbinger_ammo",400},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["EP2_5"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["HLS_1"] = {}
CHAPTERSELECT_LOADOUT_DATA["HLS_1"]["Health"] = 100

CHAPTERSELECT_LOADOUT_DATA["HLS_2"] = {}
CHAPTERSELECT_LOADOUT_DATA["HLS_2"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_2"]["Armour"] = 0
CHAPTERSELECT_LOADOUT_DATA["HLS_2"]["Ammo"] = {
{"fenic_ammo",100},
{"wolf_ammo",6}}
CHAPTERSELECT_LOADOUT_DATA["HLS_2"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver"}

CHAPTERSELECT_LOADOUT_DATA["HLS_3"] = {}
CHAPTERSELECT_LOADOUT_DATA["HLS_3"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_3"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_3"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",120},
{"crossbow_ammo",10},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HLS_3"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_smg",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["HLS_4"] = {}
CHAPTERSELECT_LOADOUT_DATA["HLS_4"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_4"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_4"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"crossbow_ammo",10},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HLS_4"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["HLS_5"] = {}
CHAPTERSELECT_LOADOUT_DATA["HLS_5"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_5"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_5"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"crossbow_ammo",10},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HLS_5"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["HLS_6"] = {}
CHAPTERSELECT_LOADOUT_DATA["HLS_6"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_6"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_6"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HLS_6"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["HLS_7"] = {}
CHAPTERSELECT_LOADOUT_DATA["HLS_7"]["Health"] = 47

CHAPTERSELECT_LOADOUT_DATA["HLS_8"] = {}
CHAPTERSELECT_LOADOUT_DATA["HLS_8"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_8"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_8"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HLS_8"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["HLS_9"] = {}
CHAPTERSELECT_LOADOUT_DATA["HLS_9"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_9"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HLS_9"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"harbinger_ammo",400},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HLS_9"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner",
"temporal_anthrakia"}

CHAPTERSELECT_LOADOUT_DATA["STRIDER_1"] = {}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_1"]["Health"] = 100

CHAPTERSELECT_LOADOUT_DATA["STRIDER_2"] = {}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_2"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_2"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_2"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_2"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg"}

CHAPTERSELECT_LOADOUT_DATA["STRIDER_3"] = {}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_3"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_3"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_3"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_3"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["STRIDER_4"] = {}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_4"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_4"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_4"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"harbinger_ammo",400},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_4"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["STRIDER_5"] = {}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_5"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_5"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_5"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"harbinger_ammo",400},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"frontliner_ammo",3},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_5"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["STRIDER_6"] = {}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_6"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_6"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_6"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"harbinger_ammo",400},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_6"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner",
"temporal_anthrakia"}

CHAPTERSELECT_LOADOUT_DATA["STRIDER_7"] = {}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_7"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_7"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["STRIDER_7"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"harbinger_ammo",400},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["STRIDER_7"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_anthrakia"}

CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_1"] = {}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_1"]["Health"] = 100

CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_2"] = {}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_2"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_2"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_2"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"crossbow_ammo",10},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_2"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_3"] = {}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_3"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_3"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_3"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_3"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_4"] = {}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_4"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_4"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_4"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_4"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_5"] = {}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_5"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_5"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_5"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"harbinger_ammo",400},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_5"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_6"] = {}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_6"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_6"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_6"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"harbinger_ammo",400},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_6"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_anthrakia"}

CHAPTERSELECT_LOADOUT_DATA["COSMO_1"] = {}
CHAPTERSELECT_LOADOUT_DATA["COSMO_1"]["Health"] = 100

CHAPTERSELECT_LOADOUT_DATA["COSMO_2"] = {}
CHAPTERSELECT_LOADOUT_DATA["COSMO_2"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["COSMO_2"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["COSMO_2"]["Ammo"] = {
{"photoop_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["COSMO_2"]["Weapons"] = {
"temporal_photoop",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg"}

CHAPTERSELECT_LOADOUT_DATA["COSMO_3"] = {}
CHAPTERSELECT_LOADOUT_DATA["COSMO_3"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["COSMO_3"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["COSMO_3"]["Ammo"] = {
{"photoop_ammo",200},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["COSMO_3"]["Weapons"] = {
"temporal_falanran",
"temporal_photoop",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner",
"temporal_anthrakia"}

CHAPTERSELECT_LOADOUT_DATA["COUNTRY_1"] = {}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_1"]["Health"] = 100

CHAPTERSELECT_LOADOUT_DATA["COUNTRY_2"] = {}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_2"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_2"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_2"]["Ammo"] = {
{"griseus_ammo",90},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_2"]["Weapons"] = {
"temporal_griseus",
"temporal_revolver",
"temporal_supershotgun"}

CHAPTERSELECT_LOADOUT_DATA["COUNTRY_3"] = {}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_3"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_3"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_3"]["Ammo"] = {
{"griseus_ammo",90},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_3"]["Weapons"] = {
"temporal_griseus",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["COUNTRY_4"] = {}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_4"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_4"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_4"]["Ammo"] = {
{"griseus_ammo",90},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_4"]["Weapons"] = {
"temporal_griseus",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["COUNTRY_5"] = {}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_5"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_5"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_5"]["Ammo"] = {
{"griseus_ammo",90},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_5"]["Weapons"] = {
"temporal_griseus",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["COUNTRY_6"] = {}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_6"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_6"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_6"]["Ammo"] = {
{"griseus_ammo",90},
{"wolf_ammo",12},
{"m6g_ammo",18},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["COUNTRY_6"]["Weapons"] = {
"temporal_griseus",
"temporal_revolver",
"temporal_m6g",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_harbinger",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_DATA = {}
CHAPTERSELECT_DATA["Half Life 2"] = {
{"Campaign Start", "d1_trainstation_01", CHAPTERSELECT_LOADOUT_DATA["HL2_1"]},
{"Canals", "d1_canals_01", CHAPTERSELECT_LOADOUT_DATA["HL2_2"]},
{"Ravenholm", "d1_town_01", CHAPTERSELECT_LOADOUT_DATA["HL2_3"]},
{"Coast", "d2_coast_01", CHAPTERSELECT_LOADOUT_DATA["HL2_4"]},
{"Nova Prospekt", "d2_prison_01", CHAPTERSELECT_LOADOUT_DATA["HL2_5"]},
{"City 17", "d3_c17_01", CHAPTERSELECT_LOADOUT_DATA["HL2_6"]}}

CHAPTERSELECT_DATA["Half Life 2: Episode 2"] = {
{"Campaign Start", "ep2_outland_01", CHAPTERSELECT_LOADOUT_DATA["EP2_1"]},
{"Antlion Defence", "ep2_outland_02", CHAPTERSELECT_LOADOUT_DATA["EP2_2"]},
{"Alyx Awakens", "ep2_outland_05", CHAPTERSELECT_LOADOUT_DATA["EP2_3"]},
{"Helicopter Chase", "ep2_outland_08", CHAPTERSELECT_LOADOUT_DATA["EP2_4"]},
{"White Forest", "ep2_outland_11", CHAPTERSELECT_LOADOUT_DATA["EP2_5"]}}

CHAPTERSELECT_DATA["Half Life Source"] = {
{"Campaign Start", "hls01amrl", CHAPTERSELECT_LOADOUT_DATA["HLS_1"]},
{"Unforseen Consequences", "hls02amrl", CHAPTERSELECT_LOADOUT_DATA["HLS_2"]},
{"Office Complex", "hls03amrl", CHAPTERSELECT_LOADOUT_DATA["HLS_3"]},
{"We've Got Hostiles", "hls04amrl", CHAPTERSELECT_LOADOUT_DATA["HLS_4"]},
{"Blast Pit", "hls05bmrl", CHAPTERSELECT_LOADOUT_DATA["HLS_5"]},
{"On A Rail", "hls07amrl", CHAPTERSELECT_LOADOUT_DATA["HLS_6"]},
{"Residue Processing", "hls09amrl", CHAPTERSELECT_LOADOUT_DATA["HLS_7"]},
{"Forget About Freeman", "hls12amrl", CHAPTERSELECT_LOADOUT_DATA["HLS_8"]},
{"Xen", "hls14amrl", CHAPTERSELECT_LOADOUT_DATA["HLS_9"]}}

CHAPTERSELECT_DATA["Strider Mountain"] = {
{"Campaign Start", "1_sm_off_course", CHAPTERSELECT_LOADOUT_DATA["STRIDER_1"]},
{"Propaganda", "3_sm_propaganda", CHAPTERSELECT_LOADOUT_DATA["STRIDER_2"]},
{"Outpost", "5_sm_outpost", CHAPTERSELECT_LOADOUT_DATA["STRIDER_3"]},
{"Ascent", "7_sm_ascent", CHAPTERSELECT_LOADOUT_DATA["STRIDER_4"]},
{"Crevasse", "9_sm_crevasse", CHAPTERSELECT_LOADOUT_DATA["STRIDER_5"]},
{"Depth Charged", "11_sm_depth_charged", CHAPTERSELECT_LOADOUT_DATA["STRIDER_6"]},
{"Descent", "14_sm_descent", CHAPTERSELECT_LOADOUT_DATA["STRIDER_7"]}}

CHAPTERSELECT_DATA["Offshore"] = {
{"Campaign Start", "islandescape", CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_1"]},
{"Cove", "islandcove", CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_2"]},
{"Lockdown", "islandlockdown", CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_3"]},
{"Underground", "islandunderground", CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_4"]},
{"Overwhelmed", "islandoverwhelmed", CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_5"]},
{"Plant", "islandplant", CHAPTERSELECT_LOADOUT_DATA["OFFSHORE_6"]}}

CHAPTERSELECT_DATA["Cosmonaut"] = {
{"Campaign Start", "cosmonaut02", CHAPTERSELECT_LOADOUT_DATA["COSMO_1"]},
{"Asylum", "cosmonaut04", CHAPTERSELECT_LOADOUT_DATA["COSMO_2"]},
{"Industrialisation", "cosmonaut06", CHAPTERSELECT_LOADOUT_DATA["COSMO_3"]}}

CHAPTERSELECT_DATA["Silesia Country"] = {
{"Campaign Start", "01_uvalno_villa", CHAPTERSELECT_LOADOUT_DATA["COUNTRY_1"]},
{"Roadway", "05_uvalno_roadway", CHAPTERSELECT_LOADOUT_DATA["COUNTRY_2"]},
{"Pension", "08_ov_pension", CHAPTERSELECT_LOADOUT_DATA["COUNTRY_3"]},
{"Robinson", "10_out_robinson", CHAPTERSELECT_LOADOUT_DATA["COUNTRY_4"]},
{"Night Return", "13_uvalno_night", CHAPTERSELECT_LOADOUT_DATA["COUNTRY_5"]},
{"Villa", "15_uvalno_villa", CHAPTERSELECT_LOADOUT_DATA["COUNTRY_6"]}}

CAMPAIGN_DATA = {}
CAMPAIGN_DATA[1] = {"Half Life 2", IsMounted("hl2"), "d1_trainstation_01", "A Contractor has tasked us with fixing a paradoxical event that would change history if gone unchecked, take your mantle and put yourself in his shoes and go through his hardships to fix it.", "Requires Half-Life 2 mounted to play", {"Agency/Mthuulra",Color(50,50,50,255)}}
CAMPAIGN_DATA[2] = {"Half Life 2: Episode 1", IsMounted("episodic"), "ep1_c17_00", "A continuation of our previous endeavour, the same man has seemingly dissapeared into what was mentioned as 'Stasis', step back into his shoes and continue.", "Requires Half-Life 2: Episode 1 mounted to play", {"Agency/Mthuulra",Color(50,50,50,255)}}
CAMPAIGN_DATA[3] = {"Half Life 2: Episode 2", IsMounted("ep2"), "ep2_outland_01", "The final stretch for this paradoxical event, it should be no problem for you with what you've faced thus far.", "Requires Half-Life 2: Episode 2 mounted to play", {"Agency/Mthuulra",Color(50,50,50,255)}}
CAMPAIGN_DATA[4] = {"Half Life Source", ((IsMounted("hl1") or IsMounted("hl1mp")) and file.Exists("maps/hls01amrl.bsp","GAME")), "hls01amrl", "It seems Mr. Freeman called in sick during this time line and as such has stopped an important experiment, Put on the mantle once more and go through hell to make up for it.", "Requires Half-Life: Source mounted and HL:S Resized downloaded to play", {"Agency/Mthuulra",Color(50,50,50,255)}}
CAMPAIGN_DATA[5] = {"Strider Mountain", file.Exists("maps/1_sm_off_course.bsp","GAME"), "1_sm_off_course", "Once again it seems our hazard suit wearing friend has gotten himself into trouble on another job and is stuck in stasis licking his wounds, You know what to do.", "Requires Half-Life 2 mounted and Strider Mountain content to play", {"Agency/Mthuulra",Color(50,50,50,255)}}
CAMPAIGN_DATA[6] = {"Offshore", (IsMounted("ep2") and file.Exists("maps/islandescape.bsp","GAME")), "islandescape", "This is an odd one, it seems that our mutual friend has a task that 'Only you can handle', must be worth your while.", "Requires Half-Life 2: Episode 2 mounted and Offshore content to play", {"Agency/Mthuulra",Color(50,50,50,255)}}
CAMPAIGN_DATA[7] = {"Cosmonaut", (IsMounted("ep2") and file.Exists("maps/cosmonaut02.bsp","GAME")), "cosmonaut02", "Not the usual contractor this time, a pair of twins asked us to get a space shuttle ready, for what reasons we where not told, but they paid up front so it's up to you now.", "Requires Half-Life 2: Episode 2 mounted and Cosmonaut content to play", {"The Twins",Color(255,150,0,255)}}
CAMPAIGN_DATA[8] = {"Silesia Country", (file.Exists("maps/01_uvalno_villa.bsp","GAME")), "01_uvalno_villa", "We got a note directed straight towards you, It reads 'I think you deserve a break, a vacation if you will. Why not take a load off for a while at this little spot I picked out, everything should be alright, I think...' - Griseus", "Requires Half-Life 2 mounted and Silesia Country content to play", {"Griseus",Color(0,100,255,255)}}
CAMPAIGN_DATA[9] = {"Minerva: Metastasis", (file.Exists("maps/metastasis_1.bsp","GAME")), "metastasis_1", "A rouge agent was killed doing an important mission for something rather world changing, honour a dead mans wishes and finish the job yourself.", "Requires Half-Life 2 mounted and MINERVA: METASTASIS content to play", {"The Twins",Color(255,150,0,255)}}
CAMPAIGN_COUNT = 9

SIDEMISSION_DATA = {}
SIDEMISSION_COUNT = 0

SINHUNTER_DATA = {}
SINMISSION_COUNT = 0

CHRONOLOGICALLIST_DATA = {}
CHRONOLOGICALLIST_DATA[1] = {"The Hub", true, "Kit gets ready to do what she was assigned to do, her first job and her first lot of fun out in the field after getting her new coat. The old one wasn't quite cutting it."}
CHRONOLOGICALLIST_DATA[2] = {"Training", true, "Kit meets B in the training area that he conjured up for her to practise in. This is her first run in with B as he was contracted to train Kit by Mthuulra."}
CHRONOLOGICALLIST_DATA[3] = {"Half Life 2", IsMounted("hl2"), "Kits first assignment, Gordon went missing for some reason and The Agency thinks something's up. Replace the one freeman until this can get sorted."}
CHRONOLOGICALLIST_DATA[4] = {"Half Life 2: Episode 1", IsMounted("episodic"), "Kits first assignment, Gordon went missing for some reason and The Agency thinks something's up. Replace the one freeman until this can get sorted."}
CHRONOLOGICALLIST_DATA[5] = {"Half Life 2: Episode 2", IsMounted("ep2"), "Kits first assignment, Gordon went missing for some reason and The Agency thinks something's up. Replace the one freeman until this can get sorted."}
CHRONOLOGICALLIST_DATA[6] = {"Half Life Source", ((IsMounted("hl1") or IsMounted("hl1mp")) and file.Exists("maps/hls01amrl.bsp","GAME")), "Kits second assignment, after fixing Gordons dissapearence it seems the timeline decided to break at a much earlier point in time. Kit is sent in to fix it once again."}
CHRONOLOGICALLIST_DATA[7] = {"Challenge #1 (Story)", (file.Exists("maps/misc_elderthrone.bsp","GAME")), "After finding out a bit more about Mthuulra, Kit finds herself in front of a large tower hidden behind the sun. Mthuulra tells her to go back home and leave this place, but what choice does she have if she wishes to uncover more?"}
CHRONOLOGICALLIST_DATA[8] = {"Forest Train", (IsMounted("ep2") and file.Exists("maps/foresttrain.bsp","GAME")), "An unknown person contracts Kit for a short mission that seems lucrative, Kit would soon get to know the person after this contract as Griseus."}
CHRONOLOGICALLIST_DATA[9] = {"Cosmonaut", (IsMounted("ep2") and file.Exists("maps/cosmonaut02.bsp","GAME")), "Two new faces contract Kit to help get a Space Shuttle ready for launch, The two that contracted her paid up front so not many questions where asked, Kit would learn more about the Twins after this contract."}
CHRONOLOGICALLIST_DATA[10] = {"Year Long Alarm", (IsMounted("ep2") and file.Exists("maps/yla_mine.bsp","GAME")), "Griseus has a small little side job for Kit in order to distract the combine and open up a time-frame for the one Orange Suited Free-Man to slip by."}
CHRONOLOGICALLIST_DATA[11] = {"Silesia Country", (file.Exists("maps/01_uvalno_villa.bsp","GAME")), "A sort of vacation gone wrong, Griseus meant well but not everything went as planned."}
CHRONOLOGICALLIST_DATA[12] = {"Offshore", (IsMounted("ep2") and file.Exists("maps/islandescape.bsp","GAME")), "A Mutual Friend finds that only Kit can handle this sort of thing, a misplaced person in the wrong place at the wrong time. Kit takes their role and completes the task."}
CHRONOLOGICALLIST_DATA[13] = {"Entropy Zero", (IsMounted("ep2") and file.Exists("maps/az_c1_1.bsp","GAME")), "Griseus sets out a contract to challenge Kits character, to see if she's willing to fight for the 'Bad Guys'."}
CHRONOLOGICALLIST_DATA[14] = {"Minerva: Metastasis", (file.Exists("maps/metastasis_1.bsp","GAME")), "A Rogue agent was killed doing an important mission for something rather world changing, Kit is sent in to finish the job in place of them."}
CHRONOLOGICALLIST_DATA[15] = {"Downfall", (IsMounted("ep2") and file.Exists("maps/dwn01.bsp","GAME")), "There were several reports of an unknown anomaly being active inside the combine-infested mines at an abandoned train station. Kit is contracted by a new face to get the job done and she would learn more about Akane after this contract."}
CHRONOLOGICALLIST_DATA[16] = {"Precursor", (IsMounted("ep2") and file.Exists("maps/r_map1.bsp","GAME")), "Romanov, a man that was supposed to not get captured by the initial raid on his civil housing block was instead captured, Griseus has a contract for you to go in and fix the time line, but something feels a little off."}
CHRONOLOGICALLIST_DATA[17] = {"Spherical Nightmares", (IsMounted("ep2") and file.Exists("maps/sn_level01a.bsp","GAME")), "The Twins cooperate with Malum and have another job for Kit, A combine containment facility is showing strange behaviour and Kit is sent into take care of it hands-on. Kit learns a bit more about Malum during and after this contract."}
CHRONOLOGICALLIST_DATA[18] = {"Challenge #2 (Story)", (file.Exists("maps/misc_elderthrone.bsp","GAME")), "After learning more about Malum, Kit finds herself on the strange tower behind the sun. Tasked by Mthuulra to 'Entertain her' by descending the tower and slaughtering thousands."}
CHRONOLOGICALLIST_DATA[19] = {"False Destination", (IsMounted("ep2") and file.Exists("maps/d1_b1.bsp","GAME")), "Griseus takes on a job all by themself, soon learning that Kits job and in summary her abilities don't just come naturally. Griseus soon looking for an out as soon as possible."}
CHRONOLOGICALLIST_DATA[20] = {"The Citizen", (IsMounted("ep2") and file.Exists("maps/thecitizen_part1.bsp","GAME")), "A note is left at Kits apartment signed by B, The note states that this contract is confidential and only Kit and Mthuulra know about it, Kit sets off to find out what it's all about and on her way through she learns much more about B."}
CHRONOLOGICALLIST_DATA[21] = {"Bear Party", (file.Exists("maps/bear_e1_m1.bsp","GAME")), "Malum tells Kit about this contract, a certain dimension is showing instability and strange oddities. Upon Kits arrival she can only put her hand to her head in grief at the sights she saw. Kit learns more about Mthuulra and Malum after this contract."}
CHRONOLOGICALLIST_DATA[22] = {"Mission Improbable", (IsMounted("ep2") and file.Exists("maps/mimp1.bsp","GAME")), "Something about an improbable mission, a mission that is improbable. Kit is sent in to take care of business as usual."}
CHRONOLOGICALLIST_DATA[23] = {"Old School", (file.Exists("maps/lwr.bsp","GAME")), "B sends Kit another note, simply stating 'a hunch that something is wrong' and that he needs her help. What entails is a long journey to try and stop one of the biggest, most dangerous threats yet. Kit learns more about B during this contract and even meets a new face."}
CHRONOLOGICALLIST_DATA[24] = {"Exhumed/Power Slave", (file.Exists("maps/exhumed_set_arena.bsp","GAME")), "Corzo calls upon Kits help for a matter he's having a slight problem with, something of his has gone missing and he knows just where to find it, only problem is he's somehow being blocked from entering the plane where it resides."}
CHRONOLOGICALLIST_DATA[25] = {"Coastline 2 Atmosphere", (file.Exists("maps/leonhl2-2.bsp","GAME")), "A strange presence fills the air that Kit can feel, she has been notified of her fathers strange return and it's up to her to set it right. Kit learns more about her father, Malum and Akane during this contract."}
CHRONOLOGICALLIST_DATA[26] = {"Challenge #3 (Story)", (file.Exists("maps/misc_elderthrone.bsp","GAME")), "Kit finds herself once again inside the strange tower behind the sun, no challenge is set by Mthuulra, however Malums presence can be felt in the air as well. What was going on?"}
CHRONOLOGICALLIST_DATA[27] = {"Blasted Lands", (IsMounted("ep2") and file.Exists("maps/bl1.bsp","GAME")), "Griseus sets out on their own once again with some important data to transmit to a friend, their journey is rough, long and ultimately comes to an unknown close leaving Griseus's where-abouts unknown."}
CHRONOLOGICALLIST_DATA[28] = {"The Abyss (Story)", (file.Exists("maps/abyss.bsp","GAME"))}
CHRONOLOGICALLIST_DATA[29] = {"Astral Reflection (Story)", (file.Exists("maps/astral_apartment.bsp","GAME"))}
CHRONOLOGICALLIST_COUNT = 29

function AddCampaignMission( name, mountcheck, map, desc, faildesc, contractdata, fileappend )

	if not name then
		print("No name was passed, cancelling")
	return end
	
	if not mountcheck then
		print("No mount check was passed, just set to true if theres no needed resources")
	return end
	
	if not map then
		print("No map start name was passed")
	return end
	
	if not desc then
		print("No description was passed")
	return end
	
	if not faildesc then
		print("No fail description was passed, this shows if you dont have mount check return true")
	return end
	
	if not contractdata then
		print("No contract data set")
	return end
	
	if not istable(contractdata) then
		print("Contract data setup is wrong, try again")
	return end
	
	if not fileappend then
		fileappend = false
	end

	CAMPAIGN_COUNT = CAMPAIGN_COUNT + 1
	CAMPAIGN_DATA[CAMPAIGN_COUNT] = { name, mountcheck, map, desc, faildesc, contractdata, fileappend }

end

function AddSideMission( name, mountcheck, map, desc, faildesc, contractdata, fileappend )

	if not name then
		print("No name was passed, cancelling")
	return end
	
	if not mountcheck then
		print("No mount check was passed, just set to true if theres no needed resources")
	return end
	
	if not map then
		print("No map start name was passed")
	return end
	
	if not desc then
		print("No description was passed")
	return end
	
	if not faildesc then
		print("No fail description was passed, this shows if you dont have mount check return true")
	return end
	
	if not contractdata then
		print("No contract data set")
	return end
	
	if not istable(contractdata) then
		print("Contract data setup is wrong, try again")
	return end
	
	if not fileappend then
		fileappend = false
	end

	SIDEMISSION_COUNT = SIDEMISSION_COUNT + 1
	SIDEMISSION_DATA[SIDEMISSION_COUNT] = { name, mountcheck, map, desc, faildesc, contractdata, fileappend }

end

function AddSinHunterMission( name, mountcheck, map, desc, faildesc, contractdata, fileappend )

	if not name then
		print("No name was passed, cancelling")
	return end
	
	if not mountcheck then
		print("No mount check was passed, just set to true if theres no needed resources")
	return end
	
	if not map then
		print("No map start name was passed")
	return end
	
	if not desc then
		print("No description was passed")
	return end
	
	if not faildesc then
		print("No fail description was passed, this shows if you dont have mount check return true")
	return end
	
	if not contractdata then
		print("No contract data set")
	return end
	
	if not istable(contractdata) then
		print("Contract data setup is wrong, try again")
	return end
	
	if not fileappend then
		fileappend = false
	end

	SINMISSION_COUNT = SINMISSION_COUNT + 1
	SINHUNTER_DATA[SINMISSION_COUNT] = { name, mountcheck, map, desc, faildesc, contractdata, fileappend }

end

local HUB_CAMPAIGN_STARTING = false

CAMPAIGN_SELECT_SCROLL = 0

function HUB_CampaignSelect_LIB()

	local PL = LocalPlayer()
	
	if GAME_OVER_ENABLED then return end
	if SUIT_LOCKER_ENABLED then return end
	if PL.EasyBind_SettingUp then return end
	if PL.GameGuide_Open then return end
	if HUB_CAMPAIGN_STARTING then return end
	
	CAMPAIGN_SELECT_SCROLL = 0
	local AM = net.ReadFloat()
	
	PL.HUB_CAMPAIGN_SELECT = (AM or true)
	PL.HUB_CAMPAIGN_MOUSE = true
	gui.EnableScreenClicker(true)
	NoDrawCursor(false)
	HUDParticles_Reset()

end
net.Receive("HUB_CampaignSelect",HUB_CampaignSelect_LIB)

local D_COL = Color(0,0,0,0)

local HUB_SHITTER = {}
HUB_SHITTER[1] = Color(250,250,250,255)
HUB_SHITTER[2] = Color(100,200,100,255)
HUB_SHITTER[3] = Color(200,200,200,255)
HUB_SHITTER[4] = Color(220,240,172,255)
HUB_SHITTER[5] = Color(0,240,0,255)
HUB_SHITTER[6] = Color(100,255,100,255)
HUB_SHITTER[7] = Color(0,0,240,255)

function DRAW_CAMPAIGN_SELECT4( PL )

	if not PL.HUB_CAMPAIGN_MOUSE then
		PL.HUB_CAMPAIGN_MOUSE = true
		gui.EnableScreenClicker(true)
		NoDrawCursor(false)
		HUDParticles_Reset()
	end
	
	if gui.IsGameUIVisible() then
		PL.HUB_CAMPAIGN_SELECT = false
	end

	draw.RoundedBox( 4, GUI_X2(18), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
	draw.RoundedBox( 4, GUI_X2(20), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
	
	draw.SimpleText( "- Chronological Order List -", "HUD_Numbers", GUI_X2(116), GUI_Y(28), HUB_SHITTER[1], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	local CAMP_Y = 0
	local MOUSE_CLICK = MousePressed("OrderMission_Press")
	local WL_X = GUI_X2(119)
	local MAX_COUNTER = CHRONOLOGICALLIST_COUNT
	
	if MAX_COUNTER > 13 then
	
		if CAMPAIGN_SELECT_SCROLL < (MAX_COUNTER-13) then
	
			if MouseInside2( WL_X - GUI_X(85), GUI_Y(399), WL_X - GUI_X(65), GUI_Y(424) ) then
				if MOUSE_CLICK then
					CAMPAIGN_SELECT_SCROLL = math.Clamp(CAMPAIGN_SELECT_SCROLL + 1,0,MAX_COUNTER-13)
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(119,121), 0.6, CHAN_STATIC )
				end
				draw.SimpleText( [[\]], "LORE_Title", WL_X - GUI_X(80), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X - GUI_X(70), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				draw.SimpleText( [[\]], "LORE_Title", WL_X - GUI_X(80), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X - GUI_X(70), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
		
		if CAMPAIGN_SELECT_SCROLL > 0 then
			
			if MouseInside2( WL_X + GUI_X(65), GUI_Y(399), WL_X + GUI_X(85), GUI_Y(424) ) then
				if MOUSE_CLICK then
					CAMPAIGN_SELECT_SCROLL = math.Clamp(CAMPAIGN_SELECT_SCROLL - 1,0,MAX_COUNTER-13)
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(119,121), 0.6, CHAN_STATIC )
				end
				draw.SimpleText( [[\]], "LORE_Title", WL_X + GUI_X(80), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X + GUI_X(70), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				draw.SimpleText( [[\]], "LORE_Title", WL_X + GUI_X(80), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X + GUI_X(70), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
	end
	
	for loop = 1+CAMPAIGN_SELECT_SCROLL,13+CAMPAIGN_SELECT_SCROLL do
	
		local DATA = CHRONOLOGICALLIST_DATA[loop]
		
		if DATA then
			
			CAMP_Y = CAMP_Y + 25
			D_COL:SetUnpacked(250,250,250,255)
			local CAN_PICK = true
			
			if not DATA[2] then
				D_COL:SetUnpacked(250,0,0,255)
				CAN_PICK = false
			end
			
			local C_X, C_Y = 116, (40 + CAMP_Y)
			
			if not CAN_PICK and MouseInside2( GUI_X2(C_X - 70), GUI_Y(C_Y - 10), GUI_X2(C_X + 70), GUI_Y(C_Y + 10) ) then
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), COLOUR_BLUE2, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if DATA[3] then
				
					local EXT_Y = 0
					local TEXT_TABLE = WrapText( DATA[3], 32 )
					
					draw.RoundedBox( 4, GUI_X2(224), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
					draw.RoundedBox( 4, GUI_X2(226), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
					
					for _,v in pairs (TEXT_TABLE) do
						EXT_Y = EXT_Y + 16
						draw.SimpleText( v, "HUD_Numbers", GUI_X2(326), GUI_Y(20 + EXT_Y), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					end
				
				end
			elseif CAN_PICK and MouseInside2( GUI_X2(C_X - 70), GUI_Y(C_Y - 10), GUI_X2(C_X + 70), GUI_Y(C_Y + 10) ) then
			
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), HUB_SHITTER[5], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if DATA[3] then
				
					local EXT_Y = 0
					local TEXT_TABLE = WrapText( DATA[3], 32 )
					
					draw.RoundedBox( 4, GUI_X2(224), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
					draw.RoundedBox( 4, GUI_X2(226), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
					
					for _,v in pairs (TEXT_TABLE) do
						EXT_Y = EXT_Y + 16
						draw.SimpleText( v, "HUD_Numbers", GUI_X2(326), GUI_Y(20 + EXT_Y), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					end
				
				end
				
				if DATA[6] then
				
					local TXT = DATA[6][1]
					local COL = DATA[6][2]
					local ALPHA = math.sin(UnPredictedCurTime()*20)*50 + 200
					
					draw.SimpleText( "- Contractor -", "HUD_Numbers", GUI_X2(326), GUI_Y(396), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					draw.SimpleText( TXT, "HUD_Numbers", GUI_X2(326), GUI_Y(415), Color(COL.r,COL.g,COL.b,ALPHA), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				end
				
			else
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), D_COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
	end
	
	local A1,A2 = 116, 400
	local SINE = math.abs(math.sin(UnPredictedCurTime()*10)*55)
	
	draw.SimpleText( "This lists the 'Time-Line' of missions in TA", "HUD_Text", GUI_X2(A1), GUI_Y(A2+12), Color(200 + SINE,240 - SINE,240 - SINE,140), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	draw.SimpleText( "Red missions are ones you do not have installed", "HUD_Text", GUI_X2(A1), GUI_Y(A2+22), Color(200 + SINE,240 - SINE,240 - SINE,140), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	if MouseInside2( GUI_X2(A1 - 15), GUI_Y(A2 - 8), GUI_X2(A1 + 15), GUI_Y(A2 + 8) ) then
	
		draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_HIGHLIGHT, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
		if MOUSE_CLICK == 1 then
			PL.HUB_CAMPAIGN_SELECT = false
		end
		
	else
		draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_WHITE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

function DRAW_CAMPAIGN_SELECT3( PL )

	if not PL.HUB_CAMPAIGN_MOUSE then
		PL.HUB_CAMPAIGN_MOUSE = true
		gui.EnableScreenClicker(true)
		NoDrawCursor(false)
		HUDParticles_Reset()
	end
	
	if gui.IsGameUIVisible() then
		PL.HUB_CAMPAIGN_SELECT = false
	end

	draw.RoundedBox( 4, GUI_X2(18), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
	draw.RoundedBox( 4, GUI_X2(20), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
	
	draw.SimpleText( "- Select a place to Hunt -", "HUD_Numbers", GUI_X2(116), GUI_Y(28), HUB_SHITTER[1], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	local CAMP_Y = 0
	local MOUSE_CLICK = MousePressed("SinMission_Press")
	local WL_X = GUI_X2(119)
	local MAX_COUNTER = SINMISSION_COUNT
	
	if MAX_COUNTER > 13 then
	
		if CAMPAIGN_SELECT_SCROLL < (MAX_COUNTER-13) then
	
			if MouseInside2( WL_X - GUI_X(85), GUI_Y(399), WL_X - GUI_X(65), GUI_Y(424) ) then
				if MOUSE_CLICK then
					CAMPAIGN_SELECT_SCROLL = math.Clamp(CAMPAIGN_SELECT_SCROLL + 1,0,MAX_COUNTER-13)
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(119,121), 0.6, CHAN_STATIC )
				end
				draw.SimpleText( [[\]], "LORE_Title", WL_X - GUI_X(80), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X - GUI_X(70), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				draw.SimpleText( [[\]], "LORE_Title", WL_X - GUI_X(80), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X - GUI_X(70), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
		
		if CAMPAIGN_SELECT_SCROLL > 0 then
			
			if MouseInside2( WL_X + GUI_X(65), GUI_Y(399), WL_X + GUI_X(85), GUI_Y(424) ) then
				if MOUSE_CLICK then
					CAMPAIGN_SELECT_SCROLL = math.Clamp(CAMPAIGN_SELECT_SCROLL - 1,0,MAX_COUNTER-13)
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(119,121), 0.6, CHAN_STATIC )
				end
				draw.SimpleText( [[\]], "LORE_Title", WL_X + GUI_X(80), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X + GUI_X(70), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				draw.SimpleText( [[\]], "LORE_Title", WL_X + GUI_X(80), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X + GUI_X(70), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
	end
	
	for loop = 1+CAMPAIGN_SELECT_SCROLL,13+CAMPAIGN_SELECT_SCROLL do
	
		local DATA = SINHUNTER_DATA[loop]
		
		if DATA then
			
			CAMP_Y = CAMP_Y + 25
			D_COL:SetUnpacked(250,250,250,255)
			local CAN_PICK = true
			
			if not DATA[2] then
				D_COL:SetUnpacked(250,0,0,255)
				CAN_PICK = false
			end
			
			local C_X, C_Y = 116, (40 + CAMP_Y)
			
			if not CAN_PICK and MouseInside2( GUI_X2(C_X - 70), GUI_Y(C_Y - 10), GUI_X2(C_X + 70), GUI_Y(C_Y + 10) ) then
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), COLOUR_BLUE2, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if DATA[5] then
				
					local EXT_Y = 0
					local TEXT_TABLE = WrapText( DATA[5], 32 )
					
					draw.RoundedBox( 4, GUI_X2(224), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
					draw.RoundedBox( 4, GUI_X2(226), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
					
					for _,v in pairs (TEXT_TABLE) do
						EXT_Y = EXT_Y + 16
						draw.SimpleText( v, "HUD_Numbers", GUI_X2(326), GUI_Y(20 + EXT_Y), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					end
				
				end
			elseif CAN_PICK and MouseInside2( GUI_X2(C_X - 70), GUI_Y(C_Y - 10), GUI_X2(C_X + 70), GUI_Y(C_Y + 10) ) then
			
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), HUB_SHITTER[5], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if DATA[4] then
				
					local EXT_Y = 0
					local TEXT_TABLE = WrapText( DATA[4], 32 )
					
					draw.RoundedBox( 4, GUI_X2(224), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
					draw.RoundedBox( 4, GUI_X2(226), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
					
					for _,v in pairs (TEXT_TABLE) do
						EXT_Y = EXT_Y + 16
						draw.SimpleText( v, "HUD_Numbers", GUI_X2(326), GUI_Y(20 + EXT_Y), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					end
				
				end
				
				if DATA[6] then
				
					local TXT = DATA[6][1]
					local COL = DATA[6][2]
					local ALPHA = math.sin(UnPredictedCurTime()*20)*50 + 200
					
					draw.SimpleText( "- Contractor -", "HUD_Numbers", GUI_X2(326), GUI_Y(396), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					draw.SimpleText( TXT, "HUD_Numbers", GUI_X2(326), GUI_Y(415), Color(COL.r,COL.g,COL.b,ALPHA), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				end
				
				if MOUSE_CLICK then
				
					local MAP_SWAP = DATA[3]
					HUB_CAMPAIGN_STARTING = true
					PL.HUB_CAMPAIGN_SELECT = false
					
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, 100, 0.8, CHAN_STATIC )
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_StartMission.wav"), 80, 100, 0.8, CHAN_STATIC )
					
					if SaveAchievements then
						SaveAchievements()
					end
					
					if DATA[7] then
						file.Write("temporalassassin/levelload/special_load.txt",DATA[7])
					else
						file.Delete("temporalassassin/levelload/special_load.txt")
					end
					
					timer.Simple(4,function()
						RunConsoleCommand("changelevel",MAP_SWAP)
					end)
				
				end
				
			else
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), D_COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
	end
	
	local A1,A2 = 116, 400
	local SINE = math.abs(math.sin(UnPredictedCurTime()*10)*55)
	
	if MouseInside2( GUI_X2(A1 - 15), GUI_Y(A2 - 8), GUI_X2(A1 + 15), GUI_Y(A2 + 8) ) then
	
		draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_HIGHLIGHT, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
		if MOUSE_CLICK == 1 then
			PL.HUB_CAMPAIGN_SELECT = false
		end
		
	else
		draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_WHITE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

function DRAW_CAMPAIGN_SELECT2(PL)

	if not PL.HUB_CAMPAIGN_MOUSE then
		PL.HUB_CAMPAIGN_MOUSE = true
		gui.EnableScreenClicker(true)
		NoDrawCursor(false)
		HUDParticles_Reset()
	end
	
	if gui.IsGameUIVisible() then
		PL.HUB_CAMPAIGN_SELECT = false
	end

	draw.RoundedBox( 4, GUI_X2(18), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
	draw.RoundedBox( 4, GUI_X2(20), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
	
	draw.SimpleText( "- Select a Mission -", "HUD_Numbers", GUI_X2(116), GUI_Y(28), HUB_SHITTER[1], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	local CAMP_Y = 0
	local MOUSE_CLICK = MousePressed("SideMission_Press")
	local COL_MUL = ( (PL.TIME_YOU_PLAYED or 0)/60000 )
	
	if COL_MUL > 1 then COL_MUL = 1 end
	
	if MouseInside2( GUI_X2(185), GUI_Y(445), GUI_X2(215), GUI_Y(465) ) then
	
		draw.SimpleText( "????", "HUD_Numbers", GUI_X2(200), GUI_Y(455), HUB_SHITTER[6], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
		if MOUSE_CLICK then
		
			HUB_CAMPAIGN_STARTING = true
			PL.HUB_CAMPAIGN_SELECT = false
			
			PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, 100, 0.8, CHAN_STATIC )
			PL:EmitSound( Sound("TemporalAssassin/Weapons/Datharon/Datharon_Laugh.wav"), 60, 100, 0.6, CHAN_STATIC )
			
			if SaveAchievements then
				SaveAchievements()
			end
					
			RunConsoleCommand("TA_ForceNewSeed")
					
			timer.Simple(3,function()
				RunConsoleCommand("changelevel","misc_elderthrone")
			end)
			
		end
		
	else
		draw.SimpleText( "????", "HUD_Numbers", GUI_X2(200), GUI_Y(455), Color(150,150,150,255*COL_MUL), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end
	
	local WL_X = GUI_X2(119)
	local MAX_COUNTER = SIDEMISSION_COUNT
	
	if MAX_COUNTER > 13 then
	
		if CAMPAIGN_SELECT_SCROLL < (MAX_COUNTER-13) then
	
			if MouseInside2( WL_X - GUI_X(85), GUI_Y(399), WL_X - GUI_X(65), GUI_Y(424) ) then
				if MOUSE_CLICK then
					CAMPAIGN_SELECT_SCROLL = math.Clamp(CAMPAIGN_SELECT_SCROLL + 1,0,MAX_COUNTER-13)
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(119,121), 0.6, CHAN_STATIC )
				end
				draw.SimpleText( [[\]], "LORE_Title", WL_X - GUI_X(80), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X - GUI_X(70), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				draw.SimpleText( [[\]], "LORE_Title", WL_X - GUI_X(80), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X - GUI_X(70), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
		
		if CAMPAIGN_SELECT_SCROLL > 0 then
			
			if MouseInside2( WL_X + GUI_X(65), GUI_Y(399), WL_X + GUI_X(85), GUI_Y(424) ) then
				if MOUSE_CLICK then
					CAMPAIGN_SELECT_SCROLL = math.Clamp(CAMPAIGN_SELECT_SCROLL - 1,0,MAX_COUNTER-13)
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(119,121), 0.6, CHAN_STATIC )
				end
				draw.SimpleText( [[\]], "LORE_Title", WL_X + GUI_X(80), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X + GUI_X(70), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				draw.SimpleText( [[\]], "LORE_Title", WL_X + GUI_X(80), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X + GUI_X(70), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
	end
	
	for loop = 1+CAMPAIGN_SELECT_SCROLL,13+CAMPAIGN_SELECT_SCROLL do
	
		local DATA = SIDEMISSION_DATA[loop]
		
		if DATA then
	
			CAMP_Y = CAMP_Y + 25
			D_COL:SetUnpacked(250,250,250,255)
			local CAN_PICK = true
			
			if not DATA[2] then
				D_COL:SetUnpacked(250,0,0,255)
				CAN_PICK = false
			end
			
			local C_X, C_Y = 116, (40 + CAMP_Y)
			
			if not CAN_PICK and MouseInside2( GUI_X2(C_X - 70), GUI_Y(C_Y - 10), GUI_X2(C_X + 70), GUI_Y(C_Y + 10) ) then
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), COLOUR_BLUE2, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if DATA[5] then
				
					local EXT_Y = 0
					local TEXT_TABLE = WrapText( DATA[5], 32 )
					
					draw.RoundedBox( 4, GUI_X2(224), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
					draw.RoundedBox( 4, GUI_X2(226), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
					
					for _,v in pairs (TEXT_TABLE) do
						EXT_Y = EXT_Y + 16
						draw.SimpleText( v, "HUD_Numbers", GUI_X2(326), GUI_Y(20 + EXT_Y), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					end
				
				end
			elseif CAN_PICK and MouseInside2( GUI_X2(C_X - 70), GUI_Y(C_Y - 10), GUI_X2(C_X + 70), GUI_Y(C_Y + 10) ) then
			
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), HUB_SHITTER[5], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if DATA[4] then
				
					local EXT_Y = 0
					local TEXT_TABLE = WrapText( DATA[4], 32 )
					
					draw.RoundedBox( 4, GUI_X2(224), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
					draw.RoundedBox( 4, GUI_X2(226), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
					
					for _,v in pairs (TEXT_TABLE) do
						EXT_Y = EXT_Y + 16
						draw.SimpleText( v, "HUD_Numbers", GUI_X2(326), GUI_Y(20 + EXT_Y), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					end
				
				end
				
				if DATA[6] then
				
					local TXT = DATA[6][1]
					local COL = DATA[6][2]
					local ALPHA = math.sin(UnPredictedCurTime()*20)*50 + 200
					
					draw.SimpleText( "- Contractor -", "HUD_Numbers", GUI_X2(326), GUI_Y(396), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					draw.SimpleText( TXT, "HUD_Numbers", GUI_X2(326), GUI_Y(415), Color(COL.r,COL.g,COL.b,ALPHA), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				end
				
				if MOUSE_CLICK then
				
					local MAP_SWAP = DATA[3]
					HUB_CAMPAIGN_STARTING = true
					PL.HUB_CAMPAIGN_SELECT = false
					
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, 100, 0.8, CHAN_STATIC )
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_StartMission.wav"), 80, 100, 0.8, CHAN_STATIC )
					
					if SaveAchievements then
						SaveAchievements()
					end
					
					if DATA[7] then
						file.Write("temporalassassin/levelload/special_load.txt",DATA[7])
					else
						file.Delete("temporalassassin/levelload/special_load.txt")
					end
					
					RunConsoleCommand("TA_ForceNewSeed")
					
					timer.Simple(4,function()
						RunConsoleCommand("changelevel",MAP_SWAP)
					end)
				
				end
				
			else
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), D_COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
	end
	
	local A1,A2 = 116, 400
	local SINE = math.abs(math.sin(UnPredictedCurTime()*10)*55)
	
	draw.SimpleText( "Playing a new Mission will reset your level save", "HUD_Text", GUI_X2(A1), GUI_Y(A2+15), Color(200 + SINE,240 - SINE,240 - SINE,140), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	if MouseInside2( GUI_X2(A1 - 15), GUI_Y(A2 - 8), GUI_X2(A1 + 15), GUI_Y(A2 + 8) ) then
	
		draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_HIGHLIGHT, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
		if MOUSE_CLICK == 1 then
			PL.HUB_CAMPAIGN_SELECT = false
		end
		
	else
		draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_WHITE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

function DRAW_CAMPAIGN_SELECT(PL)
	
	if not PL.HUB_CAMPAIGN_MOUSE then
		PL.HUB_CAMPAIGN_MOUSE = true
		gui.EnableScreenClicker(true)
		NoDrawCursor(false)
		HUDParticles_Reset()
	end
	
	if gui.IsGameUIVisible() then
		PL.HUB_CAMPAIGN_SELECT = false
		PL.HUB_CAMPAIGN_CHAPTERS = false
	end

	draw.RoundedBox( 4, GUI_X2(18), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
	draw.RoundedBox( 4, GUI_X2(20), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
	
	if PL.HUB_CAMPAIGN_CHAPTERS then
	
		draw.SimpleText( "- Select a Chapter -", "HUD_Numbers", GUI_X2(116), GUI_Y(28), HUB_SHITTER[1], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
		local CHAP_DATA = PL.HUB_CAMPAIGN_CHAPTERS
		local COUNT = table.Count(CHAP_DATA)
		local CAMP_Y = 0
		local MOUSE_CLICK = MousePressed("Campaign_Press")
		
		for loop = 1,COUNT do
		
			CAMP_Y = CAMP_Y + 25
			local DATA = CHAP_DATA[loop]
			local LOCKED = DATA[4]
			D_COL:SetUnpacked(250,250,250,255)
			local CAN_PICK = true
			
			local C_X, C_Y = 116, (40 + CAMP_Y)
			
			if LOCKED then
				D_COL:SetUnpacked(240,0,0,255)
			end
			
			if CAN_PICK and MouseInside2( GUI_X2(C_X - 70), GUI_Y(C_Y - 10), GUI_X2(C_X + 70), GUI_Y(C_Y + 10) ) then
			
				if LOCKED then
					draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), HUB_SHITTER[7], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				else
					draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), HUB_SHITTER[5], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				end
				
				if MOUSE_CLICK and not LOCKED then
				
					local MAP_SWAP = DATA[2]
					HUB_CAMPAIGN_STARTING = true
					PL.HUB_CAMPAIGN_SELECT = false
					PL.HUB_CAMPAIGN_CHAPTERS = false
					
					if DATA[3] then
						net.Start("ChapterLoadout")
						net.WriteTable( DATA[3] )
						net.WriteString( MAP_SWAP )
						net.SendToServer()
					end
					
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, 100, 0.8, CHAN_STATIC )
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_StartMission.wav"), 80, 100, 0.8, CHAN_STATIC )
					
					if SaveAchievements then
						SaveAchievements()
					end
					
					if DATA[7] then
						file.Write("temporalassassin/levelload/special_load.txt",DATA[7])
					else
						file.Delete("temporalassassin/levelload/special_load.txt")
					end
					
					RunConsoleCommand("TA_ForceNewSeed")
					
					timer.Simple(4,function()
						RunConsoleCommand("changelevel",MAP_SWAP)
					end)
				
				end
				
			else
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), D_COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
		local A1,A2 = 116, 400
		local SINE = math.abs(math.sin(UnPredictedCurTime()*10)*55)
		
		draw.SimpleText( "Playing a new Campaign will reset your level save", "HUD_Text", GUI_X2(A1), GUI_Y(A2+15), Color(200 + SINE,240 - SINE,240 - SINE,140), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
		if MouseInside2( GUI_X2(A1 - 15), GUI_Y(A2 - 8), GUI_X2(A1 + 15), GUI_Y(A2 + 8) ) then
	
			draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_HIGHLIGHT, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
			if MOUSE_CLICK == 1 then
				PL.HUB_CAMPAIGN_SELECT = false
				PL.HUB_CAMPAIGN_CHAPTERS = false
			end
		
		else
			draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_WHITE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		end
	
	return end
	
	draw.SimpleText( "- Select a Campaign -", "HUD_Numbers", GUI_X2(116), GUI_Y(28), HUB_SHITTER[1], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	local CAMP_Y = 0
	local MOUSE_CLICK = MousePressed("Campaign_Press")
	local WL_X = GUI_X2(119)
	local MAX_COUNTER = CAMPAIGN_COUNT
	
	if MAX_COUNTER > 13 then
	
		if CAMPAIGN_SELECT_SCROLL < (MAX_COUNTER-13) then
	
			if MouseInside2( WL_X - GUI_X(85), GUI_Y(399), WL_X - GUI_X(65), GUI_Y(424) ) then
				if MOUSE_CLICK then
					CAMPAIGN_SELECT_SCROLL = math.Clamp(CAMPAIGN_SELECT_SCROLL + 1,0,MAX_COUNTER-13)
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(119,121), 0.6, CHAN_STATIC )
				end
				draw.SimpleText( [[\]], "LORE_Title", WL_X - GUI_X(80), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X - GUI_X(70), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				draw.SimpleText( [[\]], "LORE_Title", WL_X - GUI_X(80), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X - GUI_X(70), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
		
		if CAMPAIGN_SELECT_SCROLL > 0 then
			
			if MouseInside2( WL_X + GUI_X(65), GUI_Y(399), WL_X + GUI_X(85), GUI_Y(424) ) then
				if MOUSE_CLICK then
					CAMPAIGN_SELECT_SCROLL = math.Clamp(CAMPAIGN_SELECT_SCROLL - 1,0,MAX_COUNTER-13)
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(119,121), 0.6, CHAN_STATIC )
				end
				draw.SimpleText( [[\]], "LORE_Title", WL_X + GUI_X(80), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X + GUI_X(70), GUI_Y(409), HUB_SHITTER[2], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				draw.SimpleText( [[\]], "LORE_Title", WL_X + GUI_X(80), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.SimpleText( [[/]], "LORE_Title", WL_X + GUI_X(70), GUI_Y(409), HUB_SHITTER[3], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
	end
	
	for loop = 1+CAMPAIGN_SELECT_SCROLL,13+CAMPAIGN_SELECT_SCROLL do
	
		local DATA = CAMPAIGN_DATA[loop]
		
		if DATA then
		
			CAMP_Y = CAMP_Y + 25
			D_COL:SetUnpacked(250,250,250,255)
			local CAN_PICK = true
			
			if not DATA[2] then
				D_COL:SetUnpacked(250,0,0,255)
				CAN_PICK = false
			end
			
			local C_X, C_Y = 116, (40 + CAMP_Y)
			
			if not CAN_PICK and MouseInside2( GUI_X2(C_X - 70), GUI_Y(C_Y - 10), GUI_X2(C_X + 70), GUI_Y(C_Y + 10) ) then
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), COLOUR_BLUE2, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if DATA[5] then
				
					local EXT_Y = 0
					local TEXT_TABLE = WrapText( DATA[5], 32 )
					
					draw.RoundedBox( 4, GUI_X2(224), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
					draw.RoundedBox( 4, GUI_X2(226), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
					
					for _,v in pairs (TEXT_TABLE) do
						EXT_Y = EXT_Y + 16
						draw.SimpleText( v, "HUD_Numbers", GUI_X2(326), GUI_Y(20 + EXT_Y), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					end
				
				end
			elseif CAN_PICK and MouseInside2( GUI_X2(C_X - 70), GUI_Y(C_Y - 10), GUI_X2(C_X + 70), GUI_Y(C_Y + 10) ) then
			
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), HUB_SHITTER[5], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if DATA[4] then
				
					local EXT_Y = 0
					local TEXT_TABLE = WrapText( DATA[4], 32 )
					
					draw.RoundedBox( 4, GUI_X2(224), GUI_Y(13), GUI_X(204), GUI_Y(419), COLOUR_BOX1 )
					draw.RoundedBox( 4, GUI_X2(226), GUI_Y(15), GUI_X(200), GUI_Y(415), COLOUR_BOX2 )
					
					for _,v in pairs (TEXT_TABLE) do
						EXT_Y = EXT_Y + 16
						draw.SimpleText( v, "HUD_Numbers", GUI_X2(326), GUI_Y(20 + EXT_Y), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					end
				
				end
				
				if DATA[6] then
				
					local TXT = DATA[6][1]
					local COL = DATA[6][2]
					local ALPHA = math.sin(UnPredictedCurTime()*20)*50 + 200
					
					draw.SimpleText( "- Contractor -", "HUD_Numbers", GUI_X2(326), GUI_Y(396), HUB_SHITTER[4], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					draw.SimpleText( TXT, "HUD_Numbers", GUI_X2(326), GUI_Y(415), Color(COL.r,COL.g,COL.b,ALPHA), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				end
				
				local CAP_N = DATA[1]
				
				if MOUSE_CLICK then
					
					if CHAPTERSELECT_DATA and not CHAPTERSELECT_DATA[CAP_N] then
			
						local MAP_SWAP = DATA[3]
						HUB_CAMPAIGN_STARTING = true
						PL.HUB_CAMPAIGN_SELECT = false
						PL.HUB_CAMPAIGN_CHAPTERS = false
						
						PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, 100, 0.8, CHAN_STATIC )
						PL:EmitSound( Sound("TemporalAssassin/UI/HUB_StartMission.wav"), 80, 100, 0.8, CHAN_STATIC )
					
						if SaveAchievements then
							SaveAchievements()
						end
						
						if DATA[7] then
							file.Write("temporalassassin/levelload/special_load.txt",DATA[7])
						else
							file.Delete("temporalassassin/levelload/special_load.txt")
						end
					
						RunConsoleCommand("TA_ForceNewSeed")
						
						timer.Simple(4,function()
							RunConsoleCommand("changelevel",MAP_SWAP)
						end)
					
					else
					
						PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, 100, 0.8, CHAN_STATIC )
						
						PL.HUB_CAMPAIGN_CHAPTERS = CHAPTERSELECT_DATA[CAP_N]
						
					end
				
				end
				
			else
				draw.SimpleText( DATA[1], "HUD_Numbers", GUI_X2(C_X), GUI_Y(C_Y), D_COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
	end
	
	local A1,A2 = 116, 400
	local SINE = math.abs(math.sin(UnPredictedCurTime()*10)*55)
	
	draw.SimpleText( "Playing a new Campaign will reset your level save", "HUD_Text", GUI_X2(A1), GUI_Y(A2+15), Color(200 + SINE,240 - SINE,240 - SINE,140), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	if MouseInside2( GUI_X2(A1 - 15), GUI_Y(A2 - 8), GUI_X2(A1 + 15), GUI_Y(A2 + 8) ) then
	
		draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_HIGHLIGHT, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
		if MOUSE_CLICK == 1 then
			PL.HUB_CAMPAIGN_SELECT = false
			PL.HUB_CAMPAIGN_CHAPTERS = false
		end
		
	else
		draw.SimpleText( "Close", "Secret_TXT", GUI_X2(A1), GUI_Y(A2), COLOUR_WHITE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

local DRAW_TESTED = false
local HUB_DRAW = false
local LOAD_PRESS = false
local LOAD_USED_ONCE = false
local MAP_LOADING_HUB = false

local TRAINING_PRESS = false
local TRAINING_LOADING = false
local TRAINING_USED_ONCE = false

local MIRROR_PRESS = false
local MIRROR_LOADING = false
local MIRROR_USED_ONCE = false

local ZIO_TEXTURE = surface.GetTextureID("TemporalAssassin/HUB/Zio_Logo_Door")

local MIRROR_TEXTURE1 = surface.GetTextureID("TemporalAssassin/HUB/Mirror_Mist1")
local MIRROR_TEXTURE2 = surface.GetTextureID("TemporalAssassin/HUB/Mirror_Mist2")
local MIRROR_TEXTURE3 = surface.GetTextureID("TemporalAssassin/HUB/Mirror_Mist3")

function HUB_GetEyeHit()

	local PL = LocalPlayer()
	local POS, ANG = (PL.VIEW_POSITION or PL:EyePos()), (AIM_BULLET_ANGLE or PL:EyeAngles())
	
	local TR = {}
	TR.start = POS
	TR.endpos = POS + ANG:Forward()*40
	TR.mask = MASK_PLAYERSOLID
	TR.filter = PL
	TR.mins = Vector(-0.5,-0.5,-0.5)
	TR.maxs = Vector(0.5,0.5,0.5)
	local Trace = util.TraceHull(TR)
	
	return Trace.HitPos

end
		
local SINHUNTLOGO_POS, SINHUNTLOGO_ANGLE = Vector(-401.2,357.7,33), Angle(0,90,0)
SINHUNTLOGO_ANGLE:RotateAroundAxis(SINHUNTLOGO_ANGLE:Right(),-90)
SINHUNTLOGO_ANGLE:RotateAroundAxis(SINHUNTLOGO_ANGLE:Up(),90)
		
local MIRROR_POS, MIRROR_ANGLE = Vector(-713.05,481,40.5), Angle(0,-180,0)
local MIRROR_USE_POS = Vector(-737.913,430.995,-4.655)
MIRROR_ANGLE:RotateAroundAxis(MIRROR_ANGLE:Right(),-90)
MIRROR_ANGLE:RotateAroundAxis(MIRROR_ANGLE:Up(),90)
	
local TRAINING_POS, TRAINING_ANGLE = Vector(-278.27,845.521,37.969), Angle(0,-130,0)
local TRAINING_USE_POS = Vector(-278.27,845.521,-7.969)
TRAINING_ANGLE:RotateAroundAxis(TRAINING_ANGLE:Right(),-90)
TRAINING_ANGLE:RotateAroundAxis(TRAINING_ANGLE:Up(),90)
		
local MISSION_POS, MISSION_ANG = Vector(-537.17895507813,370.77639770508,37.721199035645), Angle(42,90,0)
MISSION_ANG:RotateAroundAxis(MISSION_ANG:Right(),-90)
MISSION_ANG:RotateAroundAxis(MISSION_ANG:Up(),90)
		
local CHRONO_POS, CHRONO_ANG = Vector(-647.735,370.391,46.598), Angle(42,90,0)
CHRONO_ANG:RotateAroundAxis(CHRONO_ANG:Right(),-90)
CHRONO_ANG:RotateAroundAxis(CHRONO_ANG:Up(),90)

local SIDEMISSION_POS, SIDEMISSION_ANG = Vector(-209.69,419.75,44.236), Angle(30,180,0)
SIDEMISSION_ANG:RotateAroundAxis(SIDEMISSION_ANG:Right(),-90)
SIDEMISSION_ANG:RotateAroundAxis(SIDEMISSION_ANG:Up(),90)

local SINMISSION_POS, SINMISSION_ANG = Vector(-423.591,370.242,46.144), Angle(40,90,0)
SINMISSION_ANG:RotateAroundAxis(SINMISSION_ANG:Right(),-90)
SINMISSION_ANG:RotateAroundAxis(SINMISSION_ANG:Up(),90)

local HELPTXT_POS, HELPTXT_ANG = Vector(-206.959,726.069,53.448), Angle(30,180,0)
local HELPTXT_POS2, HELPTXT_ANG2 = Vector(-968.502,285.793,19.86), Angle(0,210,120)
local HELPTXT_POS3, HELPTXT_ANG3 = Vector(-689.716,420.129,55.14), Angle(30,0,0)
local HELPTXT_POS4 = Vector(-691.716,420.129,46.14), Angle(30,0,0)
HELPTXT_ANG:RotateAroundAxis(HELPTXT_ANG:Right(),-90)
HELPTXT_ANG:RotateAroundAxis(HELPTXT_ANG:Up(),90)
HELPTXT_ANG3:RotateAroundAxis(HELPTXT_ANG3:Right(),-90)
HELPTXT_ANG3:RotateAroundAxis(HELPTXT_ANG3:Up(),90)

local REST_LOOK_POINT = Vector(-990.739,317.813,-21.128)

local LORE_POS, LORE_ANGLE = Vector(-790.937,910.161,-10.421), Angle(0,-82.5,0)
LORE_ANGLE:RotateAroundAxis(LORE_ANGLE:Right(),-90)
LORE_ANGLE:RotateAroundAxis(LORE_ANGLE:Up(),90)
local LORE_LOOK_POINT = Vector(-790.686,910.198,-16.06)

local LOAD_LOOK_POINT = Vector(-1204.03515625,409.61636352539,-18.153018951416)

local COL_PULSING = Color(0,0,0,0)
local COL_ALTR = Color(0,0,0,0)
local COL_ALTR2 = Color(0,0,0,0)
local COL_ALTR3 = Color(0,0,0,0)

function HUB_TextDraw()

	if not FONTS_LOADED then return end

	if not DRAW_TESTED then
	
		DRAW_TESTED = true
		
		if game.GetMap() == "ta_hub" then
			HUB_DRAW = true
		else
			HUB_DRAW = false
		end
		
	end
	
	if HUB_DRAW then
	
		local PULSE = math.sin(UnPredictedCurTime()*20)*45
		COL_PULSING:SetUnpacked(45 - PULSE,210 + PULSE,45 - PULSE,255)
		local PL = LocalPlayer()
		local USE_DOWN = PL:KeyDown(IN_USE)
		
		--[[ SIN HUNT DOOR LOGO ]]
		
		cam.Start3D2D( SINHUNTLOGO_POS, SINHUNTLOGO_ANGLE, 0.05 )
			surface.SetTexture(ZIO_TEXTURE)
			surface.SetDrawColor(13,82,22,255)
			surface.DrawTexturedRect(0,0,1024*0.9,2048*0.9)
		cam.End3D2D()
		
		--[[ MIRROR AREA ]]
	
		if PL.MIRROR_HUB then
			
			local MIRROR_LOOKING = HUB_GetEyeHit():Distance(MIRROR_USE_POS) <= 25
			
			cam.Start3D2D( MIRROR_POS, MIRROR_ANGLE, 0.05 )
			
				local ALPHA_1 = 160
				local ALPHA_2 = 0
				local ALPHA_3 = 0
				
				if MIRROR_LOOKING then
						
					ALPHA_1 = 0
					ALPHA_2 = 160
					ALPHA_3 = 0
					
					if MIRROR_USED_ONCE then
						ALPHA_1 = 0
						ALPHA_2 = 0
						ALPHA_3 = 160
					end
					
					if USE_DOWN and not MIRROR_PRESS then
					
						MIRROR_PRESS = true
						
						if not MAP_LOADING_HUB and MIRROR_LOOKING and not MIRROR_USED_ONCE then
						
							ALPHA_1 = 0
							ALPHA_2 = 160
							ALPHA_3 = 0
						
							MIRROR_USED_ONCE = true
							PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
							
						elseif not MAP_LOADING_HUB and MIRROR_LOOKING and MIRROR_USED_ONCE then
						
							ALPHA_1 = 0
							ALPHA_2 = 0
							ALPHA_3 = 160
						
							MAP_LOADING_HUB = true
							
							net.Start("Mirror_SwapLevel")
							net.SendToServer()
							
							PL:EmitSound( Sound("TemporalAssassin/UI_B/HUD_Click_E.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
							PL:EmitSound( Sound("TemporalAssassin/Effects/Temporal_Headache.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
						
							if SaveAchievements then
								SaveAchievements()
							end
							
							timer.Simple(3,function()
								RunConsoleCommand("changelevel","astral_apartment")
							end)
							
						end
					
					elseif not USE_DOWN and MIRROR_PRESS then
					
						MIRROR_PRESS = false
						
					end
					
				else
				
					MIRROR_USED_ONCE = false
					ALPHA_1 = 160
					ALPHA_2 = 0
					ALPHA_3 = 0
					
				end
				
				if MAP_LOADING_HUB then
					ALPHA_1 = 0
					ALPHA_2 = 0
					ALPHA_3 = 160
				end
				
				local MIRROR_ALPHA1 = Variable_SmoothOut( "MIRROR_ALPHATARGET_1", ALPHA_1, 2, true, 0.1, 5000 )
				local MIRROR_ALPHA2 = Variable_SmoothOut( "MIRROR_ALPHATARGET_2", ALPHA_2, 2, true, 0.1, 5000 )
				local MIRROR_ALPHA3 = Variable_SmoothOut( "MIRROR_ALPHATARGET_3", ALPHA_3, 2, true, 0.1, 5000 )
				
				if MIRROR_ALPHA1 > 0 then
					surface.SetTexture(MIRROR_TEXTURE1)
					surface.SetDrawColor(255,255,255,MIRROR_ALPHA1)
					surface.DrawTexturedRect(0,0,1024*1.955,512*2.545)
				end
				
				if MIRROR_ALPHA2 > 0 then
					surface.SetTexture(MIRROR_TEXTURE2)
					surface.SetDrawColor(255,255,255,MIRROR_ALPHA2)
					surface.DrawTexturedRect(0,0,1024*1.955,512*2.545)
				end
				
				if MIRROR_ALPHA3 > 0 then
					surface.SetTexture(MIRROR_TEXTURE3)
					surface.SetDrawColor(255,255,255,MIRROR_ALPHA3)
					surface.DrawTexturedRect(0,0,1024*1.955,512*2.545)
				end
				
			cam.End3D2D()
			
		end
		
		--[[ TRAINING AREA ]]
		
		local TRAINING_LOOKING = HUB_GetEyeHit():Distance(TRAINING_USE_POS) <= 70
		
		cam.Start3D2D( TRAINING_POS, TRAINING_ANGLE, 0.05 )
		
			local TXT = "- Goto Training Area -"
			local FNT = "HUB_Text"
			
			if MAP_LOADING_HUB then
				TXT = "- Travelling... -"
			elseif TRAINING_USED_ONCE then
				TXT = "- Are you Sure? -"
			end
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			surface.SetDrawColor( 10, 10, 10, 150 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			
			if TRAINING_LOOKING then
			
				draw.SimpleText( TXT, FNT, -tW/2, 0, COL_PULSING )
				
				if USE_DOWN and not TRAINING_PRESS then
				
					TRAINING_PRESS = true
					
					if not MAP_LOADING_HUB and TRAINING_LOOKING and not TRAINING_USED_ONCE then
					
						TRAINING_USED_ONCE = true
						PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
						
					elseif not MAP_LOADING_HUB and TRAINING_LOOKING and TRAINING_USED_ONCE then
					
						MAP_LOADING_HUB = true
						
						PL:EmitSound( Sound("TemporalAssassin/UI/Game_Load.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
					
						if SaveAchievements then
							SaveAchievements()
						end
						
						timer.Simple(2,function()
							RunConsoleCommand("changelevel","ta_training")
						end)
						
					end
				
				elseif not USE_DOWN and TRAINING_PRESS then
				
					TRAINING_PRESS = false
					
				end
				
			else
			
				draw.SimpleText( TXT, FNT, -tW/2, 0, COLOUR_WHITE )
				TRAINING_USED_ONCE = false
				
			end
			
		cam.End3D2D()
		
		cam.Start3D2D( MISSION_POS, MISSION_ANG, 0.05 )
			
			local TXT = "- Start a Campaign -"
			local FNT = "HUB_Text"
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			surface.SetDrawColor( 10, 10, 10, 250 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			draw.SimpleText( TXT, FNT, -tW/2, 0, COLOUR_RED )
				
		cam.End3D2D()
		
		cam.Start3D2D( CHRONO_POS, CHRONO_ANG, 0.05 )
			
			local TXT = "- Chronological List -"
			local FNT = "HUB_Text"
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			surface.SetDrawColor( 10, 10, 10, 250 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			draw.SimpleText( TXT, FNT, -tW/2, 0, COLOUR_WHITE )
				
		cam.End3D2D()
		
		cam.Start3D2D( SIDEMISSION_POS, SIDEMISSION_ANG, 0.05 )
			
			local TXT = "- Side-Missions -"
			local FNT = "HUB_Text"
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			surface.SetDrawColor( 10, 10, 10, 250 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			draw.SimpleText( TXT, FNT, -tW/2, 0, COLOUR_GREEN )
				
		cam.End3D2D()
		
		cam.Start3D2D( SINMISSION_POS, SINMISSION_ANG, 0.05 )
			
			local TXT = "- Sin-Hunter -"
			local FNT = "HUB_Text"
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			surface.SetDrawColor( 10, 10, 10, 250 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			draw.SimpleText( TXT, FNT, -tW/2, 0, COLOUR_GREEN )
				
		cam.End3D2D()
		
		local HELP_SIN = math.sin(UnPredictedCurTime()*5)*60
		
		cam.Start3D2D( HELPTXT_POS, HELPTXT_ANG, 0.05 )
			
			local TXT = "Press ESCAPE to open the menu and change all kinds of settings"
			local FNT = "HUB_Text2"
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			COL_ALTR:SetUnpacked(140+HELP_SIN,240,140+HELP_SIN,255)
			
			surface.SetDrawColor( 10, 10, 10, 150 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			draw.SimpleText( TXT, FNT, -tW/2, 0, COL_ALTR )
				
		cam.End3D2D()
			
		local SLEEP_LOOKING = false
		
		if PL:GetPos():DistanceFix(REST_LOOK_POINT) <= 65 then
			SLEEP_LOOKING = true
		end
		
		cam.Start3D2D( HELPTXT_POS2, HELPTXT_ANG2, 0.05 )
			
			local TXT = "Take a Short Rest (Refresh HUB Area/Events)"
			local FNT = "HUB_Text3"
			
			if SLEEP_LOOKING then
				TXT = "Take a Short Rest? (Refresh HUB Area/Events)"
			end
			
			local CANT_REST = (REST_DELAY and CurTime() < REST_DELAY)
			
			if SLEEP_LOOKING and USE_DOWN and not CANT_REST then
				HUB_DoRest()
			end
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			surface.SetDrawColor( 10, 10, 10, 150 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			
			if SLEEP_LOOKING then
				COL_ALTR2:SetUnpacked(140+HELP_SIN,240,140+HELP_SIN,255)
				draw.SimpleText( TXT, FNT, -tW/2, 0, COL_ALTR2 )
			else
				COL_ALTR2:SetUnpacked(240,240,240,255)
				draw.SimpleText( TXT, FNT, -tW/2, 0, COL_ALTR2 )
			end
				
		cam.End3D2D()
			
		COL_ALTR3:SetUnpacked(140+HELP_SIN,240,140+HELP_SIN,255)
		
		cam.Start3D2D( HELPTXT_POS3, HELPTXT_ANG3, 0.05 )
			
			local TXT = "You can Load your game at any time"
			local FNT = "HUB_Text3"
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			surface.SetDrawColor( 10, 10, 10, 150 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			draw.SimpleText( TXT, FNT, -tW/2, 0, COL_ALTR3 )
				
		cam.End3D2D()
		
		cam.Start3D2D( HELPTXT_POS4, HELPTXT_ANG3, 0.05 )
			
			local TXT = "by using the Computer in Kits room"
			local FNT = "HUB_Text3"
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			surface.SetDrawColor( 10, 10, 10, 150 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			draw.SimpleText( TXT, FNT, -tW/2, 0, COL_ALTR3 )
				
		cam.End3D2D()
		
		local CAN_LOAD = false
		local MAP = "fuckall_blank"
					
		if file.Exists("TemporalAssassin/Map_Load.txt","DATA") then
			MAP = tostring(file.Read("TemporalAssassin/Map_Load.txt","DATA"))
			CAN_LOAD = true
		end
		
		if CAN_LOAD then
		
			local LOAD_LOOKING = false
			
			if HUB_GetEyeHit():Distance(LOAD_LOOK_POINT) <= 8 then
				LOAD_LOOKING = true
			else
				LOAD_USED_ONCE = false
			end
			
			if USE_DOWN and not LOAD_PRESS then
			
				LOAD_PRESS = true
				
				if not MAP_LOADING_HUB and LOAD_LOOKING and not LOAD_USED_ONCE then
				
					LOAD_USED_ONCE = true
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
					
				elseif not MAP_LOADING_HUB and LOAD_LOOKING and LOAD_USED_ONCE then
				
					MAP_LOADING_HUB = true
					
					PL:EmitSound( Sound("TemporalAssassin/UI/Game_Load.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
					
					if SaveAchievements then
						SaveAchievements()
					end
					
					timer.Simple(3,function()
						RunConsoleCommand("changelevel",MAP)
					end)
					
				end
				
			elseif not USE_DOWN and LOAD_PRESS then
			
				LOAD_PRESS = false
				
			end
		
			local pos, pos2, angle = Vector(-1210.4057617188,418.96875,-1.663148880005), Vector(-1210.4057617188,432.96875,-14), Angle(12.480,-6.263,0)
			angle:RotateAroundAxis(angle:Right(),-90)
			angle:RotateAroundAxis(angle:Up(),90)
			
			cam.Start3D2D( pos, angle, 0.05 )
			
				local TXT = "- Load Game -"
				local FNT = "HUB_Text"
				
				if MAP_LOADING_HUB then
					TXT = "- Loading... -"
				elseif LOAD_USED_ONCE then
					TXT = "- Are you Sure? -"
				end
				
				surface.SetFont( FNT )
				local tW, tH = surface.GetTextSize( TXT )
				local pad = 5
				
				surface.SetDrawColor( 10, 10, 10, 150 )
				surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
				
				if LOAD_LOOKING then
					local PULSE = math.sin(UnPredictedCurTime()*20)*45
					draw.SimpleText( TXT, FNT, -tW/2, 0, COL_PULSING )
				else
					draw.SimpleText( TXT, FNT, -tW/2, 0, COLOUR_WHITE )
				end
				
			cam.End3D2D()
			
			cam.Start3D2D( pos2, angle, 0.05 )
			
				local TXT = MAP
				local FNT = "HUD_Numbers"
				
				surface.SetFont( FNT )
				local tW, tH = surface.GetTextSize( TXT )
				local pad = 5
				
				surface.SetDrawColor( 10, 10, 10, 150 )
				surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
				
				if LOAD_LOOKING then
					local PULSE = math.sin(UnPredictedCurTime()*20)*45
					draw.SimpleText( TXT, FNT, -tW/2, 0, COL_PULSING )
				else
					draw.SimpleText( TXT, FNT, -tW/2, 0, COLOUR_WHITE )
				end
				
			cam.End3D2D()
		
		end
		
		local LORE_LOOKING = false
			
		if HUB_GetEyeHit():Distance(LORE_LOOK_POINT) <= 12 then
			LORE_LOOKING = true
		end
		
		cam.Start3D2D( LORE_POS, LORE_ANGLE, 0.05 )
		
			local TXT = "- Lore Files -"
			local FNT = "HUB_Text"
			
			surface.SetFont( FNT )
			local tW, tH = surface.GetTextSize( TXT )
			local pad = 5
			
			surface.SetDrawColor( 10, 10, 10, 150 )
			surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
			
			if LORE_LOOKING then
				draw.SimpleText( TXT, FNT, -tW/2, 0, COL_PULSING )
				
				if USE_DOWN and not LORE_PRESS and TA_CanOpenMenu() then
					LORE_PRESS = true
					PL.HUB_CAMPAIGN_SELECT = 10
					PL:EmitSound( Sound("TemporalAssassin/UI/HUB_Click.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
					PL.HUB_CAMPAIGN_MOUSE = true
					gui.EnableScreenClicker(true)
					NoDrawCursor(false)
					HUDParticles_Reset()
				end
				
			else
				LORE_PRESS = false
				draw.SimpleText( TXT, FNT, -tW/2, 0, COLOUR_WHITE )
			end
			
		cam.End3D2D()
	
	end

end
hook.Add("PostDrawOpaqueRenderables","HUB_TextDrawHook",HUB_TextDraw)

function TA_HUB_SELECTOR()

	local PL = LocalPlayer()
	
	if input.IsKeyDown(KEY_ESCAPE) and PL.HUB_CAMPAIGN_SELECT then
		PL.HUB_CAMPAIGN_SELECT = false
		ESCAPEMENU_FORCE = UnPredictedCurTime() + 0.1
	end

	if PL.HUB_CAMPAIGN_SELECT and PL.HUB_CAMPAIGN_SELECT == 11 and PL.LORE_FILE_SUBDATA and PL.LORE_FILE_NAME then
	return end

	if PL.HUB_CAMPAIGN_SELECT and PL.HUB_CAMPAIGN_SELECT == 10 then
	return end

	if PL.HUB_CAMPAIGN_SELECT and PL.HUB_CAMPAIGN_SELECT == 4 then
		if HUDParticles_DrawBehind then
			HUDParticles_DrawBehind()
		end
		DRAW_CAMPAIGN_SELECT4(PL)
		F4MENU_DrawCursor()
		if HUDParticles_DrawFront then
			HUDParticles_DrawFront()
		end
	return end

	if PL.HUB_CAMPAIGN_SELECT and PL.HUB_CAMPAIGN_SELECT == 3 then
		if HUDParticles_DrawBehind then
			HUDParticles_DrawBehind()
		end
		DRAW_CAMPAIGN_SELECT3(PL)
		F4MENU_DrawCursor()
		if HUDParticles_DrawFront then
			HUDParticles_DrawFront()
		end
	return end
	
	if PL.HUB_CAMPAIGN_SELECT and PL.HUB_CAMPAIGN_SELECT == 2 then
		if HUDParticles_DrawBehind then
			HUDParticles_DrawBehind()
		end
		DRAW_CAMPAIGN_SELECT2(PL)
		F4MENU_DrawCursor()
		if HUDParticles_DrawFront then
			HUDParticles_DrawFront()
		end
	return end
	
	if PL.HUB_CAMPAIGN_SELECT then
		if HUDParticles_DrawBehind then
			HUDParticles_DrawBehind()
		end
		DRAW_CAMPAIGN_SELECT(PL)
		F4MENU_DrawCursor()
		if HUDParticles_DrawFront then
			HUDParticles_DrawFront()
		end
	return end
	
	if PL.HUB_CAMPAIGN_MOUSE then
		PL.HUB_CAMPAIGN_MOUSE = false
		gui.EnableScreenClicker(false)
		NoDrawCursor(true)
	end

end
hook.Add("HUDPaint","TA_HUB_SELECTORHook",TA_HUB_SELECTOR)

timer.Simple(5,function()

	local LORE_OBJECTS = {
	"weapon_arcblade_1",
	"weapon_raikiri_1",
	"weapon_falanran_1",
	"weapon_hammer_1",
	"REITER_UNLOCK",
	"weapon_reiterpallasch_6",
	"weapon_fenic_1",
	"weapon_griseus_1",
	"weapon_photoop_1",
	"weapon_roaringwolf_1",
	"weapon_cobaltstorm_1",
	"weapon_outcast_1",
	"weapon_m6g_1",
	"weapon_ebonyivory_1",
	"weapon_snakecharmer_1",
	"weapon_royaldecree_1",
	"weapon_gungnir_1",
	"weapon_cruentus_1",
	"weapon_demonsbane_1",
	"weapon_foolsoath_1",
	"weapon_autoshotgun_1",
	"weapon_gearakarme_1",
	"weapon_omen_1",
	"weapon_flatline_1",
	"weapon_longhorn_1",
	"weapon_reikoririfle_1",
	"weapon_siamesespectre_1",
	"weapon_harbinger_1",
	"weapon_lstar_1",
	"weapon_kingslayer_1",
	"weapon_silverace_1",
	"weapon_queensdoom_1",
	"weapon_azarath_1",
	"weapon_frontliner_1",
	"weapon_anthrakia_1",
	"weapon_whisper_1",
	"weapon_hergift_1",
	"weapon_grenade_1",
	"weapon_falanrath_1",
	"weapon_reikori_1",
	"item_backpack_1",
	"item_corzoshat_1",
	"item_volatilebattery_1",
	"item_reinforcedcombatrig_1",
	"item_largemanacrystal_1",
	"item_smallmanacrystal_1",
	"item_beer_1",
	"item_turkey_1",
	"item_healthinhaler_1",
	"item_medkit_1",
	"item_medkit50_1",
	"item_portablemedkit_1",
	"item_armourshard_1",
	"item_armourpieces_1",
	"item_eldritcharmourshard_1",
	"item_eldritcharmourset_1",
	"item_sinfragment_1",
	"item_armourfragment_1"}
	
	local YES_TABLE = {}
	
	for _,v in pairs (LORE_OBJECTS) do
	
		if TA_HasLoreFile(v) then
			table.insert(YES_TABLE,v)
		end
	
	end
	
	net.Start("HUB_LoreObjects")
	net.WriteTable(YES_TABLE)
	net.SendToServer()
	
	if IsValid(LocalPlayer()) then
		LocalPlayer():SpeedSetup()
	end

end)
	
local FILES, DIRECTOR = file.Find("gamemodes/temporalassassin/gamemode/temporalassassin/levels/*_addhub.lua","GAME","nameasc")

for loop = 1,#FILES do

	local INC_S = FILES[loop]
	include(INC_S)
	print("Loading "..INC_S)
	
end

local SONG_LIST = {
Sound("TemporalAssassin/Music/Gramaphone_1.mp3"),
Sound("TemporalAssassin/Music/Gramaphone_2.mp3"),
Sound("TemporalAssassin/Music/Gramaphone_3.mp3")}

function HUB_Gramaphone_On_LIB( len )

	local BL = net.ReadBool()
	
	if BL then
		GLOB_MUSIC_SILENCE = true
		GRAMAPHONE_MUSIC = CreateSound( LocalPlayer(), table.Random(SONG_LIST) )
		GRAMAPHONE_MUSIC:PlayEx(0.125,100)
	else
		GLOB_MUSIC_SILENCE = false
		GRAMAPHONE_MUSIC:FadeOut(0.4)
		GRAMAPHONE_MUSIC = nil
	end
	
	GLOBAL_GRAMAPHONE_ON = BL

end
net.Receive("HUB_Gramaphone_On",HUB_Gramaphone_On_LIB)

function HUB_CL_Think()

	local pl = LocalPlayer()
	
	if GLOBAL_GRAMAPHONE_ON then
	
		GLOB_MUSIC_SILENCE = true
		
		if GRAMAPHONE_MUSIC then
			GRAMAPHONE_MUSIC:ChangePitch(99.9,0)
			GRAMAPHONE_MUSIC:ChangePitch(100,0)
			GRAMAPHONE_MUSIC:SetSoundLevel(0)
		end
		
	end
	
	if ESSENCE_WAIT_TIME and CurTime() < ESSENCE_WAIT_TIME then return end
	
	if pl:HasLoreFile("lore_malum_char1") and pl:HasLoreFile("lore_binderoutcast") and pl:HasLoreFile("lore_binderoutcast2") then
	
		if TAS:GetSuit(pl) == 12 and pl:EyePos():Distance( Vector(-1189.687,302.188,-26.406) ) <= 150 then
		
			if not ESSENCE_PROP then
				ESSENCE_PROP = ClientsideModel( "models/TemporalAssassin/Projectiles/KingSlayer.mdl", RENDERGROUP_TRANSLUCENT )
				ESSENCE_PROP:SetPos( Vector(-1209.687,302.188,-16.406) )
				ESSENCE_PROP:SetNoDraw(true)
				ESSENCE_PROP:Spawn()
				ESSENCE_PROP:Activate()
				ESSENCE_PROP.PARTICLE = CreateParticleSystem( ESSENCE_PROP, "Eldritch_Essence", PATTACH_ABSORIGIN_FOLLOW, 1, Vector(0,0,0) )
				GLOB_NEAR_ESSENCE = true
				GLOB_MUSIC_SILENCE = true
				pl.ESSENCE_SOUND = CreateSound( pl, "TemporalAssassin/Misc/Dagons_Presence.wav" )
				pl.ESSENCE_SOUND:PlayEx(0,80)
				pl.ESSENCE_SOUND:ChangeVolume(0.3,1)
			end
		
		elseif pl:EyePos():Distance( Vector(-1189.687,302.188,-26.406) ) > 150 then
		
			if ESSENCE_PROP and IsValid(ESSENCE_PROP) then
			
				if ESSENCE_PROP.PARTICLE then
					ESSENCE_PROP.PARTICLE:StopEmission( false, false, false )
				end
				
				GLOB_NEAR_ESSENCE = false
				GLOB_MUSIC_SILENCE = false
				ESSENCE_PROP:Remove()
				ESSENCE_PROP = nil
				
				if pl.ESSENCE_SOUND then
					pl.ESSENCE_SOUND:FadeOut(0.3)
					pl.ESSENCE_SOUND = nil
				end
				
				ESSENCE_WAIT_TIME = CurTime() + 0.4
			
			end
		
		end
	
	end

end
hook.Add("Think","HUB_CL_ThinkHook",HUB_CL_Think)

local DAGON_MAT = Material("TemporalAssassin/NEW_HUD/Dagon_Visage.png")

function HUB_CL_HUDPaintBackground()

	local ESSENCE_BLACK = 0
	
	if GLOB_NEAR_ESSENCE then
		ESSENCE_BLACK = 200
		DEV_NOHUD = true
	else
		DEV_NOHUD = false
	end
	
	local DO_BLACK_BG = Variable_SmoothOut( "ESSENCE_DAGON_BACKBLACK", ESSENCE_BLACK, 5, true, 0.1, 5000 )

	if DO_BLACK_BG > 0 then
	
		local PL = LocalPlayer()
		
		surface.SetDrawColor(0,0,0,DO_BLACK_BG + math.Rand(-5,5)*(DO_BLACK_BG*0.01))
		surface.DrawRect( 0, 0, ScrW(), ScrH() )
		
		local SINE = math.sin(UnPredictedCurTime()*0.4)*800		
		local SINE_MUL = (-600 + SINE)/200
		
		if SINE_MUL > 0 then
		
			if not PL:HasLoreFile("lore_dagonspotted") then
				Unlock_TA_LoreFile( "lore_dagonspotted", "" )
			end
			
			if not PL:HasLoreFile("lore_dagonspotted_2") then
				Unlock_TA_LoreFile( "lore_dagonspotted_2", "" )
				file.Write("temporalassassin/____________.txt",[[R͢҉ELE̶͘A͢S̸E̢͠ ̶M҉͞҉E SO ̨͢WE̸̴͟ ̢͝C͡ÁN ́͝B̨͡E A͜ FA̛M̨̕IĻ͝Y ̧AG̶̛AIN̛]])
			end
			
			if PL.ESSENCE_SOUND and not DAGON_SOUNDER then
				DAGON_SOUNDER = true
				PL.ESSENCE_SOUND:ChangeVolume(0.65,1)
			end
			
			surface.SetMaterial(DAGON_MAT)
			surface.SetDrawColor(100,100,100,(50 + math.Rand(-10,10))*SINE_MUL)
			surface.DrawTexturedRect( 0, 0, ScrW(), ScrH() )
			
		elseif SINE_MUL <= 0 then
		
			if PL.ESSENCE_SOUND and DAGON_SOUNDER then
				DAGON_SOUNDER = false
				PL.ESSENCE_SOUND:ChangeVolume(0.3,1)
			end
			
		end
		
	end
	
	surface.SetDrawColor(255,255,255,255)

end
hook.Add("HUDPaintBackground","HUB_CL_HUDPaintBackgroundHook",HUB_CL_HUDPaintBackground)

function HUB_CreateAbyssWisp_LIB( len )

	local P_VEC = Vector(-1039.142,903.154,5.631)
	local data = { P_VEC, Angle(0,0,0), VectorRand()*1, LocalPlayer(), 5, false, false, 0, 1 }
	
	GLOB_HUB_WISP_SPAWNED = false
	GLOB_ABYSS_PORTAL = false
	GLOB_ABYSS_PORTAL_GO = false
	GLOB_MUSIC_SILENCE = false
	GLOB_ABYSS_PORTAL_WAIT = nil
	
	if G_PORTAL_PARTICLE then
		G_PORTAL_PARTICLE:StopEmission( false, true, false )
		G_PORTAL_PARTICLE = nil
	end
	
	if ABYSS_SOUND then
		ABYSS_SOUND:Stop()
		ABYSS_SOUND = nil
	end
	
	if GLOB_HUB_WISP and IsValid(GLOB_HUB_WISP) then
		GLOB_HUB_WISP:Remove() 
		GLOB_HUB_WISP = nil
	end
	
	HUB_WISP_MOVE = false
	
	GLOB_HUB_WISP = WISP_Projectile( data )

end
net.Receive("HUB_CreateAbyssWisp",HUB_CreateAbyssWisp_LIB)

function HUB_WispThink()
	
	local PL = LocalPlayer()

	if GLOB_HUB_WISP and IsValid(GLOB_HUB_WISP) then
		
		GLOB_HUB_WISP_SPAWNED = true
		GLOB_HUB_WISP.Target = nil
		GLOB_HUB_WISP.Move_Speed = 5
		GLOB_HUB_WISP.DieTime = 999999999
		GLOB_HUB_WISP.Target_Wait = 999999999
	
		if not HUB_WISP_MOVE then
		
			local P_VEC = Vector(-1039.142,903.154,5.631)
			
			if GLOB_HUB_WISP:GetPos():Distance( P_VEC ) >= 25 then
				GLOB_HUB_WISP.Move_Direction = (P_VEC - GLOB_HUB_WISP:GetPos()):GetNormal() + VectorRand()*0.1
			end
			
			if PL:GetPos():Distance(GLOB_HUB_WISP:GetPos()) <= 110 then
				HUB_WISP_MOVE = true
				GLOB_HUB_WISP.Move_Direction = (Vector(-908.706,915.851,-63.969) - GLOB_HUB_WISP:GetPos()):GetNormal()
				GLOB_HUB_WISP.Move_Speed = 15
			end
			
		else
		
			GLOB_HUB_WISP.Move_Direction = (Vector(-908.706,915.851,-63.969) - GLOB_HUB_WISP:GetPos()):GetNormal()
			GLOB_HUB_WISP.Move_Speed = 15
			
		end
		
	elseif GLOB_HUB_WISP_SPAWNED and GLOB_HUB_WISP and not IsValid(GLOB_HUB_WISP) then
	
		GLOB_HUB_WISP = nil
		GLOB_HUB_WISP_SPAWNED = false
		GLOB_ABYSS_PORTAL = true
		
		G_PORTAL_PARTICLE = CreateParticleSystem( LocalPlayer(), "HUB_Abyss_Beacon", PATTACH_ABSORIGIN, 0, Vector( 0, 0, 0 ) )
		G_PORTAL_PARTICLE:SetControlPoint( 0, Vector(-908.706,915.851,-63.969) )
	
	end
	
	if GLOB_ABYSS_PORTAL then
	
		if PL:GetPos():Distance(Vector(-908.706,915.851,-63.969)) <= 20 then
			GLOB_ABYSS_PORTAL_GO = true
		else
			GLOB_ABYSS_PORTAL_GO = false
		end
	
	end
	
	if GLOB_ABYSS_PORTAL_GO then
	
		local CT = UnPredictedCurTime()
	
		if not GLOB_ABYSS_PORTAL_WAIT then
			GLOB_ABYSS_PORTAL_WAIT = CT + 4
			GLOB_MUSIC_SILENCE = true
			
			ABYSS_SOUND = CreateSound( PL, Sound("TemporalAssassin/Abyss/Abyss_Ambience.mp3") )
			ABYSS_SOUND:PlayEx(0,100)
			ABYSS_SOUND:ChangeVolume(1, 1.5)
		end
		
		if GLOB_ABYSS_PORTAL_WAIT and CT >= GLOB_ABYSS_PORTAL_WAIT then
			GLOB_ABYSS_PORTAL_WAIT = nil
			RunConsoleCommand("changelevel","abyss")
		end
		
		local data = {}
		data.IgnoreEnabled = true
		data.IgnoreTemporalShift = true
		data.Material = "smoke1"
		data.FadeSpeed = 100
		data.FadeTime = CT + 1.5
		data.Position = {math.Rand(0,ScrW()), math.Rand(0,ScrH())}
		data.PositionDrift = {0,0}
		data.PositionNoise = 0
		data.PositionShake = 0
		data.Scale = {GUI_ScaleX(3),GUI_ScaleY(3)}
		data.ScaleNoise = 0
		data.ScaleNoiseSpeed = 0
		data.ScaleIncrease = 4
		data.Rotation = math.Rand(0,360)
		data.RotationSpeed = 0
		data.Gravity = 0
		data.Alpha = 0
		data.AlphaFlicker = 0
		data.AlphaMax = 250
		data.AlphaPulse = 0
		data.AlphaPulseSpeed = 0
		data.Colour = {0,0,0}
		
		HUDParticles_AddFront(data)
		
		if ABYSS_SOUND then
			ABYSS_SOUND:ChangePitch(99.9,0)
			ABYSS_SOUND:ChangePitch(100,0)
			ABYSS_SOUND:SetSoundLevel(0)
		end
		
	else
			
		if ABYSS_SOUND then
			ABYSS_SOUND:Stop()
			ABYSS_SOUND = nil
		end
	
		if GLOB_ABYSS_PORTAL_WAIT then
			
			GLOB_MUSIC_SILENCE = false
			GLOB_ABYSS_PORTAL_WAIT = nil
		
		end
	
	end

end
hook.Add("Think","HUB_WispThinkHook",HUB_WispThink)