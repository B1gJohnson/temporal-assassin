
if CLIENT then return end

function MAP_LOADED_FUNC()	
	
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")
	
	CreateClothesLocker( Vector(-7699.0869140625,-8200.2041015625,896.03125), Angle(0,268.39605712891,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	else
	
		Create_HarbingerAmmo( Vector(-10801.312, -8385.921, 1024.031), Angle(0, 90.65, 0) )
		Create_Backpack( Vector(-10722.273, -8341.013, 1024.031), Angle(0, 145.826, 0) )
		Create_EldritchArmour( Vector(-10714.732, -8268.731, 1024.031), Angle(0, 167.98, 0) )
		Create_MegaSphere( Vector(-10793.944, -8308.868, 1024.031), Angle(0, 147.08, 0) )
		Create_InfiniteGrenades( Vector(-10672.99, -8257.617, 1024.031), Angle(0, 141.437, 0) )
		Create_Medkit25( Vector(-12976.266, 624.707, 1524.031), Angle(0, 14.672, 0) )
		Create_PortableMedkit( Vector(-12954.277, 696.568, 1524.031), Angle(0, 320.541, 0) )
		Create_Armour( Vector(-12880.351, 569.884, 1524.031), Angle(0, 100.153, 0) )
	
		CreateSecretArea( Vector(-10765.659,-8313.05,1024.031), Vector(-112.057,-112.057,0), Vector(112.057,112.057,70.097) )
	
	end
	
	timer.Simple(5,function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_4.txt","DATA") then
			Create_LoreItem( Vector(-10727.96,-8221.008,1024.031), Angle(0,99.416,0), "lore_mthuulra_4", "Lore File Unlocked: Mthuulra #4" )
		end
	end)
	
	local REMIND_LORE = function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_4.txt","DATA") then
			net.Start("DollMessage")
			net.WriteString("There is another book around here, somewhere")
			net.Broadcast()
		end
	end
	
	CreateTrigger( Vector(-7260.89,-2047.042,491.13), Vector(-6020.377,-6020.377,0), Vector(6020.377,6020.377,3495.911), REMIND_LORE )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)