		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["10_out_robinson"] = {"11_out_combine_resort", Vector(-4143.426,1358.121,-50.54), Vector(-72.444,-72.444,0), Vector(72.444,72.444,74.104)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(2042.064,1800.806,-110.23), Angle(0,-57.362,0))
	
	Create_Medkit25( Vector(2270.671, 404.121, 112.031), Angle(0, 202.102, 0) )
	Create_Beer( Vector(2106.983, 400.03, 63.423), Angle(0, 91.662, 0) )
	Create_FrontlinerWeapon( Vector(204.288, -512.29, 40.133), Angle(2.189, -171.131, 0.234) )
	Create_FrontlinerAmmo( Vector(189.501, -469.395, 40.066), Angle(0, 158.613, 0) )
	Create_Medkit25( Vector(256.696, -365.405, 40.445), Angle(0, 187.829, 0) )
	Create_Armour( Vector(-1761.303, 1119.93, 32.049), Angle(0, 246.613, 0) )
	Create_Armour5( Vector(-1730.078, 1090.376, 43.712), Angle(17.543, -13.418, 24.727) )
	Create_Armour200( Vector(3457.814, 3503.228, 100.205), Angle(-67.765, -12.512, 8.598) )
	Create_M6GWeapon( Vector(3471.407, 3505.002, 90.976), Angle(11.11, -8.072, 0.56) )
	Create_InfiniteGrenades( Vector(3450.714, 3409.837, 92.334), Angle(3.056, 66.703, 5.955) )
	Create_GrenadeAmmo( Vector(3471.725, 3406.659, 89.526), Angle(0, 44.389, 0) )
	Create_M6GAmmo( Vector(3476.406, 3524.102, 89.1), Angle(-2.271, -95.631, -4.659) )
	Create_ShotgunAmmo( Vector(2188.581, 268.585, 16.031), Angle(0, 80.224, 0) )

	CreateSecretArea( Vector(3426.719,3454.858,92.796), Vector(-70.966,-70.966,0), Vector(70.966,70.966,63.29) )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)