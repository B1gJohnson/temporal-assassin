
if SERVER then return end

DEV_GAMEOVER = true
GLOB_MUSIC_SILENCE = true

if MAP_NICENAMES then
	MAP_NICENAMES["whitespace"] = "?????"
end

function GetWhiteSpaceVolume(ply)

	local POS = ply:GetPos()
	POS.x = 0
	POS.z = 0

	local TARGET = Vector(0,-2034,0)
	
	local VOLUME = math.Clamp(1 - (POS:Distance(TARGET)/10500),0,1)*1
	
	return VOLUME
	
end

function WhiteSpace_Think()

	local PL = LocalPlayer()
		
	if PL and IsValid(PL) then
	
		if not PL:Alive() then
		
			PL.WAIT_INIT_TIME = CurTime() + 3
			PL.RGStare = false
			PL.RGStare_Stopped = false
			
			if PL.WhiteSpace_Music then
				PL.WhiteSpace_Music:Stop()
				PL.WhiteSpace_Music = nil
			end
		
		else
		
			if PL.WAIT_INIT_TIME and CurTime() < PL.WAIT_INIT_TIME then return end
		
			if not PL.WhiteSpace_Music then
				PL.WhiteSpace_Music = CreateSound( PL, "TemporalAssassin/Maps/WhiteSpace_Loop.mp3" )
				PL.WhiteSpace_Music:PlayEx(0,100)
				net.Start("WhiteSpace_Loaded")
				net.SendToServer()
			else
			
				if PL.RGStare then
				
					if not PL.RGStare_Stopped then
						PL.RGStare_Stopped = true
						PL.WhiteSpace_Music:Play()
						PL.WhiteSpace_Music:ChangeVolume( 0, 1 )
						PL.WhiteSpace_Music2 = CreateSound( PL, "TemporalAssassin/Weapons/AutoShotgun/OpenLoop.ogg" )
						PL.WhiteSpace_Music2:PlayEx( 0, 100 )
						PL.WhiteSpace_Music2:ChangeVolume( 0.3, 2 )
					end
					
					if PL.WhiteSpace_Music2 then
						PL.WhiteSpace_Music2:Play()
						PL.WhiteSpace_Music2:ChangeVolume( 0.3, 0 )
					end
					
				else
				
					PL.WhiteSpace_Music:Play()
					PL.WhiteSpace_Music:ChangeVolume( GetWhiteSpaceVolume(PL), 0 )
					
				end
				
			end
		
		end
		
	end

end
hook.Add("Think","WhiteSpace_ThinkHook",WhiteSpace_Think)