		
if CLIENT then return end

util.AddNetworkString("SIN_UpdateCount")
util.AddNetworkString("SIN_AskForGoal")
util.AddNetworkString("SIN_SinOnMap")

function SIN_AskForGoal_LIB( len, pl )

	net.Start("SIN_UpdateCount")
	net.WriteUInt(LAST_SIN_COUNT,6)
	net.WriteUInt(SIN_COUNT_GOAL,6)
	net.Broadcast()

end
net.Receive("SIN_AskForGoal",SIN_AskForGoal_LIB)

GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true

GLOB_NO_FENIC = false
GLOB_NO_SINS = true

GLOB_SINHUNTER_SPAWN1 = false
GLOB_SINHUNTER_SPAWN2 = false
GLOB_SINHUNTER_SPAWNNPCS = 0
GLOB_SINHUNTER_ENEMYCOUNT = 0
GLOB_SINFUL_ENEMY_COUNT = 0

SIN_SPAWN_SPEED = 5
SIN_SPAWN_MAX = 40
SIN_ENEMIES_KILLED = 0
SIN_SINSPAWN_THRESHOLD = 20

local SPAWN_DIFFICULTIES = {}
SPAWN_DIFFICULTIES[1] = {5,30,20}
SPAWN_DIFFICULTIES[2] = {4,40,25}
SPAWN_DIFFICULTIES[3] = {3,50,30}
SPAWN_DIFFICULTIES[4] = {3,50,30}
SPAWN_DIFFICULTIES[5] = {2.5,60,35}
SPAWN_DIFFICULTIES[6] = {2,60,35}
SPAWN_DIFFICULTIES[7] = {1.5,60,35}
SPAWN_DIFFICULTIES[8] = {1.5,70,40}
SPAWN_DIFFICULTIES[9] = {1,80,45}
SPAWN_DIFFICULTIES[10] = {1,100,50}
SPAWN_DIFFICULTIES[11] = {1,120,50}
SPAWN_DIFFICULTIES[12] = {1,120,50} --[[ Just incase more difficulties ARE added ]]
SPAWN_DIFFICULTIES[13] = {1,120,50}
SPAWN_DIFFICULTIES[14] = {1,120,50}

GLOB_SINTYPE = math.random(1,5)
--[[
This defines what enemies will spawn for this sin hunter mission.

1 = Combine
2 = Zombies
3 = Antlions
4 = HL:S Humans
5 = HL:S Aliens

You can also define it as GLOB_SINTYPE = math.random(1,5) to make it random everytime its played
]]

local SIN_CLASSES = {}

SIN_CLASSES[1] = {
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s"}

if IsMounted("ep2") then
	SIN_CLASSES[1] = {
	"npc_metropolice",
	"npc_combine_s",
	"npc_metropolice",
	"npc_combine_s",
	"npc_metropolice",
	"npc_combine_s"}
end

SIN_CLASSES[2] = {
"npc_zombie",
"npc_zombie",
"npc_zombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_poisonzombie"}

SIN_CLASSES[3] = {
"npc_antlion"}

if IsMounted("ep2") then
	SIN_CLASSES[3] = {
	"npc_antlion",
	"npc_antlion",
	"npc_antlion",
	"npc_antlion_worker"}
end

SIN_CLASSES[4] = SIN_CLASSES[1]
SIN_CLASSES[5] = SIN_CLASSES[2]

if IsMounted("hl1") then

	SIN_CLASSES[4] = {
	"monster_human_grunt"}
	
	SIN_CLASSES[5] = {
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullsquid",
	"monster_houndeye",
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullchicken",
	"monster_houndeye"}
	
end

local BIGSIN_CLASSES = {}

BIGSIN_CLASSES[1] = {
"npc_combine_s"}

if IsMounted("ep2") then
	BIGSIN_CLASSES[1] = {
	"npc_hunter"}
end

BIGSIN_CLASSES[2] = {
"npc_poisonzombie"}

if IsMounted("ep2") or IsMounted("episodic") then
	BIGSIN_CLASSES[2] = {
	"npc_zombine"}
end

BIGSIN_CLASSES[3] = {
"npc_antlionguard"}
BIGSIN_CLASSES[4] = BIGSIN_CLASSES[1]
BIGSIN_CLASSES[5] = BIGSIN_CLASSES[2]

if IsMounted("hl1") then
	BIGSIN_CLASSES[4] = {
	"monster_human_assassin"}
	
	BIGSIN_CLASSES[5] = {
	"monster_alien_slave"}
end

local SIN_WEAPONS = {}
SIN_WEAPONS["npc_metropolice"] = {
"weapon_pistol",
"weapon_smg1",
"weapon_stunstick"}
SIN_WEAPONS["npc_combine_s"] = {
"weapon_smg1",
"weapon_ar2",
"weapon_shotgun"}

function SetAsRespawningItem( ent, del )

	if not ent then return end
	if not ent.ITEM_USE then return end
	
	local POS,ANG,MDL,USE = ent:GetPos(), ent:GetAngles(), ent:GetModel(), ent.ITEM_USE
	
	if not del then del = 30 end
	
	ent.OnRemove = function()
	
		timer.Simple(del,function()
		
			local N_ITEM = CreateItemPickup( POS, ANG, MDL, USE )
			CreatePointParticle( "Doll_EvilSpawn_Activate", POS, POS, Angle(0,0,0), Angle(0,0,0), 1 )
			sound.Play( Sound("TemporalAssassin/Doll/ExtraSpawn_Item.wav"), POS, 70, math.random(99,101), 0.7 )
			SetAsRespawningItem(N_ITEM,del)
			
		end)
	
	end

end

LAST_SIN_COUNT = 0
SIN_COUNT_GOAL = 5

function CanWeSpawnHere(pos)

	local TR = {}
	TR.start = pos
	TR.endpos = pos
	TR.mask = MASK_NPCSOLID
	TR.filter = {}
	table.Add(TR.filter,GLOB_TRIGGER_ENTS)
	TR.mins = Vector(-16,-16,0)
	TR.maxs = Vector(16,16,72)
	local Trace = util.TraceHull(TR)
	
	if Trace.Hit then return false end
	
	return true

end

function EnemySpawns_Think()

	local CT = CurTime()
	
	if SIN_ENEMIES_KILLED and SIN_ENEMIES_KILLED > SIN_SINSPAWN_THRESHOLD then
		SIN_ENEMIES_KILLED = 0
		Create_SinfulBoi()
	end
	
	net.Start("SIN_SinOnMap")
	if GLOB_SINFUL_ENEMY_COUNT and GLOB_SINFUL_ENEMY_COUNT > 0 then
		net.WriteBool(true)
	else
		net.WriteBool(false)
	end
	net.Broadcast()
	
	if GLOB_SIN_DEVOURED_COUNT and GLOB_SIN_DEVOURED_COUNT > LAST_SIN_COUNT then
	
		LAST_SIN_COUNT = GLOB_SIN_DEVOURED_COUNT
		
		local SIN_LEFT = (SIN_COUNT_GOAL - LAST_SIN_COUNT)
		
		net.Start("SIN_UpdateCount")
		net.WriteUInt(LAST_SIN_COUNT,6)
		net.WriteUInt(SIN_COUNT_GOAL,6)
		net.Broadcast()
		
		if SIN_LEFT <= 0 and not SIN_GAME_ENDED then
			SIN_GAME_ENDED = true
			END_SIN_HUNTER()			
		end
		
	end
	
	if GLOB_SINHUNTER_SPAWNNPCS and CT >= GLOB_SINHUNTER_SPAWNNPCS and GLOB_SINHUNTER_STARTED then
	
		GLOB_SINHUNTER_SPAWNNPCS = CT + SIN_SPAWN_SPEED
	
		local SPAWN_POS
	
		if GLOB_SINHUNTER_SPAWN2 then
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_FRONT) + Vector(0,0,5)
		else
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_BACK) + Vector(0,0,5)
		end
		
		if SPAWN_POS and GLOB_SINHUNTER_ENEMYCOUNT < SIN_SPAWN_MAX and GLOB_SINTYPE and SIN_CLASSES[GLOB_SINTYPE] then
		
			local CLASS = table.Random(SIN_CLASSES[GLOB_SINTYPE])
			local WEP = false
			
			if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
				WEP = table.Random(SIN_WEAPONS[CLASS])
			end
			
			if CanWeSpawnHere(SPAWN_POS) then
				CreateSnapNPC( CLASS, SPAWN_POS, Angle(0,math.random(-360,360),0), WEP, false )
				GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT + 1
			end
		
		end
	
	end

end
timer.Create("EnemySpawns_ThinkTimer", 0.5, 0, EnemySpawns_Think)

function Set_EnemySpawns_1()

	print("Back of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = true
	GLOB_SINHUNTER_SPAWN2 = false

end

function Set_EnemySpawns_2()

	print("Front of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = true

end

function START_SIN_HUNTER()

	RemoveAllTeleporters()
	
	local STP1, STP2 = CreateTeleporter( "SEC_1", Vector(1422.88,-4320.302,1344.031), "SEC_2", Vector(-590.438,2248.069,576.031) )
	
	local RemTelSecret1 = function()
	
		local RemTelSecret2 = function()
			if STP1 and STP2 then
				STP1:Remove()
				STP2:Remove()
			end
		end
		
		CreateTrigger( Vector(-591.704,2263.37,576.031), Vector(-94.597,-94.597,0), Vector(94.597,94.597,124.068), RemTelSecret2 )
	
	end
	
	CreateTrigger( Vector(1433.249,-4351.768,1344.031), Vector(-223.374,-223.374,0), Vector(223.374,223.374,116.818), RemTelSecret1 )
	
	--[[ This is the trigger that enables Back side map spawns ]]	
	CreateTrigger( Vector(-95.697,-2252.737,-1471.017), Vector(-3549.594,-3549.594,0), Vector(3549.594,3549.594,6495.496), Set_EnemySpawns_1, true )
	
	--[[ This is the trigger that enables Front side map spawns ]]
	CreateTrigger( Vector(-618.727,6388.354,-2975.045), Vector(-4832.996,-4832.996,0), Vector(4832.996,4832.996,10731.443), Set_EnemySpawns_2, true )
	
	GLOB_SINHUNTER_STARTED = true
	GLOB_SINHUNTER_SPAWNNPCS = CurTime() + 20
	
	local DIFF = GetDifficulty2()

	if SPAWN_DIFFICULTIES and SPAWN_DIFFICULTIES[DIFF] then
		SIN_SPAWN_SPEED = SPAWN_DIFFICULTIES[DIFF][1]
		SIN_SPAWN_MAX = SPAWN_DIFFICULTIES[DIFF][2]
		SIN_SINSPAWN_THRESHOLD = SPAWN_DIFFICULTIES[DIFF][3]
	end
	
end

function END_SIN_HUNTER()

	GLOB_SINHUNTER_STARTED = false

	for _,v in pairs (ents.GetAll()) do
	
		if v and IsValid(v) then
		
			if v:IsNPC() then
				v:Remove() --[[ Lets remove all the npcs ]]
			end
		
		end
	
	end
	
	GLOB_SINFUL_ENEMY_COUNT = 0
	net.Start("SIN_SinOnMap")
	net.WriteBool(false)
	net.Broadcast()
	
	CreateTeleporter( "START_1", Vector(69.402,-1253.25,-687.969), "START_2", Vector(74.969,-602.434,-191.969) )
	
	local GAME_END_TEXT = {
	""}

	MakeGameEnd( GAME_END_TEXT, 3, Vector(74.453,-1266.303,-687.969), Vector(-196.195,-196.195,0), Vector(196.195,196.195,120.771) )
	
end

function Create_SinfulBoi()

	local SPAWNS_BACK = {
	Vector(-299.254,2234.258,-40.82),
	Vector(158.289,2226.207,13.146),
	Vector(-861.462,2215.083,15.483)}
	
	local SPAWNS_FRONT = {
	Vector(75.588,-410.975,12.718),
	Vector(49.168,262.48,12.472),
	Vector(81.826,-21.793,-52.642)}
	
	local SPAWN_POINT
	
	if GLOB_SINHUNTER_SPAWN2 then
		SPAWN_POINT = table.Random(SPAWNS_FRONT)
	elseif GLOB_SINHUNTER_SPAWN1 then
		SPAWN_POINT = table.Random(SPAWNS_BACK)
	end
	
	if not SPAWN_POINT then return end
	if not GLOB_SINTYPE then return end
	if not BIGSIN_CLASSES then return end
	if not BIGSIN_CLASSES[GLOB_SINTYPE] then return end
	
	local CLASS = table.Random(BIGSIN_CLASSES[GLOB_SINTYPE])
	local WEP = false
			
	if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
		WEP = table.Random(SIN_WEAPONS[CLASS])
	end
	
	GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT + 1
	
	local SIN_BOI = CreateSnapNPC( CLASS, SPAWN_POINT, Angle(0,math.random(-360,360),0), WEP, false )
			
	timer.Simple(0.1,function()
		if SIN_BOI and IsValid(SIN_BOI) then
			SIN_BOI:SetHealth( SIN_BOI:Health()*2 )
			SIN_BOI:CreateSinner(true)
			SIN_BOI:SetModelScale(1.2,0) --[[ Lets make sin bois a bit bigger in scale ]]
			if GLOB_SINTYPE == 1 then
				SIN_BOI:MakeCombineShielder()
			end
		end
	end)

end

function SinHunter_NPCDeath( npc, att, inf )

	if not npc.SinHealth then
		SIN_ENEMIES_KILLED = SIN_ENEMIES_KILLED + 1
		GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT - 1
	elseif npc.SinHealth then
		GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT - 1
	end

end
hook.Add("OnNPCKilled","SinHunter_NPCDeathHook",SinHunter_NPCDeath)

function SINHUNTER_DEATH_RESET()

	if #player.GetAll() <= 1 then

		GLOB_NO_SAVEDATA = true
		GLOB_SAFE_GAMEEND = true
		
		GLOB_NO_FENIC = false
		GLOB_NO_SINS = true
		
		GLOB_SINHUNTER_SPAWN1 = false
		GLOB_SINHUNTER_SPAWN2 = false
		GLOB_SINHUNTER_SPAWNNPCS = 0
		GLOB_SINHUNTER_ENEMYCOUNT = 0
		GLOB_SINFUL_ENEMY_COUNT = 0
		
		SIN_SPAWN_SPEED = 5
		SIN_SPAWN_MAX = 40
		SIN_ENEMIES_KILLED = 0
		SIN_SINSPAWN_THRESHOLD = 20
		
		GLOB_SIN_DEVOURED_COUNT = 0
		LAST_SIN_COUNT = 0
		SIN_COUNT_GOAL = 5
		
		GLOB_SINHUNTER_STARTED = false
		GLOB_SINHUNTER_SPAWNNPCS = false
		SIN_GAME_ENDED = false
		
	end

end
hook.Add("PlayerDeath","SINHUNTER_DEATH_RESETHOOK",SINHUNTER_DEATH_RESET)

function MAP_LOADED_FUNC()

	GLOB_NO_SAVEDATA = true
	GLOB_SAFE_GAMEEND = true
	
	GLOB_NO_FENIC = false
	GLOB_NO_SINS = true
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = false
	GLOB_SINHUNTER_SPAWNNPCS = 0
	GLOB_SINHUNTER_ENEMYCOUNT = 0
	GLOB_SINFUL_ENEMY_COUNT = 0
	
	SIN_SPAWN_SPEED = 5
	SIN_SPAWN_MAX = 40
	SIN_ENEMIES_KILLED = 0
	SIN_SINSPAWN_THRESHOLD = 20
	
	GLOB_SIN_DEVOURED_COUNT = 0
	LAST_SIN_COUNT = 0
	SIN_COUNT_GOAL = 5
	
	GLOB_SINHUNTER_STARTED = false
	GLOB_SINHUNTER_SPAWNNPCS = false
	SIN_GAME_ENDED = false
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	RemoveAllEnts("ambient_generic")
	
	CreateProp(Vector(1643.5,-4582.469,1344.438), Angle(0.044,124.937,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
	
	CreatePlayerSpawn( Vector(91.27,-1216.644,-687.969), Angle(0,90,0) )
	
	CreateClothesLocker( Vector(-140.381,-749.573,-687.969), Angle(0,270.513,0) )
	
	Create_ShotgunWeapon( Vector(-854.006, -459.846, -191.969), Angle(0, 0.118, 0) )
	Create_SMGWeapon( Vector(-494.111, 36.121, 64.031), Angle(0, 271.708, 0) )
	Create_RifleWeapon( Vector(908.622, 1238.894, -191.969), Angle(0, 270.778, 0) )
	Create_RevolverWeapon( Vector(650.319, 3635.774, -267.969), Angle(0, 180.792, 0) )
	Create_FrontlinerWeapon( Vector(-1415.485, 1974.506, 64.031), Angle(0, 129.85, 0) )
	Create_KingSlayerWeapon( Vector(-1546.185, 882.177, -343.969), Angle(0, 90.9, 0) )
	Create_SuperShotgunWeapon( Vector(86.381, 11.301, -447.969), Angle(0, 90.126, 0) )
	Create_CrossbowWeapon( Vector(-296.186, 2233.442, -343.969), Angle(0, 268.515, 0) )
	Create_PistolWeapon( Vector(-1503.514, 1614.919, -343.969), Angle(0, 0.095, 0) )
	
	local HEALING = {
	Create_Medkit10( Vector(-1213.191, 1015.825, 64.031), Angle(0, 63.75, 0) ),
	Create_ManaCrystal( Vector(-836.156, 1031.525, 64.031), Angle(0, 272.145, 0) ),
	Create_ManaCrystal( Vector(-836.67, 1228.672, 64.031), Angle(0, 270.22, 0) ),
	Create_Medkit10( Vector(290.299, 523.451, 68.031), Angle(0, 93.324, 0) ),
	Create_Medkit10( Vector(-1251.047, -637.49, -191.969), Angle(0, 1.089, 0) ),
	Create_ManaCrystal( Vector(-1140.3, 737.622, -6.815), Angle(0, 265.03, 0) ),
	Create_Medkit10( Vector(-998.203, 2586.074, -267.969), Angle(0, 359.11, 0) ),
	Create_Medkit10( Vector(-2134.181, 484.111, -87.969), Angle(0, 31.17, 0) ),
	Create_ManaCrystal( Vector(-1497.623, 2500.343, 64.031), Angle(0, 201.812, 0) ),
	Create_ManaCrystal( Vector(-1633.924, 2420.463, 64.031), Angle(0, 121.953, 0) ),
	Create_Medkit25( Vector(-1476.143, -339.176, -87.969), Angle(0, 91.037, 0) ),
	Create_Medkit10( Vector(-165.882, -616.283, 68.031), Angle(0, 180.192, 0) ),
	Create_Medkit10( Vector(-165.836, -595.346, 68.031), Angle(0, 180.027, 0) ),
	Create_Medkit10( Vector(-111.903, 521.275, 68.031), Angle(0, 90.928, 0) ),
	Create_Medkit25( Vector(1030.866, 683.918, 64.031), Angle(0, 181.183, 0) )}
	
	local AMMO_ENTS = {
	Create_SMGAmmo( Vector(-448.627, 11.414, 64.031), Angle(0, 269.838, 0) ),
	Create_SMGAmmo( Vector(-530.697, 51.781, 64.031), Angle(0, 272.368, 0) ),
	Create_SMGAmmo( Vector(-529.032, 27.119, 64.031), Angle(0, 271.928, 0) ),
	Create_SMGAmmo( Vector(-525.077, 5.151, 64.031), Angle(0, 187.203, 0) ),
	Create_ShotgunAmmo( Vector(-867.497, -402.27, -191.969), Angle(0, 0.283, 0) ),
	Create_ShotgunAmmo( Vector(-843.085, -402.497, -191.969), Angle(0, 0.558, 0) ),
	Create_ShotgunAmmo( Vector(-868.218, -513.174, -191.969), Angle(0, 358.633, 0) ),
	Create_ShotgunAmmo( Vector(-845.428, -513.242, -191.969), Angle(0, 358.358, 0) ),
	Create_RifleAmmo( Vector(966.605, 1258.155, -191.969), Angle(0, 270.613, 0) ),
	Create_RifleAmmo( Vector(966.905, 1230.134, -191.969), Angle(0, 270.613, 0) ),
	Create_RifleAmmo( Vector(967.266, 1203.356, -191.969), Angle(0, 270.558, 0) ),
	Create_RevolverAmmo( Vector(665.392, 3585.958, -267.969), Angle(0, 181.067, 0) ),
	Create_RevolverAmmo( Vector(644.299, 3585.955, -267.969), Angle(0, 181.342, 0) ),
	Create_RevolverAmmo( Vector(620.067, 3585.826, -267.969), Angle(0, 181.782, 0) ),
	Create_PistolAmmo( Vector(-1503.48, 1593.809, -343.969), Angle(0, 0.095, 0) ),
	Create_PistolAmmo( Vector(-1510.265, 1593.987, -340.937), Angle(0, 359.985, 0) ),
	Create_PistolAmmo( Vector(-1485.448, 1594.122, -341.036), Angle(0, 359.875, 0) ),
	Create_KingSlayerAmmo( Vector(-1612.021, 853.843, -343.969), Angle(0, 90.405, 0) ),
	Create_KingSlayerAmmo( Vector(-1613.419, 920.784, -343.969), Angle(0, 89.635, 0) ),
	Create_CrossbowAmmo( Vector(-232.464, 2274.908, -343.969), Angle(0, 269.23, 0) ),
	Create_CrossbowAmmo( Vector(-233.426, 2227.827, -343.969), Angle(0, 269.45, 0) ),
	Create_CrossbowAmmo( Vector(-234.436, 2190.772, -343.969), Angle(0, 270.22, 0) )}
	
	local AMMO_S_ENTS = {	
	Create_HarbingerAmmo( Vector(-1487.405, 1114.482, 564.031), Angle(0, 268.777, 0) ),
	Create_FoolsOathAmmo( Vector(386.022, 2829.696, 372.031), Angle(0, 268.973, 0) )}
	
	for _,v in pairs (HEALING) do
		SetAsRespawningItem(v,30)
	end
	
	for _,v in pairs (AMMO_ENTS) do
		SetAsRespawningItem(v,60)
	end
	
	for _,v in pairs (AMMO_S_ENTS) do
		SetAsRespawningItem(v,120)
	end
	
	Create_Armour100( Vector(-70.553, -750.152, -687.969), Angle(0, 271.183, 0) )
	Create_Backpack( Vector(-20.976, -749.129, -687.969), Angle(0, 271.183, 0) )
	Create_Backpack( Vector(16.607, -748.363, -687.969), Angle(0, 270.523, 0) )
	Create_EldritchArmour( Vector(-117.096, -711.162, 420.031), Angle(0, 355.827, 0) )
	Create_InfiniteGrenades( Vector(377.231, -753.113, -687.969), Angle(0, 271.018, 0) )
	Create_Beer( Vector(290.437, -407.16, -447.969), Angle(0, 270.278, 0) )
	Create_Beer( Vector(290.412, -434.654, -447.969), Angle(0, 270.388, 0) )
	Create_Beer( Vector(290.139, -455.465, -447.969), Angle(0, 270.773, 0) )
	Create_Beer( Vector(289.907, -477.567, -447.969), Angle(0, 271.543, 0) )
	Create_Beer( Vector(294.519, -501.035, -447.969), Angle(0, 181.123, 0) )
	Create_Beer( Vector(292.057, -534.38, -447.969), Angle(0, 182.938, 0) )
	Create_Beer( Vector(289.907, -566.996, -447.969), Angle(0, 158.738, 0) )
	Create_ShotgunAmmo( Vector(-824.051, -402.413, -191.969), Angle(0, 0.668, 0) )
	Create_ShotgunAmmo( Vector(-824.727, -513.082, -191.969), Angle(0, 357.808, 0) )
	Create_SMGAmmo( Vector(-449.14, 59.93, 64.031), Angle(0, 270.223, 0) )
	Create_SMGAmmo( Vector(-448.498, 33.22, 64.031), Angle(0, 269.783, 0) )
	Create_Beer( Vector(-1258.388, 113.849, -70.556), Angle(0, 180.933, 0) )
	Create_Beer( Vector(-1200.149, 114.62, -38.934), Angle(0, 180.878, 0) )
	Create_Beer( Vector(-1107.343, 114.716, 11.455), Angle(0, 180.603, 0) )
	Create_Beer( Vector(-1017.796, 116.231, 60.075), Angle(0, 296.707, 0) )
	Create_Medkit10( Vector(80.166, 1653.187, -80.969), Angle(0, 90.408, 0) )
	Create_Medkit10( Vector(80.198, 1932.103, -80.969), Angle(0, 269.623, 0) )
	Create_Armour100( Vector(-839.76, 1124.2, 64.031), Angle(0, 90.958, 0) )
	Create_Armour100( Vector(1091.859, 770.561, -447.969), Angle(0, 359.375, 0) )
	Create_Beer( Vector(-575.873, 580.02, -447.969), Angle(0, 91.026, 0) )
	Create_Beer( Vector(-563.359, 583.395, -444.937), Angle(0, 100.101, 0) )
	Create_Beer( Vector(-589.456, 584.742, -444.937), Angle(0, 81.071, 0) )
	Create_Beer( Vector(-581.83, 583.125, -441.906), Angle(0, 86.736, 0) )
	Create_Beer( Vector(-569.988, 587.514, -438.875), Angle(0, 95.646, 0) )
	Create_FalanranWeapon( Vector(664.309, -512.34, 108.031), Angle(0, 179.686, 0) )
	Create_WhisperWeapon( Vector(-1486.269, 971.565, 564.031), Angle(0, 270.597, 0) )
	Create_WhisperAmmo( Vector(-1486.269, 891.659, 564.031), Angle(0, 270.597, 0) )
	Create_WhisperAmmo( Vector(-1486.181, 862.175, 564.031), Angle(0, 270.597, 0) )
	Create_WhisperAmmo( Vector(-1487.363, 1034.069, 564.031), Angle(0, 269.938, 0) )
	Create_WhisperAmmo( Vector(-1487.246, 1055.41, 564.031), Angle(0, 269.773, 0) )
	Create_MegaSphere( Vector(-1487.684, 1417.298, 564.031), Angle(0, 271.422, 0) )
	Create_HarbingerWeapon( Vector(-1486.264, 1168.548, 564.031), Angle(0, 270.702, 0) )
	Create_HarbingerAmmo( Vector(-1485.394, 1236.384, 564.031), Angle(0, 269.547, 0) )
	Create_HarbingerAmmo( Vector(-1486.499, 1084.738, 564.031), Angle(0, 269.382, 0) )
	Create_HarbingerAmmo( Vector(-1485.425, 1267.801, 564.031), Angle(0, 271.527, 0) )
	Create_MegaSphere( Vector(-1488.134, 740.499, 564.031), Angle(0, 90.332, 0) )
	Create_Backpack( Vector(-1725.464, 2661.815, -267.969), Angle(0, 195.763, 0) )
	Create_PortableMedkit( Vector(382.796, 2959.865, -38.202), Angle(0, 270.783, 0) )
	Create_FoolsOathWeapon( Vector(355.451, 2779.193, 372.031), Angle(0, 358.863, 0) )
	Create_FoolsOathAmmo( Vector(326.724, 2830.28, 372.031), Angle(0, 269.248, 0) )
	Create_FalanrathThorn( Vector(-304.837, 2236.428, 576.031), Angle(0, 269.891, 0) )
	Create_PortableMedkit( Vector(-1797.462, 829.819, -343.969), Angle(0, 135.032, 0) )
	Create_Bread( Vector(-2026.42, 384.736, -87.969), Angle(0, 88.447, 0) )
	Create_Armour( Vector(179.849, 3809.963, -267.969), Angle(0, 269.892, 0) )
	Create_Armour( Vector(537.987, 3815.53, -267.969), Angle(0, 266.647, 0) )
	Create_Medkit10( Vector(687.277, 2350.81, -191.969), Angle(0, 358.962, 0) )
	Create_ReikoriKey( Vector(81.422, 316.979, 794.768), Angle(0, 179.227, 0) )
	Create_SpiritSphere( Vector(77.904, 33.279, 454.031), Angle(0, 268.902, 0) )
	Create_Armour200( Vector(77.442, 479.424, 462.031), Angle(0, 268.847, 0) )
	Create_ShotgunAmmo( Vector(38.849, 19.659, -447.969), Angle(0, 91.226, 0) )
	Create_ShotgunAmmo( Vector(137.387, 21.409, -447.969), Angle(0, 89.851, 0) )
	Create_Bread( Vector(-1681.323, 1522.985, -217.969), Angle(0, 269.071, 0) )
	Create_Bread( Vector(-1614.451, 1523.828, -217.969), Angle(0, 269.731, 0) )
	Create_Armour5( Vector(-1771.958, 2632.543, -267.969), Angle(0, 122.911, 0) )
	Create_EldritchArmour10( Vector(44.642, 3551.162, 3.031), Angle(0, 358.71, 0) )
	Create_EldritchArmour10( Vector(694.845, 3551.138, 3.031), Angle(0, 180.705, 0) )
	Create_Medkit25( Vector(356.123, 2341.926, -343.969), Angle(0, 179.745, 0) )
	Create_PortableMedkit( Vector(326.4, 2052.425, -343.969), Angle(0, 316.336, 0) )
	Create_PortableMedkit( Vector(-50.661, 1577.058, -343.969), Angle(0, 179.031, 0) )
	Create_EldritchArmour10( Vector(-1408.452, -2.602, 136.431), Angle(0, 270.415, 0) )
	Create_Backpack( Vector(-372.89, 799.642, -447.969), Angle(0, 359.875, 0) )
	Create_Bread( Vector(-2153.357, 622.298, -87.969), Angle(0, 0.48, 0) )
	Create_ShotgunAmmo( Vector(-1035.883, 1975.823, -343.969), Angle(0, 37.72, 0) )
	Create_SMGAmmo( Vector(267.761, 1792.949, -343.969), Angle(0, 182.15, 0) )
	Create_ShotgunAmmo( Vector(-110.564, 1791.213, -343.969), Angle(0, 358.479, 0) )
	Create_FrontlinerAmmo( Vector(-1457.549, 1942.02, 64.031), Angle(0, 127.595, 0) )
	Create_GrenadeAmmo( Vector(-698.606, 134.233, 64.141), Angle(0, 272, 0) )
	Create_RaikiriWeapon( Vector(-591.242, 2251.941, 576.031), Angle(0, 272.633, 0) )

	CreateTeleporter( "START_1", Vector(498.532,-858.836,-687.969), "START_2", Vector(74.969,-602.434,-191.969) )
	
	--[[ The trigger to start the mode ]]
	CreateTrigger( Vector(73.29,-612.463,-191.969), Vector(-67.99,-67.99,0), Vector(67.99,67.99,103.303), START_SIN_HUNTER )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

SINHUNTER_SPAWNS_FRONT = {
Vector(1096.371,-566.325,-191.969),
Vector(1095.112,-457.311,-191.969),
Vector(1094.285,-285.766,-191.969),
Vector(1087.736,-113.508,-191.969),
Vector(993.41,10.386,-191.969),
Vector(841.58,-43.439,-191.969),
Vector(832.104,-236.285,-191.969),
Vector(846.66,-402.672,-191.969),
Vector(947.172,-314.176,-191.969),
Vector(955.275,-123.498,-191.969),
Vector(855.607,63.033,-158.086),
Vector(846.053,251.704,-15.772),
Vector(846.756,455.507,64.031),
Vector(998.802,498.823,64.031),
Vector(865.57,630.497,64.031),
Vector(836.537,728.742,64.031),
Vector(1014.378,768.354,68.031),
Vector(985.459,533.053,-191.969),
Vector(812.241,521.943,-191.969),
Vector(713.513,557.455,-191.969),
Vector(741.342,684.1,-191.969),
Vector(893.942,728.929,-191.969),
Vector(1000.562,786.04,-191.969),
Vector(980.634,977.352,-191.969),
Vector(860.428,1065.218,-191.969),
Vector(755.11,683.963,-191.969),
Vector(729.408,593.068,-191.969),
Vector(733.484,471.969,-191.969),
Vector(627.317,604.032,-191.969),
Vector(566.388,537.605,-191.969),
Vector(517.49,379.013,-191.969),
Vector(516.423,160.122,-191.969),
Vector(526.468,-26.883,-191.969),
Vector(533.851,-222.768,-191.969),
Vector(538.205,-429.243,-191.969),
Vector(515.086,-560.539,-191.969),
Vector(384.001,-560.431,-191.969),
Vector(366.906,-438.108,-191.969),
Vector(376.978,-273.602,-191.969),
Vector(389.99,-61.271,-191.969),
Vector(389.073,124.273,-191.969),
Vector(391.028,311.556,-191.969),
Vector(303.498,691.149,-191.969),
Vector(228.444,606.949,-191.969),
Vector(160.149,657.143,-191.969),
Vector(106.611,620.751,-191.969),
Vector(12.858,604.391,-191.969),
Vector(-39.431,681.489,-191.969),
Vector(-132.972,682.047,-191.969),
Vector(-151.338,616.959,-191.969),
Vector(-211.912,581.167,-191.969),
Vector(-266.861,658.498,-191.969),
Vector(-347.407,630.671,-191.969),
Vector(-441.998,546.34,-191.969),
Vector(-548.527,487.072,-191.969),
Vector(-640.478,369.683,-191.969),
Vector(-592.549,295.779,-191.969),
Vector(-454.441,281.365,-191.969),
Vector(-327.266,270.633,-191.969),
Vector(-244.085,277.316,-191.969),
Vector(-240.743,107.149,-191.969),
Vector(-339.218,99.671,-191.969),
Vector(-509.851,104.047,-191.969),
Vector(-643.219,148.407,-191.969),
Vector(-764.61,239.1,-191.969),
Vector(-895.15,210.751,-191.969),
Vector(-940.781,153.251,-191.969),
Vector(-1070.77,152.069,-232.85),
Vector(-1255.685,209.843,-325.307),
Vector(-1355.418,195.59,-343.969),
Vector(-1409.36,138.482,-343.969),
Vector(-1503.11,206.227,-343.969),
Vector(-1565.783,278.046,-343.969),
Vector(-1584.647,347.663,-343.969),
Vector(-1558.659,417.152,-343.969),
Vector(-1454.639,450.377,-343.969),
Vector(-1465.531,547.309,-343.969),
Vector(-1523.986,605.468,-343.969),
Vector(-1599.402,696.241,-343.969),
Vector(-1679.338,854.132,-343.969),
Vector(-1698.834,945.549,-343.969),
Vector(-1004.761,707.001,-385.891),
Vector(-964.699,857.011,-406.315),
Vector(-822.507,967.209,-447.969),
Vector(-818.514,865.244,-447.969),
Vector(-813.389,751.374,-447.969),
Vector(-772.257,636.485,-447.969),
Vector(-655.563,636.385,-447.969),
Vector(-605.895,737.846,-447.969),
Vector(-591.618,867.737,-447.969),
Vector(-569.78,940.767,-447.969),
Vector(-470.783,948.408,-447.969),
Vector(-416.639,917.579,-447.969),
Vector(-433.434,828.285,-447.969),
Vector(-440.864,720.453,-447.969),
Vector(-389.596,627.181,-447.969),
Vector(-283.802,626.558,-447.969),
Vector(-205.455,696.051,-447.969),
Vector(-205.168,815.333,-447.969),
Vector(-199.947,915.653,-447.969),
Vector(-87.003,945.989,-447.969),
Vector(15.124,955.601,-447.969),
Vector(85.713,871.64,-447.969),
Vector(70.568,755.322,-447.969),
Vector(111.72,680.055,-447.969),
Vector(218.77,663.043,-447.969),
Vector(297.698,730.463,-447.969),
Vector(338.571,820.012,-447.969),
Vector(400.391,899.783,-447.969),
Vector(482.639,928.213,-447.969),
Vector(557.928,874.585,-447.969),
Vector(576.365,786.558,-447.969),
Vector(604.867,685.745,-447.969),
Vector(682.791,675.649,-447.969),
Vector(746.086,738.993,-447.969),
Vector(787.076,839.822,-447.969),
Vector(865.376,924.109,-447.969),
Vector(969.035,941.056,-447.969),
Vector(1044.5,883.972,-447.969),
Vector(1052.324,770.563,-447.969),
Vector(1062.021,683.112,-447.969),
Vector(1136.762,637.392,-447.969),
Vector(1226.063,682.541,-447.969),
Vector(1258.735,774.43,-447.969),
Vector(1291.833,850.522,-447.969),
Vector(1317.613,913.648,-447.969),
Vector(1327.351,697.661,-447.969),
Vector(198.69,456.561,-447.969),
Vector(85.021,464.459,-447.969),
Vector(-44.453,419.791,-447.969),
Vector(-72.325,319.153,-447.969),
Vector(-12.934,243.762,-447.969),
Vector(101.468,197.999,-447.969),
Vector(182.418,123.94,-447.969),
Vector(214.328,20.554,-447.969),
Vector(181.716,-75.129,-447.969),
Vector(82.049,-108.919,-447.969),
Vector(-45.497,-92.327,-447.969),
Vector(-23.307,-176.54,-447.969),
Vector(52.73,-245.506,-447.969),
Vector(128.597,-300.239,-447.969),
Vector(158.334,-399.922,-447.969),
Vector(201.663,-507.261,-447.969),
Vector(204.499,-598.81,-447.969),
Vector(104.756,-635.381,-447.969),
Vector(-0.299,-617.385,-447.969),
Vector(-69.378,-542.095,-447.969),
Vector(-88.848,-441.965,-447.969),
Vector(-42.755,-351.514,-447.969),
Vector(-250.518,-601.497,-191.969),
Vector(-248.688,-517.922,-191.969),
Vector(-246.011,-414.695,-191.969),
Vector(-247.098,-312.908,-191.969),
Vector(-279.792,-224.328,-191.969),
Vector(-373.039,-177.212,-191.969),
Vector(-485.949,-199.9,-191.969),
Vector(-560.633,-288.346,-191.969),
Vector(-574.115,-392.137,-191.969),
Vector(-536.681,-479.631,-191.969),
Vector(-453.505,-558.741,-191.969),
Vector(-503.061,-650.78,-191.969),
Vector(-612.362,-629.165,-191.969),
Vector(-727.752,-551.651,-191.969),
Vector(-867.856,-552.937,-191.969),
Vector(-982.486,-550.747,-191.969),
Vector(-1068.529,-483.747,-191.969),
Vector(-1126.149,-373.617,-191.969),
Vector(-1134.97,-280.301,-191.969),
Vector(-1001.453,-280.68,-191.969),
Vector(-875.729,-296.097,-191.969),
Vector(-752.011,-293.351,-191.969),
Vector(-671.286,-217.677,-191.969),
Vector(-742.248,-112.352,-184.731),
Vector(-859.555,-117.232,-163.1),
Vector(-964.324,-139.038,-143.781),
Vector(-1035.375,-91.114,-130.679),
Vector(-1132.01,-55.437,-112.86),
Vector(-1205.08,-113.248,-99.386),
Vector(-1275.458,-191.103,-87.967),
Vector(-1366.712,-172.157,-87.969),
Vector(-1436.609,-103.01,-87.969),
Vector(-1531.853,-89.221,-87.969),
Vector(-1591.407,-180.53,-87.969),
Vector(-1578.732,-273.691,-87.969),
Vector(-1529.192,-359.997,-87.969),
Vector(-1442.187,-390.77,-87.969),
Vector(-1365.879,-392.432,-87.969),
Vector(-1394.48,-449.591,-87.969),
Vector(-1474.124,-538.36,-87.969),
Vector(-1457.632,-617.506,-87.969),
Vector(-1346.177,-633.521,-87.969),
Vector(-1262.137,-595.357,-79.65),
Vector(-1179.281,-526.072,-57.32),
Vector(-1084.396,-497.651,-31.748),
Vector(-998.913,-540.849,-8.71),
Vector(-905.859,-585.099,16.368),
Vector(-809.754,-567.796,42.269),
Vector(-746.52,-506.268,59.311),
Vector(-687.332,-439.527,64.031),
Vector(-615.366,-413.819,64.031),
Vector(-518.539,-494.259,64.031),
Vector(-434.964,-530.57,64.031),
Vector(-320.992,-547.189,64.031),
Vector(-238.164,-501.78,64.031),
Vector(-211.188,-392.994,64.031),
Vector(-246.265,-299.785,64.031),
Vector(-323.033,-241.552,64.031),
Vector(-412.623,-200.722,64.031),
Vector(-505.712,-147.295,64.031),
Vector(-574.184,-82.809,64.031),
Vector(-614.445,10.809,64.031),
Vector(-572.359,106.253,64.031),
Vector(-465.81,132.297,64.031),
Vector(-372.861,109.726,64.031),
Vector(-267.107,76.319,64.031),
Vector(-199.899,148.332,64.031),
Vector(-217.876,258.034,64.031),
Vector(-278.385,331.77,64.031),
Vector(-359.936,384.481,64.031),
Vector(-466.168,398.658,64.031),
Vector(-559.505,352.408,64.031),
Vector(-609.619,268.819,64.031),
Vector(-621.407,197.234,64.031),
Vector(-1069.16,93.27,41.418),
Vector(-1256.195,101.775,-60.134),
Vector(-1474.344,136.275,-87.969),
Vector(-1527.945,239.192,-87.969),
Vector(-1453.968,346.053,-87.969),
Vector(-1331.36,426.97,-87.969),
Vector(-1372.648,549.494,-87.969),
Vector(-1523.378,552.128,-87.969),
Vector(-1731.82,608.498,-87.969),
Vector(-1851.437,619.012,-87.969),
Vector(-1950.313,546.75,-87.969),
Vector(-2032.68,489.811,-87.969),
Vector(-2122.928,551.19,-87.969),
Vector(-2120.928,669.182,-87.969),
Vector(-2001.922,735.786,-87.969)}

SINHUNTER_SPAWNS_BACK = {
Vector(-1328.309,1853.615,-343.969),
Vector(-1417.477,1887.467,-343.969),
Vector(-1493.502,1955.956,-343.969),
Vector(-1501.631,2068.993,-343.969),
Vector(-1426.915,2127.142,-343.969),
Vector(-1308.755,2123.449,-343.969),
Vector(-1222.842,2088.245,-343.969),
Vector(-1136.272,2097.249,-343.969),
Vector(-1117.491,2183.188,-343.969),
Vector(-1181.132,2258.046,-343.969),
Vector(-1247.637,2302.094,-343.969),
Vector(-1258.776,2378.355,-343.969),
Vector(-1174.524,2387.408,-343.969),
Vector(-1079.663,2342.943,-343.969),
Vector(-1016.205,2262.84,-343.969),
Vector(-996.672,2174.576,-343.969),
Vector(-985.951,2082.697,-343.969),
Vector(-927.226,1999.254,-343.969),
Vector(-840.83,1995.854,-343.969),
Vector(-789.17,2065.917,-343.969),
Vector(-778.315,2154.083,-343.969),
Vector(-781.083,2241.731,-343.969),
Vector(-771.396,2336.573,-343.969),
Vector(-723.289,2420.689,-343.969),
Vector(-630.521,2450.34,-343.969),
Vector(-543.861,2419.653,-343.969),
Vector(-490.255,2336.354,-343.969),
Vector(-481.017,2246.237,-343.969),
Vector(-478.648,2156.017,-343.969),
Vector(-427.085,2067.555,-343.969),
Vector(-337.643,2031.651,-343.969),
Vector(-247.457,2044.857,-343.969),
Vector(-171.417,2103.17,-343.969),
Vector(-144.601,2182.562,-343.969),
Vector(-142.278,2278.76,-343.969),
Vector(-125.074,2367.541,-343.969),
Vector(-58.266,2434.148,-343.969),
Vector(37.612,2449.418,-343.969),
Vector(117.474,2424.643,-343.969),
Vector(195.826,2381.055,-343.969),
Vector(274.067,2311.751,-343.969),
Vector(318.26,2248.362,-343.969),
Vector(324.688,2144.481,-342.959),
Vector(154.833,2110.508,-343.969),
Vector(89.146,2052.978,-343.969),
Vector(119.141,1940.503,-343.969),
Vector(206.714,1924.268,-343.969),
Vector(389.445,2148.366,-343.969),
Vector(286.209,2532.592,-267.969),
Vector(258.067,2574.903,-267.969),
Vector(267.325,2671.215,-267.969),
Vector(333.339,2742.587,-267.969),
Vector(364.777,2830.299,-267.969),
Vector(311.775,2903.565,-267.969),
Vector(207,2897.349,-267.969),
Vector(115.963,2870.122,-267.969),
Vector(34.768,2840.196,-267.969),
Vector(-57.441,2772.824,-267.969),
Vector(-135.55,2727.664,-267.969),
Vector(-233.048,2696.587,-267.969),
Vector(-332.452,2691.769,-267.969),
Vector(-417.524,2710.716,-267.969),
Vector(-525.305,2737.441,-267.969),
Vector(-624.443,2737.033,-267.969),
Vector(-719.058,2706.147,-267.969),
Vector(-805.034,2658.645,-267.969),
Vector(-899.714,2642.467,-267.969),
Vector(-992.58,2660.838,-267.969),
Vector(-1096.213,2679.919,-267.969),
Vector(-1190.868,2674.486,-267.969),
Vector(-1292.029,2666.136,-267.969),
Vector(-1389.201,2650.812,-267.969),
Vector(-1488.161,2620.478,-267.969),
Vector(-1571.64,2585.646,-267.969),
Vector(-1657.835,2537.797,-267.969),
Vector(-1739.972,2480.927,-267.969),
Vector(-1824.855,2415.776,-267.969),
Vector(-1892.763,2336.028,-267.969),
Vector(-1926.516,2253.601,-267.969),
Vector(-1991.069,2178.277,-267.969),
Vector(-2083.615,2190.979,-267.969),
Vector(-2123.482,2295.853,-267.969),
Vector(-2108.726,2401.245,-267.969),
Vector(-2073.725,2480.035,-267.969),
Vector(-1988.308,2542.981,-267.969),
Vector(-1897.118,2575.947,-267.969),
Vector(-1859.789,2658.079,-267.969),
Vector(-1842.213,2748.722,-267.969),
Vector(-1765.696,2815.662,-267.969),
Vector(-1664.327,2809.578,-267.969),
Vector(-1594.421,2751.885,-267.969),
Vector(-1500.539,2748.46,-267.969),
Vector(-1451.882,2824.855,-267.969),
Vector(-1429.178,2915.919,-267.969),
Vector(-1366.395,2981.725,-267.969),
Vector(-1267.98,2975.683,-267.969),
Vector(-1188.443,2927.078,-267.969),
Vector(-1101.317,2901.946,-267.969),
Vector(-1026.923,2900.981,-267.969),
Vector(-206.617,2935.708,-267.969),
Vector(-118.204,2998.99,-267.969),
Vector(-49.268,3035.565,-267.969),
Vector(61.274,3038.175,-267.969),
Vector(201.726,3010.029,-267.969),
Vector(86.167,3139.248,-267.969),
Vector(34.099,3154.998,-267.969),
Vector(-1.543,3240.865,-267.969),
Vector(-60.256,3278.024,-267.969),
Vector(-171.033,3285.264,-267.969),
Vector(-172.748,3405.21,-267.969),
Vector(-75.474,3452.98,-267.969),
Vector(33.152,3452.871,-267.969),
Vector(135.712,3526.666,-267.969),
Vector(147.401,3637.033,-267.969),
Vector(153.511,3694.845,-267.969),
Vector(263.545,3724.865,-267.969),
Vector(386.471,3739.881,-267.969),
Vector(485.912,3695.795,-267.969),
Vector(536.825,3577.344,-267.969),
Vector(519.762,3457.543,-267.969),
Vector(486.161,3358.411,-267.969),
Vector(484.784,3244.757,-267.969),
Vector(591.818,3217.71,-267.969),
Vector(685.485,3274.229,-267.969),
Vector(766.125,3339.937,-240.535),
Vector(875.5,3359.504,-239.969),
Vector(979.578,3338.328,-239.969),
Vector(1031.528,3245.781,-239.969),
Vector(985.933,3150.215,-228.034),
Vector(883.781,3113.445,-216.72),
Vector(815.53,3036.865,-193.158),
Vector(824.332,2932.46,-191.969),
Vector(902.111,2857.323,-191.969),
Vector(972.037,2796.213,-191.969),
Vector(999.523,2696.604,-191.969),
Vector(967.187,2598.14,-191.969),
Vector(906.134,2530.583,-191.969),
Vector(843.206,2453.754,-191.969),
Vector(802.013,2352,-191.969),
Vector(790.152,2255.977,-191.969),
Vector(840.346,2157.109,-191.969),
Vector(943.937,2148.777,-191.969),
Vector(1020.24,2211.706,-191.969),
Vector(662.183,2043.908,-181.911),
Vector(531.829,2021.9,-124.467),
Vector(382.95,2024.677,-87.969),
Vector(277.043,1879.53,-87.969),
Vector(242.963,1797.372,-87.969),
Vector(158.655,1728.963,-87.969),
Vector(57.138,1717.126,-87.969),
Vector(-40.014,1762.395,-87.969),
Vector(-138.028,1843.956,-87.843),
Vector(-245.22,1841.397,-56.556),
Vector(-328.329,1772.794,-31.979),
Vector(-422.552,1706.622,-4.116),
Vector(-533.22,1714.837,28.611),
Vector(-606.847,1787.949,50.384),
Vector(-703.02,1848.497,64.031),
Vector(-806.868,1801.257,64.031),
Vector(-879.79,1691.695,64.031),
Vector(-964.591,1649.329,64.031),
Vector(-1064.089,1723.615,64.031),
Vector(-206.617,2935.708,-267.969),
Vector(-118.204,2998.99,-267.969),
Vector(-49.268,3035.565,-267.969),
Vector(61.274,3038.175,-267.969),
Vector(201.726,3010.029,-267.969),
Vector(86.167,3139.248,-267.969),
Vector(34.099,3154.998,-267.969),
Vector(-1.543,3240.865,-267.969),
Vector(-60.256,3278.024,-267.969),
Vector(-171.033,3285.264,-267.969),
Vector(-172.748,3405.21,-267.969),
Vector(-75.474,3452.98,-267.969),
Vector(33.152,3452.871,-267.969),
Vector(135.712,3526.666,-267.969),
Vector(147.401,3637.033,-267.969),
Vector(153.511,3694.845,-267.969),
Vector(263.545,3724.865,-267.969),
Vector(386.471,3739.881,-267.969),
Vector(485.912,3695.795,-267.969),
Vector(536.825,3577.344,-267.969),
Vector(519.762,3457.543,-267.969),
Vector(486.161,3358.411,-267.969),
Vector(484.784,3244.757,-267.969),
Vector(591.818,3217.71,-267.969),
Vector(685.485,3274.229,-267.969),
Vector(766.125,3339.937,-240.535),
Vector(875.5,3359.504,-239.969),
Vector(979.578,3338.328,-239.969),
Vector(1031.528,3245.781,-239.969),
Vector(985.933,3150.215,-228.034),
Vector(883.781,3113.445,-216.72),
Vector(815.53,3036.865,-193.158),
Vector(824.332,2932.46,-191.969),
Vector(902.111,2857.323,-191.969),
Vector(972.037,2796.213,-191.969),
Vector(999.523,2696.604,-191.969),
Vector(967.187,2598.14,-191.969),
Vector(906.134,2530.583,-191.969),
Vector(843.206,2453.754,-191.969),
Vector(802.013,2352,-191.969),
Vector(790.152,2255.977,-191.969),
Vector(840.346,2157.109,-191.969),
Vector(943.937,2148.777,-191.969),
Vector(1020.24,2211.706,-191.969),
Vector(662.183,2043.908,-181.911),
Vector(531.829,2021.9,-124.467),
Vector(382.95,2024.677,-87.969),
Vector(277.043,1879.53,-87.969),
Vector(242.963,1797.372,-87.969),
Vector(158.655,1728.963,-87.969),
Vector(57.138,1717.126,-87.969),
Vector(-40.014,1762.395,-87.969),
Vector(-138.028,1843.956,-87.843),
Vector(-245.22,1841.397,-56.556),
Vector(-328.329,1772.794,-31.979),
Vector(-422.552,1706.622,-4.116),
Vector(-533.22,1714.837,28.611),
Vector(-606.847,1787.949,50.384),
Vector(-703.02,1848.497,64.031),
Vector(-806.868,1801.257,64.031),
Vector(-879.79,1691.695,64.031),
Vector(-964.591,1649.329,64.031),
Vector(-1064.089,1723.615,64.031)}