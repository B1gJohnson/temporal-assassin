
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("trigger_hurt")
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then

		Create_SMGAmmo( Vector(10790.008789063, 1829.0345458984, -222.4208984375), Angle(0, 1.9703521728516, 0) )
		Create_SMGAmmo( Vector(10790.20703125, 1845.4456787109, -222.4208984375), Angle(0, 343.05023193359, 0) )
		Create_PistolAmmo( Vector(10789.658203125, 1919.1861572266, -222.4208984375), Angle(0, 293.55014038086, 0) )
		Create_PistolAmmo( Vector(10791.348632813, 1902.1981201172, -222.4208984375), Angle(0, 298.39016723633, 0) )
		Create_Medkit25( Vector(10785.615234375, 1872.8918457031, -222.4208984375), Angle(0, 359.77020263672, 0) )
		
		Create_PortableMedkit( Vector(7762.759765625, 1422.9506835938, -255.96875), Angle(0, 359.92822265625, 0) )
		Create_PortableMedkit( Vector(7758.5698242188, 1464.9169921875, -255.96875), Angle(0, 340.12811279297, 0) )
		Create_PortableMedkit( Vector(7766.1396484375, 1380.8909912109, -255.96875), Angle(0, 21.048248291016, 0) )
		Create_RevolverAmmo( Vector(7795.9653320313, 1404.0640869141, -255.96875), Angle(0, 6.9679718017578, 0) )
		Create_RevolverAmmo( Vector(7791.7065429688, 1442.6104736328, -255.96875), Angle(0, 347.16784667969, 0) )
		Create_RevolverWeapon( Vector(7807.8505859375, 1528.7193603516, -255.96875), Angle(0, 263.56768798828, 0) )
		Create_SpiritSphere( Vector(7833.212890625, 1422.1159667969, -255.96875), Angle(0, 21.127670288086, 0) )
		Create_EldritchArmour( Vector(9447.751953125, 1512.3558349609, -491.56365966797), Angle(0, 228.91958618164, 0) )
		
		CreateSecretArea( Vector(7798.1479492188,1466.1408691406,-255.96875), Vector(-102.10679626465,-102.10679626465,0), Vector(102.10679626465,102.10679626465,125.17463684082) )
		CreateSecretArea( Vector(9440.2236328125,1502.8679199219,-487.03125), Vector(-32.603412628174,-32.603412628174,0), Vector(32.603412628174,32.603412628174,43.226043701172) )
		
	else
	
		CreateProp(Vector(7690.531,1476.063,-236.219), Angle(-0.044,-0.396,-0.044), "models/props_interiors/furniture_chair03a.mdl", true)
		CreateProp(Vector(7690.125,1552.594,-236.219), Angle(0,0.352,0), "models/props_interiors/furniture_chair03a.mdl", true)
	
		Create_GrenadeAmmo( Vector(10276.924, 1694.117, -255.969), Angle(0, 202.707, 0) )
		Create_GrenadeAmmo( Vector(10229.738, 1679.603, -255.969), Angle(0, 268.751, 0) )
		Create_SMGWeapon( Vector(10177.151, 1678.006, -255.969), Angle(0, 282.963, 0) )
		Create_Backpack( Vector(10264.372, 1790.035, -255.969), Angle(0, 101.342, 0) )
		Create_Medkit25( Vector(10169.691, 2506.332, -255.969), Angle(0, 275.393, 0) )
		Create_Medkit25( Vector(10093.612, 1803.578, -255.969), Angle(0, 178.524, 0) )
		Create_Armour5( Vector(9750.383, 1586.346, -255.969), Angle(0, 19.266, 0) )
		Create_Armour5( Vector(9744.518, 1615.07, -255.969), Angle(0, 357.112, 0) )
		Create_Armour5( Vector(9762.152, 1605.049, -255.969), Angle(0, 345.198, 0) )
		Create_Medkit25( Vector(8760.477, 2002.911, -255.969), Angle(0, 319.693, 0) )
		Create_FoolsOathAmmo( Vector(8671.136, 1967.935, -162.743), Angle(-41.274, -85.339, -39.91) )
		Create_Armour5( Vector(7855.305, 2006.886, -255.969), Angle(0, 331.96, 0) )
		Create_Beer( Vector(7851.864, 1981.392, -255.969), Angle(0, 349.307, 0) )
		Create_Beer( Vector(7882.503, 2011.412, -255.969), Angle(0, 317.33, 0) )
		Create_GrenadeAmmo( Vector(7091.038, 1908.823, -383.969), Angle(0, 307.925, 0) )
		Create_Medkit10( Vector(7057.916, 2045.965, -383.969), Angle(0, 288.488, 0) )
		Create_Medkit10( Vector(7055.135, 2006.413, -383.969), Angle(0, 294.758, 0) )
		Create_SMGAmmo( Vector(7650.115, 1887.065, -274.639), Angle(43.252, -165.145, 10.19) )
		Create_Bread( Vector(7660.674, 1673.237, -256.969), Angle(0, 170.187, 0) )
		Create_ShotgunAmmo( Vector(7070.598, 1595.867, -383.969), Angle(0, 78.126, 0) )
		Create_PistolWeapon( Vector(7571.186, 1438.724, -255.969), Angle(0, 171.131, 0) )
		Create_PistolAmmo( Vector(7560.905, 1470.676, -255.969), Angle(0, 197.465, 0) )
		Create_Medkit25( Vector(8518.903, 1960.972, -255.969), Angle(0, 181.881, 0) )
		Create_Medkit25( Vector(10772.934, 1757.787, -255.969), Angle(0, 4.858, 0) )
		Create_Medkit10( Vector(10767.918, 1794.177, -255.969), Angle(0, 343.957, 0) )
		Create_ShotgunAmmo( Vector(10795.028, 1780.324, -255.969), Angle(0, 348.764, 0) )
		Create_PistolAmmo( Vector(10799.714, 1739.803, -255.969), Angle(0, 23.041, 0) )
		Create_RevolverAmmo( Vector(10803.541, 1760.625, -255.969), Angle(0, 5.903, 0) )
		Create_RifleWeapon( Vector(6566.637, 209.707, -92.099), Angle(0.104, 88.132, 0) )
		Create_RifleAmmo( Vector(6591.444, 200.17, -92.009), Angle(0.627, 176.847, 0) )
		Create_RifleAmmo( Vector(6520.923, 213.613, -90.804), Angle(0, 80.821, 0) )
		Create_WhisperAmmo( Vector(-10751.485, -7277.788, -1002.313), Angle(0, 95.456, 0) )
		
		local SEC_BUT_1 = function()
			Create_SpiritEye2( Vector(7882.7, 1350.942, -255.969), Angle(0, 83.226, 0) )
			Create_SpiritSphere( Vector(7748.657, 1401.756, -255.969), Angle(0, 43.307, 0) )
			Create_Armour( Vector(7827.614, 1375.026, -255.969), Angle(0, 85.734, 0) )
			Create_Armour( Vector(7787.419, 1378.012, -255.969), Angle(0, 65.461, 0) )
			Create_EldritchArmour10( Vector(7850.619, 1411.473, -255.969), Angle(0, 94.93, 0) )
			Create_EldritchArmour10( Vector(7812.134, 1419.943, -255.969), Angle(0, 73.612, 0) )
			Create_EldritchArmour10( Vector(7772.713, 1421.176, -255.969), Angle(0, 55.011, 0) )
		end
		
		AddSecretButton( Vector(7680.031,1515.352,-200.538), SEC_BUT_1 )
		CreateSecretArea( Vector(10266.623,1799.439,-251.406), Vector(-24.782,-24.782,0), Vector(24.782,24.782,17.786) )
		CreateSecretArea( Vector(6558.581,212.149,-87.562), Vector(-49.315,-49.315,0), Vector(49.315,49.315,46.924) )
		CreateSecretArea( Vector(-10751.477,-7277.479,-997.781), Vector(-24.828,-24.828,0), Vector(24.828,24.828,34.913) )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)