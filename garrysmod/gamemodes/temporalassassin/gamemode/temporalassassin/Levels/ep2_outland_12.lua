		
if CLIENT then return end

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("trigger_playermovement")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	
	Create_Armour100( Vector(777.20678710938, -2720.0795898438, -39.968746185303), Angle(0, 300.44006347656, 0) )
	Create_Backpack( Vector(842.50604248047, -2715.6892089844, -39.968753814697), Angle(0, 286.58001708984, 0) )
	Create_Backpack( Vector(890.46862792969, -2716.2590332031, -39.968746185303), Angle(0, 257.97985839844, 0) )
	Create_Backpack( Vector(947.91937255859, -2720.3334960938, -39.96875), Angle(0, 229.15969848633, 0) )
	Create_PortableMedkit( Vector(755.16040039063, -2860.2958984375, -39.968753814697), Angle(0, 16.559844970703, 0) )
	Create_Medkit25( Vector(865.18908691406, -2902.0400390625, -14.765172958374), Angle(0, 1.5996551513672, 0) )
	Create_Medkit25( Vector(866.82861328125, -2944.0466308594, -14.765167236328), Angle(0, 356.9794921875, 0) )
	Create_Medkit25( Vector(867.47253417969, -2986.3977050781, -14.765171051025), Angle(0, 4.8992156982422, 0) )
	Create_PortableMedkit( Vector(785.59094238281, -2858.4699707031, -39.96875), Angle(0, 7.9793548583984, 0) )
	Create_PortableMedkit( Vector(812.80303955078, -2859.4985351563, -39.968753814697), Angle(0, 12.159378051758, 0) )
	Create_RevolverWeapon( Vector(774.37261962891, -2793.4287109375, -39.968753814697), Angle(0, 344.65927124023, 0) )
	Create_ShotgunWeapon( Vector(822.73138427734, -2798.1518554688, -39.96875), Angle(0, 359.17913818359, 0) )
	Create_FalanrathThorn( Vector(993.14556884766, -2786.1552734375, -39.968753814697), Angle(0, 208.03927612305, 0) )
	Create_SuperShotgunWeapon( Vector(851.34527587891, -2800.0673828125, -39.96875), Angle(0, 353.89926147461, 0) )
	Create_SMGWeapon( Vector(885.47387695313, -2804.4360351563, -39.96875), Angle(0, 354.77926635742, 0) )
	Create_KingSlayerWeapon( Vector(931.05010986328, -2808.1977539063, -39.96875), Angle(0, 353.67926025391, 0) )
	Create_Backpack( Vector(3046.2619628906, 57.791122436523, 122.98492431641), Angle(0, 356.0989074707, 0) )
	Create_Backpack( Vector(3042.6784667969, 97.891296386719, 122.98492431641), Angle(0, 346.19888305664, 0) )
	Create_Backpack( Vector(3044.5170898438, 138.56832885742, 122.98492431641), Angle(0, 351.91870117188, 0) )
	Create_RifleWeapon( Vector(3199.7333984375, 152.40435791016, 96.03125), Angle(0, 230.0387878418, 0) )
	Create_M6GWeapon( Vector(3225.3229980469, 87.415313720703, 96.03125), Angle(0, 192.19877624512, 0) )
	Create_WhisperWeapon( Vector(3179.6254882813, -341.83923339844, 88.03125), Angle(0, 102.8787612915, 0) )
	Create_WhisperAmmo( Vector(3101.8874511719, -327.98147583008, 88.03125), Angle(0, 78.458702087402, 0) )
	Create_WhisperAmmo( Vector(3047.4699707031, -309.65100097656, 88.03125), Angle(0, 57.118598937988, 0) )
	Create_WhisperAmmo( Vector(3082.4096679688, -296.22091674805, 88.03125), Angle(0, 67.018630981445, 0) )
	Create_Medkit25( Vector(3372.5568847656, -106.92375946045, 96.03125), Angle(0, 180.61853027344, 0) )
	Create_Medkit25( Vector(3371.2392578125, -154.04777526855, 96.03125), Angle(0, 176.87854003906, 0) )
	Create_SpiritSphere( Vector(3301.9123535156, -131.0622253418, 96.03125), Angle(0, 173.35852050781, 0) )
	Create_SpiritEye2( Vector(-389.57189941406, 5268.4921875, 192.03125), Angle(0, 278.41403198242, 0) )
	
	CreateProp(Vector(4163.125,2042.25,606.3125), Angle(22.1044921875,-85.95703125,2.6806640625), "models/temporalassassin/doll/unknown.mdl", true)
	
	local GAME_END_TXT = {
	"After murdering tons of 'Real Bad Dudes' practically all by",
	"herself, kit decided to simply head back to the agency for",
	"a huge ass nap, a girl gets kinda tired after saving pretty",
	"much an entire dimension from world domination.",
	"",
	"",
	"Wonder what kit would have saw if only she stuck around for",
	"just another 20 minutes, the death of a franchise? who knows.",
	"",
	"",
	"Where kit will end up next time in Temporal Assassin? That's up to you."}	
	
	MakeGameEnd( GAME_END_TXT, 30, Vector(158.45445251465,-8831.927734375,-319.96875), Vector(-76.644599914551,-76.644599914551,0), Vector(76.644599914551,76.644599914551,142.01495361328) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)