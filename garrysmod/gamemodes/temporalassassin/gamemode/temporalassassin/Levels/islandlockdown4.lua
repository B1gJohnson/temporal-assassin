		
if CLIENT then return end

LEVEL_STRINGS["islandlockdown4"] = { "islandunderground", Vector(2874.63,270.291,256.725), Vector(-92.253,-92.253,0), Vector(92.253,92.253,87.966) }

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(4116.344,3397.545,512.031),Angle(0,-90,0))
	
	for _,v in pairs (ents.FindInSphere(Vector(4125.031,3272.062,512.031),100)) do
	
		if IsValid(v) and v:IsDoor() then
			v:Fire("Open")
		end
	
	end
	
	Create_VolatileBattery( Vector(5075.53, 3606.026, 512.031), Angle(0, 194.023, 0) )
	Create_Armour200( Vector(5051.548, 3536.02, 512.031), Angle(0, 169.823, 0) )
	Create_CrossbowWeapon( Vector(4995.685, 3593.907, 512.031), Angle(0, 181.043, 0) )
	Create_CrossbowAmmo( Vector(4995.265, 3528.333, 512.031), Angle(0, 154.423, 0) )
	Create_KingSlayerAmmo( Vector(4940.619, 3567.296, 512.031), Angle(0, 180.383, 0) )
	Create_Medkit25( Vector(3862.529, 3207.483, 512.031), Angle(0, 0.123, 0) )
	Create_Medkit25( Vector(3867.599, 3242.193, 512.031), Angle(0, 357.483, 0) )
	Create_ShotgunAmmo( Vector(3900.374, 3115.923, 528.031), Angle(0, 7.603, 0) )
	Create_SMGAmmo( Vector(3900.457, 3169.444, 528.031), Angle(0, 353.743, 0) )
	Create_RevolverWeapon( Vector(3885.112, 2973.485, 512.031), Angle(0, 34.883, 0) )
	Create_PistolWeapon( Vector(3917.819, 2930.867, 512.031), Angle(0, 57.983, 0) )
	Create_RifleAmmo( Vector(3956.842, 2929.7, 512.031), Angle(0, 74.043, 0) )
	Create_SMGWeapon( Vector(3925.577, 2979.022, 512.031), Angle(0, 46.323, 0) )
	Create_MaskOfShadows( Vector(4873.835, 3203.559, 512.031), Angle(0, 0.122, 0) )
	Create_Medkit10( Vector(4496.378, 3090.103, 512.031), Angle(0, 75.582, 0) )
	Create_Medkit10( Vector(4557.98, 3090.914, 512.031), Angle(0, 103.523, 0) )
	Create_SuperShotgunWeapon( Vector(5349.563, 2402.042, 641.031), Angle(0, 177.142, 0) )
	Create_ShotgunWeapon( Vector(5861.435, 2413.601, 641.031), Angle(0, 128.082, 0) )
	Create_SMGAmmo( Vector(5891.293, 2572.584, 641.031), Angle(0, 205.742, 0) )
	Create_M6GAmmo( Vector(5859.592, 2582.402, 641.031), Angle(0, 238.082, 0) )
	Create_CrossbowWeapon( Vector(5833.075, 2206.786, 641.031), Angle(0, 235.806, 0) )
	Create_Medkit25( Vector(5894.455, 1981.936, 641.031), Angle(0, 186.826, 0) )
	Create_Medkit25( Vector(5891.978, 1930.095, 641.031), Angle(0, 152.286, 0) )
	Create_PortableMedkit( Vector(5896.985, 1775.176, 641.031), Angle(0, 202.886, 0) )
	Create_PortableMedkit( Vector(5880.963, 1753.108, 641.031), Angle(0, 193.426, 0) )
	Create_RifleWeapon( Vector(5847.956, 1542.714, 641.031), Angle(0, 93.546, 0) )
	Create_RifleAmmo( Vector(5915.77, 1533.822, 641.031), Angle(0, 128.306, 0) )
	Create_PistolWeapon( Vector(5068.629, 1931.933, 641.031), Angle(0, 167.247, 0) )
	Create_PistolAmmo( Vector(5067.614, 1965.996, 641.031), Angle(0, 187.927, 0) )
	Create_KingSlayerAmmo( Vector(5039.395, 1947.422, 641.031), Angle(0, 174.947, 0) )
	Create_Medkit25( Vector(4582.133, 2251.158, 641.031), Angle(0, 358.427, 0) )
	Create_Armour100( Vector(4581.014, 2179.202, 641.198), Angle(0, 2.387, 0) )
	Create_KingSlayerWeapon( Vector(4779.333, 1864.424, 641.031), Angle(0, 99.627, 0) )
	Create_ManaCrystal100( Vector(4617.531, 2849.29, 664.031), Angle(0, 284.977, 0) )
	Create_EldritchArmour10( Vector(5060.367, 2333.231, 641.031), Angle(0, 264.635, 0) )
	Create_EldritchArmour10( Vector(5069.533, 2308.413, 641.031), Angle(0, 254.075, 0) )
	Create_Bread( Vector(4375.055, 1525.476, 512.199), Angle(0, 77.008, 0) )
	Create_Bread( Vector(4378.85, 1567.133, 512.199), Angle(0, 72.828, 0) )
	Create_Beer( Vector(4367.483, 1546.149, 512.199), Angle(0, 70.188, 0) )
	Create_Beer( Vector(4387.831, 1541.124, 512.199), Angle(0, 82.068, 0) )
	Create_Armour( Vector(4445.027, 1538.538, 512.199), Angle(0, 89.248, 0) )
	Create_HarbingerAmmo( Vector(4314.693, 1830.506, 383.99), Angle(0, 168.643, 0) )
	Create_WhisperAmmo( Vector(4320.51, 1622.243, 383.99), Angle(0, 129.923, 0) )
	Create_RevolverAmmo( Vector(4300.569, 1614.271, 383.99), Angle(0, 116.063, 0) )
	Create_SMGAmmo( Vector(4321.622, 1648.498, 383.99), Angle(0, 143.123, 0) )
	Create_PistolAmmo( Vector(4296.042, 1638.22, 383.99), Angle(0, 121.783, 0) )
	Create_CrossbowAmmo( Vector(4272.467, 1611.696, 383.99), Angle(0, 97.363, 0) )
	Create_RifleAmmo( Vector(4265.882, 1634.371, 383.99), Angle(0, 93.843, 0) )
	Create_SpiritSphere( Vector(3432.911, 798.213, 383.99), Angle(0, 357.043, 0) )
	Create_Armour5( Vector(3176.927, 1206.926, 384.031), Angle(0, 63.175, 0) )
	Create_Armour5( Vector(3174.948, 1144.407, 384.031), Angle(0, 90.235, 0) )
	Create_Armour5( Vector(3138.165, 1079.304, 384.031), Angle(0, 75.795, 0) )
	Create_Medkit25( Vector(2664.864, 625.381, 256.725), Angle(0, 6.499, 0) )
	Create_Medkit25( Vector(2663.024, 671.283, 256.725), Angle(0, 347.139, 0) )
	Create_M6GAmmo( Vector(2688.603, 647.007, 256.725), Angle(0, 356.599, 0) )
	Create_RevolverAmmo( Vector(2705.534, 673.508, 256.725), Angle(0, 340.319, 0) )
	Create_PistolAmmo( Vector(2702.763, 616.622, 256.725), Angle(0, 356.159, 0) )
	
	local B_FUNC = function()
		Create_HarbingerWeapon( Vector(4672.601, 3137.443, 512.031), Angle(0, 90.102, 0) )
	end
	
	AddSecretButton( Vector(4638.604,3407.69,568.262), B_FUNC )
	
	CreateSecretArea( Vector(4943.978,3563.05,516.563), Vector(-81.588,-81.588,0), Vector(81.588,81.588,172.088) )
	CreateSecretArea( Vector(3443.302,799.177,383.99), Vector(-33.203,-33.203,0), Vector(33.203,33.203,115.978) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)