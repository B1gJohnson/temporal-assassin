		
if CLIENT then return end

LEVEL_STRINGS["mimp2"] = {"mimp3", Vector(4288.827,-3076.412,-1156.969), Vector(-61.013,-61.013,0), Vector(61.013,61.013,106.938) }

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_teleport")
	RemoveAllEnts("env_fade")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(1323,-401.875,-1275.875),Angle(0,180,0))
	
	CreateProp(Vector(-6617.562,-7397.062,-1000.844), Angle(36.387,-134.648,-88.33), "models/props_docks/channelmarker_gib04.mdl", true)
	CreateProp(Vector(-6698,-7322.031,-1000.937), Angle(34.98,-134.736,-88.418), "models/props_docks/channelmarker_gib04.mdl", true)
	CreateProp(Vector(-6771.719,-7244.687,-1001.187), Angle(31.729,-135.527,-88.594), "models/props_docks/channelmarker_gib04.mdl", true)
	CreateProp(Vector(-6843.312,-7173.406,-1001.219), Angle(31.641,-132.231,-88.594), "models/props_docks/channelmarker_gib04.mdl", true)
	CreateProp(Vector(-6910.187,-7110.187,-1001.062), Angle(31.816,-130.166,-88.638), "models/props_docks/channelmarker_gib04.mdl", true)
	CreateProp(Vector(-6980.75,-7051,-1001.125), Angle(31.465,-131.484,-88.594), "models/props_docks/channelmarker_gib04.mdl", true)
	local DOLL_AP1 = CreateProp(Vector(-9181.812,-10833.562,-561.469), Angle(7.295,112.324,-0.088), "models/temporalassassin/doll/unknown.mdl", true)
	
	--[[ DOLL DISSAPEAR ]]
	local DOLL_DISSAPEAR1 = function()
	
		CreateParticleEffect( "Eldritch_Disappear", DOLL_AP1:GetBoxCenter(), Angle(0,0,0), 1 )
		sound.Play( "TemporalAssassin/Doll/Doll_Teleported.wav", DOLL_AP1:GetBoxCenter(), 140, 100, 1, CHAN_STATIC )
		
		timer.Simple(0,function()
			if IsValid(DOLL_AP1) then
				DOLL_AP1:SetPos( Vector(-8231.344,-12638.339,-1054.872) )
				DOLL_AP1:Remove()
			end
		end)
		
	end
	
	CreateTrigger( Vector(-9203.18,-10795.722,-762.047), Vector(-262.211,-262.211,0), Vector(262.211,262.211,445.83), DOLL_DISSAPEAR1 )
	
	--[[ PROGRESS BLOCK ]]
	local MIMP2_PROG_BLOCK1 = function(self,ply)
	
		if ply and IsValid(ply) and ply:IsPlayer() then
		
			local P1, P2 = CreateTeleporter( "STALL_PROGRESS_1", ply:GetPos(), "STALL_PROGRESS_2", Vector(-7285.419,-6704.855,-1007.969) )
			
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
			
		end
	
		net.Start("DollMessage")
		net.WriteString("Not that way kiddo")
		net.Broadcast()
	
	end
	
	local BLOCK1_TRIG = CreateTrigger( Vector(-8397.933,-5493.263,-2559.969), Vector(-979.006,-979.006,0), Vector(979.006,979.006,2926.861), MIMP2_PROG_BLOCK1, true )
	--[[ PROGRESS BLOCK END ]]
	
	--[[ PROGRESS BLOCK 2 ]]
	local MIMP2_PROG_BLOCK2 = function(self,ply)
	
		if ply and IsValid(ply) and ply:IsPlayer() then
		
			local P1, P2 = CreateTeleporter( "STALL_PROGRESS2_1", ply:GetPos(), "STALL_PROGRESS2_2", Vector(-10306.541,-7402.633,-1347.39) )
			
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
			
		end
	
		net.Start("DollMessage")
		net.WriteString("Wrong way Kit")
		net.Broadcast()
		
	end
	
	local BLOCK2_TRIG = CreateTrigger( Vector(-10808.367,-4791.204,-2430.642), Vector(-1686.662,-1686.662,0), Vector(1686.662,1686.662,822.18), MIMP2_PROG_BLOCK2, true )
	--[[ PROGRESS BLOCK 2 END ]]
	
	local DESTROY_BLOCKS = function()
	
		if BLOCK1_TRIG and IsValid(BLOCK1_TRIG) then
			BLOCK1_TRIG:Remove()
		end
		
		if BLOCK2_TRIG and IsValid(BLOCK2_TRIG) then
			BLOCK2_TRIG:Remove()
		end
		
		CreateProp(Vector(-12048.812,3375.031,-1231.625), Angle(0.044,-39.199,0.044), "models/temporalassassin/doll/unknown.mdl", true)
		
		CreateProp(Vector(-11178.937,3006.344,-1184.656), Angle(0.044,-178.55,-0.044), "models/props_junk/trafficcone001a.mdl", false)
		CreateProp(Vector(-11305.375,2805.031,-1184.687), Angle(0.044,89.692,-0.044), "models/props_junk/trafficcone001a.mdl", false)
		CreateProp(Vector(-11363,2802.156,-1184.687), Angle(0.044,82.881,-0.044), "models/props_junk/trafficcone001a.mdl", false)
		
		--[[ Make the rest of the items too ]]
		Create_Medkit10( Vector(-10392.714, -3865.579, -1223.969), Angle(0, 118.213, 0) )
		Create_Medkit10( Vector(-10357.21, -3831.064, -1223.969), Angle(0, 164.633, 0) )
		Create_SMGWeapon( Vector(-10025.758, -2937.843, -1286.308), Angle(0, 212.832, 0) )
		Create_RevolverAmmo( Vector(-10146.943, -2361.874, -1279.969), Angle(0, 17.612, 0) )
		Create_Medkit25( Vector(-10238.812, -2180.583, -1279.969), Angle(0, 358.252, 0) )
		Create_Medkit25( Vector(-10240.844, -2267.103, -1279.969), Angle(0, 6.392, 0) )
		Create_PortableMedkit( Vector(-11106.774, -673.746, -1351.969), Angle(0, 13.513, 0) )
		Create_Medkit10( Vector(-11111.856, -605.882, -1351.969), Angle(0, 325.992, 0) )
		Create_CrossbowAmmo( Vector(-11106.749, -638.872, -1351.969), Angle(0, 2.073, 0) )
		Create_SMGAmmo( Vector(-11073.802, -677.951, -1351.969), Angle(0, 19.673, 0) )
		Create_SuperShotgunWeapon( Vector(-11072.243, -632.2, -1351.969), Angle(0, 343.373, 0) )
		Create_GrenadeAmmo( Vector(-11351.135, 94.2, -1342.505), Angle(0, 355.253, 0) )
		Create_GrenadeAmmo( Vector(-11834.574, 2943.302, -1167.969), Angle(0, 81.275, 0) )
		Create_GrenadeAmmo( Vector(-11788.285, 2940.774, -1167.969), Angle(0, 134.955, 0) )
		Create_Armour200( Vector(-11863.087, 2929.958, -1199.969), Angle(0, 88.315, 0) )
		Create_CrossbowAmmo( Vector(-11824.811, 2937.142, -1197.969), Angle(0, 48.055, 0) )
		Create_KingSlayerAmmo( Vector(-11792.432, 2939.871, -1197.969), Angle(0, 81.055, 0) )
		Create_SuperShotgunWeapon( Vector(-11810.128, 2940.382, -1167.969), Angle(0, 57.075, 0) )
		Create_ShotgunAmmo( Vector(-11762.357, 2943.935, -1197.969), Angle(0, 96.895, 0) )
		Create_SMGAmmo( Vector(-11753.541, 3298.39, -1199.969), Angle(0, 278.835, 0) )
		Create_ManaCrystal100( Vector(-11796.748, 3335.034, -1231.969), Angle(0, 260.055, 0) )
		Create_SMGAmmo( Vector(-11729.068, 3297.875, -1199.969), Angle(0, 255.655, 0) )
		Create_M6GAmmo( Vector(-11747.299, 3332.275, -1199.969), Angle(0, 275.015, 0) )
		Create_HarbingerAmmo( Vector(-11706.501, 3324.251, -1199.969), Angle(0, 244.435, 0) )
		Create_Backpack( Vector(-11644.669, 3272.439, -1199.969), Angle(0, 204.615, 0) )
		Create_Medkit10( Vector(-10729.609, 3045.315, -1199.969), Angle(0, 120.495, 0) )
		Create_Armour( Vector(12956.395, -7958.882, -1647.969), Angle(0, 140.736, 0) )
		Create_Bread( Vector(12970.778, -7925.276, -1647.969), Angle(0, 169.336, 0) )
		Create_Bread( Vector(12948.158, -7917.323, -1647.969), Angle(0, 173.516, 0) )
		Create_ShotgunAmmo( Vector(12171.841, -6191.698, -1799.969), Angle(0, 309.172, 0) )
		Create_Medkit25( Vector(12220.829, -6194.361, -1799.969), Angle(0, 259.452, 0) )
		Create_ShotgunWeapon( Vector(12210.696, -6223.566, -1799.969), Angle(0, 274.632, 0) )
		Create_GrenadeAmmo( Vector(13277.119, -6759.644, -1647.969), Angle(0, 204.291, 0) )
		Create_ManaCrystal( Vector(13101.079, -6895.414, -1647.969), Angle(0, 359.734, 0) )
		Create_ManaCrystal( Vector(13161.779, -6894.943, -1647.969), Angle(0, 359.074, 0) )
		Create_CrossbowAmmo( Vector(13286.538, -6926.553, -1647.969), Angle(0, 129.314, 0) )
		Create_VolatileBattery( Vector(12476.9, -9331.872, -1647.969), Angle(0, 46.567, 0) )
		
		local BLOCKPROP1 = CreateProp(Vector(12858.75,-7707.469,-1647.562), Angle(-0.571,-69.873,0.044), "models/props_lab/blastdoor001b.mdl", true)
		BLOCKPROP1:SetRenderMode(RENDERMODE_TRANSALPHA)
		BLOCKPROP1:SetColor(Color(255,255,255,0))
		
		CreateTeleporter( "TP_1", Vector(-10149.651,3864.847,-1398.323), "TP_2", Vector(12156.479,-9437.086,-1534.897) )
		
	end
	
	CreateTrigger( Vector(-10924.905,-3899.388,-1279.784), Vector(-99.548,-99.548,0), Vector(99.548,99.548,138.167), DESTROY_BLOCKS )
	
	local FINAL_ITEMS = function()
		
		Create_Medkit25( Vector(12872.225, -5560.831, -1631.969), Angle(0, 7.33, 0) )
		Create_Medkit25( Vector(12870.688, -5521.481, -1631.969), Angle(0, 345.33, 0) )
		Create_Armour100( Vector(11847.114, -5562.189, -1471.969), Angle(0, 41.65, 0) )
		Create_Beer( Vector(11917.623, -5581.318, -1471.969), Angle(0, 102.067, 0) )
		Create_Beer( Vector(11926.004, -5543.521, -1471.969), Angle(0, 120.107, 0) )
		Create_Beer( Vector(11903.702, -5560.477, -1471.969), Angle(0, 93.487, 0) )
		Create_Armour( Vector(11652.16, -5057.441, -1471.969), Angle(0, 310.847, 0) )
		Create_Backpack( Vector(11693.718, -5062.126, -1471.969), Angle(0, 256.067, 0) )
		Create_GrenadeAmmo( Vector(8190.28, -4017.577, -2219.816), Angle(0, 278.919, 0) )
		Create_GrenadeAmmo( Vector(8184.029, -4034.048, -2217.699), Angle(0, 296.3, 0) )
		Create_GrenadeAmmo( Vector(8198.419, -4037.456, -2218.323), Angle(0, 266.379, 0) )
		Create_GrenadeAmmo( Vector(8178.684, -4008.191, -2220.881), Angle(-14.735, -71.946, 0) )
		Create_Medkit25( Vector(7709.521, -3913.835, -2218.187), Angle(0, 3.543, -7.3) )
		Create_Armour( Vector(7710.809, -3813.399, -2234.465), Angle(0.11, -32.646, -12.8) )
		Create_RevolverAmmo( Vector(7246.464, -4854.347, -1879.969), Angle(0, 341.386, 0) )
		Create_RevolverWeapon( Vector(7248.722, -4898.598, -1879.969), Angle(0, 15.486, 0) )
		Create_PistolAmmo( Vector(7292.875, -4914.114, -1879.969), Angle(0, 45.627, 0) )
		Create_SMGAmmo( Vector(7281.96, -4850.378, -1879.969), Angle(0, 327.306, 0) )
		Create_KingSlayerAmmo( Vector(7273.086, -4880.382, -1879.969), Angle(0, 1.187, 0) )
		Create_CrossbowAmmo( Vector(7303.88, -4878.722, -1879.969), Angle(0, 11.527, 0) )
		Create_ManaCrystal100( Vector(6152.593, -4853.425, -1860.951), Angle(-79.997, -5.993, 0) )
		Create_KingSlayerWeapon( Vector(6004.757, -4999.881, -2095.969), Angle(0, 345.866, 0) )
		Create_KingSlayerAmmo( Vector(5986.852, -4943.077, -2095.969), Angle(0, 331.786, 0) )
		Create_EldritchArmour( Vector(6107.764, -4809.641, -2223.056), Angle(12.32, 83.875, 0) )
		Create_SpiritSphere( Vector(5318.418, -4980.579, -2082.711), Angle(0, 117.491, 0) )
		Create_Medkit25( Vector(4254.106, -4766.697, -1155.969), Angle(0, 21.557, 0) )
		Create_Medkit25( Vector(4247.876, -4706.966, -1155.969), Angle(0, 332.277, 0) )
		Create_PortableMedkit( Vector(4265.319, -4734.511, -1155.969), Angle(0, 350.977, 0) )

	end
	
	CreateTrigger( Vector(13065.641,-6666.219,-1647.969), Vector(-112.054,-112.054,0), Vector(112.054,112.054,201.313), FINAL_ITEMS )
	
	local SAFETY_ON = function(self,ply)
		ply:SetSharedBool("Safety_Zone",true)
	end
	
	local SAFETY_OFF = function(self,ply)
		ply:SetSharedBool("Safety_Zone",false)
	end
	
	CreateTrigger( Vector(-10961.413,586.869,-1351.969), Vector(-88.839,-88.839,0), Vector(88.839,88.839,183.809), SAFETY_ON, true )
	CreateTrigger( Vector(11773.133,-9473.468,-1647.969), Vector(-60.95,-60.95,0), Vector(60.95,60.95,127.938), SAFETY_OFF, true )
	
	local TONYHAWK = function()
		net.Start("DollMessage")
		net.WriteString("Never Once did i think i'd get to see Retired Tony Hawk")
		net.Broadcast()
	end
	
	CreateTrigger( Vector(-11592.447,2982.253,-1199.969), Vector(-39.355,-39.355,0), Vector(39.355,39.355,117.841), TONYHAWK )
	
	local MINIGUN_SEC_BUTTON = function()
		Create_HarbingerWeapon( Vector(-11328.239, 2922.473, -1199.969), Angle(0, 89.476, 0) )
	end
	
	AddSecretButton( Vector(-11333.748,2784.031,-1144.991), MINIGUN_SEC_BUTTON )
	
	Create_MegaSphere( Vector(-7453.729, -7064.795, -1551.969), Angle(0, 229.575, 0) )
	Create_Armour5( Vector(-7521.669, -6786.54, -1007.969), Angle(0, 351.826, 0) )
	Create_Armour5( Vector(-7521.14, -6786.616, -1004.937), Angle(0, 351.826, 0) )
	Create_Armour5( Vector(-7520.611, -6786.691, -1001.906), Angle(0, 351.826, 0) )
	Create_Armour5( Vector(-7520.082, -6786.768, -998.875), Angle(0, 351.826, 0) )
	Create_Armour5( Vector(-7218.732, -6483.867, -1007.969), Angle(0, 277.026, 0) )
	Create_Armour5( Vector(-7218.667, -6484.397, -1004.937), Angle(0, 277.026, 0) )
	Create_Armour5( Vector(-7218.601, -6484.928, -1001.906), Angle(0, 277.026, 0) )
	Create_Armour5( Vector(-7218.536, -6485.458, -998.875), Angle(0, 277.026, 0) )
	Create_GrenadeAmmo( Vector(-7190.903, -6498.612, -1007.969), Angle(0, 265.146, 0) )
	Create_SMGWeapon( Vector(-7212.095, -6522.094, -1007.969), Angle(0, 262.946, 0) )
	Create_SMGAmmo( Vector(-7164.978, -6518.833, -1007.969), Angle(0, 244.246, 0) )
	Create_SMGAmmo( Vector(-7260.629, -6521.78, -1007.969), Angle(0, 303.316, 0) )
	Create_ShotgunWeapon( Vector(-7475.508, -6797.293, -1007.969), Angle(0, 348.306, 0) )
	Create_ShotgunAmmo( Vector(-7492.374, -6762.443, -1007.969), Angle(0, 350.286, 0) )
	Create_ShotgunAmmo( Vector(-7499.638, -6819.792, -1007.969), Angle(0, 29.666, 0) )
	Create_PistolAmmo( Vector(-10107.425, -9455.294, -914.184), Angle(0, 1.488, 0) )
	Create_Medkit25( Vector(-10227.304, -9512.607, -934.61), Angle(0, -3.137, 0) )
	Create_GrenadeAmmo( Vector(-10223.705, -9321.388, -934.192), Angle(0, 323.737, 0) )
	Create_Armour5( Vector(-10357.237, -9433.345, -934.192), Angle(0, 357.397, 0) )
	Create_Armour5( Vector(-10355.503, -9433.424, -931.161), Angle(0, 357.397, 0) )
	Create_Armour5( Vector(-10355.936, -9450.432, -934.192), Angle(0, 6.637, 0) )
	Create_Armour5( Vector(-10354.331, -9450.245, -931.161), Angle(0, 6.637, 0) )
	Create_Bread( Vector(-10358.478, -9325.711, -934.192), Angle(0, 341.997, 0) )
	Create_CrossbowAmmo( Vector(-10360.562, -9556.675, -934.192), Angle(0, 352.337, 0) )
	Create_SMGAmmo( Vector(-9914.594, -9403.932, -934.192), Angle(0, 225.617, 0) )
	Create_KingSlayerAmmo( Vector(-9912.989, -9336.938, -934.192), Angle(0, 138.937, 0) )
	Create_KingSlayerWeapon( Vector(-9939.665, -9659.312, -952.292), Angle(35.079, -156.162, -22.3) )
	Create_CrossbowWeapon( Vector(-9534.678, -9293.884, -927.969), Angle(0, 269.699, 0) )
	Create_Medkit10( Vector(-9815.154, -9654.978, -767.969), Angle(0, 106.243, 0) )
	Create_Medkit10( Vector(-9900.5, -9652.944, -767.969), Angle(0, 59.823, 0) )
	Create_PortableMedkit( Vector(-9857.549, -9644.071, -767.969), Angle(0, 81.383, 0) )
	Create_Armour( Vector(-9369.608, -10004.146, -983.969), Angle(0, 185.883, 0) )
	Create_RevolverWeapon( Vector(-9863.107, -9589.6, -767.969), Angle(0, 94.944, 0) )
	Create_RevolverAmmo( Vector(-9887.712, -9578.185, -767.969), Angle(0, 72.944, 0) )
	Create_Medkit25( Vector(-10590.704, -9507.211, -951.969), Angle(0, 292.584, 0) )
	Create_Armour100( Vector(-10543.445, -9506.473, -951.969), Angle(0, 260.244, 0) )
	Create_PistolWeapon( Vector(-10606.442, -9705.2, -951.969), Angle(0, 52.704, 0) )
	Create_PistolAmmo( Vector(-10609.294, -9680.984, -951.969), Angle(0, 32.464, 0) )
	Create_GrenadeAmmo( Vector(-10628.423, -9657.617, -909.969), Angle(0, 318.103, 0) )
	Create_EldritchArmour10( Vector(-10464.662, -9804.227, -796.969), Angle(0, 149.284, 0) )
	Create_EldritchArmour10( Vector(-10487.341, -9828.277, -796.969), Angle(0, 117.824, 0) )
	Create_M6GWeapon( Vector(-10493.905, -9801.604, -796.969), Angle(0, 138.504, 0) )
	Create_M6GAmmo( Vector(-10486.905, -9780.18, -796.969), Angle(0, 159.184, 0) )
	Create_RaikiriWeapon( Vector(-8980.264, -8892.345, -1041.056), Angle(0, 245.624, 0) )
	
	CreateSecretArea( Vector(-7454.593,-7061.473,-1551.969), Vector(-33.595,-33.595,0), Vector(33.595,33.595,119.363) )
	CreateSecretArea( Vector(-10487.226,-9803.932,-792.437), Vector(-39.972,-39.972,0), Vector(39.972,39.972,41.502) )
	CreateSecretArea( Vector(12483.287,-9325.611,-1643.437), Vector(-17.612,-17.612,0), Vector(17.612,17.612,39.993) )
	CreateSecretArea( Vector(8187.725,-4021.605,-2213.156), Vector(-7.356,-7.356,0), Vector(7.356,7.356,9.294) )
	CreateSecretArea( Vector(6099.521,-4829.835,-2219.329), Vector(-53.187,-53.187,0), Vector(53.187,53.187,67.689) )
	CreateSecretArea( Vector(5317.379,-4980.282,-2092.802), Vector(-28.393,-28.393,0), Vector(28.393,28.393,50.943) )
	CreateSecretArea( Vector(-8987.587,-8902.023,-1036.5), Vector(-29.529,-29.529,0), Vector(29.529,29.529,54.657) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

local MIMP2_BOARDED = false

function MIMP2_PlayerUse( ply, ent )

	if ent and IsValid(ent) then
	
		if not MIMP2_BOARDED and ent:GetClass() == "func_button" and ent:GetName() == "canister_board_button" then
		
			MIMP2_BOARDED = true
			
			timer.Simple(12,function()
				net.Start("DollMessage")
				net.WriteString("Alright, it is stable")
				net.Broadcast()
			end)
			
			timer.Simple(16,function()
				CreateTeleporter( "SKIP_1", Vector(-10920.083,-9202.791,-984.59), "SKIP_2", Vector(-10935.801,-3905.252,-1278.877) )
			end)
			
			net.Start("DollMessage")
			net.WriteString("Give me a sec kiddo")
			net.Broadcast()
		
		end
	
	end

end
hook.Add("PlayerUse","MIMP2_PlayerUseHook",MIMP2_PlayerUse)

function NO_TELEPORT( ent )

	timer.Simple(0,function()

		if ent and IsValid(ent) and ent:GetClass() == "trigger_teleport" then
			ent:Remove()
		end
		
	end)
	
end
hook.Add("OnEntityCreated","NO_TELEPORTHook",NO_TELEPORT)