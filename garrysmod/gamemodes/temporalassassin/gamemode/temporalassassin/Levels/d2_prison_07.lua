
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	CreatePlayerSpawn( Vector(165.25,-2633.5,-239.875), Angle(0,-180,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Medkit25( Vector(154.22344970703, -2432.5625, -239.96875), Angle(0, 270.16009521484, 0) )
		Create_Medkit25( Vector(205.61637878418, -2416.9406738281, -239.96875), Angle(0, 269.50012207031, 0) )
		Create_Medkit25( Vector(108.787940979, -2416.0952148438, -239.96875), Angle(0, 269.50012207031, 0) )
		Create_Armour100( Vector(334.62533569336, -2432.5178222656, -239.96875), Angle(0, 204.60003662109, 0) )
		Create_Armour( Vector(-227.3938293457, -3448.9645996094, -167.96875), Angle(0, 181.31988525391, 0) )
		Create_Medkit10( Vector(-527.70196533203, -3584.9809570313, -167.96875), Angle(0, 60.319793701172, 0) )
		Create_Medkit10( Vector(-559.26959228516, -3552.2907714844, -167.96875), Angle(0, 34.359649658203, 0) )
		Create_Medkit10( Vector(-599.86437988281, -3511.7961425781, -167.96875), Angle(0, 7.2995452880859, 0) )
		Create_Medkit10( Vector(-612.41259765625, -3458.3090820313, -167.96875), Angle(0, 344.41949462891, 0) )
		Create_Medkit10( Vector(-607.99139404297, -3408.2509765625, -167.96875), Angle(0, 326.15942382813, 0) )
		Create_ManaCrystal100( Vector(-429.18444824219, -3703.0563964844, -295.96875), Angle(0, 183.00022888184, 0) )
		Create_Beer( Vector(-175.59977722168, -3117.6042480469, -269.14758300781), Angle(0, 302.45999145508, 0) )
		Create_Beer( Vector(-153.48797607422, -3116.7097167969, -268.90795898438), Angle(0, 276.27984619141, 0) )
		Create_Beer( Vector(-127.40729522705, -3116.1967773438, -268.77047729492), Angle(0, 243.93971252441, 0) )
		Create_Beer( Vector(-99.88892364502, -3116.267578125, -268.78948974609), Angle(0, 221.71965026855, 0) )
		Create_Beer( Vector(-66.098709106445, -3116.1418457031, -268.75579833984), Angle(0, 207.85961914063, 0) )
		Create_Beer( Vector(-39.796234130859, -3116.9548339844, -268.9736328125), Angle(0, 201.47961425781, 0) )
		Create_Backpack( Vector(-417.77267456055, -3213.3796386719, -7.9687519073486), Angle(0, 269.92651367188, 0) )
		Create_Backpack( Vector(-417.80014038086, -3234.8486328125, -7.9687519073486), Angle(0, 269.92651367188, 0) )
		Create_Backpack( Vector(-417.82632446289, -3255.2734375, -7.9687519073486), Angle(0, 269.92651367188, 0) )
		Create_Backpack( Vector(-417.84817504883, -3272.3017578125, -7.9687519073486), Angle(0, 269.92651367188, 0) )
		Create_Backpack( Vector(-417.87301635742, -3291.7124023438, -7.9687519073486), Angle(0, 269.92651367188, 0) )
		Create_Backpack( Vector(-417.90533447266, -3316.9235839844, -7.9687519073486), Angle(0, 269.92651367188, 0) )
		Create_Backpack( Vector(-417.62930297852, -3333.2116699219, -7.96875), Angle(0, 269.70651245117, 0) )
		Create_Backpack( Vector(-417.71704101563, -3350.3376464844, -7.9687538146973), Angle(0, 269.70651245117, 0) )
		Create_SpiritSphere( Vector(-1140.482421875, -4218.7373046875, -295.96875), Angle(0, 100.3673324585, 0) )
		Create_M6GAmmo( Vector(1051.828125, -3556.7592773438, -295.96875), Angle(0, 21.651184082031, 0) )
		Create_KingSlayerAmmo( Vector(1048.6085205078, -3496.9836425781, -295.96875), Angle(0, 359.2109375, 0) )
		Create_PistolAmmo( Vector(1046.6243896484, -3438.4855957031, -295.96875), Angle(0, 347.55099487305, 0) )
		Create_RevolverAmmo( Vector(1047.3547363281, -3394.6352539063, -295.96875), Angle(0, 356.13095092773, 0) )
		Create_ShotgunAmmo( Vector(1051.9119873047, -3354.1066894531, -295.96875), Angle(0, 337.43087768555, 0) )
		Create_SMGAmmo( Vector(1080.3582763672, -3459.6335449219, -295.96875), Angle(0, 350.41064453125, 0) )
		Create_Medkit10( Vector(1234.9521484375, -3316.8369140625, -295.96875), Angle(0, 269.37060546875, 0) )
		Create_Medkit10( Vector(1233.9548339844, -3407.6166992188, -295.96875), Angle(0, 269.37060546875, 0) )
		Create_Medkit10( Vector(1232.9653320313, -3497.6674804688, -295.96875), Angle(0, 269.37060546875, 0) )
		Create_Medkit10( Vector(1231.8526611328, -3598.9533691406, -295.96875), Angle(0, 269.37060546875, 0) )
		Create_Medkit10( Vector(1230.9522705078, -3680.9321289063, -295.96875), Angle(0, 269.37060546875, 0) )
		Create_PortableMedkit( Vector(1284.490234375, -3004.6225585938, -679.96875), Angle(0, 347.53115844727, 0) )
		Create_PortableMedkit( Vector(1282.6339111328, -3023.0290527344, -679.96875), Angle(0, 0.95123291015625, 0) )
		Create_PortableMedkit( Vector(1304.6507568359, -3014.5080566406, -679.96875), Angle(0, 352.81118774414, 0) )
		Create_M6GAmmo( Vector(953.49743652344, -3081.9575195313, -679.96875), Angle(0, 270.6096496582, 0) )
		Create_M6GAmmo( Vector(953.91778564453, -3121.4565429688, -679.96875), Angle(0, 270.6096496582, 0) )
		Create_Medkit25( Vector(3098.0842285156, -3333.6174316406, -799.96875), Angle(0, 269.20999145508, 0) )
		Create_Medkit25( Vector(3153.4853515625, -3328.8859863281, -799.96875), Angle(0, 271.18997192383, 0) )
		Create_ShotgunWeapon( Vector(3234.0524902344, -3378.3498535156, -799.96875), Angle(0, 222.7900390625, 0) )
		Create_MegaSphere( Vector(3408.5581054688, -3360.0678710938, -280.87673950195), Angle(0, 116.38990020752, 0) )
		Create_KingSlayerAmmo( Vector(3953.9165039063, -3458.4836425781, -415.96875), Angle(0, 89.790260314941, 0) )
		Create_KingSlayerAmmo( Vector(3952.970703125, -3328.2268066406, -415.96875), Angle(0, 269.83026123047, 0) )
		Create_Beer( Vector(3505.7893066406, -3734.09765625, -415.96875), Angle(0, 88.91015625, 0) )
		Create_Beer( Vector(3506.0617675781, -3692.8334960938, -415.96875), Angle(0, 88.690155029297, 0) )
		Create_Beer( Vector(3505.9235839844, -3663.7937011719, -415.96875), Angle(0, 88.250152587891, 0) )
		Create_Beer( Vector(3505.30859375, -3630.3454589844, -415.96875), Angle(0, 86.930145263672, 0) )
		Create_Beer( Vector(3505.171875, -3600.4230957031, -415.96875), Angle(0, 84.510131835938, 0) )
		Create_Beer( Vector(3502.4851074219, -3565.9084472656, -415.96875), Angle(0, 17.410095214844, 0) )
		Create_Beer( Vector(3504.8559570313, -3530.8791503906, -415.96875), Angle(0, 349.02993774414, 0) )
		Create_Medkit25( Vector(4015.9887695313, -4069.1740722656, -543.96875), Angle(0, 90.669731140137, 0) )
		Create_Medkit25( Vector(4295.3325195313, -4071.6013183594, -543.96875), Angle(0, 89.789657592773, 0) )
		Create_Armour( Vector(4713.5102539063, -4197.3637695313, -510.50506591797), Angle(0, 225.74967956543, 0) )
		Create_Medkit25( Vector(4269.8872070313, -4356.5590820313, -543.96875), Angle(0, 266.88916015625, 0) )
		Create_Medkit25( Vector(4051.03515625, -4357.6870117188, -543.96875), Angle(0, 270.62930297852, 0) )
		Create_ShotgunAmmo( Vector(3708.1335449219, -4756.671875, -543.96875), Angle(0, 41.126571655273, 0) )
		Create_SMGAmmo( Vector(3704.5266113281, -4717.8735351563, -543.96875), Angle(0, 13.40641784668, 0) )
		Create_SMGAmmo( Vector(3750.095703125, -4764.1611328125, -543.96875), Angle(0, 71.706390380859, 0) )
		Create_RevolverAmmo( Vector(4763.1235351563, -4631.0590820313, -510.50506591797), Angle(0, 205.68634033203, 0) )
		Create_RevolverAmmo( Vector(4707.8520507813, -4630.9990234375, -510.50506591797), Angle(0, 236.70639038086, 0) )
		
		CreateSecretArea( Vector(-416.15005493164,-3351.4765625,-3.4375), Vector(-31.767177581787,-31.767177581787,0), Vector(31.767177581787,31.767177581787,44.999320983887) )
		CreateSecretArea( Vector(-1174.4113769531,-4172.5966796875,-295.96875), Vector(-112.34240722656,-112.34240722656,0), Vector(112.34240722656,112.34240722656,127.9375) )
		CreateSecretArea( Vector(1333.8664550781,-3000.1848144531,-679.96875), Vector(-40.290355682373,-40.290355682373,0), Vector(40.290355682373,40.290355682373,4.9160766601563) )
		CreateSecretArea( Vector(3410.88671875,-3307.6225585938,-280.87673950195), Vector(-104.62776947021,-104.62776947021,0), Vector(104.62776947021,104.62776947021,121.81901550293) )
		
	else
	
		local P1 = CreateProp(Vector(3661.25,-3631.531,-345.281), Angle(0,179.956,0), "models/gibs/hgibs.mdl", true)
		local P2 = CreateProp(Vector(3662.094,-3631.312,-344.812), Angle(0.527,93.999,10.679), "models/temporalassassin/items/corzohat.mdl", true)
	
		Create_Armour100( Vector(-504.779, -2737.076, -239.969), Angle(0, 23.409, 0) )
		Create_Medkit25( Vector(-507.947, -2596.572, -239.969), Angle(0, 323.216, 0) )
		Create_Medkit25( Vector(-502.903, -2696.004, -239.969), Angle(0, 3.971, 0) )
		Create_Medkit25( Vector(-440.224, -2588.228, -239.969), Angle(0, 252.365, 0) )
		Create_Medkit25( Vector(-504.059, -2645.624, -239.969), Angle(0, 351.431, 0) )
		Create_RevolverWeapon( Vector(-423.21, -2806.634, -239.969), Angle(0, 89.662, 0) )
		Create_SMGWeapon( Vector(-422.266, -2779.464, -239.969), Angle(0, 90.498, 0) )
		Create_ShotgunWeapon( Vector(-423.06, -2754.516, -239.969), Angle(0, 89.348, 0) )
		Create_SuperShotgunWeapon( Vector(-422.247, -2727.969, -239.969), Angle(0, 91.752, 0) )
		Create_ShotgunAmmo( Vector(-369.114, -2941.221, -239.969), Angle(0, 100.53, 0) )
		Create_ShotgunAmmo( Vector(-366.95, -2905.255, -239.969), Angle(0, 112.652, 0) )
		Create_SMGAmmo( Vector(-510.145, -2942.81, -239.969), Angle(0, 44.309, 0) )
		Create_SMGAmmo( Vector(-511.275, -2907.836, -239.969), Angle(0, 3.972, 0) )
		Create_SMGAmmo( Vector(-505.615, -2876.74, -239.969), Angle(0, 320.29, 0) )
		Create_RevolverAmmo( Vector(-484.56, -2925.355, -239.969), Angle(0, 3.553, 0) )
		Create_RevolverAmmo( Vector(-486.521, -2895.338, -239.969), Angle(0, 352.058, 0) )
		Create_RifleWeapon( Vector(-422.678, -2829.636, -239.969), Angle(0, 91.124, 0) )
		Create_RifleAmmo( Vector(-392.412, -2940.467, -239.969), Angle(0, 99.902, 0) )
		Create_RifleAmmo( Vector(-394.971, -2906.485, -239.969), Angle(0, 107.426, 0) )
		Create_PistolAmmo( Vector(-506.476, -2815.049, -239.969), Angle(0, 300.96, 0) )
		Create_PistolAmmo( Vector(-484.862, -2820.264, -239.969), Angle(0, 284.449, 0) )
		Create_PistolAmmo( Vector(-510.041, -2849.986, -239.969), Angle(0, 328.548, 0) )
		Create_PistolAmmo( Vector(-489.218, -2853.689, -239.969), Angle(0, 312.037, 0) )
		Create_Armour200( Vector(-417.871, -3224.047, -7.969), Angle(0, 268.773, 0) )
		Create_Bread( Vector(-419.094, -3257.046, -7.969), Angle(0, 269.609, 0) )
		Create_Bread( Vector(-418.894, -3272.819, -7.969), Angle(0, 269.191, 0) )
		Create_Bread( Vector(-419.171, -3292.468, -7.969), Angle(0, 269.191, 0) )
		Create_Bread( Vector(-419.407, -3309.17, -7.969), Angle(0, 269.191, 0) )
		Create_Bread( Vector(-419.684, -3328.82, -7.969), Angle(0, 269.191, 0) )
		Create_Bread( Vector(-419.419, -3351.981, -7.969), Angle(0, 268.146, 0) )
		Create_Bread( Vector(-420.524, -3371.239, -7.969), Angle(0, 270.236, 0) )
		Create_Bread( Vector(-416.723, -3394.303, -7.969), Angle(0, 270.028, 0) )
		Create_Medkit25( Vector(-536.464, -3563.39, -167.969), Angle(0, 44.941, 0) )
		Create_Medkit25( Vector(-582.793, -3516.289, -167.969), Angle(0, 44.314, 0) )
		Create_Armour( Vector(-600.402, -3414.229, -167.969), Angle(0, 334.507, 0) )
		Create_Armour( Vector(-600.89, -3465.478, -167.969), Angle(0, 17.98, 0) )
		Create_PortableMedkit( Vector(-558.527, -3519.257, -167.969), Angle(0, 46.404, 0) )
		Create_PortableMedkit( Vector(-540.827, -3536.516, -167.969), Angle(0, 61.974, 0) )
		Create_Armour5( Vector(-561.458, -3539.114, -167.969), Angle(0, 47.759, 0) )
		Create_Armour5( Vector(-570.169, -3547.791, -164.937), Angle(0, 47.132, 0) )
		Create_FrontlinerAmmo( Vector(-811.634, -3121.636, -295.969), Angle(0, 336.807, 0) )
		Create_ManaCrystal100( Vector(-415.079, -3701.829, -295.969), Angle(0, 268.153, 0) )
		Create_ManaCrystal( Vector(-830.318, -3314.128, -295.969), Angle(0, 285.709, 0) )
		Create_ManaCrystal( Vector(-834.219, -3384.599, -295.969), Angle(0, 318.104, 0) )
		Create_ManaCrystal( Vector(-835.173, -3454.251, -295.969), Angle(0, 359.486, 0) )
		Create_ManaCrystal( Vector(-6.702, -3328.76, -295.969), Angle(0, 208.379, 0) )
		Create_ManaCrystal( Vector(2.435, -3393.27, -295.969), Angle(0, 192.286, 0) )
		Create_ManaCrystal( Vector(4.51, -3463.637, -295.969), Angle(0, 167.833, 0) )
		Create_FoolsOathAmmo( Vector(1.372, -3179.069, -295.969), Angle(0, 45.856, 0) )
		Create_Medkit10( Vector(-848.141, -3438.803, -167.969), Angle(0, 3.561, 0) )
		Create_Medkit10( Vector(-855.854, -3471.635, -167.969), Angle(0, 17.564, 0) )
		Create_Medkit10( Vector(-817.903, -3468.49, -167.969), Angle(0, 22.58, 0) )
		Create_Armour5( Vector(5.051, -3472.073, -167.969), Angle(0, 121.437, 0) )
		Create_Armour5( Vector(-23.533, -3470.227, -167.969), Angle(0, 106.18, 0) )
		Create_Armour5( Vector(11.617, -3435.342, -167.969), Angle(0, 139.829, 0) )
		Create_Armour5( Vector(14.097, -3456.423, -167.969), Angle(0, 131.051, 0) )
		Create_Armour5( Vector(-17.204, -3452.269, -167.969), Angle(0, 114.749, 0) )
		Create_Armour5( Vector(-6.699, -3434.342, -167.969), Angle(0, 130.006, 0) )
		Create_GrenadeAmmo( Vector(-365.231, -3557.263, -167.969), Angle(0, 117.049, 0) )
		Create_GrenadeAmmo( Vector(-377.868, -3564.642, -167.969), Angle(0, 101.374, 0) )
		Create_M6GAmmo( Vector(-403.515, -3563.562, -167.969), Angle(0, 80.683, 0) )
		Create_Beer( Vector(2885.302, -3835.187, -871.969), Angle(0, 355.991, 0) )
		Create_Beer( Vector(3022.534, -3834.729, -807.969), Angle(0, 353.901, 0) )
		Create_Beer( Vector(3088.478, -3770.727, -807.969), Angle(0, 282.527, 0) )
		Create_Beer( Vector(3079.797, -3591.438, -807.969), Angle(0, 269.674, 0) )
		Create_Beer( Vector(3080.703, -3499.255, -799.969), Angle(0, 268.629, 0) )
		Create_SMGWeapon( Vector(3164.129, -3340.586, -799.969), Angle(0, 296.426, 0) )
		Create_SMGAmmo( Vector(3142.053, -3362.125, -799.969), Angle(0, 332.165, 0) )
		Create_Backpack( Vector(3372.284, -3430.174, -415.911), Angle(0, 46.875, 0) )
		Create_Backpack( Vector(3406.951, -3433.901, -415.911), Angle(0, 68.402, 0) )
		Create_SpiritSphere( Vector(3438.122, -3405.144, -415.416), Angle(0, 94.422, 0) )
		Create_InfiniteGrenades( Vector(3941.339, -3469.911, -415.969), Angle(0, 90.549, 0) )
		Create_KingSlayerWeapon( Vector(4006.693, -3356.033, -415.969), Angle(0, 172.372, 0) )
		Create_KingSlayerAmmo( Vector(3974.326, -3401.647, -415.969), Angle(0, 124.616, 0) )
		Create_CrossbowWeapon( Vector(3962.985, -3346.201, -415.969), Angle(0, 199.229, 0) )
		Create_CrossbowAmmo( Vector(3917.949, -3350.451, -415.969), Angle(0, 270.707, 0) )
		Create_CrossbowAmmo( Vector(3935.846, -3374.718, -415.969), Angle(0, 238.939, 0) )
		Create_CrossbowAmmo( Vector(3937.296, -3407.685, -415.969), Angle(0, 166.938, 0) )
		Create_EldritchArmour( Vector(3881.471, -3429.283, -415.969), Angle(0, 269.555, 0) )
		Create_ManaCrystal100( Vector(4418.143, -4193.25, -543.969), Angle(0, 1.617, 0) )
		Create_ManaCrystal100( Vector(3937.507, -4213.738, -543.969), Angle(0, 87.516, 0) )
		Create_Armour100( Vector(4808.568, -4629.234, -543.969), Angle(0, 212.31, 0) )
		Create_CorzoHat( Vector(4400.397, -3076.897, -415.969), Angle(0, 271.393, 0) )
		Create_Medkit25( Vector(4055.378, -4348.677, -543.969), Angle(0, 269.388, 0) )
		Create_Medkit25( Vector(4285.971, -4350.744, -543.969), Angle(0, 270.851, 0) )
		Create_Medkit25( Vector(4798.623, -4206.991, -543.969), Angle(0, 215.573, 0) )
		Create_Armour( Vector(4798.117, -4271.657, -543.969), Angle(0, 157.053, 0) )
		Create_FrontlinerAmmo( Vector(4017.52, -3923.311, -543.969), Angle(0, 281.301, 0) )
		Create_HarbingerAmmo( Vector(4308.194, -3929.789, -543.969), Angle(0, 221.318, 0) )
		
		local SBUTTON1 = function()		
			CreateTeleporter( "SB1", Vector(3523.729,-3629.323,-415.969), "SB2", Vector(4379.937,-3365.788,-415.969) )
			CreateRestingPoint( Vector(4400.048,-3022.693,-415.969), Angle(0,266.177,0) )
		end
		
		AddSecretButton( Vector(3669.082,-3631.545,-360.882), SBUTTON1 )
		
		CreateSecretArea( Vector(-415.597,-3408.203,-3.438), Vector(-57.492,-57.492,0), Vector(57.492,57.492,43.984) )
		CreateSecretArea( Vector(3411.406,-3406.057,-415.911), Vector(-48.779,-48.779,0), Vector(48.779,48.779,55.298) )
	
	end
	
	for _,v in pairs (ents.FindByClass("npc_turret_floor")) do
	
		if v and IsValid(v) then
			v.FAKE_HEALTH = 999999
			v:AddRelationship("player D_LI 99")
		end
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

function NOKILL_TURRETS( ent )

	timer.Simple(0.1,function()
	
		if ent and IsValid(ent) then
		
			if ent:GetClass() == "npc_turret_floor" then
				ent.FAKE_HEALTH = 999999
				ent:AddRelationship("player D_LI 99")
				ent:AddRelationship("npc_alyx D_LI 99")
			end
		
		end
	
	end)

end
hook.Add("OnEntityCreated","NOKILL_TURRETSHook",NOKILL_TURRETS)

local DONT_DIE_LMAO = {
npc_eli = true,
npc_kleiner = true,
npc_kliener = true,
npc_magnusson = true,
npc_mossman = true}

function STOP_NPC_DEATH( ent, dmginfo )

	if ent and IsValid(ent) and ent.GetClass then

		local CLASS = ent:GetClass()
		
		if DONT_DIE_LMAO and DONT_DIE_LMAO[CLASS] then
			ent:SetHealth(50000)
			dmginfo:SetDamage(0)
			dmginfo:ScaleDamage(0)
		end
		
	end

end
hook.Add("EntityTakeDamage","XORDER_STOP_NPC_DEATHHook",STOP_NPC_DEATH)