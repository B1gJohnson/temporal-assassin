		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["09_ov_firm_hq"] = {"10_out_robinson", Vector(2288.871,138.354,183.592), Vector(-108.246,-108.246,0), Vector(108.246,108.246,116.143)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("env_fade")
	RemoveAllEnts("trigger_teleport")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(1906.658,-1864.913,-96.469), Angle(0,89.318,0))

	Create_Medkit25( Vector(2552.257, 260.392, 40.413), Angle(0, 262.366, 0) )
	Create_Armour( Vector(2633.346, 31.071, 41.054), Angle(0, 177.35, 0) )
	Create_Backpack( Vector(2316.031, -128.075, 0.033), Angle(0, 355.991, 0) )
	Create_ShotgunAmmo( Vector(1107.608, -328.732, 33.429), Angle(0, -191.697, 0) )
	Create_ShotgunAmmo( Vector(1106.944, -329.073, 38.406), Angle(0, -133.192, 0) )
	Create_GrenadeAmmo( Vector(328.459, 295.838, 40.15), Angle(0, 12.982, 0) )
	Create_GrenadeAmmo( Vector(327.426, 303.292, 40.328), Angle(0, 355.206, 0) )
	Create_Medkit25( Vector(374.884, -289.395, 0.031), Angle(0, 340.519, 0) )
	Create_ShotgunAmmo( Vector(326.581, 193.105, 40.191), Angle(0, -328.881, 0) )
	Create_ShotgunAmmo( Vector(327.598, 192.667, 45.396), Angle(0, 346.151, 0) )
	Create_RifleAmmo( Vector(327.809, 210.934, 40.575), Angle(0, 350.551, 0) )
	Create_RifleAmmo( Vector(329.036, 210.707, 43.162), Angle(0, 328.903, 0) )
	Create_Bread( Vector(-165.76, -212.5, 38.019), Angle(0, 193.479, 0) )
	Create_Beer( Vector(-167.823, -194.135, 38.118), Angle(0, 262.823, 0) )
	Create_PortableMedkit( Vector(-160.259, -157.75, 38.031), Angle(0, 287.551, 0) )
	Create_Armour( Vector(-272.988, -162.35, 36.031), Angle(0, 345.799, 0) )
	Create_Medkit10( Vector(100.947, -201.676, 218.98), Angle(-90, 416.272, -135) )
	Create_RevolverAmmo( Vector(107.982, -223.526, 218.178), Angle(90, 12.8, 180) )
	Create_Bread( Vector(112.603, -163.106, 230.53), Angle(0, 208.431, 0) )
	Create_GrenadeAmmo( Vector(816.048, 33.789, 216.246), Angle(0, 107.671, 0) )
	Create_GrenadeAmmo( Vector(825.215, 37.143, 216.01), Angle(0, 122.543, 0) )
	Create_Medkit25( Vector(558.268, -244.015, 216.054), Angle(0, 348.959, 0) )
	Create_Armour( Vector(-430.541, 79.432, 207.693), Angle(0, 342.447, 0) )
	Create_PortableMedkit( Vector(-458.565, 77.872, 207.693), Angle(0, 329.775, 0) )
	Create_PistolWeapon( Vector(-570.299, -6.48, 207.693), Angle(0, 365.503, 0) )
	Create_Armour100( Vector(1405.988, -158.891, 207.693), Angle(0, 122.191, 0) )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)