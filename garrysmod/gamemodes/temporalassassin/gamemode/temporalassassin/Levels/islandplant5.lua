		
if CLIENT then return end

LEVEL_STRINGS["islandplant5"] = { "islandresort", Vector(-3743.772,-1804.532,472.031), Vector(-97.129,-97.129,0), Vector(97.129,97.129,327.419) }

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-2237.777,-3243.541,343.976),Angle(0,0,0))
	
	CreateProp(Vector(-2239.781,-1539.625,606.563), Angle(0,-88.374,0.088), "models/props_c17/lampshade001a.mdl", true)
	CreateProp(Vector(-2151.625,-1540.25,606.656), Angle(0.439,-104.37,0), "models/props_c17/lampshade001a.mdl", true)
	
	Create_VolatileBattery( Vector(-2194.068, -3124.387, 600.031), Angle(0, 85.84, 0) )
	Create_Medkit10( Vector(-2342.014, -2044.157, 216.031), Angle(0, 57.02, 0) )
	Create_Medkit10( Vector(-2303.995, -2036.863, 216.031), Angle(0, 72.64, 0) )
	Create_RevolverWeapon( Vector(-2391.655, -1966.762, 344.031), Angle(0, 185.58, 0) )
	Create_PistolWeapon( Vector(-2758.932, -2080.339, 344.031), Angle(0, 80.86, 0) )
	Create_SuperShotgunWeapon( Vector(-2707.258, -2116.729, 344.031), Angle(0, 92.96, 0) )
	Create_SMGWeapon( Vector(-2425.166, -2177.619, 472.031), Angle(0, 175.24, 0) )
	Create_SMGAmmo( Vector(-2430.899, -2125.629, 472.031), Angle(0, 219.9, 0) )
	Create_Armour5( Vector(-2760.538, -2764.931, 472.031), Angle(0, 359.9, 0) )
	Create_Armour5( Vector(-2872.829, -2764.733, 472.031), Angle(0, 359.9, 0) )
	Create_Armour5( Vector(-3004.998, -2764.5, 472.031), Angle(0, 359.9, 0) )
	Create_Armour5( Vector(-3147.373, -2764.25, 472.031), Angle(0, 359.9, 0) )
	Create_Beer( Vector(-3315.611, -2732.254, 472.031), Angle(0, 8.92, 0) )
	Create_Beer( Vector(-3317.374, -2674.979, 472.031), Angle(0, 0.56, 0) )
	Create_Beer( Vector(-3318.082, -2622.634, 472.031), Angle(0, 0.56, 0) )
	Create_Armour( Vector(-2958.857, -2438.976, 472.031), Angle(0, 249.924, 0) )
	Create_ManaCrystal( Vector(-3248.478, -2890.323, 472.031), Angle(0, 101.374, 0) )
	Create_ManaCrystal( Vector(-3307.123, -2891.228, 472.031), Angle(0, 73.434, 0) )
	Create_RevolverAmmo( Vector(-3118.765, -2384.663, 472.031), Angle(0, 120.147, 0) )
	Create_Medkit25( Vector(-3163.87, -2382.756, 472.031), Angle(0, 99.027, 0) )
	Create_Medkit25( Vector(-3209.758, -2353.671, 472.031), Angle(0, 68.667, 0) )
	Create_ShotgunAmmo( Vector(-3082.671, -2195.823, 472.031), Angle(0, 170.686, 0) )
	Create_M6GAmmo( Vector(-3083.454, -2099.544, 472.031), Angle(0, 191.366, 0) )
	Create_RifleAmmo( Vector(-3085.154, -2149.48, 472.031), Angle(0, 176.186, 0) )
	Create_PortableMedkit( Vector(-3590.391, -2327.745, 472.031), Angle(0, 0.326, 0) )
	Create_CorzoHat( Vector(-3864.558, -3995.593, 472.031), Angle(0, 93.246, 0) )
	Create_Armour200( Vector(-3778.734, -3983.106, 472.031), Angle(0, 118.986, 0) )
	
	local B_FUNC = function()
		CreateTeleporter( "sectp_1", Vector(-3960.179,-3599.856,472.031), "sectp_2", Vector(-2196.777,-1810.827,600.031) )
	end
	
	AddSecretButton( Vector(-2194.354,-1529.052,623.893), B_FUNC )
		
	CreateSecretArea( Vector(-2194.42,-3108.778,604.563), Vector(-33.658,-33.658,0), Vector(33.658,33.658,88.978) )
	CreateSecretArea( Vector(-2957.135,-2438.906,476.563), Vector(-18.911,-18.911,0), Vector(18.911,18.911,21.779) )
	CreateSecretArea( Vector(-3887.428,-3959.658,472.031), Vector(-186.287,-186.287,0), Vector(186.287,186.287,231.938) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)