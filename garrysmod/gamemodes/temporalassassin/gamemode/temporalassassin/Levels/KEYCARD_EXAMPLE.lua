		
if CLIENT then return end

function CheckKeyUnlocks( red, blue, yellow, green, purple )

	--[[
	This runs whenever you pick up a keycard of the corresponding colour, pretty self explanitory.
	You can do whatever you want here if you know what you're doing.
	]]

	if red then
	
		if GLOB_DOOR_RED and IsValid(GLOB_DOOR_RED) then
			GLOB_DOOR_RED:Fire("Unlock")
		end
	
	end
	
	if blue then
	
		if GLOB_DOOR_BLUE and IsValid(GLOB_DOOR_BLUE) then
			GLOB_DOOR_BLUE:Fire("Unlock")
		end
	
	end
	
	if yellow then
	
		if GLOB_DOOR_YELLOW and IsValid(GLOB_DOOR_YELLOW) then
			GLOB_DOOR_YELLOW:Fire("Unlock")
		end
	
	end
	
	if green then
	
		if GLOB_DOOR_GREEN and IsValid(GLOB_DOOR_GREEN) then
			GLOB_DOOR_GREEN:Fire("Unlock")
		end
	
	end
	
	if purple then
	
		if GLOB_DOOR_PURPLE and IsValid(GLOB_DOOR_PURPLE) then
			GLOB_DOOR_PURPLE:Fire("Unlock")
		end
	
	end

end

function Setup_KeyCards()

	--[[
	DO NOT CHANGE THE NAMES OF THESE GLOBALS
	They are used internally for the keycard items
	]]
	GLOB_RED_KEY = false
	GLOB_BLUE_KEY = false
	GLOB_YELLOW_KEY = false
	GLOB_GREEN_KEY = false
	GLOB_PURPLE_KEY = false

	timer.Simple(1,function()
	
		GLOB_DOOR_RED = false
		GLOB_DOOR_BLUE = false
		GLOB_DOOR_YELLOW = false
		GLOB_DOOR_GREEN = false
		GLOB_DOOR_PURPLE = false
		
		--[[ You can grab map creation ID's with the following while standing right next to a door:
			lua_run PrintTable(ents.FindInSphere(Entity(1):GetPos(),100))
			Find the func_door or prop_rotating_door entity and look at its number between the []'s
			lua_run print(Entity(NUMBER):MapCreationID())
			Pop that ID in the check below with what door you wish it to be
			You can find key hint props under models/TemporalAssassin/Props/KeyHint/
			If you wish to find them quickly use DEV MODE, F1, SPAWNLISTS, BROWSE, GAMES, ALL, TEMPORAL ASSASSIN, PROPS, KEY HINTS
		]]
		for _,v in pairs (ents.FindByClass("func_door")) do
			if v and IsValid(v) then
				local ID = v:MapCreationID()
				if ID == 1411 then GLOB_DOOR_RED = v end
				if ID == 1328 then GLOB_DOOR_BLUE = v end
				if ID == 1355 then GLOB_DOOR_YELLOW = v end
			end
		end
	
		if GLOB_DOOR_RED and IsValid(GLOB_DOOR_RED) then
			GLOB_DOOR_RED:Fire("Lock")
		end
		
		if GLOB_DOOR_BLUE and IsValid(GLOB_DOOR_BLUE) then
			GLOB_DOOR_BLUE:Fire("Lock")
		end
		
		if GLOB_DOOR_YELLOW and IsValid(GLOB_DOOR_YELLOW) then
			GLOB_DOOR_YELLOW:Fire("Lock")
		end
		
		if GLOB_DOOR_GREEN and IsValid(GLOB_DOOR_GREEN) then
			GLOB_DOOR_GREEN:Fire("Lock")
		end
		
		if GLOB_DOOR_PURPLE and IsValid(GLOB_DOOR_PURPLE) then
			GLOB_DOOR_PURPLE:Fire("Lock")
		end
		
	end)
	
	--[[ Key Skins:
		0 = Red
		1 = Blue
		2 = Yellow
		3 = Green
		4 = Purple
	]]
	
	--[[ Red Key ]]
	Create_KeyCard( Vector(608.524,1888.54,64.031), Angle(0,0,0), 0 )
	
	--[[ Blue Key ]]
	Create_KeyCard( Vector(1048.524,188.54,314.031), Angle(0,0,0), 1 )

end

function MAP_LOADED_FUNC()

	Setup_KeyCards()
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

--[[
Everything below here is all about putting a message on the players screen if they try to
use a locked key door that has been set with a global from above, you can change this
as you see fit as long as you know what you're doing.
]]

util.AddNetworkString("KEY_ScreenMessage")

function KEYMessage( ply, str )

	net.Start("KEY_ScreenMessage")
	net.WriteString(str)
	net.Send(ply)

end

function MAP_PlayerUse( ply, ent )

	local CT = CurTime()
	local LAST_USE = (ply.MAP_LastPlayerUse and CT < ply.MAP_LastPlayerUse)
	
	if not LAST_USE then
	
		ply.MAP_LastPlayerUse = CT + 0.5
		
		if ent and IsValid(ent) then
		
			if GLOB_DOOR_RED and ent == GLOB_DOOR_RED and GLOB_DOOR_RED:IsDoorLocked() then
				KEYMessage( ply, "I NEED A RED KEYCARD" )
			end
		
			if GLOB_DOOR_BLUE and ent == GLOB_DOOR_BLUE and GLOB_DOOR_BLUE:IsDoorLocked() then
				KEYMessage( ply, "I NEED A BLUE KEYCARD" )
			end
		
			if GLOB_DOOR_YELLOW and ent == GLOB_DOOR_YELLOW and GLOB_DOOR_YELLOW:IsDoorLocked() then
				KEYMessage( ply, "I NEED A YELLOW KEYCARD" )
			end
		
			if GLOB_DOOR_GREEN and ent == GLOB_DOOR_GREEN and GLOB_DOOR_GREEN:IsDoorLocked() then
				KEYMessage( ply, "I NEED A GREEN KEYCARD" )
			end
		
			if GLOB_DOOR_PURPLE and ent == GLOB_DOOR_PURPLE and GLOB_DOOR_PURPLE:IsDoorLocked() then
				KEYMessage( ply, "I NEED A PURPLE KEYCARD" )
			end
		
		end
	
	end

end
hook.Add("PlayerUse","MAP_PlayerUseHook",MAP_PlayerUse)