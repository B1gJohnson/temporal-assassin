
if CLIENT then return end

function MAP_LOADED_FUNC()

	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then

		Create_Armour( Vector(-9188.2255859375, -3426.7233886719, 192.03125), Angle(0, 46.510101318359, 0) )
		Create_PistolAmmo( Vector(-8008.6059570313, -2181.33984375, -3.96875), Angle(0, 185.94538879395, 0) )
		Create_Backpack( Vector(-7833.5073242188, -742.50140380859, -63.968746185303), Angle(0, 165.92503356934, 0) )
		Create_Armour100( Vector(-6856.224609375, -1496.4290771484, -63.96875), Angle(0, 88.264701843262, 0) )
		
		CreateSecretArea( Vector(-7839.787109375,-740.67236328125,-59.40625), Vector(-31.545774459839,-31.545774459839,0), Vector(31.545774459839,31.545774459839,75.272590637207) )
		CreateSecretArea( Vector(-6855.57421875,-1491.9985351563,-59.437503814697), Vector(-24.593053817749,-24.593053817749,0), Vector(24.593053817749,24.593053817749,45.302726745605) )
		
	else
	
		Create_Armour( Vector(-9194.944, -3419.308, 192.031), Angle(0, 22.344, 0) )
		Create_PistolAmmo( Vector(-8754.529, -3006.842, -5.906), Angle(0, 37.602, 0) )
		Create_PistolAmmo( Vector(-8773.58, -2966.02, -5.906), Angle(0, 332.811, 0) )
		Create_Medkit25( Vector(-8016.753, -2180.605, -3.969), Angle(0, 173.804, 0) )
		Create_Armour100( Vector(-8170.964, -747.627, -63.969), Angle(0, 21.946, 0) )
		Create_Medkit25( Vector(-7903.851, -855.406, -63.969), Angle(0, 240.245, 0) )
		Create_Medkit10( Vector(-7831.748, -854.59, -63.969), Angle(0, 216.523, 0) )
		Create_Medkit10( Vector(-7864.827, -857.127, -63.969), Angle(0, 224.465, 0) )
		Create_PortableMedkit( Vector(-7834.551, -899.335, -63.969), Angle(0, 201.266, 0) )
	
		CreateSecretArea( Vector(-8157.586,-720.554,-63.969), Vector(-45.863,-45.863,0), Vector(45.863,45.863,78.604) )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)