		
if CLIENT then return end

util.AddNetworkString("SIN_UpdateCount")
util.AddNetworkString("SIN_AskForGoal")
util.AddNetworkString("SIN_SinOnMap")

function SIN_AskForGoal_LIB( len, pl )

	net.Start("SIN_UpdateCount")
	net.WriteUInt(LAST_SIN_COUNT,6)
	net.WriteUInt(SIN_COUNT_GOAL,6)
	net.Broadcast()

end
net.Receive("SIN_AskForGoal",SIN_AskForGoal_LIB)

GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true

GLOB_NO_FENIC = false
GLOB_NO_SINS = true

GLOB_SINHUNTER_SPAWN1 = false
GLOB_SINHUNTER_SPAWN2 = false
GLOB_SINHUNTER_SPAWNNPCS = 0
GLOB_SINHUNTER_ENEMYCOUNT = 0
GLOB_SINFUL_ENEMY_COUNT = 0

SIN_SPAWN_SPEED = 5
SIN_SPAWN_MAX = 30
SIN_ENEMIES_KILLED = 0
SIN_SINSPAWN_THRESHOLD = 15

local SPAWN_DIFFICULTIES = {}
SPAWN_DIFFICULTIES[1] = {5,20,15}
SPAWN_DIFFICULTIES[2] = {4,30,22}
SPAWN_DIFFICULTIES[3] = {3,40,25}
SPAWN_DIFFICULTIES[4] = {3,40,25}
SPAWN_DIFFICULTIES[5] = {2.5,50,30}
SPAWN_DIFFICULTIES[6] = {2,50,30}
SPAWN_DIFFICULTIES[7] = {1.5,50,30}
SPAWN_DIFFICULTIES[8] = {1.5,60,35}
SPAWN_DIFFICULTIES[9] = {1,65,35}
SPAWN_DIFFICULTIES[10] = {1,70,35}
SPAWN_DIFFICULTIES[11] = {1,70,35}
SPAWN_DIFFICULTIES[12] = {1,70,35} --[[ Just incase more difficulties ARE added ]]
SPAWN_DIFFICULTIES[13] = {1,70,35}
SPAWN_DIFFICULTIES[14] = {1,70,35}

GLOB_SINTYPE = 1
--[[
This defines what enemies will spawn for this sin hunter mission.

1 = Combine
2 = Zombies
3 = Antlions
4 = HL:S Humans
5 = HL:S Aliens

You can also define it as GLOB_SINTYPE = math.random(1,5) to make it random everytime its played
]]

local SIN_CLASSES = {}

SIN_CLASSES[1] = {
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s"}

if IsMounted("ep2") then
	SIN_CLASSES[1] = {
	"npc_metropolice",
	"npc_combine_s",
	"npc_metropolice",
	"npc_combine_s",
	"npc_metropolice",
	"npc_combine_s"}
end

SIN_CLASSES[2] = {
"npc_zombie",
"npc_zombie",
"npc_zombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_poisonzombie"}

SIN_CLASSES[3] = {
"npc_antlion"}

if IsMounted("ep2") then
	SIN_CLASSES[3] = {
	"npc_antlion",
	"npc_antlion",
	"npc_antlion",
	"npc_antlion_worker"}
end

SIN_CLASSES[4] = SIN_CLASSES[1]
SIN_CLASSES[5] = SIN_CLASSES[2]

if IsMounted("hl1") then

	SIN_CLASSES[4] = {
	"monster_human_grunt"}
	
	SIN_CLASSES[5] = {
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullsquid",
	"monster_houndeye",
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullchicken",
	"monster_houndeye"}
	
end

local BIGSIN_CLASSES = {}

BIGSIN_CLASSES[1] = {
"npc_combine_s"}

if IsMounted("ep2") then
	BIGSIN_CLASSES[1] = {
	"npc_hunter"}
end

BIGSIN_CLASSES[2] = {
"npc_poisonzombie"}

if IsMounted("ep2") or IsMounted("episodic") then
	BIGSIN_CLASSES[2] = {
	"npc_zombine"}
end

BIGSIN_CLASSES[3] = {
"npc_antlionguard"}
BIGSIN_CLASSES[4] = BIGSIN_CLASSES[1]
BIGSIN_CLASSES[5] = BIGSIN_CLASSES[2]

if IsMounted("hl1") then
	BIGSIN_CLASSES[4] = {
	"monster_human_assassin"}
	
	BIGSIN_CLASSES[5] = {
	"monster_alien_slave"}
end

local SIN_WEAPONS = {}
SIN_WEAPONS["npc_metropolice"] = {
"weapon_pistol",
"weapon_smg1",
"weapon_stunstick"}
SIN_WEAPONS["npc_combine_s"] = {
"weapon_smg1",
"weapon_ar2",
"weapon_shotgun"}

function SetAsRespawningItem( ent, del )

	if not ent then return end
	if not ent.ITEM_USE then return end
	
	local POS,ANG,MDL,USE = ent:GetPos(), ent:GetAngles(), ent:GetModel(), ent.ITEM_USE
	
	if not del then del = 30 end
	
	ent.OnRemove = function()
	
		timer.Simple(del,function()
		
			local N_ITEM = CreateItemPickup( POS, ANG, MDL, USE )
			CreatePointParticle( "Doll_EvilSpawn_Activate", POS, POS, Angle(0,0,0), Angle(0,0,0), 1 )
			sound.Play( Sound("TemporalAssassin/Doll/ExtraSpawn_Item.wav"), POS, 70, math.random(99,101), 0.7 )
			SetAsRespawningItem(N_ITEM,del)
			
		end)
	
	end

end

LAST_SIN_COUNT = 0
SIN_COUNT_GOAL = 4

function CanWeSpawnHere(pos)

	local TR = {}
	TR.start = pos
	TR.endpos = pos
	TR.mask = MASK_NPCSOLID
	TR.filter = {}
	table.Add(TR.filter,GLOB_TRIGGER_ENTS)
	TR.mins = Vector(-16,-16,0)
	TR.maxs = Vector(16,16,72)
	local Trace = util.TraceHull(TR)
	
	if Trace.Hit then return false end
	
	return true

end

function EnemySpawns_Think()

	local CT = CurTime()
	
	if SIN_ENEMIES_KILLED and SIN_ENEMIES_KILLED > SIN_SINSPAWN_THRESHOLD then
		SIN_ENEMIES_KILLED = 0
		Create_SinfulBoi()
	end
	
	net.Start("SIN_SinOnMap")
	if GLOB_SINFUL_ENEMY_COUNT and GLOB_SINFUL_ENEMY_COUNT > 0 then
		net.WriteBool(true)
	else
		net.WriteBool(false)
	end
	net.Broadcast()
	
	if GLOB_SIN_DEVOURED_COUNT and GLOB_SIN_DEVOURED_COUNT > LAST_SIN_COUNT then
	
		LAST_SIN_COUNT = GLOB_SIN_DEVOURED_COUNT
		
		local SIN_LEFT = (SIN_COUNT_GOAL - LAST_SIN_COUNT)
		
		net.Start("SIN_UpdateCount")
		net.WriteUInt(LAST_SIN_COUNT,6)
		net.WriteUInt(SIN_COUNT_GOAL,6)
		net.Broadcast()
		
		if SIN_LEFT <= 0 and not SIN_GAME_ENDED then
			SIN_GAME_ENDED = true
			END_SIN_HUNTER()			
		end
		
	end
	
	if GLOB_SINHUNTER_SPAWNNPCS and CT >= GLOB_SINHUNTER_SPAWNNPCS and GLOB_SINHUNTER_STARTED then
	
		GLOB_SINHUNTER_SPAWNNPCS = CT + SIN_SPAWN_SPEED
	
		local SPAWN_POS
	
		if GLOB_SINHUNTER_SPAWN2 then
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_FRONT) + Vector(0,0,5)
		else
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_BACK) + Vector(0,0,5)
		end
		
		if SPAWN_POS and GLOB_SINHUNTER_ENEMYCOUNT < SIN_SPAWN_MAX and GLOB_SINTYPE and SIN_CLASSES[GLOB_SINTYPE] then
		
			local CLASS = table.Random(SIN_CLASSES[GLOB_SINTYPE])
			local WEP = false
			
			if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
				WEP = table.Random(SIN_WEAPONS[CLASS])
			end
			
			if CanWeSpawnHere(SPAWN_POS) then
				CreateSnapNPC( CLASS, SPAWN_POS, Angle(0,math.random(-360,360),0), WEP, false )
				GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT + 1
			end
		
		end
	
	end

end
timer.Create("EnemySpawns_ThinkTimer", 0.5, 0, EnemySpawns_Think)

function Set_EnemySpawns_1()

	print("Back of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = true
	GLOB_SINHUNTER_SPAWN2 = false

end

function Set_EnemySpawns_2()

	print("Front of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = true

end

function START_SIN_HUNTER()

	RemoveAllTeleporters()
	
	CreateProp(Vector(1180.375,1434.906,226.094), Angle(2.461,14.282,-14.897), "models/temporalassassin/doll/unknown.mdl", true)
	
	local STP1, STP2 = CreateTeleporter( "SEC_1", Vector(-876.119,304.517,448.031), "SEC_2", Vector(1451.339,1474.022,230.309) )
	
	local RemTelSecret1 = function()
	
		local RemTelSecret2 = function()
			if STP1 and STP2 then
				STP1:Remove()
				STP2:Remove()
			end
		end
		
		CreateTrigger( Vector(-880.326,308.003,448.031), Vector(-114.761,-114.761,0), Vector(114.761,114.761,165.647), RemTelSecret2 )
	
	end
	
	CreateTrigger( Vector(1361.663,1490.456,218.274), Vector(-172.198,-172.198,0), Vector(172.198,172.198,331.257), RemTelSecret1 )
	
	--[[ This is the trigger that enables Back side map spawns ]]	
	CreateTrigger( Vector(271.415,-3285.106,-2003.742), Vector(-2895.564,-2895.564,0), Vector(2895.564,2895.564,5306.351), Set_EnemySpawns_1, true )
	
	--[[ This is the trigger that enables Front side map spawns ]]
	CreateTrigger( Vector(-292.515,2350.074,-2226.07), Vector(-2712.515,-2712.515,0), Vector(2712.515,2712.515,4910.658), Set_EnemySpawns_2, true )
	
	GLOB_SINHUNTER_STARTED = true
	GLOB_SINHUNTER_SPAWNNPCS = CurTime() + 20
	
	local DIFF = GetDifficulty2()

	if SPAWN_DIFFICULTIES and SPAWN_DIFFICULTIES[DIFF] then
		SIN_SPAWN_SPEED = SPAWN_DIFFICULTIES[DIFF][1]
		SIN_SPAWN_MAX = SPAWN_DIFFICULTIES[DIFF][2]
		SIN_SINSPAWN_THRESHOLD = SPAWN_DIFFICULTIES[DIFF][3]
	end
	
end

function END_SIN_HUNTER()

	GLOB_SINHUNTER_STARTED = false

	for _,v in pairs (ents.GetAll()) do
	
		if v and IsValid(v) then
		
			if v:IsNPC() then
				v:Remove() --[[ Lets remove all the npcs ]]
			end
		
		end
	
	end
	
	GLOB_SINFUL_ENEMY_COUNT = 0
	net.Start("SIN_SinOnMap")
	net.WriteBool(false)
	net.Broadcast()
	
	CreateTeleporter( "START_1", Vector(2.336,-2067.98,260.06), "START_2", Vector(-963.351,-1221.561,243.674) )
	
	local GAME_END_TEXT = {
	""}

	MakeGameEnd( GAME_END_TEXT, 3, Vector(50.824,-2046.59,253.809), Vector(-136.981,-136.981,0), Vector(136.981,136.981,111.966) )
	
end

function Create_SinfulBoi()

	local SPAWNS_BACK = {
	Vector(341.33,965.308,226.311)}
	
	local SPAWNS_FRONT = {
	Vector(942.744,-545.317,214.026)}
	
	local SPAWN_POINT
	
	if GLOB_SINHUNTER_SPAWN2 then
		SPAWN_POINT = table.Random(SPAWNS_FRONT)
	elseif GLOB_SINHUNTER_SPAWN1 then
		SPAWN_POINT = table.Random(SPAWNS_BACK)
	end
	
	if not SPAWN_POINT then return end
	if not GLOB_SINTYPE then return end
	if not BIGSIN_CLASSES then return end
	if not BIGSIN_CLASSES[GLOB_SINTYPE] then return end
	
	local CLASS = table.Random(BIGSIN_CLASSES[GLOB_SINTYPE])
	local WEP = false
			
	if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
		WEP = table.Random(SIN_WEAPONS[CLASS])
	end
	
	GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT + 1
	
	local SIN_BOI = CreateSnapNPC( CLASS, SPAWN_POINT, Angle(0,math.random(-360,360),0), WEP, false )
			
	timer.Simple(0.1,function()
		if SIN_BOI and IsValid(SIN_BOI) then
			SIN_BOI:SetHealth( SIN_BOI:Health()*2 )
			SIN_BOI:CreateSinner(true)
			SIN_BOI:SetModelScale(1.2,0) --[[ Lets make sin bois a bit bigger in scale ]]
			if GLOB_SINTYPE == 1 then
				SIN_BOI:MakeCombineShielder()
			end
		end
	end)

end

function SinHunter_NPCDeath( npc, att, inf )

	if not npc.SinHealth then
		SIN_ENEMIES_KILLED = SIN_ENEMIES_KILLED + 1
		GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT - 1
	elseif npc.SinHealth then
		GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT - 1
	end

end
hook.Add("OnNPCKilled","SinHunter_NPCDeathHook",SinHunter_NPCDeath)

function SINHUNTER_DEATH_RESET()

	if #player.GetAll() <= 1 then

		GLOB_NO_SAVEDATA = true
		GLOB_SAFE_GAMEEND = true
		
		GLOB_NO_FENIC = false
		GLOB_NO_SINS = true
		
		GLOB_SINHUNTER_SPAWN1 = false
		GLOB_SINHUNTER_SPAWN2 = false
		GLOB_SINHUNTER_SPAWNNPCS = 0
		GLOB_SINHUNTER_ENEMYCOUNT = 0
		GLOB_SINFUL_ENEMY_COUNT = 0
		
		SIN_SPAWN_SPEED = 5
		SIN_SPAWN_MAX = 30
		SIN_ENEMIES_KILLED = 0
		SIN_SINSPAWN_THRESHOLD = 15
		
		GLOB_SIN_DEVOURED_COUNT = 0
		LAST_SIN_COUNT = 0
		SIN_COUNT_GOAL = 4
		
		GLOB_SINHUNTER_STARTED = false
		GLOB_SINHUNTER_SPAWNNPCS = false
		SIN_GAME_ENDED = false
		
	end

end
hook.Add("PlayerDeath","SINHUNTER_DEATH_RESETHOOK",SINHUNTER_DEATH_RESET)

function MAP_LOADED_FUNC()

	GLOB_NO_SAVEDATA = true
	GLOB_SAFE_GAMEEND = true
	
	GLOB_NO_FENIC = false
	GLOB_NO_SINS = true
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = false
	GLOB_SINHUNTER_SPAWNNPCS = 0
	GLOB_SINHUNTER_ENEMYCOUNT = 0
	GLOB_SINFUL_ENEMY_COUNT = 0
	
	SIN_SPAWN_SPEED = 5
	SIN_SPAWN_MAX = 30
	SIN_ENEMIES_KILLED = 0
	SIN_SINSPAWN_THRESHOLD = 15
	
	LAST_SIN_COUNT = 0
	SIN_COUNT_GOAL = 4
	
	GLOB_SIN_DEVOURED_COUNT = 0
	GLOB_SINHUNTER_STARTED = false
	GLOB_SINHUNTER_SPAWNNPCS = false
	SIN_GAME_ENDED = false

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("info_player_deathmatch")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("trigger_multiple")
	RemoveAllEnts("game_text")
	RemoveAllEnts("ambient_generic")
	RemoveAllEnts("func_door")
	
	CreateProp(Vector(-102,-1913.25,248.594), Angle(0,180,0), "models/props_wasteland/controlroom_desk001b.mdl", true)
	
	CreatePlayerSpawn( Vector(-275.005,-1762.37,271.719), Angle(0,-40,0) )
	CreateClothesLocker( Vector(-137.003,-1828.922,256.031), Angle(0,224.892,0) )

	CreateTeleporter( "START_1", Vector(2.336,-2067.98,260.06), "START_2", Vector(-963.351,-1221.561,243.674) )
	
	--[[ The trigger to start the mode ]]
	CreateTrigger( Vector(-896.05,-1223.454,208.502), Vector(-116.194,-116.194,0), Vector(116.194,116.194,250.004), START_SIN_HUNTER )
	
	Create_Armour100( Vector(-111.809, -1877.627, 265.456), Angle(0, 179.955, 0) )
	Create_Backpack( Vector(-112.151, -1910.351, 265.456), Angle(0, 175.984, 0) )
	Create_Backpack( Vector(-114.11, -1947.286, 265.456), Angle(0, 181.627, 0) )
	Create_FalanrathThorn( Vector(-128.727, -1910.855, 268.488), Angle(0, 178.283, 0) )
	
	Create_PistolWeapon( Vector(-618.855, -1099.319, 192.031), Angle(0, 188.205, 0) )
	Create_SuperShotgunWeapon( Vector(193.373, -832.129, 256.031), Angle(0, 180.053, 0) )
	Create_GrenadeAmmo( Vector(136.516, -341.007, 256.031), Angle(0, 353.733, 0) )
	Create_GrenadeAmmo( Vector(148.025, -303.472, 256.031), Angle(0, 322.8, 0) )
	Create_GrenadeAmmo( Vector(193.954, -279.028, 256.031), Angle(0, 281.209, 0) )
	Create_GrenadeAmmo( Vector(185.916, -314.675, 256.031), Angle(0, 302.632, 0) )
	Create_GrenadeAmmo( Vector(175.219, -357.847, 256.031), Angle(0, 15.469, 0) )
	Create_GrenadeAmmo( Vector(95.561, -364.913, 256.031), Angle(0, 8.153, 0) )
	Create_RifleWeapon( Vector(564.404, -308.764, 256.031), Angle(0, 182.668, 0) )
	Create_RevolverWeapon( Vector(993.648, 680.67, 128.031), Angle(0, 183.713, 0) )
	Create_CrossbowWeapon( Vector(-112.493, 851.979, 128.031), Angle(0, 89.561, 0) )
	Create_SMGWeapon( Vector(-1377.538, 1320.829, 0.031), Angle(0, 268.776, 0) )
	Create_KingSlayerWeapon( Vector(-598.53, 448.975, 0.031), Angle(0, 180.894, 0) )
	Create_ShotgunWeapon( Vector(-1061.656, -144.664, 0.031), Angle(0, 179.849, 0) )
	Create_FoolsOathWeapon( Vector(-891.096, 989.812, 138.031), Angle(0, 46.62, 0) )
	Create_RaikiriWeapon( Vector(-999.985, 298.572, 406.435), Angle(0, 183.793, 0) )
	
	Create_Beer( Vector(190.331, 402.502, 128.031), Angle(0, 353.329, 0) )
	Create_Beer( Vector(243.769, 400.041, 128.031), Angle(0, 351.03, 0) )
	Create_Beer( Vector(296.768, 398.239, 128.031), Angle(0, 342.043, 0) )
	Create_Bread( Vector(838.389, -229.976, 128.031), Angle(0, 102.845, 0) )
	Create_Bread( Vector(703.779, -225.537, 128.031), Angle(0, 68.151, 0) )
	Create_ManaCrystal100( Vector(746.662, -273.856, 264.031), Angle(0, 131.687, 0) )
	Create_Armour( Vector(192.074, 1022.182, 128.031), Angle(0, 314.771, 0) )
	Create_Armour( Vector(186.507, 869.038, 128.031), Angle(0, 26.667, 0) )
	Create_Armour100( Vector(502.975, -996.157, 192.031), Angle(0, 223.539, 0) )
	Create_EldritchArmour10( Vector(-3.83, -742.533, 120.031), Angle(0, 44.845, 0) )
	Create_EldritchArmour10( Vector(135.498, -742.06, 120.031), Angle(0, 118.831, 0) )
	Create_EldritchArmour10( Vector(66.493, -747.87, 120.031), Angle(0, 95.005, 0) )
	Create_VolatileBattery( Vector(-1377.756, 1088.501, 48.031), Angle(0, 183.117, 0) )
	
	local HEALING = {
	Create_Medkit25( Vector(-1511.11, 448.46, 0.031), Angle(0, 1.684, 0) ),
	Create_Medkit25( Vector(-1251.888, -144.154, 0.031), Angle(0, 1.266, 0) ),
	Create_Medkit25( Vector(-1608.388, 1088.426, 0.031), Angle(0, 359.283, 0) ),
	Create_Medkit10( Vector(-187.774, 1107.819, 128.031), Angle(0, 91.87, 0) ),
	Create_Medkit10( Vector(-56.574, 1115.887, 128.031), Angle(0, 90.825, 0) ),
	Create_Medkit25( Vector(935.957, 586.014, 128.031), Angle(0, 160.631, 0) ),
	Create_Medkit10( Vector(204.593, 225.404, 128.031), Angle(0, 89.363, 0) ),
	Create_Medkit10( Vector(270.74, 223.467, 128.031), Angle(0, 104.829, 0) ),
	Create_Medkit10( Vector(-295.244, 401.984, 128.031), Angle(0, 280.281, 0) ),
	Create_Medkit10( Vector(-209.697, 402.342, 128.031), Angle(0, 255.201, 0) ),
	Create_Medkit25( Vector(-600.386, -92.671, 128.031), Angle(0, 11.196, 0) ),
	Create_ManaCrystal( Vector(-573.083, 211.34, 0.031), Angle(0, 177.978, 0) ),
	Create_ManaCrystal( Vector(-1120.004, -203.117, 136.031), Angle(0, 90.407, 0) ),
	Create_ManaCrystal( Vector(-1120.614, -81.567, 136.031), Angle(0, 91.034, 0) ),
	Create_Medkit25( Vector(-986.11, -1024.625, 192.031), Angle(0, 353.221, 0) ),
	Create_ManaCrystal( Vector(102.891, -1749.763, 192.031), Angle(0, 110.888, 0) ),
	Create_ManaCrystal( Vector(31.426, -1755.566, 192.031), Angle(0, 76.612, 0) ),
	Create_Medkit25( Vector(972.982, -632.509, 128.031), Angle(0, 129.281, 0) ),
	Create_ManaCrystal( Vector(482.252, 988.374, 128.031), Angle(0, 184.253, 0) )}

	local AMMO_ENTS = {
	Create_PistolAmmo( Vector(-635.83, -1073.997, 192.031), Angle(0, 209.523, 0) ),
	Create_PistolAmmo( Vector(-646.011, -1103.658, 192.15), Angle(0, 188.414, 0) ),
	Create_PistolAmmo( Vector(-620.442, -1127.894, 192.031), Angle(0, 193.848, 0) ),
	Create_ShotgunAmmo( Vector(166.469, -796.44, 256.031), Angle(0, 208.477, 0) ),
	Create_ShotgunAmmo( Vector(166.17, -866.138, 256.031), Angle(0, 152.674, 0) ),
	Create_RifleAmmo( Vector(524.076, -276.608, 256.031), Angle(0, 213.391, 0) ),
	Create_RifleAmmo( Vector(523.519, -333.832, 256.031), Angle(0, 159.469, 0) ),
	Create_RevolverAmmo( Vector(967.06, 708.925, 128.031), Angle(0, 208.375, 0) ),
	Create_RevolverAmmo( Vector(969.941, 671.422, 128.031), Angle(0, 177.025, 0) ),
	Create_RevolverAmmo( Vector(978.437, 638.247, 128.031), Angle(0, 177.861, 0) ),
	Create_CrossbowAmmo( Vector(-112.388, 943.58, 128.031), Angle(0, 88.934, 0) ),
	Create_CrossbowAmmo( Vector(-110.206, 718.159, 128.031), Angle(0, 90.606, 0) ),
	Create_SMGAmmo( Vector(-1426.654, 1306.056, 0.031), Angle(0, 305.978, 0) ),
	Create_SMGAmmo( Vector(-1374.61, 1287.949, 0.031), Angle(0, 264.387, 0) ),
	Create_SMGAmmo( Vector(-1319.96, 1313.427, 0.031), Angle(0, 258.013, 0) ),
	Create_KingSlayerAmmo( Vector(-638.652, 488.187, 0.031), Angle(0, 209.109, 0) ),
	Create_KingSlayerAmmo( Vector(-633.931, 404.695, 0.031), Angle(0, 151.53, 0) ),
	Create_ShotgunAmmo( Vector(-1089.337, -101.753, 0.031), Angle(0, 215.588, 0) ),
	Create_ShotgunAmmo( Vector(-1087.351, -184.641, 0.031), Angle(0, 147.036, 0) )}
	
	local AMMO_S_ENTS = {
	Create_FoolsOathAmmo( Vector(-936.435, 1033.826, 136.031), Angle(0, 222.36, 0) )}
	
	for _,v in pairs (HEALING) do
		SetAsRespawningItem(v,30)
	end
	
	for _,v in pairs (AMMO_ENTS) do
		SetAsRespawningItem(v,60)
	end
	
	for _,v in pairs (AMMO_S_ENTS) do
		SetAsRespawningItem(v,120)
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

SINHUNTER_SPAWNS_FRONT = {
Vector(-735.26,-415.042,128.031),
Vector(-594.299,-457.28,128.031),
Vector(-580.52,-616.642,128.031),
Vector(-647.567,-827.797,192.031),
Vector(-720.737,-826.993,192.031),
Vector(-838.037,-816.952,192.031),
Vector(-926.32,-850.19,192.031),
Vector(-952.202,-932.146,192.031),
Vector(-870.53,-940.527,192.031),
Vector(-756.851,-919.139,192.031),
Vector(-664.269,-940.331,192.031),
Vector(-684.243,-1006.187,192.031),
Vector(-710.835,-1081.062,192.031),
Vector(-683.653,-1164.532,192.031),
Vector(-678.011,-1236.763,192.031),
Vector(-715.231,-1270.685,192.031),
Vector(-801.919,-1228.56,192.031),
Vector(-877.035,-1142.236,193.158),
Vector(-905.392,-1316.961,237.208),
Vector(-846.79,-1345.115,227.753),
Vector(-741.994,-1384.461,219.717),
Vector(-504.526,-1228.773,192.031),
Vector(-431.705,-1185.1,192.031),
Vector(-363.076,-1106.678,192.031),
Vector(-320.518,-1010.204,192.031),
Vector(-244.757,-952.995,192.031),
Vector(-154.473,-943.097,192.031),
Vector(-63.058,-966.242,192.031),
Vector(40.134,-978.152,192.031),
Vector(129.364,-981.188,192.031),
Vector(239.935,-989.251,192.031),
Vector(330.09,-1024.553,192.031),
Vector(406.543,-1091.206,192.031),
Vector(449.761,-1191.842,192.031),
Vector(447.129,-1291.433,192.031),
Vector(397.659,-1388.867,192.031),
Vector(332.523,-1460.782,192.031),
Vector(259.503,-1541.123,192.031),
Vector(184.325,-1618.144,192.031),
Vector(97.316,-1657.172,192.031),
Vector(-15.732,-1596.28,192.031),
Vector(-32.657,-1499.613,192.031),
Vector(-60.75,-1401.589,192.031),
Vector(-157.623,-1363.461,192.031),
Vector(-241.244,-1423.517,192.031),
Vector(-338.517,-1455.524,192.031),
Vector(-257.716,-1247.086,192.031),
Vector(-151.623,-1178.366,192.031),
Vector(-207.188,-823.759,256.031),
Vector(-76.233,-823.402,256.031),
Vector(140.246,-838.706,256.031),
Vector(205.883,-1272.966,256.727),
Vector(260.825,-1162.499,238.144),
Vector(345.209,-893.476,192.031),
Vector(278.735,-814.562,192.031),
Vector(286.603,-592.202,128.031),
Vector(190.144,-472.071,128.031),
Vector(103.556,-522.555,128.031),
Vector(42.518,-618.471,128.031),
Vector(56.124,-700.144,120.031),
Vector(206.448,-480.055,128.031),
Vector(306.589,-464.753,128.031),
Vector(376.709,-490.558,128.031),
Vector(443.972,-574.603,128.031),
Vector(497.662,-607.466,128.031),
Vector(536.201,-539.892,128.031),
Vector(588.823,-466.374,128.031),
Vector(675.511,-444.464,128.031),
Vector(721.738,-515.562,128.031),
Vector(767.033,-597.834,132.653),
Vector(855.024,-616.575,128.031),
Vector(920.172,-545.73,128.031),
Vector(971.873,-478.137,128.031),
Vector(1076.983,-516.399,120.031),
Vector(1073.931,-643.912,144.418),
Vector(589.016,-309.626,256.031),
Vector(382.022,-325.389,256.031),
Vector(154.448,-347.863,256.031),
Vector(-108.075,-546.608,256.031),
Vector(-252.766,-551.629,256.031),
Vector(-203.852,-446.391,256.031),
Vector(-125.706,-447.136,256.031),
Vector(-932.687,-715.422,0.031),
Vector(-1008.456,-683.432,0.031),
Vector(-1047.114,-591.072,0.031),
Vector(-1097.551,-476.741,0.031),
Vector(-1233.29,-485.1,17.439),
Vector(-1087.313,-534.947,0.031),
Vector(-918.443,-427.728,0.031),
Vector(-892.394,-499.968,0.031),
Vector(-1372.112,-483.567,128.031),
Vector(-1311.518,-549.573,128.031),
Vector(-1154.696,-626.606,128.031),
Vector(-989.394,-482.375,128.031)}

SINHUNTER_SPAWNS_BACK = {
Vector(733.901,-159.427,128.031),
Vector(851.561,-163.622,128.031),
Vector(840.514,-69.495,128.031),
Vector(760.955,-47.541,128.031),
Vector(719.057,-32.278,128.031),
Vector(713.318,63.601,128.031),
Vector(764.66,98.568,128.031),
Vector(831.324,117.785,128.031),
Vector(842.248,180.695,128.031),
Vector(782.002,247.338,128.031),
Vector(678.712,259.013,128.031),
Vector(639.805,269.077,128.031),
Vector(660.885,359.863,128.031),
Vector(711.818,382.741,128.031),
Vector(808.909,367.695,128.031),
Vector(789.206,449.119,128.031),
Vector(765.585,525.742,128.031),
Vector(781.078,595.444,128.031),
Vector(882.858,617.425,128.031),
Vector(957.651,680.211,128.031),
Vector(895.989,762.913,162.114),
Vector(776.724,726.012,138.273),
Vector(688.56,659.762,128.031),
Vector(589.221,625.83,128.031),
Vector(491.632,612.774,128.031),
Vector(395.095,645.957,128.031),
Vector(350.038,736.782,128.031),
Vector(421.087,826.332,128.031),
Vector(471.329,902.596,128.031),
Vector(438.238,992.49,128.031),
Vector(351.831,987.326,128.031),
Vector(215.514,923.222,128.031),
Vector(168.187,989.383,128.031),
Vector(226.654,1025.267,128.031),
Vector(318.04,1076.299,128.031),
Vector(334.958,1163.212,128.031),
Vector(251.849,1217.456,128.031),
Vector(150.563,1220.061,128.031),
Vector(57.923,1214.127,128.031),
Vector(-31.117,1206.291,128.031),
Vector(-110.39,1205.608,128.031),
Vector(-204.777,1211.403,128.031),
Vector(-299.035,1218.63,128.031),
Vector(-390.108,1220.673,128.031),
Vector(-484.61,1221.362,128.031),
Vector(-174.382,1030.247,128.031),
Vector(-53.324,1023.351,128.031),
Vector(-112.338,846.943,128.031),
Vector(81.499,657.633,0.031),
Vector(72.189,762.004,0.031),
Vector(65.611,865.095,0.031),
Vector(61.646,966.6,0.031),
Vector(45.325,1050.469,0.031),
Vector(-102.552,948.227,0.031),
Vector(-185.023,964.873,0.031),
Vector(-273.045,1025.643,0.031),
Vector(-377.69,1059.923,0.031),
Vector(-450.492,1012.072,0.031),
Vector(-415.131,913.36,0.031),
Vector(-331.086,858.817,0.031),
Vector(-253.436,796.57,0.031),
Vector(-205.357,697.817,0.031),
Vector(-257.881,670.513,0.031),
Vector(-335.04,710.187,0.031),
Vector(-27.791,549.437,0.031),
Vector(5.425,503.784,0.031),
Vector(66.767,442.131,0.031),
Vector(49.368,353.252,0.031),
Vector(-35.597,287.957,0.031),
Vector(-46.303,209.133,0.031),
Vector(4.319,137.947,2.044),
Vector(54.425,-95.671,128.031),
Vector(-3.133,-133.663,128.031),
Vector(-114.109,-104.714,128.031),
Vector(-231.136,-54.957,128.031),
Vector(-276.603,-103.748,128.031),
Vector(-23.763,-228.07,256.031),
Vector(81.465,30.687,128.031),
Vector(-48.712,474.865,0.031),
Vector(76.681,469.58,0.031),
Vector(-35.115,650.597,0.031),
Vector(-469.53,975.4,0.031),
Vector(-444.086,851.673,0.031),
Vector(-552.37,986.132,0.031),
Vector(-618.311,1044.876,0.031),
Vector(-624.162,973.645,0.031),
Vector(-670.252,897.974,0.031),
Vector(-739.31,953.364,0.031),
Vector(-808.594,1023.92,0.031),
Vector(-925.782,1008.498,0.031),
Vector(-922.664,918.922,0.031),
Vector(-841.605,876.011,0.031),
Vector(-819.51,810.563,0.031),
Vector(-877.432,781.302,0.031),
Vector(-960.939,800.026,0.031),
Vector(-901.068,719.341,0.031),
Vector(-865.343,658.952,0.031),
Vector(-800.54,580.433,0.031),
Vector(-732.894,525.715,0.031),
Vector(-652.207,465.677,0.031),
Vector(-601.038,379.181,0.031),
Vector(-595.039,270.458,0.031),
Vector(-654.583,202.044,0.031),
Vector(-755.247,209.249,0.031),
Vector(-833.914,264.224,0.031),
Vector(-895.925,331.429,0.031),
Vector(-963.456,388.301,0.031),
Vector(-1042.763,431.45,0.031),
Vector(-1142.969,428.733,0.031),
Vector(-1194.641,339.622,0.031),
Vector(-1186.329,253.299,0.031),
Vector(-1169.342,151.743,0.031),
Vector(-1124.323,64.911,0.031),
Vector(-1073.809,66.571,0.031),
Vector(-1010.277,136.477,0.031),
Vector(-912.007,178.621,0.031),
Vector(-828.648,161.549,0.031),
Vector(-842.85,88.644,0.031),
Vector(-873.632,-6.067,0.031),
Vector(-822.784,-41.6,0.031),
Vector(-688.685,174.565,0.031),
Vector(-596.813,228.233,0.031),
Vector(-579.717,329.864,0.031),
Vector(-461.052,-72.572,128.031),
Vector(-445.548,10.026,128.031),
Vector(-441.062,122.221,128.031),
Vector(-670.566,98.976,128.031),
Vector(-578.479,-90.356,128.031),
Vector(-992.288,4.945,128.031),
Vector(-1236.368,-140.216,0.031),
Vector(-1105.087,-134.703,0.031),
Vector(-1229.841,-292.976,0.031),
Vector(-1148.315,-297.122,0.031),
Vector(-905.112,-308.66,0.031),
Vector(-1357.436,440.374,0.031),
Vector(-1457.978,473.927,0.031),
Vector(-1488.016,598.794,0.031),
Vector(-1472.989,745.566,0.031),
Vector(-1561.727,965.979,0.031),
Vector(-1583.127,1093.169,0.031),
Vector(-1542.749,1232.952,7.563),
Vector(-1427.774,1289.926,0.031),
Vector(-1271.908,1281.648,0.031),
Vector(-1175.967,1203.666,0.031),
Vector(-1160.362,1055.691,0.031),
Vector(-1243.515,899.207,0.031),
Vector(-1456.019,996.614,8.031),
Vector(-1447.648,1130.688,7.658),
Vector(-1299.249,1168.226,8.031),
Vector(-1279.759,1021.215,8.031),
Vector(-485.117,1206.629,128.031),
Vector(-385.154,1205.312,128.031),
Vector(-273.42,1177.547,128.031),
Vector(-153.513,1192.251,128.031),
Vector(-39.319,1232.917,128.031),
Vector(99.979,1215.523,128.031),
Vector(582.399,373.298,128.031),
Vector(556.104,304.725,128.031),
Vector(503.724,242.665,128.031),
Vector(431.455,273.819,128.031),
Vector(395.185,345.104,128.031),
Vector(335.628,376.227,128.031),
Vector(293.505,311.524,128.031),
Vector(245.662,247.524,128.031),
Vector(194.716,275.44,128.031),
Vector(145.914,331.116,128.031),
Vector(60.218,322.421,128.031),
Vector(-22.98,290.863,128.031),
Vector(-109.584,304.356,128.031),
Vector(-187.098,325.626,128.031),
Vector(-243.011,303.991,128.031),
Vector(-263.782,225.176,128.031),
Vector(-303.813,246.354,128.031),
Vector(-278.743,345.156,128.031),
Vector(-285.58,415.761,128.031),
Vector(-347.415,316.869,128.031),
Vector(-409.215,266.126,128.031),
Vector(-433.474,168.361,128.031),
Vector(-431.23,82.887,128.031),
Vector(-482.367,-36.922,128.031),
Vector(-458.559,364.073,128.031)}