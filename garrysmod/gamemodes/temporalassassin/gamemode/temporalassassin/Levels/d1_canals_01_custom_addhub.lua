
--[[

This is commented out so it doesnt actually show up in-game, it's just an example
after all.

[UNDERSTANDING HOW IT WORKS]

There is a new final 7th argument for all the AddXMission functions
called File Append.

Simply put your custom name into it as a string to make the game load files
with that name after the initial map name.

A raw example of this would be something along the lines of this:

The maps you wish to work on are already made in-game, let's say you wanted to
make a custom HL2 campaign that isn't the base game's version.

[HOW TO MAKE IT HAPPEN]

First you'll need a custom name that all your files will need to take after.

Let's call this example "alternatehl2"

If this is the case, all of your lua files will be named after the maps like so:
d1_trainstation_01alternatehl2.lua
d1_trainstation_01alternatehl2_cl.lua

d1_trainstation_02alternatehl2.lua
d1_trainstation_02alternatehl2_cl.lua

etc. etc.

Due to this, lets change the example to "_alternatehl2"

This is much neater and the files should now be as follows:
d1_trainstation_01_alternatehl2.lua
d1_trainstation_01_alternatehl2_cl.lua

d1_trainstation_02_alternatehl2.lua
d1_trainstation_02_alternatehl2_cl.lua

After you've finished your custom name, place it on the 7th argument
like i did below with the example "_custom"

[LOADING IT VIA COMMANDS FOR DEVELOPMENT]

If you wish to load a specific custom variation of a map you are working on, you
can use the following command in the console:

TA_LoadCustomMap "YOUR_EXAMPLE"

for the example stated above it would be:

TA_LoadCustomMap "_alternatehl2"

for the example of this map in particular it is:

TA_LoadCustomMap "_custom"

If you want to stop loading your custom maps you can use the following command
to delete the special level load append:

TA_ClearCustomMap

[EXTRAS]

You will NEED the file to exist and be correctly labelled and everything for the
game to load it, if no custom named lua is found, the game will default to trying
to load the default maps lua.

When launching a campaign/mission with a file append name, the file append will be
active forever until the player selects a new mission/campaign from the HUB

AddCampaignMission( "HL2 Custom", true, "d1_canals_01", "Custom Test", "Fuck", {"Mthuulra",Color(50,50,50,255)}, "_custom" )

]]