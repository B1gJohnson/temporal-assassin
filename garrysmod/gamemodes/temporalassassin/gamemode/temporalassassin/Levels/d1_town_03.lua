
if CLIENT then return end

LEVEL_STRINGS["d1_town_03"] = {"d1_town_02",Vector(-3758.4172363281,-60.721694946289,-3455.96875), Vector(-40.669799804688,-40.669799804688,0), Vector(40.669799804688,40.669799804688,247.9375)}

function MAP_LOADED_FUNC()

	RemoveAllEnts("trigger_changelevel")
	
	file.Write("TemporalAssassin/Town_2_OffShoot.txt","Ya did it kiddo")
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Armour( Vector(-3118.9250488281, -800.18225097656, -3602.8139648438), Angle(0, 278.75250244141, 0) )
		Create_Beer( Vector(-3027.7055664063, -791.24377441406, -3603.2526855469), Angle(0, 243.99249267578, 0) )
		Create_Beer( Vector(-3025.7541503906, -822.42956542969, -3602.9274902344), Angle(0, 237.39245605469, 0) )
		Create_Beer( Vector(-3027.6027832031, -857.40686035156, -3603.2355957031), Angle(0, 227.93241882324, 0) )
		Create_Beer( Vector(-3024.0554199219, -892.18737792969, -3602.64453125), Angle(0, 210.33238220215, 0) )
		Create_KingSlayerAmmo( Vector(-3182.4929199219, -1626.6268310547, -3631.96875), Angle(0, 314.03216552734, 0) )
		Create_KingSlayerAmmo( Vector(-3197.8129882813, -1672.9252929688, -3631.96875), Angle(0, 343.51223754883, 0) )
		Create_KingSlayerAmmo( Vector(-3201.5290527344, -1733.36328125, -3631.96875), Angle(0, 22.012298583984, 0) )
		Create_KingSlayerWeapon( Vector(-2005.3029785156, -2012.3206787109, -3151.96875), Angle(0, 24.952011108398, 0) )
		Create_ManaCrystal100( Vector(-1951.9692382813, -2041.2071533203, -3151.96875), Angle(0, 57.072189331055, 0) )
		Create_Medkit25( Vector(-1189.2109375, -790.13397216797, -3655.96875), Angle(0, 286.03186035156, 0) )
		Create_Medkit25( Vector(-1117.0885009766, -792.97723388672, -3655.96875), Angle(0, 242.91174316406, 0) )
		Create_PortableMedkit( Vector(-944.02551269531, -974.01519775391, -3703.96875), Angle(0, 205.95179748535, 0) )
		Create_PortableMedkit( Vector(-945.03375244141, -1010.0428466797, -3703.96875), Angle(0, 179.55180358887, 0) )
		Create_ShotgunWeapon( Vector(-1383.9007568359, -1342.5394287109, -3455.96875), Angle(0, 298.48086547852, 0) )
		Create_ShotgunAmmo( Vector(-1355.1453857422, -1355.2116699219, -3455.96875), Angle(0, 279.78076171875, 0) )
		Create_ShotgunAmmo( Vector(-1392.6029052734, -1374.9771728516, -3455.96875), Angle(0, 312.78082275391, 0) )
		Create_ShotgunAmmo( Vector(-1363.9102783203, -1379.0202636719, -3455.96875), Angle(0, 293.20050048828, 0) )
		Create_Medkit25( Vector(-2976.0246582031, -1301.4580078125, -3479.96875), Angle(0, 358.98046875, 0) )
		Create_Armour( Vector(-2982.7905273438, -1378.6832275391, -3479.96875), Angle(0, 26.480560302734, 0) )
		Create_Beer( Vector(-3609.5549316406, -736.20318603516, -3359.96875), Angle(0, 226.04988098145, 0) )
		Create_Beer( Vector(-3605.3095703125, -764.015625, -3359.96875), Angle(0, 205.14984130859, 0) )
		Create_Beer( Vector(-3604.2055664063, -799.30688476563, -3359.96875), Angle(0, 171.48983764648, 0) )
		Create_Beer( Vector(-3622.4038085938, -756.20550537109, -3359.96875), Angle(0, 220.98986816406, 0) )
		Create_Beer( Vector(-3614.0837402344, -778.95202636719, -3359.96875), Angle(0, 193.70986938477, 0) )
		Create_Beer( Vector(-3636.3420410156, -770.79449462891, -3359.96875), Angle(0, 217.68989562988, 0) )
		Create_SpiritEye1( Vector(-3704.2194824219, -553.61645507813, -3359.96875), Angle(0, 262.78988647461, 0) )
		Create_ManaCrystal( Vector(-3916.4028320313, -530.72296142578, -3359.96875), Angle(0, 269.60989379883, 0) )
		Create_ManaCrystal( Vector(-3916.5754394531, -621.32287597656, -3359.96875), Angle(0, 269.38989257813, 0) )
		Create_ManaCrystal( Vector(-3916.0476074219, -709.7197265625, -3359.96875), Angle(0, 268.06988525391, 0) )
		Create_RifleAmmo( Vector(-3843.2836914063, -798.42333984375, -3359.96875), Angle(0, 62.809730529785, 0) )
		Create_RifleAmmo( Vector(-3801.4956054688, -804.57977294922, -3359.96875), Angle(0, 103.94982147217, 0) )
		Create_RifleWeapon( Vector(-3377.5903320313, -519.31896972656, -3119.96875), Angle(0, 90.169441223145, 0) )
		Create_RifleAmmo( Vector(-3350.0673828125, -471.25341796875, -3119.96875), Angle(0, 173.98947143555, 0) )
		Create_RifleAmmo( Vector(-3399.3911132813, -466.43130493164, -3119.96875), Angle(0, 173.54946899414, 0) )
		
		CreateSecretArea( Vector(-1977.1422119141,-2009.4024658203,-3151.96875), Vector(-65.609039306641,-65.609039306641,0), Vector(65.609039306641,65.609039306641,83.555419921875) )
		CreateSecretArea( Vector(-3378.3112792969,-492.56332397461,-3119.96875), Vector(-49.632705688477,-49.632705688477,0), Vector(49.632705688477,49.632705688477,96.009033203125) )

	else
	
		CreateProp(Vector(-2092.281,-346.375,-3655.562), Angle(0.044,-106.875,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
		local HINT_1 = CreateProp(Vector(-1102.281,-770.125,-3604.25), Angle(0,-90,45), "models/props_borealis/door_wheel001a.mdl", true)
		local HINT_2 = CreateProp(Vector(-1198.375,-770.219,-3603.5), Angle(0,-90,0), "models/props_borealis/door_wheel001a.mdl", true)
		
		Create_Medkit25( Vector(-3300.258, -1237.017, -3607.969), Angle(0, 80.771, 0) )
		Create_Medkit25( Vector(-3340.719, -1206.002, -3607.969), Angle(0, 44.405, 0) )
		Create_RifleWeapon( Vector(-3585.493, -835.245, -3583.969), Angle(0, 90.385, 0) )
		Create_RifleAmmo( Vector(-3553.268, -796.148, -3583.969), Angle(0, 113.584, 0) )
		Create_RifleAmmo( Vector(-3612.429, -795.444, -3583.969), Angle(0, 70.739, 0) )
		Create_Armour( Vector(-2714.874, -1199.163, -3639.969), Angle(0, 78.472, 0) )
		Create_Medkit10( Vector(-2651.435, -981.456, -3635.544), Angle(0, 242.328, 0) )
		Create_Medkit10( Vector(-2736.218, -987.827, -3636.606), Angle(0, 294.996, 0) )
		Create_KingSlayerWeapon( Vector(-3182.027, -1689.693, -3631.969), Angle(0, 342.124, 0) )
		Create_KingSlayerAmmo( Vector(-3173.281, -1630.069, -3631.969), Angle(0, 309.937, 0) )
		Create_CrossbowAmmo( Vector(-3418.802, -1911.725, -3631.969), Angle(0, 46.913, 0) )
		Create_Armour5( Vector(-1618.547, -1574.88, -3687.969), Angle(0, 102.716, 0) )
		Create_Armour5( Vector(-1644.628, -1583.093, -3687.969), Angle(0, 89.131, 0) )
		Create_Armour5( Vector(-1621.513, -1545.568, -3687.969), Angle(0, 105.224, 0) )
		Create_Armour5( Vector(-1645.954, -1546.419, -3687.969), Angle(0, 87.772, 0) )
		Create_Beer( Vector(-1637.045, -1515.476, -3687.969), Angle(0, 96.864, 0) )
		Create_ShotgunAmmo( Vector(-1454.679, -1896.994, -3711.969), Angle(0, 82.443, 0) )
		Create_SuperShotgunWeapon( Vector(-1449.748, -1854.274, -3711.969), Angle(0, 51.093, 0) )
		Create_GrenadeAmmo( Vector(-1359.942, -1376.254, -3455.969), Angle(0, 275.141, 0) )
		Create_GrenadeAmmo( Vector(-1343.884, -1362.177, -3455.969), Angle(0, 261.242, 0) )
		Create_ShotgunWeapon( Vector(-1389.428, -1381.737, -3455.969), Angle(0, 152.667, 0) )
		Create_ShotgunAmmo( Vector(-1371.054, -1397.178, -3455.969), Angle(0, 170.014, 0) )
		Create_EldritchArmour( Vector(-1678.953, -1992.969, -3543.969), Angle(0, 180.355, 0) )
		Create_ManaCrystal( Vector(-2080.874, -1751.85, -3503.969), Angle(0, 85.677, 0) )
		Create_ManaCrystal( Vector(-2015.779, -1754.039, -3503.969), Angle(0, 117.654, 0) )
		Create_Beer( Vector(-2002.787, -1551.857, -3503.969), Angle(0, 201.359, 0) )
		Create_PistolAmmo( Vector(-3905.817, -44.649, -3327.969), Angle(0, 276.387, 0) )
		Create_PistolAmmo( Vector(-3882.165, -27.014, -3327.969), Angle(0, 263.011, 0) )
		Create_RevolverAmmo( Vector(-3855.471, -39.852, -3327.969), Angle(0, 224.346, 0) )
		Create_SMGAmmo( Vector(-3880.614, -55.655, -3327.969), Angle(0, 230.407, 0) )
		
		local SEC_BUT_1 = function()
			
			Create_WhisperAmmo( Vector(-1129.887, -1153.029, -3703.969), Angle(0, 100.099, 0) )
			Create_CorzoHat( Vector(-1142.02, -1067.399, -3703.969), Angle(0, 103.861, 0) )
		
		end
		
		AddSecretButton( Vector(-1147.282,-768.031,-3602.183), SEC_BUT_1 )
		
		CreateSecretArea( Vector(-1680.432,-1994.099,-3539.437), Vector(-34.332,-34.332,0), Vector(34.332,34.332,80.378) )
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)