
if CLIENT then return end

function MAP_LOADED_FUNC()

	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_SpiritSphere( Vector(-215.56806945801, -973.4072265625, -1071.96875), Angle(0, 0.20010375976563, 0) )
		Create_PistolWeapon( Vector(-441.62249755859, -1119.2301025391, -766.93829345703), Angle(0, 90.180297851563, 0) )
		Create_RevolverAmmo( Vector(-132.83055114746, -795.85754394531, -863.96875), Angle(0, 334.32000732422, 0) )
		Create_Beer( Vector(-28.342765808105, 1097.9758300781, -767.96875), Angle(0, 3.1407775878906, 0) )
		Create_Beer( Vector(-15.968441009521, 1096.4669189453, -767.96875), Angle(0, 6.4407958984375, 0) )
		Create_Beer( Vector(-3.7669525146484, 1097.4958496094, -767.96875), Angle(0, 6.8807983398438, 0) )
		Create_PistolWeapon( Vector(-47.102916717529, 1567.7448730469, -766.03686523438), Angle(0, 275.28100585938, 0) )
		Create_EldritchArmour10( Vector(2605.6052246094, -2248.1184082031, -543.96875), Angle(0, 324.85931396484, 0) )
		Create_EldritchArmour10( Vector(2604.8312988281, -2306.1469726563, -543.96875), Angle(0, 223.79956054688, 0) )

		CreateSecretArea( Vector(-232.20623779297,-977.49353027344,-1071.96875), Vector(-73.201324462891,-73.201324462891,0), Vector(73.201324462891,73.201324462891,88.203430175781) )
		CreateSecretArea( Vector(2620.6669921875,-2276.40625,-543.96875), Vector(-52.256950378418,-52.256950378418,0), Vector(52.256950378418,52.256950378418,68.123657226563) )
		
	else
	
		CreateProp(Vector(499.656,1695.375,-872.75), Angle(-0.044,-119.927,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
	
		Create_EldritchArmour( Vector(3017.293, -2600.237, -735.969), Angle(0, 355.898, 0) )
		Create_Medkit25( Vector(891.968, -337.799, -819.037), Angle(0, 290.068, 0) )
		Create_Armour5( Vector(347.552, -1782.912, -831.969), Angle(0, 187.284, 0) )
		Create_Armour5( Vector(294.09, -1780.607, -831.969), Angle(0, 195.121, 0) )
		Create_Armour5( Vector(241.975, -1779.44, -831.969), Angle(0, 213.2, 0) )
		Create_Beer( Vector(265.986, -1780.557, -831.969), Angle(0, 201.287, 0) )
		Create_Beer( Vector(320.805, -1780.201, -831.969), Angle(0, 269.63, 0) )
		Create_SpiritSphere( Vector(-222.428, -973.445, -1071.969), Angle(0, 0.228, 0) )
		Create_FoolsOathAmmo( Vector(-1055.248, -1102.671, -799.969), Angle(0, 260.585, 0) )
		Create_Bread( Vector(-1047.169, -1151.093, -799.969), Angle(0, 35.729, 0) )
		Create_Bread( Vector(-1029.379, -1116.566, -799.969), Angle(0, 352.884, 0) )
		Create_RevolverAmmo( Vector(-131.754, -811.791, -863.969), Angle(0, 0.197, 0) )
		Create_PistolAmmo( Vector(-126.161, -521.124, -863.969), Angle(0, 7.196, 0) )
		Create_Beer( Vector(-25.215, 1094.651, -767.969), Angle(0, 4.897, 0) )
		Create_Beer( Vector(-2.473, 1095.075, -767.969), Angle(0, 6.569, 0) )
		Create_PistolWeapon( Vector(-45.677, 1565.693, -766.002), Angle(0, 271.683, 0) )
		Create_QuadDamage( Vector(3186.414, -1840.746, -639.969), Angle(0, 168.008, 0) )
		
		CreateSecretArea( Vector(3016.441,-2600.403,-731.437), Vector(-27.21,-27.21,0), Vector(27.21,27.21,14.097) )
		CreateSecretArea( Vector(-204.577,-974.48,-1071.969), Vector(-42.835,-42.835,0), Vector(42.835,42.835,112.084) )
		CreateSecretArea( Vector(-1041.578,-1122.093,-795.437), Vector(-40.702,-40.702,0), Vector(40.702,40.702,18.567) )
		CreateSecretArea( Vector(3178.714,-1837.781,-639.969), Vector(-35.098,-35.098,0), Vector(35.098,35.098,69.998) )
		
		CreateProp(Vector(-263,1923.969,-633.312), Angle(-0.044,6.196,0.747), "models/props_c17/lampshade001a.mdl", true)
		CreateProp(Vector(-261.969,1974.875,-633.312), Angle(0.308,-33.179,0.615), "models/props_c17/lampshade001a.mdl", true)
		
		local SECRET_BUTTON_PART1 = function()
		
			CreateProp(Vector(-111.594,-1142,-1071.625), Angle(0,90,0), "models/props_combine/breendesk.mdl", true)
			CreateProp(Vector(-112.281,-950.625,-945.219), Angle(89.736,-178.945,-88.945), "models/props_c17/lampshade001a.mdl", true)
			
			local SECRET_BUTTON_PART2 = function()
			
				Create_FoolsOathWeapon( Vector(-131.659, -1115.477, -1037.137), Angle(12.956, -175.909, -114.89) )
				Create_EldritchArmour10( Vector(-91.888, -1127.616, -1040.591), Angle(0, 107.405, 0) )
				Create_FoolsOathAmmo( Vector(-111.21, -1127.583, -1040.591), Angle(0, 4.238, 0) )
			
			end
			
			AddSecretButton( Vector(-111.999,-944.031,-973.497), SECRET_BUTTON_PART2, "YOU FOUND THE SUPER SECRET BUTTON", false, "TemporalAssassin/Secrets/SuperSecret_Find.wav" )
		
		end
		
		AddSecretButton( Vector(-271.969,1950.059,-622.894), SECRET_BUTTON_PART1 )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)