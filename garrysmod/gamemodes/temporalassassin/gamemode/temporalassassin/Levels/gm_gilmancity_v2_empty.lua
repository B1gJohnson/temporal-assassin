		
if CLIENT then return end

util.AddNetworkString("SIN_UpdateCount")
util.AddNetworkString("SIN_AskForGoal")
util.AddNetworkString("SIN_SinOnMap")

function SIN_AskForGoal_LIB( len, pl )

	net.Start("SIN_UpdateCount")
	net.WriteUInt(LAST_SIN_COUNT,6)
	net.WriteUInt(SIN_COUNT_GOAL,6)
	net.Broadcast()

end
net.Receive("SIN_AskForGoal",SIN_AskForGoal_LIB)

GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true

GLOB_NO_FENIC = false
GLOB_NO_SINS = true

GLOB_SINHUNTER_SPAWN1 = false
GLOB_SINHUNTER_SPAWN2 = false
GLOB_SINHUNTER_SPAWNNPCS = 0
GLOB_SINHUNTER_ENEMYCOUNT = 0
GLOB_SINFUL_ENEMY_COUNT = 0

SIN_SPAWN_SPEED = 5
SIN_SPAWN_MAX = 40
SIN_ENEMIES_KILLED = 0
SIN_SINSPAWN_THRESHOLD = 20

local SPAWN_DIFFICULTIES = {}
SPAWN_DIFFICULTIES[1] = {5,30,20}
SPAWN_DIFFICULTIES[2] = {4,40,25}
SPAWN_DIFFICULTIES[3] = {3,50,30}
SPAWN_DIFFICULTIES[4] = {3,50,30}
SPAWN_DIFFICULTIES[5] = {2.5,60,35}
SPAWN_DIFFICULTIES[6] = {2,60,35}
SPAWN_DIFFICULTIES[7] = {1.5,60,35}
SPAWN_DIFFICULTIES[8] = {1.5,70,40}
SPAWN_DIFFICULTIES[9] = {1,80,45}
SPAWN_DIFFICULTIES[10] = {1,100,50}
SPAWN_DIFFICULTIES[11] = {1,120,50}
SPAWN_DIFFICULTIES[12] = {1,120,50} --[[ Just incase more difficulties ARE added ]]
SPAWN_DIFFICULTIES[13] = {1,120,50}
SPAWN_DIFFICULTIES[14] = {1,120,50}

GLOB_SINTYPE = 2
--[[
This defines what enemies will spawn for this sin hunter mission.

1 = Combine
2 = Zombies
3 = Antlions
4 = HL:S Humans
5 = HL:S Aliens

You can also define it as GLOB_SINTYPE = math.random(1,5) to make it random everytime its played
]]

local SIN_CLASSES = {}

SIN_CLASSES[1] = {
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s",
"npc_metropolice",
"npc_combine_s"}

SIN_CLASSES[2] = {
"npc_zombie",
"npc_zombie",
"npc_zombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_fastzombie",
"npc_poisonzombie"}

SIN_CLASSES[3] = {
"npc_antlion"}

if IsMounted("ep2") then
	SIN_CLASSES[3] = {
	"npc_antlion",
	"npc_antlion",
	"npc_antlion",
	"npc_antlion_worker"}
end

SIN_CLASSES[4] = SIN_CLASSES[1]
SIN_CLASSES[5] = SIN_CLASSES[2]

if IsMounted("hl1") then

	SIN_CLASSES[4] = {
	"monster_human_grunt"}
	
	SIN_CLASSES[5] = {
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullsquid",
	"monster_houndeye",
	"monster_alien_controller",
	"monster_alien_grunt",
	"monster_bullchicken",
	"monster_houndeye"}
	
end

local BIGSIN_CLASSES = {}

BIGSIN_CLASSES[1] = {
"npc_combine_s"}

if IsMounted("ep2") then
	BIGSIN_CLASSES[1] = {
	"npc_hunter"}
end

BIGSIN_CLASSES[2] = {
"npc_poisonzombie"}

if IsMounted("ep2") or IsMounted("episodic") then
	BIGSIN_CLASSES[2] = {
	"npc_zombine"}
end

BIGSIN_CLASSES[3] = {
"npc_antlionguard"}
BIGSIN_CLASSES[4] = BIGSIN_CLASSES[1]
BIGSIN_CLASSES[5] = BIGSIN_CLASSES[2]

if IsMounted("hl1") then
	BIGSIN_CLASSES[4] = {
	"monster_human_assassin"}
	
	BIGSIN_CLASSES[5] = {
	"monster_alien_slave"}
end

local SIN_WEAPONS = {}
SIN_WEAPONS["npc_metropolice"] = {
"weapon_pistol",
"weapon_smg1",
"weapon_stunstick"}
SIN_WEAPONS["npc_combine_s"] = {
"weapon_smg1",
"weapon_ar2",
"weapon_shotgun"}

function SetAsRespawningItem( ent, del )

	if not ent then return end
	if not ent.ITEM_USE then return end
	
	local POS,ANG,MDL,USE = ent:GetPos(), ent:GetAngles(), ent:GetModel(), ent.ITEM_USE
	
	if not del then del = 30 end
	
	ent.OnRemove = function()
	
		timer.Simple(del,function()
		
			local N_ITEM = CreateItemPickup( POS, ANG, MDL, USE )
			CreatePointParticle( "Doll_EvilSpawn_Activate", POS, POS, Angle(0,0,0), Angle(0,0,0), 1 )
			sound.Play( Sound("TemporalAssassin/Doll/ExtraSpawn_Item.wav"), POS, 70, math.random(99,101), 0.7 )
			SetAsRespawningItem(N_ITEM,del)
			
		end)
	
	end

end

LAST_SIN_COUNT = 0
SIN_COUNT_GOAL = 5

function CanWeSpawnHere(pos)

	local TR = {}
	TR.start = pos
	TR.endpos = pos
	TR.mask = MASK_NPCSOLID
	TR.filter = {}
	table.Add(TR.filter,GLOB_TRIGGER_ENTS)
	TR.mins = Vector(-16,-16,0)
	TR.maxs = Vector(16,16,72)
	local Trace = util.TraceHull(TR)
	
	if Trace.Hit then return false end
	
	return true

end

function EnemySpawns_Think()

	local CT = CurTime()
	
	if SIN_ENEMIES_KILLED and SIN_ENEMIES_KILLED > SIN_SINSPAWN_THRESHOLD then
		SIN_ENEMIES_KILLED = 0
		Create_SinfulBoi()
	end
	
	net.Start("SIN_SinOnMap")
	if GLOB_SINFUL_ENEMY_COUNT and GLOB_SINFUL_ENEMY_COUNT > 0 then
		net.WriteBool(true)
	else
		net.WriteBool(false)
	end
	net.Broadcast()
	
	if GLOB_SIN_DEVOURED_COUNT and GLOB_SIN_DEVOURED_COUNT > LAST_SIN_COUNT then
	
		LAST_SIN_COUNT = GLOB_SIN_DEVOURED_COUNT
		
		local SIN_LEFT = (SIN_COUNT_GOAL - LAST_SIN_COUNT)
		
		net.Start("SIN_UpdateCount")
		net.WriteUInt(LAST_SIN_COUNT,6)
		net.WriteUInt(SIN_COUNT_GOAL,6)
		net.Broadcast()
		
		if SIN_LEFT <= 0 and not SIN_GAME_ENDED then
			SIN_GAME_ENDED = true
			END_SIN_HUNTER()			
		end
		
	end
	
	if GLOB_SINHUNTER_SPAWNNPCS and CT >= GLOB_SINHUNTER_SPAWNNPCS and GLOB_SINHUNTER_STARTED then
	
		GLOB_SINHUNTER_SPAWNNPCS = CT + SIN_SPAWN_SPEED
	
		local SPAWN_POS
	
		if GLOB_SINHUNTER_SPAWN2 then
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_FRONT) + Vector(0,0,5)
		else
			SPAWN_POS = table.Random(SINHUNTER_SPAWNS_BACK) + Vector(0,0,5)
		end
		
		if SPAWN_POS and GLOB_SINHUNTER_ENEMYCOUNT < SIN_SPAWN_MAX and GLOB_SINTYPE and SIN_CLASSES[GLOB_SINTYPE] then
		
			local CLASS = table.Random(SIN_CLASSES[GLOB_SINTYPE])
			local WEP = false
			
			if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
				WEP = table.Random(SIN_WEAPONS[CLASS])
			end
			
			if CanWeSpawnHere(SPAWN_POS) then
				CreateSnapNPC( CLASS, SPAWN_POS, Angle(0,math.random(-360,360),0), WEP, false )
				GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT + 1
			end
		
		end
	
	end

end
timer.Create("EnemySpawns_ThinkTimer", 0.5, 0, EnemySpawns_Think)

function Set_EnemySpawns_1()

	print("Back of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = true
	GLOB_SINHUNTER_SPAWN2 = false

end

function Set_EnemySpawns_2()

	print("Front of map spawns activated")
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = true

end

function START_SIN_HUNTER()

	RemoveAllTeleporters()
	
	local TSC1, TSC2 = CreateTeleporter( "SEC_1", Vector(-1104.069,-1088.201,-127.969), "SEC_2", Vector(-1113.332,681.301,-183.969) )
	
	local RemTeleSecret1 = function()
	
		local RemTeleSecret2 = function()
			if TSC1 and TSC2 then
				TSC1:Remove()
				TSC2:Remove()
			end
		end
		
		CreateTrigger( Vector(-1113.271,677.817,-183.969), Vector(-44.589,-44.589,0), Vector(44.589,44.589,93.706), RemTeleSecret2 )
		
	end
	
	CreateTrigger( Vector(-3920.743,516.934,-143.651), Vector(-103.949,-103.949,0), Vector(103.949,103.949,95.298), RemTeleSecret1 )
	
	--[[ This is the trigger that enables Back side map spawns ]]
	CreateTrigger( Vector(-5039.857,887.739,-127.96), Vector(-3741.324,-3741.324,0), Vector(3741.324,3741.324,1509.156), Set_EnemySpawns_1, true )
	
	--[[ This is the trigger that enables Front side map spawns ]]
	CreateTrigger( Vector(2257.658,585.058,-127.822), Vector(-3539.492,-3539.492,0), Vector(3539.492,3539.492,2277.042), Set_EnemySpawns_2, true )
	
	GLOB_SINHUNTER_STARTED = true
	GLOB_SINHUNTER_SPAWNNPCS = CurTime() + 20
	
	local DIFF = GetDifficulty2()

	if SPAWN_DIFFICULTIES and SPAWN_DIFFICULTIES[DIFF] then
		SIN_SPAWN_SPEED = SPAWN_DIFFICULTIES[DIFF][1]
		SIN_SPAWN_MAX = SPAWN_DIFFICULTIES[DIFF][2]
		SIN_SINSPAWN_THRESHOLD = SPAWN_DIFFICULTIES[DIFF][3]
	end
	
end

function END_SIN_HUNTER()

	GLOB_SINHUNTER_STARTED = false

	for _,v in pairs (ents.GetAll()) do
	
		if v and IsValid(v) then
		
			if v:IsNPC() then
				v:Remove() --[[ Lets remove all the npcs ]]
			end
		
		end
	
	end
	
	GLOB_SINFUL_ENEMY_COUNT = 0
	net.Start("SIN_SinOnMap")
	net.WriteBool(false)
	net.Broadcast()
	
	CreateTeleporter( "END_1", Vector(-1419.381,-424.534,-123.969), "END_2", Vector(-1525.132,-831.241,-127.969) )
	
	local GAME_END_TEXT = {
	""}

	MakeGameEnd( GAME_END_TEXT, 3, Vector(-1526.509,-831.415,-127.969), Vector(-39.512,-39.512,0), Vector(39.512,39.512,123.404) )
	
end

function Create_SinfulBoi()

	local SPAWNS_BACK = {
	Vector(47.807,2331.998,-121.49)}
	
	local SPAWNS_FRONT = {
	Vector(-1704.995,131.705,-119.971)}
	
	local SPAWN_POINT
	
	if GLOB_SINHUNTER_SPAWN2 then
		SPAWN_POINT = table.Random(SPAWNS_FRONT)
	elseif GLOB_SINHUNTER_SPAWN1 then
		SPAWN_POINT = table.Random(SPAWNS_BACK)
	end
	
	if not SPAWN_POINT then return end
	if not GLOB_SINTYPE then return end
	if not BIGSIN_CLASSES then return end
	if not BIGSIN_CLASSES[GLOB_SINTYPE] then return end
	
	local CLASS = table.Random(BIGSIN_CLASSES[GLOB_SINTYPE])
	local WEP = false
			
	if SIN_WEAPONS and SIN_WEAPONS[CLASS] then
		WEP = table.Random(SIN_WEAPONS[CLASS])
	end
	
	GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT + 1
	
	local SIN_BOI = CreateSnapNPC( CLASS, SPAWN_POINT, Angle(0,math.random(-360,360),0), WEP, false )
			
	timer.Simple(0.1,function()
		if SIN_BOI and IsValid(SIN_BOI) then
			SIN_BOI:SetHealth( SIN_BOI:Health()*2 )
			SIN_BOI:CreateSinner(true)
			SIN_BOI:SetModelScale(1.2,0) --[[ Lets make sin bois a bit bigger in scale ]]
			if GLOB_SINTYPE == 1 then
				SIN_BOI:MakeCombineShielder()
			end
		end
	end)

end

function SinHunter_NPCDeath( npc, att, inf )

	if not npc.SinHealth then
		SIN_ENEMIES_KILLED = SIN_ENEMIES_KILLED + 1
		GLOB_SINHUNTER_ENEMYCOUNT = GLOB_SINHUNTER_ENEMYCOUNT - 1
	elseif npc.SinHealth then
		GLOB_SINFUL_ENEMY_COUNT = GLOB_SINFUL_ENEMY_COUNT - 1
	end

end
hook.Add("OnNPCKilled","SinHunter_NPCDeathHook",SinHunter_NPCDeath)

function SINHUNTER_DEATH_RESET()

	if #player.GetAll() <= 1 then

		GLOB_NO_SAVEDATA = true
		GLOB_SAFE_GAMEEND = true
		
		GLOB_NO_FENIC = false
		GLOB_NO_SINS = true
		
		GLOB_SINHUNTER_SPAWN1 = false
		GLOB_SINHUNTER_SPAWN2 = false
		GLOB_SINHUNTER_SPAWNNPCS = 0
		GLOB_SINHUNTER_ENEMYCOUNT = 0
		GLOB_SINFUL_ENEMY_COUNT = 0
		
		SIN_SPAWN_SPEED = 5
		SIN_SPAWN_MAX = 40
		SIN_ENEMIES_KILLED = 0
		SIN_SINSPAWN_THRESHOLD = 20
		
		GLOB_SIN_DEVOURED_COUNT = 0
		LAST_SIN_COUNT = 0
		SIN_COUNT_GOAL = 5
		
		GLOB_SINHUNTER_STARTED = false
		GLOB_SINHUNTER_SPAWNNPCS = false
		SIN_GAME_ENDED = false
		
	end

end
hook.Add("PlayerDeath","SINHUNTER_DEATH_RESETHOOK",SINHUNTER_DEATH_RESET)

function MAP_LOADED_FUNC()

	GLOB_NO_SAVEDATA = true
	GLOB_SAFE_GAMEEND = true
	
	GLOB_NO_FENIC = false
	GLOB_NO_SINS = true
	
	GLOB_SINHUNTER_SPAWN1 = false
	GLOB_SINHUNTER_SPAWN2 = false
	GLOB_SINHUNTER_SPAWNNPCS = 0
	GLOB_SINHUNTER_ENEMYCOUNT = 0
	GLOB_SINFUL_ENEMY_COUNT = 0
	
	SIN_SPAWN_SPEED = 5
	SIN_SPAWN_MAX = 40
	SIN_ENEMIES_KILLED = 0
	SIN_SINSPAWN_THRESHOLD = 20
	
	GLOB_SIN_DEVOURED_COUNT = 0
	LAST_SIN_COUNT = 0
	SIN_COUNT_GOAL = 5
	
	GLOB_SINHUNTER_STARTED = false
	GLOB_SINHUNTER_SPAWNNPCS = false
	SIN_GAME_ENDED = false

	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("ambient_generic")
	RemoveAllEnts("info_ladder_dismount")
	
	CreatePlayerSpawn( Vector(-1514.905,-799.602,-127.969), Angle(0,90,0) )
	CreateClothesLocker( Vector(-1578.45,-777.517,-127.969), Angle(0,1.835,0) )
	
	CreateTeleporter( "START_1", Vector(-1420.642,-425.265,-123.969), "START_2", Vector(-1422.236,-548.657,-127.969) )
	
	CreateProp(Vector(-1437.406,-500.406,-66.219), Angle(0,90,0), "models/props_wasteland/interior_fence002d.mdl", true)
	CreateProp(Vector(-719.031,-1090.031,-127.594), Angle(0.044,179.297,0), "models/temporalassassin/doll/unknown.mdl", true)
	
	CreateTrigger( Vector(-1418.551,-415.264,-123.969), Vector(-70.591,-70.591,0), Vector(70.591,70.591,126.476), START_SIN_HUNTER )
	
	Create_PistolWeapon( Vector(-1506.431, -137.004, -125.621), Angle(0, 274.68, 0) )
	Create_SuperShotgunWeapon( Vector(-1629.974, 1500.828, -127.969), Angle(0, 2.251, 0) )
	Create_SMGWeapon( Vector(-788.413, 2373.328, -127.969), Angle(0, 278.753, 0) )
	Create_ShotgunWeapon( Vector(1112.767, 518.941, -123.969), Angle(0, 188.883, 0) )
	Create_KingSlayerWeapon( Vector(1721.585, -463.121, -127.969), Angle(0, 343.125, 0) )
	Create_EldritchArmour( Vector(-1086.861, -82.627, 0.031), Angle(0, 265.485, 0) )
	Create_Armour100( Vector(-1485.597, -666.215, -127.969), Angle(0, 247.511, 0) )
	Create_Backpack( Vector(-1486.82, -709.065, -127.969), Angle(0, 235.18, 0) )
	Create_Backpack( Vector(-1481.552, -747.286, -127.969), Angle(0, 179.586, 0) )
	Create_RevolverWeapon( Vector(-286.237, 1416.483, -127.969), Angle(0, 71.223, 0) )
	Create_RifleWeapon( Vector(941.691, -460.141, 0.031), Angle(0, 81.989, 0) )
	Create_Armour( Vector(381.017, -98.731, 0.031), Angle(0, 11.766, 0) )
	Create_Armour( Vector(383.254, -29.982, 0.031), Angle(0, 351.075, 0) )
	Create_CrossbowWeapon( Vector(15.242, 1.494, -127.969), Angle(0, 2.152, 0) )
	Create_PortableMedkit( Vector(-1554.085, -728.082, -127.969), Angle(0, 73.213, 0) )
	Create_PortableMedkit( Vector(-1553.181, -698.303, -127.969), Angle(0, 66.211, 0) )
	Create_PortableMedkit( Vector(-1556.909, -668.51, -127.969), Angle(0, 43.952, 0) )
	Create_FrontlinerWeapon( Vector(-2461.875, 1214.931, 40.031), Angle(0, 78.647, 0) )
	Create_HarbingerWeapon( Vector(1294.072, -881.611, -127.969), Angle(0, 159.005, 0) )
	Create_VolatileBattery( Vector(806.535, -375.737, 2.031), Angle(0, 244.97, 0) )
	Create_MegaSphere( Vector(303.801, -187.023, 0.031), Angle(0, 297.638, 0) )
	Create_Armour5( Vector(-197.321, -833.532, -127.969), Angle(0, 108.808, 0) )
	Create_Armour5( Vector(-265.55, -831.377, -127.969), Angle(0, 91.357, 0) )
	Create_Armour5( Vector(-343.483, -830.902, -127.969), Angle(0, 68.262, 0) )
	Create_Armour5( Vector(-309.958, -798.958, -127.969), Angle(0, 81.22, 0) )
	Create_Armour5( Vector(-232.105, -803.521, -127.969), Angle(0, 89.789, 0) )
	Create_Armour5( Vector(-233.031, -863.997, -127.969), Angle(0, 89.58, 0) )
	Create_Armour5( Vector(-308.272, -862.76, -127.969), Angle(0, 101.075, 0) )
	Create_Beer( Vector(-352.693, 558.831, -126.012), Angle(0, 269.947, 0) )
	Create_Beer( Vector(-352.432, 510.813, -125.996), Angle(0, 269.738, 0) )
	Create_Beer( Vector(-352.644, 464.454, -126.009), Angle(0, 269.738, 0) )
	Create_Beer( Vector(-352.873, 414.347, -126.023), Angle(0, 269.738, 0) )
	Create_Beer( Vector(-353.105, 363.495, -126.038), Angle(0, 269.738, 0) )
	Create_ShadeCloak( Vector(274.999, 68.11, -239.969), Angle(0, 36.913, 0) )
	Create_GrenadeAmmo( Vector(450.492, 51.413, -239.969), Angle(0, 113.615, 0) )
	Create_GrenadeAmmo( Vector(416.977, 45.741, -239.969), Angle(0, 89.789, 0) )
	Create_GrenadeAmmo( Vector(381.636, 56.032, -239.969), Angle(0, 63.456, 0) )
	Create_GrenadeAmmo( Vector(407.404, 86.513, -239.969), Angle(0, 71.189, 0) )
	Create_GrenadeAmmo( Vector(454.212, 88.741, -239.969), Angle(0, 110.689, 0) )
	Create_GrenadeAmmo( Vector(433.557, 71.607, -239.969), Angle(0, 92.506, 0) )
	Create_GrenadeAmmo( Vector(452.997, 124.542, -239.969), Angle(0, 106.509, 0) )
	Create_GrenadeAmmo( Vector(376.839, 104.511, -239.969), Angle(0, 64.501, 0) )
	Create_GrenadeAmmo( Vector(411.636, 126.547, -239.969), Angle(0, 79.34, 0) )
	Create_EldritchArmour10( Vector(207.729, 238.596, -239.969), Angle(0, 5.981, 0) )
	Create_EldritchArmour10( Vector(214.629, 51.486, -239.969), Angle(0, 44.228, 0) )
	Create_Bread( Vector(-849.044, 827.571, -183.969), Angle(0, 214.037, 0) )
	Create_Bread( Vector(-844.755, 760.458, -183.969), Angle(0, 161.16, 0) )
	Create_FalanranWeapon( Vector(1355.15, 343.409, -128), Angle(0, 269.32, 0) )
	Create_Armour100( Vector(488.074, 2094.248, -127.969), Angle(0, 193.973, 0) )
	Create_SpiritSphere( Vector(-1311.882, 2173.954, -127.969), Angle(0, 269.315, 0) )
	Create_Armour( Vector(-1588.017, 2018.82, -127.969), Angle(0, 6.71, 0) )
	Create_Armour5( Vector(-1552.22, 2003.227, -127.969), Angle(0, 16.115, 0) )
	Create_Armour5( Vector(-1545.701, 2033.408, -127.969), Angle(0, 1.693, 0) )
	Create_Armour5( Vector(-1579.218, 2050.149, -128), Angle(0, 354.587, 0) )
	Create_WhisperWeapon( Vector(-3822.579, 1167.519, -127.969), Angle(0, 293.768, 0) )
	Create_WhisperAmmo( Vector(-3746.087, 1170.334, -127.969), Angle(0, 272.868, 0) )
	Create_WhisperAmmo( Vector(-3840.919, 1092.945, -127.969), Angle(0, 341.003, 0) )
	Create_FalanrathThorn( Vector(-4015.985, 472.985, 42.212), Angle(0, 51.747, 0) )
	Create_ManaCrystal100( Vector(-3754.592, 735.385, -127.969), Angle(0, 145.598, 0) )
	Create_ManaCrystal100( Vector(-4345.491, 730.151, -127.969), Angle(0, 13.928, 0) )
	Create_EldritchArmour10( Vector(777.961, 715.722, -127.969), Angle(0, 281.968, 0) )
	Create_EldritchArmour10( Vector(830.502, 718.389, -127.969), Angle(0, 273.817, 0) )
	Create_Beer( Vector(397.699, 1636.512, -126.825), Angle(0, 181.439, 0) )
	Create_Beer( Vector(396.385, 1682.922, -126.743), Angle(0, 181.439, 0) )
	Create_Beer( Vector(397.775, 1744.525, -126.83), Angle(0, 180.812, 0) )
	Create_Beer( Vector(401.474, 1513.599, -127.061), Angle(0, 180.812, 0) )
	Create_Beer( Vector(402.488, 1450.322, -127.124), Angle(0, 180.812, 0) )
	Create_Beer( Vector(399.84, 1382.246, -126.959), Angle(0, 186.455, 0) )
	Create_PortableMedkit( Vector(939.482, -195.752, -127.969), Angle(0, 205.47, 0) )
	Create_PortableMedkit( Vector(1025.385, -208.982, 0.031), Angle(0, 91.147, 0) )
	Create_RaikiriWeapon( Vector(-1086.928, 1524.397, -127.969), Angle(0, 89.842, 0) )
	
	local HEALING = {
	Create_Medkit25( Vector(-1237.931, -489.554, -127.969), Angle(0, 89.688, 0) ),
	Create_Medkit10( Vector(-1182.703, -486.128, -127.969), Angle(0, 105.154, 0) ),
	Create_Medkit25( Vector(-894.514, 1354.107, -176.937), Angle(0, 261.068, 0) ),
	Create_Medkit25( Vector(402.886, 1579.346, -127.149), Angle(0, 181.23, 0) ),
	Create_Medkit25( Vector(-208.952, 2103.928, -127.456), Angle(0, 301.196, 0) ),
	Create_Medkit25( Vector(-2150.225, 682.341, -127.969), Angle(0, 104.007, 0) ),
	Create_Medkit25( Vector(-2223.737, 678.521, -127.969), Angle(0, 73.493, 0) ),
	Create_Medkit25( Vector(-4461.101, 1110.373, -127.969), Angle(0, 318.543, 0) ),
	Create_Medkit25( Vector(1097.285, 710.358, -127.969), Angle(0, 222.403, 0) ),
	Create_Medkit25( Vector(704.326, 330.383, -112.398), Angle(0, 88.016, 0) ),
	Create_ManaCrystal( Vector(966.614, 139.915, -7.969), Angle(0, 357.937, 0) ),
	Create_ManaCrystal( Vector(287.386, 793.723, -123.969), Angle(0, 272.874, 0) ),
	Create_ManaCrystal( Vector(-404.496, -635.426, -127.969), Angle(0, 21.555, 0) ),
	Create_ManaCrystal( Vector(-407.567, -586.196, -127.969), Angle(0, 358.564, 0) ),
	Create_ManaCrystal( Vector(2031.716, -640.055, -123.969), Angle(0, 176.423, 0) ),
	Create_ManaCrystal( Vector(-734.808, 792.23, -135.969), Angle(0, 179.558, 0) ),
	Create_ManaCrystal( Vector(-2238.596, 1603.783, -127.969), Angle(0, 212.264, 0) ),
	Create_ManaCrystal( Vector(-2304.221, 1615.758, -127.969), Angle(0, 259.289, 0) ),
	Create_ManaCrystal( Vector(352.553, 170.805, -127.969), Angle(0, 208.084, 0) ),
	Create_ManaCrystal( Vector(903.69, -483.822, -127.969), Angle(0, 83.311, 0) )}
	
	local AMMO_ENTS = {
	Create_SMGAmmo( Vector(-827.419, 2353.347, -127.969), Angle(0, 312.894, 0) ),
	Create_SMGAmmo( Vector(-775.77, 2339.274, -127.969), Angle(0, 19.67, 0) ),
	Create_SMGAmmo( Vector(-739.304, 2378.882, -127.969), Angle(0, 248.523, 0) ),
	Create_RevolverAmmo( Vector(-248.378, 1436.984, -127.969), Angle(0, 109.431, 0) ),
	Create_RevolverAmmo( Vector(-267.717, 1448.05, -127.969), Angle(0, 96.891, 0) ),
	Create_RevolverAmmo( Vector(-295.707, 1454.64, -127.969), Angle(0, 71.602, 0) ),
	Create_PistolAmmo( Vector(-1526.059, -155.242, -126.847), Angle(0, 288.436, 0) ),
	Create_PistolAmmo( Vector(-1508.33, -165.793, -123.816), Angle(0, 278.404, 0) ),
	Create_PistolAmmo( Vector(-1484.929, -157.495, -124.277), Angle(0, 261.893, 0) ),
	Create_CrossbowAmmo( Vector(15.715, 86.851, -127.969), Angle(0, 359.706, 0) ),
	Create_CrossbowAmmo( Vector(14.357, -79.978, -127.969), Angle(0, 11.201, 0) ),
	Create_RifleAmmo( Vector(956.305, -429.812, 0.031), Angle(0, 98.354, 0) ),
	Create_RifleAmmo( Vector(923.336, -427.284, 0.031), Angle(0, 75.051, 0) ),
	Create_ShotgunAmmo( Vector(1095.241, 568.315, -124.988), Angle(0, 223.336, 0) ),
	Create_ShotgunAmmo( Vector(1122.547, 463.061, -123.969), Angle(0, 147.887, 0) ),
	Create_ShotgunAmmo( Vector(-1600.726, 1465.455, -127.969), Angle(0, 14.234, 0) ),
	Create_ShotgunAmmo( Vector(-1601.853, 1532.449, -127.969), Angle(0, 343.719, 0) ),
	Create_ShotgunAmmo( Vector(-1596.107, 1499.868, -127.969), Angle(0, 2.948, 0) ),
	Create_KingSlayerAmmo( Vector(1767.979, -445.011, -127.969), Angle(0, 340.591, 0) ),
	Create_KingSlayerAmmo( Vector(1801.526, -470.054, -127.969), Angle(0, 351.25, 0) )}
	
	local AMMO_S_ENTS = {
	Create_FrontlinerAmmo( Vector(-2443.768, 1258.169, 40.031), Angle(0, 81.95, 0) ),
	Create_HarbingerAmmo( Vector(1532.778, -843.448, -127.969), Angle(0, 0.237, 0) )}
	
	for _,v in pairs (HEALING) do
		SetAsRespawningItem(v,30)
	end
	
	for _,v in pairs (AMMO_ENTS) do
		SetAsRespawningItem(v,40)
	end
	
	for _,v in pairs (AMMO_S_ENTS) do
		SetAsRespawningItem(v,120)
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

SINHUNTER_SPAWNS_FRONT = {
Vector(-1532.805,-429.271,-126.47),
Vector(-1438.27,-428.122,-123.969),
Vector(-1352.12,-361.921,-123.969),
Vector(-1443.896,-305.641,-123.969),
Vector(-1525.451,-291.895,-125.997),
Vector(-1479.818,-214.416,-123.969),
Vector(-1383.223,-162.541,-123.969),
Vector(-1377.213,-63.05,-123.969),
Vector(-1441.007,-25.528,-123.969),
Vector(-1496.451,57.953,-124.185),
Vector(-1468.078,105.152,-123.969),
Vector(-1360.151,131.716,-123.969),
Vector(-1350.591,252.107,-123.969),
Vector(-1429.302,327.044,-123.969),
Vector(-1505.451,385.426,-124.747),
Vector(-1517.901,463.746,-125.525),
Vector(-1463.768,517.414,-123.969),
Vector(-1398.061,560.474,-123.969),
Vector(-1358.692,640.582,-123.969),
Vector(-1383.293,707.89,-123.969),
Vector(-1473.282,734.264,-123.969),
Vector(-1529.221,787.38,-125.963),
Vector(-1503.255,893.623,-123.969),
Vector(-1434.677,981.731,-123.969),
Vector(-1421.162,1095.816,-123.969),
Vector(-1459.193,1174.173,-123.969),
Vector(-1474.09,1280.721,-123.969),
Vector(-1420.067,1371.375,-123.969),
Vector(-1390.645,1480.799,-123.969),
Vector(-1470.692,1519.137,-123.969),
Vector(-1554.447,1497.322,-127.809),
Vector(-1576.884,1540.207,-127.969),
Vector(-1495.474,1597.449,-124.124),
Vector(-1440.079,1690.071,-123.969),
Vector(-1410.138,1778.356,-123.969),
Vector(-1387.051,1869.61,-123.969),
Vector(-1409.186,1980.22,-123.969),
Vector(-1503.767,2033.447,-124.642),
Vector(-1581.996,2050.631,-127.969),
Vector(-1594.114,2158.838,-118.139),
Vector(-1591.395,2246.946,-122.801),
Vector(-1533.943,2193.324,-123.274),
Vector(-1459.537,2115.819,-125.34),
Vector(-1385.712,2087.6,-125.623),
Vector(-1574.562,940.083,-123.969),
Vector(-1602.271,838.389,-123.969),
Vector(-1660.316,753.549,-127.997),
Vector(-1750.074,744.361,-127.244),
Vector(-1817.685,809.691,-124.55),
Vector(-1828.806,907.012,-123.969),
Vector(-1860.427,995.412,-125.369),
Vector(-1949.716,1049.262,-127.969),
Vector(-1995.625,1126.538,-127.969),
Vector(-1989.94,1223.11,-127.969),
Vector(-1966.569,1337.856,-127.969),
Vector(-1979.306,1427.968,-127.969),
Vector(-2003.214,1527.143,-127.969),
Vector(-2086.342,969.837,-123.955),
Vector(-2135.535,883.36,-123.969),
Vector(-2151.446,781.847,-126.291),
Vector(-2174.621,693.313,-127.969),
Vector(-2235.939,777.277,-126.576),
Vector(-2289.216,842.438,-123.969),
Vector(-2277.112,922.023,-123.969),
Vector(-2183.06,1013.854,-126.522),
Vector(-2169.882,1102.942,-122.974),
Vector(-2269.988,1104,-127.969),
Vector(-2349.864,1046.542,-127.969),
Vector(-2387.492,942.498,-123.97),
Vector(-2444.465,862.626,-123.97),
Vector(-2532.333,818.242,-124.034),
Vector(-2606.438,749.741,-126.015),
Vector(-2686.354,697.226,-124.726),
Vector(-2785.421,690.347,-127.969),
Vector(-2834.989,763.299,-127.45),
Vector(-2789.518,848.106,-123.975),
Vector(-2742.401,928.892,-123.975),
Vector(-2753.829,1034.018,-127.782),
Vector(-2831.024,1092.042,-128.013),
Vector(-2928.47,1103.213,-121.351),
Vector(-3023.397,1149.271,-126.065),
Vector(-3063.133,1231.622,-127.285),
Vector(-2911.269,1277.077,-121.735),
Vector(-3156.315,1028.67,-127.448),
Vector(-3165.817,920.536,-123.969),
Vector(-3106.236,843.167,-123.969),
Vector(-3055.781,754.853,-127.965),
Vector(-3068.843,632.185,-125.592),
Vector(-3108.245,577.329,-127.969),
Vector(-3162.116,623.335,-127.969),
Vector(-3151.205,752.655,-127.969),
Vector(-3168.358,855.713,-123.969),
Vector(-3259.401,872.229,-123.969),
Vector(-3354.993,836.071,-123.969),
Vector(-3452.708,843.073,-123.969),
Vector(-3509.953,928.361,-123.969),
Vector(-3485.982,1020.008,-126.907),
Vector(-3514.686,1101.856,-127.969),
Vector(-3597.6,1086.038,-127.969),
Vector(-3639.87,986.085,-124.787),
Vector(-3694.623,885.648,-123.969),
Vector(-3768.985,824.422,-123.969),
Vector(-3878.724,783.009,-126.218),
Vector(-3977.18,788.951,-125.847),
Vector(-4059.542,891.091,-123.969),
Vector(-4062.882,977.569,-124.229),
Vector(-4097.127,1067.308,-127.969),
Vector(-4188.988,1111.273,-127.969),
Vector(-4286.864,1082.817,-127.969),
Vector(-4336.148,994.995,-125.343),
Vector(-4330.898,906.06,-123.975),
Vector(-4339.288,823.471,-123.975),
Vector(-4397.351,788.85,-125.856),
Vector(-4466.146,829.129,-123.969),
Vector(-4494.708,951.436,-123.969),
Vector(-3976.87,626.811,-122.904),
Vector(-3885.506,597.402,-119.49),
Vector(-3807.128,543.422,-117.969),
Vector(-3774.623,435.82,-119.621),
Vector(-3819.792,350.837,-123.899),
Vector(-3915.299,308.749,-120.018),
Vector(-4008.819,322.406,-118.836),
Vector(-4105.52,334.114,-119.133),
Vector(-4211.216,335.686,-117.905),
Vector(-4302.45,372.205,-122.896),
Vector(-4357.599,464.722,-123.517),
Vector(-4344.063,564.493,-120.754),
Vector(-4255.402,610.37,-127.973),
Vector(-4146.113,560.396,-127.969),
Vector(-4102.496,492.213,-130.987),
Vector(-4030.93,415.562,-124.339),
Vector(-3939.359,440.27,-127.592),
Vector(-3956.124,526.193,-123.096),
Vector(-2184.302,1487.102,-127.969),
Vector(-2281.281,1544.26,-127.969),
Vector(-2403.293,1534.903,-127.969),
Vector(-2474.131,1426.407,-127.969),
Vector(-2471.801,1241.341,-127.969),
Vector(-2354.113,1203.26,-127.969),
Vector(-2243.907,1281.366,-127.969),
Vector(-2216.268,1413.889,-127.969),
Vector(-2108.981,1207.048,-15.969),
Vector(-2371.79,1212.456,40.031),
Vector(-2291.847,1270.937,40.031),
Vector(-2274.079,1364.748,40.031),
Vector(-2417.012,1458.132,40.031),
Vector(-2513.233,1455.319,40.031),
Vector(-2302.796,1481.541,40.031),
Vector(-2173.266,1486.328,40.031),
Vector(-2100.089,1495.085,40.031),
Vector(-2496.66,1588.403,176.031),
Vector(-2490.511,1416.499,176.031),
Vector(-2488.393,1276.562,176.031),
Vector(-2327.83,1198.583,176.031),
Vector(-2163.981,1246.078,176.031),
Vector(-2087.964,1378.774,176.031),
Vector(-2107.363,1517.188,176.031),
Vector(-2246.425,1596.337,176.031),
Vector(-2350.121,1599.217,176.031),
Vector(-2326.278,1399.037,176.031),
Vector(-2269.983,1339.588,176.031)}

SINHUNTER_SPAWNS_BACK = {
Vector(1035.192,291.484,-123.264),
Vector(922.48,260.697,-122.969),
Vector(862.911,196.474,-120.766),
Vector(813.55,109.795,-123.056),
Vector(723.724,82.026,-117.472),
Vector(679.318,94.93,-121.187),
Vector(660.51,184.94,-121.591),
Vector(686.577,269.894,-123.401),
Vector(781.736,283.24,-127.115),
Vector(998.286,139.287,-7.969),
Vector(1100.43,139.292,-7.969),
Vector(1217.678,140.363,-7.969),
Vector(1302.941,139.696,-7.969),
Vector(1324.23,48.11,-7.969),
Vector(1317.927,-78.611,-7.969),
Vector(1319.069,-190.568,-63.969),
Vector(1320.791,-339.314,-127.969),
Vector(1319.54,-466.512,-127.969),
Vector(1333.989,-576.918,-123.969),
Vector(1450.425,-633.476,-123.969),
Vector(1592.304,-661.371,-123.969),
Vector(1756.898,-665.929,-123.969),
Vector(1889.394,-575.163,-123.969),
Vector(1846.248,-494.973,-127.969),
Vector(1730.449,-442.724,-127.969),
Vector(2056.701,-558.648,-124.74),
Vector(2076.847,-617.83,-123.969),
Vector(2038.252,-715.433,-123.969),
Vector(1960.342,-771.273,-126.861),
Vector(1932.645,-847.124,-127.969),
Vector(1822.686,-856.437,-127.969),
Vector(1722.362,-853.55,-127.969),
Vector(1636.346,-849.215,-127.969),
Vector(1438.38,-717.801,-123.969),
Vector(1336.697,-689.897,-123.969),
Vector(1265.407,-594.446,-123.969),
Vector(1158.278,-586.149,-123.969),
Vector(1108.318,-671.908,-123.969),
Vector(1115.742,-775.15,-127.103),
Vector(1213.532,-839.464,-127.969),
Vector(1292.802,-836.881,-127.969),
Vector(1317.587,-897.513,-127.969),
Vector(1266.275,-918.063,-127.969),
Vector(980.049,-742.63,-125.071),
Vector(886.008,-749.113,-125.476),
Vector(786.536,-773.43,-126.996),
Vector(677.63,-781.833,-127.521),
Vector(580.311,-794.601,-126.333),
Vector(473.763,-802.437,-126.863),
Vector(371.369,-819.029,-128.705),
Vector(255.703,-840.286,-119.476),
Vector(173.702,-843.594,-125.883),
Vector(130.049,-960.455,-127.969),
Vector(151.129,-743.865,-125.148),
Vector(217.096,-673.578,-123.969),
Vector(333.802,-635.322,-123.969),
Vector(408.275,-583.925,-123.969),
Vector(356.894,-492.984,-122.967),
Vector(255.664,-458.961,-128.903),
Vector(76.262,-577.664,-123.98),
Vector(52.037,-483.891,-127.969),
Vector(69.817,-402.074,-127.969),
Vector(101.63,-319.78,-127.969),
Vector(-4.331,-326.5,-127.969),
Vector(-154.359,-417.651,-125.509),
Vector(-231.061,-539.141,-123.969),
Vector(-227.852,-683.368,-123.969),
Vector(-258.266,-788.397,-127.913),
Vector(-307.914,-825.27,-127.969),
Vector(-352.24,-726.984,-125.171),
Vector(-348.327,-597.777,-124.926),
Vector(-340.584,-499.068,-124.442),
Vector(-334.781,-390.632,-124.08),
Vector(-328.974,-287.371,-123.981),
Vector(-324.886,-178.111,-123.981),
Vector(-322.928,-76.413,-123.981),
Vector(-319.791,32.172,-123.981),
Vector(-317.366,140.913,-123.981),
Vector(-319.887,266.492,-123.981),
Vector(-323.786,370.777,-123.981),
Vector(-326.858,485.466,-123.981),
Vector(-335.966,594.737,-125.827),
Vector(-390.971,661.884,-127.969),
Vector(-138.701,580.738,-124.951),
Vector(-56.125,538.719,-123.969),
Vector(-6.405,437.831,-123.969),
Vector(95.045,410.469,-124.01),
Vector(172.858,477.23,-123.969),
Vector(209.341,570.621,-124.099),
Vector(260.874,653.028,-123.969),
Vector(363.638,627.316,-123.969),
Vector(418.279,544.867,-123.969),
Vector(496.31,478.36,-123.969),
Vector(602.006,451.511,-123.969),
Vector(705.808,511.464,-123.969),
Vector(115.907,662.505,-127.969),
Vector(205.422,675.727,-124.317),
Vector(310.989,701.884,-123.969),
Vector(370.501,782.409,-124.313),
Vector(335.222,861.525,-123.969),
Vector(245.321,901.522,-123.969),
Vector(160.972,947.256,-127.095),
Vector(161.389,1046.873,-127.069),
Vector(264.658,1074.459,-123.969),
Vector(394.966,1055.895,-125.842),
Vector(434.902,1072.999,-127.969),
Vector(318.682,1146.299,-123.969),
Vector(232.802,1181.123,-123.969),
Vector(130.526,1195.686,-127.969),
Vector(103.602,1222.097,-127.969),
Vector(160.318,1289.084,-127.136),
Vector(261.928,1331.972,-123.969),
Vector(346.743,1380.538,-123.969),
Vector(341.487,1476.647,-123.969),
Vector(259.11,1532.88,-123.969),
Vector(170.667,1569.632,-126.489),
Vector(116.752,1643.768,-127.969),
Vector(175.821,1698.133,-126.167),
Vector(289.265,1695.308,-123.969),
Vector(387.218,1690.501,-125.357),
Vector(450.91,1689.374,-124.413),
Vector(322.986,1756.997,-123.969),
Vector(275.75,1838.177,-123.969),
Vector(317.731,1952.168,-123.969),
Vector(368.152,2026.571,-124.156),
Vector(443.863,2079.977,-127.969),
Vector(380.344,2104.346,-126.679),
Vector(294.457,2110.42,-127.05),
Vector(189.669,2118.842,-126.767),
Vector(83.936,2121.617,-127.752),
Vector(-14.038,2118.299,-122.969),
Vector(-105.661,2107.189,-126.848),
Vector(-206.255,2074.701,-124.817),
Vector(-279.533,2027.768,-123.969),
Vector(-349.129,1957.041,-123.969),
Vector(-420.04,1895.644,-124.678),
Vector(-523.733,1882.344,-125.51),
Vector(-606.492,1936.742,-123.969),
Vector(-674.48,2016.931,-123.969),
Vector(-763.562,2066.358,-124.296),
Vector(-864.703,2070.269,-124.54),
Vector(-944.341,2012.65,-123.98),
Vector(-993.363,1927.678,-123.98),
Vector(-1079.211,1871.474,-126.189),
Vector(-1170.513,1871.779,-126.17),
Vector(-1249.514,1937.999,-123.969),
Vector(-1265.81,2042.516,-123.969),
Vector(-1215.01,2133.499,-124.376),
Vector(-1107.793,2137.641,-126.845),
Vector(-1029.367,2092.898,-125.955),
Vector(-950.173,2039.253,-123.969),
Vector(-851.601,1999.388,-123.969),
Vector(-695.743,1985.697,-123.969),
Vector(-440.05,1989.107,-123.969),
Vector(-344.145,2017.466,-123.969),
Vector(-245.802,2048.414,-123.969),
Vector(-143.872,2003.596,-123.969),
Vector(-61.736,1914.505,-123.969),
Vector(42.005,1888.56,-125.135),
Vector(-489.722,21.734,-123.969),
Vector(-493.695,110.307,-123.969),
Vector(-525.292,198.199,-126.044),
Vector(-582.451,272.964,-127.969),
Vector(-661.277,283.806,-127.969),
Vector(-717.364,215.365,-127.117),
Vector(-722.779,113.479,-123.969),
Vector(-755.267,35.309,-123.969),
Vector(-854.185,-2.312,-124.801),
Vector(-925.457,57.761,-123.969),
Vector(-939.42,156.882,-123.969),
Vector(-972.049,245.938,-127.969),
Vector(-1071.762,281.871,-124.38),
Vector(-1162.129,225.807,-127.796),
Vector(-1193.7,135.519,-123.969),
Vector(-1210.391,42.873,-123.969),
Vector(-1255.489,33.082,-123.984),
Vector(-1274.158,108.493,-123.984),
Vector(-1258.946,216.553,-127.191),
Vector(-995.993,328.345,-127.969),
Vector(-920.956,403.282,-131.247),
Vector(-943.371,505.077,-184.666),
Vector(-967.913,595.724,-183.969),
Vector(-962.478,701.491,-183.969),
Vector(-952.807,801.57,-183.969),
Vector(-1001.685,885.068,-183.969),
Vector(-1093.9,889.772,-183.969),
Vector(-1197.934,880.118,-183.969),
Vector(-1238.994,896.631,-183.969),
Vector(-1197.633,990.175,-184.552),
Vector(-1143.861,1079.945,-178.136),
Vector(-1094.065,1158.708,-173.302),
Vector(-1029.546,1226.903,-180.619),
Vector(-957.364,1294.916,-176.148),
Vector(-983.217,1379.787,-170.731),
Vector(-1088.231,1411.016,-169.205),
Vector(-1188.563,1405.644,-165.105),
Vector(-917.19,1353.104,-175.865),
Vector(-840.795,1306.718,-178.012),
Vector(-745.113,1273.519,-183.474),
Vector(-650.206,1286.27,-183.718),
Vector(-560.341,1334.034,-182.646),
Vector(-502.532,1411.729,-175.261),
Vector(-520.879,1521.984,-181.702),
Vector(-472.198,1483.202,-180.475),
Vector(-434.144,1400.434,-172.551),
Vector(-411.741,1292.405,-183.439),
Vector(-357.802,1214.671,-183.969),
Vector(-255.041,1178.456,-183.969),
Vector(-145.134,1198.401,-183.969),
Vector(61.011,1183.733,-127.969),
Vector(119.824,1112.616,-127.969),
Vector(89.006,1214.879,-127.969),
Vector(-862.669,756.224,-183.969),
Vector(-809.859,664.46,-182.009),
Vector(-855.964,821.993,-183.969),
Vector(-826.724,912.919,-183.943),
Vector(-767.739,998.912,-180.092),
Vector(-682.796,1040.647,-180.022),
Vector(-580.445,1041.191,-172.766),
Vector(-477.542,1027.303,-174.597),
Vector(-372.279,1031.521,-182.156),
Vector(-268.341,1046.874,-178.969),
Vector(-273.122,1550.61,-127.969),
Vector(-227.354,1445.163,-127.969),
Vector(-363.756,1550.037,-127.969),
Vector(-232.824,2198.645,-127.265),
Vector(-228.669,2347.471,-116.85),
Vector(-690.258,2175.91,-127.969),
Vector(-759.389,2346.187,-127.969),
Vector(-868.954,2292.262,-127.969),
Vector(-606.785,2390.823,-127.969),
Vector(-562.729,2281.946,-127.969),
Vector(-558.625,2194.858,-127.969),
Vector(-1103.141,1764.57,-127.969),
Vector(-1079.198,1647.243,-127.969),
Vector(-1112.26,1561.683,-127.969),
Vector(-1068.318,1485.53,-127.969),
Vector(1142.79,36.275,0.031),
Vector(1107.171,-30.854,0.031),
Vector(1199.705,-198.783,0.031),
Vector(1029.371,-249.483,0.031),
Vector(956.577,-293.322,0.031),
Vector(951.162,-427.961,0.031),
Vector(912.081,-320.149,0.031),
Vector(967.004,-110.468,0.031),
Vector(984.459,50.686,-39.969),
Vector(906.192,36.05,-39.969),
Vector(897.593,-208.481,-127.969),
Vector(903.747,-346.197,-127.969),
Vector(907.134,-440.672,-127.969),
Vector(777.449,-406.708,-127.969),
Vector(631.066,-436.818,-127.969),
Vector(514.079,-439.843,-127.969),
Vector(490.156,-307.194,-127.969),
Vector(388.685,-325.074,-127.969),
Vector(253.832,-336.634,-127.969),
Vector(289.86,-237.439,-127.969),
Vector(330.674,-179.396,-127.969),
Vector(620.158,-265.963,-127.969),
Vector(729.772,-282.194,-127.969),
Vector(542.013,-241.606,-127.969),
Vector(497.175,-98.654,-127.969),
Vector(427.77,-67.025,-127.969),
Vector(299.399,-57.534,-127.969),
Vector(180.75,1.221,-127.969),
Vector(72.669,-19.922,-127.969),
Vector(78.765,-169.806,-127.969),
Vector(1.36,-202.467,-127.969),
Vector(56.176,-10.419,-127.969),
Vector(11.821,162.979,-127.969),
Vector(1.945,213.81,-127.969),
Vector(228.437,116.377,-127.969),
Vector(304.367,221.05,-127.969),
Vector(366.967,276.349,-127.969),
Vector(517.546,309.629,-87.969),
Vector(529.527,79.844,0.031),
Vector(522.123,-8.971,0.031),
Vector(447.7,-73.92,0.031),
Vector(520.969,-174.605,0.031),
Vector(585.972,-276.307,0.031),
Vector(712.015,-265.41,0.031),
Vector(439.512,-313.666,0.031),
Vector(357.958,-323.11,0.031),
Vector(260.265,-300.605,0.031),
Vector(253.922,-159.336,0.031),
Vector(338.331,-198.477,0.031),
Vector(459.151,245.74,-239.969),
Vector(443.046,150.562,-239.969),
Vector(383.009,68.595,-239.969),
Vector(275.263,46.431,-239.969),
Vector(285.897,123.218,-239.969),
Vector(269.638,204.936,-239.969),
Vector(234.46,276.977,-239.969),
Vector(313.683,311.713,-239.969),
Vector(384.166,304.162,-239.969),
Vector(-1211.828,-87.111,-127.969),
Vector(-1225.919,-202.202,-127.969),
Vector(-1200.244,-391.219,-127.969),
Vector(-1098.236,-377.05,-127.969),
Vector(-1086.978,-101.638,0.031),
Vector(-1088.782,-237.872,0.031)}