		
if CLIENT then return end

LEVEL_STRINGS["ep2_outland_10"] = {"ep2_outland_11",Vector(563.79449462891,-2523.2158203125,160.03125), Vector(-74.552032470703,-74.552032470703,0), Vector(74.552032470703,74.552032470703,109.67135620117)}

function EXTRA_FAIL_STATE( npc, att, inf )

	if IsValid(npc) and ( npc:GetClass() == "npc_alyx" ) then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRA_FAIL_STATEHook",EXTRA_FAIL_STATE)

function CHECK_UNKILL_DAMAGE( ent )

	if IsValid(ent) and ent:GetClass() == "npc_turret_floor" then
		ent.FAKE_HEALTH = 999999
		ent:ApplyPhysicalForce( VectorRand()*20000 )
	end

end

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("trigger_playermovement")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")

	Create_Armour( Vector(512.90563964844, -3280.22265625, 176.03125), Angle(0, 100.75702667236, 0) )
	Create_Backpack( Vector(465.72338867188, -3276.2800292969, 176.03125), Angle(0, 78.316902160645, 0) )
	Create_SMGAmmo( Vector(516.04754638672, -3227.423828125, 176.03125), Angle(0, 100.97702026367, 0) )
	Create_SMGAmmo( Vector(519.08532714844, -3177.5590820313, 176.03125), Angle(0, 113.07707977295, 0) )
	Create_SuperShotgunWeapon( Vector(466.46765136719, -3230.5688476563, 176.03125), Angle(0, 74.796928405762, 0) )
	Create_RevolverWeapon( Vector(475.00695800781, -3196.0827636719, 176.03125), Angle(0, 74.356925964355, 0) )
	Create_RevolverAmmo( Vector(459.65902709961, -3159.6088867188, 176.03125), Angle(0, 55.876846313477, 0) )
	Create_RevolverAmmo( Vector(489.88223266602, -3163.1596679688, 176.03125), Angle(0, 83.157073974609, 0) )
	Create_Armour100( Vector(3246.8579101563, -520.49829101563, -127.96875), Angle(0, 116.18831634521, 0) )
	Create_Medkit10( Vector(3254.1513671875, -296.96267700195, -127.96875), Angle(0, 224.42834472656, 0) )
	Create_Medkit10( Vector(3201.3625488281, -291.41915893555, -127.96875), Angle(0, 253.02841186523, 0) )
	Create_Medkit10( Vector(3154.3129882813, -290.2734375, -127.96875), Angle(0, 279.86849975586, 0) )
	Create_Medkit10( Vector(3071.1213378906, -302.00790405273, -127.96875), Angle(0, 319.02862548828, 0) )
	Create_PortableMedkit( Vector(3057.0639648438, -450.30975341797, -127.96875), Angle(0, 27.448623657227, 0) )
	Create_M6GWeapon( Vector(3064.4765625, -356.03482055664, -127.96875), Angle(0, 330.02838134766, 0) )
	Create_M6GAmmo( Vector(3059.0922851563, -417.13275146484, -127.96875), Angle(0, 8.7485046386719, 0) )
	Create_M6GAmmo( Vector(3070.4753417969, -383.09521484375, -127.96875), Angle(0, 344.98837280273, 0) )
	Create_M6GAmmo( Vector(3088.4025878906, -408.96865844727, -127.96875), Angle(0, 5.2284698486328, 0) )
	Create_SMGAmmo( Vector(2905.642578125, -953.93798828125, -127.96875), Angle(0, 348.94799804688, 0) )
	Create_SMGAmmo( Vector(2905.8273925781, -1012.10546875, -127.96875), Angle(0, 27.668197631836, 0) )
	Create_SMGAmmo( Vector(2887.876953125, -982.23889160156, -127.96875), Angle(0, 7.2080841064453, 0) )
	Create_SMGWeapon( Vector(2944.3327636719, -981.24822998047, -127.96875), Angle(0, 355.76806640625, 0) )
	Create_Armour100( Vector(2466.5505371094, -1354.2923583984, -255.96875), Angle(0, 89.70760345459, 0) )
	Create_PistolAmmo( Vector(2437.6967773438, -1361.7145996094, -255.96875), Angle(0, 74.527702331543, 0) )
	Create_PistolAmmo( Vector(2433.6606445313, -1325.3778076172, -255.96875), Angle(0, 64.407646179199, 0) )
	Create_PistolAmmo( Vector(2437.1926269531, -1286.2647705078, -255.96875), Angle(0, 45.707550048828, 0) )
	Create_RevolverAmmo( Vector(2500.1452636719, -1367.9542236328, -255.96875), Angle(0, 87.067504882813, 0) )
	Create_RevolverAmmo( Vector(2499.5336914063, -1323.2331542969, -255.96875), Angle(0, 84.647491455078, 0) )
	Create_RevolverAmmo( Vector(2499.81640625, -1288.1917724609, -255.96875), Angle(0, 79.587463378906, 0) )
	Create_RifleAmmo( Vector(2468.7214355469, -1293.1728515625, -255.96875), Angle(0, 80.907424926758, 0) )
	Create_RifleWeapon( Vector(2684.8115234375, -822.65893554688, -255.96875), Angle(0, 269.88729858398, 0) )
	Create_RifleAmmo( Vector(2648.615234375, -870.45172119141, -255.96875), Angle(0, 290.5673828125, 0) )
	Create_RifleAmmo( Vector(2719.4006347656, -871.04248046875, -255.96875), Angle(0, 249.8673248291, 0) )
	Create_Backpack( Vector(2671.8784179688, -1374.3004150391, -255.96875), Angle(0, 86.627365112305, 0) )
	Create_Beer( Vector(2749.0166015625, -1373.1328125, -127.96875), Angle(0, 129.88757324219, 0) )
	Create_Beer( Vector(2715.7546386719, -1373.2230224609, -127.96875), Angle(0, 112.72750854492, 0) )
	Create_Beer( Vector(2684.2724609375, -1376.4069824219, -127.96875), Angle(0, 91.387428283691, 0) )
	Create_Medkit25( Vector(2545.7727050781, -1364.7785644531, -103.96875), Angle(0, 83.687255859375, 0) )
	Create_ManaCrystal100( Vector(2591.6303710938, -1364.3453369141, -103.96875), Angle(0, 77.967170715332, 0) )
	Create_KingSlayerAmmo( Vector(2645.4240722656, -1365.6885986328, -103.96875), Angle(0, 92.927055358887, 0) )
	Create_KingSlayerAmmo( Vector(2617.6745605469, -1365.5301513672, -103.96875), Angle(0, 77.527061462402, 0) )
	Create_SpiritSphere( Vector(2602.6889648438, -984.51574707031, 0.031253814697266), Angle(0, 87.06665802002, 0) )
	Create_VolatileBattery( Vector(3068.4738769531, -805.56127929688, 0.03125), Angle(0, 194.20668029785, 0) )
	Create_ShotgunWeapon( Vector(3064.0302734375, -861.14477539063, 0.03125), Angle(0, 179.24667358398, 0) )
	Create_ShotgunAmmo( Vector(3061.5935058594, -975.04223632813, 0.03125), Angle(0, 133.48669433594, 0) )
	Create_ShotgunAmmo( Vector(3067.1555175781, -938.77209472656, 0.03125), Angle(0, 150.42674255371, 0) )
	Create_ShotgunAmmo( Vector(3055.6276855469, -908.29779052734, 0.03125), Angle(0, 166.04675292969, 0) )
	Create_WhisperAmmo( Vector(3070.1711425781, -1120.1229248047, 0.031246185302734), Angle(0, 121.60683441162, 0) )
	Create_WhisperAmmo( Vector(3038.3049316406, -1116.8406982422, 0.03125), Angle(0, 104.66676330566, 0) )
	Create_WhisperAmmo( Vector(3071.4592285156, -1087.1826171875, 0.031246185302734), Angle(0, 135.46682739258, 0) )
	
	CreateProp(Vector(668.375,-3304.40625,248.34375), Angle(3.8671875,93.9111328125,-1.0986328125), "models/temporalassassin/doll/unknown.mdl", true)
	
	local FUNC = function()
	
		for _,v in pairs (ents.GetAll()) do
		
			if IsValid(v) and v.GetName then
				
				if v:GetName() == "hunter_1_breakin_seq" then
					v:Fire("Use")
					v:Fire("FireUser1")
					v:Fire("FireUser2")
					v:Fire("FireUser3")
					v:Fire("FireUser4")
					v:Fire("BeginSequence")
				elseif v:GetName() == "hunter_2_breakin_seq" then
					v:Fire("Use")
					v:Fire("FireUser1")
					v:Fire("FireUser2")
					v:Fire("FireUser3")
					v:Fire("FireUser4")
					v:Fire("BeginSequence")
				end
				
			end
		
		end
	
	end
	
	CreateTrigger( Vector(2580.1135253906,-1109.4739990234,-255.96875), Vector(-195.30659484863,-195.30659484863,0), Vector(195.30659484863,195.30659484863,7.8987121582031), FUNC )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)