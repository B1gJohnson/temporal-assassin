
if CLIENT then return end

if not file.Exists("TemporalAssassin/Town_2_OffShoot.txt","DATA") then
	LEVEL_STRINGS["d1_town_02"] = {"d1_town_03",Vector(-3600.7302246094,-686.56616210938,-3583.96875), Vector(-95.290939331055,-95.290939331055,0), Vector(95.290939331055,95.290939331055,190.8271484375)}
else
	LEVEL_STRINGS["d1_town_02"] = {"d1_town_02a",Vector(-5278.7622070313,1762.4868164063,-3243.6625976563), Vector(-192.17520141602,-192.17520141602,0), Vector(192.17520141602,192.17520141602,238.34155273438)}
end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	if file.Exists("TemporalAssassin/Town_2_OffShoot.txt","DATA") then
	
		local START = Vector(-1786.9333496094,672.71380615234,-3014.96875)
		local END = Vector(-1789.552734375,582.85589599609,-3014.96875)
		local DOOR = GetObjectFromTrace( START, END, MASK_PLAYERSOLID )
		
		if IsValid(DOOR) then
			DOOR:Fire("Open")
		end
	
		local FUCK_NO = {CreateProp(Vector(-2826.281,555.813,-3034.156), Angle(89.995,-0.033,180), "models/hunter/plates/plate32x32.mdl", true),
		CreateProp(Vector(-2825.406,2074.219,-3034.156), Angle(89.995,-0.033,180), "models/hunter/plates/plate32x32.mdl", true),
		CreateProp(Vector(-2827.187,-962.562,-3034.156), Angle(89.995,-0.033,180), "models/hunter/plates/plate32x32.mdl", true)}
		
		for _,v in pairs (FUCK_NO) do
		
			if v and IsValid(v) then
				v:SetNoDraw(true)
			end
		
		end
		
		CreatePlayerSpawn( Vector(-3762.9208984375,-168.75202941895,-3383.96875), Angle(0,90,0) )
		
		if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
		
			Create_Medkit25( Vector(-3525.7416992188, 292.99252319336, -3455.96875), Angle(0, 232.77886962891, 0) )
			Create_Medkit25( Vector(-3497.671875, 243.89880371094, -3455.96875), Angle(0, 207.47885131836, 0) )
			Create_PortableMedkit( Vector(-3483.1064453125, 293.91110229492, -3455.96875), Angle(0, 220.89886474609, 0) )
			Create_Backpack( Vector(-3483.7399902344, 546.30084228516, -3327.96875), Angle(0, 225.59736633301, 0) )
			Create_Armour( Vector(-3689.9333496094, 547.94421386719, -3327.96875), Angle(0, 334.4973449707, 0) )
			Create_PistolAmmo( Vector(-3662.3610839844, 512.34313964844, -3327.96875), Angle(0, 341.75738525391, 0) )
			Create_PistolAmmo( Vector(-3589.8557128906, 541.61279296875, -3327.96875), Angle(0, 304.13720703125, 0) )
			Create_Medkit10( Vector(-3597.4887695313, 505.30743408203, -3327.96875), Angle(0, 324.59722900391, 0) )
			Create_RevolverAmmo( Vector(-3537.3247070313, 539.09893798828, -3327.96875), Angle(0, 280.81707763672, 0) )
			Create_RevolverAmmo( Vector(-3507.0441894531, 508.17364501953, -3327.96875), Angle(0, 262.99700927734, 0) )
			Create_WhisperAmmo( Vector(-3475.7702636719, 106.79838562012, -3327.96875), Angle(0, 151.15669250488, 0) )
			Create_WhisperAmmo( Vector(-4350.884765625, 1537.4786376953, -3007.96875), Angle(0, 273.64532470703, 0) )
			Create_WhisperWeapon( Vector(-3599.9965820313, 1907.1127929688, -3583.96875), Angle(0, 358.77642822266, 0) )
			Create_Medkit10( Vector(-4367.6215820313, 1500.0955810547, -3263.96875), Angle(0, 14.536224365234, 0) )
			Create_Medkit10( Vector(-4369.0483398438, 1538.2231445313, -3263.96875), Angle(0, 358.69613647461, 0) )
			Create_PistolWeapon( Vector(-4349.1665039063, 1626.0185546875, -3263.96875), Angle(0, 322.83605957031, 0) )
			Create_SuperShotgunWeapon( Vector(-4295.8276367188, 1626.6370849609, -3263.96875), Angle(0, 301.71600341797, 0) )
			Create_ShotgunAmmo( Vector(-4096.669921875, 1630.1119384766, -3135.96875), Angle(0, 229.33590698242, 0) )
			Create_ShotgunAmmo( Vector(-4060.9543457031, 1636.1196289063, -3135.96875), Angle(0, 220.09588623047, 0) )
			Create_ShotgunAmmo( Vector(-4071.548828125, 1602.2893066406, -3135.96875), Angle(0, 210.6358795166, 0) )
			Create_SpiritSphere( Vector(-4996.40234375, 613.48132324219, -3175.96875), Angle(0, 54.277938842773, 0) )
			
			CreateSecretArea( Vector(-3597.1997070313,1909.2010498047,-3580.3937988281), Vector(-61.700183868408,-61.700183868408,0), Vector(61.700183868408,61.700183868408,123.05834960938) )
			
		else
		
			Create_Medkit25( Vector(-3505.178, 124.939, -3327.969), Angle(0, 124.198, 0) )
			Create_Armour5( Vector(-3512.286, 375.005, -3327.969), Angle(0, 188.988, 0) )
			Create_Armour5( Vector(-3404.013, 391.764, -3327.969), Angle(0, 189.197, 0) )
			Create_Armour5( Vector(-3396.25, 476.277, -3327.969), Angle(0, 256.077, 0) )
			Create_Armour5( Vector(-3414.693, 579.989, -3325.969), Angle(0, 274.469, 0) )
			Create_Armour5( Vector(-3424.263, 737.841, -3325.969), Angle(0, 273.633, 0) )
			Create_Armour5( Vector(-3440.093, 895.649, -3264.121), Angle(0, 277.813, 0) )
			Create_Armour5( Vector(-3501.684, 1094.93, -3327.969), Angle(0, 286.173, 0) )
			Create_Beer( Vector(-3262.29, 1335.799, -3519.969), Angle(0, 224.936, 0) )
			Create_Beer( Vector(-3261.108, 1599.122, -3391.969), Angle(0, 285.337, 0) )
			Create_Beer( Vector(-3681.437, 1632.557, -3263.969), Angle(0, 355.77, 0) )
			Create_Beer( Vector(-3683.729, 1439.699, -3263.969), Angle(0, 30.673, 0) )
			Create_Beer( Vector(-3977.148, 1440.329, -3263.969), Angle(0, 358.696, 0) )
			Create_WhisperWeapon( Vector(-3588.852, 1914.646, -3583.969), Angle(0, 91.614, 0) )
			Create_ShotgunAmmo( Vector(-4360.466, 1507.351, -3263.969), Angle(0, 22.334, 0) )
			Create_RevolverAmmo( Vector(-4347.347, 1630.938, -3263.969), Angle(0, 330.711, 0) )
			Create_RevolverAmmo( Vector(-4314.724, 1639.681, -3263.969), Angle(0, 316.289, 0) )
			Create_SMGAmmo( Vector(-4319.31, 1497.62, -3263.969), Angle(0, 46.369, 0) )
			Create_SMGWeapon( Vector(-4363.243, 1575.578, -3263.969), Angle(0, 359.344, 0) )
			Create_SuperShotgunWeapon( Vector(-4312.022, 1574.272, -3263.969), Angle(0, 359.971, 0) )
			Create_RifleWeapon( Vector(-4260.298, 1575.034, -3263.969), Angle(0, 359.344, 0) )
			Create_RifleAmmo( Vector(-4267.469, 1498.131, -3263.969), Angle(0, 16.273, 0) )
			Create_PistolAmmo( Vector(-4267.606, 1635.151, -3263.969), Angle(0, 355.582, 0) )
			Create_CrossbowAmmo( Vector(-4223.926, 1628.93, -3263.969), Angle(0, 338.235, 0) )
			Create_KingSlayerAmmo( Vector(-4212.171, 1511.04, -3263.969), Angle(0, 24.633, 0) )
			Create_SpiritSphere( Vector(-4151.41, 1316.309, -3007.969), Angle(0, 88.378, 0) )
			Create_PortableMedkit( Vector(-5029.482, 833.435, -3263.969), Angle(0, 1.327, 0) )
			Create_PortableMedkit( Vector(-4995, 819.048, -3263.969), Angle(0, 20.346, 0) )
			Create_FalanranWeapon( Vector(-5685.63, 752.105, -3263.969), Angle(0, 99.349, 0) )
		
			CreateSecretArea( Vector(-3603.509,1913.778,-3577.786), Vector(-45.897,-45.897,0), Vector(45.897,45.897,66.623) )
			CreateSecretArea( Vector(-5685.79,755.34,-3259.437), Vector(-52.621,-52.621,0), Vector(52.621,52.621,92.389) )
		
		end
	
	else
	
		local FUCK_NO = {CreateProp(Vector(-3887.562,697.125,-3025.5), Angle(89.995,-179.962,180), "models/hunter/plates/plate32x32.mdl", true),
		CreateProp(Vector(-3886.625,-821.219,-3025.5), Angle(89.995,-179.962,180), "models/hunter/plates/plate32x32.mdl", true),
		CreateProp(Vector(-3888.469,2215.531,-3025.5), Angle(89.995,-179.962,180), "models/hunter/plates/plate32x32.mdl", true),
		CreateProp(Vector(-3401.875,1169,-3022.375), Angle(89.995,-90.022,180), "models/hunter/plates/plate32x32.mdl", true),
		CreateProp(Vector(-2643.094,1928.094,-3022.375), Angle(89.995,-0.022,180), "models/hunter/plates/plate32x32.mdl", true)}
		
		for _,v in pairs (FUCK_NO) do
		
			if v and IsValid(v) then
				v:SetNoDraw(true)
			end
		
		end

		CreatePlayerSpawn( Vector(-753.91235351563,809.57025146484,-3439.96875), Angle(0,-180,0) )

		if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
		
			Create_PistolAmmo( Vector(-1700.4522705078, 677.07971191406, -3455.96875), Angle(0, 177.67279052734, 0) )
			Create_PistolAmmo( Vector(-1701.9558105469, 724.82049560547, -3455.96875), Angle(0, 179.65278625488, 0) )
			Create_PistolAmmo( Vector(-1737.2111816406, 700.18023681641, -3455.96875), Angle(0, 179.43278503418, 0) )
			Create_Medkit25( Vector(-2141.6437988281, 421.67132568359, -3327.96875), Angle(0, 73.312705993652, 0) )
			Create_Medkit25( Vector(-2091.1333007813, 421.30410766602, -3327.96875), Angle(0, 107.41272735596, 0) )
			Create_PortableMedkit( Vector(-2116.5837402344, 451.53924560547, -3327.96875), Angle(0, 90.472640991211, 0) )
			Create_ShotgunAmmo( Vector(-1840.4132080078, 441.87133789063, -3327.96875), Angle(0, 359.61218261719, 0) )
			Create_ShotgunAmmo( Vector(-1833.0303955078, 411.94281005859, -3327.96875), Angle(0, 40.312408447266, 0) )
			Create_ShotgunAmmo( Vector(-1831.6579589844, 472.24234008789, -3327.96875), Angle(0, 317.59210205078, 0) )
			Create_ShotgunWeapon( Vector(-2104.5922851563, 465.32815551758, -3071.96875), Angle(0, 19.145462036133, 0) )
			Create_ShotgunAmmo( Vector(-2072.8830566406, 456.58905029297, -3071.96875), Angle(0, 32.785507202148, 0) )
			Create_ShotgunAmmo( Vector(-2085.7629394531, 483.03555297852, -3071.96875), Angle(0, 11.665390014648, 0) )
			Create_SMGAmmo( Vector(-2122.955078125, 711.59252929688, -3071.96875), Angle(0, 312.26544189453, 0) )
			Create_SMGAmmo( Vector(-2126.8874511719, 660.8994140625, -3071.96875), Angle(0, 8.5854644775391, 0) )
			Create_SpiritSphere( Vector(-1769.8603515625, 510.95101928711, -2943.96875), Angle(0, 343.28555297852, 0) )
		
			CreateSecretArea( Vector(-1777.4243164063,519.97918701172,-2943.96875), Vector(-57.961452484131,-57.961452484131,0), Vector(57.961452484131,57.961452484131,97.07666015625) )
		
		else
		
			CreateProp(Vector(-847.125,990.969,-3439.625), Angle(0.044,-9.844,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
			local BLOCK_1 = CreateProp(Vector(-697.875,972.719,-3439.5), Angle(0.044,-0.044,0), "models/props_lab/blastdoor001b.mdl", true)
			BLOCK_1:SetMaterial("models/props_combine/tprings_globe")
			
			Create_FoolsOathAmmo( Vector(-667.025, 974.381, -3439.969), Angle(0, -88.953, 0) )
			Create_EldritchArmour10( Vector(-674.787, 946.737, -3439.969), Angle(0, 15.05, 0) )
			Create_EldritchArmour10( Vector(-677.861, 1002.199, -3439.969), Angle(0, 347.044, 0) )
			Create_Beer( Vector(-1464.278, 810.476, -3455.969), Angle(0, 10.079, 0) )
			Create_Beer( Vector(-1554.93, 703.735, -3455.969), Angle(0, 54.387, 0) )
			Create_Beer( Vector(-1657.665, 576.234, -3455.969), Angle(0, 35.367, 0) )
			Create_Armour5( Vector(-1761.344, 579.885, -3455.969), Angle(0, 358.374, 0) )
			Create_KingSlayerWeapon( Vector(-1829.918, 623.658, -3455.969), Angle(0, 308.941, 0) )
			Create_Medkit25( Vector(-2020.987, 611.56, -3455.969), Angle(0, 314.691, 0) )
			Create_PortableMedkit( Vector(-1884.72, 412.381, -3455.969), Angle(0, 35.885, 0) )
			Create_Medkit10( Vector(-2150.481, 411.433, -3327.969), Angle(0, 66.297, 0) )
			Create_Medkit10( Vector(-2078.467, 410.862, -3327.969), Angle(0, 117.502, 0) )
			Create_Armour( Vector(-2114.416, 437.269, -3327.969), Angle(0, 93.257, 0) )
			Create_KingSlayerAmmo( Vector(-1843.861, 471.731, -3327.969), Angle(0, 329.738, 0) )
			Create_ManaCrystal( Vector(-1868.7, 501.305, -3071.969), Angle(0, 50.522, 0) )
			Create_ManaCrystal( Vector(-1871.057, 599.723, -3071.969), Angle(0, 355.346, 0) )
			Create_Medkit10( Vector(-1689.864, 620.15, -3071.969), Angle(0, 208.21, 0) )
			Create_Medkit10( Vector(-1748.211, 405.128, -3071.969), Angle(0, 95.768, 0) )
			Create_ShotgunAmmo( Vector(-2148.838, 512.456, -3071.969), Angle(0, 2.97, 0) )
			Create_ShotgunAmmo( Vector(-2121.799, 418.37, -3071.969), Angle(0, 57.937, 0) )
			Create_ShotgunWeapon( Vector(-2107.983, 477.553, -3071.969), Angle(0, 26.169, 0) )
			Create_Armour100( Vector(-2418.448, 646.862, -3395.969), Angle(0, 14.297, 0) )
			Create_Beer( Vector(-2969.407, 1099.839, -3135.969), Angle(0, 343.68, 0) )
			Create_CrossbowAmmo( Vector(-2863.74, 1139.686, -3135.969), Angle(0, 270.32, 0) )
			Create_GrenadeAmmo( Vector(-2650.542, 844.619, -3135.969), Angle(0, 104.375, 0) )
			Create_VolatileBattery( Vector(-1335.684, 886.771, -3567.969), Angle(0, 146.801, 0) )
			Create_SMGAmmo( Vector(-3019.663, 985.439, -3519.969), Angle(0, 47.317, 0) )
			Create_SMGWeapon( Vector(-3006.757, 1034.598, -3519.969), Angle(0, 359.246, 0) )
			Create_Armour5( Vector(-3108.765, 536.414, -3548.436), Angle(0, 69.68, 0) )
			Create_Armour5( Vector(-3117.088, 384.579, -3465.14), Angle(32.081, 89.429, 0) )
			Create_Armour5( Vector(-3162.046, 114.901, -3573.323), Angle(0, 95.495, 0) )
			Create_Armour5( Vector(-3068.357, -69.595, -3570.969), Angle(0, 86.508, 0) )
			Create_Beer( Vector(-3202.091, -124.167, -3583.969), Angle(0, 4.789, 0) )
			Create_Beer( Vector(-3440.496, -129.278, -3583.969), Angle(0, 2.49, 0) )
			Create_Beer( Vector(-3643.2, -200.252, -3583.969), Angle(0, 28.198, 0) )
			
			CreateSecretArea( Vector(-617.678,974.013,-3439.969), Vector(-51.262,-51.262,0), Vector(51.262,51.262,150.969) )
			CreateSecretArea( Vector(-1344.987,894.913,-3563.437), Vector(-23.845,-23.845,0), Vector(23.845,23.845,62.617) )
		
		end
		
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)