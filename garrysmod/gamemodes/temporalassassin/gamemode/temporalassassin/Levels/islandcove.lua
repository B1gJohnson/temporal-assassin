		
if CLIENT then return end

LEVEL_STRINGS["islandcove"] = { "islandcove2", Vector(782.895,-1296.365,256.031), Vector(-104.612,-104.612,0), Vector(104.612,104.612,145.023) }

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	for _,v in pairs (ents.FindInSphere(Vector(1586.969,1472.351,496.031),100)) do
		
		if IsValid(v) and v:IsDoor() then
			v:Fire("Open")
		end
	
	end
	
	for _,v in pairs (ents.FindInSphere(Vector(1629.031,2051.881,496.031),100)) do
		
		if IsValid(v) and v:IsDoor() then
			v:Fire("Open")
		end
	
	end
	
	CreatePlayerSpawn(Vector(1589.11,1873.69,496.38),Angle(0,82,0))
	
	Create_ShotgunWeapon( Vector(1063.726, 1045.423, 496.031), Angle(0, 96.475, 0) )
	Create_SuperShotgunWeapon( Vector(1034.768, 1081.04, 496.031), Angle(0, 84.375, 0) )
	Create_ShotgunAmmo( Vector(996.101, 1095.284, 519.83), Angle(0, 44.775, 0) )
	Create_SMGAmmo( Vector(997.281, 1185.471, 496.031), Angle(0, 313.145, 0) )
	Create_SMGAmmo( Vector(991.024, 1157.051, 496.031), Angle(0, 340.315, 0) )
	Create_PistolWeapon( Vector(995.349, 1123.376, 496.031), Angle(0, 19.475, 0) )
	Create_PistolAmmo( Vector(1003.755, 1140.049, 496.031), Angle(0, 358.905, 0) )
	Create_Medkit25( Vector(1582.762, 2031.617, 496.031), Angle(0, 269.916, 0) )
	Create_ManaCrystal( Vector(1710.987, 2030.05, 496.031), Angle(0, 220.856, 0) )
	Create_ManaCrystal( Vector(1679.499, 2031.314, 496.031), Angle(0, 235.376, 0) )
	Create_PistolAmmo( Vector(1627.57, 2469.363, 496.031), Angle(0, 331.957, 0) )
	Create_Armour200( Vector(1817.742, 2373.502, 712.328), Angle(0, 183.176, 0) )
	Create_Medkit25( Vector(2975.759, 2438.945, 496.031), Angle(0, 180.836, 0) )
	Create_RifleAmmo( Vector(2156.297, 2733.732, 496.031), Angle(0, 53.676, 0) )
	Create_RifleAmmo( Vector(2231.973, 2731.496, 496.031), Angle(0, 105.156, 0) )
	Create_PortableMedkit( Vector(2190.604, 2727.873, 496.031), Angle(0, 89.316, 0) )
	Create_Medkit10( Vector(2843.791, 2998.422, 624.031), Angle(0, 194.976, 0) )
	Create_Armour( Vector(2801.926, 2751.194, 624.031), Angle(0, 140.416, 0) )
	Create_ShotgunAmmo( Vector(2789.376, 2916.511, 624.031), Angle(0, 193.106, 0) )
	Create_ShotgunAmmo( Vector(2780.759, 2825.46, 624.031), Angle(0, 147.896, 0) )
	Create_ManaCrystal( Vector(2785.906, 2869.057, 624.031), Angle(0, 182.656, 0) )
	Create_Bread( Vector(2497.164, 3000.766, 624.031), Angle(0, 330.496, 0) )
	Create_Armour5( Vector(2752.196, 3363.036, 704.031), Angle(0, 345.455, 0) )
	Create_Armour5( Vector(2793.581, 3360.937, 704.031), Angle(0, 330.935, 0) )
	Create_Armour5( Vector(2824.519, 3362.456, 704.031), Angle(0, 266.475, 0) )
	Create_Armour5( Vector(2865.092, 3364.081, 704.031), Angle(0, 205.315, 0) )
	Create_Backpack( Vector(2512.386, 3047.127, 624.031), Angle(0, 97.735, 0) )
	Create_Medkit25( Vector(2375.606, 3981.488, 624.031), Angle(0, 270.735, 0) )
	Create_EldritchArmour( Vector(2782.691, 3472.143, 624.031), Angle(0, 79.088, 0) )
	Create_Medkit10( Vector(2283.902, 3109.89, 488.031), Angle(0, 3.109, 0) )
	Create_Medkit10( Vector(2390.111, 3111.455, 488.031), Angle(0, 175.589, 0) )
	Create_SMGAmmo( Vector(2357.295, 3110.694, 488.031), Angle(0, 105.849, 0) )
	Create_RevolverAmmo( Vector(2322.515, 3110.708, 488.031), Angle(0, 66.029, 0) )
	Create_Armour100( Vector(1836.999, 3027.639, 467.031), Angle(0, 71.089, 0) )
	Create_M6GWeapon( Vector(2106.541, 3155.267, 569.031), Angle(0, 250.91, 0) )
	Create_M6GAmmo( Vector(2102.915, 3125.445, 569.031), Angle(0, 235.73, 0) )
	Create_Medkit25( Vector(1691.49, 2981.557, 529.759), Angle(0, 3.19, 0) )
	Create_Armour5( Vector(1693.234, 2956.372, 529.774), Angle(0, 30.91, 0) )
	Create_Armour5( Vector(1897.058, 2827.868, 537.031), Angle(0, 123.39, 0) )
	Create_RevolverWeapon( Vector(1899.461, 2875.246, 537.031), Angle(0, 171.35, 0) )
	Create_PistolWeapon( Vector(1896.924, 2847.217, 537.031), Angle(0, 171.35, 0) )
	Create_KingSlayerWeapon( Vector(1522.638, 2860.016, 496.031), Angle(0, 359.225, 0) )
	Create_KingSlayerAmmo( Vector(1547.879, 2875, 496.031), Angle(0, 313.904, 0) )
	Create_EldritchArmour10( Vector(1450.16, 2773.189, 582.939), Angle(0, 193.221, 0) )
	Create_EldritchArmour10( Vector(1446.01, 2752.973, 582.62), Angle(0, 172.761, 0) )
	Create_Bread( Vector(1429.184, 2764.793, 581.33), Angle(0, 187.941, 0) )
	Create_Medkit10( Vector(631.333, 1172.505, 128.031), Angle(0, 105.453, 0) )
	Create_Medkit10( Vector(423.697, 1349.044, 128.031), Angle(0, 340.273, 0) )
	Create_Medkit10( Vector(434.133, 1520.728, 128.031), Angle(0, 266.353, 0) )
	Create_RevolverAmmo( Vector(634.67, 1277.272, 128.031), Angle(0, 183.853, 0) )
	Create_SpiritSphere( Vector(945.348, 878.317, 266.529), Angle(0, 89.693, 0) )
	Create_SMGAmmo( Vector(1362.155, 651.933, 128.031), Angle(0, 59.113, 0) )
	Create_RevolverAmmo( Vector(1412.717, 638.448, 128.031), Angle(0, 81.993, 0) )
	Create_M6GAmmo( Vector(1388.043, 655.282, 128.031), Angle(0, 69.013, 0) )
	Create_RifleAmmo( Vector(1933.093, 427.816, 128.031), Angle(0, 127.753, 0) )
	Create_RifleWeapon( Vector(1893.377, 407.218, 128.031), Angle(0, 93.212, 0) )
	Create_CrossbowAmmo( Vector(1642.149, -627.478, 128.031), Angle(0, 270.547, 0) )
	Create_CrossbowAmmo( Vector(1641.821, -665.351, 128.031), Angle(0, 272.087, 0) )
	Create_Medkit25( Vector(1340.791, -823.513, 128.031), Angle(0, 359.866, 0) )
	Create_RifleAmmo( Vector(1404.283, -1086.555, 256.031), Angle(0, 330.107, 0) )
	Create_Medkit10( Vector(1037.709, -1214.998, 256.031), Angle(0, 310.087, 0) )
	Create_Medkit10( Vector(1039.626, -1255.266, 256.031), Angle(0, 14.767, 0) )
	Create_M6GAmmo( Vector(1224.43, -1191.238, 256.031), Angle(0, 227.367, 0) )
	Create_SMGWeapon( Vector(1210.816, -1400.468, 256.031), Angle(0, 92.507, 0) )
	
	CreateSecretArea( Vector(1041.228,1148.003,496.031), Vector(-55.813,-55.813,0), Vector(55.813,55.813,110.528) )
	CreateSecretArea( Vector(1816.318,2373.578,712.281), Vector(-36.618,-36.618,0), Vector(36.618,36.618,41.079) )
	CreateSecretArea( Vector(2501.739,3066.181,624.031), Vector(-28.27,-28.27,0), Vector(28.27,28.27,75.012) )
	CreateSecretArea( Vector(2779.023,3470.724,628.563), Vector(-25.381,-25.381,0), Vector(25.381,25.381,2.546) )
	CreateSecretArea( Vector(2108.368,3141.088,573.563), Vector(-32.541,-32.541,0), Vector(32.541,32.541,27.9) )
	CreateSecretArea( Vector(1544.82,2862.32,500.563), Vector(-39.786,-39.786,0), Vector(39.786,39.786,23.496) )
	CreateSecretArea( Vector(949.334,873.003,265.581), Vector(-58.006,-58.006,0), Vector(58.006,58.006,57.291) )
	CreateSecretArea( Vector(1895.753,411.604,132.563), Vector(-55.374,-55.374,0), Vector(55.374,55.374,31.497) )
	
	CreateCosmicStatue( Vector(-896.39,926.094,164.423), Angle(0,135,0), "whispers_1", "Faint whispers fill your mind about a time long passed #1. (Lore Unlocked)" )
	CreateProp(Vector(-1003.562,1029.906,215.313), Angle(0,-135.016,0), "models/props_wasteland/cargo_container01b.mdl", true)
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)