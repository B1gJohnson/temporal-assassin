		
if CLIENT then return end

LEVEL_STRINGS["ep1_c17_01"] = {"ep1_c17_02",Vector(-3800.41015625,409.16049194336,0.03125), Vector(-95.106010437012,-95.106010437012,0), Vector(95.106010437012,95.106010437012,119.9375)}

function EXTRA_FAIL_STATE( npc, att, inf )

	if IsValid(npc) and ( npc:GetClass() == "npc_alyx" ) then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRA_FAIL_STATEHook",EXTRA_FAIL_STATE)

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("trigger_changelevel")
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Medkit25( Vector(4760.1586914063, 346.33392333984, -255.96875), Angle(0, 269.79943847656, 0) )
		Create_Medkit25( Vector(4696.685546875, 348.97412109375, -255.96875), Angle(0, 269.5793762207, 0) )
		Create_Medkit25( Vector(4637.6552734375, 350.87811279297, -255.96875), Angle(0, 270.67935180664, 0) )
		Create_Medkit10( Vector(4667.8759765625, 323.88162231445, -255.96875), Angle(0, 272.43936157227, 0) )
		Create_Medkit10( Vector(4728.8999023438, 325.82489013672, -255.96875), Angle(0, 269.79943847656, 0) )
		Create_Medkit10( Vector(4698.5942382813, 295.529296875, -255.96875), Angle(0, 270.67935180664, 0) )
		Create_Armour100( Vector(4789.091796875, 18.043746948242, -255.96875), Angle(0, 178.85939025879, 0) )
		Create_Beer( Vector(4526.6987304688, 291.59100341797, -239.76974487305), Angle(0, 307.33941650391, 0) )
		Create_Beer( Vector(4526, 260.11090087891, -239.76974487305), Angle(0, 318.55947875977, 0) )
		Create_Beer( Vector(4526.4077148438, 230.0640411377, -239.76974487305), Angle(0, 333.95956420898, 0) )
		Create_PistolAmmo( Vector(4607.7221679688, -515.80474853516, -255.96875), Angle(0, 324.49914550781, 0) )
		Create_PistolAmmo( Vector(4595.4560546875, -528.08190917969, -255.96875), Angle(0, 332.41918945313, 0) )
		Create_PistolAmmo( Vector(4608.3784179688, -547.50982666016, -255.96875), Angle(0, 343.30923461914, 0) )
		Create_PistolAmmo( Vector(4617.0180664063, -532.30828857422, -255.96875), Angle(0, 326.25915527344, 0) )
		Create_RevolverWeapon( Vector(4607.46875, -693.7568359375, -255.96875), Angle(0, 46.119125366211, 0) )
		Create_RevolverAmmo( Vector(4598.6772460938, -658.62567138672, -255.96875), Angle(0, 26.539016723633, 0) )
		Create_KingSlayerAmmo( Vector(4604.76171875, -610.12457275391, -255.96875), Angle(0, 0.578857421875, 0) )
		Create_KingSlayerAmmo( Vector(4634.8828125, -609.82025146484, -255.96875), Angle(0, 0.578857421875, 0) )
		Create_SMGAmmo( Vector(4769.4799804688, -695.11590576172, -255.96875), Angle(0, 111.67882537842, 0) )
		Create_SMGAmmo( Vector(4744.2446289063, -696.35729980469, -255.96875), Angle(0, 96.938758850098, 0) )
		Create_SMGAmmo( Vector(4763.8872070313, -668.94738769531, -255.96875), Angle(0, 115.19886016846, 0) )
		Create_SMGAmmo( Vector(4740.5698242188, -670.47369384766, -255.96875), Angle(0, 96.498756408691, 0) )
		Create_VolatileBattery( Vector(4369.7465820313, -231.67248535156, -29.968753814697), Angle(0, 257.99771118164, 0) )
		Create_Backpack( Vector(4344.1328125, -230.38739013672, -29.96875), Angle(0, 282.6376953125, 0) )
		Create_Armour( Vector(3636.0634765625, 156.83598327637, 40.03125), Angle(0, 346.57934570313, 0) )
		Create_Medkit25( Vector(3682.2973632813, 87.793426513672, 40.03125), Angle(0, 1.5393676757813, 0) )
		Create_PortableMedkit( Vector(3675.1271972656, -274.51751708984, 0), Angle(0, 347.67910766602, 0) )
		Create_PortableMedkit( Vector(3716.4907226563, -276.50039672852, 0), Angle(0, 339.7590637207, 0) )
		Create_SMGAmmo( Vector(4012.8940429688, -339.53588867188, 0.03125), Angle(0, 186.11938476563, 0) )
		Create_SMGAmmo( Vector(4015.8793945313, -423.48626708984, 0.031246185302734), Angle(0, 167.63938903809, 0) )
		Create_RevolverAmmo( Vector(4015.4184570313, -385.7873840332, 0.03125), Angle(0, 176.87939453125, 0) )
		Create_SpiritSphere( Vector(4049.0183105469, -1116.1158447266, 319.03125), Angle(0, 227.91885375977, 0) )
		Create_ShotgunAmmo( Vector(3723.1779785156, -1549.2856445313, 13.79076385498), Angle(0, 188.89157104492, 0) )
		Create_ShotgunAmmo( Vector(3704.556640625, -1569.0466308594, 12.472023010254), Angle(0, 172.17156982422, 0) )
		Create_Medkit25( Vector(1676.1687011719, -119.0916519165, 0.03125), Angle(0, 348.50921630859, 0) )
		Create_Medkit25( Vector(1671.5865478516, -196.02145385742, 0.03125), Angle(0, 11.389083862305, 0) )
		Create_ManaCrystal100( Vector(2251.1052246094, 671.95031738281, -3.9687538146973), Angle(0, 295.4091796875, 0) )
		Create_ShotgunWeapon( Vector(2640.1564941406, 413.55218505859, 0.03125), Angle(0, 213.56918334961, 0) )
		Create_PistolAmmo( Vector(1847.3405761719, 861.92297363281, 0.03125), Angle(0, 315.20928955078, 0) )
		Create_PistolAmmo( Vector(1897.7491455078, 829.89678955078, 0.03125), Angle(0, 306.18923950195, 0) )
		Create_PistolAmmo( Vector(1891.0926513672, 774.42108154297, 0.03125), Angle(0, 349.30944824219, 0) )
		Create_SMGAmmo( Vector(2246.9460449219, 1244.5629882813, 0.03125), Angle(0, 228.08935546875, 0) )
		Create_SMGAmmo( Vector(2288.9970703125, 1160.9796142578, 0.03125), Angle(0, 173.74935913086, 0) )
		Create_SMGAmmo( Vector(2270.2153320313, 1203.2235107422, 0.03125), Angle(0, 198.609375, 0) )
		Create_Medkit10( Vector(952.88439941406, 1746.3096923828, 0.031246185302734), Angle(0, 7.2087097167969, 0) )
		Create_Medkit10( Vector(949.80169677734, 1785.0936279297, 0.031253814697266), Angle(0, 350.04861450195, 0) )
		Create_PortableMedkit( Vector(946.96704101563, 1739.9593505859, -71.96875), Angle(0, 55.60865020752, 0) )
		Create_PistolWeapon( Vector(952.35412597656, 1778.9029541016, -71.96875), Angle(0, 30.308624267578, 0) )
		Create_ShotgunAmmo( Vector(983.62731933594, 1745.7080078125, -71.96875), Angle(0, 53.628753662109, 0) )
		Create_SMGAmmo( Vector(951.37640380859, 2332.0017089844, 0.031253814697266), Angle(0, 286.9873046875, 0) )
		Create_SMGAmmo( Vector(983.34515380859, 2327.6333007813, 0.03125), Angle(0, 257.06726074219, 0) )
		Create_RevolverAmmo( Vector(1008.703125, 1913.6225585938, 192.03125), Angle(0, 159.52757263184, 0) )
		Create_PistolAmmo( Vector(965.61291503906, 1924.7938232422, 192.03125), Angle(0, 154.68756103516, 0) )
		Create_PistolAmmo( Vector(947.31256103516, 1880.2598876953, 192.03125), Angle(0, 121.9075088501, 0) )
		Create_Armour100( Vector(316.74102783203, 1274.0853271484, 0.03125), Angle(0, 89.787086486816, 0) )
		Create_Medkit25( Vector(280.05291748047, 1271.9846191406, 0.031253814697266), Angle(0, 69.766990661621, 0) )
		Create_Medkit25( Vector(359.18185424805, 1272.8470458984, 0.031253814697266), Angle(0, 112.88706970215, 0) )
		Create_KingSlayerWeapon( Vector(241.22653198242, 1321.9763183594, 0.031246185302734), Angle(0, 46.634963989258, 0) )
		Create_KingSlayerAmmo( Vector(225.23902893066, 1369.5574951172, -3.96875), Angle(0, 21.554885864258, 0) )
		Create_KingSlayerAmmo( Vector(223.59208679199, 1409.34765625, -3.96875), Angle(0, 359.33477783203, 0) )
		Create_KingSlayerAmmo( Vector(236.58963012695, 1450.4326171875, -3.96875), Angle(0, 331.39462280273, 0) )
		Create_Armour( Vector(136.13635253906, -211.61793518066, 0.03125), Angle(0, 104.50102233887, 0) )
		Create_Medkit25( Vector(59.57213973999, -213.16397094727, 0.031253814697266), Angle(0, 73.260856628418, 0) )
		Create_SMGAmmo( Vector(12.433990478516, -170.47222900391, 0.03125), Angle(0, 46.20085144043, 0) )
		Create_SMGAmmo( Vector(9.1283645629883, -126.51983642578, 0.031253814697266), Angle(0, 27.500747680664, 0) )
		Create_RevolverAmmo( Vector(163.65190124512, -155.79684448242, 0.031253814697266), Angle(0, 129.80073547363, 0) )
		Create_PortableMedkit( Vector(-692.69171142578, -157.53823852539, 64.03125), Angle(0, 27.830688476563, 0) )
		Create_Medkit10( Vector(-689.29663085938, -217.38931274414, 64.03125), Angle(0, 57.970779418945, 0) )
		Create_Medkit10( Vector(-659.83453369141, -219.48892211914, 64.03125), Angle(0, 73.37084197998, 0) )
		Create_Medkit10( Vector(-612.94946289063, -219.18267822266, 64.03125), Angle(0, 101.31094360352, 0) )
		Create_Medkit10( Vector(-638.11901855469, -219.99472045898, 64.03125), Angle(0, 86.130882263184, 0) )
		Create_Beer( Vector(-653.10418701172, 182.11631774902, 64.03125), Angle(0, 280.03085327148, 0) )
		Create_Beer( Vector(-574.33056640625, 191.57331848145, 64.03125), Angle(0, 250.11074829102, 0) )
		Create_Beer( Vector(-593.66784667969, 139.28309631348, 64.03125), Angle(0, 250.33074951172, 0) )
		Create_Beer( Vector(-607.68298339844, 156.73803710938, 64.03125), Angle(0, 259.79080200195, 0) )
		Create_Beer( Vector(-576.42456054688, 35.409233093262, 64.03125), Angle(0, 201.49072265625, 0) )
		Create_Beer( Vector(-654.01898193359, 38.832984924316, 64.03125), Angle(0, 265.29077148438, 0) )
		Create_Beer( Vector(-624.80212402344, -100.72520446777, 64.03125), Angle(0, 103.04074859619, 0) )
		Create_Beer( Vector(-643.98004150391, 105.08084106445, 64.03125), Angle(0, 271.45080566406, 0) )
		Create_Armour100( Vector(-1018.0216674805, 186.35745239258, 0.031246185302734), Angle(0, 323.81048583984, 0) )
		Create_RevolverWeapon( Vector(-2937.2739257813, 633.0390625, 104.03125), Angle(0, 176.99545288086, 0) )
		Create_SMGWeapon( Vector(-2980.4970703125, 619.55560302734, 92.03125), Angle(0, 177.21543884277, 0) )
		Create_SMGAmmo( Vector(-2932.4128417969, 607.3408203125, 104.03125), Angle(0, 171.49542236328, 0) )
		Create_SMGAmmo( Vector(-2960.1643066406, 597.43853759766, 104.03125), Angle(0, 160.71542358398, 0) )
		
		Create_CrossbowWeapon( Vector(1612.6683349609, 2316.8801269531, 0.03125), Angle(0, 187.64547729492, 0) )
		Create_CrossbowAmmo( Vector(1618.6398925781, 2272.1193847656, 0.031253814697266), Angle(0, 271.02554321289, 0) )
		Create_CrossbowAmmo( Vector(1619.5812988281, 2248.357421875, 0.031246185302734), Angle(0, 270.36553955078, 0) )
		
		CreateSecretArea( Vector(4380.736328125,-250.96299743652,-29.968746185303), Vector(-68.04190826416,-68.04190826416,0), Vector(68.04190826416,68.04190826416,127.28942871094) )
		CreateSecretArea( Vector(4043.1906738281,-1127.9016113281,319.03125), Vector(-75.894325256348,-75.894325256348,0), Vector(75.894325256348,75.894325256348,101.68215942383) )
		
		CreateProp(Vector(7781.75,813.84375,648.59375), Angle(20.126953125,-135.615234375,-5.625), "models/temporalassassin/doll/unknown.mdl", true)
		
	else
	
		CreateProp(Vector(11000.625,-2096.406,-994.687), Angle(0.044,-94.175,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
		
		local PH1 = CreateProp(Vector(-569.031,-227.594,79.219), Angle(0.308,150.073,0), "models/props_junk/metalgascan.mdl", true)
		local PH2 = CreateProp(Vector(-707.437,-228.406,79.281), Angle(0.615,45.044,0), "models/props_junk/metalgascan.mdl", true)
		PH1:SetPhysicsWeight(5000)
		PH2:SetPhysicsWeight(5000)
		
		Create_Medkit25( Vector(4524.822, -346.018, -239.398), Angle(0, 14.11, 0) )
		Create_Medkit25( Vector(4524.183, -296.058, -239.398), Angle(0, 356.136, 0) )
		Create_Beer( Vector(4524.755, -459.826, -239.398), Angle(0, 54.137, 0) )
		Create_Beer( Vector(4544.756, -459.602, -239.398), Angle(0, 71.066, 0) )
		Create_Armour( Vector(4592.774, -462.315, -239.398), Angle(0, 118.717, 0) )
		Create_CrossbowWeapon( Vector(4724.722, 301.558, -255.969), Angle(0, 247.564, 0) )
		Create_CrossbowAmmo( Vector(4663.888, 313.417, -255.969), Angle(0, 164.591, 0) )
		Create_VolatileBattery( Vector(4358.316, -245.597, -29.969), Angle(0, 324.618, 0) )
		Create_GrenadeAmmo( Vector(4344.491, -326.089, -29.969), Angle(0, 6, 0) )
		Create_GrenadeAmmo( Vector(4342.714, -295.771, -29.969), Angle(0, 346.772, 0) )
		Create_CrossbowAmmo( Vector(4374.392, -319.231, -29.969), Angle(0, 2.447, 0) )
		Create_CrossbowAmmo( Vector(4398.268, -287.488, -29.969), Angle(0, 13.525, 0) )
		Create_PortableMedkit( Vector(4158.347, 694.113, 8.031), Angle(0, 88.557, 0) )
		Create_AnthrakiaWeapon( Vector(9069.081, -3.879, 206.886), Angle(0, 12.588, 0) )
		Create_Armour5( Vector(4180.754, 693.54, 8.031), Angle(0, 40.175, 0) )
		Create_Armour5( Vector(4207.463, 694.585, 8.031), Angle(0, 60.031, 0) )
		Create_Bread( Vector(4172.503, 720.976, 0.031), Angle(0, 86.992, 0) )
		Create_Armour( Vector(3999.776, -162.29, 40.031), Angle(0, 151.154, 0) )
		Create_Beer( Vector(4001.534, -113.274, 40.031), Angle(0, 187.311, 0) )
		Create_Beer( Vector(3994.658, -139.622, 40.031), Angle(0, 165.784, 0) )
		Create_KingSlayerAmmo( Vector(3643.583, 483.694, 0.031), Angle(0, 332.776, 0) )
		Create_RifleAmmo( Vector(3632.352, 396.266, 0.031), Angle(0, 26.489, 0) )
		Create_RifleAmmo( Vector(3669.783, 389.203, 0.031), Angle(0, 22.309, 0) )
		Create_Medkit10( Vector(3941.703, -928.597, 0.031), Angle(0, 169.027, 0) )
		Create_Medkit10( Vector(3938.629, -834.332, 0.031), Angle(0, 193.48, 0) )
		Create_RevolverWeapon( Vector(3938.71, -882.266, 0.031), Angle(0, 182.821, 0) )
		Create_RifleWeapon( Vector(3779.725, -204.543, 0.031), Angle(0, 269.448, 0) )
		Create_Bread( Vector(2868.899, -1336.114, 0.031), Angle(0, 173.724, 0) )
		Create_Beer( Vector(2830.34, -1334.426, 0.031), Angle(0, 172.052, 0) )
		Create_Beer( Vector(2793.979, -1326.956, 0.031), Angle(0, 174.769, 0) )
		Create_Armour100( Vector(2847.729, -365.747, 0), Angle(0, 225.556, 0) )
		Create_Medkit25( Vector(2161.368, -91.304, 0.031), Angle(0, 297.869, 0) )
		Create_MaskOfShadows( Vector(2769.648, -1028.133, 0.031), Angle(0, 93.366, 0) )
		Create_SMGAmmo( Vector(2115.815, -112.011, 0.031), Angle(0, 269.864, 0) )
		Create_SMGAmmo( Vector(2075.299, -115.282, 0.031), Angle(0, 284.076, 0) )
		Create_ShotgunAmmo( Vector(2209.834, -63.927, 0.031), Angle(0, 314.799, 0) )
		Create_ShotgunAmmo( Vector(2238.032, -49.049, 0.031), Angle(0, 292.227, 0) )
		Create_PistolAmmo( Vector(2208.914, -101.906, 0.031), Angle(0, 285.33, 0) )
		Create_PistolAmmo( Vector(2192.078, -130.589, 0.031), Angle(0, 305.185, 0) )
		Create_PistolAmmo( Vector(2153.749, -130.585, 0.031), Angle(0, 290.973, 0) )
		Create_RevolverAmmo( Vector(2156.661, -167.238, 0.031), Angle(0, 314.799, 0) )
		Create_RevolverAmmo( Vector(2227.791, -133.413, 0.031), Angle(0, 289.719, 0) )
		Create_GrenadeAmmo( Vector(2187.701, -164.245, 0.031), Angle(0, 305.499, 0) )
		Create_GrenadeAmmo( Vector(2212.304, -153.289, 0.031), Angle(0, 284.912, 0) )
		Create_CrossbowAmmo( Vector(2210.36, -183.783, 0.031), Angle(0, 294.317, 0) )
		Create_PortableMedkit( Vector(1556.216, 1998.688, 0.031), Angle(0, 171.749, 0) )
		Create_Medkit25( Vector(1533.697, 2021.174, 0.031), Angle(0, 186.379, 0) )
		Create_GrenadeAmmo( Vector(1507.611, 1997.501, 0.031), Angle(0, 169.659, 0) )
		Create_CrossbowWeapon( Vector(1425.399, 1992.818, 0.031), Angle(0, 167.57, 0) )
		Create_Armour( Vector(1353.787, 2020.749, 0.031), Angle(0, 192.649, 0) )
		Create_PistolWeapon( Vector(1631.216, 2226.382, 0.031), Angle(0, 243.959, 0) )
		Create_PistolAmmo( Vector(1647.59, 2208.048, 0.031), Angle(0, 224.94, 0) )
		Create_PistolAmmo( Vector(1615.277, 2196.519, 0.031), Angle(0, 245.631, 0) )
		Create_Medkit10( Vector(1709.144, 1754.723, 192.031), Angle(0, 56.175, 0) )
		Create_Medkit10( Vector(1739.371, 1752.259, 192.031), Angle(0, 70.387, 0) )
		Create_KingSlayerAmmo( Vector(1788.987, 1754.188, 192.031), Angle(0, 97.661, 0) )
		Create_Medkit25( Vector(850.038, 1919.441, 192.031), Angle(0, 25.243, 0) )
		Create_PortableMedkit( Vector(871.725, 1874.438, 192.031), Angle(0, 59.101, 0) )
		Create_InfiniteGrenades( Vector(339.606, 1260.919, 0.031), Angle(0, 88.779, 0) )
		Create_KingSlayerWeapon( Vector(376.616, 1285.239, 0.031), Angle(0, 56.175, 0) )
		Create_Medkit25( Vector(204.367, 578.048, -3.969), Angle(0, 358.731, 0) )
		Create_Armour( Vector(172.924, 536.054, -3.969), Angle(0, 12.943, 0) )
		Create_SuperShotgunWeapon( Vector(57.989, -177.926, 0.031), Angle(0, 59.442, 0) )
		Create_RevolverWeapon( Vector(99.967, -206.925, 0.031), Angle(0, 85.985, 0) )
		Create_SMGWeapon( Vector(145.902, -178.449, 0.031), Angle(0, 113.991, 0) )
		Create_ShotgunWeapon( Vector(103.124, -169.391, 0.031), Angle(0, 89.329, 0) )
		Create_Medkit25( Vector(-543.211, 1118.589, 0.031), Angle(0, 261.438, 0) )
		Create_ManaCrystal100( Vector(-786.165, 1069.627, 0.031), Angle(0, 5.938, 0) )
		Create_PistolAmmo( Vector(-1022.781, 977.96, 64.031), Angle(0, 59.651, 0) )
		Create_PistolAmmo( Vector(-993.14, 968.916, 64.031), Angle(0, 110.438, 0) )
		Create_FoolsOathAmmo( Vector(-1004.184, 996.774, 64.031), Angle(0, -13.838, 0) )
		Create_Medkit25( Vector(-1016.37, 183.066, 0.031), Angle(0, 332.708, 0) )
		Create_Medkit25( Vector(-3013.57, 735.689, 83.754), Angle(0, 267.29, 0) )
		Create_Medkit25( Vector(-3015.752, 687.172, 83.754), Angle(0, 267.081, 0) )
		Create_Armour( Vector(-3425.105, 388.822, 0.031), Angle(0, 342.529, 0) )
		Create_SMGWeapon( Vector(-3431.584, 308.84, 0.031), Angle(0, 12.208, 0) )
		Create_SMGAmmo( Vector(-3400.057, 284.893, 0.031), Angle(0, 33.944, 0) )
		Create_SMGAmmo( Vector(-3411.702, 336.761, 0.031), Angle(0, 355.279, 0) )
		Create_CrossbowAmmo( Vector(-2138.454, 696.933, 0.031), Angle(0, 276.802, 0) )
		Create_EldritchArmour( Vector(-3464.938, 698.147, 200.031), Angle(0, 280.044, 0) )
		Create_RaikiriWeapon( Vector(1473.92, 2342.978, -159.969), Angle(0, 318.528, 0) )
		
		local S_BUTTON1 = function()
			Create_FoolsOathWeapon( Vector(-617.406, 170.997, 79.27), Angle(0, 272.934, 0) )
			CreateProp(Vector(-619.187,184.75,79.219), Angle(-0.044,180,0), "models/props_wasteland/prison_bedframe001b.mdl", true)
		end
		
		AddSecretButton( Vector(-639.423,-239.969,118.786), S_BUTTON1 )
		
		local STUPID_SECRET = function()
			CreateSecretArea( Vector(9069.591,-1.058,172.081), Vector(-76.031,-76.031,0), Vector(76.031,76.031,121.091), "WHAT ARE YOU DOING BACK HERE?!", Sound("TemporalAssassin/Secrets/Secret_Yak1.wav") )
		end
		
		CreateSecretArea( Vector(4378.79,-281.203,-25.437), Vector(-60.687,-60.687,0), Vector(60.687,60.687,57.034) )
		CreateTrigger( Vector(9069.591,-1.058,172.081), Vector(-76.031,-76.031,0), Vector(76.031,76.031,121.091), STUPID_SECRET )
		CreateSecretArea( Vector(3780.895,-205.318,4.563), Vector(-34.784,-34.784,0), Vector(34.784,34.784,7.69) )
		CreateSecretArea( Vector(2771.582,-1028.587,0.031), Vector(-52.384,-52.384,0), Vector(52.384,52.384,65.406) )
		CreateSecretArea( Vector(-3463.267,696.258,204.563), Vector(-31.268,-31.268,0), Vector(31.268,31.268,25.452) )
		CreateSecretArea( Vector(1473.446,2336.047,-155.437), Vector(-25.143,-25.143,0), Vector(25.143,25.143,65.664) )
		
		--[[ Stupid trigger shit ]]
		
		local STUPID_1 = function(self,ply)
		
			ply:SetPos( Vector(1128.147,2332.274,111.75) )
			local B_P1 = CreateProp(Vector(1067.719,2299.25,77.406), Angle(0,-90.044,0), "models/props_lab/blastdoor001c.mdl", true)
			local B_P2 = CreateProp(Vector(1008.406,2314.531,78.281), Angle(-0.044,179.956,0), "models/props_lab/blastdoor001a.mdl", true)
			local B_P3 = CreateProp(Vector(972.5,2335.313,44.031), Angle(-0.044,-89.912,0), "models/props_wasteland/controlroom_storagecloset001a.mdl", true)
			local B_P4 = CreateProp(Vector(959.938,2337.875,101.688), Angle(0,-5.273,0), "models/props_wasteland/controlroom_filecabinet001a.mdl", true)
			local B_DOLL = CreateProp(Vector(956.75,2338.25,115.625), Angle(-0.044,-7.91,-0.264), "models/temporalassassin/doll/unknown.mdl", true)
			B_P1:SetNoDraw(true)
			B_P2:SetNoDraw(true)
			
			local DOLL_DISSAPEAR1 = function()
	
				CreateParticleEffect( "Eldritch_Disappear", B_DOLL:GetBoxCenter(), Angle(0,0,0), 1 )
				sound.Play( "TemporalAssassin/Doll/Doll_Teleported.wav", B_DOLL:GetBoxCenter(), 140, 100, 1, CHAN_STATIC )
			
				timer.Simple(0,function()
					
					if IsValid(B_DOLL) then
						B_DOLL:SetPos( Vector(956.75,2338.25,-3115.625) )
						B_DOLL:Remove()
					end
			
					B_P1:Remove()
					B_P2:Remove()
					
				end)
				
			end
			
			timer.Simple(4,function()
				DOLL_DISSAPEAR1()
			end)
			
		end
		
		CreateTrigger( Vector(1079.669,2359.969,110.471), Vector(-50.945,-50.945,0), Vector(50.945,50.945,63.33), STUPID_1 )
		
	end
	
	for _,v in pairs (ents.GetAll()) do
	
		if v and IsValid(v) then

			if v.GetModel and v:IsProp() and string.find(v:GetModel(),"concrete_spawnchunk") then
				v:SetCollisionGroup(COLLISION_GROUP_WEAPON)
			end
			
			if v.GetClass and v:GetClass() == "momentary_rot_button" then
				v:SetKeyValue("speed",200)
			end
			
		end
	
	end
	
	CreateTeleporter( "Fix1", Vector(-3475.426,467.388,0.031), "Fix2", Vector(-3564.508,464.401,0.031) )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)