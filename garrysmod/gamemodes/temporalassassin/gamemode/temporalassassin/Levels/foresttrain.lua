		
if CLIENT then return end

GLOB_NO_FENIC = true
GLOB_REPLACE_FENIC_GRISEUS = true

function FTRAIN_RemoveCredits()

	for _,v in pairs (ents.FindByClass("logic_relay")) do
	
		if v and IsValid(v) and v:GetName() == "endrly" then
		
			print("Removing bad logic_relay")
			v:Remove()
			
			local END_FUNC = function()
			
				local END_GAME = function()
					
					timer.Simple(2,function()
					
						local GAME_END_TXT = {
						"Suddenly, Kit felt a rather rude jerk on the back of her vest, Before she knew it",
						"she found herself in a large warehouse, safe from the sudden visitor in the rebel hideout.",
						"",
						"'Let's not mention that little slip-up to 'Her', Kit.'",
						"",
						"Nodding, Kit looked around herself.",
						"",
						"'I'm The Lieutenant, but I suppose you can call me Griseus, though I doubt former will stick.'",
						"",
						"Kit nodded again, looking to Griseus in a questioning way.",
						"",
						"'You're probably wondering where 'She' is. On her way to next assigment.",
						"'I imagine that getting some rest is in order, no?'",
						"",
						"With that, Kit was dropped off back at her apartment. Perhaps that was not the last of Griseus."}
            
						MakeGameEnd( GAME_END_TXT, 45, Vector(-504.927,-35.032,160.031), Vector(-479.546,-479.546,0), Vector(479.546,479.546,312.916) )
						
					end)
					
				end
				
				CreateTrigger( Vector(-643.643,154.923,160.031), Vector(-114.663,-114.663,0), Vector(114.663,114.663,127.938), END_GAME )
				
			end
			
			CreateTrigger( Vector(2423.042,1282.702,8.333), Vector(-388.755,-388.755,0), Vector(388.755,388.755,1136.598), END_FUNC )
		
		end
	
	end

end

function MAP_LOADED_FUNC()

	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	RemoveAllEnts("env_credits")

	CreatePlayerSpawn(Vector(-12149.041,-3457.515,512.031),Angle(0,13.438,0))
	CreateClothesLocker( Vector(-281.371,232.921,160.031), Angle(0,179.98,0) )
	
	Create_PistolWeapon( Vector(-738.324, 290.064, 160.056), Angle(0, 28.808, 0) )
	Create_PistolAmmo( Vector(-735.294, 335.491, 161.478), Angle(0, 184.576, 180) )
	Create_PistolWeapon( Vector(121.765, 734.858, 163.152), Angle(0, 67, 180) )
	Create_RevolverWeapon( Vector(-48.693, 875.034, 160.055), Angle(0, -177.36, 0) )
	Create_RevolverAmmo( Vector(-107.464, 866.898, 163.135), Angle(-90, 35.584, 45) )
	Create_Medkit25( Vector(1303.527, 1511.136, 76.357), Angle(6.276, 130.639, 9.831) )
	Create_Bread( Vector(1503.019, 6121.934, 256.411), Angle(0, -132.184, 0) )
	Create_Beer( Vector(1501.902, 6101.311, 257.453), Angle(0, -89.824, 0) )
	Create_PistolWeapon( Vector(1503.406, 6090.67, 259.965), Angle(0, 131.128, 180) )
	Create_PistolAmmo( Vector(1498.749, 6078.322, 259.815), Angle(0, -252.92, -180) )
	Create_Medkit10( Vector(1396.347, 6153.084, 260.136), Angle(-90, 0, -90) )
	Create_Armour( Vector(1380.188, 6163.048, 256.492), Angle(5.861, 133.313, 5.545) )
	Create_Bread( Vector(-94.164, 6125.92, 226.199), Angle(-1.658, 16.226, 22.266) )
	Create_Bread( Vector(-95.834, 6131.969, 224.02), Angle(2.774, -79.624, -2.178) )
	Create_SMGWeapon( Vector(1266.947, 5917.817, 436.216), Angle(-0.569, -85.657, -60.886) )
	Create_SMGAmmo( Vector(1243.779, 5914.794, 416.621), Angle(0, 335.261, 0) )
	Create_SMGAmmo( Vector(1248.874, 5928.85, 416.182), Angle(0, 319.598, 0) )
	Create_Armour5( Vector(2023.003, 40.642, 128.393), Angle(0, 128.366, 0) )
	Create_Armour5( Vector(2009.7, 26.742, 128.237), Angle(0, 116.222, 0) )
	Create_Medkit25( Vector(1182.9, 6179.884, 445.862), Angle(0, 313.909, 0) )
	Create_Medkit10( Vector(1198.345, 6161.733, 450.467), Angle(-86.102, -22.666, -88.603) )
	Create_Medkit10( Vector(1218.053, 6162.766, 448.939), Angle(-90, -89.296, 0) )
	Create_Armour100( Vector(1288.631, 6146.46, 437.429), Angle(0, 208.926, 0) )
	Create_Beer( Vector(1139.494, 5964.031, 460.933), Angle(0, 167.208, 0) )
	Create_Beer( Vector(1151.065, 5964.028, 461.045), Angle(0, -32.56, 0) )
	Create_Beer( Vector(1129.234, 5964.031, 460.964), Angle(0, 155.605, 0) )
	Create_Bread( Vector(1109.355, 5964.031, 460.975), Angle(0, 87.933, 0) )
	Create_PortableMedkit( Vector(1259.332, 6171.745, 445.957), Angle(0, 284.253, 0) )
	Create_PortableMedkit( Vector(1279.244, 6170.375, 451.119), Angle(45, 87.536, -90) )
	Create_Medkit25( Vector(1198.584, 6176.898, 416.319), Angle(0, 296.013, 0) )
	Create_Medkit25( Vector(1232.821, 6175.403, 416.558), Angle(0, 237.036, 0) )
	Create_Armour100( Vector(1253.32, 6167.685, 426.204), Angle(-5.453, -72.888, -85.824) )
	Create_RevolverAmmo( Vector(1149.985, 5965.47, 483.493), Angle(90, -88.24, -90) )
	Create_RevolverAmmo( Vector(1143.781, 5964.159, 480.994), Angle(0, 94.307, 0) )
	Create_ShotgunWeapon( Vector(1175.19, 5980.787, 435.103), Angle(-8.034, 79.633, 38.679) )
	Create_ShotgunAmmo( Vector(1125.754, 5964.031, 481.573), Angle(0, -98.272, 0) )
	Create_ShotgunAmmo( Vector(1121.14, 5964.495, 484.899), Angle(-44.776, 4.722, -0.468) )
	Create_Bread( Vector(1187.026, 5886.62, 434.881), Angle(-11.728, -90.396, -45.672) )
	Create_Beer( Vector(1194.009, 5904.942, 416.048), Angle(0, 74.688, 0) )
	Create_RifleWeapon( Vector(3569.114, 5415.297, 450.933), Angle(5.257, -20.461, 106.905) )
	Create_RifleAmmo( Vector(3530.559, 5386.643, 416.896), Angle(-0.255, -2.057, 0.082) )
	Create_RifleAmmo( Vector(3514.983, 5371.149, 416.072), Angle(0, 31.448, 0) )
	Create_RifleAmmo( Vector(3525.709, 5390.394, 418.127), Angle(-15.366, -36.793, -4.16) )
	Create_EldritchArmour( Vector(3452.607, 4925.001, 552.036), Angle(0, 85.571, 0) )
	Create_PortableMedkit( Vector(1248.144, 6172.912, 445.971), Angle(0, 253.965, 0) )
	Create_PortableMedkit( Vector(1236.107, 6167.99, 446.519), Angle(0, 263.293, 0) )
	Create_Medkit25( Vector(3206.359, 6378.013, 449.793), Angle(2.462, -174.692, -2.022) )
	Create_Medkit25( Vector(1523.553, 6366.104, 65.78), Angle(0, 188.08, 0) )
	Create_Medkit10( Vector(1523.026, 6380.977, 32.023), Angle(0, 113.176, 0) )
	Create_Armour100( Vector(1522.982, 6339.43, 32.031), Angle(0, 208.848, 0) )
	Create_Backpack( Vector(1499.858, 6380.098, 35.95), Angle(-81.857, -186.229, -79.861) )
	Create_Armour200( Vector(1221.83, 6319.86, 552.031), Angle(0, 268.508, 0) )
	Create_Medkit25( Vector(1227.284, 6288.023, 552.031), Angle(0, 370.316, 0) )
	Create_Medkit25( Vector(1202.033, 6284.815, 552.031), Angle(0, 169.94, 0) )
	Create_VolatileBattery( Vector(1188.176, 6314.817, 552.123), Angle(0, 131.308, 0) )
	Create_Backpack( Vector(1197.126, 6346.02, 556.516), Angle(-90, -90.176, -90) )
	Create_ShotgunAmmo( Vector(1417.4, 6496.245, 48.476), Angle(0, 206.797, 0) )
	Create_ShotgunAmmo( Vector(1416.116, 6498.817, 53.04), Angle(-55.434, -74.61, 3.605) )
	Create_Armour5( Vector(-302.057, -116.198, 194.082), Angle(0, 77.886, 0) )
	Create_Armour5( Vector(-301.925, -112.884, 195.964), Angle(34.8, 91.455, 0.107) )
	Create_Armour5( Vector(-719.464, -410.458, 125.153), Angle(0, 19.19, 0) )
	Create_Armour5( Vector(-731.275, -405.765, 123.783), Angle(0, 10.829, 0) )
	Create_Armour5( Vector(-601.125, 1000.576, 141.332), Angle(0, 184.454, 0) )
	Create_Armour5( Vector(-601.14, 997.208, 141.716), Angle(-2.719, 188.445, -29.559) )
	Create_Armour( Vector(1992.656, 2809.348, -159.311), Angle(13.487, -191.157, -25.205) )
	Create_PistolAmmo( Vector(1982.862, 2822.908, -154.506), Angle(11.482, -179.076, 54.291) )
	Create_PistolAmmo( Vector(1987.506, 2809.861, -138.744), Angle(-18.65, 110.847, 0.84) )
	Create_PistolWeapon( Vector(1183.475, 2734.657, 7.706), Angle(0, -92.992, -180) )
	Create_PistolWeapon( Vector(1212.273, 2710.484, 7.101), Angle(0, 182.752, 0) )
	Create_Beer( Vector(60.239, 4400.754, 207.895), Angle(0, 84.142, 0) )
	Create_Beer( Vector(51.988, 4400.665, 208.363), Angle(0, 81.238, 0) )
	Create_Beer( Vector(53.053, 4410.227, 207.845), Angle(0, 78.51, 0) )
	Create_Beer( Vector(60.831, 4409.183, 207.376), Angle(0, 92.238, 0) )
	Create_Beer( Vector(60.434, 4418.786, 207.119), Angle(0, -282.546, 0) )
	Create_Beer( Vector(52.518, 4420.114, 207.848), Angle(0, 77.893, 0) )
	Create_Medkit10( Vector(-126.987, 5590.074, 354.315), Angle(-86.24, 93.648, 95.227) )
	Create_Armour( Vector(-98.257, 5564.601, 350.782), Angle(10.208, -38.876, 0) )
	Create_RifleAmmo( Vector(3530.131, 5376.272, 418.129), Angle(14.915, 8.852, 1.747) )
	Create_RifleAmmo( Vector(3523.11, 5375.514, 417.681), Angle(12.643, 8.513, -2.363) )
	Create_Armour( Vector(553.705, 5468.069, 198.232), Angle(0, 279.501, 0) )
	Create_Armour( Vector(1057.61, 5977.857, 437.987), Angle(0, 83.805, 0) )
	Create_Armour( Vector(1055.558, 5977.648, 416.003), Angle(0, 83.63, 0) )
	Create_SuperShotgunWeapon( Vector(3233.168, 4823.368, 439.02), Angle(36.451, -9.802, -94) )
	Create_ShotgunAmmo( Vector(3251.969, 4862.032, 452.708), Angle(0, 100.672, 0) )
	Create_ShotgunAmmo( Vector(3251.969, 4862.002, 457.811), Angle(0, 111.056, 0) )
	Create_RevolverAmmo( Vector(3249.253, 4874.001, 454.789), Angle(-22.151, 66.195, 87.807) )
	Create_SMGAmmo( Vector(3110.107, 4936.114, 449.345), Angle(1.056, -10.973, 0) )
	Create_SMGWeapon( Vector(3102.624, 4924.497, 449.127), Angle(-4.91, -85.535, 0.344) )
	Create_Medkit25( Vector(2701.114, 5383.118, 9.911), Angle(0, 289.086, 0) )
	Create_Medkit25( Vector(2700.881, 5385.41, 27.004), Angle(0, 239.63, 0) )
	Create_Armour100( Vector(2605.546, 3276.438, 19.768), Angle(-90, 69.992, 45) )
	Create_Medkit25( Vector(2607.239, 1717.646, 31.847), Angle(0, 84.829, 0) )
	Create_Armour( Vector(2609.652, 1719.475, 9.733), Angle(0, 87.822, 0) )
	Create_FrontlinerWeapon( Vector(1504.609, 6485.234, 70.089), Angle(-6.2, -162.597, 0.04) )
	Create_FrontlinerAmmo( Vector(1504.595, 6426.792, 71.393), Angle(0, 174.333, 0) )
	Create_FrontlinerAmmo( Vector(1507.223, 6428.629, 32.165), Angle(0, 144.413, 0) )
	Create_FrontlinerAmmo( Vector(2610.793, 3262.505, 33.46), Angle(0, 70.317, 0) )
	Create_FrontlinerAmmo( Vector(2712.342, 5365.65, 17.324), Angle(17.51, 166.228, -68.016) )
	Create_FrontlinerAmmo( Vector(2609.315, 1717.535, 13.782), Angle(-45, -145.088, 0) )


	CreateProp(Vector(8487.281,142.75,82.938), Angle(-0.308,145.679,0.176), "models/temporalassassin/doll/unknown.mdl", true)
	CreateProp(Vector(1250.688,5923,416.375), Angle(-0.044,-93.164,0), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(2016.25,32.125,128.469), Angle(0.044,90.264,0.044), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(1228.5,6180.688,431.813), Angle(0.439,-90,0), "models/props_wasteland/cafeteria_table001a.mdl", true)
	CreateProp(Vector(1172.563,5903.594,418.094), Angle(0.703,-50.713,-0.22), "models/props_junk/garbage_newspaper001a.mdl", true)
	CreateProp(Vector(3534.781,5381.5,425.125), Angle(89.824,-118.257,67.061), "models/props_junk/plasticcrate01a.mdl", true)
	CreateProp(Vector(3453.438,4923.406,552.469), Angle(-0.044,89.956,-0.044), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(1284.938,6143.094,435.719), Angle(0.044,-146.03,-0.044), "models/props_interiors/furniture_chair03a.mdl", true)
	CreateProp(Vector(1524.094,6363.938,50.219), Angle(0,179.956,0.044), "models/props_wasteland/prison_shelf002a.mdl", true)
	CreateProp(Vector(1463.375,6360.625,557.5), Angle(-2.197,-98.306,0.088), "models/props_lab/cactus.mdl", true)
	CreateProp(Vector(1014.375,6332.781,557.469), Angle(-0.879,-74.004,-0.044), "models/props_lab/cactus.mdl", true)
	CreateProp(Vector(-602.969,996.719,141.719), Angle(1.099,168.97,-0.044), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(-728,-406.437,123.031), Angle(-1.23,12.305,-8.701), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(1997.281,2809.844,-163.5), Angle(4.263,-166.025,-16.831), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(59.938,4409.375,206.625), Angle(3.34,-5.054,-2.505), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(555.563,5465.688,202.906), Angle(4.526,-70.62,-5.933), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(1056.313,5979.719,416.469), Angle(0,3.34,0), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(1466.688,6422.219,33.188), Angle(-0.703,61.611,52.119), "models/weapons/w_crowbar.mdl", true)
	CreateProp(Vector(2700.125,5384.031,5.938), Angle(-2.021,-163.081,-1.099), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(2610.906,3265.531,9.75), Angle(0.044,90.527,-1.099), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(2607.125,1717.656,10.125), Angle(0.396,84.727,-1.187), "models/items/item_item_crate.mdl", true)

	local GR_PROP = CreateProp(Vector(1428.656,6496.438,54.625), Angle(-1.187,-3.428,55.283), "models/temporalassassin/props/corzo_cintia1901.mdl", true)
	local GR_PROP2 = CreateProp(Vector(1418.094,6499.813,52.313), Angle(-0.264,-123.794,0), "models/props_c17/furniturechair001a.mdl", true)

	local GR_B1 = function()
		Create_CorzoHat( Vector(1378.7, 6502.484, 32.031), Angle(0, 271.095, 0) )
	end

	AddSecretButton(Vector(1371.401,6514.986,88.031), GR_B1, "EVERYONE RELAX, SHE PUT A HAT ON", "TemporalAssassin/Secrets/Secret_Find2.wav", false)

	CreateSecretArea( Vector(1209.427,6312.767,556.531), Vector(-45.419,-45.419,0), Vector(45.419,45.419,38.198), "SECRET ATTIC STASH HAS BEEN FOUND" )
	CreateSecretArea( Vector(3455.715,4927.376,556.563), Vector(-19.25,-19.25,0), Vector(19.25,19.25,21.029) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

timer.Simple(5,function()

	FTRAIN_RemoveCredits()
	
end)