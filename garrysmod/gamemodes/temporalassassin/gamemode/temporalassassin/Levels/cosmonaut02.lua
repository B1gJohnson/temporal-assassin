		
if CLIENT then return end

LEVEL_STRINGS["cosmonaut02"] = {"cosmonaut03", Vector(-1090.031,4898.297,416.031), Vector(-89.991,-89.991,0), Vector(89.991,89.991,132.611)}

GLOB_NO_FENIC = true
GLOB_REPLACE_FENIC_PHOTOOP = true

function MAP_LOADED_FUNC()

	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	CreatePlayerSpawn(Vector(8598.798,2242.644,-255.969),Angle(0,-179.821,0))
	CreateClothesLocker( Vector(7882.42,1715.875,-255.969), Angle(0,359.483,0) )
	
	local AL_PROP = CreateProp(Vector(7287.781,1802.906,-19), Angle(70.972,-58.931,49.966), "models/props_debris/metal_panel02a.mdl", true)
	AL_PROP:SetRenderMode(RENDERMODE_TRANSALPHA)
	AL_PROP:SetColor( Color(255,255,255,0) )
	
    CreateProp(Vector(4160.625,278.063,824.594), Angle(0,139.614,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
    CreateProp(Vector(7248.438,1815.219,-13.937), Angle(18.018,-30.234,-10.239), "models/temporalassassin/doll/unknown.mdl", true)
	CreateProp(Vector(2042.719,-1976.437,507), Angle(-0.308,168.662,-57.041), "models/temporalassassin/doll/unknown.mdl", true)
    CreateProp(Vector(8898,2159.844,467.656), Angle(1.846,90,0), "models/props_trainstation/bench_indoor001a.mdl", true)
    CreateProp(Vector(8982.375,2186.031,482.313), Angle(-0.088,-144.229,0.132), "models/props_junk/pushcart01a.mdl", true)
    CreateProp(Vector(8975.75,2190.406,469.375), Angle(0.044,-17.578,-90.044), "models/props_c17/suitcase_passenger_physics.mdl", true)
    CreateProp(Vector(8978.594,2194.281,475.625), Angle(32.476,58.403,-90.088), "models/props_c17/suitcase_passenger_physics.mdl", true)
    CreateProp(Vector(9003.906,2201.344,476.094), Angle(0.176,125.771,0.308), "models/props_c17/suitcase001a.mdl", true)
    CreateProp(Vector(8870.656,2318.906,468.344), Angle(0.176,-90.088,-0.044), "models/props_junk/trashbin01a.mdl", true)
	CreateProp(Vector(4386.625,2053.313,-181.125), Angle(0.176,-23.906,26.631), "models/props_c17/streetsign005c.mdl", true)
	CreateProp(Vector(3911.875,-31.531,215.844), Angle(45,0,-0.044), "models/props_c17/streetsign005c.mdl", true)
	CreateProp(Vector(5897.906,2055.563,-248.219), Angle(0,158.071,0.176), "models/props_lab/jar01a.mdl", true) 
	CreateProp(Vector(5908.031,2059.313,-252.5), Angle(-29.048,-114.873,-90.22), "models/props_lab/jar01b.mdl", true)
	CreateProp(Vector(4664.781,2546.875,-184.187), Angle(0.088,40.034,-0.176), "models/props_lab/jar01a.mdl", true)
	CreateProp(Vector(4697.688,2589.344,-220.594), Angle(89.561,82.837,75.894), "models/props_lab/jar01b.mdl", true)
	CreateProp(Vector(1500.25,-2036.969,469.406), Angle(-30.103,89.956,0.132), "models/props_lab/hevplate.mdl", true)
	CreateProp(Vector(1950.563,-763.969,21.219), Angle(-8.262,88.462,0.088), "models/props_lab/hevplate.mdl", true)
	CreateProp(Vector(4406.75,1867.688,-114.312), Angle(0,0,0), "models/props_trainstation/tracksign10.mdl", true)
	
	Create_Armour( Vector(8978.968, 2315.824, 448.031), Angle(0, 202.825, 0) )
	Create_Armour( Vector(9005.517, 2292.1, 448.003), Angle(0, 92.728, 0) )
	Create_RevolverWeapon( Vector(8981.264, 2288.959, 448.02), Angle(0, -137.731, 0) )
	Create_RevolverAmmo( Vector(8962.527, 2309.675, 448.031), Angle(0, 232.344, 0) )
	Create_Armour5( Vector(8953.798, 2318.12, 450.887), Angle(0, 90, 0) )
	Create_Armour5( Vector(9011.969, 2268.88, 448.031), Angle(0, -180, 0) )
	Create_Armour5( Vector(8024.313, 2084.458, -255.969), Angle(0, 87.152, 0) )
	Create_Armour5( Vector(7999.267, 2153.579, -255.969), Angle(0, 110.255, 0) )
	Create_Armour5( Vector(8027.129, 2001.368, -255.969), Angle(0, 87.387, 0) )
	Create_Armour5( Vector(8024.992, 1924.466, -255.969), Angle(0, 89.915, 0) )
	Create_PistolWeapon( Vector(7978.195, 1570.155, -255.969), Angle(0, 124.222, 0) )
	Create_PistolAmmo( Vector(7975.995, 1604.956, -255.969), Angle(0, 91.857, 0) )
	Create_PistolAmmo( Vector(5040.485, 2202.458, -223.935), Angle(0, 109.1, 0) )
	Create_PistolAmmo( Vector(5041.115, 2201.904, -221.872), Angle(3.724, 23.73, 19.343) )
	Create_PortableMedkit( Vector(5077.629, 2193.088, -223.949), Angle(0, 104.589, 0) )
	Create_PortableMedkit( Vector(5061.049, 2200.861, -223.969), Angle(0, 93.539, 0) )
	Create_EldritchArmour10( Vector(4713.505, 2610.575, -255.818), Angle(0, 286.788, 0) )
	Create_EldritchArmour10( Vector(4729.607, 2608.82, -255.969), Angle(0, 252.725, 0) )
	Create_Beer( Vector(6065.494, 2849.365, -255.969), Angle(0, 179.88, 0) )
	Create_Bread( Vector(6060.89, 2835.858, -255.988), Angle(0, 117.148, 0) )
	Create_Beer( Vector(6045.747, 2866.097, -255.969), Angle(0, 226.472, 0) )
	Create_SMGWeapon( Vector(2280.995, 907.605, 68.008), Angle(0.225, 283.152, 0.331) )
	Create_SMGAmmo( Vector(2288.257, 876.206, 32.012), Angle(0, 125.04, 0) )
	Create_Armour100( Vector(2603.592, 915.969, 32.413), Angle(0, -90, 0) )
	Create_Medkit10( Vector(2583.788, 915.972, 32.543), Angle(0, 126.238, 0) )
	Create_Medkit10( Vector(2583.124, 901.856, 33.831), Angle(-90, -45, 0) )
	Create_M6GWeapon( Vector(1540.897, -1997.754, 500.258), Angle(0, 145.154, 0) )
	Create_ShotgunWeapon( Vector(1067.919, -3085.851, 80.133), Angle(0, 76.077, 0) )
	Create_ShotgunAmmo( Vector(1136.717, -3077.728, 80.343), Angle(0, 50.607, 0) )
	Create_PistolAmmo( Vector(1112.949, -3068.791, 80.031), Angle(0, 101.141, 0) )
	Create_PistolAmmo( Vector(1119.936, -3087.128, 80.146), Angle(0, 183.194, 0) )
	Create_Armour5( Vector(1954.568, -419.296, -127.969), Angle(0, 21.669, 0) )
	Create_Armour5( Vector(1989.074, -407.96, -127.969), Angle(0, 24.398, 0) )
	Create_Armour5( Vector(2015.234, -391.802, -127.969), Angle(0, 10.532, 0) )
	Create_Armour5( Vector(2017.281, -359.75, -127.969), Angle(0, 295.701, 0) )
	Create_Medkit25( Vector(2409.126, -535.183, 64.031), Angle(0, -131.436, 0) )
	Create_Medkit25( Vector(2330.623, -532.564, 64.031), Angle(0, -45, 0) )
	Create_RevolverWeapon( Vector(4063.066, 2248.564, 128.031), Angle(0, 171.512, 0) )
	Create_RevolverAmmo( Vector(4045.496, 2226.914, 128.031), Angle(0, 160.684, 0) )
	Create_PistolAmmo( Vector(4080.763, 1512.409, 12.027), Angle(0, 33.844, 0) )
	Create_PistolAmmo( Vector(4053.858, 1535.804, 10.14), Angle(0, 80.064, 0) )
	Create_Beer( Vector(3943.072, 1597.508, 2.374), Angle(0, 78.537, 0) )
	Create_Beer( Vector(3913.358, 1602.922, 0.917), Angle(0, 60.373, 0) )
	Create_Beer( Vector(2782.283, 69.928, 0.031), Angle(0, 357.642, 0) )
	Create_Beer( Vector(2657.017, 79.236, 0.031), Angle(0, 354.853, 0) )
	Create_Beer( Vector(2626.472, 171.296, 0.031), Angle(0, 287.032, 0) )
	Create_Beer( Vector(2582.982, 283.323, 0.031), Angle(0, 293.558, 0) )
	Create_Beer( Vector(2453.004, 330.52, 0.031), Angle(0, 335.376, 0) )
	Create_Beer( Vector(2367.337, 479.764, 0.031), Angle(0, 290.824, 0) )
	Create_Beer( Vector(2432.612, 619.189, 0.031), Angle(0, 242.838, 0) )
	Create_Medkit25( Vector(3689.332, 732.405, 128.031), Angle(0, 242.085, 0) )
	Create_Beer( Vector(2271.883, -1439.826, 64.031), Angle(0, 177.376, 0) )
	Create_Beer( Vector(2274.746, -1417.107, 64.031), Angle(0, 187.122, 0) )
	Create_PistolAmmo( Vector(1682.738, -2471.733, 64.031), Angle(0, 61.462, 0) )
	Create_PistolAmmo( Vector(1685.06, -2430.106, 64.031), Angle(0, 7.011, 0) )
	Create_PistolWeapon( Vector(1685.017, -2450.231, 64.031), Angle(0, 23.272, 0) )
	Create_M6GAmmo( Vector(452.617, -1588.989, 320.04), Angle(0, -45, 0) )
	Create_M6GAmmo( Vector(453.835, -1599.964, 320.372), Angle(0, 135, 0) )
	Create_EldritchArmour10( Vector(422.67, -1622.736, 320.031), Angle(0, 76.717, 0) )
	Create_EldritchArmour10( Vector(434.45, -1602.248, 320.031), Angle(0, 90.08, 0) )
	Create_SpiritSphere( Vector(409.472, -1573.141, 320.031), Angle(0, 93.891, 0) )
	Create_PortableMedkit( Vector(-90.205, 2730.474, 288.031), Angle(0, 305.882, 0) )
	Create_PortableMedkit( Vector(-45.662, 2735.053, 288.031), Angle(0, 272.929, 0) )
	Create_Armour100( Vector(146.937, 2509.823, 288.031), Angle(0, 90, 0) )
	Create_Armour( Vector(3688.521, 784.612, 0.006), Angle(0, 90, 0) )
	Create_Armour( Vector(3641.62, 782.61, 0.732), Angle(0, 0, 0) )
	Create_Medkit10( Vector(3741.188, 936.732, 0.031), Angle(0, 169.132, 0) )
	Create_Medkit10( Vector(3754.868, 964.683, 0.031), Angle(0, 142.366, 0) )
	Create_PistolAmmo( Vector(2617.056, 899.663, 32.031), Angle(0, 276.47, 0) )
	Create_PistolAmmo( Vector(2626.932, 916.709, 32.031), Angle(0, 347.485, 0) )
	Create_Bread( Vector(1788.155, -284.652, -127.969), Angle(0, 81.067, 0) )
	Create_Medkit10( Vector(1717.322, -2475.446, 64.031), Angle(0, 88.51, 0) )
	Create_Medkit10( Vector(1744.98, -2456.423, 64.031), Angle(0, 135.107, 0) )

	
	local AL_B2 = function()
	Create_CorzoHat( Vector(4294.331, 62.286, 128.031), Angle(0, 179.846, 0) )
	end
	
	local AL_B3 = function()
	CreateTeleporter( "AL_S1", Vector(62.279,-785.346,320.031), "AL_S1", Vector(162.312,-805.574,320.031))
	Create_Armour200( Vector(243.89, -722.534, 320.031), Angle(0, 271.382, 0) )
	Create_GrenadeAmmo( Vector(273.794, -715.951, 320.031), Angle(0, 260.691, 0) )
	Create_GrenadeAmmo( Vector(269.249, -737.9, 320.031), Angle(0, 262.313, 0) )
	end
	
	AddSecretButton(Vector(3908.596,-18.969,184.031), AL_B2)
	AddSecretButton(Vector(-31.746,-656.031,376.201), AL_B3)
	
	CreateSecretArea( Vector(8940.565,2233.787,448.031), Vector(-81.301,-81.301,0), Vector(81.301,81.301,128.318) )
	CreateSecretArea( Vector(4722.853,2602.229,-251), Vector(-17.247,-17.247,0), Vector(17.247,17.247,24.12) )
	CreateSecretArea( Vector(1500.187,-1996.01,450.824), Vector(-33.023,-33.023,0), Vector(33.023,33.023,153.962) )
	CreateSecretArea( Vector(414.155,-1477.898,320.031), Vector(-79.4,-79.4,0), Vector(79.4,79.4,98.164) )
	
    CreateTeleporter( "skip_1", Vector(1617.05,-1109.147,448.031), "skip_2", Vector(958.712,-1224.797,320.031) )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)