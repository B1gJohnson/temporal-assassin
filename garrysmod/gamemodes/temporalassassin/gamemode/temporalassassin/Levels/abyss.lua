		
if CLIENT then return end

PrecacheParticleSystem("Abyss_Beacon")
util.AddNetworkString("AbyssBeacon")
util.AddNetworkString("AbyssBeaconClear")
util.AddNetworkString("ShadowBeastMessage")
util.AddNetworkString("Abyss_StartMap")

GLOB_NO_FENIC = true
GLOB_FORCE_ROYALDECREE = true
GLOB_NO_SAVEEXIT = true
GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true
NPC_NO_DUPES = true
GLOBAL_ALWAYS_SIN = true
GLOB_TEMPORAL_CHAOS = true
HUD_DONTDRAW_POCKETWATCH = true
GLOB_NO_SWAP_NAG = true

function ABYSS_StartBeacon7()

	local B_POS = Vector(397.272,-5455.055,-511.931)

	net.Start("AbyssBeacon")
	net.WriteVector(B_POS)
	net.WriteString("Beacon_7")
	net.WriteBool(true)
	net.Broadcast()
		
	local BEACON_7_SPAWNS = function(self,ply)
	
		LEVIATHAN_TALK("we shall meet again. during the coming age.")
	
	end
	CreateTrigger( B_POS, Vector(-2500,-2500,-2500), Vector(2500,2500,2500), BEACON_7_SPAWNS )
	
	local BEACON_7_END = function(self,ply)
	
		ply:Unlock_TA_LoreFile( "abyss_1", "" )
	
		net.Start("ShadowBeastMessage")
		net.WriteString("")
		net.WriteBool(true)
		net.Broadcast()
		
		timer.Simple(3.5,function()
			net.Start("CancelContractorMessages")
			net.Broadcast()
			MALUM_TALK("Stardust...")
		end)
		
		ply:SetSharedFloat("Abyss_Progress",3)
		
		net.Start("AbyssBeacon")
		net.WriteVector(B_POS)
		net.WriteString("Beacon_7")
		net.WriteBool(false)
		net.Broadcast()
	
	end
	CreateTrigger( B_POS, Vector(-100,-100,-100), Vector(100,100,100), BEACON_7_END )

end

function ABYSS_StartBeacon6()

	local B_POS = Vector(-3970.087,7108.985,-511.931)

	net.Start("AbyssBeacon")
	net.WriteVector(B_POS)
	net.WriteString("Beacon_6")
	net.WriteBool(true)
	net.Broadcast()
		
	local BEACON_6_SPAWNS = function(self,ply)
	
		CreateNPC("npc_headcrab_black",Vector(-3974.8740234375,6950.9077148438,-511.93075561523),Angle(0,12.270904541016,0),"false",false)
		CreateNPC("npc_headcrab_black",Vector(-3861.6499023438,7012.6088867188,-511.93075561523),Angle(0,12.270904541016,0),"false",false)
		CreateNPC("npc_headcrab_black",Vector(-3822.90234375,7136.3251953125,-511.93075561523),Angle(0,6.6275177001953,0),"false",false)
		CreateNPC("npc_headcrab_black",Vector(-3876.3881835938,7270.5673828125,-511.93075561523),Angle(0,6.6275177001953,0),"false",false)
		CreateNPC("npc_headcrab_black",Vector(-4012.4516601563,7293.955078125,-511.93075561523),Angle(0,6.6275177001953,0),"false",false)
		CreateNPC("npc_headcrab_black",Vector(-4099.9672851563,7224.4311523438,-511.93075561523),Angle(0,6.6275177001953,0),"false",false)
		CreateNPC("npc_headcrab_black",Vector(-4120.515625,7094.8676757813,-511.93075561523),Angle(0,6.6275177001953,0),"false",false)
		CreateNPC("npc_headcrab_black",Vector(-4065.1389160156,6997.3032226563,-511.93075561523),Angle(0,6.6275177001953,0),"false",false)
		CreateNPC("npc_zombie",Vector(-3942.3684082031,6839.7563476563,-511.93075561523),Angle(0,7.8820953369141,0),"false",false)
		CreateNPC("npc_zombie",Vector(-3756.19140625,6975.4501953125,-511.93075561523),Angle(0,7.8820953369141,0),"false",false)
		CreateNPC("npc_zombie",Vector(-3706.9440917969,7184.8876953125,-511.93075561523),Angle(0,18.541809082031,0),"false",false)

		timer.Simple(1,function()
			LEVIATHAN_TALK("he will reassure you he will be fine. this information is false.")
		end)
	
	end
	CreateTrigger( B_POS, Vector(-2500,-2500,-2500), Vector(2500,2500,2500), BEACON_6_SPAWNS )
	
	local BEACON_6_END = function(self,ply)
	
		net.Start("ShadowBeastMessage")
		net.WriteString("I NEED TO FIND HER. SHE IS THE ONLY THING THAT MATTERS. MY STARDUST.")
		net.WriteBool(true)
		net.Broadcast()
		
		net.Start("AbyssBeacon")
		net.WriteVector(B_POS)
		net.WriteString("Beacon_6")
		net.WriteBool(false)
		net.Broadcast()
		
		ABYSS_StartBeacon7()
	
	end
	CreateTrigger( B_POS, Vector(-100,-100,-100), Vector(100,100,100), BEACON_6_END )

end

function ABYSS_StartBeacon5()

	local B_POS = Vector(5039.617,6595.598,-511.931)

	net.Start("AbyssBeacon")
	net.WriteVector(B_POS)
	net.WriteString("Beacon_5")
	net.WriteBool(true)
	net.Broadcast()
		
	local BEACON_5_SPAWNS = function(self,ply)
	
		CreateNPC("npc_fastzombie",Vector(4905.21484375,6756.9604492188,-511.93075561523),Angle(0,215.6083984375,0),"false",false)
		CreateNPC("npc_fastzombie",Vector(4870.6552734375,6513.529296875,-511.93075561523),Angle(0,215.6083984375,0),"false",false)
		CreateNPC("npc_fastzombie",Vector(5021.1533203125,6308.5043945313,-511.93075561523),Angle(0,215.6083984375,0),"false",false)
		CreateNPC("npc_fastzombie",Vector(4902.6430664063,6370.3950195313,-511.93075561523),Angle(0,215.6083984375,0),"false",false)
		CreateNPC("npc_fastzombie",Vector(4847.0200195313,6660.8959960938,-511.93075561523),Angle(0,215.6083984375,0),"false",false)
		CreateNPC("npc_antlion",Vector(4885.9208984375,7008.0014648438,-511.93075561523),Angle(0,204.1134185791,0),"false",false)
		CreateNPC("npc_antlion",Vector(4709.95703125,6826.1791992188,-511.93075561523),Angle(0,204.1134185791,0),"false",false)
		CreateNPC("npc_antlion",Vector(4655.28515625,6484.3266601563,-511.93075561523),Angle(0,204.1134185791,0),"false",false)
		CreateNPC("npc_antlion",Vector(4817.7001953125,6211.6967773438,-511.93075561523),Angle(0,204.1134185791,0),"false",false)

		timer.Simple(1,function()
			LEVIATHAN_TALK("her presence has changed him. without her he does not know.")
		end)
	
	end
	CreateTrigger( B_POS, Vector(-2500,-2500,-2500), Vector(2500,2500,2500), BEACON_5_SPAWNS )
	
	local BEACON_5_END = function(self,ply)
	
		net.Start("ShadowBeastMessage")
		net.WriteString("GET OUT OF MY HEAD. I NEVER WANTED THIS.")
		net.WriteBool(true)
		net.Broadcast()
	
		net.Start("ShadowBeastMessage")
		net.WriteString("GET OUT GET OUT GET OUT GET OUT GET OUT GET OUT")
		net.WriteBool(true)
		net.Broadcast()
		
		TAS:GiveArmour2(ply,50)
		
		net.Start("AbyssBeacon")
		net.WriteVector(B_POS)
		net.WriteString("Beacon_5")
		net.WriteBool(false)
		net.Broadcast()
		
		ABYSS_StartBeacon6()
	
	end
	CreateTrigger( B_POS, Vector(-100,-100,-100), Vector(100,100,100), BEACON_5_END )

end

function ABYSS_StartBeacon4()

	local B_POS = Vector(-2233.514,1521.598,-511.931)

	net.Start("AbyssBeacon")
	net.WriteVector(B_POS)
	net.WriteString("Beacon_4")
	net.WriteBool(true)
	net.Broadcast()
		
	local BEACON_4_SPAWNS = function(self,ply)
	
		CreateNPC("npc_antlionguard",Vector(-2498.5378417969,1399.0270996094,-511.93075561523),Angle(0,299.73141479492,0),"false",false)
		CreateNPC("npc_antlionguard",Vector(-1897.7947998047,1609.2801513672,-511.93075561523),Angle(0,291.99841308594,0),"false",false)
		CreateNPC("npc_antlion",Vector(-2273.83203125,1304.8857421875,-511.93075561523),Angle(0,306.41961669922,0),"false",false)
		CreateNPC("npc_antlion",Vector(-1982.0329589844,1439.1015625,-511.93075561523),Angle(0,293.67065429688,0),"false",false)
		CreateNPC("npc_antlion",Vector(-2133.462890625,1353.9055175781,-511.93075561523),Angle(0,293.67065429688,0),"false",false)
		CreateNPC("npc_antlion",Vector(-2417.759765625,1557.8479003906,-511.93075561523),Angle(0,293.67065429688,0),"false",false)
		CreateNPC("npc_antlion",Vector(-2259.4125976563,1694.1276855469,-511.93075561523),Angle(0,293.67065429688,0),"false",false)
		CreateNPC("npc_antlion",Vector(-2070.8181152344,1647.443359375,-511.93075561523),Angle(0,293.67065429688,0),"false",false)
		
		timer.Simple(1,function()
			LEVIATHAN_TALK("he is right. the claw called to her.")
		end)
	
	end
	CreateTrigger( B_POS, Vector(-2500,-2500,-2500), Vector(2500,2500,2500), BEACON_4_SPAWNS )
	
	local BEACON_4_END = function(self,ply)
	
		MALUM_TALK("I think i know where she is, she has to be there, i need her to be there.")
		
		net.Start("ShadowBeastMessage")
		net.WriteString("I AM GOING TO BECOME A MONSTER. I CAN FEEL IT.")
		net.WriteBool(true)
		net.Broadcast()
		
		MALUM_TALK("Just gotta hold on...",2)
		
		TAS:GiveArmour2(ply,50)
		
		net.Start("AbyssBeacon")
		net.WriteVector(B_POS)
		net.WriteString("Beacon_4")
		net.WriteBool(false)
		net.Broadcast()
		
		ply:SetSharedFloat("Abyss_Progress",2)
		
		ABYSS_StartBeacon5()
	
	end
	CreateTrigger( B_POS, Vector(-100,-100,-100), Vector(100,100,100), BEACON_4_END )

end

function ABYSS_StartBeacon3()

	local B_POS = Vector(-4788.534,-8197.998,-511.931)

	net.Start("AbyssBeacon")
	net.WriteVector(B_POS)
	net.WriteString("Beacon_3")
	net.WriteBool(true)
	net.Broadcast()
		
	local BEACON_3_SPAWNS = function(self,ply)
	
		CreateNPC("npc_headcrab_fast",Vector(-4785.8452148438,-8573.755859375,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_headcrab_fast",Vector(-4502.4404296875,-8385.89453125,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_headcrab_fast",Vector(-4426.6733398438,-8070.59765625,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_headcrab_fast",Vector(-4711.85546875,-7850.4301757813,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_headcrab_fast",Vector(-4973.99609375,-7937.1977539063,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_headcrab_fast",Vector(-5156.828125,-8164.8188476563,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_headcrab_fast",Vector(-4974.287109375,-8388.783203125,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_headcrab_fast",Vector(-4579.8452148438,-7938.1450195313,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_fastzombie",Vector(-4276.4077148438,-8227.6171875,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_fastzombie",Vector(-4504.4833984375,-8573.138671875,-511.93075561523),Angle(0,54.055297851563,0),"false",false)
		CreateNPC("npc_fastzombie",Vector(-4965.6875,-8580.896484375,-511.93075561523),Angle(0,57.399337768555,0),"false",false)
		CreateNPC("npc_zombie",Vector(-4798.1967773438,-8393.451171875,-511.93075561523),Angle(0,48.203231811523,0),"false",false)
		CreateNPC("npc_zombie",Vector(-4596.0356445313,-8253.03515625,-511.93075561523),Angle(0,48.203231811523,0),"false",false)
		CreateNPC("npc_zombie",Vector(-4595.5126953125,-8090.19921875,-511.93075561523),Angle(0,52.383232116699,0),"false",false)
		CreateNPC("npc_zombie",Vector(-4741.2934570313,-7961.029296875,-511.93075561523),Angle(0,56.98120880127,0),"false",false)
		CreateNPC("npc_zombie",Vector(-4931.7719726563,-8036.7841796875,-511.93075561523),Angle(0,53.637214660645,0),"false",false)
		CreateNPC("npc_zombie",Vector(-4976.8818359375,-8188.24609375,-511.93075561523),Angle(0,53.637214660645,0),"false",false)

		timer.Simple(1,function()
			LEVIATHAN_TALK("a part of her is attached to him. all because of that claw.")
		end)
	
	end
	CreateTrigger( B_POS, Vector(-2500,-2500,-2500), Vector(2500,2500,2500), BEACON_3_SPAWNS )
	
	local BEACON_3_END = function(self,ply)
	
		MALUM_TALK("She can't be far right? Did she leave because of the claw?")
		MALUM_TALK("Was dagon calling to her? Wait, i think...",2)
		
		TAS:GiveArmour2(ply,50)
		
		net.Start("AbyssBeacon")
		net.WriteVector(B_POS)
		net.WriteString("Beacon_3")
		net.WriteBool(false)
		net.Broadcast()
		
		ABYSS_StartBeacon4()
	
	end
	CreateTrigger( B_POS, Vector(-100,-100,-100), Vector(100,100,100), BEACON_3_END )

end

function ABYSS_StartBeacon2()

	local B_POS = Vector(9047.659,-10501.703,-511.931)

	net.Start("AbyssBeacon")
	net.WriteVector(B_POS)
	net.WriteString("Beacon_2")
	net.WriteBool(true)
	net.Broadcast()
		
	local BEACON_2_SPAWNS = function(self,ply)
	
		CreateNPC("npc_antlion",Vector(8476.9208984375,-10573.875,-511.93075561523),Angle(0,73.492202758789,0),"false",false)
		CreateNPC("npc_antlion",Vector(8712.6650390625,-10499.993164063,-511.93075561523),Angle(0,66.59521484375,0),"false",false)
		CreateNPC("npc_antlion",Vector(9275.3486328125,-10020.500976563,-511.93075561523),Angle(0,111.59521484375,0),"false",false)
		CreateNPC("npc_antlion",Vector(9036.5048828125,-9942.4638671875,-511.93075561523),Angle(0,111.59521484375,0),"false",false)
		CreateNPC("npc_antlion",Vector(8642.4208984375,-10074.99609375,-511.93075561523),Angle(0,66.59521484375,0),"false",false)
		CreateNPC("npc_antlion",Vector(8774.03515625,-10704.865234375,-511.93075561523),Angle(0,66.59521484375,0),"false",false)
		CreateNPC("npc_antlion",Vector(8995.2041015625,-10886.758789063,-511.93075561523),Angle(0,66.59521484375,0),"false",false)
		CreateNPC("npc_antlionguard",Vector(9763.462890625,-10021.415039063,-511.93075561523),Angle(0,111.59521484375,0),"false",false)

		timer.Simple(1,function()
			LEVIATHAN_TALK("his mind unsettled. he is slowly changing from her voice.")
		end)
	
	end
	CreateTrigger( B_POS, Vector(-2500,-2500,-2500), Vector(2500,2500,2500), BEACON_2_SPAWNS )
	
	local BEACON_2_END = function(self,ply)
	
		MALUM_TALK("Why can't i find her, she can't have gone that far")
		MALUM_TALK("Why? Why so sudden...",2)
		
		TAS:GiveArmour2(ply,50)
		
		net.Start("AbyssBeacon")
		net.WriteVector(B_POS)
		net.WriteString("Beacon_2")
		net.WriteBool(false)
		net.Broadcast()
		
		ply:SetSharedFloat("Abyss_Progress",1)
		
		ABYSS_StartBeacon3()
	
	end
	CreateTrigger( B_POS, Vector(-100,-100,-100), Vector(100,100,100), BEACON_2_END )

end

function Abyss_StartMap_LIB( len, pl )
	
	local PLAYER = player.GetAll()[1]
	
	if not PLAYER:HasLoreFile("weapon_outcast_1") then
		timer.Simple(5,function() LEVIATHAN_TALK("you are not supposed to be here. young one.") end)
	return end
	
	if PLAYER:HasLoreFile("abyss_1") then
		timer.Simple(5,function() LEVIATHAN_TALK("you are not supposed to be here. young one.") end)
	return end
	
	if PLAYER:HasLoreFile("weapon_royaldecree_1") then
		timer.Simple(5,function() LEVIATHAN_TALK("you are not supposed to be here. young one.") end)
	return end

	timer.Simple(2,function()
		PLAYER:SetBackpack(true)
		PLAYER:GiveAmmo2(500,"cruentus_ammo")
	end)

	if not GLOB_MAP_STARTED then
	
		GLOB_MAP_STARTED = true
		
		net.Start("AbyssBeacon")
		net.WriteVector(Vector(5472.768,-5598.037,-511.931))
		net.WriteString("Beacon_1")
		net.WriteBool(true)
		net.Broadcast()
		
		local BEACON_1_SPAWNS = function(self,ply)
		
			CreateNPC("npc_zombie",Vector(6454.509765625,-5461.478515625,-511.93075561523),Angle(0,164.49230957031,0),"false",false)
			CreateNPC("npc_zombie",Vector(6062.2397460938,-5343.279296875,-511.93075561523),Angle(0,119.49230194092,0),"false",false)
			CreateNPC("npc_zombie",Vector(5998.78125,-5875.3251953125,-511.93075561523),Angle(0,119.49230194092,0),"false",false)
			CreateNPC("npc_zombie",Vector(5515.1845703125,-5919.08984375,-511.93075561523),Angle(0,74.492301940918,0),"false",false)
			CreateNPC("npc_zombie",Vector(5697.47265625,-6261.634765625,-511.93075561523),Angle(0,74.492301940918,0),"false",false)
			CreateNPC("npc_zombie",Vector(6442.9018554688,-5800.80078125,-511.93075561523),Angle(0,119.46067810059,0),"false",false)
			CreateNPC("npc_fastzombie",Vector(4881.732421875,-5773.4287109375,-511.93075561523),Angle(0,74.492301940918,0),"false",false)
			CreateNPC("npc_fastzombie",Vector(4854.3930664063,-5406.46484375,-511.93075561523),Angle(0,30.34245300293,0),"false",false)
			CreateNPC("npc_fastzombie",Vector(6254.3642578125,-5677.7231445313,-511.93075561523),Angle(0,74.492309570313,0),"false",false)
			CreateNPC("npc_fastzombie",Vector(6131.1459960938,-6151.205078125,-511.93075561523),Angle(0,74.492309570313,0),"false",false)
			
			timer.Simple(1,function()
				LEVIATHAN_TALK("your mother is elsewhere. the shadow will listen.")
			end)
		
		end
		CreateTrigger( Vector(5472.768,-5598.037,-511.931), Vector(-2500,-2500,-2500), Vector(2500,2500,2500), BEACON_1_SPAWNS )
		
		local BEACON_1_END = function(self,ply)
		
			MALUM_TALK("She's been missing for a while, why do i feel so different.")
			
			net.Start("AbyssBeacon")
			net.WriteVector(Vector(5472.768,-5598.037,-511.931))
			net.WriteString("Beacon_1")
			net.WriteBool(false)
			net.Broadcast()
			
			ABYSS_StartBeacon2()
		
		end
		CreateTrigger( Vector(5472.768,-5598.037,-511.931), Vector(-100,-100,-100), Vector(100,100,100), BEACON_1_END )
		
	end

end
net.Receive("Abyss_StartMap",Abyss_StartMap_LIB)

function ABYSS_PlayerDeath( ply )

	net.Start("AbyssBeaconClear")
	net.Broadcast()
	
	ply:SetSharedBool("Abyss_Progress",false)

end
hook.Add("DoPlayerDeath","ABYSS_PlayerDeathHook",ABYSS_PlayerDeath)

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	CreatePlayerSpawn(Vector(-24.828,-385.357,-511.931),Angle(0,120,0))
	
	Create_SuperShotgunWeapon( Vector(-24.75,-385.25,-511), Angle(0,0,0) )
	
	if GLOB_MAP_STARTED then
		GLOB_MAP_STARTED = false
		Abyss_StartMap_LIB()
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

timer.Create("FuckOff_NoHax", 1, 0, function()

	if GetConVarNumber("ta_sv_skill") < 12 then
		RunConsoleCommand("ta_sv_skill",12)
	end
	
	if GetConVarNumber("ta_dev_developermode") < 1 then
	
		local PLAYERS = player.GetAll()
		
		for _,v in pairs (PLAYERS) do
		
			v:SetMoveType(MOVETYPE_WALK)
			TAS:TakeTemporalMana( v, 500 )
			TAS:TakeTemporalBurst( v, 500 )
			
			for xx,mm in pairs (v:GetWeapons()) do
				if IsValid(mm) and mm.TA_MaxAmmunition and not mm.AMMO_MOD then
					mm.AMMO_MOD = true
					mm.TA_MaxAmmunition = mm.TA_MaxAmmunition*5
					mm:SetSharedFloat("TA_MaxAmmunition",mm.TA_MaxAmmunition)
					mm:SetSharedBool("AMMO_MOD",true)
				end
			end
		
			if TAS:GetSuit(v) == 5 then
			
				local SUIT = 0
			
				if SUIT then
				
					v.Assassin_CurrentSuit = SUIT
					v:SetSharedFloat("Assassin_CurrentSuit",SUIT)
					
					if SUIT == 16 then
						v:SetMaxHealth(100)
					else
						v:SetMaxHealth(150)
					end
					
				end
			
				v:ApplyPlayerMaterials()
				
			end
		
			v:GodDisable()
			v.GOD_ON = false
		
		end
		
	end

end)