
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	CreatePlayerSpawn( Vector(607,2839,-95.96875), Angle(0,35,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_MegaSphere( Vector(540.90814208984, 3715.6970214844, 26.22777557373), Angle(0, 51.428924560547, 0) )
		Create_PistolAmmo( Vector(910.29614257813, 5779.6333007813, -95.96875), Angle(0, 155.92845153809, 0) )
		Create_PistolAmmo( Vector(916.35101318359, 5847.9907226563, -95.96875), Angle(0, 195.52844238281, 0) )
		Create_Beer( Vector(2469.005859375, 6065.8422851563, -159.96875), Angle(0, 183.42837524414, 0) )
		Create_Beer( Vector(2467.9213867188, 6051.1870117188, -159.96875), Angle(0, 174.84837341309, 0) )
		Create_Beer( Vector(2468.1604003906, 6083.439453125, -159.96875), Angle(0, 193.54837036133, 0) )
		Create_Beer( Vector(2450.1044921875, 6072.4282226563, -159.96875), Angle(0, 188.92837524414, 0) )
		Create_Medkit10( Vector(2473.4038085938, 6006.71875, -159.96875), Angle(0, 170.44834899902, 0) )
		Create_PistolAmmo( Vector(-2159.91015625, 4477.873046875, -43.52486038208), Angle(0, 354.83520507813, 0) )
		Create_PistolAmmo( Vector(-1958.4812011719, 4476.5932617188, -43.258319854736), Angle(0, 354.83520507813, 0) )
		Create_Medkit25( Vector(-4948.783203125, 9315.8212890625, 55.031253814697), Angle(0, 325.21533203125, 0) )

		CreateSecretArea( Vector(538.50622558594,3713.6997070313,26.22777557373), Vector(-39.226295471191,-39.226295471191,0), Vector(39.226295471191,39.226295471191,77.347129821777) )
		
	else
	
		Create_MegaSphere( Vector(707.436, -1224.366, -157.189), Angle(0, 90.039, 0) )
		Create_Medkit25( Vector(924.31, 5803.797, -95.969), Angle(0, 179.281, 0) )
		Create_Medkit10( Vector(922.914, 5858.387, -95.969), Angle(0, 181.58, 0) )
		Create_Beer( Vector(1582.089, 6293.478, -95.969), Angle(0, 217.094, 0) )
		Create_Beer( Vector(1590.29, 6124.591, -95.969), Angle(0, 149.587, 0) )
		Create_Bread( Vector(1596.567, 6211.526, -95.969), Angle(0, 193.059, 0) )
		Create_Medkit10( Vector(2462.643, 6018.711, -159.969), Angle(0, 156.961, 0) )
		Create_Medkit10( Vector(2454.338, 6043.64, -159.969), Angle(0, 187.684, 0) )
		Create_Beer( Vector(2532.929, 6046.156, -159.969), Angle(0, 162.813, 0) )
		Create_Beer( Vector(2526.733, 6068.991, -159.969), Angle(0, 180.369, 0) )
		Create_Beer( Vector(2505.333, 6045.289, -159.969), Angle(0, 152.572, 0) )
		Create_Medkit25( Vector(2162.425, 6240.455, 32.031), Angle(0, 344.745, 0) )
		Create_Armour( Vector(2516.901, 6270.444, -159.969), Angle(0, 160.305, 0) )
		Create_Beer( Vector(-895.891, 6280.614, -65.192), Angle(0, 120.757, 0) )
		Create_Beer( Vector(-806.524, 6090.609, -83.005), Angle(0, 115.741, 0) )
		Create_Armour5( Vector(-1276.917, 6352.6, -58.443), Angle(0, 0.581, 0) )
		Create_Armour5( Vector(-1175.254, 6351.308, -58.564), Angle(0, 1.626, 0) )
		Create_Armour5( Vector(-1070.14, 6350.444, -58.645), Angle(0, 4.97, 0) )
		Create_SMGAmmo( Vector(-1583.822, 4745.585, -44.598), Angle(0, 95.861, 0) )
		Create_SMGAmmo( Vector(-1612.259, 4791.828, -45.414), Angle(0, 79.35, 0) )
		Create_Medkit10( Vector(-1531.395, 4573.864, -127.969), Angle(0, 182.503, 0) )
		Create_SMGWeapon( Vector(-1488.595, 4379.992, -108.57), Angle(8.042, -45.302, -5.54) )
		Create_Armour5( Vector(-3060.504, 5692.795, -95.969), Angle(0, 246.15, 0) )
		Create_Armour5( Vector(-3096.248, 5664.457, -95.969), Angle(0, 274.469, 0) )
		Create_Beer( Vector(-3046.615, 5946.849, -95.969), Angle(0, 227.653, 0) )
		Create_PistolAmmo( Vector(-3172.316, 6152.012, -89.278), Angle(0, 326.929, 0) )
		Create_Medkit25( Vector(-2190.026, 6208.264, 0.031), Angle(0, 180.243, 0) )
		Create_PistolAmmo( Vector(-3124.827, 6243.202, -78.461), Angle(2.296, 83.787, -11.23) )
		Create_GrenadeAmmo( Vector(-2945.185, 7964.973, -15.969), Angle(0, 249.619, 0) )
		Create_Medkit25( Vector(-4325.757, 9190.987, 53.575), Angle(0, 5.567, 0) )
		Create_Medkit25( Vector(-4957.802, 9313.003, 55.031), Angle(0, 332.962, 0) )
		Create_Medkit10( Vector(-4654.048, 8977.215, 39.304), Angle(0, 197.637, 0) )
		Create_Medkit10( Vector(-4648.355, 8958.015, 40.836), Angle(0, 186.351, 0) )
		
		CreateSecretArea( Vector(702.726,-1186.81,-156.854), Vector(-180.018,-180.018,0), Vector(180.018,180.018,129.343) )
		CreateSecretArea( Vector(-1476.704,4370.964,-104.031), Vector(-25.139,-25.139,0), Vector(25.139,25.139,3.536) )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)