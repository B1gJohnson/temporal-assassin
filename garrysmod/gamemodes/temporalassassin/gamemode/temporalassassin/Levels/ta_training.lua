
if CLIENT then return end

TRAINING_LOADED = false

GLOB_CONTRACTOR_B = true
NPC_NO_DUPES = true
GLOB_NO_SINS = true

TRAINING_FORMATION = 1

PrecacheParticleSystem("B_Idle")
PrecacheParticleSystem("Training_Portal")
PrecacheParticleSystem("Training_StarFragment")

function Inside_B_BanterBox( ply )

	if ply:GetPos():Distance( Vector(752.42,-513.327,-495.811) ) <= 200 then
		return true
	end
	
	return false

end

function Training_NoKill( ply )

	ply:TakeDamage(500)
	
	return false

end
hook.Add("CanPlayerSuicide","Training_NoKillHook",Training_NoKill)

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("info_player_deathmatch")
	RemoveAllEnts("info_player_combine")
	RemoveAllEnts("info_player_rebel")
	RemoveAllEnts("prop_dynamic")
	RemoveAllEnts("weapon_")
	
	RunConsoleCommand("ai_disabled",1)
	
	CreatePlayerSpawn(Vector(672.586,-651.793,-495.969),Angle(0,60,0))
	
	CreateClothesLocker( Vector(640.567,-384.206,-495.969), Angle(0,314.782,0) )
	CreateEquipmentLocker( Vector(923.499,-412.757,-495.969), Angle(0,214.594,0) )
	
	CreateClothesLocker( Vector(-1359.524,-577.838,-511.969), Angle(0,44.86,0) )
	CreateEquipmentLocker( Vector(-1401.868,-437.844,-511.969), Angle(0,10.035,0) )
	
	TRAINING_LOADED = true
	
	GLOB_TRAINER = CreateProp(Vector(752.531,-518.594,-497.5), Angle(-0.044,-140.098,-0.044), "models/temporalassassin/training/b.mdl", true)
	GLOB_TRAINER:SetCollisionGroup(COLLISION_GROUP_WEAPON)
	
	local PATCHPROP_1 = CreateProp(Vector(-1105.281,-8.219,-519.562), Angle(0,-0.044,0), "models/props_phx/construct/wood/wood_panel2x2.mdl", true)
	local PATCHPROP_2 = CreateProp(Vector(-810.031,148.188,-519.562), Angle(0,-90.044,0), "models/props_phx/construct/wood/wood_panel2x2.mdl", true)
	PATCHPROP_1:SetHealth(9999999)
	PATCHPROP_2:SetHealth(9999999)
	PATCHPROP_1:SetColor(Color(187,187,187,233))
	PATCHPROP_2:SetColor(Color(187,187,187,233))
	
	local BACK_PROP = CreateProp(Vector(-3325.375,1,-520.656), Angle(89.956,-0.044,180), "models/props_lab/blastdoor001c.mdl", true)

	timer.Create("B_BanterStuff", 30, 0, function()

		for _,v in pairs (player.GetAll()) do
		
			if v and IsValid(v) and Inside_B_BanterBox(v) then
				net.Start("Training_SendBanter")
				net.Send(v)
			end
		
		end

	end)
	
	GLOB_GUARD_TRAINING = CreateProp(Vector(-31.562,1215.906,-187.625), Angle(-0.044,-90,0), "models/props_c17/oildrum001_explosive.mdl", true)
	
	local GUARD_TRAINING_ON = function()
	
		if GLOB_GUARD_TRAINING and IsValid(GLOB_GUARD_TRAINING) then
			GLOB_GUARD_TRAINING:Ignite(10)
			GLOB_GUARD_TRAINING:SetHealth(10)
		end
		
	end
	
	CreateTrigger( Vector(-31.718,1216.04,-187.969), Vector(-72.372,-72.372,0), Vector(72.372,72.372,88.388), GUARD_TRAINING_ON, true )
	
	local COSMO_START = function( self, ply )
	
		if not ply:HasLoreFile("pet_unlocked") and not ply:HasLoreFile("pet_acquired") then
		
			local FUNC = function( self, ply )
	
				if IsValid(ply) then

					if not ply:Alive() then return end
					
					local TXT = "Some kind of... Egg?"
					local SND = Sound("TemporalAssassin/Gore/Flesh_Gloop6.wav")	
					
					net.Start("TEMPORAL_AddLog")
					net.WriteString(TXT)
					net.Send(ply)
					ply:SetSharedFloat("Item_ScreenFlash",CurTime() + 0.15)
					ply:EmitSound( SND, 80, math.random(98,102), 0.7, CHAN_BODY )
					self:Remove()
					
					ply:Unlock_TA_LoreFile( "pet_acquired", "" )
					
					timer.Simple(2,function()
						net.Start("BMessage")
						net.WriteString("Find something interesting? I'd be careful trying to take anything out of here.")
						net.Broadcast()
						
						net.Start("BMessage")
						net.WriteString("We carved this place from an existing dimension so it isn't exactly stable.")
						net.Broadcast()
					end)
					
				end
			
			end
			
			local COSMO_ITEM = CreateItemPickup( Vector(701.862,-473.009,782.031), Angle(0, 0, 0), "models/temporalassassin/hub/pet_egg.mdl", FUNC )
			timer.Simple(0.2,function()
				if COSMO_ITEM and IsValid(COSMO_ITEM) then
					COSMO_ITEM:SetModelScale(0.4,0)
					COSMO_ITEM:SetCollisionBounds( Vector(-20,-20,0), Vector(20,20,40) )
				end
			end)
		
		end
	
	end
	
	CreateTrigger( Vector(767.969,-512.448,697.239), Vector(-1872.478,-1872.478,0), Vector(1872.478,1872.478,740.522), COSMO_START )
	
	local CHECK_WHITESPACE_START = function(self,ply)
	
		--[[ If the player has white space lore, spawn shit in ]]
		if ply:HasLoreFile("WHITESPACE_STARTED") and not ply:HasLoreFile("WHITESPACE_GOTFRAGMENT") and not ply:HasLoreFile("WHITESPACE_GOTGUN") then
		
			local FUCKER = CreateParticleEffect( "Training_StarFragment", Vector(-934.414,2.382,-760.324+50), Angle(0,0,0), 999999 )
		
			local FUNC = function( self, ply )
	
				if IsValid(ply) then

					if not ply:Alive() then return end
					
					local TXT = "I got some kind of fragment of... Something."
					local SND = Sound("TemporalAssassin/Maps/Fragment_Pickup.wav")	
					
					net.Start("TEMPORAL_AddLog")
					net.WriteString(TXT)
					net.Send(ply)
					ply:SetSharedFloat("Item_ScreenFlash",CurTime() + 0.15)
					ply:EmitSound( SND, 100, math.random(98,102), 1, CHAN_BODY )
					self:Remove()
					
					ply:Unlock_TA_LoreFile( "WHITESPACE_GOTFRAGMENT", "" )
					
					timer.Simple(2,function()
						B_TALK("What are you doing here, Fox? It's not training time yet.")
						B_TALK("You're acting a bit odd, maybe go do a contract or two and let off some stress.",2)
						B_TALK("Or you could just go lay down for a bit and actually get some rest.",2)
					end)
					
					FUCKER:Remove()
					
				end
			
			end
			
			local FRAGMENT_ITEM = CreateItemPickup( Vector(-934.414,2.382,-760.324), Angle(0, 0, 0), "models/Combine_Helicopter/helicopter_bomb01.mdl", FUNC )
			timer.Simple(0,function()
				if FRAGMENT_ITEM and IsValid(FRAGMENT_ITEM) then
					FRAGMENT_ITEM:SetNoDraw(true)
					FRAGMENT_ITEM:SetCollisionBounds( Vector(-20,-20,0), Vector(20,20,40) )
				end
			end)
		
		end
	
	end
	
	CreateTrigger( Vector(-1141.795,1.532,-779.534), Vector(-1079.274,-1079.274,0), Vector(1079.274,1079.274,1332.382), CHECK_WHITESPACE_START )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

function TRAINING_PlayerHurt( ply, att, hpl, dmg )

	if ply and IsValid(ply) and hpl <= 1 then
			
		hpl = 1
		ply:SetHealth(1)
		
	end

end
hook.Add("PlayerHurt","TRAINING_PlayerHurtHook",TRAINING_PlayerHurt)

function TRAINING_EntityRemoved( ent )

	if ent and IsValid(ent) and GLOB_GUARD_TRAINING and GLOB_GUARD_TRAINING == ent then
	
		timer.Simple(0.1,function()
			GLOB_GUARD_TRAINING = CreateProp(Vector(-31.562,1215.906,-187.625), Angle(-0.044,-90,0), "models/props_c17/oildrum001_explosive.mdl", true)
		end)
		
	end

end
hook.Add("EntityRemoved","TRAINING_EntityRemovedHook",TRAINING_EntityRemoved)

local DUMMY_POSITIONS = {}

DUMMY_POSITIONS[1] = {
Vector(-1133.956299, -230.242554, -515.968750),
Vector(-1477.880371, 258.384338, -515.968750),
Vector(-892.101990, 92.855385, -515.968750),
Vector(-1954.822266, -108.568680, -515.968750),
Vector(-2535.880859, 197.140976, -515.968750),
Vector(-3369.306,1.395,-516.235)}

DUMMY_POSITIONS[2] = {
Vector(-1069.935,147.219,-515.969),
Vector(-1063.86,-37.711,-515.969),
Vector(-1056.777,-253.367,-515.969),
Vector(-1280.801,-79.968,-515.969),
Vector(-1284.344,114.058,-515.969),
Vector(-1287.557,290.062,-515.969)}

DUMMY_POSITIONS[3] = {
Vector(-2437.948,-0.892,-515.969),
Vector(-2186.331,-1.34,-515.969),
Vector(-1930.924,-1.794,-515.969),
Vector(-1480.161,-2.595,-515.254),
Vector(-1217.591,-3.061,-515.969),
Vector(-996.201,-3.455,-515.969)}

DUMMY_POSITIONS[4] = {
Vector(-998.728,1.193,-515.969),
Vector(-998.728,-81.033,-515.969),
Vector(-998.728,63.679,-515.969),
Vector(-1093.599,65.085,-515.969),
Vector(-1093.599,-8.513,-515.969),
Vector(-1093.599,-90.766,-515.969)}

DUMMY_POSITIONS[5] = {
Vector(-1088.699,349.304,-515.969),
Vector(-1088.699,212.141,-515.969),
Vector(-1088.699,68.957,-515.969),
Vector(-1088.699,-86.212,-515.969),
Vector(-1088.699,-214.767,-515.969),
Vector(-1088.699,-335.572,-515.969)}

local DUMMY_BONUS_POS = {}
DUMMY_BONUS_POS["npc_manhack"] = Vector(0,0,40)
DUMMY_BONUS_POS["monster_alien_controller"] = Vector(0,0,25)

local DUMMY_ENTS = {}

local DUMMY_CLASS = "npc_combine_s"
local DUMMY_WEP = "weapon_smg1"

local SEQUENCE_B = {}
SEQUENCE_B["npc_combine_s"] = 7
SEQUENCE_B["npc_heavycombine"] = 5
SEQUENCE_B["npc_metropolice"] = 9
SEQUENCE_B["npc_zombine"] = 2
SEQUENCE_B["npc_alien_slave"] = 2
SEQUENCE_B["npc_headcrab_poison"] = 3
SEQUENCE_B["npc_headcrab_black"] = 3
SEQUENCE_B["monster_bullchicken"] = 8
SEQUENCE_B["monster_houndeye"] = 2
SEQUENCE_B["monster_human_grunt"] = 12
SEQUENCE_B["monster_human_assassin"] = 2

function TrainingDummy_Stuff()

	if not TRAINING_LOADED then return end

	local SPAWN_ANG = Angle(0,0,0)
	
	for loop = 1,6 do
	
		if (not DUMMY_ENTS[loop] or (DUMMY_ENTS[loop] and not IsValid(DUMMY_ENTS[loop]))) and CreateSnapNPC then
		
			local POS_BONUS = Vector(0,0,0)
			
			if DUMMY_BONUS_POS and DUMMY_BONUS_POS[DUMMY_CLASS] then
				POS_BONUS = DUMMY_BONUS_POS[DUMMY_CLASS]
			end
			
			local POSITION_TABLE = DUMMY_POSITIONS[TRAINING_FORMATION]
			
			DUMMY_ENTS[loop] = CreateSnapNPC( DUMMY_CLASS, POSITION_TABLE[loop] + POS_BONUS, SPAWN_ANG, (DUMMY_WEP or false), false )
			
			if TRAINING_SIN then
				DUMMY_ENTS[loop]:CreateSinner(true)
			end
			
		elseif DUMMY_ENTS[loop] and IsValid(DUMMY_ENTS[loop]) then
		
			local CLASS = DUMMY_ENTS[loop]:GetClass()
			
			if DUMMY_ENTS[loop].Tank_Combine then
				CLASS = "npc_heavycombine"
			end
			
			if GetConVarNumber("ai_disabled") >= 1 and SEQUENCE_B and SEQUENCE_B[CLASS] then
				DUMMY_ENTS[loop]:SetSequence( SEQUENCE_B[CLASS] )
			end
			
		end
	
	end

end
timer.Create("TrainingDummy_Respawner", 0.5, 0, TrainingDummy_Stuff)

function TrainingDummy_CleanUp()

	if not TRAINING_LOADED then return end

	local RAG_MAX = GetConVarNumber("g_ragdoll_maxcount")
	
	RunConsoleCommand("g_ragdoll_maxcount",0)
	
	timer.Simple(0.1,function()
		RunConsoleCommand("g_ragdoll_maxcount",RAG_MAX)
	end)

end
timer.Create("TrainingDummy_CleanUpTimer", 10, 0, TrainingDummy_CleanUp)

function TrainingDummy_ResetDummys()

	for loop = 1,6 do
	
		if DUMMY_ENTS[loop] and IsValid(DUMMY_ENTS[loop]) then
		
			DUMMY_ENTS[loop]:Remove()
			DUMMY_ENTS[loop] = false
		
		end
	
	end

end

util.AddNetworkString("Training_ShotDamage")
util.AddNetworkString("TRAINING_RefillMana")
util.AddNetworkString("TRAINING_RefillBurst")
util.AddNetworkString("TRAINING_RefillSpiritEye")
util.AddNetworkString("TRAINING_ResetPocketWatch")
util.AddNetworkString("TRAINING_ResetSummons")
util.AddNetworkString("TRAINING_SetTrainingEnemies")
util.AddNetworkString("TRAINING_EnemyInfiniteHealth")
util.AddNetworkString("TRAINING_EnemyInfiniteHealth_CL")
util.AddNetworkString("Training_SendBanter")
util.AddNetworkString("TRAINING_ToggleShadeCloak")
util.AddNetworkString("TRAINING_ToggleVolatileBattery")
util.AddNetworkString("TRAINING_ToggleSinEnemies")
util.AddNetworkString("TRAINING_SinEnemies_CL")
util.AddNetworkString("TRAINING_ChangeEnemyFormation")
util.AddNetworkString("TRAINING_RaikiriSwap")
util.AddNetworkString("TRAINING_CosmicRealm")
util.AddNetworkString("TRAINING_FalanranAwaken")
util.AddNetworkString("Training_ReiterBuff")
util.AddNetworkString("Training_SpawnItem")

function Training_SpawnItem_LIB( len, pl )

	GLOB_NOITEMDOUBLES = true

	if TRAINING_ITEM and IsValid(TRAINING_ITEM) then
		TRAINING_ITEM:Remove()
		TRAINING_ITEM = nil
	end
	
	local ITEM = net.ReadString()
	local FUNC = _G[ITEM]
	TRAINING_ITEM = FUNC( Vector(-912.799,-555.341,-511.963), Angle(0,90,0) )

end
net.Receive("Training_SpawnItem",Training_SpawnItem_LIB)

function Training_ReiterBuff_LIB( len, pl )

	if not pl:HasWeapon("temporal_reiterpallasch") then return end
	
	local WEP = pl:GetWeapon("temporal_reiterpallasch")
	
	if WEP.SAVE_VAR1 == 0 then
		WEP:SetSharedFloat("SAVE_VAR1",2)
	elseif WEP.SAVE_VAR1 == 2 then
		WEP:SetSharedFloat("SAVE_VAR1",0)
	end

end
net.Receive("Training_ReiterBuff",Training_ReiterBuff_LIB)

function TRAINING_FalanranAwaken_LIB( len, pl )

	if not pl.Falanran_SuperBuff then
		pl:SetSharedBool("Falanran_SuperBuff",true)
	else
		pl:SetSharedBool("Falanran_SuperBuff",false)
	end
	
end
net.Receive("TRAINING_FalanranAwaken",TRAINING_FalanranAwaken_LIB)

function TRAINING_CosmicRealm_LIB( len, pl )

	if not TRAINING_COSMIC_REALM then
		TRAINING_COSMIC_REALM = true
		for _,v in pairs (player.GetAll()) do
			v:SetSharedFloat("CosmicRealm_Counter",1)
		end
	elseif TRAINING_COSMIC_REALM then
		TRAINING_COSMIC_REALM = false
		for _,v in pairs (player.GetAll()) do
			v:SetSharedFloat("CosmicRealm_Counter",0)
		end
	end

end
net.Receive("TRAINING_CosmicRealm",TRAINING_CosmicRealm_LIB)

function TRAINING_RaikiriSwap_LIB( len, pl )

	if pl:HasWeapon("temporal_raikiri") then
		pl:Give("temporal_sword")
		pl:StripWeapon("temporal_raikiri")
		pl:SelectWeapon("temporal_sword")
	else
		pl:Give("temporal_raikiri")
		pl:StripWeapon("temporal_sword")
		pl:SelectWeapon("temporal_raikiri")
	end

end
net.Receive("TRAINING_RaikiriSwap",TRAINING_RaikiriSwap_LIB)

function TRAINING_ChangeEnemyFormation_LIB( len, pl )

	TRAINING_FORMATION = TRAINING_FORMATION + 1
	
	if TRAINING_FORMATION > 5 then
		TRAINING_FORMATION = 1
	end

	TrainingDummy_ResetDummys()

end
net.Receive("TRAINING_ChangeEnemyFormation",TRAINING_ChangeEnemyFormation_LIB)

function TRAINING_ToggleSinEnemies_LIB( len, pl )

	if TRAINING_SIN then
		TRAINING_SIN = false
	else
		TRAINING_SIN = true
	end
	
	net.Start("TRAINING_SinEnemies_CL")
	net.WriteBool(TRAINING_SIN)
	net.Broadcast()
	
	TrainingDummy_ResetDummys()

end
net.Receive("TRAINING_ToggleSinEnemies",TRAINING_ToggleSinEnemies_LIB)

function TRAINING_ToggleShadeCloak_LIB( len, pl )

	if pl.ITEM_SHADECLOAK then
		pl:SetShadeCloak(false)
	else
		pl:SetShadeCloak(true)
	end

end
net.Receive("TRAINING_ToggleShadeCloak",TRAINING_ToggleShadeCloak_LIB)

function TRAINING_ToggleVolatileBattery_LIB( len, pl )

	if pl.ARCBlade_PowerUp then
		pl:SetSharedBool("ARCBlade_PowerUp",false)
	else
		pl:SetSharedBool("ARCBlade_PowerUp",true)
	end

end
net.Receive("TRAINING_ToggleVolatileBattery",TRAINING_ToggleVolatileBattery_LIB)

function TRAINING_RefillMana_LIB( len, pl )
	TAS:GiveTemporalMana( pl, 5000 )
	pl:EmitSound( Sound("TemporalAssassin/Items/ManaCrystal100.wav"), 80, 100, 0.8, CHAN_BODY )
end
net.Receive("TRAINING_RefillMana",TRAINING_RefillMana_LIB)

function TRAINING_RefillBurst_LIB( len, pl )
	TAS:GiveTemporalBurst( pl, 5000 )
	TAS:GiveTemporalHeat( pl, 5000 )
	pl:EmitSound( Sound("TemporalAssassin/Items/ManaCrystal.wav"), 80, 100, 0.8, CHAN_BODY )
end
net.Receive("TRAINING_RefillBurst",TRAINING_RefillBurst_LIB)

function TRAINING_RefillSpiritEye_LIB( len, pl )
	TAS:GiveSpiritEye( pl, 500 )
end
net.Receive("TRAINING_RefillSpiritEye",TRAINING_RefillSpiritEye_LIB)

function TRAINING_ResetPocketWatch_LIB( len, pl )
	TAS:SetPocketWatch( pl, 0 )
end
net.Receive("TRAINING_ResetPocketWatch",TRAINING_ResetPocketWatch_LIB)

function TRAINING_ResetSummons_LIB( len, pl )
	pl:SetSummonCooldown(1,0)
	pl:SetSummonCooldown(2,0)
	pl:SetSummonCooldown(3,0)
	pl:SetSummonCooldown(4,0)
end
net.Receive("TRAINING_ResetSummons",TRAINING_ResetSummons_LIB)

function TRAINING_SetTrainingEnemies_LIB( len, pl )

	local CLASS = net.ReadString()
	local WEP = net.ReadString()
	
	DUMMY_CLASS = CLASS
	
	if WEP == "" then
		DUMMY_WEP = false
	else
		DUMMY_WEP = WEP
	end
	
	TrainingDummy_ResetDummys()

end
net.Receive("TRAINING_SetTrainingEnemies",TRAINING_SetTrainingEnemies_LIB)

GLOB_INFINITE_HEALTH = false

function TRAINING_EnemyInfiniteHealth_LIB( len, pl )

	if GLOB_INFINITE_HEALTH then
		GLOB_INFINITE_HEALTH = false
	else
		GLOB_INFINITE_HEALTH = true
	end
	
	net.Start("TRAINING_EnemyInfiniteHealth_CL")
	net.WriteBool(GLOB_INFINITE_HEALTH)
	net.Broadcast()

end
net.Receive("TRAINING_EnemyInfiniteHealth",TRAINING_EnemyInfiniteHealth_LIB)

function FixTrainingTrash_C( ply, com, arg )

	hook.Remove("EntityTakeDamage","Training_EntityTakeDamageHook")
	hook.Remove("EntityRemoved","TRAINING_EntityRemovedHook")
	hook.Remove("PlayerHurt","TRAINING_PlayerHurtHook")
	hook.Remove("InitPostEntity","MAP_LOADED_FUNCHOOK")
	timer.Remove("TrainingDummy_CleanUpTimer")
	timer.Remove("TrainingDummy_Respawner")

end
concommand.Add("FixTrainingTrash",FixTrainingTrash_C)

function RedoTrainingTrash_C( ply, com, arg )

	hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)
	hook.Add("CanPlayerSuicide","Training_NoKillHook",Training_NoKill)
	hook.Add("PlayerHurt","TRAINING_PlayerHurtHook",TRAINING_PlayerHurt)
	hook.Add("EntityRemoved","TRAINING_EntityRemovedHook",TRAINING_EntityRemoved)
	timer.Create("TrainingDummy_Respawner", 0.5, 0, TrainingDummy_Stuff)
	timer.Create("TrainingDummy_CleanUpTimer", 10, 0, TrainingDummy_CleanUp)

end
concommand.Add("RedoTrainingTrash",RedoTrainingTrash_C)