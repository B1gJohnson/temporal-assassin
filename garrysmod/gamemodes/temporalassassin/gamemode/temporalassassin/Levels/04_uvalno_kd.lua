		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["04_uvalno_kd"] = {"05_uvalno_roadway", Vector(9706.211,-2965.424,-48.612), Vector(-382.343,-382.343,0), Vector(382.343,382.343,222.189)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-5093.36,-6289.841,214.98), Angle(0,87.149,0))

	local VEH = ents.Create("prop_vehicle_jeep")
	VEH:SetModel("models/vehicle.mdl")
	VEH:SetKeyValue( "vehiclescript", "scripts/vehicles/jalopy.txt" )
	VEH:SetPos( Vector(-4959.031,-6153.344,204.375) )
	VEH:SetAngles( Angle(-0.967,1.099,0.527) )
	VEH:Spawn()
	VEH:Activate()
	
	for _,v in pairs (ents.FindByClass("npc_metropolice")) do
	
		if v and IsValid(v) then
			v:AddRelationship("player D_LI 99")
			v:AddRelationship("npc_citizen D_LI 99")
		end
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

function NoShooty()

	for _,v in pairs (player.GetAll()) do
		
		if v then
			v:SetSharedBool("Safety_Zone",true)
		end
		
	end
	
end
timer.Create("NoShooty_ThisMap", 1, 0, NoShooty)

function SAFE_NPCS( ent )

	timer.Simple(0.1,function()
	
		if ent and IsValid(ent) then
		
			if ent:GetClass() == "npc_metropolice" then
				ent:AddRelationship("player D_LI 99")
				ent:AddRelationship("npc_citizen D_LI 99")
			end
		
		end
	
	end)

end
hook.Add("OnEntityCreated","SAFE_NPCSHook",SAFE_NPCS)