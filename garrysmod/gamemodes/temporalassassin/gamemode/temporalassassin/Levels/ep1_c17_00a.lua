		
if CLIENT then return end

LEVEL_STRINGS["ep1_c17_00a"] = {"ep1_c17_01",Vector(4599.1396484375,3579.9929199219,431.12628173828), Vector(-55.543891906738,-55.543891906738,0), Vector(55.543891906738,55.543891906738,191.48876953125)}

function EXTRA_FAIL_STATE( npc, att, inf )

	if IsValid(npc) and ( npc:GetClass() == "npc_alyx" ) then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRA_FAIL_STATEHook",EXTRA_FAIL_STATE)

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("trigger_changelevel")
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Armour( Vector(1122.1513671875, 4590.9428710938, 372.03125), Angle(0, 249.81950378418, 0) )
		Create_Beer( Vector(1072.5432128906, 4538.6669921875, 372.03125), Angle(0, 269.83959960938, 0) )
		Create_Beer( Vector(1072.4879150391, 4518.919921875, 372.03125), Angle(0, 269.83959960938, 0) )
		Create_Beer( Vector(1072.3934326172, 4485.1572265625, 372.03125), Angle(0, 269.83959960938, 0) )
		Create_Beer( Vector(1072.2969970703, 4450.8583984375, 372.03125), Angle(0, 269.83959960938, 0) )
		Create_Beer( Vector(1072.2120361328, 4420.4995117188, 372.03125), Angle(0, 269.83959960938, 0) )
		Create_Beer( Vector(1072.1142578125, 4385.451171875, 372.03125), Angle(0, 269.83959960938, 0) )
		Create_PistolAmmo( Vector(1122.8665771484, 4537.3256835938, 372.03125), Angle(0, 266.97961425781, 0) )
		Create_PistolAmmo( Vector(1124.1000976563, 4478.521484375, 372.03125), Angle(0, 264.99960327148, 0) )
		Create_PistolAmmo( Vector(1124.4934082031, 4440.4970703125, 372.03125), Angle(0, 262.57958984375, 0) )
		Create_PistolAmmo( Vector(1123.1513671875, 4396.2685546875, 372.03125), Angle(0, 256.85955810547, 0) )
		Create_ShotgunAmmo( Vector(156.18965148926, 4204.1254882813, 380.03125), Angle(0, 263.89953613281, 0) )
		Create_ShotgunAmmo( Vector(15.536514282227, 4222.1157226563, 380.03125), Angle(0, 289.19961547852, 0) )
		Create_SuperShotgunWeapon( Vector(84.277877807617, 4205.263671875, 380.03125), Angle(0, 269.83966064453, 0) )
		Create_Armour100( Vector(81.595901489258, 4258.8803710938, 380.03125), Angle(0, 268.51962280273, 0) )
		Create_Medkit25( Vector(-639.91265869141, 3912.294921875, 372.03125), Angle(0, 293.81942749023, 0) )
		Create_Medkit25( Vector(-539.07843017578, 3911.3278808594, 372.03125), Angle(0, 247.3994140625, 0) )
		Create_PortableMedkit( Vector(-514.64483642578, 3799.6875, 372.03125), Angle(0, 184.25939941406, 0) )
		Create_Medkit10( Vector(-606.48693847656, 3917.0961914063, 372.03125), Angle(0, 278.63940429688, 0) )
		Create_Medkit10( Vector(-577.35729980469, 3919.3393554688, 372.03125), Angle(0, 265.21936035156, 0) )
		Create_RevolverAmmo( Vector(-638.04223632813, 3922.4543457031, 500.03125), Angle(0, 330.77954101563, 0) )
		Create_RevolverAmmo( Vector(-658.03723144531, 3911.8552246094, 500.03125), Angle(0, 339.57958984375, 0) )
		Create_ManaCrystal( Vector(-534.04821777344, 3930.1662597656, 500.03125), Angle(0, 283.69946289063, 0) )
		Create_ManaCrystal( Vector(-591.60101318359, 3933.169921875, 500.03125), Angle(0, 294.25939941406, 0) )
		Create_PistolWeapon( Vector(-638.24468994141, 3881.9455566406, 500.03125), Angle(0, 338.69952392578, 0) )
		Create_SMGAmmo( Vector(1029.5704345703, 3582.0710449219, 628.03125), Angle(0, 119.58029174805, 0) )
		Create_SMGAmmo( Vector(984.34826660156, 3592.80859375, 628.03125), Angle(0, 88.010139465332, 0) )
		Create_SMGAmmo( Vector(1035.1245117188, 3632.5930175781, 628.03125), Angle(0, 152.58016967773, 0) )
		Create_PortableMedkit( Vector(1114.861328125, 4146.0673828125, 513.30401611328), Angle(0, 342.73999023438, 0) )
		Create_Medkit10( Vector(1098.4544677734, 4115.1157226563, 510.64996337891), Angle(0, 1.0000762939453, 0) )
		Create_Medkit10( Vector(1128.3734130859, 4115.6376953125, 515.48980712891), Angle(0, 1.0000762939453, 0) )
		Create_Medkit25( Vector(276.62991333008, 3844.3872070313, 628.03125), Angle(0, 358.40313720703, 0) )
		Create_Medkit25( Vector(280.98037719727, 3283.0573730469, 628.03125), Angle(0, 89.48323059082, 0) )
		Create_Beer( Vector(594.22760009766, 3451.8974609375, 490.11083984375), Angle(0, 178.00315856934, 0) )
		Create_Beer( Vector(525.50396728516, 3451.8786621094, 488.07260131836), Angle(0, 181.08316040039, 0) )
		Create_Beer( Vector(445.74377441406, 3451.9831542969, 488.07260131836), Angle(0, 179.10316467285, 0) )
		Create_Beer( Vector(334.56228637695, 3452.052734375, 490.90634155273), Angle(0, 182.18316650391, 0) )
		Create_Beer( Vector(208.78117370605, 3451.0148925781, 514.46899414063), Angle(0, 178.00315856934, 0) )
		Create_Beer( Vector(87.10147857666, 3450.814453125, 563.91522216797), Angle(0, 179.76316833496, 0) )
		Create_Beer( Vector(-51.664176940918, 3450.9226074219, 610.53540039063), Angle(0, 177.78315734863, 0) )
		Create_KingSlayerWeapon( Vector(1561.1889648438, 4010.3283691406, 684.51086425781), Angle(0, 176.11868286133, 0) )
		Create_KingSlayerAmmo( Vector(1553.0883789063, 4051.3857421875, 686.68145751953), Angle(0, 205.15870666504, 0) )
		Create_KingSlayerAmmo( Vector(1541.1440429688, 3976.7319335938, 688.88189697266), Angle(0, 120.89863586426, 0) )
		Create_ShotgunWeapon( Vector(1203.0915527344, 4310.5981445313, 308.03125), Angle(0, 306.88922119141, 0) )
		Create_ShotgunAmmo( Vector(1197.9156494141, 4275.9716796875, 308.03125), Angle(0, 327.78930664063, 0) )
		Create_ShotgunAmmo( Vector(1220.1458740234, 4294.2685546875, 308.03125), Angle(0, 303.58917236328, 0) )
		Create_ShotgunAmmo( Vector(1245.2960205078, 4302.8012695313, 308.03125), Angle(0, 280.37908935547, 0) )
		Create_Medkit25( Vector(1990.7745361328, 4271.41796875, 372.03125), Angle(0, 72.395195007324, 0) )
		Create_PortableMedkit( Vector(2038.2619628906, 4276.0522460938, 372.03125), Angle(0, 104.07530212402, 0) )
		Create_KingSlayerAmmo( Vector(5164.5009765625, 3325.6965332031, 408.03125), Angle(0, 201.14706420898, 0) )
		Create_KingSlayerAmmo( Vector(5171.2407226563, 3288.4526367188, 408.03125), Angle(0, 179.36706542969, 0) )
		Create_Beer( Vector(5134.7524414063, 3313.3498535156, 408.03125), Angle(0, 200.48707580566, 0) )
		Create_Beer( Vector(5141.4096679688, 3296.6374511719, 408.03125), Angle(0, 185.7470703125, 0) )
		Create_Beer( Vector(5141.1225585938, 3276.5354003906, 408.03125), Angle(0, 169.46708679199, 0) )
		Create_Armour( Vector(3106.1276855469, 3139.6682128906, 408.03125), Angle(0, 93.566886901855, 0) )
		Create_PistolAmmo( Vector(4471.4731445313, 3646.8791503906, 408.03125), Angle(0, 183.06300354004, 0) )
		Create_PistolAmmo( Vector(4424.7626953125, 3645.2897949219, 408.03125), Angle(0, 183.72300720215, 0) )
		Create_PistolAmmo( Vector(4424.478515625, 3612.3212890625, 408.03125), Angle(0, 181.5230255127, 0) )
		Create_PistolAmmo( Vector(4424.0107421875, 3579.8579101563, 408.03125), Angle(0, 179.10301208496, 0) )
		Create_PistolAmmo( Vector(4426.4375, 3544.6772460938, 408.03125), Angle(0, 174.04302978516, 0) )
		Create_PistolAmmo( Vector(4423.4208984375, 3504.5524902344, 408.03125), Angle(0, 149.18299865723, 0) )
		Create_PistolAmmo( Vector(4473.2485351563, 3506.173828125, 408.03125), Angle(0, 161.94300842285, 0) )
		Create_Backpack( Vector(3899.92578125, 3342.05859375, 542.03125), Angle(0, 199.95802307129, 0) )
		Create_VolatileBattery( Vector(3896.6159667969, 3294.7897949219, 542.03125), Angle(0, 172.01802062988, 0) )
		Create_SpiritSphere( Vector(3832.6940917969, 3334.7976074219, 542.03125), Angle(0, 202.59802246094, 0) )
		
		CreateSecretArea( Vector(1523.0643310547,4006.4291992188,688.72625732422), Vector(-39.297481536865,-39.297481536865,0), Vector(39.297481536865,39.297481536865,63.288696289063) )
		CreateSecretArea( Vector(3800.9743652344,3241.3229980469,543.03125), Vector(-108.55475616455,-108.55475616455,0), Vector(108.55475616455,108.55475616455,82.050354003906) )
		
	else
	
		local P1 = CreateProp(Vector(942.188,3546.25,662.813), Angle(0.879,134.956,0.571), "models/props_interiors/pot01a.mdl", true)
		local P2 = CreateProp(Vector(894.594,3545.813,662.563), Angle(0.439,-133.066,0), "models/props_interiors/pot01a.mdl", true)
		P1:SetPhysicsWeight(5000)
		P2:SetPhysicsWeight(5000)
		
		Create_VolatileBattery( Vector(1132.1, 4601.521, 372.031), Angle(0, 234.543, 0) )
		Create_Armour( Vector(1100.837, 4599.323, 372.031), Angle(0, 254.607, 0) )
		Create_Medkit25( Vector(1072.378, 4575.636, 372.031), Angle(0, 282.404, 0) )
		Create_Medkit25( Vector(1077.786, 4540.585, 372.031), Angle(0, 282.195, 0) )
		Create_Armour100( Vector(689.218, 4328.263, 372.031), Angle(0, 0.972, 0) )
		Create_Medkit25( Vector(644.441, 4284.371, 376.031), Angle(0, 344.042, 0) )
		Create_Medkit25( Vector(689.256, 4269.271, 372.031), Angle(0, 48.415, 0) )
		Create_Beer( Vector(205.675, 3800.922, 378.953+2), Angle(0, 355.955, 0) )
		Create_Beer( Vector(93.65, 3814.427, 427.364+2), Angle(0, 352.82, 0) )
		Create_Beer( Vector(-19.451, 3830.901, 476.24+2), Angle(0, 352.193, 0) )
		Create_Beer( Vector(-104.211, 3839.524, 499.144+2), Angle(0, 354.701, 0) )
		Create_ManaCrystal( Vector(-495.556, 3527.135, 508.031), Angle(0, 352.61, 0) )
		Create_ManaCrystal( Vector(-636.978, 3709.362, 500.031), Angle(0, 269.22, 0) )
		Create_ManaCrystal( Vector(-626.92, 3924.709, 500.031), Angle(0, 314.364, 0) )
		Create_ManaCrystal( Vector(-555.637, 3931.764, 500.031), Angle(0, 277.998, 0) )
		Create_ManaCrystal( Vector(-594.113, 3891.972, 500.031), Angle(0, 313.946, 0) )
		Create_Armour5( Vector(549.517, 3452.306, 488.514+2), Angle(0, 0.911, 0) )
		Create_Armour5( Vector(450.319, 3450.773, 488.073+2), Angle(0, 0.911, 0) )
		Create_Armour5( Vector(345.607, 3452.657, 490.584+2), Angle(0, 359.448, 0) )
		Create_Armour5( Vector(278.053, 3452.408, 492.031+2), Angle(0, 0.075, 0) )
		Create_Armour5( Vector(138.578, 3450.575, 542.997+2), Angle(0, 358.821, 0) )
		Create_Armour5( Vector(29.814, 3452.423, 587.194+2), Angle(0, 359.239, 0) )
		Create_Armour5( Vector(-128.644, 3449.85, 628.031+2), Angle(0, 1.329, 0) )
		Create_SpiritSphere( Vector(276.804, 3832.172, 628.031), Angle(0, 270.832, 0) )
		Create_Medkit25( Vector(661.235, 4174.852, 628.031), Angle(0, 290.06, 0) )
		Create_Armour( Vector(723.294, 4180.287, 628.031), Angle(0, 238.228, 0) )
		Create_Beer( Vector(1122.936, 4156.366, 514.61), Angle(0, 340.169, 0) )
		Create_Beer( Vector(1119.438, 4125.484, 514.044), Angle(0, 5.249, 0) )
		Create_Beer( Vector(1151.479, 4139.922, 519.228), Angle(0, 348.528, 0) )
		Create_PistolAmmo( Vector(1021.372, 4597.832, 374.503), Angle(0, 235.358, 0) )
		Create_PistolAmmo( Vector(1014.418, 4570.653, 375.298), Angle(0, 228.252, 0) )
		Create_ShotgunAmmo( Vector(987.28, 4602.188, 376.031), Angle(0, 254.377, 0) )
		Create_ShotgunAmmo( Vector(983.711, 4575.419, 376.031), Angle(0, 139.427, 0) )
		Create_GrenadeAmmo( Vector(949.796, 4605.223, 376.031), Angle(0, 263.991, 0) )
		Create_GrenadeAmmo( Vector(947.712, 4573.235, 376.031), Angle(0, 262.319, 0) )
		Create_GrenadeAmmo( Vector(926.822, 4589.992, 376.031), Angle(0, 284.055, 0) )
		Create_SMGWeapon( Vector(715.219, 4585.736, 500.031), Angle(0, 277.447, 0) )
		Create_SMGAmmo( Vector(694.411, 4556.98, 500.031), Angle(0, 295.838, 0) )
		Create_SMGAmmo( Vector(743.43, 4562.552, 500.031), Angle(0, 258.428, 0) )
		Create_Medkit25( Vector(1034.962, 3177.872, 500.031), Angle(0, 166.678, 0) )
		Create_Medkit25( Vector(1015.609, 3092.5, 500.031), Angle(0, 172.112, 0) )
		Create_RevolverAmmo( Vector(1002.869, 3159.034, 500.031), Angle(0, 169.186, 0) )
		Create_RevolverAmmo( Vector(987.461, 3129.526, 500.031), Angle(0, 149.749, 0) )
		Create_Armour5( Vector(1222.068, 4158.351, 630.271), Angle(0, 213.266, 0) )
		Create_Armour5( Vector(1227.485, 4118.779, 630.178), Angle(0, 190.276, 0) )
		Create_PortableMedkit( Vector(1218.163, 4243.946, 628.031), Angle(0, 107.512, 0) )
		Create_Medkit25( Vector(1178.926, 4250.636, 628.031), Angle(0, 79.924, 0) )
		Create_CorzoHat( Vector(1212.489, 4300.377, 308.031), Angle(0, 305.321, 0) )
		Create_Medkit10( Vector(1657.249, 4504.528, 372.031), Angle(0, 192.366, 0) )
		Create_RevolverAmmo( Vector(1708.189, 4178.289, 372.031), Angle(0, 136.354, 0) )
		Create_SMGAmmo( Vector(1992.306, 4524.483, 372.031), Angle(0, 148.476, 0) )
		Create_SMGAmmo( Vector(1988.919, 4551.402, 372.031), Angle(0, 174.601, 0) )
		Create_ShotgunAmmo( Vector(1973.103, 4509.086, 372.031), Angle(0, 87.97, 0) )
		Create_ShotgunWeapon( Vector(2753.874, 4144.778, 408.031), Angle(0, 89.194, 0) )
		Create_WhisperWeapon( Vector(3167.129, 3576.656, 683.031), Angle(0, 357.651, 0) )
		Create_WhisperAmmo( Vector(3243.333, 3518.79, 672.031), Angle(0, 20.642, 0) )
		Create_WhisperAmmo( Vector(3242.523, 3634.789, 672.031), Angle(0, 345.111, 0) )
		Create_MegaSphere( Vector(3883.144, 3329.805, 542.031), Angle(0, 224.401, 0) )
		Create_GrenadeAmmo( Vector(3820.453, 3337.591, 542.031), Angle(0, 255.751, 0) )
		Create_GrenadeAmmo( Vector(3889.533, 3273.452, 542.031), Angle(0, 196.813, 0) )
		Create_GrenadeAmmo( Vector(3845.33, 3292.774, 542.031), Angle(0, 224.401, 0) )
		Create_ShotgunAmmo( Vector(3893.072, 3148.918, 408.031), Angle(0, 91.166, 0) )
		Create_ShotgunAmmo( Vector(3858.049, 3159.74, 408.031), Angle(0, 66.922, 0) )
		Create_SMGAmmo( Vector(3898.285, 3185.246, 408.031), Angle(0, 97.645, 0) )
		Create_RevolverWeapon( Vector(3856.788, 3192.918, 408.031), Angle(0, 52.397, 0) )
		Create_RevolverAmmo( Vector(3814.494, 3190.053, 408.031), Angle(0, 31.81, 0) )
		Create_PistolAmmo( Vector(3824.696, 3222.041, 408.031), Angle(0, 13.209, 0) )
		Create_PistolAmmo( Vector(3842.071, 3243.392, 408.031), Angle(0, 353.354, 0) )
		Create_PistolAmmo( Vector(3874.617, 3221.043, 408.031), Angle(0, 44.977, 0) )
	
		local SBUTTON1 = function()
			Create_Backpack( Vector(1038.661, 3666.591, 628.031), Angle(0, 183.209, 0) )
			Create_KingSlayerWeapon( Vector(1002.67, 3666.893, 628.031), Angle(0, 181.328, 0) )
			Create_KingSlayerAmmo( Vector(1045.321, 3706.232, 628.031), Angle(0, 174.222, 0) )
			Create_EldritchArmour10( Vector(1043.437, 3634.966, 628.031), Angle(0, 200.138, 0) )
			Create_EldritchArmour10( Vector(1027.285, 3611.104, 628.031), Angle(0, 180.283, 0) )
			Create_EldritchArmour10( Vector(995.534, 3613.948, 628.031), Angle(0, 183, 0) )
			Create_CrossbowAmmo( Vector(1004.222, 3725.593, 628.031), Angle(0, 167.325, 0) )
			Create_CrossbowWeapon( Vector(956.386, 3666.8, 628.031), Angle(0, 184.254, 0) )
		end
	
		AddSecretButton( Vector(919.234,3546.031,652.949), SBUTTON1 )
		CreateSecretArea( Vector(1216.339,4295.647,308.031), Vector(-33.215,-33.215,0), Vector(33.215,33.215,48.234) )
		CreateSecretArea( Vector(3194.156,3578.857,676.585), Vector(-119.606,-119.606,0), Vector(119.606,119.606,99.273) )
		CreateSecretArea( Vector(3859.952,3319.057,546.563), Vector(-50.844,-50.844,0), Vector(50.844,50.844,67.396) )
	
	end
	
	for _,v in pairs (ents.GetAll()) do
	
		if v and IsValid(v) then

			if v.GetModel and v:IsProp() and string.find(v:GetModel(),"concrete_spawnchunk") then
				v:SetCollisionGroup(COLLISION_GROUP_WEAPON)
			end
			
			if v.GetClass and v:GetClass() == "momentary_rot_button" then
				v:SetKeyValue("speed",50)
			end
			
		end
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)