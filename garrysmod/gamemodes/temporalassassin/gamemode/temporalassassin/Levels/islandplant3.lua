		
if CLIENT then return end

LEVEL_STRINGS["islandplant3"] = { "islandplant4", Vector(907.928,4789.42,208.031), Vector(-97.499,-97.499,0), Vector(97.499,97.499,125.566) }

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(5796.751,219.38,410.031),Angle(0,0,0))
	
	Create_ManaCrystal100( Vector(6979.737, 746.488, 408.031), Angle(0, 174.324, 0) )
	Create_Medkit25( Vector(6863.663, 844.693, 408.031), Angle(0, 267.824, 0) )
	Create_Medkit25( Vector(6707.403, 839.957, 408.031), Angle(0, 269.803, 0) )
	Create_Armour100( Vector(6659.312, 613.394, 408.031), Angle(0, 166.104, 0) )
	Create_SuperShotgunWeapon( Vector(6612.544, 822.265, 408.031), Angle(0, 269.504, 0) )
	Create_SMGWeapon( Vector(6780.558, 819.324, 408.031), Angle(0, 268.844, 0) )
	Create_CrossbowWeapon( Vector(6946.866, 817.852, 408.031), Angle(0, 267.964, 0) )
	Create_Medkit10( Vector(6623.657, 916.593, 336.031), Angle(0, 96.724, 0) )
	Create_Medkit10( Vector(6576.988, 938.556, 336.031), Angle(0, 73.844, 0) )
	Create_EldritchArmour( Vector(6558.912, 697.138, 408.031), Angle(0, 1.024, 0) )
	Create_SpiritSphere( Vector(7161.891, 743.805, 408.031), Angle(0, 179.964, 0) )
	Create_HarbingerAmmo( Vector(8222.91, 2256.465, 336.031), Angle(0, 155.544, 0) )
	Create_ManaCrystal( Vector(8089.78, 2515.33, 336.031), Angle(0, 218.244, 0) )
	Create_ManaCrystal( Vector(8095.613, 2402.438, 336.031), Angle(0, 161.484, 0) )
	Create_ManaCrystal( Vector(8008.433, 2233.495, 336.031), Angle(0, 154.444, 0) )
	Create_ManaCrystal( Vector(7935.779, 2027.305, 336.031), Angle(0, 127.604, 0) )
	Create_Medkit25( Vector(8116.375, 1884.879, 336.031), Angle(0, 268.184, 0) )
	Create_RevolverWeapon( Vector(6820.596, 2485.665, 336.031), Angle(0, 11.584, 0) )
	Create_RevolverAmmo( Vector(6846.902, 2446.604, 336.031), Angle(0, 34.244, 0) )
	Create_PistolWeapon( Vector(6753.997, 2988.213, 336.031), Angle(0, 312.404, 0) )
	Create_VolatileBattery( Vector(6681.207, 3010.291, 336.031), Angle(0, 336.385, 0) )
	Create_Beer( Vector(8003.408, 3986.691, 336.031), Angle(0, 351.996, 0) )
	Create_Beer( Vector(8092.067, 3921.431, 336.031), Angle(0, 2.006, 0) )
	Create_Beer( Vector(8180.299, 3794.508, 336.031), Angle(0, 50.736, 0) )
	Create_Beer( Vector(8100.191, 4061.937, 336.031), Angle(0, 343.636, 0) )
	Create_Beer( Vector(8157.257, 4179.209, 336.031), Angle(0, 328.456, 0) )
	Create_Armour5( Vector(8137.792, 4595.224, 336.031), Angle(0, 253.216, 0) )
	Create_Armour5( Vector(8176.5, 4562.246, 336.031), Angle(0, 227.916, 0) )
	Create_Armour5( Vector(8208.565, 4534.439, 336.031), Angle(0, 206.796, 0) )
	Create_Armour5( Vector(8233.598, 4490.258, 336.031), Angle(0, 183.916, 0) )
	Create_Armour( Vector(7565.91, 4225.738, 336.031), Angle(0, 96.276, 0) )
	Create_EldritchArmour10( Vector(7658.52, 4061.361, 336.031), Angle(0, 141.156, 0) )
	Create_EldritchArmour10( Vector(7560.139, 4016.74, 336.031), Angle(0, 78.896, 0) )
	Create_EldritchArmour10( Vector(7599.509, 4059.425, 336.031), Angle(0, 105.956, 0) )
	Create_Backpack( Vector(7107.143, 4079.517, 368.031), Angle(0, 180.756, 0) )
	Create_ShotgunAmmo( Vector(6730.156, 2372.909, 336.031), Angle(0, 198.884, 0) )
	Create_SMGAmmo( Vector(6701.218, 2404.933, 336.031), Angle(0, 218.244, 0) )
	Create_RifleWeapon( Vector(6644.545, 2343.852, 336.031), Angle(0, 208.124, 0) )
	Create_RifleAmmo( Vector(6585.762, 2360.68, 336.031), Angle(0, 240.904, 0) )
	Create_RifleAmmo( Vector(6627.268, 2285.685, 336.031), Angle(0, 175.564, 0) )
	Create_Medkit25( Vector(5568.17, 2201.622, 336.031), Angle(0, 39.076, 0) )
	Create_Medkit25( Vector(5481.5, 2313.078, 336.031), Angle(0, 6.736, 0) )
	Create_M6GWeapon( Vector(8173.892, 876.765, 336.031), Angle(0, 112.336, 0) )
	Create_M6GAmmo( Vector(8130.398, 874.807, 336.031), Angle(0, 107.716, 0) )
	Create_Medkit25( Vector(9143.121, 1013.385, 336.031), Angle(0, 160.912, 0) )
	Create_Medkit25( Vector(9015.98, 899.254, 336.031), Angle(0, 96.562, 0) )
	Create_ShotgunWeapon( Vector(8738.689, 2261.539, 336.031), Angle(0, 359.792, 0) )
	Create_CrossbowAmmo( Vector(8781.388, 2311.051, 336.031), Angle(0, 330.642, 0) )
	Create_CrossbowAmmo( Vector(8796.952, 2235.603, 336.031), Angle(0, 19.152, 0) )
	Create_Armour100( Vector(8553.558, 4002.915, 336.031), Angle(0, 351.432, 0) )
	Create_RevolverAmmo( Vector(6592.858, 4906.907, 336.031), Angle(0, 322.26, 0) )
	Create_RevolverAmmo( Vector(6577.273, 4782.123, 336.031), Angle(0, 34.42, 0) )
	Create_PistolAmmo( Vector(6599.209, 4815.265, 336.031), Angle(0, 23.42, 0) )
	Create_PistolAmmo( Vector(6606.782, 4870.517, 336.031), Angle(0, 339.42, 0) )
	Create_PistolAmmo( Vector(6561.641, 4845.983, 336.031), Angle(0, 0.21, 0) )
	Create_AnthrakiaWeapon( Vector(5955.269, 4229.285, 336.031), Angle(0, 318.872, 0) )
	Create_SpiritEye1( Vector(7902.568, 3704.949, 703.815), Angle(0, 133.772, 0) )
	Create_Armour5( Vector(9438.93, 2026.863, 408.031), Angle(0, 183.008, 0) )
	Create_Armour5( Vector(9513.829, 2025.175, 408.031), Angle(0, 180.368, 0) )
	Create_Armour5( Vector(9622.432, 2022.96, 408.031), Angle(0, 179.488, 0) )
	Create_Medkit10( Vector(10032.199, 2446.241, 408.031), Angle(0, 280.468, 0) )
	Create_Medkit10( Vector(10149.973, 2443.171, 408.031), Angle(0, 252.088, 0) )
	Create_Backpack( Vector(10300.832, 2370.754, 408.031), Angle(0, 184.328, 0) )
	Create_Medkit25( Vector(10519.277, 1932.968, 666.031), Angle(0, 266.292, 0) )
	Create_ManaCrystal( Vector(10308.215, 1936.576, 666.031), Angle(0, 269.592, 0) )
	Create_ManaCrystal( Vector(10209.104, 1935.996, 666.031), Angle(0, 268.492, 0) )
	Create_Bread( Vector(10937.544, 1625.294, 666.031), Angle(0, 169.712, 0) )
	Create_Bread( Vector(10840.466, 1626.288, 666.031), Angle(0, 178.292, 0) )
	Create_Beer( Vector(10719.812, 1566.072, 666.031), Angle(0, 92.051, 0) )
	Create_Beer( Vector(10691.521, 1650.196, 666.031), Angle(0, 35.511, 0) )
	Create_FalanrathThorn( Vector(9774.138, 2462.193, 664.031), Angle(0, 88.531, 0) )
	Create_EldritchArmour10( Vector(9538.354, 4084.089, 664.031), Angle(0, 232.931, 0) )
	Create_EldritchArmour10( Vector(9479.506, 4085.152, 664.031), Angle(0, 233.811, 0) )
	Create_EldritchArmour10( Vector(9468.342, 4149.629, 664.031), Angle(0, 255.151, 0) )
	Create_Medkit25( Vector(9410.914, 5319.814, 736.031), Angle(0, 267.911, 0) )
	Create_Medkit25( Vector(9294.558, 5310.661, 736.031), Angle(0, 272.751, 0) )
	Create_Armour( Vector(9353.279, 5294.823, 736.031), Angle(0, 242.611, 0) )
	Create_ShotgunAmmo( Vector(8245.677, 5441.747, 736.031), Angle(0, 237.11, 0) )
	Create_ShotgunAmmo( Vector(8257.531, 5374.478, 736.031), Angle(0, 207.19, 0) )
	Create_SMGAmmo( Vector(8219.127, 5403.33, 736.031), Angle(0, 237.77, 0) )
	Create_WhisperAmmo( Vector(8279.952, 5408.159, 736.031), Angle(0, 182.55, 0) )
	Create_WhisperAmmo( Vector(8268.814, 5486.072, 736.031), Angle(0, 217.75, 0) )
	Create_SuperShotgunWeapon( Vector(4444.781, 4965.751, 736.031), Angle(0, 351.07, 0) )
	Create_ShotgunAmmo( Vector(4493.931, 4997.259, 736.031), Angle(0, 331.71, 0) )
	Create_RevolverWeapon( Vector(4414.076, 4807.757, 736.031), Angle(0, 353.27, 0) )
	Create_RevolverAmmo( Vector(4444.132, 4764.363, 736.031), Angle(0, 14.39, 0) )
	Create_Medkit25( Vector(4524.449, 3242.906, 336.031), Angle(0, 266.951, 0) )
	Create_Medkit25( Vector(4586.434, 3239.604, 336.031), Angle(0, 266.951, 0) )
	Create_SpiritSphere( Vector(4871.472, 2387.917, 336.031), Angle(0, 195.89, 0) )
	Create_HarbingerWeapon( Vector(3793.01, 4534.119, 336.031), Angle(0, 165.561, 0) )
	Create_Armour100( Vector(1011.533, 3567.053, 408.031), Angle(0, 40.388, 0) )
	Create_Armour( Vector(1005.984, 3636.116, 408.031), Angle(0, 5.847, 0) )
	Create_Medkit25( Vector(1001.919, 3712.582, 408.031), Angle(0, 350.887, 0) )
	Create_Bread( Vector(1524.03, 3699.377, 408.031), Angle(0, 195.787, 0) )
	Create_Bread( Vector(1482.624, 3704.931, 408.031), Angle(0, 209.647, 0) )

	CreateSecretArea( Vector(4879.6,2397.529,336.031), Vector(-48.595,-48.595,0), Vector(48.595,48.595,68.331) )
	
	local OPEN_THE_DOOR = function()
		
		for _,v in pairs (ents.FindInSphere( Vector(2584.187,3430.878,462.418), 250 )) do
		
			if v and IsValid(v) and v:IsDoor() then
				v:Fire("Unlock")
			end
		
		end
		
	end
	
	CreateTrigger( Vector(2593.828,3431.822,408.031), Vector(-199.353,-199.353,0), Vector(199.353,199.353,210.028), OPEN_THE_DOOR )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)