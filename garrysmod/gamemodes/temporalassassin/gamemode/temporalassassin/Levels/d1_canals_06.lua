
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("trigger_hurt")
	
	timer.Simple(2,function()
		--[[ Change these triggers to work with the player and not with some stupid vehicle ]]
		for _,v in pairs (ents.FindByClass("trigger_once")) do
			if v and IsValid(v) then
				v:SetKeyValue("spawnflags",1)
			end
		end
		for _,v in pairs (ents.FindByClass("trigger_multiple")) do
			if v and IsValid(v) then
				v:SetKeyValue("spawnflags",1)
			end
		end
	end)
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then

		Create_RevolverAmmo( Vector(7895.1889648438, 9044.0673828125, -237.96875), Angle(0, 83.65746307373, 0) )
		Create_RevolverAmmo( Vector(7913.4038085938, 9042.27734375, -237.96875), Angle(0, 95.317527770996, 0) )
		Create_RevolverAmmo( Vector(7903.49609375, 9064.7392578125, -237.96875), Angle(0, 88.717491149902, 0) )
		
		Create_FalanrathThorn( Vector(-8192.2109375, 13921.215820313, -26.715522766113), Angle(0, 266.47747802734, 0) )
		
		Create_Armour100( Vector(3967.9465332031, 6088.3149414063, -187.96875), Angle(0, 286.18963623047, 0) )
		Create_Beer( Vector(3958.6604003906, 6106.0263671875, -39.968746185303), Angle(0, 276.94998168945, 0) )
		Create_Beer( Vector(3958.3618164063, 6071.1401367188, -39.96875), Angle(0, 280.25, 0) )
		Create_Beer( Vector(3958.5395507813, 6042.8754882813, -39.968753814697), Angle(0, 271.89004516602, 0) )
		Create_Beer( Vector(3960.0979003906, 6008.0405273438, -39.96875), Angle(0, 271.67004394531, 0) )
		Create_Beer( Vector(3956.4765625, 5978.986328125, -39.968746185303), Angle(0, 274.09005737305, 0) )
		Create_Beer( Vector(3953.3259277344, 5953.1508789063, -39.96875), Angle(0, 277.39007568359, 0) )
		Create_Beer( Vector(3951.6262207031, 5916.380859375, -39.96875), Angle(0, 284.6501159668, 0) )
		Create_Beer( Vector(3954.8120117188, 5864.46875, -39.96875), Angle(0, 16.170455932617, 0) )
		Create_Beer( Vector(3957.3627929688, 5826.1899414063, -39.96875), Angle(0, 80.190490722656, 0) )
		Create_Backpack( Vector(4370.9375, 6004.1752929688, 96.03125), Angle(0, 341.85034179688, 0) )
		Create_Medkit25( Vector(4373.7221679688, 5913.1923828125, 96.03125), Angle(0, 28.050231933594, 0) )
		Create_PortableMedkit( Vector(4366.6411132813, 5947.3193359375, 96.03125), Angle(0, 4.5101928710938, 0) )
		Create_PortableMedkit( Vector(4366.11328125, 5974.8056640625, 96.03125), Angle(0, 346.03009033203, 0) )
		Create_PistolAmmo( Vector(4400.4838867188, 6004.30078125, 96.03125), Angle(0, 314.56994628906, 0) )
		Create_PistolAmmo( Vector(4427.4760742188, 6016.380859375, 96.03125), Angle(0, 289.92984008789, 0) )
		Create_PistolAmmo( Vector(4451.6762695313, 6008.2768554688, 96.03125), Angle(0, 268.36975097656, 0) )
		
		CreateSecretArea( Vector(-8195.9990234375,13801.116210938,-51.008598327637), Vector(-201.04139709473,-201.04139709473,0), Vector(201.04139709473,201.04139709473,134.89819335938) )
		
	else
	
		Create_RevolverWeapon( Vector(7904.165, 9054.957, -237.969), Angle(0, 73.508, 0) )
		Create_ShotgunWeapon( Vector(7896.567, 9078.857, -237.969), Angle(0, 94.617, 0) )
		Create_SuperShotgunWeapon( Vector(7899.183, 9097.712, -240.396), Angle(0, 81.659, 0) )
		Create_SMGWeapon( Vector(7902.565, 9115.186, -244.764), Angle(0, 83.122, 0) )
		Create_PistolWeapon( Vector(7904.735, 9138.93, -250.7), Angle(0, 81.032, 0) )
		Create_Medkit25( Vector(4087.191, 5814.982, -187.969), Angle(0, 71.515, 0) )
		Create_PistolWeapon( Vector(4044.165, 5842.37, -187.969), Angle(0, 81.128, 0) )
		Create_PistolAmmo( Vector(4382.119, 6000.853, 96.031), Angle(0, 338.718, 0) )
		Create_Bread( Vector(4378.73, 5932.384, 96.031), Angle(0, 41.209, 0) )
		Create_Armour( Vector(4380.884, 5971.816, 96.031), Angle(0, 9.232, 0) )
		Create_FrontlinerAmmo( Vector(-400.5, 4855.928, -948.321), Angle(0, 89.797, -60.54) )
		Create_FrontlinerWeapon( Vector(-8286.307, 13941.332, -39.643), Angle(0, 287.808, 0) )

		local TRIG_SEC1 = function()
		
			local PHYS_BOX = ents.GetMapCreatedEntity(1673)
			D1_C_06_KEY = CreateProp(Vector(4805.125,5956.344,28.031), Angle(11.426,-119.004,-12.744), "models/temporalassassin/items/reikori_key.mdl", true)
			D1_C_06_KEY:SetParent(PHYS_BOX)
		
		end
		
		local DOLL_AP1 = CreateProp(Vector(-8165.281,13943.313,-36.375), Angle(11.602,-102.92,-0.352), "models/temporalassassin/doll/unknown.mdl", true)
		
		local DOLL_DISSAPEAR1 = function()
		
			CreateParticleEffect( "Eldritch_Disappear", DOLL_AP1:GetBoxCenter(), Angle(0,0,0), 1 )
			sound.Play( "TemporalAssassin/Doll/Doll_Teleported.wav", DOLL_AP1:GetBoxCenter(), 140, 100, 1, CHAN_STATIC )
			
			timer.Simple(0,function()
				if IsValid(DOLL_AP1) then
					DOLL_AP1:SetPos( Vector(-8165.281,13943.313,-50000.375) )
					DOLL_AP1:Remove()
				end
			end)
			
			net.Start("DollMessage")
			net.WriteString("Sorry kiddo, Falanraths in another castle.")
			net.Broadcast()
		
		end
		
		CreateRestingPoint( Vector(-8162.437,13945.156,-36.031), Angle(12.327,-98.394,0) )
		
		CreateTrigger( Vector(6772.688,8138.408,-494.548), Vector(-533.499,-533.499,0), Vector(533.499,533.499,516.634), TRIG_SEC1 )
		CreateTrigger( Vector(-8168.72,13931.661,-38.062), Vector(-112.319,-112.319,0), Vector(112.319,112.319,171.194), DOLL_DISSAPEAR1 )
		
		CreateSecretArea( Vector(7901.544,9089.592,-233.437), Vector(-68.857,-68.857,0), Vector(68.857,68.857,59.34) )
		D1_C_06_KEYSECRET = CreateSecretArea( Vector(5005.705,6072.675,-50089.781), Vector(-96.774,-96.774,0), Vector(96.774,96.774,86.63), "YOU FOUND A SUPER SECRET", "TemporalAssassin/Secrets/SuperSecret_Find.wav" )
		CreateSecretArea( Vector(-404.392,4864.784,-943.781), Vector(-49.978,-49.978,0), Vector(49.978,49.978,63.82) )
		CreateSecretArea( Vector(-8204.481,13515.563,-65.437), Vector(-147.876,-147.876,0), Vector(147.876,147.876,239.516) )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

local BOX_FALLEN = false

function BOX_FALL_INPUT( ent, input, act, caller, alue )

	if GetConVarNumber("TA_DEV_LegacyMaps") < 1 and ent and IsValid(ent) then
		
		if not BOX_FALLEN and ent:GetClass() == "func_physbox" and ent:GetName() == "gear_assembly" and input == "EnableMotion" then
		
			BOX_FALLEN = true
			D1_C_06_KEY:Remove()
			D1_C_06_KEYSECRET:SetPos( Vector(5005.705,6072.675,-689.781) )
			Create_ReikoriKey( Vector(5006.594, 6075.033, -695.969), Angle(0, 135.539, 0) )
		
		end
	
	end

end
hook.Add("AcceptInput","BOX_FALL_INPUTHook",BOX_FALL_INPUT)