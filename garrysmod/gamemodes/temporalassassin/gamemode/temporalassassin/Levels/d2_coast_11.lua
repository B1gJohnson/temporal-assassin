
if CLIENT then return end

SEAGULLS_LEFT = 4

function Coast_11_SG( npc, att, inf )

	if npc:GetClass() == "npc_seagull" and IsValid(att) and att:IsPlayer() then
	
		SEAGULLS_LEFT = SEAGULLS_LEFT - 1
	
		if SEAGULLS_LEFT == 0 then
		
			if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
		
				att:PrintMessage( HUD_PRINTCENTER, tostring("ALL SEAGULLS DOWN, SECRET REWARD HAS APPEARED SOMEWHERE") )
				Create_Beer( Vector(3701.8522949219, 824.03948974609, 564.03125), Angle(0, 219.95370483398, 0) )
				Create_Beer( Vector(3695.0903320313, 830.58001708984, 564.03125), Angle(0, 231.95370483398, 0) )
				Create_Beer( Vector(3687.1162109375, 835.57165527344, 564.03125), Angle(0, 243.95370483398, 0) )
				Create_Beer( Vector(3678.2788085938, 838.79638671875, 564.03125), Angle(0, 255.95370483398, 0) )
				Create_Beer( Vector(3668.9638671875, 840.11322021484, 564.03125), Angle(0, 267.95370483398, 0) )
				Create_Beer( Vector(3659.5786132813, 839.46453857422, 564.03125), Angle(0, 279.95370483398, 0) )
				Create_Beer( Vector(3650.5334472656, 836.87878417969, 564.03125), Angle(0, 291.95370483398, 0) )
				Create_Beer( Vector(3642.2233886719, 832.46893310547, 564.03125), Angle(0, 303.95370483398, 0) )
				Create_Beer( Vector(3635.0119628906, 826.42767333984, 564.03125), Angle(0, 315.95370483398, 0) )
				Create_Beer( Vector(3629.2141113281, 819.01910400391, 564.03125), Angle(0, 327.95370483398, 0) )
				Create_Beer( Vector(3625.0832519531, 810.56695556641, 564.03125), Angle(0, 339.95370483398, 0) )
				Create_Beer( Vector(3622.8000488281, 801.44067382813, 564.03125), Angle(0, 351.95370483398, 0) )
				Create_Beer( Vector(3622.4641113281, 792.03912353516, 564.03125), Angle(0, 363.95370483398, 0) )
				Create_Beer( Vector(3624.0900878906, 782.77319335938, 564.03125), Angle(0, 375.95370483398, 0) )
				Create_Beer( Vector(3627.6071777344, 774.04779052734, 564.03125), Angle(0, 387.95370483398, 0) )
				Create_Beer( Vector(3632.8615722656, 766.24432373047, 564.03125), Angle(0, 399.95370483398, 0) )
				Create_Beer( Vector(3639.6235351563, 759.70379638672, 564.03125), Angle(0, 411.95370483398, 0) )
				Create_Beer( Vector(3647.59765625, 754.71215820313, 564.03125), Angle(0, 423.95370483398, 0) )
				Create_Beer( Vector(3656.4350585938, 751.48742675781, 564.03125), Angle(0, 435.95370483398, 0) )
				Create_Beer( Vector(3665.75, 750.17059326172, 564.03125), Angle(0, 447.95370483398, 0) )
				Create_Beer( Vector(3675.1352539063, 750.81927490234, 564.03125), Angle(0, 459.95370483398, 0) )
				Create_Beer( Vector(3684.1804199219, 753.40502929688, 564.03125), Angle(0, 471.95370483398, 0) )
				Create_Beer( Vector(3692.4904785156, 757.81488037109, 564.03125), Angle(0, 483.95370483398, 0) )
				Create_Beer( Vector(3699.7019042969, 763.85614013672, 564.03125), Angle(0, 495.95370483398, 0) )
				Create_Beer( Vector(3705.4997558594, 771.26470947266, 564.03125), Angle(0, 507.95370483398, 0) )
				Create_Beer( Vector(3709.6306152344, 779.71685791016, 564.03125), Angle(0, 519.95373535156, 0) )
				Create_Beer( Vector(3711.9138183594, 788.84307861328, 564.03125), Angle(0, 531.95373535156, 0) )
				Create_Beer( Vector(3712.2497558594, 798.24468994141, 564.03125), Angle(0, 543.95373535156, 0) )
				Create_Beer( Vector(3710.6237792969, 807.51062011719, 564.03125), Angle(0, 555.95373535156, 0) )
				Create_Beer( Vector(3707.1066894531, 816.23602294922, 564.03125), Angle(0, 567.95373535156, 0) )
				Create_Backpack( Vector(3639.1604003906, 935.11138916016, 564.03125), Angle(0, 297.71365356445, 0) )
				Create_FalanrathThorn( Vector(3706.4399414063, 650.30816650391, 564.03125), Angle(0, 73.314064025879, 0) )
				Create_PistolWeapon( Vector(3557.9909667969, 889.79760742188, 564.03125), Angle(0, 305.27407836914, 0) )
				Create_M6GWeapon( Vector(3558.3835449219, 889.24261474609, 567.0625), Angle(0, 305.27407836914, 0) )
				Create_RevolverWeapon( Vector(3558.7761230469, 888.68768310547, 570.09375), Angle(0, 305.27407836914, 0) )
				Create_ShotgunWeapon( Vector(3560.681640625, 886.38659667969, 573.125), Angle(0, 303.73406982422, 0) )
				Create_SuperShotgunWeapon( Vector(3560.978515625, 885.94213867188, 576.15625), Angle(0, 303.73406982422, 0) )
				Create_SMGWeapon( Vector(3561.275390625, 885.49761962891, 579.1875), Angle(0, 303.73406982422, 0) )
				Create_KingSlayerWeapon( Vector(3561.572265625, 885.05310058594, 582.21875), Angle(0, 303.73406982422, 0) )
				Create_RifleWeapon( Vector(3561.572265625, 885.05310058594, 585.21875), Angle(0, 303.73406982422, 0) )
				Create_CrossbowWeapon( Vector(3561.572265625, 885.05310058594, 588.21875), Angle(0, 303.73406982422, 0) )
				Create_Backpack( Vector(3603.8505859375, 916.16821289063, 564.03125), Angle(0, 307.03414916992, 0) )
				Create_WhisperWeapon( Vector(3670.78125, 797.42889404297, 564.03125), Angle(0, 33.274169921875, 0) )
				CreateProp(Vector(3541.03125,844.0625,626.59375), Angle(13.4033203125,-6.1962890625,2.28515625), "models/temporalassassin/doll/unknown.mdl", true)
				CreateSecretArea( Vector(3679.0969238281,814.02026367188,570.89233398438), Vector(-174.58909606934,-174.58909606934,0), Vector(174.58909606934,174.58909606934,210.38146972656) )
				
			else
			
				att:PrintMessage( HUD_PRINTCENTER, tostring("ALL SEAGULLS DOWN, SOMETHING CHANGED...") )
				
				Create_CorzoHat( Vector(3611.786, 787.145, 564.031), Angle(0, 24.424, 0) )
				Create_CorzoHat( Vector(3659.728, 926.308, 564.031), Angle(0, 310.647, 0) )
				CreateProp(Vector(3610.438,872.594,564.375), Angle(0.044,-15.249,0), "models/temporalassassin/doll/unknown.mdl", true)
				CreateEquipmentLocker( Vector(3697.906,710.625,564.031), Angle(0,63.896,0) )
				
				GLOB_SUP_SECRET:SetPos( Vector(3679.096,814.02,570.89) )
			
			end
			
		elseif SEAGULLS_LEFT > 0 then
		
			att:PrintMessage( HUD_PRINTCENTER, tostring("SEAGULL DOWN, "..SEAGULLS_LEFT.." LEFT!") )
			
		end
	
	end

end
hook.Add("OnNPCKilled","Coast_11_SGHook",Coast_11_SG)

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	CreatePlayerSpawn( Vector(3421.625,-10193.375,1542.375), Angle(0,90,0) )
	
	local P1 = GetEntityByID(2080)
	local P2 = GetEntityByID(2081)
	local P3 = GetEntityByID(2301)
	
	if P1 and IsValid(P1) then
		P1:Remove()
	end
	if P2 and IsValid(P2) then
		P2:Remove()
	end
	if P3 and IsValid(P3) then
		P3:Remove()
	end

	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Armour200( Vector(1147.9395751953, -266.18569946289, 291.4919128418), Angle(0, 297.7119140625, 0) )
		Create_Medkit25( Vector(6051.5229492188, -1355.1160888672, 396.03125), Angle(0, 285.0842590332, 0) )
		Create_Medkit25( Vector(6049.28125, -1476.8354492188, 396.03125), Angle(0, 63.244277954102, 0) )
		Create_PortableMedkit( Vector(6253.1821289063, -1493.4007568359, 396.03125), Angle(0, 132.98419189453, 0) )
		Create_PortableMedkit( Vector(6230.4877929688, -1492.4914550781, 396.03125), Angle(0, 111.6441116333, 0) )

		CreateSecretArea( Vector(1152.2924804688,-274.47262573242,296), Vector(-48.792953491211,-48.792953491211,0), Vector(48.792953491211,48.792953491211,90.364196777344) )
		CreateSecretArea( Vector(6239.8891601563,-1488.4710693359,400.5625), Vector(-15.114405632019,-15.114405632019,0), Vector(15.114405632019,15.114405632019,21.633605957031) )
		
	else
	
		Create_Medkit25( Vector(1984.652, -3720.301, 1107.782), Angle(0, 298.006, 0) )
		Create_ManaCrystal( Vector(1919.044, -3788.476, 1110.121), Angle(0, 318.906, 0) )
		Create_ManaCrystal( Vector(1866.408, -3857.38, 1098.233), Angle(0, 356.317, 0) )
		Create_SpiritSphere( Vector(1154.748, -283.976, 291.676), Angle(0, 282.957, 0) )
		Create_PistolWeapon( Vector(6054.028, -1478.391, 396.031), Angle(0, 27.873, 0) )
		Create_PistolAmmo( Vector(6047.489, -1448.861, 396.031), Angle(0, 2.897, 0) )
		Create_ShotgunAmmo( Vector(6065.25, -1356.033, 396.031), Angle(0, 296.644, 0) )
		Create_SMGAmmo( Vector(6049.986, -1386.704, 396.031), Angle(0, 321.933, 0) )
		Create_SMGAmmo( Vector(6089.408, -1382.188, 396.031), Angle(0, 284.104, 0) )
		Create_SuperShotgunWeapon( Vector(6081.327, -1434.152, 396.031), Angle(0, 4.779, 0) )
		Create_KingSlayerAmmo( Vector(6113.092, -1482.945, 396.031), Angle(0, 73.54, 0) )
		Create_Medkit25( Vector(6229.644, -1413.711, 396.031), Angle(0, 106.143, 0) )
		Create_Medkit25( Vector(6176.098, -1434.928, 396.031), Angle(0, 94.23, 0) )
		Create_Medkit10( Vector(6195.171, -1405.398, 396.031), Angle(0, 119.31, 0) )
		Create_Armour( Vector(6129.992, -1421.36, 396.031), Angle(0, 38.219, 0) )
		Create_Medkit25( Vector(3408.466, 1832.929, 608.031), Angle(0, 102.278, 0) )
		Create_Medkit25( Vector(3295.221, 1842.41, 608.031), Angle(0, 23.067, 0) )
		Create_Armour( Vector(3478.704, 2075.33, 608.031), Angle(0, 298.631, 0) )
		Create_Medkit10( Vector(3465.517, 1980.283, 608.031), Angle(0, 9.064, 0) )
		Create_Medkit10( Vector(3466.497, 2025.206, 608.031), Angle(0, 335.415, 0) )
		Create_GrenadeAmmo( Vector(3464.203, 2145.116, 623.28), Angle(0, 343.357, 0) )
		Create_ShotgunWeapon( Vector(2984.116, 3210.216, 576.031), Angle(0, 45.957, 0) )
		Create_ShotgunAmmo( Vector(3021.52, 3217.961, 576.031), Angle(0, 70.828, 0) )
		Create_KingSlayerWeapon( Vector(2982.968, 3331.092, 576.031), Angle(0, 309.607, 0) )
		Create_KingSlayerAmmo( Vector(3012.354, 3382.907, 576.031), Angle(0, 282.228, 0) )
		Create_SMGAmmo( Vector(2912.969, 3213.02, 576.031), Angle(0, 30.282, 0) )
		Create_SMGAmmo( Vector(2919.578, 3310.241, 576.031), Angle(0, 325.7, 0) )
		Create_RevolverAmmo( Vector(2893.46, 3240.114, 576.031), Angle(0, 12.726, 0) )
		Create_RevolverAmmo( Vector(2892.729, 3284.483, 576.031), Angle(0, 347.018, 0) )
		Create_RevolverWeapon( Vector(2930.725, 3260.924, 576.031), Angle(0, 1.022, 0) )
		
		for _,v in pairs (ents.FindInSphere(Vector(3763.797,739.32,595.044),50)) do
		
			if IsValid(v) and v:IsProp() then
				v:Remove()
			end
		
		end

		GLOB_SUP_SECRET = CreateSecretArea( Vector(3679.0969238281,814.02026367188,-50070.89233398438), Vector(-174.58909606934,-174.58909606934,0), Vector(174.58909606934,174.58909606934,210.38146972656), "YOU FOUND A SUPER SECRET", "TemporalAssassin/Secrets/SuperSecret_Find.wav" )
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)