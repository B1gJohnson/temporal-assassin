		
if CLIENT then return end

LEVEL_STRINGS["islandunderground3"] = { "islandunderground4", Vector(3876.547,798.246,-1279.969), Vector(-97.308,-97.308,0), Vector(97.308,97.308,127.938) }

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(5393.95,371.042,-1927.62),Angle(0,-180,0))
	
	for _,v in pairs (ents.FindInSphere(Vector(5319.745,289.954,-1927.969),100)) do
		if IsValid(v) and v:IsDoor() then
			v:Fire("Open")
		end
	end
	
	CreateProp(Vector(4600.125,1291.969,-2180.625), Angle(0,-0.044,0), "models/props_wasteland/controlroom_filecabinet002a.mdl", true)
	CreateProp(Vector(4599.219,1395.719,-2180.625), Angle(-0.132,1.362,0.132), "models/props_wasteland/controlroom_filecabinet002a.mdl", true)
	
	local PROP1 = CreateProp(Vector(5151.938,1434.656,-2049.562), Angle(89.341,-0.439,-178.242), "models/props_debris/metal_panel01a.mdl", true)
	CreateProp(Vector(5223.688,1447.875,-2032.937), Angle(-61.436,-113.555,-50.405), "models/props_vehicles/tire001b_truck.mdl", true)
	CreateProp(Vector(5097.594,1459.375,-2035), Angle(-0.044,-89.824,1.23), "models/props_wasteland/controlroom_filecabinet001a.mdl", true)
	CreateProp(Vector(5095.281,1469.594,-2016.312), Angle(-0.879,-89.824,-87.803), "models/props_junk/meathook001a.mdl", true)
	CreateProp(Vector(5128.188,1472.906,-2033.781), Angle(-24.258,-90.571,1.362), "models/props_trainstation/tracksign09.mdl", true)
	
	local BLOCK1 = CreateProp(Vector(4335.813,169.875,-1155.156), Angle(-90,180,180), "models/props_lab/blastdoor001c.mdl", true)
	local BLOCK2 = CreateProp(Vector(4453.281,72.75,-1252.094), Angle(0,-90,0), "models/props_lab/blastdoor001c.mdl", true)
	local BLOCK3 = CreateProp(Vector(4309.719,72.5,-1253.937), Angle(0,-90,0), "models/props_lab/blastdoor001c.mdl", true)
	local BLOCK4 = CreateProp(Vector(4223.594,155.313,-1257.062), Angle(0,180,0), "models/props_lab/blastdoor001c.mdl", true)
	local BLOCK5 = CreateProp(Vector(4225.875,312.5,-1257.594), Angle(0,180,0), "models/props_lab/blastdoor001c.mdl", true)
	local BLOCK6 = CreateProp(Vector(4226.906,480.469,-1254.75), Angle(0,180,0), "models/props_lab/blastdoor001c.mdl", true)
	
	BLOCK1:SetRenderMode(RENDERMODE_TRANSALPHA)
	BLOCK1:SetColor(Color(255,255,255,0))
	BLOCK2:SetRenderMode(RENDERMODE_TRANSALPHA)
	BLOCK2:SetColor(Color(255,255,255,0))
	BLOCK3:SetRenderMode(RENDERMODE_TRANSALPHA)
	BLOCK3:SetColor(Color(255,255,255,0))
	BLOCK4:SetRenderMode(RENDERMODE_TRANSALPHA)
	BLOCK4:SetColor(Color(255,255,255,0))
	BLOCK5:SetRenderMode(RENDERMODE_TRANSALPHA)
	BLOCK5:SetColor(Color(255,255,255,0))
	BLOCK6:SetRenderMode(RENDERMODE_TRANSALPHA)
	BLOCK6:SetColor(Color(255,255,255,0))
	
	Create_Medkit25( Vector(5078.194, 75.157, -1927.969), Angle(0, 0.319, 0) )
	Create_Backpack( Vector(4747.325, 256.177, -1671.969), Angle(0, 118.319, 0) )
	Create_ManaCrystal( Vector(4262.778, 308.442, -1647.969), Angle(0, 5.898, 0) )
	Create_ManaCrystal( Vector(4261.389, 370.262, -1647.969), Angle(0, 355.778, 0) )
	Create_SMGAmmo( Vector(4905.006, 431.014, -1415.969), Angle(0, 298.957, 0) )
	Create_SMGAmmo( Vector(4938.724, 439.922, -1415.969), Angle(0, 281.797, 0) )
	Create_RifleWeapon( Vector(4940.934, 392.365, -1415.969), Angle(0, 288.177, 0) )
	Create_Medkit10( Vector(4698.14, 422.694, -1415.969), Angle(0, 358.417, 0) )
	Create_Medkit10( Vector(4702.283, 460.89, -1415.969), Angle(0, 339.277, 0) )
	Create_PistolWeapon( Vector(4525.511, 856.651, -1415.969), Angle(0, 287.796, 0) )
	Create_PistolAmmo( Vector(4550.729, 858.636, -1415.969), Angle(0, 270.856, 0) )
	Create_Armour100( Vector(3589.084, 1126.427, -1386.004), Angle(0, 354.94, 0) )
	Create_SpiritEye1( Vector(3285.473, 418.945, -1511.969), Angle(0, 92.414, 0) )
	Create_ManaCrystal100( Vector(4055.554, 144.224, -1513.969), Angle(0, 192.813, 0) )
	Create_RevolverAmmo( Vector(3921.011, -393.882, -1511.969), Angle(0, 89.414, 0) )
	Create_SMGAmmo( Vector(3936.912, -359.216, -1511.969), Angle(0, 106.574, 0) )
	Create_SuperShotgunWeapon( Vector(3957.307, -311.854, -1511.969), Angle(0, 89.194, 0) )
	Create_Medkit25( Vector(4039.681, -878.326, -1415.969), Angle(0, 104.874, 0) )
	Create_Medkit25( Vector(3984.254, -877.215, -1415.969), Angle(0, 70.114, 0) )
	Create_RifleAmmo( Vector(4106.296, -425.799, -1288.089), Angle(0, 93.184, 0) )
	Create_SMGWeapon( Vector(4173.83, -417.493, -1288.089), Angle(0, 134.214, 0) )
	Create_CrossbowAmmo( Vector(4058.007, -259.021, -1287.969), Angle(0, 309.194, 0) )
	Create_Medkit10( Vector(4132.832, -547.53, -1511.809), Angle(0, 14.094, 0) )
	Create_Medkit10( Vector(4132.352, -495.834, -1511.809), Angle(0, 355.834, 0) )
	Create_ShotgunAmmo( Vector(4147.79, -519.57, -1511.809), Angle(0, 6.174, 0) )
	Create_EldritchArmour( Vector(4090.16, -905.15, -1211.969), Angle(0, 117.354, 0) )
	Create_SpiritSphere( Vector(4142.496, 615.461, -1279.139), Angle(0, 267.694, 0) )
	Create_PortableMedkit( Vector(3935.546, 28.655, -1647.969), Angle(0, 311.694, 0) )
	Create_PortableMedkit( Vector(3969.193, 28.511, -1647.969), Angle(0, 299.374, 0) )
	Create_Medkit25( Vector(4156.607, 158.987, -1647.969), Angle(0, 270.774, 0) )
	Create_Beer( Vector(3425.809, 218.089, -1647.969), Angle(0, 262.414, 0) )
	Create_Beer( Vector(3465.844, 270.513, -1647.969), Angle(0, 248.114, 0) )
	Create_Beer( Vector(3508.932, 335.414, -1647.969), Angle(0, 242.834, 0) )
	Create_Beer( Vector(3588.936, 340.6, -1639.969), Angle(0, 178.374, 0) )
	Create_EldritchArmour10( Vector(3634.546, 112.777, -1647.969), Angle(0, 205.434, 0) )
	Create_EldritchArmour10( Vector(3631.166, 87.973, -1647.969), Angle(0, 193.114, 0) )
	Create_Armour( Vector(3218.609, 366.374, -1647.969), Angle(0, 323.134, 0) )
	Create_Armour100( Vector(3914.148, 203.416, -1647.969), Angle(0, 275.482, 0) )
	Create_Medkit25( Vector(3911.404, 165.216, -1647.969), Angle(0, 349.842, 0) )
	Create_WhisperAmmo( Vector(3907.145, 106.77, -1647.969), Angle(0, 12.282, 0) )
	Create_RevolverAmmo( Vector(3228.718, -326.387, -1375.969), Angle(0, 34.721, 0) )
	Create_ShotgunAmmo( Vector(3228.516, -300.047, -1375.969), Angle(0, 22.621, 0) )
	Create_SMGAmmo( Vector(3253.593, -308.349, -1375.969), Angle(0, 34.281, 0) )
	Create_HarbingerAmmo( Vector(4510.647, 40.761, -983.969), Angle(0, 88.842, 0) )
	Create_Medkit10( Vector(4504.298, 327.359, -1137.969), Angle(0, 144.141, 0) )
	Create_SMGAmmo( Vector(4424.13, 334.399, -1137.969), Angle(0, 67.8, 0) )
	Create_ShotgunAmmo( Vector(4459.959, 330.688, -1137.969), Angle(0, 90.68, 0) )
	Create_ShotgunWeapon( Vector(4050.054, 959.36, -1135.969), Angle(0, 358.5, 0) )
	Create_ManaCrystal( Vector(4052.858, 722.349, -1279.969), Angle(0, 89.58, 0) )

	local B_FUNC = function()
		Create_FalanrathThorn( Vector(5090.634, 1346.967, -2215.969), Angle(0, 180.26, 0) )
	end
	
	local B_FUNC2 = function()
		Create_AnthrakiaWeapon( Vector(5154.96, 1512.027, -2215.969), Angle(0, 275.3, 0) )
	end
	
	AddSecretButton( Vector(4583.27,1346.22,-2160.226), B_FUNC )
	AddSecretButton( Vector(5169.546,1470.216,-2023.362), B_FUNC2 )
	
	CreateSecretArea( Vector(4087.923,-900.908,-1207.437), Vector(-27.397,-27.397,0), Vector(27.397,27.397,40.917) )
	CreateSecretArea( Vector(4140.876,618.463,-1279.139), Vector(-47.533,-47.533,0), Vector(47.533,47.533,91.773) )
	CreateSecretArea( Vector(4508.847,40.13,-979.437), Vector(-21.165,-21.165,0), Vector(21.165,21.165,53.506) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)