		
if CLIENT then return end

LEVEL_STRINGS["ep2_outland_04"] = {"ep2_outland_05",Vector(4925.5297851563,-1607.9337158203,3001.0112304688), Vector(-98.39274597168,-98.39274597168,0), Vector(98.39274597168,98.39274597168,245.94213867188)}

function EXTRACT_GUARD_DIED( npc, att, inf )

	if IsValid(npc) and npc:GetClass() == "npc_antlionguard" and IsValid(att) and att:IsPlayer() then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRACT_GUARD_DIEDHook",EXTRACT_GUARD_DIED)

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("trigger_playermovement")
	RemoveAllEnts("trigger_changelevel")

	Create_SpiritSphere( Vector(5015.9619140625, -3403.0317382813, 779.30004882813), Angle(0, 126.48091888428, 0) )
	Create_Medkit25( Vector(6657.119140625, -4117.4360351563, -1037.9799804688), Angle(0, 120.54058837891, 0) )
	Create_Medkit25( Vector(6542.2924804688, -4099.5366210938, -1031.9367675781), Angle(0, 25.280456542969, 0) )
	Create_Medkit10( Vector(6620.0703125, -4130.1909179688, -1036.5593261719), Angle(0, 86.220527648926, 0) )
	Create_Medkit10( Vector(6577.4165039063, -4119.8549804688, -1032.2615966797), Angle(0, 62.020477294922, 0) )
	Create_PortableMedkit( Vector(6604.0869140625, -4092.3024902344, -1037.0047607422), Angle(0, 70.820518493652, 0) )
	Create_KingSlayerWeapon( Vector(7269.1259765625, -3135.3283691406, -1138.5642089844), Angle(0, 169.87019348145, 0) )
	Create_KingSlayerAmmo( Vector(7234.1860351563, -3153.4416503906, -1142.7069091797), Angle(0, 146.33020019531, 0) )
	Create_KingSlayerAmmo( Vector(7240.5122070313, -3109.4711914063, -1141.2568359375), Angle(0, 195.17021179199, 0) )
	
	Create_FoolsOathWeapon( Vector(7402.537, 1413.396, -2681.645), Angle(0, 238.056, 0) )
	Create_FoolsOathAmmo( Vector(7350.699, 1417.592, -2683.468), Angle(0, 160.596, 0) )
	Create_FoolsOathAmmo( Vector(7425.679, 1363.895, -2686.047), Angle(0, 116.288, 0) )
	Create_FoolsOathAmmo( Vector(7375.93, 1377.643, -2686.025), Angle(0, 141.368, 0) )
	
	CreateSecretArea( Vector(5015.962890625,-3403.0329589844,779.29937744141), Vector(-29.531776428223,-29.531776428223,0), Vector(29.531776428223,29.531776428223,68.248168945313) )
	CreateSecretArea( Vector(7389.248,1395.909,-2677.094), Vector(-83.756,-83.756,0), Vector(83.756,83.756,111.555) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

GLOB_ELEVATOR_COG = false

function FIX_IT_FELIX()

	for _,v in pairs (ents.FindByClass("logic_branch")) do
	
		if v and IsValid(v) then
			v:Input("SetValue",v,v,1)
			v:Input("SetValueTest",v,v,1)
		end
	
	end
	
	if GLOB_ELEVATOR_COG and IsValid(GLOB_ELEVATOR_COG) then
		GLOB_ELEVATOR_COG:Remove()
	end
	
	for _,v in pairs (ents.FindByClass("prop_dynamic")) do
	
		if v and IsValid(v) and v.MapCreationID and v:MapCreationID() == 2232 then
			v:SetBodygroup(1,1)
		end
	
	end

end

function LIFT_FIX()

	if GLOB_ELEVATOR_COG and IsValid(GLOB_ELEVATOR_COG) then
	
		if GLOB_ELEVATOR_COG:GetPos():Distance( Vector(5071.32,-1648.529,-2304.417) ) <= 20 then
			FIX_IT_FELIX()
			timer.Simple(1,function()
				FIX_IT_FELIX()
			end)
		end
	
	else

		for _,v in pairs (ents.FindByClass("prop_physics")) do
	
			if IsValid(v) and v.MapCreationID then
			
				if v:MapCreationID() == 2236 then
					GLOB_ELEVATOR_COG = v
				end
				
			end
		
		end
		
	end

end
timer.Create("EP2_LIFT_FIX", 0.5, 0, LIFT_FIX)