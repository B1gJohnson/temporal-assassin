		
if CLIENT then return end

LEVEL_STRINGS["islandescape"] = {"islandescape2", Vector(1686.922,1204.033,268.031), Vector(-176.629,-176.629,0), Vector(176.629,176.629,125.46)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(160,-296,-15),Angle(0,120,0))
	CreateClothesLocker( Vector(1461.797,-230.412,360.134), Angle(0,317.507,0) )
	
	Create_Armour100( Vector(82.456, -305.343, 7.495), Angle(0, 26.374, 0) )
	Create_RevolverWeapon( Vector(83.306, -250.416, 7.495), Angle(0, 318.614, 0) )
	Create_RevolverAmmo( Vector(88.145, -267.292, 7.495), Angle(0, 339.074, 0) )
	Create_RevolverAmmo( Vector(87.785, -285.628, 7.495), Angle(0, 18.674, 0) )
	Create_EldritchArmour10( Vector(469.117, -8.244, 7.83), Angle(0, 121.669, 0) )
	Create_EldritchArmour10( Vector(442.028, -9.147, 7.83), Angle(0, 86.248, 0) )
	Create_EldritchArmour10( Vector(412.612, -8.309, 7.83), Angle(0, 51.048, 0) )
	Create_SpiritEye1( Vector(127.183, -86.285, -15.969), Angle(0, 93.588, 0) )
	Create_Backpack( Vector(-52.853, -18.836, -15.969), Angle(0, 145.368, 0) )
	Create_Medkit25( Vector(-383.251, 410.408, 112.031), Angle(0, 90.328, 0) )
	Create_Medkit25( Vector(-484.667, 411.121, 112.031), Angle(0, 90.108, 0) )
	Create_Medkit10( Vector(-392.892, 370.507, 112.031), Angle(0, 271.028, 0) )
	Create_Medkit10( Vector(-370.197, 366.678, 112.031), Angle(0, 256.948, 0) )
	Create_Beer( Vector(403.527, -305.576, 240.031), Angle(0, 153.857, 0) )
	Create_Beer( Vector(384.391, -304.361, 240.031), Angle(0, 148.357, 0) )
	Create_Beer( Vector(367.767, -304.122, 240.031), Angle(0, 140.877, 0) )
	Create_Beer( Vector(351.924, -303.985, 240.031), Angle(0, 130.317, 0) )
	Create_Armour( Vector(1374.29, -276.73, 368.031), Angle(0, 211.495, 0) )
	Create_CorzoHat( Vector(4844.865, -4188.778, 683.419), Angle(0, 142.526, 0) )
	Create_SuperShotgunWeapon( Vector(2330.584, -3984.929, 369.185), Angle(0, 270.206, 0) )
	Create_ShotgunAmmo( Vector(2330.516, -4010.271, 369.175), Angle(0, 270.646, 0) )
	Create_PistolWeapon( Vector(719.338, -570.874, 368.031), Angle(0, 0.217, 0) )
	Create_PistolAmmo( Vector(724.84, -589.768, 368.031), Angle(0, 15.837, 0) )
	Create_PistolAmmo( Vector(730.291, -555.845, 368.031), Angle(0, 328.097, 0) )
	Create_Medkit25( Vector(1146.334, -467.346, 360.184), Angle(0, 274.615, 0) )
	Create_Medkit25( Vector(1201.982, -469.366, 360.293), Angle(0, 262.075, 0) )
	Create_PistolAmmo( Vector(1044.723, 310.349, 391.83), Angle(0, 129.072, 0) )
	Create_PistolWeapon( Vector(993.779, 310.049, 391.83), Angle(0, 60.432, 0) )
	Create_Medkit25( Vector(901.328, -78.227, 368.031), Angle(0, 94.172, 0) )
	Create_PortableMedkit( Vector(979.267, -70.918, 391.83), Angle(0, 157.532, 0) )
	Create_PortableMedkit( Vector(1039.406, 399.764, 368.031), Angle(0, 213.272, 0) )
	Create_RevolverAmmo( Vector(430, 1079.969, 432.031), Angle(0, 262.552, 0) )
	Create_Medkit10( Vector(400.122, 1081.874, 432.031), Angle(0, 270.032, 0) )
	Create_Medkit25( Vector(98.698, 236.918, 464.031), Angle(0, 89.318, 0) )
	Create_Medkit10( Vector(-97.994, 1386.932, 240.031), Angle(0, 290.758, 0) )
	Create_Medkit10( Vector(-79.597, 1375.922, 240.031), Angle(0, 239.718, 0) )
	Create_PistolWeapon( Vector(206.797, 1404.68, 464.031), Angle(0, 159.384, 0) )
	Create_Medkit10( Vector(212.604, 1471.413, 464.031), Angle(0, 206.024, 0) )
	Create_RevolverAmmo( Vector(285.515, 370.15, 464.031), Angle(0, 194.32, 0) )
	Create_Medkit10( Vector(282.787, 280.286, 464.031), Angle(0, 158.68, 0) )
	Create_VolatileBattery( Vector(45.464, 743.041, 464.031), Angle(0, 230.92, 0) )
	
	local B_FUNC = function()
		Create_MegaSphere( Vector(268.864, 405.462, 256.031), Angle(0, 238.396, 0) )
		Create_SMGWeapon( Vector(241.411, 386.144, 256.031), Angle(0, 276.236, 0) )
		Create_SMGAmmo( Vector(218.557, 362.4, 256.031), Angle(0, 244.116, 0) )
		Create_SMGAmmo( Vector(267.03, 364.239, 256.031), Angle(0, 217.496, 0) )
	end
	
	AddSecretButton( Vector(272.065,229.194,280.485), B_FUNC )
	
	CreateProp(Vector(278.656,257.938,266.594), Angle(-20.127,179.473,0), "models/props_trainstation/tracksign10.mdl", true)
	
	CreateSecretArea( Vector(-61.697,-14.477,-11.437), Vector(-21.103,-21.103,0), Vector(21.103,21.103,53.912) )
	CreateSecretArea( Vector(411.677,33.09,-15.969), Vector(-63.931,-63.931,0), Vector(63.931,63.931,104.899) )
	CreateSecretArea( Vector(4839.675,-4187.213,682.996), Vector(-79.843,-79.843,0), Vector(79.843,79.843,67.973) )
	CreateSecretArea( Vector(2329.974,-3997.203,373.688), Vector(-47.995,-47.995,0), Vector(47.995,47.995,67.528) )
	CreateSecretArea( Vector(41.529,738.798,468.563), Vector(-18.739,-18.739,0), Vector(18.739,18.739,54.611) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)