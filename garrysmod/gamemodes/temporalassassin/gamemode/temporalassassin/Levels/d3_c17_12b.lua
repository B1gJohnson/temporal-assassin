	
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	CreatePlayerSpawn( Vector(-8371.89453125,-1336.1008300781,-127.96875), Angle(0,180,0) )
	
	Create_PortableMedkit( Vector(-8288.390625, -1708.3745117188, 0.03125), Angle(0, 301.95715332031, 0) )
	Create_PortableMedkit( Vector(-8237.2314453125, -1699.4329833984, 0.03125), Angle(0, 270.27697753906, 0) )
	Create_PortableMedkit( Vector(-8189.673828125, -1702.8679199219, 0.03125), Angle(0, 241.89686584473, 0) )
	Create_ManaCrystal100( Vector(-5653.7373046875, 210.02447509766, -319.96875), Angle(0, 268.34469604492, 0) )
	Create_M6GAmmo( Vector(-5731.7319335938, 170.96656799316, -319.96875), Angle(0, 6.0247497558594, 0) )
	Create_M6GAmmo( Vector(-5725.5463867188, 246.92080688477, -319.96875), Angle(0, 325.10455322266, 0) )
	Create_MaskOfShadows( Vector(-5691.8471679688, 213.49673461914, -319.96875), Angle(0, 272.30450439453, 0) )
	Create_RevolverWeapon( Vector(-4294.9619140625, 109.62249755859, -287.96875), Angle(0, 90.255516052246, 0) )
	Create_RevolverAmmo( Vector(-4267.2841796875, 130.61877441406, -287.96875), Angle(0, 106.53559112549, 0) )
	Create_PistolWeapon( Vector(-4270.5395507813, 294.57693481445, -287.96875), Angle(0, 196.95558166504, 0) )
	Create_Medkit25( Vector(-5198.294921875, 337.04089355469, -191.96875), Angle(0, 288.77542114258, 0) )
	Create_Medkit25( Vector(-5231.0209960938, 259.67587280273, -191.96875), Angle(0, 350.81561279297, 0) )
	Create_Medkit10( Vector(-5048.0922851563, 334.86010742188, -191.96875), Angle(0, 215.29560852051, 0) )
	Create_Medkit10( Vector(-5092.1484375, 338.30767822266, -191.96875), Angle(0, 229.26564025879, 0) )
	Create_Medkit10( Vector(-5141.40625, 340.41091918945, -191.96875), Angle(0, 253.35572814941, 0) )
	Create_SpiritSphere( Vector(-5457.1196289063, -221.11201477051, -191.96875), Angle(0, 85.164848327637, 0) )
	Create_Beer( Vector(-5255.119140625, 292.99765014648, 175.48500061035), Angle(0, 269.82434082031, 0) )
	Create_Beer( Vector(-5255.2944335938, 235.79043579102, 160.25816345215), Angle(0, 269.82434082031, 0) )
	Create_Beer( Vector(-5255.5532226563, 151.1309967041, 137.72438049316), Angle(0, 269.82434082031, 0) )
	Create_Beer( Vector(-5255.8051757813, 69.526992797852, 116.00388336182), Angle(0, 269.82434082031, 0) )
	Create_Beer( Vector(-5256.1831054688, -54.099624633789, 83.098220825195), Angle(0, 269.82434082031, 0) )
	Create_Beer( Vector(-5256.4155273438, -129.31903076172, 63.077102661133), Angle(0, 269.82434082031, 0) )
	Create_Beer( Vector(-5256.8110351563, -258.71667480469, 28.635381698608), Angle(0, 269.82434082031, 0) )
	Create_Beer( Vector(-5256.7470703125, -329.80432128906, 9.7139015197754), Angle(0, 269.60433959961, 0) )
	Create_ShotgunAmmo( Vector(-4598.4077148438, 326.55535888672, 192.03125), Angle(0, 200.74424743652, 0) )
	Create_ShotgunAmmo( Vector(-4587.177734375, 276.82180786133, 192.03125), Angle(0, 180.94424438477, 0) )
	Create_SMGAmmo( Vector(-4641.169921875, 310.35913085938, 192.03125), Angle(0, 200.74423217773, 0) )
	Create_SMGAmmo( Vector(-4633.6953125, 271.72479248047, 192.03125), Angle(0, 178.52424621582, 0) )
	Create_Backpack( Vector(-4332.2905273438, 209.84014892578, 288.03125), Angle(0, 358.484375, 0) )
	
	CreateSecretArea( Vector(-5454.2114257813,-219.28425598145,-191.96875), Vector(-49.724681854248,-49.724681854248,0), Vector(49.724681854248,49.724681854248,92.268203735352) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)