		
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	CreateClothesLocker( Vector(6327.833,4313.251,-287.969), Angle(0,337.399,0) )
	CreatePlayerSpawn( Vector(6404.419921875,2843.7199707031,-303.96875), Angle(0,90,0) )
	
	local P1 = CreateProp(Vector(6454.656,5926.781,-252.219), Angle(0,89.956,0), "models/props_docks/channelmarker_gib03.mdl", true)
	local P2 = CreateProp(Vector(6344.469,5923.375,-247.437), Angle(0,90,0), "models/props_docks/channelmarker_gib03.mdl", true)
	P1:SetHealth(50000)
	P2:SetHealth(50000)
	
	local LORE_1 = function()
	
		local P3 = CreateProp(Vector(6641.875,4350.625,-235.469), Angle(0,-147.349,0), "models/props_wasteland/cargo_container01b.mdl", true)
		local P4 = CreateProp(Vector(6684.719,4322.313,-281.687), Angle(3.384,121.816,-0.22), "models/props_lab/blastdoor001a.mdl", true)
		local P5 = CreateProp(Vector(6648.719,4302.188,-281.75), Angle(1.538,121.816,-0.176), "models/props_lab/blastdoor001a.mdl", true)
		local P6 = CreateProp(Vector(6737.563,4203.281,-286.062), Angle(0.044,123.047,0), "models/props_combine/breenchair.mdl", true)
		local P7 = CreateProp(Vector(6735.75,4206.781,-266.094), Angle(4.79,122.432,-0.132), "models/temporalassassin/doll/unknown.mdl", true)
		local P8 = CreateProp(Vector(6640.063,4270.375,-264.969), Angle(-0.176,-10.986,-0.088), "models/props_interiors/furniture_chair01a.mdl", true)
		local P9 = CreateProp(Vector(6628.031,4261.906,-257.844), Angle(6.987,-2.285,82.09), "models/temporalassassin/props/corzo_cintia1901.mdl", true)
		local P10 = CreateProp(Vector(6640.313,4269.906,-264.406), Angle(0.088,-85.342,-0.967), "models/temporalassassin/items/corzohat.mdl", true)
		local P11 = CreateProp(Vector(6715.531,4316,-266.656), Angle(0,-113.73,0.132), "models/props_interiors/furniture_chair03a.mdl", true)
		local P12 = CreateProp(Vector(6709.688,4307.594,-263.687), Angle(67.896,20.786,-83.408), "models/temporalassassin/items/phial.mdl", true)
		local P13 = CreateProp(Vector(6717.406,4312.406,-263.719), Angle(24.565,-77.783,0.264), "models/temporalassassin/items/weapon_griseus.mdl", true)
		P4:SetRenderMode(RENDERMODE_TRANSALPHA)
		P5:SetRenderMode(RENDERMODE_TRANSALPHA)
		P4:SetColor(Color(0,0,0,0))
		P5:SetColor(Color(0,0,0,0))
		P8:SetHealth(5000)
		
		local LORE_2 = function(self,ply)
		
			if TAS:GetSuit(ply) == 12 or ply.Eldritch_Buff then
				
				net.Start("GriseusMessage")
				net.WriteString("Tell me a good reason why a low-life like you would have the need for Kit.")
				net.Broadcast()
				
				timer.Simple(5,function()
					net.Start("CorzoMessage")
					net.WriteString("Strictly business, there are some things that even i can't take care of by myself.")
					net.Broadcast()
				end)
				
				timer.Simple(10,function()
					net.Start("DollMessage")
					net.WriteString("Intruiging, i thought you boycows were all about doing it yourself")
					net.Broadcast()
				end)
				
				timer.Simple(15,function()
					net.Start("CorzoMessage")
					net.WriteString("It's Cowboy, Ma'am, i do keep to myself but this is something i need a little help with.")
					net.Broadcast()
				end)
				
				timer.Simple(20,function()
					net.Start("GriseusMessage")
					net.WriteString("A little help that'll turn her gradually into a murder-craving psychopath.")
					net.Broadcast()
				end)
				
				timer.Simple(25,function()
					net.Start("DollMessage")
					net.WriteString("Kit's existance is wrapped in existential chaos, it's her nature to kill, Our nature")
					net.Broadcast()
				end)
				
				timer.Simple(30,function()
					net.Start("CorzoMessage")
					net.WriteString("What do you mean by 'Our nature'?")
					net.Broadcast()
				end)
				
				timer.Simple(35,function()
					net.Start("DollMessage")
					net.WriteString("All of us, even you yourself Cowboy, you need to commit to it in order to survive")
					net.Broadcast()
				end)
				
				timer.Simple(40,function()
					net.Start("CorzoMessage")
					net.WriteString("So ya know all about that, what about prim and proper over here?")
					net.Broadcast()
				end)
				
				timer.Simple(45,function()
					net.Start("GriseusMessage")
					net.WriteString("I have a name, asshat. And to me, unlike for the likes of you, killing is not a be all, end all solution to everything.")
					net.Broadcast()
				end)
				
				timer.Simple(50,function()
					net.Start("DollMessage")
					net.WriteString("Enough, Yes i do know about that, however that doesn't change what it is you require of my daughter")
					net.Broadcast()
				end)
				
				timer.Simple(55,function()
					net.Start("GriseusMessage")
					net.WriteString("Even if her job is HVT elimination and other tasks, it does not justify homicide on a regional level.")
					net.Broadcast()
				end)
				
				timer.Simple(60,function()
					net.Start("CorzoMessage")
					net.WriteString("Jobs are hard ta come by sometimes, if you're good at somethin' you should probably stick to it.")
					net.Broadcast()
				end)
				
				timer.Simple(65,function()
					net.Start("DollMessage")
					net.WriteString("Get to the point Cowboy")
					net.Broadcast()
				end)
				
				timer.Simple(70,function()
					net.Start("CorzoMessage")
					net.WriteString("I need to get a little something that was taken from me, but i can't track it down as easily as i thought.")
					net.Broadcast()
				end)
				
				timer.Simple(75,function()
					net.Start("CorzoMessage")
					net.WriteString("She can travel to other places on a whim, anything she holds or looks over she can identify in mere seconds.")
					net.Broadcast()
				end)
				
				timer.Simple(80,function()
					net.Start("GriseusMessage")
					net.WriteString("That information is classified to personel outside of the Agency. Who shown you the notes?")
					net.Broadcast()
				end)
				
				timer.Simple(85,function()
					net.Start("CorzoMessage")
					net.WriteString("She showed me them herself, likely in return for teaching her a couple of things with that there 'Wolf' 6 shooter")
					net.Broadcast()
				end)
				
				timer.Simple(90,function()
					net.Start("DollMessage")
					net.WriteString("Enough bickering, i am intrigued")
					net.Broadcast()
				end)
				
				timer.Simple(95,function()
					net.Start("DollMessage")
					net.WriteString("You may set up a contract when you see fit, we are done for now")
					net.Broadcast()
				end)
				
				timer.Simple(100,function()
					net.Start("GriseusMessage")
					net.WriteString("Don't come crying to me when Kit steps out of line to satisfy her lust for blood.")
					net.Broadcast()
				end)
				
				timer.Simple(105,function()
					net.Start("CorzoMessage")
					net.WriteString("Thank ya kindly Ma'am, i'll get right on that")
					net.Broadcast()
				end)
				
				timer.Simple(110,function()
					net.Start("CorzoMessage")
					net.WriteString("Besides, comin cryin would imply these people don't deserve to be ended in the first place.")
					net.Broadcast()
				end)
			
			end
		
		end
		
		CreateTrigger( Vector(6652.681,4334.167,-286.383), Vector(-42.022,-42.022,0), Vector(42.022,42.022,95.392), LORE_2 )
	
	end
	
	AddSecretButton( Vector(6402.48,5951.969,-249.807), LORE_1, "A VEIL HAS BEEN LIFTED", "TemporalAssassin/Secrets/Secret_Spooky.wav" )
	
	local GAME_END_TXT = {
	"Kit found her way to the metal pods that would take her up to murder",
	"the big bad white beardo man Walace Mc-Breeno, the third.",
	"",
	"",
	"Sadly some guy in an orange suit shoved her out of the way to steal",
	"her ride up to the top of the citadel, must be the one they mentioned.",
	"",
	"",
	"Who knows where kit will end up, next time in Temporal Assassin."}	
	
	MakeGameEnd( GAME_END_TXT, 30, Vector(10244.782226563,4554.5390625,-1791.96875), Vector(-349.70959472656,-349.70959472656,0), Vector(349.70959472656,349.70959472656,188.83325195313) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)