		
if CLIENT then return end

GLOB_SCENE_SKIP_AVAILABLE = true

GLOB_REPLACE_CRUENTUS_DEMON = true

LEVEL_STRINGS["hls01amrl"] = {"hls02amrl",Vector(-2177.9230957031,931.10064697266,-1583), Vector(-126.76340484619,-126.76340484619,0), Vector(126.76340484619,126.76340484619,155.29174804688)}

function MAP_LOADED_FUNC()

	RemoveAllEnts("trigger_changelevel")
	
	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end
	
	CreateClothesLocker( Vector(-1067.8115234375,4968.12109375,728.03125), Angle(0,270.54382324219,0) )

	Create_RevolverWeapon( Vector(-1242.1881103516, 4695.41015625, 732.03125), Angle(0, 347.94790649414, 0) )
	Create_RevolverAmmo( Vector(1351.0913085938, 3424.1052246094, 912.03125), Angle(0, 0.814453125, 0) )
	Create_RevolverAmmo( Vector(1398.5743408203, 3423.8132324219, 912.03125), Angle(0, 2.1344604492188, 0) )
	Create_PistolAmmo( Vector(1372.2749023438, 3424.0307617188, 912.03125), Angle(0, 359.05444335938, 0) )
	Create_Beer( Vector(2056.6640625, 2033.0731201172, 800.03125), Angle(0, 129.12690734863, 0) )
	Create_Beer( Vector(2076.7241210938, 2064.42578125, 800.03125), Angle(0, 158.82696533203, 0) )
	Create_Beer( Vector(2055.142578125, 2056.5795898438, 800.03125), Angle(0, 143.20693969727, 0) )
	Create_Beer( Vector(1856.3459472656, 195.58158874512, 128.03125), Angle(0, 355.53494262695, 0) )
	Create_PortableMedkit( Vector(377.76260375977, 410.17208862305, -127.96875), Angle(0, 268.0354309082, 0) )
	
	CreateSecretArea( Vector(-1226.6481933594,4687.8286132813,736.5625), Vector(-17.840925216675,-17.840925216675,0), Vector(17.840925216675,17.840925216675,31.440795898438) )
	CreateSecretArea( Vector(1372.5787353516,3424.1499023438,912.03125), Vector(-18.826221466064,-18.826221466064,0), Vector(18.826221466064,18.826221466064,14.345642089844) )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)