		
if CLIENT then return end

LEVEL_STRINGS["ep1_c17_02"] = {"ep1_c17_02b",Vector(-2595.8637695313,2406.9587402344,-127.96875), Vector(-101.05674743652,-101.05674743652,0), Vector(101.05674743652,101.05674743652,119.9375)}

function EXTRA_FAIL_STATE( npc, att, inf )

	if IsValid(npc) and ( npc:GetClass() == "npc_alyx" ) then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRA_FAIL_STATEHook",EXTRA_FAIL_STATE)

if not G_MAP_CLEANED then
	NPC_NO_DUPES = true
	G_MAP_CLEANED = false
	G_MAP_CLEANDELAY = 10
end

function MAP_DO_RESET( ply )

	NPC_NO_DUPES = true
	G_MAP_CLEANED = false
	G_MAP_CLEANDELAY = 1.5

end
hook.Add("PlayerDeath","MAP_DO_RESETHook",MAP_DO_RESET)

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("trigger_changelevel")
	
	if G_MAP_CLEANED then
	
		if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
		
			Create_SMGAmmo( Vector(1445.7255859375, -599.97290039063, 16.03125), Angle(0, 236.09957885742, 0) )
			Create_SMGAmmo( Vector(1405.9920654297, -602.04534912109, 16.03125), Angle(0, 260.51971435547, 0) )
			Create_SMGWeapon( Vector(1349.3306884766, -602.02178955078, 16.031246185303), Angle(0, 276.13977050781, 0) )
			Create_SMGAmmo( Vector(1150.6285400391, -538.55145263672, 16.03125), Angle(0, 21.380065917969, 0) )
			Create_SMGAmmo( Vector(1150.0255126953, -469.1943359375, 16.03125), Angle(0, 332.0998840332, 0) )
			Create_Medkit25( Vector(1145.3931884766, -502.80319213867, 16.031246185303), Angle(0, 1.3600463867188, 0) )
			Create_Medkit10( Vector(1538.5109863281, -663.93353271484, 16.03125), Angle(0, 106.51218414307, 0) )
			Create_KingSlayerWeapon( Vector(1273.1649169922, -104.85919952393, 54.03125), Angle(0, 357.83221435547, 0) )
			Create_KingSlayerAmmo( Vector(1271.9759521484, 15.358260154724, 54.03125), Angle(0, 359.59216308594, 0) )
			Create_KingSlayerAmmo( Vector(1255.5930175781, 13.640628814697, 54.03125), Angle(0, 182.71202087402, 0) )
			Create_PortableMedkit( Vector(1484.2746582031, -103.77372741699, 49.03125), Angle(0, 170.83210754395, 0) )
			Create_Beer( Vector(1481.0203857422, -28.169212341309, 49.03125), Angle(0, 162.47212219238, 0) )
			Create_Beer( Vector(1480.6778564453, -7.6321249008179, 49.03125), Angle(0, 200.75213623047, 0) )
			Create_SMGAmmo( Vector(1144.8959960938, -396.97525024414, 56.03125), Angle(0, 14.411956787109, 0) )
			Create_SMGAmmo( Vector(1149.5739746094, -371.05453491211, 56.03125), Angle(0, 335.25173950195, 0) )
			Create_SMGAmmo( Vector(1149.1845703125, -345.03308105469, 56.03125), Angle(0, 309.73162841797, 0) )
			Create_SMGWeapon( Vector(1164.4031982422, -368.97268676758, 16.03125), Angle(0, 0.77159118652344, 0) )
			Create_Armour( Vector(1084.283203125, -426.34860229492, 16.03125), Angle(0, 103.608543396, 0) )
			Create_Armour( Vector(1041.24609375, -432.42044067383, 16.03125), Angle(0, 81.388458251953, 0) )
			Create_PistolAmmo( Vector(924.09478759766, -517.37530517578, 16.031246185303), Angle(0, 117.02805328369, 0) )
			Create_PistolAmmo( Vector(859.95257568359, -521.93896484375, 16.031246185303), Angle(0, 83.808013916016, 0) )
			Create_PistolAmmo( Vector(801.76818847656, -516.6767578125, 16.031253814697), Angle(0, 55.647933959961, 0) )
			Create_SpiritSphere( Vector(666.24365234375, -84.523162841797, 144.03125), Angle(0, 89.747451782227, 0) )
			Create_ManaCrystal( Vector(-569.75775146484, 891.68792724609, 0.031253814697266), Angle(0, 190.45620727539, 0) )
			Create_ManaCrystal( Vector(-567.16711425781, 831.43725585938, 0.03125), Angle(0, 161.41619873047, 0) )
			Create_SMGAmmo( Vector(-607.94732666016, 894.65515136719, 0.03125), Angle(0, 199.80619812012, 0) )
			Create_SMGAmmo( Vector(-613.11669921875, 826.86730957031, 0.03125), Angle(0, 166.03619384766, 0) )
			Create_ShotgunAmmo( Vector(-647.62744140625, 889.89929199219, 0.03125), Angle(0, 206.07620239258, 0) )
			Create_ShotgunAmmo( Vector(-644.42547607422, 829.22106933594, 0.031253814697266), Angle(0, 162.29618835449, 0) )
			Create_MegaSphere( Vector(-1159.5671386719, -973.94006347656, 768.03125), Angle(0, 302.29522705078, 0) )
			Create_Backpack( Vector(-1298.1040039063, -935.07000732422, 768.03125), Angle(0, 321.21530151367, 0) )
			Create_Backpack( Vector(-1144.6340332031, -1060.8270263672, 768.03125), Angle(0, 322.31530761719, 0) )
			Create_Backpack( Vector(-1177.1597900391, -816.74450683594, 768.03125), Angle(0, 289.97528076172, 0) )
			Create_Backpack( Vector(-1069.6071777344, -1002.2528686523, 768.03125), Angle(0, 271.49520874023, 0) )
			Create_AnthrakiaWeapon( Vector(-1282.4104003906, -1076.2985839844, 768.03125), Angle(0, 339.91528320313, 0) )
			Create_MegaSphere( Vector(-1159.5671386719, -973.94006347656, 768.03125), Angle(0, 302.29522705078, 0) )
			Create_Backpack( Vector(-1298.1040039063, -935.07000732422, 768.03125), Angle(0, 321.21530151367, 0) )
			Create_Backpack( Vector(-1144.6340332031, -1060.8270263672, 768.03125), Angle(0, 322.31530761719, 0) )
			Create_Backpack( Vector(-1177.1597900391, -816.74450683594, 768.03125), Angle(0, 289.97528076172, 0) )
			Create_Backpack( Vector(-1069.6071777344, -1002.2528686523, 768.03125), Angle(0, 271.49520874023, 0) )
			Create_AnthrakiaWeapon( Vector(-1282.4104003906, -1076.2985839844, 768.03125), Angle(0, 339.91528320313, 0) )
			Create_Armour( Vector(-505.77774047852, 1357.041015625, 0.03125), Angle(0, 182.77236938477, 0) )
			Create_PortableMedkit( Vector(-1557.1794433594, 1942.7165527344, -127.96875), Angle(0, 125.06994628906, 0) )
			Create_Medkit25( Vector(-1619.9814453125, 1947.4217529297, -127.96875), Angle(0, 102.18996429443, 0) )
			Create_Medkit25( Vector(-1695.134765625, 1947.3897705078, -127.96875), Angle(0, 68.309951782227, 0) )
			Create_ShotgunAmmo( Vector(-1941.3790283203, 2191.8676757813, -127.96875), Angle(0, 319.62963867188, 0) )
			Create_SMGAmmo( Vector(-1939.3244628906, 2154.3405761719, -127.96875), Angle(0, 341.40979003906, 0) )
			Create_SMGAmmo( Vector(-1939.0943603516, 2117.8427734375, -127.96875), Angle(0, 10.669876098633, 0) )
			Create_KingSlayerAmmo( Vector(-1843.7850341797, 1417.6342773438, -543.96875), Angle(0, 269.46334838867, 0) )
			Create_KingSlayerAmmo( Vector(-1843.5849609375, 1263.6937255859, -543.96875), Angle(0, 90.603324890137, 0) )
			Create_Medkit25( Vector(-2338.3041992188, 1177.2047119141, -535.96875), Angle(0, 7.2227935791016, 0) )
			Create_Medkit25( Vector(-2337.2407226563, 1227.9415283203, -535.96875), Angle(0, 335.10266113281, 0) )
			Create_PortableMedkit( Vector(-2072.4064941406, 1488.7644042969, -535.96875), Angle(0, 249.52261352539, 0) )
			Create_SuperShotgunWeapon( Vector(-2190.5317382813, 1070.8248291016, -535.96875), Angle(0, 90.022666931152, 0) )
			Create_ShotgunAmmo( Vector(-2222.1589355469, 1099.3249511719, -535.96875), Angle(0, 65.382553100586, 0) )
			Create_ShotgunAmmo( Vector(-2159.4379882813, 1102.2843017578, -535.96875), Angle(0, 115.10261535645, 0) )
			Create_Armour( Vector(-1950.6201171875, 1202.6329345703, -311.96875), Angle(0, 269.25958251953, 0) )
			Create_Backpack( Vector(-1871.4729003906, 1445.3618164063, 76.03125), Angle(0, 179.93948364258, 0) )
			Create_FalanrathThorn( Vector(-365.64318847656, -380.17999267578, 0.031246185302734), Angle(0, 0.47245788574219, 0) )
			
			CreateSecretArea( Vector(-1204.4674072266,-1002.0119628906,768.03125), Vector(-317.91125488281,-317.91125488281,0), Vector(317.91125488281,317.91125488281,219.83843994141) )
			
		else
		
			Create_GrenadeAmmo( Vector(1259.19, -114.917, 54.141), Angle(0, 323, 0) )
			Create_GrenadeAmmo( Vector(1264, -106, 57.282), Angle(0, 262, 0) )
			Create_GrenadeAmmo( Vector(1268, -98, 60.424), Angle(0, 35, 0) )
			Create_Armour200( Vector(1632.386, 248.602, 16.031), Angle(0, 166.903, 0) )
			Create_GrenadeAmmo( Vector(1634.117, -681.646, 16.031), Angle(0, 88.006, 0) )
			Create_GrenadeAmmo( Vector(1578.089, -650.706, 16.031), Angle(0, 59.582, 0) )
			Create_Armour5( Vector(1143.987, -464.152, 16.031), Angle(0, 331.384, 0) )
			Create_Armour5( Vector(1178.747, -464.676, 16.031), Angle(0, 311.32, 0) )
			Create_Armour5( Vector(1140.267, -500.388, 16.031), Angle(0, 0.226, 0) )
			Create_Beer( Vector(1139.608, -521.498, 16.031), Angle(0, 0.226, 0) )
			Create_Beer( Vector(1140.331, -550.027, 16.031), Angle(0, 22.38, 0) )
			Create_Beer( Vector(1178.279, -551.587, 16.031), Angle(0, 43.489, 0) )
			Create_Medkit25( Vector(1169.842, -505.517, 16.031), Angle(0, 358.763, 0) )
			Create_KingSlayerWeapon( Vector(1211.718, -507.135, 16.031), Angle(0, 359.39, 0) )
			Create_PortableMedkit( Vector(1532.138, -659.555, 16.031), Angle(0, 100.544, 0) )
			Create_RevolverWeapon( Vector(1497.641, -657.908, 16.031), Angle(0, 75.255, 0) )
			Create_PistolAmmo( Vector(1488.198, -628.723, 16.031), Angle(0, 57.072, 0) )
			Create_PistolAmmo( Vector(1530.35, -631.295, 16.031), Angle(0, 105.769, 0) )
			Create_FoolsOathAmmo( Vector(1146.729, -148.449, 16.031), Angle(0, 226.333, 0) )
			Create_RifleWeapon( Vector(1201.726, -149.18, 16.031), Angle(0, 270.98, 0) )
			Create_RifleAmmo( Vector(1265.461, -144.804, 16.031), Angle(0, 237.54, 0) )
			Create_RifleAmmo( Vector(1234.981, -171.627, 16.031), Angle(0, 259.485, 0) )
			Create_KingSlayerAmmo( Vector(1263.288, -654.923, 16.031), Angle(0, 46.305, 0) )
			Create_ShadeCloak( Vector(1389.505, -633.105, 16.031), Angle(0, 359.178, 0) )
			Create_Medkit25( Vector(808.809, -512.699, 16.031), Angle(0, 38.679, 0) )
			Create_Medkit25( Vector(805.872, -428.015, 16.031), Angle(0, 326.782, 0) )
			Create_CorzoHat( Vector(481.857, -471.812, 16.031), Angle(0, 358.825, 0) )
			Create_FalanrathThorn( Vector(-142.029, 386.833, 0.031), Angle(0, 351.511, 0) )
			Create_ManaCrystal100( Vector(-88.738, 300.991, 0.031), Angle(0, 5.514, 0) )
			Create_ManaCrystal100( Vector(-94.412, 488.629, 4.031), Angle(0, 357.363, 0) )
			Create_Medkit25( Vector(-39.77, 342.964, 0.031), Angle(0, 346.704, 0) )
			Create_Medkit25( Vector(-45.459, 431.402, 0.031), Angle(0, 12.62, 0) )
			Create_Armour100( Vector(-7.803, 390.468, 0.031), Angle(0, 4.26, 0) )
			Create_SMGAmmo( Vector(96.785, 491.322, 4.031), Angle(0, 258.924, 0) )
			Create_ShotgunAmmo( Vector(62.032, 488.707, 4.031), Angle(0, 263.94, 0) )
			Create_RevolverAmmo( Vector(26.949, 487.193, 4.031), Angle(0, 262.895, 0) )
			Create_PistolAmmo( Vector(-13.351, 488.718, 4.031), Angle(0, 274.808, 0) )
			Create_Beer( Vector(-622.057, 522.252, 4.031), Angle(0, 262.766, 0) )
			Create_Beer( Vector(-677.416, 519.767, 4.031), Angle(0, 283.457, 0) )
			Create_Beer( Vector(-679.733, 576.883, 4.031), Angle(0, 276.978, 0) )
			Create_Armour5( Vector(-1385.391, 518.759, 4.031), Angle(0, 3.295, 0) )
			Create_Armour5( Vector(-1334.105, 519.484, 4.031), Angle(0, 6.221, 0) )
			Create_Armour5( Vector(-1336.385, 574.135, 4.031), Angle(0, 350.755, 0) )
			Create_EldritchArmour10( Vector(-1983.076, 1270.693, 329.549), Angle(0, 3.729, 0) )
			Create_EldritchArmour10( Vector(-1938.851, 1272.348, 329.549), Angle(0, 354.95, 0) )
			Create_EldritchArmour10( Vector(-1898.688, 1270.853, 329.549), Angle(0, 343.246, 0) )
			
			local BP1 = CreateProp(Vector(880.375,12.75,153.219), Angle(0.044,-173.013,-0.132), "models/props_combine/breenglobe.mdl", true)
			local BP2 = CreateProp(Vector(880.094,-61.187,153.25), Angle(-0.044,102.437,-0.571), "models/props_combine/breenglobe.mdl", true)
			BP1:SetPhysicsWeight(5000)
			BP2:SetPhysicsWeight(5000)
			
			local SBUTTON1 = function()
			
				local START = Vector(786.142,-474.891,103.148)
				local END = Vector(739.676,-475.294,104.882)
				local DOOR = GetObjectFromTrace( START, END, MASK_SHOT )
				
				if DOOR and IsValid(DOOR) then
					DOOR:Fire("Unlock")
					DOOR:Fire("Open")
				end
			
			end
			
			AddSecretButton( Vector(887.969,-25.266,199.526), SBUTTON1 )
		
			CreateSecretArea( Vector(1631.049,255.511,20.563), Vector(-23.067,-23.067,0), Vector(23.067,23.067,33.539) )
			CreateSecretArea( Vector(-1935.854,1272.084,334.063), Vector(-63.204,-63.204,0), Vector(63.204,63.204,68.498) )
		
		end
		
	end
	
	timer.Simple( G_MAP_CLEANDELAY, function()
	
		if not G_MAP_CLEANED then
			G_MAP_CLEANED = true
			NPC_NO_DUPES = false
			game.CleanUpMap()
		end
	
	end)

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)