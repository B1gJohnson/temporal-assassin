
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	CreatePlayerSpawn( Vector(155.699,4453.863,-1407.969), Angle(0,-140,0) )
	CreateClothesLocker( Vector(-890.11138916016,3200.4458007813,-1279.96875), Angle(0,47.701324462891,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Armour( Vector(-141.63230895996, 2667.4150390625, -1279.96875), Angle(0, 20.201202392578, 0) )
		Create_Armour( Vector(-129.83438110352, 2817.505859375, -1279.96875), Angle(0, 312.6608581543, 0) )
		Create_Medkit25( Vector(-142.7042388916, 2721.8999023438, -1279.96875), Angle(0, 3.0408325195313, 0) )
		Create_Medkit25( Vector(-140.81463623047, 2762.1401367188, -1279.96875), Angle(0, 0.40089416503906, 0) )
		Create_CrossbowWeapon( Vector(912.57629394531, 1815.1866455078, -2735.96875), Angle(0, 135.30529785156, 0) )
		Create_CrossbowAmmo( Vector(922.75964355469, 1874.2177734375, -2735.96875), Angle(0, 179.52528381348, 0) )
		Create_CrossbowAmmo( Vector(862.89154052734, 1794.1469726563, -2735.96875), Angle(0, 97.465301513672, 0) )
		Create_CrossbowAmmo( Vector(878.00994873047, 1848.1879882813, -2735.96875), Angle(0, 133.98522949219, 0) )
		
		Create_Backpack( Vector(77.895088195801, 2418.0476074219, -2735.96875), Angle(0, 1.2879943847656, 0) )
		
		CreateSecretArea( Vector(913.45532226563,1819.9165039063,-2735.96875), Vector(-46.859188079834,-46.859188079834,0), Vector(46.859188079834,46.859188079834,82.004150390625) )
		CreateSecretArea( Vector(69.756828308105,2415.8208007813,-2735.96875), Vector(-10.255058288574,-10.255058288574,0), Vector(10.255058288574,10.255058288574,31.348876953125) )
		
		CreateProp(Vector(439.09375,2084.5,-1552.59375), Angle(0,-90.3515625,0), "models/temporalassassin/doll/unknown.mdl", true)
		
	else
	
		CreateProp(Vector(439.09375,2084.5,-1552.59375), Angle(0,-90.3515625,0), "models/temporalassassin/doll/unknown.mdl", true)
		
		Create_Medkit25( Vector(-148.837, 2658.976, -1279.969), Angle(0, 359.664, 0) )
		Create_Medkit25( Vector(-150.246, 2831.988, -1279.969), Angle(0, 359.664, 0) )
		Create_InfiniteGrenades( Vector(69.731, 2693.573, -1247.969), Angle(0, 37.182, 0) )
		Create_FrontlinerAmmo( Vector(352.882, 3704.779, -1406.566), Angle(0, -5.197, -58.12) )
		Create_CrossbowWeapon( Vector(918.282, 1810.262, -2735.969), Angle(0, 136.136, 0) )
		Create_CrossbowAmmo( Vector(920.781, 1862.34, -2735.969), Angle(0, 173.756, 0) )
		Create_CrossbowAmmo( Vector(880.238, 1805.749, -2735.969), Angle(0, 110.22, 0) )
		Create_Armour100( Vector(526.898, 2583.84, -2735.969), Angle(0, 268.046, 0) )
		
		CreateSecretArea( Vector(353.514,3710.574,-1402.031), Vector(-61.611,-61.611,0), Vector(61.611,61.611,81.293) )
		CreateSecretArea( Vector(898.247,1832.945,-2735.969), Vector(-61.501,-61.501,0), Vector(61.501,61.501,52.598) )
		
	end
	
	local STOP_SCENESKIP = function()
		SetSceneSkipAvailable(false)
	end
	
	local ALLOW_SCENESKIP = function()
		SetSceneSkipAvailable(true)
	end
	
	CreateTrigger( Vector(-64.954,2727.996,-1279.969), Vector(-133.344,-133.344,0), Vector(133.344,133.344,108.958), ALLOW_SCENESKIP )
	CreateTrigger( Vector(113.298,2128.808,-2735.969), Vector(-97.228,-97.228,0), Vector(97.228,97.228,186.969), STOP_SCENESKIP )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)