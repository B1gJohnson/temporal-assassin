		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["11_out_combine_resort"] = {"12_uvalno_kd", Vector(-125.785,4177.655,0.031), Vector(-132.34,-132.34,0), Vector(132.34,132.34,136.618)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(984.473,-30.133,-86.334), Angle(0,110.056,0))
	
	Create_ShotgunAmmo( Vector(1294.462, 1.628, 23.443), Angle(0, 45.159, 0) )
	Create_ShotgunWeapon( Vector(1320.649, -40.515, 25.451), Angle(0, 31.871, 0) )
	Create_Beer( Vector(105.858, 751.025, 34.031), Angle(0, 278.263, 0) )
	Create_Bread( Vector(83.399, 749.962, 34.031), Angle(0, 258.551, 0) )
	Create_Bread( Vector(-238.189, 479.334, 44.031), Angle(0, 346.727, 0) )
	Create_Beer( Vector(-245.302, 455.41, 44.031), Angle(0, 337.927, 0) )
	Create_Beer( Vector(-250.899, 445.113, 44.108), Angle(0, 337.047, 0) )
	Create_Beer( Vector(-254.715, 434.054, 44.018), Angle(0, 336.871, 0) )
	Create_Beer( Vector(-261.408, 423.471, 44.177), Angle(0, 322.439, 0) )
	Create_Beer( Vector(-268.077, 410.536, 44.031), Angle(0, 311.351, 0) )
	Create_Beer( Vector(-282.063, 402.54, 44.071), Angle(0, 368.199, 0) )
	Create_Medkit25( Vector(-186.288, 960.948, 47.444), Angle(0, 290.134, 0) )
	Create_Medkit10( Vector(-162.459, 950.826, 49.446), Angle(-90, 152.424, 135) )
	Create_RevolverAmmo( Vector(-517.911, 507.723, 40.128), Angle(90, 130.6, 180) )
	Create_SMGWeapon( Vector(-520.131, 1051.558, 12.24), Angle(0, 2.288, 180) )
	Create_ShotgunWeapon( Vector(-134.785, 716.406, 194.306), Angle(0, 172.432, 45) )
	Create_ShotgunAmmo( Vector(-150.522, 697.314, 208.15), Angle(0, 154.246, 0) )
	Create_SMGAmmo( Vector(-168.659, 696.841, 208.217), Angle(0, 137.878, 0) )
	Create_RevolverWeapon( Vector(346.03, 597.517, 196.031), Angle(0, 176.598, 0) )
	Create_RifleWeapon( Vector(547.475, 803.991, 31.658), Angle(-9.999, 23.057, -0.098) )
	Create_EldritchArmour( Vector(-90.009, 1260.031, 211.609), Angle(0, 84.935, 0) )
	Create_Medkit25( Vector(-3.539, 1259.108, 192.174), Angle(0, 356.055, 0) )
	Create_Armour100( Vector(-119.675, 1260.031, 211.751), Angle(0, 49.559, 0) )
	Create_Backpack( Vector(-155.712, 1255.129, 192.031), Angle(0, 133.423, 0) )
	Create_RifleAmmo( Vector(554.41, 801.986, 35.668), Angle(0, 20.888, 180) )
	Create_M6GAmmo( Vector(-64.687, 1260.031, 212.022), Angle(0, 81.6, 0) )
	Create_PortableMedkit( Vector(-33.531, 1260.033, 211.554), Angle(0, 150.768, 0) )

	CreateSecretArea( Vector(-81.238,1313.446,192.031), Vector(-91.454,-91.454,0), Vector(91.454,91.454,127.938), "NOT SO SECRET STASH OF PROVISIONS HAS BEEN FOUND. (Password: The Bowl of Petunias)" )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)