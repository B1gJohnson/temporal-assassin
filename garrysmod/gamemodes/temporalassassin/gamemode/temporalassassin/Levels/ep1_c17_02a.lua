		
if CLIENT then return end

--[[ LEVEL_STRINGS["ep1_c17_02a"] = {"ep1_c17_05",Vector(-2635.0961914063,8057.2255859375,-2833.96875), Vector(-146.68493652344,-146.68493652344,0), Vector(146.68493652344,146.68493652344,137.61791992188)} ]]

function EXTRA_FAIL_STATE( npc, att, inf )

	if IsValid(npc) and ( npc:GetClass() == "npc_alyx" ) then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRA_FAIL_STATEHook",EXTRA_FAIL_STATE)

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("trigger_changelevel")
	
	local START = Vector(1736.3591308594,8455.1904296875,-2467.7087402344)
	local END = Vector(1676.7058105469,8457.8232421875,-2470.6110839844)
	local BRUSH = GetObjectFromTrace( START, END, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH) then BRUSH:Remove() end
	
	local START2 = Vector(1050.2398681641,8732.96875,-2469.6586914063)
	local END2 = Vector(1049.8861083984,8785.38671875,-2472.5161132813)
	local BRUSH2 = GetObjectFromTrace( START2, END2, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH2) then BRUSH2:Remove() end
	
	local START3 = Vector(1677.96875,8459.2646484375,-2502.96875)
	local END3 = Vector(1745.0759277344,8461.90234375,-2502.96875)
	local BRUSH3 = GetObjectFromTrace( START3, END3, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH3) then BRUSH3:Remove() end
	
	local START4 = Vector(1719.7420654297,8408.5947265625,-2502.96875)
	local END4 = Vector(1637.6186523438,8403.98046875,-2502.96875)
	local BRUSH4 = GetObjectFromTrace( START4, END4, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH4) then BRUSH4:Remove() end
	
	local START5 = Vector(1677.96875,8412.927734375,-2502.96875)
	local END5 = Vector(1754.1325683594,8412.9423828125,-2502.96875)
	local BRUSH5 = GetObjectFromTrace( START5, END5, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH5) then BRUSH5:Remove() end
	
	local PORTALS = ents.FindByClass("func_areaportal")
	
	PORTALS[4]:Input("Open")
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Armour100( Vector(4681.4721679688, 6942.55078125, -2547.96875), Angle(0, 351.55993652344, 0) )
		Create_Backpack( Vector(4708.7490234375, 6604.2368164063, -2547.96875), Angle(0, 89.459892272949, 0) )
		Create_Medkit25( Vector(4814.2978515625, 7256.7797851563, -2547.96875), Angle(0, 250.79983520508, 0) )
		Create_Medkit25( Vector(4728.4091796875, 7260.9116210938, -2547.96875), Angle(0, 291.05975341797, 0) )
		Create_PortableMedkit( Vector(4773.595703125, 7260.3071289063, -2547.96875), Angle(0, 267.9596862793, 0) )
		Create_PortableMedkit( Vector(4755.3759765625, 7260.0893554688, -2547.96875), Angle(0, 278.73974609375, 0) )
		Create_PortableMedkit( Vector(4790.6538085938, 7260.8125, -2547.96875), Angle(0, 258.05963134766, 0) )
		Create_Armour( Vector(4396.6430664063, 7095.8706054688, -2461.8647460938), Angle(0, 13.119995117188, 0) )
		Create_SMGAmmo( Vector(4634.841796875, 7260.3764648438, -2547.96875), Angle(0, 210.2827911377, 0) )
		Create_SMGAmmo( Vector(4638.5908203125, 7229.6298828125, -2547.96875), Angle(0, 194.88278198242, 0) )
		Create_RevolverAmmo( Vector(4156.3095703125, 7264.7924804688, -2547.96875), Angle(0, 346.46270751953, 0) )
		Create_RevolverAmmo( Vector(4184.8657226563, 7263.1684570313, -2547.96875), Angle(0, 343.60266113281, 0) )
		Create_PistolAmmo( Vector(4042.4438476563, 7299.9033203125, -2547.96875), Angle(0, 307.52249145508, 0) )
		Create_PistolAmmo( Vector(4081.7607421875, 7302.9155273438, -2547.96875), Angle(0, 285.74237060547, 0) )
		Create_PistolAmmo( Vector(4118.35546875, 7292.9965820313, -2547.96875), Angle(0, 260.66235351563, 0) )
		Create_PistolAmmo( Vector(4116.3022460938, 7255.3393554688, -2547.96875), Angle(0, 254.72232055664, 0) )
		Create_KingSlayerAmmo( Vector(3797.27734375, 7296.37890625, -2547.96875), Angle(0, 267.26208496094, 0) )
		Create_KingSlayerAmmo( Vector(3633.4174804688, 7299.4814453125, -2547.96875), Angle(0, 266.38198852539, 0) )
		Create_SpiritSphere( Vector(2869.0834960938, 6862.984375, -2547.96875), Angle(0, 179.84167480469, 0) )
		Create_ShotgunAmmo( Vector(2165.3229980469, 7284.8168945313, -2547.96875), Angle(0, 329.74127197266, 0) )
		Create_ShotgunAmmo( Vector(2194.1745605469, 7308.3422851563, -2547.96875), Angle(0, 305.98114013672, 0) )
		Create_KingSlayerWeapon( Vector(2236.5366210938, 8705.92578125, -2559.96875), Angle(0, 270.45050048828, 0) )
		Create_KingSlayerAmmo( Vector(2176.4301757813, 8695.275390625, -2559.96875), Angle(0, 304.99053955078, 0) )
		Create_KingSlayerAmmo( Vector(2309.4418945313, 8697.0634765625, -2559.96875), Angle(0, 230.85049438477, 0) )
		Create_KingSlayerAmmo( Vector(2214.5715332031, 8667.740234375, -2559.96875), Angle(0, 290.91046142578, 0) )
		Create_KingSlayerAmmo( Vector(2267.0646972656, 8669.0615234375, -2559.96875), Angle(0, 243.83041381836, 0) )
		Create_KingSlayerAmmo( Vector(2239.814453125, 8632.341796875, -2559.96875), Angle(0, 268.47039794922, 0) )
		Create_Beer( Vector(1073.6208496094, 8297.0009765625, -2552.6887207031), Angle(0, 96.426200866699, 0) )
		Create_Beer( Vector(1056.1730957031, 8298.560546875, -2552.6887207031), Angle(0, 79.266136169434, 0) )
		Create_Beer( Vector(1041.1352539063, 8302.4140625, -2505.6887207031), Angle(0, 89.165832519531, 0) )
		Create_Armour( Vector(1186.4235839844, 8964.54296875, -2559.96875), Angle(0, 226.90730285645, 0) )
		Create_Medkit25( Vector(1194.6680908203, 8912.517578125, -2559.96875), Angle(0, 197.42729187012, 0) )
		Create_Medkit25( Vector(1186.3282470703, 8846.3671875, -2559.96875), Angle(0, 150.12727355957, 0) )
		Create_Medkit25( Vector(850.78436279297, 8601.111328125, -2559.96875), Angle(0, 108.10701751709, 0) )
		Create_Medkit10( Vector(861.24957275391, 8748.3359375, -2559.96875), Angle(0, 230.20700073242, 0) )
		Create_Medkit10( Vector(864.70434570313, 8725.7255859375, -2559.96875), Angle(0, 211.50692749023, 0) )
		Create_Medkit10( Vector(864.76348876953, 8694.3408203125, -2559.96875), Angle(0, 175.64694213867, 0) )
		Create_Medkit25( Vector(690.80303955078, 8735.2177734375, -2559.96875), Angle(0, 353.48687744141, 0) )
		Create_SpiritSphere( Vector(774.22961425781, 8504.759765625, -2532.96875), Angle(0, 155.18687438965, 0) )
		Create_Armour( Vector(-364.63452148438, 8355.107421875, -2559.96875), Angle(0, 75.551399230957, 0) )
		Create_Armour( Vector(-359.64404296875, 8389.9580078125, -2559.96875), Angle(0, 71.591377258301, 0) )
		Create_PortableMedkit( Vector(-416.18881225586, 8455.5673828125, -2539.96875), Angle(0, 350.41131591797, 0) )
		Create_Medkit25( Vector(-398.64434814453, 8444.439453125, -2559.96875), Angle(0, 357.23135375977, 0) )
		Create_Medkit25( Vector(-374.89794921875, 8467.4609375, -2559.96875), Angle(0, 322.69116210938, 0) )
		Create_Medkit10( Vector(-509.40921020508, 7720.857421875, -2559.96875), Angle(0, 252.15054321289, 0) )
		Create_Medkit10( Vector(-515.13317871094, 7691.439453125, -2559.96875), Angle(0, 248.63052368164, 0) )
		Create_Medkit25( Vector(-591.45355224609, 7656.8061523438, -2559.96875), Angle(0, 336.30053710938, 0) )
		Create_Medkit25( Vector(-591.18218994141, 7599.4633789063, -2559.96875), Angle(0, 6.9906463623047, 0) )
		Create_Armour100( Vector(-539.37847900391, 7506.826171875, -2559.96875), Angle(0, 88.455963134766, 0) )
		Create_KingSlayerAmmo( Vector(-457.22189331055, 7458.09765625, -2559.96875), Angle(0, 97.475830078125, 0) )
		Create_KingSlayerAmmo( Vector(-478.19171142578, 7496.8540039063, -2559.96875), Angle(0, 81.415740966797, 0) )
		Create_PistolAmmo( Vector(-400.95547485352, 7444.2075195313, -2527.96875), Angle(0, 68.655647277832, 0) )
		Create_PistolAmmo( Vector(-379.56008911133, 7445.3950195313, -2527.96875), Angle(0, 89.995758056641, 0) )
		Create_PistolAmmo( Vector(-352.68453979492, 7443.7661132813, -2527.96875), Angle(0, 115.95584869385, 0) )
		Create_SMGWeapon( Vector(-433.21139526367, 7443.8896484375, -2527.96875), Angle(0, 78.775749206543, 0) )
		Create_Medkit10( Vector(-332.08700561523, 7459.080078125, -2559.96875), Angle(0, 94.975959777832, 0) )
		Create_Medkit10( Vector(-332.37030029297, 7484.103515625, -2559.96875), Angle(0, 97.61597442627, 0) )
		Create_SMGAmmo( Vector(-319.25354003906, 7508.7099609375, -2559.96875), Angle(0, 116.75597381592, 0) )
		Create_ShotgunAmmo( Vector(-268.09945678711, 7516.55859375, -2559.96875), Angle(0, 141.17596435547, 0) )
		Create_AnthrakiaWeapon( Vector(-698.32427978516, 7470.6865234375, -2559.96875), Angle(0, 172.19580078125, 0) )
		Create_ManaCrystal100( Vector(-713.60333251953, 7582.2724609375, -2559.96875), Angle(0, 238.85585021973, 0) )
		Create_ManaCrystal100( Vector(-796.48303222656, 7493.2294921875, -2559.96875), Angle(0, 187.81585693359, 0) )
		Create_PortableMedkit( Vector(-982.26336669922, 7449.1225585938, -2519.96875), Angle(0, 76.0556640625, 0) )
		Create_SuperShotgunWeapon( Vector(-944.71606445313, 7442.546875, -2519.96875), Angle(0, 111.91571807861, 0) )
		Create_SMGAmmo( Vector(-986.61480712891, 7459.689453125, -2559.96875), Angle(0, 67.695686340332, 0) )
		Create_SMGAmmo( Vector(-961.43212890625, 7459.3510742188, -2559.96875), Angle(0, 99.815734863281, 0) )
		Create_SMGAmmo( Vector(-933.08294677734, 7465.2353515625, -2559.96875), Angle(0, 133.69573974609, 0) )
		Create_RifleWeapon( Vector(-906.65802001953, 7441.6245117188, -2519.96875), Angle(0, 73.63557434082, 0) )
		Create_RifleAmmo( Vector(-894.72369384766, 7475.048828125, -2559.96875), Angle(0, 133.47583007813, 0) )
		Create_RifleAmmo( Vector(-916.35198974609, 7471.2275390625, -2559.96875), Angle(0, 114.99578857422, 0) )
		Create_Backpack( Vector(-518.01348876953, 9363.7734375, -2559.96875), Angle(0, 293.86419677734, 0) )
		Create_Backpack( Vector(-464.34866333008, 9365.4658203125, -2559.96875), Angle(0, 258.99411010742, 0) )
		Create_Medkit25( Vector(673.021484375, 8934.35546875, -2799.96875), Angle(0, 204.82177734375, 0) )
		Create_Medkit25( Vector(692.49108886719, 8898.353515625, -2799.96875), Angle(0, 177.76177978516, 0) )
		Create_Medkit25( Vector(662.24328613281, 8860.185546875, -2799.96875), Angle(0, 144.98175048828, 0) )
		Create_Armour( Vector(658.27740478516, 8899.814453125, -2799.96875), Angle(0, 177.98175048828, 0) )
		Create_Backpack( Vector(-615.56274414063, 9429.3564453125, -2695.96875), Angle(0, 141.32122802734, 0) )
		Create_KingSlayerWeapon( Vector(-689.45306396484, 9421.83203125, -2695.96875), Angle(0, 90.28116607666, 0) )
		Create_RifleWeapon( Vector(-690.39398193359, 9452.701171875, -2695.96875), Angle(0, 89.621162414551, 0) )
		Create_Armour100( Vector(-1093.0203857422, 9560.38671875, -2833.96875), Angle(0, 260.38497924805, 0) )
		Create_PistolAmmo( Vector(-1125.6240234375, 9517.7626953125, -2833.96875), Angle(0, 281.06506347656, 0) )
		Create_PistolAmmo( Vector(-1089.9571533203, 9517.439453125, -2833.96875), Angle(0, 253.78494262695, 0) )
		Create_PistolAmmo( Vector(-1045.6096191406, 9503.46484375, -2833.96875), Angle(0, 222.1049041748, 0) )
		Create_KingSlayerAmmo( Vector(-973.92205810547, 9512.4345703125, -2833.96875), Angle(0, 209.56491088867, 0) )
		Create_RifleAmmo( Vector(-984.0390625, 9458.4716796875, -2833.96875), Angle(0, 183.1649017334, 0) )
		Create_RifleAmmo( Vector(-978.69689941406, 9428.5048828125, -2833.96875), Angle(0, 165.56491088867, 0) )
		Create_Medkit25( Vector(-1161.4766845703, 9513.4599609375, -2833.96875), Angle(0, 273.50506591797, 0) )
		Create_Medkit25( Vector(-1158.6395263672, 9488.64453125, -2833.96875), Angle(0, 272.62506103516, 0) )
		Create_KingSlayerAmmo( Vector(-2803.3100585938, 8561.1455078125, -2833.96875), Angle(0, 18.772720336914, 0) )
		Create_Backpack( Vector(-2806.3552246094, 8606.8759765625, -2833.96875), Angle(0, 354.79260253906, 0) )
		Create_Armour( Vector(-2610.4230957031, 8562.2724609375, -2833.96875), Angle(0, 161.99261474609, 0) )
		Create_Medkit10( Vector(-2619.6281738281, 8630.32421875, -2793.96875), Angle(0, 179.81262207031, 0) )
		Create_Medkit10( Vector(-2618.1574707031, 8645.9755859375, -2793.96875), Angle(0, 193.01261901855, 0) )
		Create_Medkit10( Vector(-2618.3515625, 8664.2890625, -2793.96875), Angle(0, 206.87261962891, 0) )
		
		CreateSecretArea( Vector(2865.8193359375,6848.8291015625,-2547.96875), Vector(-40.68090057373,-40.68090057373,0), Vector(40.68090057373,40.68090057373,107.23583984375) )
		CreateSecretArea( Vector(759.712890625,8509.1220703125,-2559.96875), Vector(-52.722732543945,-52.722732543945,0), Vector(52.722732543945,52.722732543945,106.568359375) )
		
	else
	
		Create_MaskOfShadows( Vector(5672.325, 5957.104, -2547.969), Angle(0, 91.226, 0) )
		Create_GrenadeAmmo( Vector(5719.833, 5963.208, -2547.969), Angle(0, 103.348, 0) )
		Create_GrenadeAmmo( Vector(5672.663, 6010.13, -2547.969), Angle(0, 79.94, 0) )
		Create_GrenadeAmmo( Vector(5619.631, 5969.457, -2547.969), Angle(0, 71.998, 0) )
		Create_GrenadeAmmo( Vector(5672.177, 5925.121, -2547.969), Angle(0, 80.567, 0) )
		Create_KingSlayerWeapon( Vector(4742.509, 7223.942, -2547.969), Angle(0, 282.564, 0) )
		Create_KingSlayerAmmo( Vector(4698.779, 7233.734, -2547.969), Angle(0, 271.278, 0) )
		Create_RevolverWeapon( Vector(4548.374, 7097.874, -2547.969), Angle(0, 64.893, 0) )
		Create_RevolverAmmo( Vector(4571.339, 7117.005, -2547.969), Angle(0, 77.851, 0) )
		Create_RevolverAmmo( Vector(4541.042, 7129.09, -2547.969), Angle(0, 45.665, 0) )
		Create_SpiritSphere( Vector(2864.764, 6870.22, -2547.969), Angle(0, 164.9, 0) )
		Create_Armour100( Vector(4109.841, 7285.838, -2547.969), Angle(0, 187.576, 0) )
		Create_Armour5( Vector(3607.136, 7298.493, -2547.969), Angle(0, 349.862, 0) )
		Create_Armour5( Vector(3607.745, 7273.263, -2547.969), Angle(0, 10.553, 0) )
		Create_Armour5( Vector(3625.776, 7288.028, -2546.903), Angle(0, 357.804, 0) )
		Create_Beer( Vector(3605.627, 7127.518, -2547.969), Angle(0, 18.495, 0) )
		Create_Beer( Vector(3606.589, 7156.052, -2547.969), Angle(0, 0.103, 0) )
		Create_Beer( Vector(3628.476, 7141.972, -2547.969), Angle(0, 12.748, 0) )
		Create_Medkit25( Vector(3208.534, 7139.507, -2547.969), Angle(0, 57.788, 0) )
		Create_ShotgunWeapon( Vector(3547.822, 7277.058, -2547.969), Angle(0, 200.846, 0) )
		Create_SuperShotgunWeapon( Vector(3518.771, 7280.194, -2547.969), Angle(0, 216.73, 0) )
		Create_ShotgunAmmo( Vector(3541.562, 7248.216, -2547.969), Angle(0, 177.229, 0) )
		Create_CrossbowWeapon( Vector(2717.153, 7147.865, -2547.969), Angle(0, 14.944, 0) )
		Create_CrossbowAmmo( Vector(2767.937, 7132.783, -2547.969), Angle(0, 44.622, 0) )
		Create_CrossbowAmmo( Vector(2741.932, 7199.051, -2547.969), Angle(0, 2.09, 0) )
		Create_FrontlinerWeapon( Vector(2239.775, 7732.433, -2363.969), Angle(0, 269.195, 0) )
		Create_FrontlinerAmmo( Vector(2239.448, 7703.881, -2367.979), Angle(0, 268.986, 0) )
		Create_FrontlinerAmmo( Vector(2239.012, 7676.453, -2391.979), Angle(0, 268.777, 0) )
		Create_Medkit25( Vector(2696.298, 7759.678, -2559.969), Angle(0, 141.496, 0) )
		Create_Medkit25( Vector(2707.478, 7821.721, -2559.969), Angle(0, 169.711, 0) )
		Create_Medkit25( Vector(2620.077, 7751.936, -2559.969), Angle(0, 107.22, 0) )
		Create_Armour( Vector(2571.792, 8695.033, -2559.969), Angle(0, 239.099, 0) )
		Create_Armour( Vector(2703.519, 8551.121, -2559.969), Angle(0, 183.61, 0) )
		Create_Backpack( Vector(1774.488, 8696.167, -2559.969), Angle(0, 311.621, 0) )
		Create_SMGWeapon( Vector(1785.155, 8643.962, -2559.969), Angle(0, 340.255, 0) )
		Create_SMGAmmo( Vector(1814.685, 8606.547, -2559.969), Angle(0, 4.29, 0) )
		Create_SMGAmmo( Vector(1824.801, 8682.734, -2559.969), Angle(0, 306.606, 0) )
		Create_SMGAmmo( Vector(1829.076, 8641.821, -2559.969), Angle(0, 327.715, 0) )
		Create_SpiritEye2( Vector(1054.424, 8669.038, -2559.969), Angle(0, 269.885, 0) )
		Create_ManaCrystal( Vector(892, 8826.575, -2559.969), Angle(0, 0.591, 0) )
		Create_ManaCrystal( Vector(946.724, 8827.141, -2559.969), Angle(0, 0.591, 0) )
		Create_FoolsOathAmmo( Vector(789.582, 8505.535, -2532.969), Angle(0, 14.048, 0) )
		Create_FalanranWeapon( Vector(778.765, 8522.984, -2559.969), Angle(0, 165.285, 0) )
		Create_ManaCrystal100( Vector(31.096, 8906.838, -2559.969), Angle(0, 3.729, 0) )
		Create_Medkit25( Vector(-364.619, 8352.804, -2559.969), Angle(0, 86.074, 0) )
		Create_Medkit10( Vector(-344.352, 8370.223, -2559.969), Angle(0, 100.495, 0) )
		Create_CrossbowAmmo( Vector(363.052, 7590.089, -2559.969), Angle(0, 85.029, 0) )
		Create_CrossbowAmmo( Vector(349.596, 7611.424, -2559.969), Angle(0, 99.241, 0) )
		Create_EldritchArmour10( Vector(371.54, 7634.135, -2559.969), Angle(0, 100.286, 0) )
		Create_EldritchArmour10( Vector(343.419, 7633.693, -2559.969), Angle(0, 77.296, 0) )
		Create_ReikoriKey( Vector(-1135.473, 8185.034, -2559.969), Angle(0, 345.813, 0) )
		Create_Medkit25( Vector(-569.037, 9322.104, -2559.969), Angle(0, 84.462, 0) )
		Create_Medkit25( Vector(-528.418, 9349.518, -2559.969), Angle(0, 154.268, 0) )
		Create_Medkit25( Vector(-611.222, 9351.875, -2559.969), Angle(0, 18, 0) )
		Create_Armour100( Vector(-571.62, 9358.825, -2559.969), Angle(0, 267.648, 0) )
		Create_InfiniteGrenades( Vector(-1263.158, 9121.876, -2559.969), Angle(0, 288.757, 0) )
		Create_HarbingerAmmo( Vector(-499.026, 8786.086, -2539.719), Angle(0, 203.694, 0) )
		Create_AnthrakiaWeapon( Vector(-244.383, 8741.62, -2575.969), Angle(0, 178.405, 0) )
		Create_Medkit25( Vector(163.496, 8614.21, -2839.969), Angle(0, 197.302, 0) )
		Create_Medkit25( Vector(166.248, 8560.265, -2839.969), Angle(0, 182.463, 0) )
		Create_Medkit25( Vector(153.343, 8511.61, -2839.969), Angle(0, 152.263, 0) )
		Create_VolatileBattery( Vector(-46.871, 8527.896, -2839.969), Angle(0, 20.698, 0) )
		Create_EldritchArmour( Vector(-48.03, 8622.581, -2839.969), Angle(0, 325.312, 0) )
		Create_Beer( Vector(-668.726, 8709.763, -2711.969), Angle(0, 353.32, 0) )
		Create_Beer( Vector(-793.437, 8718.038, -2695.969), Angle(0, 357.082, 0) )
		Create_Beer( Vector(-862.99, 8858.931, -2695.969), Angle(0, 272.228, 0) )
		Create_Backpack( Vector(-599.514, 9696.335, -2695.969), Angle(0, 179.33, 0) )
		Create_Backpack( Vector(-599.847, 9667.865, -2695.969), Angle(0, 179.33, 0) )
		Create_Backpack( Vector(-598.935, 9634.085, -2695.969), Angle(0, 186.018, 0) )
		Create_MegaSphere( Vector(-667.505, 9678.342, -2695.969), Angle(0, 257.496, 0) )
		Create_Beer( Vector(-969.918, 9335.674, -2833.969), Angle(0, 352.483, 0) )
		Create_Armour5( Vector(-1026.142, 9418.345, -2833.969), Angle(0, 302.114, 0) )
		Create_Beer( Vector(-1123.828, 9443.616, -2833.969), Angle(0, 352.483, 0) )
		Create_Armour5( Vector(-1281.606, 9445.372, -2833.969), Angle(0, 358.544, 0) )
	
		CreateSecretArea( Vector(5669.37,5965.475,-2547.969), Vector(-80.546,-80.546,0), Vector(80.546,80.546,59.282) )
		CreateSecretArea( Vector(2864.738,6870.122,-2547.969), Vector(-31.091,-31.091,0), Vector(31.091,31.091,63.339) )
		CreateSecretArea( Vector(771.822,8520.271,-2555.437), Vector(-35.688,-35.688,0), Vector(35.688,35.688,64.056) )
		CreateSecretArea( Vector(-1133.899,8184.354,-2557.486), Vector(-45.735,-45.735,0), Vector(45.735,45.735,25.885) )
	
	end
	
	local GAME_END_TXT = {
	"But suddenly kit decided this was way too much of an escort quest",
	"and ollied outta there fast as heck to go do something a bit more fun.",
	"",
	"",
	"This clearly isn't because the scripting for the next map is broken",
	"it's perfectly fine i swear.",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"You've basically completed Episode 1, this map is broken as heck."}
	
	MakeGameEnd( GAME_END_TXT, 30, Vector(-2635.0961914063,8057.2255859375,-2833.96875), Vector(-146.68493652344,-146.68493652344,0), Vector(146.68493652344,146.68493652344,137.61791992188) )
	
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)