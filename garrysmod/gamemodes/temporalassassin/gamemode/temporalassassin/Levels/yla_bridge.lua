		
if CLIENT then return end

GLOB_NO_FENIC = false
GLOB_REPLACE_FENIC_GRISEUS = true
GLOB_ENDING_ACTIVE = false

function MAP_PreClean()
	GLOB_ENDING_ACTIVE = false
end
hook.Add("PreCleanupMap","MAP_PreCleanHook",MAP_PreClean)

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("env_fade")
	RemoveAllEnts("game_credits")
	RemoveAllEnts("game_text")
	
	GLOB_ENDING_ACTIVE = false

	CreatePlayerSpawn(Vector(2106.209,2241.985,233.531), Angle(0,179.384,0))

	Create_SMGWeapon( Vector(2156.453, 1875.638, 368.031), Angle(0, 269.825, 0) )
	Create_RevolverWeapon( Vector(1746.408, 2491.393, 272.491), Angle(0, 18.481, 0) )
	Create_PistolAmmo( Vector(2170.605, 1767.015, 401.657), Angle(0, 143.793, 0) )
	Create_PistolAmmo( Vector(2162.258, 1766.536, 401.34), Angle(0, 138.337, 0) )
	Create_SMGAmmo( Vector(2065.537, 1763.713, 401.119), Angle(0, 81.84, 0) )
	Create_SMGAmmo( Vector(2065.023, 1763.736, 403.803), Angle(0, 81.136, 0) )
	Create_ShotgunAmmo( Vector(2097.007, 1765.55, 400.68), Angle(0, 90.113, 0) )
	Create_ShotgunAmmo( Vector(2090.248, 1764.415, 403.745), Angle(28.872, 167.317, -1.707) )
	Create_Bread( Vector(2221.619, 1822.545, 398.551), Angle(0, 151.361, 0) )
	Create_Beer( Vector(2220.929, 1807.164, 401.255), Angle(0, 168.081, 0) )
	Create_Medkit25( Vector(2139.974, 1763.91, 399.422), Angle(0, 95.217, 0) )
	Create_Armour100( Vector(2149.677, 1760.135, 388.773), Angle(-90, -5.096, 0) )
	Create_ShotgunWeapon( Vector(-454.468, -1498.891, -48.28), Angle(-12.919, -156.325, 176.569) )
	Create_Medkit10( Vector(-432.407, -1559.707, -54.422), Angle(-72.171, 16.116, 98.127) )
	Create_Medkit10( Vector(-424.8, -1553.426, -56.488), Angle(-74.842, 5.685, 120.989) )
	Create_EldritchArmour( Vector(-1388.93, -1060.088, 202.39), Angle(0, 344.627, 0) )
	Create_Beer( Vector(-1214.34, 457.073, -63.969), Angle(0, 208.795, 0) )
	Create_Beer( Vector(-1205.093, -192.069, -61.355), Angle(90, -182.992, 0) )
	Create_Medkit25( Vector(-1206.899, -278.248, 83.571), Angle(0, 100.379, 0) )
	Create_SuperShotgunWeapon( Vector(-1219.697, 518.935, -51.678), Angle(1.585, 40.821, -45.217) )
	Create_Beer( Vector(-1839.99, 774.035, 334.002), Angle(0, 83.992, 0) )
	Create_Beer( Vector(-1839.595, 766.044, 334.031), Angle(0, 703.583, 0) )
	Create_Beer( Vector(-1839.612, 757.555, 334.02), Angle(0, 8.4, 0) )
	Create_Beer( Vector(-1839.604, 750.521, 334.058), Angle(0, 716.784, 0) )
	Create_Beer( Vector(-1838.378, 743.109, 334.036), Angle(0, 3.824, 0) )
	Create_Beer( Vector(-1847.907, 774.651, 334.075), Angle(0, 177.008, 0) )
	Create_Beer( Vector(-1849.468, 765.122, 334.046), Angle(0, 186.336, 0) )
	Create_Beer( Vector(-1849.692, 757.832, 334.008), Angle(0, 100.096, 0) )
	Create_Beer( Vector(-1851.261, 751.031, 334.018), Angle(0, 186.864, 0) )
	Create_Beer( Vector(-1849.145, 744.807, 334.024), Angle(0, 88.832, 0) )
	Create_Beer( Vector(-1858.309, 747.09, 334.273), Angle(0, 88.303, 0) )
	Create_Beer( Vector(-1858.11, 757.012, 334.031), Angle(0, 88.655, 0) )
	Create_Beer( Vector(-1858.034, 767.364, 334.07), Angle(0, 97.103, 0) )
	Create_Beer( Vector(-1856.919, 775.999, 334.036), Angle(0, 61.727, 0) )
	Create_Beer( Vector(-1841.604, 774.384, 370.154), Angle(0, 1.184, 0) )
	Create_Beer( Vector(-1842.614, 766.721, 370.04), Angle(0, 171.728, 0) )
	Create_Beer( Vector(-1843.392, 758.619, 370.04), Angle(0, 173.136, 0) )
	Create_Beer( Vector(-1847.238, 752.411, 370.317), Angle(0, 149.024, 0) )
	Create_Beer( Vector(-1849.297, 772.766, 370.054), Angle(0, 180.176, 0) )
	Create_Beer( Vector(-1852.03, 764.525, 370.163), Angle(0, 173.136, 0) )
	Create_Beer( Vector(-1853.539, 756.819, 370.202), Angle(0, 164.688, 0) )
	Create_Beer( Vector(-1841.082, 748.264, 372.813), Angle(45, 322.864, -90) )
	Create_PistolAmmo( Vector(-1487.488, 718.012, 370.009), Angle(0, 214.752, 0) )
	Create_PistolAmmo( Vector(-1489.118, 710.443, 370.023), Angle(0, 227.776, 0) )
	Create_SMGAmmo( Vector(-1486.149, 722.363, 334.264), Angle(0, 179.2, 0) )
	Create_SMGAmmo( Vector(-1485.486, 726.361, 334.876), Angle(-14.372, -94.269, 0.376) )
	Create_Beer( Vector(-1680.201, 851.978, 367.945), Angle(0, 535.152, 0) )
	Create_Beer( Vector(-1687.177, 851.969, 367.946), Angle(0, 345.872, 0) )
	Create_Beer( Vector(-1694.975, 851.969, 367.963), Angle(0, 132.888, 0) )
	Create_Beer( Vector(-1672.461, 851.684, 367.944), Angle(0, 93.761, 0) )
	Create_Beer( Vector(-1664.96, 851.871, 367.968), Angle(0, 142.512, 0) )
	Create_Beer( Vector(-1657.194, 851.674, 367.995), Angle(0, 114.704, 0) )
	Create_Beer( Vector(-1641.695, 851.969, 367.967), Angle(0, 377.568, 0) )
	Create_Beer( Vector(-1649.284, 851.969, 367.992), Angle(0, 77.68, 0) )
	Create_Beer( Vector(-1633.679, 851.969, 368.209), Angle(0, 354.072, 0) )
	Create_Beer( Vector(-1696.096, 851.969, 387.984), Angle(0, 1.584, 0) )
	Create_Beer( Vector(-1687.434, 851.969, 388.077), Angle(0, 8.448, 0) )
	Create_Beer( Vector(-1679.104, 851.987, 387.992), Angle(0, 308.76, 0) )
	Create_Beer( Vector(-1671.858, 851.969, 387.997), Angle(0, -23.88, 0) )
	Create_Beer( Vector(-1662.811, 844.28, 323.001), Angle(45, 2.112, 90) )
	Create_InfiniteGrenades( Vector(-1596.985, -683.002, 323.565), Angle(0, 71.404, 0) )
	Create_Armour200( Vector(-1707.548, 989.232, 584.45), Angle(0, 275.293, 0) )
	Create_Medkit25( Vector(-1662.112, 981.666, 616.51), Angle(0, 262.973, 0) )
	Create_Medkit25( Vector(-1662.227, 999.895, 616.83), Angle(0, 273.181, 0) )
	Create_ShotgunAmmo( Vector(-1330.153, 886.144, 582.036), Angle(0, 250.749, 0) )
	Create_ShotgunAmmo( Vector(-1339.34, 883.043, 582.02), Angle(0, 278.909, 0) )
	Create_ShotgunWeapon( Vector(-1324.631, 860.207, 594.937), Angle(1.193, -90.685, 28.899) )
	Create_RifleWeapon( Vector(60.73, 367.884, -114.251), Angle(10.1, -85.657, 84.887) )
	Create_GrenadeAmmo( Vector(2095.58, 2239.47, -39.859), Angle(0, 5, 0) )
	Create_RifleAmmo( Vector(25.791, 264.587, 376.051), Angle(0, -92.112, 0) )
	Create_RifleAmmo( Vector(22.384, 263.789, 377.032), Angle(21.844, -160.579, -2.459) )
	Create_RifleAmmo( Vector(11.48, 349.363, -140.986), Angle(55.449, 36.176, -3.185) )

	CreateProp(Vector(-424.375,-1569.781,-59.094), Angle(-12.7,147.568,7.471), "models/items/item_item_crate.mdl", true)

	local CORZOWEAPON = CreateProp(Vector(-1611.219,856.281,348.656), Angle(5.361,175.737,98.438), "models/temporalassassin/props/corzo_cintia1901.mdl", true)
	
	local GR_B1 = function()
		Create_CorzoHat( Vector(-1595.971, 841.095, 320.031), Angle(0, 89.716, 0) )
	end
	
	local GR_CINCODEMAYO = function()
		CreateProp(Vector(-1519.75,917.938,322.625), Angle(61.963,-96.855,94.087), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1570.125,820.156,322.531), Angle(23.027,-132.012,-90.088), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1600.969,791.656,322.625), Angle(-53.613,114.609,-93.076), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1599.406,848.719,322.656), Angle(61.172,61.787,-89.209), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1652.344,814.938,322.688), Angle(-53.877,-25.708,-93.647), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1682.844,807.313,322.594), Angle(65.61,46.406,-90.659), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1737.875,829.156,322.625), Angle(-50.625,-141.812,88.813), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1789.844,799.969,322.656), Angle(69.873,-102.832,93.384), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1841.687,802.75,322.531), Angle(-36.387,-2.637,-90.22), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1809.031,785.188,322.594), Angle(70.576,-27.51,93.032), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1736.625,753.156,322.625), Angle(-45.835,-88.374,-92.769), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1820.937,746.969,322.5), Angle(61.567,-163.345,-87.891), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1795.937,735,322.563), Angle(-77.651,165.059,88.11), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1801.687,668.281,322.563), Angle(-43.726,-176.089,-92.021), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1705.906,688.469,322.469), Angle(78.354,-16.04,86.924), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1788.625,644.375,322.531), Angle(23.95,-97.515,89.077), "models/temporalassassin/items/beer_5.mdl", true)
		CreateProp(Vector(-1850.687,607.844,322.5), Angle(-72.202,57.085,88.066), "models/temporalassassin/items/beer_5.mdl", true)
	end
	
	AddSecretButton(Vector(-1592.603,850.969,376.031), GR_B1, "SWEET HAT ABOVE, BIG GUNS BELOW" )
	
	CreateSecretArea( Vector(-1387.209,-1057.84,206.906), Vector(-22.787,-22.787,0), Vector(22.787,22.787,26.978), "SECRET ELDRITCH ARMOUR HAS BEEN LOCATED (Password: YLA Freebie)" )
	CreateTrigger( Vector(-1768.632,1020.188,192.031), Vector(-24.357,-24.357,0), Vector(24.357,24.357,109.19), GR_CINCODEMAYO)
	
	local END_SAFE = function(self,ply)
	
		GLOB_ENDING_ACTIVE = true
	
	end
	CreateTrigger( Vector(-989.82,12.257,344.031), Vector(-989.82,12.257,344.031), Vector(831.969,257.751,488.937), END_SAFE, false, Angle(0,0,0) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

local SAFE_END = CurTime() + 5

function BRIDGE_ENDING( ent )

	if ent and IsValid(ent) and ent.GetClass then
	
		local CLASS = ent:GetClass()

		if GLOB_ENDING_ACTIVE and CLASS == "npc_combinegunship" then

			local GAME_END_TXT = {
			"With the Gunship destroyed, Kit's phone rang up as she",
			"received a message from Griseus.",
			"",
			"'Beautiful work, Kit. Can it here and get some rest. Don't want to",
			"thin out Combine too much from this distaction, do we?'",
			"",
			"Shrugging to herself, Kit went back to her apartment to crack open",
			"a cold one and watch some Japanese game shows.",
			"",
			"",
			"Guess someone being Thai kicked never gets old to watch."}	
		
			MakeGameEnd( GAME_END_TXT, 33, Vector(-352.043,129.528,468.758), Vector(-1500,-1500,-1500), Vector(1500,1500,1500) )
		
		end
		
	end

end
hook.Add("EntityRemoved","BRIDGE_ENDING_HOOK",BRIDGE_ENDING)