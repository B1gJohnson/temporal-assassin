
if CLIENT then return end

GLOB_NO_FENIC = true
GLOB_REPLACE_FENIC_PHOTOOP = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	CreatePlayerSpawn(Vector(589.076,-12952.96,56.031),Angle(0,91.152,0) )
	CreateClothesLocker( Vector(-2064.555,-8530.419,160.031), Angle(0,179.64,0) )
	
	CreateProp(Vector(296.688,-9459.406,41.813), Angle(-10.898,79.453,35.596), "models/props_c17/streetsign003b.mdl", true)
	CreateProp(Vector(735.531,-10839.312,346.344), Angle(0,90,0), "models/props_c17/streetsign003b.mdl", true)
	CreateProp(Vector(-0.719,-7727.625,1419.5), Angle(81.387,-88.11,2.637), "models/temporalassassin/doll/unknown.mdl", true)
	CreateProp(Vector(18.656,-8358.125,169.594), Angle(-32.256,136.274,-0.22), "models/props_forest/whiteforestsignleft.mdl", true)
	CreateProp(Vector(1079.5,-8958.219,222.438), Angle(0.044,90.527,1.846), "models/props_forest/whiteforestsignright.mdl", true)
	CreateProp(Vector(-1889.875,-8128.219,299.5), Angle(-0.044,130.957,16.831), "models/props_c17/streetsign005d.mdl", true)
	CreateProp(Vector(-2297.562,-8529.437,168.531), Angle(0.22,-43.857,44.868), "models/props_c17/streetsign005d.mdl", true)
	
	Create_SMGWeapon( Vector(453.26, -11038.604, 288.031), Angle(0, 311.377, 0) )
	Create_SMGAmmo( Vector(475.897, -11052.788, 288.031), Angle(0, 260.395, 0) )
	Create_RifleWeapon( Vector(822.545, -11112.91, 329.607), Angle(-1.599, 87.874, -75.769) )
	Create_RifleAmmo( Vector(815.324, -11095.13, 288.031), Angle(0, 136.038, 0) )
	Create_Medkit25( Vector(819.997, -11213.348, 288.031), Angle(0, 184.632, 0) )
	Create_Armour100( Vector(821.656, -11172.872, 288.031), Angle(0, 180.166, 0) )
	Create_ShotgunWeapon( Vector(415.518, -11010.918, 288.031), Angle(0, 209.345, 0) )
	Create_ShotgunAmmo( Vector(429.426, -10967.791, 288.031), Angle(0, 96.859, 0) )
	Create_ShotgunAmmo( Vector(410.183, -10970.495, 288.031), Angle(0, 126.311, 0) )
	Create_RevolverAmmo( Vector(702.204, -11183.259, 288.031), Angle(0, 107.469, 0) )
	Create_RevolverWeapon( Vector(728.397, -11177.898, 288.031), Angle(0, 40.745, 0) )
	Create_PistolWeapon( Vector(747.291, -11178.538, 288.031), Angle(0, 76.743, 0) )
	Create_PistolAmmo( Vector(746.351, -11165.671, 288.031), Angle(0, 71.027, 0) )
	Create_Backpack( Vector(497.536, -10970.715, 288.031), Angle(0, 9.607, 0) )
	Create_CrossbowWeapon( Vector(-172.557, -11244.007, 56.495), Angle(0, 115.289, 0) )
	Create_CrossbowAmmo( Vector(-171.274, -11209.008, 32.031), Angle(0, 100.627, 0) )
	Create_Medkit10( Vector(-142.601, -10222.826, 76.031), Angle(0, 177.734, 0) )
	Create_Medkit10( Vector(-142.626, -10281.94, 76.031), Angle(0, 191.925, 0) )
	Create_Armour( Vector(-145.059, -10252.994, 76.031), Angle(0, 173.249, 0) )
	Create_KingSlayerWeapon( Vector(-99.046, -10013.495, 32.031), Angle(0, 222.065, 0) )
	Create_KingSlayerAmmo( Vector(-79.944, -10062.636, 32.031), Angle(0, 181.245, 0) )
	Create_SMGAmmo( Vector(-952.529, -10188.282, 324.617), Angle(0, 32.829, 0) )
	Create_SMGAmmo( Vector(-968.459, -10177.264, 324.617), Angle(0, 79.581, 0) )
	Create_GrenadeAmmo( Vector(-939.967, -10219.909, 288.031), Angle(0, 342.877, 0) )
	Create_GrenadeAmmo( Vector(-926.151, -10205.28, 288.031), Angle(0, 325.276, 0) )
	Create_Armour100( Vector(-2513.175, -10295.476, 423.312), Angle(0, 89.203, 0) )
	Create_Armour5( Vector(-2505.391, -10286.984, 470.312), Angle(0, 75.84, 0) )
	Create_Armour5( Vector(-2519.915, -10287.301, 470.312), Angle(0, 105.407, 0) )
	Create_Medkit10( Vector(-2535.344, -10284.395, 423.312), Angle(0, 81.342, 0) )
	Create_Medkit25( Vector(-2567.145, -10289.519, 416.031), Angle(0, 113.221, 0) )
	Create_SpiritSphere( Vector(-2571.148, -9824.882, 417.031), Angle(0, 200.043, 0) )
	Create_ManaCrystal( Vector(-1201.577, -9385.479, 288.031), Angle(0, 283.651, 0) )
	Create_ManaCrystal( Vector(-1372.959, -9660.154, 288.031), Angle(0, 62.043, 0) )
	Create_ManaCrystal100( Vector(-2654.637, -9817.158, 417.031), Angle(0, 266.854, 0) )
	Create_MaskOfShadows( Vector(-2637.464, -10214.826, 417.031), Angle(0, 10.508, 0) )
	Create_RifleAmmo( Vector(-2548.805, -10266.576, 416.031), Angle(0, 118.267, 0) )
	Create_RifleAmmo( Vector(-2530.516, -10264.334, 416.031), Angle(0, 89.707, 0) )
	Create_SMGAmmo( Vector(-2496.317, -10267.254, 416.031), Angle(0, 75.279, 0) )
	Create_SMGAmmo( Vector(-2512.696, -10263.601, 416.031), Angle(0, 94.848, 0) )
	Create_PistolAmmo( Vector(-2553.786, -9854.974, 416.031), Angle(0, 156.802, 0) )
	Create_PistolAmmo( Vector(-2565.817, -9857.528, 417.465), Angle(0, 201.574, 0) )
	Create_RevolverAmmo( Vector(-2584.865, -9864.463, 416.031), Angle(0, 101.684, 0) )
	Create_RevolverAmmo( Vector(-2599.201, -9856.882, 416.031), Angle(0, 132.231, 0) )
	Create_KingSlayerAmmo( Vector(-1811.965, -9372.481, 440.353), Angle(0, -180.048, 0) )
	Create_Medkit25( Vector(-1834.881, -9144.877, 417.594), Angle(0, -136.378, 0) )
	Create_Bread( Vector(-1897.471, -9130.168, 446.737), Angle(0, 314.394, 0) )
	Create_Beer( Vector(-1856.978, -9129.397, 446.737), Angle(0, 271.067, 0) )
	Create_Beer( Vector(-1842.529, -9128.357, 446.737), Angle(0, 272.108, 0) )
	Create_GrenadeAmmo( Vector(-1817.973, -9337.876, 416.031), Angle(0, 184.256, 0) )
	Create_GrenadeAmmo( Vector(-1829.549, -9356.102, 416.031), Angle(0, 167.682, 0) )
	Create_Medkit25( Vector(-2659.082, -8969.85, 416.031), Angle(0, 178.21, 0) )
	Create_Medkit25( Vector(-2660.577, -9034.23, 416.031), Angle(0, 184.366, 0) )
	Create_PistolAmmo( Vector(-2854.876, -9548.797, 196.617), Angle(0, 188.101, 0) )
	Create_PistolWeapon( Vector(-2849.318, -9529.408, 160.031), Angle(0, 115.623, 0) )
	Create_Medkit10( Vector(-2887.349, -9536.708, 160.031), Angle(0, 85.898, 0) )
	Create_Medkit10( Vector(-2917.196, -9521.749, 160.031), Angle(0, 40.49, 0) )
	Create_Armour( Vector(-2896.263, -9520.663, 160.031), Angle(0, 50.357, 0) )
	Create_SMGAmmo( Vector(-2584.538, -9093.468, 192.031), Angle(0, 255.709, 0) )
	Create_SMGAmmo( Vector(-2568.354, -9092.197, 192.031), Angle(0, 292.856, 0) )
	Create_ShotgunAmmo( Vector(-2588.884, -8929.92, 192.031), Angle(0, 231.982, 0) )
	Create_Medkit25( Vector(-2416.21, -8895.808, 160.031), Angle(0, 359.941, 0) )
	Create_Medkit25( Vector(-2313.912, -8895.896, 160.031), Angle(0, 179.997, 0) )
	Create_Armour( Vector(-2667.405, -8877.287, 192.031), Angle(0, 302.707, 0) )
	Create_Armour( Vector(-2578.544, -8784.306, 192.031), Angle(0, 336.302, 0) )
	Create_GrenadeAmmo( Vector(-2006.17, -8385.843, 184.353), Angle(0, 109.173, 0) )
	Create_GrenadeAmmo( Vector(-2006.504, -8396.174, 184.457), Angle(0, 183.371, 0) )
	Create_M6GWeapon( Vector(-2024.198, -8387.928, 184.353), Angle(0, 105.536, 0) )
	Create_M6GAmmo( Vector(-2039.797, -8388.692, 184.552), Angle(5.714, 178.889, 4.899) )
	Create_SuperShotgunWeapon( Vector(-1923.1, -8393.029, 174.633), Angle(-12.177, 101.077, -42.13) )
	Create_Armour200( Vector(-2340.46, -8218.832, 160.031), Angle(0, 271.805, 0) )
	Create_Bread( Vector(-1899.372, -8216.729, 160.005), Angle(0, 160.283, 0) )
	Create_Bread( Vector(-1917.596, -8215.477, 160.019), Angle(0, 187.562, 0) )
	Create_Beer( Vector(-1900.26, -8238.818, 160.031), Angle(0, -180, 0) )
	Create_Beer( Vector(-1900.502, -8246.834, 160.057), Angle(0, -180, 0) )
	Create_SMGAmmo( Vector(-1878.354, -8019.956, 176.23), Angle(0, 172.89, 0) )
	Create_SMGAmmo( Vector(-1879.127, -8035.944, 176.23), Angle(0, 179.759, 0) )
	Create_ShotgunAmmo( Vector(-1909.108, -8366.667, 160.031), Angle(0, 110.032, 0) )
	Create_ShotgunAmmo( Vector(-1926.268, -8377.676, 160.031), Angle(0, 142.143, 0) )
	Create_RifleAmmo( Vector(-1890.933, -8019.626, 160.031), Angle(0, 173.66, 0) )
	Create_RifleAmmo( Vector(-1893.645, -8038.871, 160.031), Angle(0, 190.186, 0) )
	Create_PistolAmmo( Vector(-1879.936, -8048.012, 176.238), Angle(0, 265.516, 0) )
	Create_PistolAmmo( Vector(-1880.837, -8057.198, 176.282), Angle(0, 268.009, 0) )
	Create_RevolverAmmo( Vector(-1879.439, -8069.75, 176.23), Angle(0, 197.98, 0) )
	Create_RevolverAmmo( Vector(-1891.84, -8078.144, 162.778), Angle(90, 81.667, -135) )
	Create_Medkit25( Vector(-414.447, -7651.862, 160.031), Angle(0, 91.087, 0) )
	Create_Medkit25( Vector(-376.251, -7661.723, 160.031), Angle(0, 33.414, 0) )
	Create_Armour100( Vector(-406.634, -8037.283, 160.031), Angle(0, 133.752, 0) )
	Create_MegaSphere( Vector(1190.142, -8727.738, 160.031), Angle(0, 168.413, 0) )
	Create_Medkit10( Vector(-367.594, -7520.783, 208.031), Angle(0, -90, 0) )
	Create_Medkit10( Vector(-333.711, -7518.975, 208.008), Angle(0, -45, 0) )
	Create_EldritchArmour( Vector(-278.368, -7443.981, 160.031), Angle(0, 259.789, 0) )
	Create_Beer( Vector(-1626.032, -9147.445, 320.011), Angle(0, 188.946, 0) )
	Create_Beer( Vector(-1631.727, -9168.42, 320.013), Angle(0, 166.992, 0) )
	Create_Bread( Vector(-1626.98, -9018.954, 320.031), Angle(0, 263.025, 0) )
	Create_Bread( Vector(-1627.337, -9038.648, 320.031), Angle(0, 279.561, 0) )
	Create_Armour200( Vector(-1743.564, -8984.75, 288.031), Angle(0, 320.679, 0) )
	Create_InfiniteGrenades( Vector(-414.46, -7832.031, 160.439), Angle(0, -90, 0) )
	Create_Armour( Vector(-397.283, -8929.926, 186.247), Angle(0, 89.989, 0) )
	Create_Armour( Vector(-439.59, -8929.416, 186.235), Angle(0, 91.227, 0) )
	Create_Armour( Vector(-448.048, -8036.245, 160.031), Angle(0, 126.123, 0) )
	Create_Armour( Vector(-407.392, -7999.676, 160.031), Angle(0, 158.011, 0) )
	Create_Backpack( Vector(-2144.113, -7765.417, 288.031), Angle(0, 88.863, 0) )
	Create_Backpack( Vector(-2194.614, -7764.416, 288.031), Angle(0, 88.863, 0) )
	Create_SpiritEye2( Vector(-2143.322, -7684.715, 289.031), Angle(0, 332.539, 0) )
	Create_Bread( Vector(-750.339, -9022.008, 160.031), Angle(0, 93.275, 0) )
	Create_Bread( Vector(-721.697, -9031.352, 160.031), Angle(0, 33.12, 0) )
	Create_Beer( Vector(-680.74, -9040.436, 160.031), Angle(0, 104.999, 0) )
	Create_Beer( Vector(-699.568, -9043.079, 160.031), Angle(0, 92.484, 0) )
	Create_ManaCrystal( Vector(643.233, -8157.355, 143.189), Angle(0, 229.079, 0) )
	Create_ManaCrystal( Vector(781.187, -7457.15, 159.23), Angle(0, 248.428, 0) )
	Create_ManaCrystal( Vector(200.27, -6971.467, 155.181), Angle(0, 325.698, 0) )
	Create_ManaCrystal100( Vector(-188.41, -7133.322, 131.799), Angle(0, 192.959, 0) )
	Create_ManaCrystal( Vector(-505.425, -7096.457, 137.713), Angle(0, 341.998, 0) )
	Create_ManaCrystal( Vector(-975.733, -7079.044, 156.749), Angle(0, 309.049, 0) )
	Create_ManaCrystal100( Vector(825.482, -6811.395, 131.86), Angle(0, 245.887, 0) )
	Create_Medkit25( Vector(-349.875, -7535.319, 160.031), Angle(0, 273.18, 0) )
	Create_FrontlinerWeapon( Vector(-577.79, -7451.896, 160.125), Angle(0, -179.53, 0) )
	Create_FrontlinerAmmo( Vector(-531.947, -7435.851, 160.193), Angle(0, -135, 0) )
	Create_FrontlinerAmmo( Vector(-532.202, -7474.413, 160.031), Angle(0, -90, 0) )
	
	local AL_B1 = function()
		Create_HarbingerWeapon( Vector(509.792, -10861.714, 288.031), Angle(0, 358.356, 0) )
		Create_WhisperWeapon( Vector(531.001, -10648.987, 288.031), Angle(0, 180.035, 0) )
		Create_AnthrakiaWeapon( Vector(536.23, -10752.18, 288.031), Angle(0, 1.899, 0) )
		Create_FalanranWeapon( Vector(463.553, -10752.05, 288.031), Angle(0, 358.825, 0) )
		Create_CorzoHat( Vector(535.996, -10650.273, 291.063), Angle(0, 2.688, 0) )
		Create_WhisperAmmo( Vector(571.4, -10668.104, 288.031), Angle(0, 352.658, 0) )
		Create_WhisperAmmo( Vector(571.908, -10636.082, 288.031), Angle(0, 18.323, 0) )
		Create_HarbingerAmmo( Vector(544.474, -10909.992, 288.031), Angle(0, 346.725, 0) )
		Create_HarbingerAmmo( Vector(547.369, -10854.029, 288.031), Angle(0, 10.492, 0) )
		Create_MegaSphere( Vector(403.836, -10860.336, 288.031), Angle(0, 358.989, 0) )
		Create_SpiritEye2( Vector(407.444, -10655.83, 288.031), Angle(0, 358.989, 0) )
		Create_VolatileBattery( Vector(415.543, -10751.972, 288.031), Angle(0, 180, 0) )
	end
	
	local AL_B2 = function()
		CreateTeleporter("AL_T1", Vector(852.45,-8793.736,160.031), "AL_T2", Vector(3558.682,-5396.118,320.403))
		CreateProp(Vector(1109.156,-8970.875,187.938), Angle(7.778,100.503,37.617), "models/props_junk/gnome.mdl", true)
		CreateProp(Vector(1863,-5749.625,455.844), Angle(-90,90,180), "models/props_phx/games/chess/board.mdl", true)
		CreateProp(Vector(2003.25,-5748.187,480.188), Angle(0,180,-0.044), "models/props_junk/gnome.mdl", true)
		CreateProp(Vector(1727.688,-5749.531,480.031), Angle(0.088,0.264,0.088), "models/temporalassassin/doll/unknown.mdl", true)
		CreateProp(Vector(1945,-5638.469,476.906), Angle(0,-174.155,0), "models/props_phx/games/chess/white_pawn.mdl", true)
		CreateProp(Vector(1947.125,-5665.969,476.906), Angle(0,163.389,0), "models/props_phx/games/chess/white_pawn.mdl", true)
		CreateProp(Vector(1945.594,-5701.406,476.906), Angle(0,176.704,0), "models/props_phx/games/chess/white_pawn.mdl", true)
		CreateProp(Vector(1946.625,-5735.281,476.906), Angle(0,173.979,0), "models/props_phx/games/chess/white_pawn.mdl", true)
		CreateProp(Vector(1949.438,-5767.062,476.906), Angle(0,-160.356,0), "models/props_phx/games/chess/white_pawn.mdl", true)
		CreateProp(Vector(1948.188,-5799.875,476.906), Angle(0,-179.429,0), "models/props_phx/games/chess/white_pawn.mdl", true)
		CreateProp(Vector(1947.656,-5831.75,476.813), Angle(-0.967,161.895,-0.264), "models/props_phx/games/chess/white_pawn.mdl", true)
		CreateProp(Vector(1947.469,-5863.281,476.906), Angle(-0.044,144.448,0), "models/props_phx/games/chess/white_pawn.mdl", true)
		CreateProp(Vector(1978.563,-5637.344,476.75), Angle(-0.044,-58.887,-0.044), "models/props_phx/games/chess/white_rook.mdl", true)
		CreateProp(Vector(1977.344,-5666.75,476.344), Angle(0,-179.604,0), "models/props_phx/games/chess/white_knight.mdl", true)
		CreateProp(Vector(1977.125,-5701.906,476.344), Angle(0,180,-0.044), "models/props_phx/games/chess/white_bishop.mdl", true)
		CreateProp(Vector(1977.25,-5734.094,476.25), Angle(0.483,-179.297,0.352), "models/props_phx/games/chess/white_king.mdl", true)
		CreateProp(Vector(1977.969,-5766.031,476.219), Angle(0.088,-173.101,0.044), "models/props_phx/games/chess/white_queen.mdl", true)
		CreateProp(Vector(1977,-5797.219,476.281), Angle(0,180,0), "models/props_phx/games/chess/white_bishop.mdl", true)
		CreateProp(Vector(1976.063,-5831.094,476.344), Angle(0,179.736,-0.044), "models/props_phx/games/chess/white_knight.mdl", true)
		CreateProp(Vector(1976.219,-5862.937,476.281), Angle(0.088,171.255,-0.264), "models/props_phx/games/chess/white_rook.mdl", true)
		CreateProp(Vector(1788.438,-5633.594,476.906), Angle(-1.187,6.372,-0.22), "models/props_phx/games/chess/black_pawn.mdl", true)
		CreateProp(Vector(1785.281,-5667.312,476.906), Angle(1.494,-3.252,0), "models/props_phx/games/chess/black_pawn.mdl", true)
		CreateProp(Vector(1784.688,-5700.156,476.844), Angle(0.088,30.059,0.264), "models/props_phx/games/chess/black_pawn.mdl", true)
		CreateProp(Vector(1788.375,-5731.625,476.719), Angle(0.088,24.697,-0.132), "models/props_phx/games/chess/black_pawn.mdl", true)
		CreateProp(Vector(1786,-5764.5,476.781), Angle(-0.088,13.359,-0.044), "models/props_phx/games/chess/black_pawn.mdl", true)
		CreateProp(Vector(1786.563,-5797.625,476.75), Angle(-0.132,3.208,0.176), "models/props_phx/games/chess/black_pawn.mdl", true)
		CreateProp(Vector(1787.781,-5829.344,476.75), Angle(-0.088,3.34,0.132), "models/props_phx/games/chess/black_pawn.mdl", true)
		CreateProp(Vector(1787.281,-5864.062,476.906), Angle(-0.308,2.505,0), "models/props_phx/games/chess/black_pawn.mdl", true)
		CreateProp(Vector(1756.125,-5635.531,476.781), Angle(0,28.652,-0.044), "models/props_phx/games/chess/black_rook.mdl", true) 
		CreateProp(Vector(1751.219,-5668.375,476.25), Angle(-0.264,1.143,0.264), "models/props_phx/games/chess/black_knight.mdl", true)
		CreateProp(Vector(1752.188,-5700.687,476.25), Angle(-0.088,-0.044,0.352), "models/props_phx/games/chess/black_bishop.mdl", true)
		CreateProp(Vector(1750.938,-5732.687,476.281), Angle(-0.088,1.055,-0.132), "models/props_phx/games/chess/black_king.mdl", true)
		CreateProp(Vector(1749.875,-5765.969,476.188), Angle(0,0,0), "models/props_phx/games/chess/black_queen.mdl", true)
		CreateProp(Vector(1751.688,-5798.781,476.344), Angle(0,0.308,-0.044), "models/props_phx/games/chess/black_bishop.mdl", true)
		CreateProp(Vector(1751,-5831,476.25), Angle(-0.308,-0.088,0.439), "models/props_phx/games/chess/black_knight.mdl", true) 
		CreateProp(Vector(1753.594,-5861.156,476.781), Angle(0.088,92.329,-0.132), "models/props_phx/games/chess/black_rook.mdl", true)
		Create_CorzoHat( Vector(1864.233, -5743.097, 478.906), Angle(0, 91.22, 0) )
	end
	
	local AL_B3 = function()
		Create_CorzoHat( Vector(-2107.465, -7933.569, 158.031), Angle(0, 271.692, 0) )
		Create_HarbingerAmmo( Vector(-2164.804, -8018.764, 160.031), Angle(0, 270.125, 0) )
		Create_HarbingerAmmo( Vector(-2124.304, -8018.685, 160.031), Angle(0, 270.125, 0) )
		Create_WhisperAmmo( Vector(-2170.881, -8031.807, 160.031), Angle(0, 250.688, 0) )
		Create_WhisperAmmo( Vector(-2119.798, -8033.183, 160.109), Angle(0, 280.718, 0) )
	end
	
	local FIX_FUNC = function()
    
		local LR = ents.FindByClass("logic_relay")
    
		for _,v in pairs (LR) do
			if v and IsValid(v) and v:GetName() == "logic_but" then
				v:Fire("Trigger")
			end
		end
		
	end  
	
	CreateTrigger( Vector(-1740.627,-7708.223,160.031), Vector(-109.16,-109.16,0), Vector(109.16,109.16,121.541), FIX_FUNC )
	
	CreateSecretArea( Vector(-279.665,-7451.182,164.563), Vector(-20.587,-20.587,0), Vector(20.587,20.587,57.492) )

	AddSecretButton(Vector(739.969,-10767.01,343.742), AL_B1)
	AddSecretButton(Vector(1022.671,-8959.969,215.771), AL_B2)
	AddSecretButton(Vector(-2269.959,-8539.41,210.658), AL_B3)
	
	local WARN_ELEC = function()
	
		net.Start("TwinsMessage")
		net.WriteString("Looks like the electricity is already active, no need to worry about it.")
		net.Broadcast()
	
	end
	
	CreateTrigger( Vector(-1756.784,-8401.591,304.031), Vector(-89.005,-89.005,0), Vector(89.005,89.005,133.898), WARN_ELEC )
	
	local GAME_END_TXT = {
    "Piloting the rocket into space was never the goal for Kit,",
    "As all she had been instructed to do was to attach a module inside the cockpit,",
    "Then let it fly into orbit to activate and connect with the satellites.",
    "",
    "Not that she cared for the reason that her contractor,",
    "an odd looking pair of glowy eyed siblings, had for using the rocket,",
    "Although she was told it was for some form of journalistic purpose.",
    "",
    "Her job done, Kit returned back to her home through her usual methods,",
	"Ready to catch some rest until her next job request came through,",
	"Whenever that would be.",
	"",
	"Until next time, Temporal Assassin..."}
	
	 MakeGameEnd( GAME_END_TXT, 45, Vector(15.029,-7809.134,892.031), Vector(-26.056,-26.056,0), Vector(26.056,26.056,119.728) )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)