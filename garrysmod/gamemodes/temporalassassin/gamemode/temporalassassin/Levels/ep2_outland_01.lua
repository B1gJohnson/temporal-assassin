		
if CLIENT then return end

LEVEL_STRINGS["ep2_outland_01"] = {"ep2_outland_01a",Vector(-5631.512,4543.418,-135.969), Vector(-142.674,-142.674,0), Vector(142.674,142.674,122.061)}

NPC_NO_DUPES = true

function EXTRA_FAIL_STATE( npc, att, inf )

	if IsValid(npc) and ( npc:GetClass() == "npc_alyx" or npc:GetClass() == "npc_vortigaunt" ) then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRA_FAIL_STATEHook",EXTRA_FAIL_STATE)

function MAP_LOADED_FUNC()
	
	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end
	
	local START = Vector(-3975.4389648438,1404.9887695313,201.07008361816)
	local END = Vector(-3975.6311035156,1464.3942871094,203.00926208496)
	local BRUSH = GetObjectFromTrace( START, END, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH) then BRUSH:Fire("Unlock") end
	
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("simple_physics_prop")
	
	CreateClothesLocker( Vector(684.40319824219,-1240.4049072266,-9.0136985778809), Angle(0,199.58848571777,0) )
	
	Create_PortableMedkit( Vector(-1383.7047119141, -707.12145996094, 368.03125), Angle(0, 270.37478637695, 0) )
	Create_PortableMedkit( Vector(-1356.0096435547, -713.24188232422, 368.03125), Angle(0, 249.69476318359, 0) )
	Create_PistolAmmo( Vector(-1470.9624023438, -720.95715332031, 368.03125), Angle(0, 323.17486572266, 0) )
	Create_PistolAmmo( Vector(-1438.3842773438, -702.88812255859, 368.03125), Angle(0, 303.37475585938, 0) )
	Create_PistolAmmo( Vector(-1411.5417480469, -720.53747558594, 368.03125), Angle(0, 277.63467407227, 0) )
	Create_RevolverWeapon( Vector(-2328.9865722656, -667.93084716797, 240.03125), Angle(0, 172.61471557617, 0) )
	Create_RevolverAmmo( Vector(-2345.9201660156, -659.94268798828, 240.03125), Angle(0, 179.43472290039, 0) )
	Create_RevolverAmmo( Vector(-3306.1965332031, -510.09075927734, 144.03125), Angle(0, 153.91450500488, 0) )
	Create_Medkit25( Vector(-3360.9721679688, -425.17358398438, 144.03125), Angle(0, 212.87452697754, 0) )
	Create_Medkit25( Vector(-3358.0222167969, -468.16564941406, 144.03125), Angle(0, 170.96453857422, 0) )
	Create_SuperShotgunWeapon( Vector(-3449.7834472656, -108.33055877686, -31.96875), Angle(0, 180.53422546387, 0) )
	Create_ShotgunAmmo( Vector(-3473.2900390625, -126.21398925781, -31.96875), Angle(0, 164.47422790527, 0) )
	Create_Armour( Vector(-3835.7368164063, 1601.7763671875, 172.03125), Angle(0, 86.82209777832, 0) )
	Create_Medkit10( Vector(-3789.6108398438, 1602.7841796875, 172.03125), Angle(0, 128.18217468262, 0) )
	Create_Medkit10( Vector(-3811.0222167969, 1604.8968505859, 172.03125), Angle(0, 112.34212493896, 0) )
	
	CreateSecretArea( Vector(-2335.830078125,-665.45111083984,244.5625), Vector(-21.550308227539,-21.550308227539,0), Vector(21.550308227539,21.550308227539,35.351470947266) )
	CreateSecretArea( Vector(-3461.3491210938,-112.83192443848,-27.4375), Vector(-33.696071624756,-33.696071624756,0), Vector(33.696071624756,33.696071624756,6.0214385986328) )
	
	CreateProp(Vector(-4027.281,1519.594,177.281), Angle(-0.176,-78.223,-0.571), "models/props_junk/shoe001a.mdl", true)
	CreateProp(Vector(-4015.969,1501.219,158.5), Angle(0,-15.249,-90), "models/props_trainstation/tracksign02.mdl", true)
	
	local NAG_KIT_KICK = function()
		
		timer.Create("KICK_THAT_SHIT_PLEASE", 15, 1, function()
			net.Start("DollMessage")
			net.WriteString("Just kick the damn door down kiddo")
			net.Broadcast()
		end)
		
	end
	
	local NAG_KIT_CANCEL = function()
		timer.Remove("KICK_THAT_SHIT_PLEASE")
	end
	
	CreateTrigger( Vector(-3934.919,1505.891,144.908), Vector(-81.692,-81.692,0), Vector(81.692,81.692,116.094), NAG_KIT_KICK )
	CreateTrigger( Vector(-4362.112,1256.142,133.083), Vector(-161.434,-161.434,0), Vector(161.434,161.434,280.271), NAG_KIT_CANCEL )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

timer.Create("REMOVE_TRASH_LAG_MOVE", 5, 0, function()
	for _,v in pairs (player.GetAll()) do
	
		if v and IsValid(v) and v:Alive() and v:GetLaggedMovementValue() != 1 then
			v:SetLaggedMovementValue(1)
		end
	
	end
end)