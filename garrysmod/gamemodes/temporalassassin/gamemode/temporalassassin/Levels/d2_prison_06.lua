
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	CreatePlayerSpawn( Vector(1952.09,1549.182,-439.969), Angle(0,-90,0) )

	local START = Vector(372.88693237305,104.65140533447,112.00508117676)
	local END = Vector(423.79949951172,98.449073791504,100.79364013672)
	local BRUSH = GetObjectFromTrace( START, END, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH) then BRUSH:Remove() end

	local START2 = Vector(316.63681030273,-85.699546813965,77.216232299805)
	local END2 = Vector(316.80029296875,-167.72138977051,68.348419189453)
	local BRUSH2 = GetObjectFromTrace( START2, END2, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH2) then
	
		timer.Create("Door_ForceOpen_Prison06", 0.5, 0, function()
			if BRUSH2 and IsValid(BRUSH2) then
				BRUSH2:Fire("Unlock")
				BRUSH2:Fire("Open")
			end
		end)
		
	end

	local START3 = Vector(443.41610717773,-891.81732177734,57.03125)
	local END3 = Vector(510.78454589844,-894.97589111328,59.815242767334)
	local BRUSH3 = GetObjectFromTrace( START3, END3, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH3) then BRUSH3:Remove() end
	
	local MTH_NAG1 = function(self,ply)
		net.Start("DollMessage")
		net.WriteString("Just keep going kiddo")
		net.Broadcast()
	end
	
	CreateTrigger( Vector(391.247,95.814,0.031), Vector(-45.014,-45.014,0), Vector(45.014,45.014,106.356), MTH_NAG1 )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_VolatileBattery( Vector(2226.2280273438, 141.86196899414, -703.96875), Angle(0, 191.39999389648, 0) )
		Create_PortableMedkit( Vector(2227.6381835938, 68.372093200684, -703.96875), Angle(0, 159.94000244141, 0) )
		Create_Medkit25( Vector(988.80511474609, 270.42984008789, -703.96875), Angle(0, 6.8198699951172, 0) )
		Create_Beer( Vector(1231.9793701172, 335.3464050293, -703.96875), Angle(0, 167.71981811523, 0) )
		Create_Beer( Vector(1232.6276855469, 353.38159179688, -703.96875), Angle(0, 185.31982421875, 0) )
		Create_Beer( Vector(1228.482421875, 374.69729614258, -703.96875), Angle(0, 206.21981811523, 0) )
		Create_MegaSphere( Vector(2047.6511230469, -936.60266113281, -31.96875), Angle(0, 89.760200500488, 0) )
		Create_Backpack( Vector(1980.3563232422, -953.45263671875, -31.96875), Angle(0, 70.400100708008, 0) )
		Create_Backpack( Vector(2013.0877685547, -918.01159667969, -31.968746185303), Angle(0, 77.220138549805, 0) )
		Create_Backpack( Vector(2048.3288574219, -911.06335449219, -31.96875), Angle(0, 89.980209350586, 0) )
		Create_Backpack( Vector(2085.015625, -933.94689941406, -31.96875), Angle(0, 102.52026367188, 0) )
		Create_Backpack( Vector(2114.2478027344, -980.75549316406, -31.96875), Angle(0, 107.80029296875, 0) )
		Create_SuperShotgunWeapon( Vector(995.30242919922, 851.78869628906, -533.40808105469), Angle(0, 270.60067749023, 0) )
		Create_ShotgunAmmo( Vector(942.17218017578, 850.16900634766, -532.08111572266), Angle(0, 179.16049194336, 0) )
		Create_ShotgunAmmo( Vector(916.17761230469, 851.71026611328, -531.78216552734), Angle(0, 180.70048522949, 0) )
		Create_ShotgunAmmo( Vector(887.7958984375, 851.37707519531, -531.36651611328), Angle(0, 180.70048522949, 0) )
		Create_ShotgunAmmo( Vector(858.95153808594, 851.0244140625, -530.94342041016), Angle(0, 180.70048522949, 0) )
		Create_Medkit10( Vector(1236.9177246094, 844.06890869141, -703.96875), Angle(0, 320.84039306641, 0) )
		Create_Medkit10( Vector(1430.7697753906, 844.60186767578, -703.96875), Angle(0, 195.44041442871, 0) )
		Create_Armour( Vector(1051.8332519531, 953.05871582031, -191.96875), Angle(0, 245.60000610352, 0) )
		Create_Beer( Vector(993.71105957031, 963.70440673828, -191.96875), Angle(0, 294.88006591797, 0) )
		Create_Beer( Vector(993.14599609375, 940.59167480469, -191.96875), Angle(0, 305.44012451172, 0) )
		Create_Beer( Vector(1016.5383911133, 965.50592041016, -191.96875), Angle(0, 277.2799987793, 0) )
		Create_Beer( Vector(1017.905090332, 944.25415039063, -191.96875), Angle(0, 278.82000732422, 0) )
		Create_SpiritSphere( Vector(1363.5989990234, 1196.3035888672, -191.96875), Angle(0, 235.47979736328, 0) )
		Create_SMGAmmo( Vector(21.16934967041, 376.83453369141, 0.03125), Angle(0, 355.15875244141, 0) )
		Create_SMGAmmo( Vector(24.156112670898, 349.22274780273, 0.03125), Angle(0, 14.40885925293, 0) )
		Create_SMGAmmo( Vector(28.40202331543, 313.18884277344, 0.03125), Angle(0, 36.738983154297, 0) )
		Create_PistolAmmo( Vector(60.394245147705, 335.19577026367, 0.031246185302734), Angle(0, 38.278991699219, 0) )
		Create_PistolAmmo( Vector(52.465938568115, 360.96789550781, 0.031253814697266), Angle(0, 9.6788940429688, 0) )
		Create_SMGWeapon( Vector(66.325485229492, 405.84857177734, 0.03125), Angle(0, 322.3786315918, 0) )
		Create_Medkit25( Vector(503.70724487305, -699.12109375, -63.96875), Angle(0, 90.058753967285, 0) )
		Create_Medkit25( Vector(539.68780517578, -670.58959960938, -63.96875), Angle(0, 141.53880310059, 0) )
		Create_RevolverAmmo( Vector(516.01733398438, -651.18298339844, -63.96875), Angle(0, 136.69879150391, 0) )
		Create_RevolverAmmo( Vector(495.46789550781, -669.8935546875, -63.968746185303), Angle(0, 117.11877441406, 0) )
		Create_RevolverAmmo( Vector(491.82662963867, -643.30334472656, -63.96875), Angle(0, 125.91879272461, 0) )
		Create_SMGAmmo( Vector(635.03277587891, -1210.5476074219, 0.031246185302734), Angle(0, 3.8052520751953, 0) )
		Create_SMGAmmo( Vector(871.88665771484, -1262.0601806641, 0.03125), Angle(0, 132.50527954102, 0) )
		Create_SMGAmmo( Vector(834.08489990234, -1271.6331787109, 0.03125), Angle(0, 100.60517120361, 0) )
		Create_Medkit25( Vector(1178.7778320313, -1347.0811767578, -135.96875), Angle(0, 90.705154418945, 0) )
		Create_Medkit10( Vector(1527.1619873047, -2180.9455566406, -239.96875), Angle(0, 174.02491760254, 0) )
		Create_Medkit10( Vector(1531.9810791016, -2140.8330078125, -239.96875), Angle(0, 196.02490234375, 0) )
		Create_Armour( Vector(871.61633300781, -2185.5883789063, -239.96875), Angle(0, 41.505096435547, 0) )
		Create_PortableMedkit( Vector(863.24371337891, -1872.8735351563, -239.96875), Angle(0, 290.54498291016, 0) )
		Create_SpiritEye1( Vector(2112.1508789063, -2734.3566894531, -239.96875), Angle(0, 84.625007629395, 0) )
		Create_Medkit10( Vector(1974.2846679688, -1872.4719238281, -239.96875), Angle(0, 280.14434814453, 0) )
		Create_Medkit10( Vector(2008.75390625, -1874.7813720703, -239.96875), Angle(0, 254.18423461914, 0) )
		Create_Beer( Vector(1998.7095947266, -2107.357421875, -239.96875), Angle(0, 47.164215087891, 0) )
		Create_SMGAmmo( Vector(1236.9373779297, -2760.4301757813, -239.96875), Angle(0, 230.72413635254, 0) )
		Create_KingSlayerAmmo( Vector(1158.7651367188, -3291.4519042969, -239.96875), Angle(0, 90.479507446289, 0) )
		Create_KingSlayerAmmo( Vector(1207.1516113281, -3292.6667480469, -239.96875), Angle(0, 91.359466552734, 0) )
		Create_KingSlayerAmmo( Vector(1246.7325439453, -3211.4655761719, -239.96875), Angle(0, 178.91946411133, 0) )
		Create_KingSlayerAmmo( Vector(1248.6207275391, -3162.6889648438, -239.96875), Angle(0, 181.55944824219, 0) )
		Create_HarbingerAmmo( Vector(-905.78454589844, 698.90692138672, 384.03125), Angle(0, 356.919921875, 0) )
		
		CreateSecretArea( Vector(2045.8631591797,-951.0146484375,-31.96875), Vector(-128.36933898926,-128.36933898926,0), Vector(128.36933898926,128.36933898926,83.420967102051) )
		
	else
	
		local P1 = CreateProp(Vector(1347.094,516.938,-593.875), Angle(-0.044,-151.743,180), "models/props_junk/plasticcrate01a.mdl", true)
		local P2 = CreateProp(Vector(984.406,965.531,-162.156), Angle(-18.589,-90.044,-0.044), "models/props_lab/lockerdoorleft.mdl", true)
		local P3 = CreateProp(Vector(1064.531,964.469,-162.5), Angle(-20.698,-90.044,-0.044), "models/props_lab/lockerdoorleft.mdl", true)
		
		local P4 = CreateProp(Vector(403.281,97.188,0.5), Angle(0.264,179.692,0), "models/props_lab/blastdoor001a.mdl", true)
		P4:SetMaterial("models/props_combine/com_shield001a")
		
		CreateProp(Vector(486.563,76.688,0.406), Angle(0.044,154.775,0), "models/temporalassassin/doll/unknown.mdl", true)
		CreateProp(Vector(463.094,-902.375,0.406), Angle(0.308,179.648,0), "models/props_lab/blastdoor001a.mdl", true)
	
		Create_VolatileBattery( Vector(1833.328, 1561.772, -311.969), Angle(0, 286.9, 0) )
		Create_InfiniteGrenades( Vector(1888.615, 1563.191, -311.969), Angle(0, 244.474, 0) )
		Create_Backpack( Vector(1825.725, 1301.77, -311.969), Angle(0, 45.923, 0) )
		Create_FalanranWeapon( Vector(1824.996, 1427.672, -311.969), Angle(0, 1.616, 0) )
		Create_SpiritSphere( Vector(1857.05, 1541.144, -311.969), Angle(0, 290.24, 0) )
		Create_Medkit25( Vector(1388.765, 366.912, -623.969), Angle(0, 208.933, 0) )
		Create_Medkit25( Vector(1387.644, 335.407, -623.969), Angle(0, 155.638, 0) )
		Create_Bread( Vector(980.048, 266.265, -703.969), Angle(0, 11.637, 0) )
		Create_Beer( Vector(982.23, 240.743, -703.969), Angle(0, 28.357, 0) )
		Create_Beer( Vector(979.11, 297.763, -703.969), Angle(0, 349.483, 0) )
		Create_EldritchArmour( Vector(1344.838, 522.195, -586.404), Angle(0, 295.874, 0) )
		Create_PortableMedkit( Vector(1345.98, 504.86, -586.4), Angle(0, 309.25, 0) )
		Create_PortableMedkit( Vector(1359.906, 510.726, -586.402), Angle(0, 278.109, 0) )
		Create_Armour5( Vector(1427.696, 756.331, -703.969), Angle(0, 174.757, 0) )
		Create_Armour5( Vector(1413.721, 778.286, -703.969), Angle(0, 191.059, 0) )
		Create_Armour5( Vector(1403.645, 757.397, -703.969), Angle(0, 173.712, 0) )
		Create_GrenadeAmmo( Vector(988.427, 814.953, -191.969), Angle(0, 75.793, 0) )
		Create_RifleWeapon( Vector(1029.483, 855.497, -191.969), Angle(0, 87.079, 0) )
		Create_RifleAmmo( Vector(1012.342, 826.405, -191.969), Angle(0, 76.002, 0) )
		Create_Medkit25( Vector(1124.2, 1211.259, -191.969), Angle(0, 293.989, 0) )
		Create_Beer( Vector(1380.632, 1063.395, -191.969), Angle(0, 139.747, 0) )
		Create_Beer( Vector(1383.494, 1078.914, -191.969), Angle(0, 164.513, 0) )
		Create_Beer( Vector(1360.914, 1058.719, -191.969), Angle(0, 107.143, 0) )
		Create_Armour( Vector(229.017, 840.009, -191.969), Angle(0, 11.839, 0) )
		Create_CrossbowAmmo( Vector(684.379, 839.929, -191.969), Angle(0, 260.34, 0) )
		Create_CrossbowAmmo( Vector(680.506, 815.235, -191.969), Angle(0, 259.922, 0) )
		Create_KingSlayerAmmo( Vector(643.502, 835.066, -191.969), Angle(0, 294.616, 0) )
		Create_ShotgunAmmo( Vector(183.2, 733.327, -191.969), Angle(0, 229.93, 0) )
		Create_Medkit25( Vector(29.283, 332.589, 0.031), Angle(0, 17.583, 0) )
		Create_M6GAmmo( Vector(24.259, 392.096, 0.031), Angle(0, 341.217, 0) )
		Create_RevolverWeapon( Vector(52.522, 367.731, 0.031), Angle(0, 359.191, 0) )
		Create_Armour200( Vector(404.386, -721.035, -63.969), Angle(0, 3.682, 0) )
		Create_SMGWeapon( Vector(813.016, -1251.481, 0.031), Angle(0, 68.472, 0) )
		Create_SMGAmmo( Vector(851.433, -1259.465, 0.031), Angle(0, 102.957, 0) )
		Create_ShotgunWeapon( Vector(866.699, -1214.967, 0.031), Angle(0, 94.806, 0) )
		Create_ShotgunAmmo( Vector(807.268, -1211.53, 0.031), Angle(0, 42.556, 0) )
		Create_GrenadeAmmo( Vector(1534.523, -2052.499, -239.969), Angle(0, 149.14, 0) )
		Create_ManaCrystal( Vector(1513.565, -2063.91, -239.969), Angle(0, 126.986, 0) )
		Create_PistolWeapon( Vector(877.586, -2177.547, -239.969), Angle(0, 69.195, 0) )
		Create_PistolAmmo( Vector(903.455, -2158.242, -239.969), Angle(0, 102.217, 0) )
		Create_PistolAmmo( Vector(872.211, -2151.115, -239.969), Angle(0, 44.324, 0) )
		Create_SpiritEye1( Vector(2024.559, -2038.32, -239.969), Angle(0, 87.587, 0) )
		Create_RevolverAmmo( Vector(1975.293, -2025.109, -239.969), Angle(0, 42.024, 0) )
		Create_Armour( Vector(2107.335, -2001.327, -239.969), Angle(0, 85.915, 0) )
		Create_Medkit25( Vector(2104.364, -1890.005, -239.969), Angle(0, 188.742, 0) )
		Create_Medkit25( Vector(2102.137, -1937.549, -239.969), Angle(0, 152.167, 0) )
		Create_KingSlayerWeapon( Vector(2131.022, -2761.696, -239.969), Angle(0, 148.196, 0) )
		Create_KingSlayerAmmo( Vector(2127.096, -2711.509, -239.969), Angle(0, 192.504, 0) )
		Create_EldritchArmour10( Vector(1732.777, -2148.113, -113.999), Angle(0, 5.241, 0) )
		Create_EldritchArmour10( Vector(1735.36, -2168.477, -112.621), Angle(0, -5.004, 0) )
		Create_EldritchArmour10( Vector(1728.682, -2186.152, -113.999), Angle(0, 9.211, 0) )
		Create_InfiniteGrenades( Vector(1078.035, -3035.999, -239.969), Angle(0, 11.933, 0) )
		Create_Medkit25( Vector(1031.392, -2983.126, -201.032), Angle(0, 179.862, 0) )
		Create_FoolsOathAmmo( Vector(1030.176, -2918.113, -201.032), Angle(0, 90.489, 0) )
		Create_ManaCrystal100( Vector(836.523, -2404.305, -239.969), Angle(0, 185.194, 0) )
		
		local S_BUTTON1 = function()
			Create_WhisperWeapon( Vector(1188.895, 1069.167, -191.969), Angle(0, 271.621, 0) )
			Create_WhisperAmmo( Vector(1129.605, 1038.215, -191.969), Angle(0, 283.325, 0) )
			Create_WhisperAmmo( Vector(1256.82, 1032.192, -191.969), Angle(0, 251.87, 0) )
		end
		
		local S_BUTTON2 = function()
			Create_ReikoriKey( Vector(650.816, -2283.97, -239.969), Angle(0, 89.054, 0) )
			Create_FalanrathThorn( Vector(651.369, -2127.307, -239.969), Angle(0, 89.472, 0) )
		end
		
		AddSecretButton( Vector(1022.963,975.969,-137.028), S_BUTTON1 )
		AddSecretButton( Vector(657.29,-1856.031,-185.298), S_BUTTON2 )
	
		CreateSecretArea( Vector(1835.063,1427.311,-307.437), Vector(-148.829,-148.829,0), Vector(148.829,148.829,52.604) )
		CreateSecretArea( Vector(1369.3,506.837,-581.844), Vector(-28.587,-28.587,0), Vector(28.587,28.587,37.678) )
		CreateSecretArea( Vector(408.328,-721.231,-59.437), Vector(-21.935,-21.935,0), Vector(21.935,21.935,63.265) )
		CreateSecretArea( Vector(1734.405,-2166.993,-108.062), Vector(-22.39,-22.39,0), Vector(22.39,22.39,73.668) )
		
		local H_P1 = CreateProp(Vector(692.438,-1871.281,-220.281), Angle(-0.044,-124.365,0.132), "models/props_interiors/furniture_chair03a.mdl", true)
		local H_P2 = CreateProp(Vector(689.219,-1871.594,-218.031), Angle(-0.088,46.406,179.561), "models/props_c17/tools_wrench01a.mdl", true)
		local H_P3 = CreateProp(Vector(701.906,-1876.656,-222.656), Angle(-13.359,7.471,-14.81), "models/props_canal/mattpipe.mdl", true)
		H_P1:SetPhysicsWeight(5000)
		H_P2:SetPhysicsWeight(5000)
		H_P3:SetPhysicsWeight(5000)
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

local DONT_DIE_LMAO = {
npc_eli = true,
npc_kleiner = true,
npc_kliener = true,
npc_magnusson = true,
npc_mossman = true}

function STOP_NPC_DEATH( ent, dmginfo )

	if ent and IsValid(ent) and ent.GetClass then

		local CLASS = ent:GetClass()
		
		if DONT_DIE_LMAO and DONT_DIE_LMAO[CLASS] then
			ent:SetHealth(50000)
			dmginfo:SetDamage(0)
			dmginfo:ScaleDamage(0)
		end
		
	end

end
hook.Add("EntityTakeDamage","XORDER_STOP_NPC_DEATHHook",STOP_NPC_DEATH)