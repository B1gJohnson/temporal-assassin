		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["07_ov_firm"] = {"08_ov_pension", Vector(4335.677,759.23,-7.969), Vector(-560.821,-560.821,0), Vector(560.821,560.821,303.461)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(1965.424,2138.12,-7.969), Angle(0,-101.128,0))
	
	Create_Medkit25( Vector(-770.7, 846.163, 35.377), Angle(0, 286.904, 0) )
	Create_Medkit10( Vector(-441.713, 1291.079, 35.375), Angle(-90, -47.816, -45) )
	Create_SuperShotgunWeapon( Vector(-905.245, 77.33, 44.139), Angle(0, 4.505, 0) )
	Create_Beer( Vector(-1000.839, 34.774, 38.104), Angle(0, -461.263, 0) )
	Create_Beer( Vector(-1001.063, 43.254, 38.078), Angle(0, -116.663, 0) )
	Create_Beer( Vector(-1000.664, 52.393, 38.021), Angle(0, -314.215, 0) )
	Create_ShotgunAmmo( Vector(-398.723, 1303.455, 0.633), Angle(0, 272.201, 0) )
	Create_ShotgunAmmo( Vector(-390.482, 1300.206, 0.031), Angle(0, 249.145, 0) )
	Create_SMGAmmo( Vector(-340.864, 1302.81, 0.211), Angle(0, 254.777, 0) )
	Create_SMGAmmo( Vector(-329.912, 1304.074, 0.31), Angle(0, 250.729, 0) )
	Create_RifleWeapon( Vector(142.819, 1067.559, 5.156), Angle(0, -81.608, -180) )
	Create_RifleAmmo( Vector(150.294, 1017.341, 0.203), Angle(0, -91.584, 0) )
	Create_RifleAmmo( Vector(155.328, 1018.611, 2.592), Angle(0, -86.832, 135) )
	Create_ShotgunWeapon( Vector(30.446, 1168.731, 3.963), Angle(0, -214.496, -180) )
	Create_Bread( Vector(250.608, 176.264, 40.173), Angle(0, 353.153, 0) )
	Create_Medkit25( Vector(757.176, 1275.164, 73.007), Angle(0, 249.269, 0) )
	Create_Armour100( Vector(691.106, 1281.078, 72.606), Angle(0, 283.061, 0) )
	Create_RifleAmmo( Vector(148.256, 988.023, 33.773), Angle(0, 147.813, 0) )
	
	local VEH = ents.Create("prop_vehicle_jeep")
	VEH:SetModel("models/vehicle.mdl")
	VEH:SetKeyValue( "vehiclescript", "scripts/vehicles/jalopy.txt" )
	VEH:SetPos( Vector(1880.125,2118.688,-8.156) )
	VEH:SetAngles( Angle(0.132,177.012,2.505) )
	VEH:Spawn()
	VEH:Activate()
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)