		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["13_uvalno_night"] = {"14_uvalno_houseyard", Vector(4535.969,-3228.649,95.957), Vector(-46.696,-46.696,0), Vector(46.696,46.696,107.865)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(5098.614,2099.686,-63.969), Angle(0,-117.643,0))
	
	Create_ShotgunAmmo( Vector(1659.465, 823.161, 96.18), Angle(0, 319.992, 0) )
	Create_GrenadeAmmo( Vector(2241.725, 873.25, -7.969), Angle(0, 96.92, 0) )
	Create_GrenadeAmmo( Vector(2278.962, 867.11, -7.969), Angle(0, 124.904, 0) )
	Create_GrenadeAmmo( Vector(2236.06, 855.958, -7.969), Angle(0, 92.784, 0) )
	Create_Medkit10( Vector(2318.001, 856.969, -7.398), Angle(0, 100.088, 0) )
	Create_SuperShotgunWeapon( Vector(2717.86, -964.088, 198.939), Angle(90, 54.624, -45) )
	Create_SuperShotgunWeapon( Vector(4413.581, 1116.692, 81.305), Angle(45, -2.64, -90) )
	Create_ShotgunAmmo( Vector(4419.002, 1117.373, 64.031), Angle(0, 272.04, 0) )
	Create_ShotgunAmmo( Vector(4419.098, 1116.525, 69.05), Angle(0, 271.864, 0) )
	Create_Beer( Vector(4460.616, 958.017, 101.031), Angle(0, 138.281, 0) )
	Create_Beer( Vector(4457.938, 936.031, 101.253), Angle(0, 116.545, 0) )
	Create_SMGWeapon( Vector(5003.287, 1227.001, 68.039), Angle(0, 17.096, 180) )
	Create_M6GWeapon( Vector(5173.574, 1484.054, 5.694), Angle(0, 98.217, -180) )
	Create_M6GAmmo( Vector(5239.358, 1471.198, 8.976), Angle(45, -132.536, -45) )
	Create_Armour5( Vector(5214.636, 1487.606, 7.651), Angle(32.106, -46.331, -15.383) )
	Create_Bread( Vector(4460.88, -149.178, 62.75), Angle(16.772, -87.302, 0.993) )
	Create_Armour100( Vector(4279.74, -3303.979, 133.474), Angle(0, 85.531, 0) )

	CreateSecretArea( Vector(4399.34,1120.844,64.031), Vector(-27.809,-27.809,0), Vector(27.809,27.809,36.046) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)