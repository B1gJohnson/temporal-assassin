
--[[
This here is the Client loaded file for your map.

If your maps name/main file is something like say

gm_maptest.lua

Then your client side loaded file would be

gm_maptest_cl.lua
]]

--[[
In here you can do anything you wish with client lua.

Make a new hud element
Make a boss health bar
Send messages to the players chat
Force them to select specific weapons at specific times

so on so forth, anything goes if you have the knowledge to do it.

Just please don't be a dick and delete data files and achievement data, thanks.
]]

DEV_GAMEOVER = true
--[[
Due to how gameovers are detected in Temporal Assassin, you will need to set
the variable DEV_GAMEOVER to true in your levels client lua file as shown above.
]]