
local TRAINING_JANKER = {}
TRAINING_JANKER[1] = Vector(760.256,-513.811,-453.969)
TRAINING_JANKER[2] = Vector(515.643,770.327,-81.969)
TRAINING_JANKER[3] = Vector(515.643,770.327,-131.969)
TRAINING_JANKER[4] = Angle(0,0,0)
TRAINING_JANKER[5] = Color(0,0,0,0)
TRAINING_JANKER[6] = Color(10,10,10,240)
TRAINING_JANKER[7] = Color(240,240,240,255)
TRAINING_JANKER[8] = Color(140,240,140,255)
TRAINING_JANKER[9] = Vector(751.655,-517.285,-495.969)
TRAINING_JANKER[10] = Vector(515.643,770.327,-142.969)
TRAINING_JANKER[11] = Angle(0,0,0)

timer.Create("B_ParticleFX", 1, 0, function()
	ParticleEffect( "B_Idle", TRAINING_JANKER[9], TRAINING_JANKER[11] )
	ParticleEffect( "Training_Portal", TRAINING_JANKER[10], TRAINING_JANKER[11] )
end)

local COLOUR_BOX1 = Color(0,0,0,150)
local COLOUR_BOX2 = Color(5,5,5,240)
local COLOUR_GREEN = Color(50,255,50,255)
local COLOUR_WHITE = Color(200,200,200,255)

local TRAINING_ARCHIVE = {}

local TRAINING_LIST_MOVEMENT = {
"Moving",
"Jumping",
"Wall Jumping",
"Fast Falling",
"Instant Fall",
"Air Control",
"Dodges/Dashes",
"Sliding",
"Dash Jumping",
"Hard Landing",
"Shotgun Jumping",
"Omen Gliding"}

local TRAINING_LIST_COMBAT = {
"Health",
"Armour",
"Weapons",
"Kicking",
"Slide Kicking",
"Summons",
"Temporal Dilation",
"Temporal Shifting",
"Temporal Guard",
"Mana Burst",
"Spirit Eye",
"Spirit Eye (CS-2)",
"Raikiri",
"Guarding",
"Offensive Guard",
"Fire-Arm Flux"}

local TRAINING_LIST_OTHER = {
"Outfits",
"Flashlight",
"Spirit Vision",
"Portable Medkits",
"Field Kit",
"Grenades",
"Music",
"Options",
"Secrets",
"Achievements",
"Cosmic Realm"}

TRAINING_ARCHIVE["Moving"] = "Moving around is easy, I'm pretty sure you know how to move around kid."
TRAINING_ARCHIVE["Jumping"] = "Jumping is simple, you know how to do it. However holding it down before leaving the ground makes you jump higher. As soon as your jump squat starts you're invulnerable to projectiles for a short while."
TRAINING_ARCHIVE["Wall Jumping"] = "Jumping mid-air near a surface will let you kick off that surface in the direction you're looking if you're not holding a movement key, if you want to climb up a wall make sure you're looking up when you jump against it. Wall jumping also grants you invulnerability just like normal jumping does but note that you can also Hold Jump to cling to the wall, Holding a directional movement key when releasing the key will give you a free dodge in the direction you're holding"
TRAINING_ARCHIVE["Fast Falling"] = "Holding crouch mid-air while falling will make you fall much faster, stops you being such an easy target at the apex of your jumps."
TRAINING_ARCHIVE["Instant Fall"] = "Double tapping crouch mid-air will make you plummet instantly, try not to do this over bottomless pits or anything like that."
TRAINING_ARCHIVE["Air Control"] = "Holding a direction while in the air will push you in that direction constantly, building speed over-time up to an infinite amount."
TRAINING_ARCHIVE["Dodges/Dashes"] = "While moving, tapping 'Sprint' makes you dodge in that direction, Dodges give you invulnerability to projectiles and a small burst of speed in that direction too. Dodging/Dashing into a wall gives you all the projectile invulnerability without any of the motion, using a wall this way allows for safe guarding against projectiles while keeping a steady aim too."
TRAINING_ARCHIVE["Sliding"] = "Holding 'Sprint' after a dodge automatically makes you slide, sliding grants projectile invulnerability as long as you keep sliding. You can also start a slide by holding 'Sprint' without moving, then tapping Crouch. During a slide, strafing left or right will consume another dodge/dash tick for a dodge to the slide, continue holding 'Sprint' to slide out of these ones too."
TRAINING_ARCHIVE["Dash Jumping"] = "Dodging/Dashing in a direction and instantly queing up a Hop/Full Hop will make you do a Dash Jump, you'll keep your velocity as you jump so you can use this travel a fair distance in a short time."
TRAINING_ARCHIVE["Hard Landing"] = "As inefficient as it is, Landing on enemies with alot of force will deal a large amount of damage if they're not large enough to resist it."
TRAINING_ARCHIVE["Shotgun Jumping"] = "While mid-air, firing the Snake Charmer will push you back in the opposite direction you're looking, the charge shot pushes you even more, combining this with perfect timing of a jump while looking down will give you an incredibly high jump."
TRAINING_ARCHIVE["Omen Gliding"] = "Shooting the Omen mid-air will cause you to bump up in the air a bit, sustaining rapid fire will cause you to keep floating for longer, in Temporal Dilation (Slow-Mo) this effect is even more apparent as you'll gain height much faster."

TRAINING_ARCHIVE["Health"] = "You know what this is, it reaches zero, you start over."
TRAINING_ARCHIVE["Armour"] = "Armour protects you from more damage, having 1 to 100 armour protects you for -60% damage taken, having 101 to 200 armour protects you for -80% damage taken. Eldritch armour on the other hands blocks all damage as the eldritch armour absorbs it."
TRAINING_ARCHIVE["Weapons"] = "You'll come across all sorts of face ruining weaponry while you're out there, using that smoking set of lockers near the stairs will give you practically everything that could possibly be at your disposal, yes 'Practically', there are some things that even that thing can't give you. You can try out whatever you want on the targets outside."
TRAINING_ARCHIVE["Kicking"] = "Kicking is pretty simple, however holding the button down will charge your kick up, making it even stronger. Stronger kicks deal much much higher damage and knock objects back much further, kicking right after a dodge also allows you to do a Drop Kick. Max Charge Drop kicks have alot of commitment but do alot of damage and give a bonus to TMP/Burst generation, plus it's pretty stylish. Low or No Charge Drop Kicks however will give you a smaller bonus to TMP/Burst generation, but you'll kick off of whatever you hit and get some air time."
TRAINING_ARCHIVE["Slide Kicking"] = "You'll hit stuff automatically while sliding, this includes enemies. It doesn't do much damage but can finish off a weakened enemy and fling them up into the air for a potential snack for a specific summon."
TRAINING_ARCHIVE["Summons"] = "Summons can be found while you're out there, you need a conduit to channel your magic through in order to summon them which is why they're not always available unless you have said conduits. Summons are extremely powerful attacks that cost a huge amount of TMP to do, you can also Pet your summons by holding the button down for a long time, in turn this will make them affectionate and increase their power."
TRAINING_ARCHIVE["Temporal Dilation"] = "I'm not calling it that. Slowing down time is one of your inherent skills, use it to act faster than your enemy can, it's that simple. Just be aware that once your TMP hits zero you'll be forced out of it. You can swap between Slow Time and Stop Time while doing either one too."
TRAINING_ARCHIVE["Temporal Shifting"] = "Stopping time is another skill you have, using it even stops bullets mid-flight, stops enemies in their tracks, you name it. You can use this to get out of really bad situations or to set up a massive wave of bullets/attacks towards your enemies before resuming time to watch the carnage you've wrought. You can swap between Slow Time and Stop Time while doing either one too. Be careful though, stopping time over and over again rapidly will de-stabalize your ability to do so. This is seen as a red pause icon over your mana amount, the higher it is the more Mana it costs to stop time and maintain it. It will diminish over time as long as you're not using Shift or Dilation abilities"
TRAINING_ARCHIVE["Temporal Guard"] = "This is the number one thing you'll need to stay alive, holding a guard costs a lot of stamina, however while in said guard you'll take heavily reduced damage if your TMP isn't capped out, if it is capped out then the damage won't be reduced as much, Releasing your guard as you get hit has you do a Repel, freezing enemies for a short period of time. Taking hits in your guard will also generate TMP. There's an exploding barrel on the roof outside that'll explode when you get near it, you can use that for practice or turn the brains of the guys down range back on. Guarding and getting hit almost instantly then releasing your guard will do an Adept Repel instead which has a longer freeze time and wider range."
TRAINING_ARCHIVE["Mana Burst"] = "The diamond you see at the bottom left is your Mana Burst Gauge, once it's full you can hit 'Attack' while Temporal Guarding to activate it, while mana bursting Slow and Stop time won't drain TMP and instead drain the Burst Gauge, other actions will still cost TMP but while bursting TMP regenerates incredibly quickly too."
TRAINING_ARCHIVE["Spirit Eye"] = "If you have the Roaring Wolf revolver, you'll get 2 bullet like icons that fill up as you murder stuff, once atleast one bullet is full and you have enough TMP, you can use the 'WEAPON ABILITY' button while holding the Roaring Wolf to activate Spirit Eye. 'ATTACK' will execute Spirit Eye, 'ALT ATTACK' will cancel it, moving your crosshair over targets will mark them."
TRAINING_ARCHIVE["Spirit Eye (CS-2)"] = "If you have the Cobalt Storm-2 revolver, you'll get 2 bullet like icons that fill up as you murder stuff, once atleast one bullet is full, you can use the 'WEAPON ABILITY' button while holding the Cobalt Storm-2 to load up a super charged magazine with 5 shots, each shot that hits while super charged deals much more damage and automatically arc-jolts to other enemies, pretty shocking."
TRAINING_ARCHIVE["Raikiri"] = "The Raikiri is a powerful tool kid, Hitting enemies will build up a combo and as long as you keep hitting the same bad guy it'll build up higher, You can use Weapon Ability + Movement to do special attacks too, the Forward Special attack has a unique property of using all your current combo gauge to enhance the damage of the attack as much as possible while the others reduce the cost of the abilities. Blocking an attack will also remove the last person you hit from the Combo, Offensive Guarding a melee attack will increase your combo by 1 as well, dodging bullets without taking damage will also give you +1 combo. If you're looking to stun smaller guys Crouch + Weapon Ability while high off the ground will give you just that."
TRAINING_ARCHIVE["Guarding"] = "Blocking with a melee weapon can be life saving however it tends to cost a lot of Stamina to do so. Losing all of your stamina and still blocking will result in a Guard Break and as you can imagine it's not something you want to happen. If you block attacks that are too powerful, or explosives you'll be instantly guard broken so be careful around those."
TRAINING_ARCHIVE["Offensive Guard"] = "Blocking an attack with any of your melee weapons with perfect timing will activate this, the weapons name at the bottom right will glow blue and red too as long as it's active. While active you'll take 25% less damage and deal 40% more melee damage. Give it a try against the guys down range once you turn their brains back on, or you could try it against the explosive barrel on the roof, but even if you Offensive Guard an explosive you'll still be guard broken."
TRAINING_ARCHIVE["Fire-Arm Flux"] = "Doing a Temporal Repel from a Temporal Guard while having Offensive Guard active will do one of these, you'll reload all of your weapons from reserve instantly without any need to reload them yourself."

TRAINING_ARCHIVE["Outfits"] = "The white locker with your last name on it, you can use it to change your outfit. Each outfit has pros and cons to them so pick whatever you feel like wearing."
TRAINING_ARCHIVE["Flashlight"] = "Always carry one, hitting the 'FLASHLIGHT' key will toggle it on and off."
TRAINING_ARCHIVE["Spirit Vision"] = "Holding your 'FLASHLIGHT' key for a short time will turn Spirit Vision on and off, it will stall your passive TMP regeneration (the regeneration from waiting) so using it while waiting for TMP to regenerate isn't the best idea. While active spirit vision will light up an incredibly high amount of surface area, much better than the flashlight."
TRAINING_ARCHIVE["Portable Medkits"] = "If you have any, tapping your 'USE MEDKIT' button will use one, you can see the number of how many you have at the bottom left, they heal 50 HP per kit."
TRAINING_ARCHIVE["Field Kit"] = "This one's pretty simple. Any excess healing from medkits is stored in your Field Kit, Any healing you apply to yourself via Medkits has 20% of the healing stored. Once your Field Kit is at 100% you'll make a Portable Medkit out of it."
TRAINING_ARCHIVE["Grenades"] = "If you have any, tapping your 'EQUIP GRENADES' button will pull them out, 'ATTACK' will throw one far and 'ALT ATTACK' will drop one just infront of you."
TRAINING_ARCHIVE["Music"] = "You can change your music in the F4 'OPTIONS' menu on the right side under Music Type. Hitting the 'PLAY MUSIC' button will start the music, hitting it again will play another random song. You can stop music at any time using the 'STOP MUSIC' button."
TRAINING_ARCHIVE["Options"] = "Hitting the F4 Key will open the Options menu, you can setup your controls and stuff in there, along side customizing a bunch of other stuff."
TRAINING_ARCHIVE["Secrets"] = "You'll find secrets out there if you look carefully enough, you'll know when you find one but keep an eye out for Secret Buttons, those will always usually have some sort of hint near them, if that's not enough then standing near a secret button for about 10 seconds will show you the button with a wolf insignia effect."
TRAINING_ARCHIVE["Achievements"] = "Achievements can be accessed in the EXTRA OPTIONS menu or via the F3 key while not in this training realm. Certain achivements will unlock coats for you to use, think of it as a test to see if you're ready for them or not."
TRAINING_ARCHIVE["Cosmic Realm"] = "These places are invisible to even you Fox, you'll know when you're in one. While inside of a cosmic realm your TMP/Burst Gauge will drain faster, your stamina will regenerate at half the rate, the shade cloak will stop working, you'll take more damage, deal a little less damage too and your Temporal Guard won't be as effective. Some of your dodges will be taken away but killing enemies inside of the cosmic realm will generate Eldritch Armour."

if TA_HasLoreFile("item_eldritcharmourshard_1") and TA_HasLoreFile("weapon_hergift_1") and TA_HasLoreFile("item_eldritcharmourset_1") and TA_HasLoreFile("lore_mthuulra_1") and TA_HasLoreFile("lore_mthuulra_6") then
TRAINING_ARCHIVE["Armour"] = "Armour protects you from more damage, having 1 to 100 armour protects you for -60% damage taken, having 101 to 200 armour protects you for -80% damage taken. Eldritch armour on the other hands blocks all damage as the eldritch armour absorbs it, dunno how your mother makes that stuff."
TRAINING_ARCHIVE["Weapons"] = "You'll come across all sorts of face ruining weaponry while you're out there, using that smoking set of lockers near the stairs will give you practically everything that could possibly be at your disposal, yes 'Practically', there are some things that even that thing can't give you that have to do with that mom of yours. You can try out whatever you want on the targets outside."
TRAINING_ARCHIVE["Temporal Dilation"] = "I'm not calling it that no matter how much your mom tells me. Slowing down time is one of your inherent skills, use it to act faster than your enemy can, it's that simple. Just be aware that once your TMP hits zero you'll be forced out of it. You can swap between Slow Time and Stop Time while doing either one too."
end

local Font_Table = {
	font 		= "Arial Black",
	size 		= GUI_X(12),
	weight 		= 800,
	antialias 	= true,
	additive 	= false,
	blursize	= 0,
	scanlines	= 0
}
surface.CreateFont( "Training_HUD", Font_Table )

local Font_Table = {
	font 		= "Arial Black",
	size 		= GUI_X(12),
	weight 		= 800,
	antialias 	= true,
	additive 	= false,
	blursize	= 3,
	scanlines	= 0
}
surface.CreateFont( "Training_HUD2", Font_Table )

local Font_Table = {
	font 		= "Arial Black",
	size 		= GUI_X(20),
	weight 		= 800,
	antialias 	= true,
	additive 	= false,
	blursize	= 0,
	scanlines	= 0
}
surface.CreateFont( "Training_HUD3", Font_Table )

local Font_Table = {
	font 		= "Arial Black",
	size 		= GUI_X(20),
	weight 		= 800,
	antialias 	= true,
	additive 	= false,
	blursize	= 3,
	scanlines	= 0
}
surface.CreateFont( "Training_HUD4", Font_Table )

local Font_Table = {
	font 		= "Arial Black",
	size 		= GUI_X(16),
	weight 		= 800,
	antialias 	= true,
	additive 	= false,
	blursize	= 0,
	scanlines	= 0
}
surface.CreateFont( "Training_HUD5", Font_Table )

local Font_Table = {
	font 		= "Arial Black",
	size 		= GUI_X(16),
	weight 		= 800,
	antialias 	= true,
	additive 	= false,
	blursize	= 3,
	scanlines	= 0
}
surface.CreateFont( "Training_HUD6", Font_Table )

local TRAIN_DIFF_NAME = {}
TRAIN_DIFF_NAME[1] = "Basement Dweller"
TRAIN_DIFF_NAME[2] = "Hometown Hero"
TRAIN_DIFF_NAME[3] = "From Coast to Coast"
TRAIN_DIFF_NAME[4] = "Globe Trotter"
TRAIN_DIFF_NAME[5] = "Queen of the Galaxy"
TRAIN_DIFF_NAME[6] = "Breaker of Time and Space"
TRAIN_DIFF_NAME[7] = "Temporal Assassin"
TRAIN_DIFF_NAME[8] = "I'M WATCHING"
TRAIN_DIFF_NAME[9] = "PERISH INTO THE COSMOS"
TRAIN_DIFF_NAME[10] = "EXISTANCE EXPUNGED"
TRAIN_DIFF_NAME[11] = "RETURN TO NOTHING"
TRAIN_DIFF_NAME[12] = "CONTROL THE VOID"
TRAIN_DIFF_NAME[13] = "DATHARON LIVES"
TRAIN_DIFF_NAME[14] = "DATHARON MUST DIE"
TRAIN_DIFF_NAME[15] = "VOID HUNGER"
TRAIN_DIFF_NAME[16] = "?"

function Training_DifficultyName()

	local DIFF = GetDifficulty2()
	
	return TRAIN_DIFF_NAME[DIFF]

end

TRAINING_LAST_SHOT_RESET = 0
TRAINING_LAST_SHOT_DMG = 0
TRAINING_LAST_SHOT_COUNT = 0
TRAINING_LAST_SHOT_LASTTIME = 0
TRAINING_LAST_SHOT_LASTTIME2 = 0
TRAINING_LAST_SHOT_DISTANCE = 0

function Training_ShotDamage_LIB( len )

	local DMG = net.ReadFloat()
	local POS = net.ReadVector()
	local CT = UnPredictedCurTime()
	
	if TRAINING_LAST_SHOT_RESET and CT >= TRAINING_LAST_SHOT_RESET then
		TRAINING_LAST_SHOT_DMG = 0
		TRAINING_LAST_SHOT_COUNT = 0
		TRAINING_LAST_SHOT_LASTTIME = 0
	end
	
	TRAINING_LAST_SHOT_RESET = CT + 1
	TRAINING_LAST_SHOT_DMG = TRAINING_LAST_SHOT_DMG + DMG
	TRAINING_LAST_SHOT_COUNT = TRAINING_LAST_SHOT_COUNT + 1
	
	if TRAINING_LAST_SHOT_DMG >= 20000 then
		SetAchievementProgress( "GOOD_ENOUGH", 1, true )
		SaveAchievements()
	end
	
	if TRAINING_LAST_SHOT_LASTTIME and TRAINING_LAST_SHOT_LASTTIME != 0 then
		TRAINING_LAST_SHOT_LASTTIME = TRAINING_LAST_SHOT_LASTTIME + (CT - TRAINING_LAST_SHOT_LASTTIME2)
		TRAINING_LAST_SHOT_LASTTIME2 = CT
	else
		TRAINING_LAST_SHOT_LASTTIME = 0.00001
		TRAINING_LAST_SHOT_LASTTIME2 = CT
	end
	
	TRAINING_LAST_SHOT_DISTANCE = POS:Distance( LocalPlayer().VIEW_POSITION )

end
net.Receive("Training_ShotDamage",Training_ShotDamage_LIB)

local B_BANTER_SND = {
Sound("TemporalAssassin/Training/B_Banter1.wav"),
Sound("TemporalAssassin/Training/B_Banter2.wav"),
Sound("TemporalAssassin/Training/B_Banter3.wav"),
Sound("TemporalAssassin/Training/B_Banter4.wav")}

function Training_SendBanter_LIB()

	local PL = LocalPlayer()
	
	if PL.TRAINING_MENU then return end
	if SUIT_LOCKER_ENABLED then return end
	if GAME_OVER_ENABLED then return end
	if PL.EasyBind_SettingUp then return end
	if PL.GameGuide_Open then return end

	Training_DoBBanter_RAND()

end
net.Receive("Training_SendBanter",Training_SendBanter_LIB)

file.CreateDir("TemporalAssassin/Training/")

local BANTER_LINES = {}
BANTER_LINES[0] = {
"Finally here kid? Let's get started, I'm B and this little corner dimension will let you hone your skills while off the job. You can also talk (USE KEY) to me to learn specifics."}

BANTER_LINES[1] = {
"White locker has all your coats in it, Blue locker has all your weaponry. It was really hard dragging those in here.",
"You can practice your Temporal Guard and Repel up on the roof outside against the Explosive Barrel.",
"If you want to leave you can use the portal on the roof outside.",
"Take as long as you need kid, Practice makes perfect and what not.",
"I'll say it now, try not to pet the bird. It's fidgety around new people and you smell of blood and gunpowder.",
"I hope you don't make too much of a... Ah, who am I kidding, you're gonna make a huge mess of the place."}

BANTER_LINES[2] = {
"White Locker, Coats. Blue Locker, Weapons. You know the drill, try not to make too much of a mess.",
"I wonder if that explosive barrel on the roof is being poofed in from other timelines or if it's a paradox object. Atleast it helps with practice.",
"I swear that thing is around here somewhere, it might appear as an innocent doll but it isn't. Not even close.",
"It's a good thing that exit portal doesn't transport blood, can't imagine what your apartment would look like otherwise.",
"A reminder that I have restricted your ability to die here, I however did not take away pain. Feel free to experiment.",
"If you happen to see any tearing in space-time or other quirks of reality, tell me. The copying of this area wasn't perfect."}

local BANT_COUNT = 0

function Training_DoBBanter_RAND()

	local LORE_FILE_COUNT = 0
	
	if TA_HasLoreFile("training_banter_1") then
		LORE_FILE_COUNT = LORE_FILE_COUNT + 1
	end
	
	if TA_HasLoreFile("training_banter_2") then
		LORE_FILE_COUNT = LORE_FILE_COUNT + 1
	end
	
	local RAND = table.Random(BANTER_LINES[LORE_FILE_COUNT])
	
	if LORE_FILE_COUNT == 0 then
		Unlock_TA_LoreFile("training_banter_1","B Familiarity - Level 1")
	elseif LORE_FILE_COUNT == 1 then
		BANT_COUNT = BANT_COUNT + 1
		if BANT_COUNT >= 7 then
			BANT_COUNT = 0
			Unlock_TA_LoreFile("training_banter_2","B Familiarity - Level 2")
		end
	elseif LORE_FILE_COUNT == 2 then
	end
	
	Training_DoBBanter(RAND)
	
end

function Training_DoBBanter( txt )

	local PL = LocalPlayer()
	
	PL.BBanter_Line = txt
	PL.BBanter_Time = UnPredictedCurTime() + 10
	
	PL:EmitSound( table.Random(B_BANTER_SND), 80, math.random(98,102), 0.8, CHAN_STATIC )

end

function Training_DrawBBanter()

	local PL = LocalPlayer()
	local PUSH = 0
	local CT = UnPredictedCurTime()
	
	if PL.BBanter_Time and CT < PL.BBanter_Time then
		PUSH = 200
	end
	
	local B_PUSH = Variable_SmoothOut( "B_TEXTBOX_PUSH", PUSH, 5, true, 1, 5000 )
	
	if B_PUSH > 0 and PL.BBanter_Line then
	
		local COL = COLOUR_WHITE
		local COL2 = Color(100,100,100,255)
		
		draw.RoundedBox( 4, GUI_X(164), GUI_Y(23 - 200 + B_PUSH), GUI_X(304), GUI_Y(74), COLOUR_BOX1 )
		draw.RoundedBox( 4, GUI_X(166), GUI_Y(25 - 200 + B_PUSH), GUI_X(300), GUI_Y(70), COLOUR_BOX2 )
		
		local TEXT_TABLE = WrapText( PL.BBanter_Line, 60 )
		local Y = (23 - 205 + B_PUSH)
		
		for _,v in pairs (TEXT_TABLE) do
			
			Y = Y + 20
			DrawShadowText( v, "Locker_NormalTXT", GUI_X(315), GUI_Y(Y), COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL2 )
			
		end
		
	end

end

function Training_HUD()

	local PL = LocalPlayer()
	
	if PL.TRAINING_MENU then
		
		local TRAINING_CLICK = MousePressed("TRAINING_CLICK")
			
		local COL = Color(200,200,200,255)
		local COL2 = Color(100,100,100,255)
			
		local COL_OP = Color(100,120,255,255)
		local COL_OP2 = Color(50,111,82,255)
			
		local COL_GOP = Color(50,200,50,255)
		local COL_GOP2 = Color(150,0,255,255)
	
		if PL.TRAINING_MENU == 2 and PL.TRAINING_MENU_TEXT then
	
			draw.RoundedBox( 4, GUI_X(110), GUI_Y(13), GUI_X(404), GUI_Y(439), COLOUR_BOX1 )
			draw.RoundedBox( 4, GUI_X(112), GUI_Y(15), GUI_X(400), GUI_Y(435), COLOUR_BOX2 )
			
			local TEXT_TABLE = WrapText( PL.TRAINING_MENU_TEXT, 55 )
			
			local X, Y = 312, 30
			
			if PL.TRAINING_MENU_TITLE then
				DrawShadowText( PL.TRAINING_MENU_TITLE, "Training_HUD3", GUI_X(X), GUI_Y(Y), COL_OP, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Training_HUD4", COL_OP2 )
			end
			
			Y = 40
			
			for _,v in pairs (TEXT_TABLE) do
			
				Y = Y + 20
				DrawShadowText( v, "Training_HUD5", GUI_X(X), GUI_Y(Y), COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Training_HUD6", COL2 )
			
			end
			
			if MouseInside2( GUI_X(312 - 50), GUI_Y(424 - 8), GUI_X(312 + 50), GUI_Y(424 + 8) ) then
			
				draw.SimpleText( "Alright", "Locker_CloseTXT", GUI_X(312), GUI_Y(424), COLOUR_GREEN, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if TRAINING_CLICK then
					PL.TRAINING_MENU = true
					PL:EmitSound( Sound("TemporalAssassin/UI_B/HUD_Click_B.wav"), 80, 80, 0.8, CHAN_STATIC )
				end
				
			else
				draw.SimpleText( "Alright", "Locker_CloseTXT", GUI_X(312), GUI_Y(424), COLOUR_WHITE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
		
		else
	
			draw.RoundedBox( 4, GUI_X(110), GUI_Y(13), GUI_X(404), GUI_Y(439), COLOUR_BOX1 )
			draw.RoundedBox( 4, GUI_X(112), GUI_Y(15), GUI_X(400), GUI_Y(435), COLOUR_BOX2 )
			
			DrawShadowText( "You need something kid?", "Locker_NormalTXT", GUI_X(312), GUI_Y(30), COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL2 )
			
			if MouseInside2( GUI_X(312 - 50), GUI_Y(424 - 8), GUI_X(312 + 50), GUI_Y(424 + 8) ) then
			
				draw.SimpleText( "Nevermind", "Locker_CloseTXT", GUI_X(312), GUI_Y(424), COLOUR_GREEN, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				if TRAINING_CLICK then
					PL.TRAINING_MENU = false
					gui.EnableScreenClicker(false)
					NoDrawCursor(true)
					PL:EmitSound( Sound("TemporalAssassin/UI_B/HUD_Click_B.wav"), 80, 80, 0.8, CHAN_STATIC )
				end
				
			else
				draw.SimpleText( "Nevermind", "Locker_CloseTXT", GUI_X(312), GUI_Y(424), COLOUR_WHITE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
			local X_POSI = 180
			local PLUS_Y = 20
			
			DrawShadowText( "- Movement -", "Locker_NormalTXT", GUI_X(X_POSI), GUI_Y(60), COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL2 )
			
			for _,v in pairs (TRAINING_LIST_MOVEMENT) do
			
				PLUS_Y = PLUS_Y + 18
				
				local MOUSE_IN = MouseInside2( GUI_X(X_POSI - 40), GUI_Y(40 + PLUS_Y - 8), GUI_X(X_POSI + 40), GUI_Y(40 + PLUS_Y + 8) )
				
				if MOUSE_IN then
					if TRAINING_CLICK then
						PL.TRAINING_MENU_TEXT = TRAINING_ARCHIVE[v]
						PL.TRAINING_MENU_TITLE = v
						PL.TRAINING_MENU = 2
						PL:EmitSound( Sound("TemporalAssassin/UI_B/HUD_Click_G.wav"), 80, 100, 0.8, CHAN_STATIC )
					end
					DrawShadowText( v, "Locker_NormalTXT", GUI_X(X_POSI), GUI_Y(40 + PLUS_Y), COL_GOP, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL_GOP2 )
				else
					DrawShadowText( v, "Locker_NormalTXT", GUI_X(X_POSI), GUI_Y(40 + PLUS_Y), COL_OP, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL_OP2 )
				end
			
			end
			
			X_POSI = 312
			PLUS_Y = 20
			
			DrawShadowText( "- Combat -", "Locker_NormalTXT", GUI_X(X_POSI), GUI_Y(60), COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL2 )
			
			for _,v in pairs (TRAINING_LIST_COMBAT) do
			
				PLUS_Y = PLUS_Y + 18
				
				local MOUSE_IN = MouseInside2( GUI_X(X_POSI - 40), GUI_Y(40 + PLUS_Y - 8), GUI_X(X_POSI + 40), GUI_Y(40 + PLUS_Y + 8) )
				
				if MOUSE_IN then
					if TRAINING_CLICK then
						PL.TRAINING_MENU_TEXT = TRAINING_ARCHIVE[v]
						PL.TRAINING_MENU_TITLE = v
						PL.TRAINING_MENU = 2
						PL:EmitSound( Sound("TemporalAssassin/UI_B/HUD_Click_G.wav"), 80, 100, 0.8, CHAN_STATIC )
					end
					DrawShadowText( v, "Locker_NormalTXT", GUI_X(X_POSI), GUI_Y(40 + PLUS_Y), COL_GOP, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL_GOP2 )
				else
					DrawShadowText( v, "Locker_NormalTXT", GUI_X(X_POSI), GUI_Y(40 + PLUS_Y), COL_OP, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL_OP2 )
				end
			
			end
			
			X_POSI = 433
			PLUS_Y = 20
			
			DrawShadowText( "- Misc -", "Locker_NormalTXT", GUI_X(X_POSI), GUI_Y(60), COL, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL2 )
			
			for _,v in pairs (TRAINING_LIST_OTHER) do
			
				PLUS_Y = PLUS_Y + 18
				
				local MOUSE_IN = MouseInside2( GUI_X(X_POSI - 40), GUI_Y(40 + PLUS_Y - 8), GUI_X(X_POSI + 40), GUI_Y(40 + PLUS_Y + 8) )
				
				if MOUSE_IN then
					if TRAINING_CLICK then
						PL.TRAINING_MENU_TEXT = TRAINING_ARCHIVE[v]
						PL.TRAINING_MENU_TITLE = v
						PL.TRAINING_MENU = 2
						PL:EmitSound( Sound("TemporalAssassin/UI_B/HUD_Click_G.wav"), 80, 100, 0.8, CHAN_STATIC )
					end
					DrawShadowText( v, "Locker_NormalTXT", GUI_X(X_POSI), GUI_Y(40 + PLUS_Y), COL_GOP, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL_GOP2 )
				else
					DrawShadowText( v, "Locker_NormalTXT", GUI_X(X_POSI), GUI_Y(40 + PLUS_Y), COL_OP, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, "Locker_NormalTXTS", COL_OP2 )
				end
			
			end
			
		end
		
		F4MENU_DrawCursor()
	
	else
	
		local CT = UnPredictedCurTime()
		local CT_A = (TRAINING_LAST_SHOT_LASTTIME2 or 0)
		local DIFF = GetDifficulty()
		local PLAYER_DMG = DIFF[4]
		local NPC_DMG = DIFF[5]
		local POS_X = GUI_X2(632)
		local POS_Y = GUI_Y(300)
		local DIF_NAME = Training_DifficultyName()
		
		Training_DrawBBanter()
		
		DrawShadowText( DIF_NAME, "Training_HUD", POS_X, POS_Y, Color(20,20,20,255), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER, "Training_HUD2", Color(200,200,200,155) )	
		
		if PLAYER_DMG >= 10 then
			PLAYER_DMG = tostring(PLAYER_DMG.." (-2)")
		end
		
		DrawShadowText("Player DMG Taken: x"..PLAYER_DMG, "Training_HUD", POS_X, POS_Y + GUI_Y(20), Color(20,20,20,255), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER, "Training_HUD2", Color(200,200,200,155) )
		
		if NPC_DMG <= 0.4 then
			NPC_DMG = tostring(NPC_DMG.." (+0.1)")
		end
		
		DrawShadowText("Enemy DMG Taken: x"..NPC_DMG, "Training_HUD", POS_X, POS_Y + GUI_Y(40), Color(20,20,20,255), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER, "Training_HUD2", Color(200,200,200,155) )
		
		POS_X = GUI_X2(632)
		POS_Y = GUI_Y(210)
		
		local DAMAGE = math.Round(TRAINING_LAST_SHOT_DMG,2)
		local COUNTER = TRAINING_LAST_SHOT_COUNT
		local DMG_TIME = math.Round(TRAINING_LAST_SHOT_LASTTIME,2)
		local DMG_DIST = math.Round((TRAINING_LAST_SHOT_DISTANCE/52.49),2)
		local DMG_COL = Color(20,20,20,255)
		
		if TRAINING_LAST_SHOT_RESET and CT < TRAINING_LAST_SHOT_RESET then
		
			local MUL = (TRAINING_LAST_SHOT_RESET - CT)/1
			
			DMG_COL = Color(20+(225*MUL),20,20,255)
		
		end
		
		DrawShadowText( "F3 - Open Edit Menu", "Training_HUD", POS_X, POS_Y - GUI_Y(25), DMG_COL, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER, "Training_HUD2", Color(200,200,200,155) )
		DrawShadowText( "Damage Amount: "..DAMAGE, "Training_HUD", POS_X, POS_Y, DMG_COL, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER, "Training_HUD2", Color(200,200,200,155) )
		DrawShadowText( "Hit Count: "..COUNTER, "Training_HUD", POS_X, POS_Y + GUI_Y(20), DMG_COL, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER, "Training_HUD2", Color(200,200,200,155) )
		DrawShadowText( "Combo Time: "..DMG_TIME.."s", "Training_HUD", POS_X, POS_Y + GUI_Y(40), DMG_COL, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER, "Training_HUD2", Color(200,200,200,155) )
		DrawShadowText( "Distance: "..DMG_DIST.." Metres", "Training_HUD", POS_X, POS_Y + GUI_Y(60), DMG_COL, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER, "Training_HUD2", Color(200,200,200,155) )
	
	end
	
end
hook.Add("HUDPaint","Training_HUDHook",Training_HUD)

local Font_Table = {
	font 		= "Digital",
	size 		= GUI_X(80),
	weight 		= 200,
	antialias 	= true,
	additive 	= false,
	blursize	= 0,
	scanlines	= 0
}
surface.CreateFont( "TRAINING_Buttons", Font_Table )

function TRAINING_GetEyeHit()

	local PL = LocalPlayer()
	local POS, ANG = (PL.VIEW_POSITION or PL:EyePos()), (AIM_BULLET_ANGLE or PL:EyeAngles())
	
	local TR = {}
	TR.start = POS
	TR.endpos = POS + ANG:Forward()*120
	TR.mask = MASK_PLAYERSOLID
	TR.filter = {PL}
	TR.mins = Vector(-0.5,-0.5,-0.5)
	TR.maxs = Vector(0.5,0.5,0.5)
	local Trace = util.TraceHull(TR)
	
	return Trace.HitPos

end

local TRAINING_POWERUPS = {}

TRAINING_POWERUPS[0] = {"Sin Fragment","Create_SinFragment"}
TRAINING_POWERUPS[1] = {"Beer","Create_Beer"}
TRAINING_POWERUPS[2] = {"Bread","Create_Bread"}
TRAINING_POWERUPS[3] = {"Floor Turkey","Create_Turkey"}
TRAINING_POWERUPS[4] = {"Health Inhaler","Create_Medkit10"}
TRAINING_POWERUPS[5] = {"Medkit","Create_Medkit25"}
TRAINING_POWERUPS[6] = {"A.T.K","Create_Medkit50"}
TRAINING_POWERUPS[7] = {"Portable Medkit","Create_PortableMedkit"}
TRAINING_POWERUPS[8] = {"Armour Fragment","Create_Armour1"}
TRAINING_POWERUPS[9] = {"Armour Shard","Create_Armour5"}
TRAINING_POWERUPS[10] = {"Light Armour","Create_Armour"}
TRAINING_POWERUPS[11] = {"Armoured Vest","Create_Armour100"}
TRAINING_POWERUPS[12] = {"Reinforced Combat Rig","Create_Armour200"}
TRAINING_POWERUPS[13] = {"Sliver of E-Armour","Create_EldritchArmour10"}
TRAINING_POWERUPS[14] = {"Eldritch Armour","Create_EldritchArmour"}
TRAINING_POWERUPS[15] = {"Mana Crystal","Create_ManaCrystal"}
TRAINING_POWERUPS[16] = {"Great Mana Crystal","Create_ManaCrystal100"}
TRAINING_POWERUPS[17] = {"Spirit Sphere","Create_SpiritSphere"}
TRAINING_POWERUPS[18] = {"Yokai Soul","Create_MegaSphere"}
TRAINING_POWERUPS[19] = {"Overdrive Sphere","Create_OverdriveSphere"}
TRAINING_POWERUPS[20] = {"Quad Damage","Create_QuadDamage"}
TRAINING_POWERUPS[21] = {"White Crystal","Create_WhiteCrystal"}
TRAINING_POWERUPS[22] = {"Yokai Essence","Create_InvulnSphere"}
TRAINING_POWERUPS[23] = {"Backpack","Create_Backpack"}

local TRAINING_ENEMY_CLASSES = {}

TRAINING_ENEMY_CLASSES[1] = {"Combine Soldier","npc_combine_s","weapon_smg1",true}
TRAINING_ENEMY_CLASSES[2] = {"Heavy Combine","npc_heavycombine","tnpc_crystalrifle",true}
TRAINING_ENEMY_CLASSES[3] = {"Metro Police","npc_metropolice","weapon_stunstick",true}
TRAINING_ENEMY_CLASSES[4] = {"Manhacks","npc_manhack","",true}
TRAINING_ENEMY_CLASSES[5] = {"Zombie","npc_zombie","",true}
TRAINING_ENEMY_CLASSES[6] = {"Fast Zombie","npc_fastzombie","",true}
TRAINING_ENEMY_CLASSES[7] = {"Poison Zombie","npc_poisonzombie","",true}
TRAINING_ENEMY_CLASSES[8] = {"Exhumed","npc_exhumed","",true}
TRAINING_ENEMY_CLASSES[9] = {"Antlion","npc_antlion","",true}
TRAINING_ENEMY_CLASSES[10] = {"Antlion Guard","npc_antlionguard","",true}
TRAINING_ENEMY_CLASSES[11] = {"Antlion Worker","npc_antlion_worker","",IsMounted("ep2")}
TRAINING_ENEMY_CLASSES[12] = {"Zombine","npc_zombine","",(IsMounted("episodic") or IsMounted("ep2"))}
TRAINING_ENEMY_CLASSES[13] = {"Hunter","npc_hunter","",IsMounted("ep2")}
TRAINING_ENEMY_CLASSES[14] = {"Headcrab","npc_headcrab","",true}
TRAINING_ENEMY_CLASSES[15] = {"Fast Headcrab","npc_headcrab_fast","",true}
TRAINING_ENEMY_CLASSES[16] = {"Poison Headcrab","npc_headcrab_poison","",true}
TRAINING_ENEMY_CLASSES[17] = {"Alien Grunt","monster_alien_grunt","",IsMounted("hl1")}
TRAINING_ENEMY_CLASSES[18] = {"Alien Slave","monster_alien_slave","",IsMounted("hl1")}
TRAINING_ENEMY_CLASSES[19] = {"Alien Controller","monster_alien_controller","",IsMounted("hl1")}
TRAINING_ENEMY_CLASSES[20] = {"Bullsquid","monster_bullchicken","",IsMounted("hl1")}
TRAINING_ENEMY_CLASSES[21] = {"Houndeye","monster_houndeye","",IsMounted("hl1")}
TRAINING_ENEMY_CLASSES[22] = {"Zombie (HL1)","monster_zombie","",IsMounted("hl1")}
TRAINING_ENEMY_CLASSES[23] = {"Soldier (HL1)","monster_human_grunt","",IsMounted("hl1")}
TRAINING_ENEMY_CLASSES[24] = {"Assassin (HL1)","monster_human_assassin","",IsMounted("hl1")}
TRAINING_ENEMY_CLASSES[25] = {"Sentry (HL1)","monster_sentry","",IsMounted("hl1")}

GLOB_BTTOUCH = {}

function TRAINING_EnemyInfiniteHealth_CL_LIB( len )

	GLOB_INFINITE_HEALTH = net.ReadBool()

end
net.Receive("TRAINING_EnemyInfiniteHealth_CL",TRAINING_EnemyInfiniteHealth_CL_LIB)

function TRAINING_SinEnemies_CL_LIB( len )

	TRAINING_SIN = net.ReadBool()

end
net.Receive("TRAINING_SinEnemies_CL",TRAINING_SinEnemies_CL_LIB)

local MAP_LOADING_HUB = false
local TRAINING_PRESS = false
local TRAINING_LOADING = false
local TRAINING_USED_ONCE = false

function HUB_GetEyeHit()

	local PL = LocalPlayer()
	local POS, ANG = (PL.VIEW_POSITION or PL:EyePos()), (AIM_BULLET_ANGLE or PL:EyeAngles())
	
	local TR = {}
	TR.start = POS
	TR.endpos = POS + ANG:Forward()*40
	TR.mask = MASK_PLAYERSOLID
	TR.filter = {PL}
	TR.mins = Vector(-0.5,-0.5,-0.5)
	TR.maxs = Vector(0.5,0.5,0.5)
	local Trace = util.TraceHull(TR)
	
	return Trace.HitPos

end

local MAP_LOADING_HUB = false
local TRAINING_PRESS = false
local TRAINING_LOADING = false
local TRAINING_USED_ONCE = false

function HUB_GetEyeHit()

	local PL = LocalPlayer()
	local POS, ANG = (PL.VIEW_POSITION or PL:EyePos()), (AIM_BULLET_ANGLE or PL:EyeAngles())
	
	local TR = {}
	TR.start = POS
	TR.endpos = POS + ANG:Forward()*40
	TR.mask = MASK_PLAYERSOLID
	TR.filter = {PL}
	TR.mins = Vector(-0.5,-0.5,-0.5)
	TR.maxs = Vector(0.5,0.5,0.5)
	local Trace = util.TraceHull(TR)
	
	return Trace.HitPos

end

timer.Create("Training_ESC_Fixer", 0.1, 0, function()

	if (gui.IsGameUIVisible() or gui.IsConsoleVisible() or ESCAPEMENU_ENABLED > 0 or TRAINING_TRAVEL_FIX) then
		
		if not TRAINING_FIXER_G then
			TRAINING_FIXER_G = true
			CL_FixTrainingTrash()
		end
		
	elseif not gui.IsGameUIVisible() and not gui.IsConsoleVisible() and not TRAINING_TRAVEL_FIX and ESCAPEMENU_ENABLED <= 0 then
	
		if TRAINING_FIXER_G then
			TRAINING_FIXER_G = false
			CL_RedoTrainingTrash()
		end
		
	end

end)

function Training_Buttons()

	local PL = LocalPlayer()
	local USE_DOWN = PL:KeyDown(IN_USE)
	
	if USE_DOWN and TRAINING_GetEyeHit():Distance( TRAINING_JANKER[1] ) <= 60 then
	
		if GAME_OVER_ENABLED then return end
		if SUIT_LOCKER_ENABLED then return end
		if PL.EasyBind_SettingUp then return end
		if PL.GameGuide_Open then return end
		if HUB_CAMPAIGN_STARTING then return end
		
		PL.TRAINING_MENU = true
		gui.EnableScreenClicker(true)
		NoDrawCursor(false)
	
	end
	
	--[[ TRAINING AREA ]]

	TRAINING_JANKER[4] = Angle(0,LocalPlayer():EyeAngles().y+180,0)
	local TRAINING_POS, TRAINING_ANGLE = TRAINING_JANKER[2], TRAINING_JANKER[4]
	local TRAINING_USE_POS = TRAINING_JANKER[3]
	TRAINING_ANGLE:RotateAroundAxis(TRAINING_ANGLE:Right(),-90)
	TRAINING_ANGLE:RotateAroundAxis(TRAINING_ANGLE:Up(),90)
	
	local TRAINING_LOOKING = HUB_GetEyeHit():Distance(TRAINING_USE_POS) <= 70
	
	cam.Start3D2D( TRAINING_POS, TRAINING_ANGLE, 0.05 )
	
		local TXT = "- Go Back Home -"
		local FNT = "TRAINING_Buttons"
		
		if MAP_LOADING_HUB then
			TXT = "- Travelling... -"
		elseif TRAINING_USED_ONCE then
			TXT = "- Are you Sure? -"
		end
		
		surface.SetFont( FNT )
		local tW, tH = surface.GetTextSize( TXT )
		local pad = 5
		
		surface.SetDrawColor( 10, 10, 10, 150 )
		surface.DrawRect( -tW / 2 - pad, -pad, tW + pad * 2, tH + pad * 2 )
		
		if TRAINING_LOOKING then
		
			local PULSE = math.sin(UnPredictedCurTime()*20)*45
			TRAINING_JANKER[5]:SetUnpacked( (45 - PULSE), (210 + PULSE), (45 - PULSE), 255 )
			draw.SimpleText( TXT, FNT, -tW/2, 0, TRAINING_JANKER[5] )
			
			if USE_DOWN and not TRAINING_PRESS then
			
				TRAINING_PRESS = true
				
				if not MAP_LOADING_HUB and TRAINING_LOOKING and not TRAINING_USED_ONCE then
				
					TRAINING_USED_ONCE = true
					PL:EmitSound( Sound("TemporalAssassin/UI_B/HUD_Click_G.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
					
				elseif not MAP_LOADING_HUB and TRAINING_LOOKING and TRAINING_USED_ONCE then
				
					MAP_LOADING_HUB = true
					
					CL_FixTrainingTrash()
					
					PL:EmitSound( Sound("TemporalAssassin/UI/Game_Load.wav"), 80, math.random(99,101), 0.8, CHAN_STATIC )
					
					timer.Simple(2,function()
						RunConsoleCommand("changelevel","ta_hub")
					end)
					
				end
			
			elseif not USE_DOWN and TRAINING_PRESS then
			
				TRAINING_PRESS = false
				
			end
			
		else
		
			TRAINING_JANKER[5]:SetUnpacked( 240, 240, 240, 255 )
			draw.SimpleText( TXT, FNT, -tW/2, 0, TRAINING_JANKER[5] )
			TRAINING_USED_ONCE = false
			
		end
		
	cam.End3D2D()

end
hook.Add("PostDrawOpaqueRenderables","Training_ButtonsHook",Training_Buttons)

local Training_GuideFunctions = {}

local COLOUR_WHITE = Color(240,240,240,255)
local COLOUR_GREEN = Color(100,255,100,255)
local COLOUR_RED = Color(255,100,100,255)

local Font_Table = {
	font 		= "Arial Black",
	size 		= GUI_X(14),
	weight 		= 300,
	antialias 	= true,
	additive 	= false,
	blursize	= 0,
	scanlines	= 0
}
surface.CreateFont( "TRAIN_Text", Font_Table )

local Font_Table = {
	font 		= "Arial Black",
	size 		= GUI_X(10),
	weight 		= 300,
	antialias 	= true,
	additive 	= false,
	blursize	= 0,
	scanlines	= 0
}
surface.CreateFont( "TRAIN_Text2", Font_Table )

Training_GuideFunctions[1] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(45),
	GUI_Y(60)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(20) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_ToggleVolatileBattery")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Volatile Battery", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	if ply.ARCBlade_PowerUp then
		draw.SimpleText( "Currently: ON", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	else
		draw.SimpleText( "Currently: OFF", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

Training_GuideFunctions[2] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(80),
	GUI_Y(95)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(20) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_ToggleShadeCloak")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Shade Cloak", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	if ply.ITEM_SHADECLOAK then
		draw.SimpleText( "Currently: ON", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	else
		draw.SimpleText( "Currently: OFF", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

Training_GuideFunctions[3] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(115),
	GUI_Y(130)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(20) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_EnemyInfiniteHealth")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Infinite Enemy Health", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	if GLOB_INFINITE_HEALTH then
		draw.SimpleText( "Currently: ON", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	else
		draw.SimpleText( "Currently: OFF", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

Training_GuideFunctions[4] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(150),
	GUI_Y(165)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(20) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_ToggleSinEnemies")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Sin Enemies", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	if TRAINING_SIN then
		draw.SimpleText( "Currently: ON", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	else
		draw.SimpleText( "Currently: OFF", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

Training_GuideFunctions[5] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(185)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(8) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_RefillMana")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Refill Temporal Mana", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

end

Training_GuideFunctions[6] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(205)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(8) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_RefillBurst")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Refill Burst Gauge", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

end

Training_GuideFunctions[7] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(225)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(8) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_RefillSpiritEye")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Refill Spirit Eye", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

end

Training_GuideFunctions[8] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(245)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(8) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_ResetPocketWatch")
			net.SendToServer()
		end
		
	end

	if ply:HasPocketWatch() then
		draw.SimpleText( "Reset Pocket Watch", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	else
		draw.SimpleText( "Reset ?????", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

Training_GuideFunctions[9] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(265)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(8) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_ResetSummons")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Reset Summons", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

end

Training_GuideFunctions[10] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(285)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(8) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_RaikiriSwap")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Swap Raikiri/ARC Blade", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

end

Training_GuideFunctions[11] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(305),
	GUI_Y(320)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(20) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_CosmicRealm")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Cosmic Realm", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	if ply.CosmicRealm_Counter and ply.CosmicRealm_Counter > 0 then
		draw.SimpleText( "Currently: ON", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	else
		draw.SimpleText( "Currently: OFF", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

Training_GuideFunctions[12] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(340),
	GUI_Y(355)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(20) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_FalanranAwaken")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Awakened Falanran", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	if ply.Falanran_SuperBuff then
		draw.SimpleText( "Currently: ON", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	else
		draw.SimpleText( "Currently: OFF", "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[3], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

Training_GuideFunctions[13] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(375)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(6), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(6) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			if GetConVarNumber("ai_disabled") >= 1 then
				RunConsoleCommand("ai_disabled",0)
			else
				RunConsoleCommand("ai_disabled",1)
			end
		end
		
	end

	draw.SimpleText( "Toggle AI", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

end

Training_GuideFunctions[16] = function(ply,mouseclick)

	if not ply:HasWeapon("temporal_reiterpallasch") then return end

	local POSITION_DATA = {
	GUI_X(100),
	GUI_Y(395)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(6), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(20) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
		
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("Training_ReiterBuff")
			net.SendToServer()
			
		end
		
	end

	local WEP = ply:GetWeapon("temporal_reiterpallasch")
	
	draw.SimpleText( "Set Reiterpallasch Buff", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	if WEP.SAVE_VAR1 <= 0 then
		draw.SimpleText( tostring("Currently: Disabled"), "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[2]+GUI_Y(12), COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	else
		draw.SimpleText( tostring("Currently: Enabled"), "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[2]+GUI_Y(12), COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	end

end

Training_GuideFunctions[14] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(430),
	GUI_Y(60)}
	
	local COLOUR_USE = COLOUR_WHITE
	
	if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] - GUI_Y(5), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(8) ) then
	
		COLOUR_USE = COLOUR_GREEN
		
		if mouseclick then
			ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
			net.Start("TRAINING_ChangeEnemyFormation")
			net.SendToServer()
		end
		
	end

	draw.SimpleText( "Change Enemy Formation", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

end

Training_GuideFunctions[15] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(430),
	GUI_Y(80)}
	
	local COLOUR_USE = COLOUR_WHITE

	draw.SimpleText( "- Change Enemy Type -", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	local Y_POS = 4
	
	for _,v in pairs (TRAINING_ENEMY_CLASSES) do
	
		local NAME = v[1]
		local CLASS = v[2]
		local WEAPON = v[3]
		local CAN_SPAWN = v[4]
		
		Y_POS = Y_POS + 9
		
		if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] + GUI_Y(Y_POS) - GUI_Y(4), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(Y_POS) + GUI_Y(4) ) and CAN_SPAWN then
		
			draw.SimpleText( NAME, "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[2] + GUI_Y(Y_POS), COLOUR_GREEN, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			if mouseclick then
				ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
				net.Start("TRAINING_SetTrainingEnemies")
				net.WriteString(CLASS)
				net.WriteString(WEAPON)
				net.SendToServer()
			end
			
		else
		
			if CAN_SPAWN then
				draw.SimpleText( NAME, "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[2] + GUI_Y(Y_POS), COLOUR_WHITE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				draw.SimpleText( NAME, "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[2] + GUI_Y(Y_POS), COLOUR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			end
			
		end
	
	end

end

Training_GuideFunctions[17] = function(ply,mouseclick)

	local POSITION_DATA = {
	GUI_X(270),
	GUI_Y(60)}
	
	local COLOUR_USE = COLOUR_WHITE

	draw.SimpleText( "- Spawn An Item -", "TRAIN_Text", POSITION_DATA[1], POSITION_DATA[2], COLOUR_USE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	
	local Y_POS = 4
	
	for _,v in pairs (TRAINING_POWERUPS) do
	
		local NAME = v[1]
		local CLASS = v[2]
		
		Y_POS = Y_POS + 9
		
		if MouseInside2( POSITION_DATA[1] - GUI_X(50), POSITION_DATA[2] + GUI_Y(Y_POS) - GUI_Y(4), POSITION_DATA[1] + GUI_X(50), POSITION_DATA[2] + GUI_Y(Y_POS) + GUI_Y(4) ) then
		
			draw.SimpleText( NAME, "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[2] + GUI_Y(Y_POS), COLOUR_GREEN, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			if mouseclick then
				ply:EmitSound("TemporalAssassin/UI/HUD_Click2.wav", 100, 100, 1, CHAN_STATIC)
				net.Start("Training_SpawnItem")
				net.WriteString(CLASS)
				net.SendToServer()
			end
			
		else
		
			draw.SimpleText( NAME, "TRAIN_Text2", POSITION_DATA[1], POSITION_DATA[2] + GUI_Y(Y_POS), COLOUR_WHITE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
		end
	
	end

end

function Training_GuideHUD()

	local PL = LocalPlayer()
	local CT = UnPredictedCurTime()
	
	local F3_PRESS = input.IsKeyDown(KEY_F3)
	
	if not F3_PRESS and KEY_F3_DOWN then
		KEY_F3_DOWN = false
	end
	
	if PL.GameGuide_Open then
	
		if F3_PRESS and not KEY_F3_DOWN then
			KEY_F3_DOWN = true
			PL.GameGuide_Open = false
			gui.EnableScreenClicker(false)
			NoDrawCursor(true)
		end
	
		local CLICK = MousePressed("GUIDE_CLICK")
		
		draw.RoundedBox( 2, GUI_X(30), GUI_Y(20), GUI_X(480), GUI_Y(440), TRAINING_JANKER[6] )
		
		draw.SimpleText( "Training Edit Menu", "OPTIONS_Extra", GUI_X(280), GUI_Y(32), TRAINING_JANKER[7], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
		for _,v in pairs (Training_GuideFunctions) do
		
			local FUNC_RUN = Training_GuideFunctions[_]
			FUNC_RUN(PL,CLICK)
		
		end
		
		if MouseInside2( GUI_X(280-40), GUI_Y(440-14), GUI_X(280+40), GUI_Y(440+14) ) then
		
			draw.SimpleText( "Close", "OPTIONS_Extra", GUI_X(280), GUI_Y(440), TRAINING_JANKER[8], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			if CLICK then
				KEY_F3_DOWN = true
				PL.GameGuide_Open = false
				gui.EnableScreenClicker(false)
				NoDrawCursor(true)
			end
			
		else
			draw.SimpleText( "Close", "OPTIONS_Extra", GUI_X(280), GUI_Y(440), TRAINING_JANKER[7], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		end
		
		F4MENU_DrawCursor()
	
	return end
	
	--[[OPTIONS_NormalTXT]]

	if F3_PRESS and not GAME_OVER_ENABLED and not PL.EasyBind_SettingUp and not PL.GameGuide_Open and not PL.TRAINING_MENU and not PL.HUB_CAMPAIGN_SELECT and not KEY_F3_DOWN and not SUIT_LOCKER_ENABLED then
		KEY_F3_DOWN = true
		PL.GameGuide_Open = true
		GUIDE_CURSOR = CT + 0.1
		gui.EnableScreenClicker(true)
		NoDrawCursor(false)
	end

end
hook.Add("HUDPaint","Training_GuideHUDHook",Training_GuideHUD)

function CL_FixTrainingTrash()

	hook.Remove("PostDrawOpaqueRenderables","Training_ButtonsHook")
	hook.Remove("HUDPaint","Training_HUDHook")
	timer.Remove("B_ParticleFX")
	RunConsoleCommand("FixTrainingTrash")

end

function CL_RedoTrainingTrash()

	if MAP_LOADING_HUB then return end

	hook.Add("PostDrawOpaqueRenderables","Training_ButtonsHook",Training_Buttons)
	hook.Add("HUDPaint","Training_HUDHook",Training_HUD)
	timer.Create("B_ParticleFX", 1, 0, function()
		ParticleEffect( "B_Idle", TRAINING_JANKER[9], TRAINING_JANKER[11] )
		ParticleEffect( "Training_Portal", TRAINING_JANKER[10], TRAINING_JANKER[11] )
	end)
	RunConsoleCommand("RedoTrainingTrash")

end