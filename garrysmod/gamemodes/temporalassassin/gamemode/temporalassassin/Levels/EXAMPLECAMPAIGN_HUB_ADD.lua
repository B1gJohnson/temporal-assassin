
--[[
If you don't know lua or understand how to read this kinda stuff, you'll get lost easily.

I will comment what i can here, but please don't ask for too much extra information.

The following is an example of what it would look like if you where to add the Half-Life 2 campaign to
the hub via a _addhub.lua
]]

--[[

--[[ This stuff sets up each chapters health, armour, ammo and loadout ]]
CHAPTERSELECT_LOADOUT_DATA["HL2_1"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_1"]["Health"] = 100

CHAPTERSELECT_LOADOUT_DATA["HL2_2"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_2"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_2"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_2"]["Ammo"] = {
{"fenic_ammo",100}}

CHAPTERSELECT_LOADOUT_DATA["HL2_3"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_3"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_3"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_3"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HL2_3"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_kingslayer",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["HL2_4"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_4"]["Health"] = 125
CHAPTERSELECT_LOADOUT_DATA["HL2_4"]["Armour"] = 125
CHAPTERSELECT_LOADOUT_DATA["HL2_4"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HL2_4"]["Weapons"] = {
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow"}

CHAPTERSELECT_LOADOUT_DATA["HL2_5"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_5"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_5"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_5"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HL2_5"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

CHAPTERSELECT_LOADOUT_DATA["HL2_6"] = {}
CHAPTERSELECT_LOADOUT_DATA["HL2_6"]["Health"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_6"]["Armour"] = 100
CHAPTERSELECT_LOADOUT_DATA["HL2_6"]["Ammo"] = {
{"fenic_ammo",200},
{"wolf_ammo",12},
{"cruentus_ammo",20},
{"foolsoath_ammo",8},
{"smig_ammo",180},
{"kingslayer_ammo",5},
{"crossbow_ammo",10},
{"longhorn_ammo",40},
{"frontliner_ammo",3},
{"egrenade_ammo",2}}
CHAPTERSELECT_LOADOUT_DATA["HL2_6"]["Weapons"] = {
"temporal_falanran",
"temporal_fenic",
"temporal_revolver",
"temporal_shotgun",
"temporal_supershotgun",
"temporal_foolsoath",
"temporal_smg",
"temporal_rifle",
"temporal_kingslayer",
"temporal_crossbow",
"temporal_frontliner"}

--[[ What our list of chapters should be, and what loadout data we should use, along with the maps they goto ]]
CHAPTERSELECT_DATA["Half Life 2"] = {
{"Campaign Start", "d1_trainstation_01", CHAPTERSELECT_LOADOUT_DATA["HL2_1"]},
{"Canals", "d1_canals_01", CHAPTERSELECT_LOADOUT_DATA["HL2_2"]},
{"Ravenholm", "d1_town_01", CHAPTERSELECT_LOADOUT_DATA["HL2_3"]},
{"Coast", "d2_coast_01", CHAPTERSELECT_LOADOUT_DATA["HL2_4"]},
{"Nova Prospekt", "d2_prison_01", CHAPTERSELECT_LOADOUT_DATA["HL2_5"]},
{"City 17", "d3_c17_01", CHAPTERSELECT_LOADOUT_DATA["HL2_6"]}}

AddCampaignMission( "Half Life 2", IsMounted("hl2"), "d1_trainstation_01", "A Contractor has tasked us with fixing a paradoxical event that would change history if gone unchecked, take your mantle and put yourself in his shoes and go through his hardships to fix it.", "Requires Half-Life 2 mounted to play", {"Agency/Mthuulra",Color(50,50,50,255)} )

]]