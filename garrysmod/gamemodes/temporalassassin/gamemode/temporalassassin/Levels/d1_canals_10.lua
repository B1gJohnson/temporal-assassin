
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_hurt")
	
	CreatePlayerSpawn( Vector(11870.126953125,-12179.435546875,-530.06646728516), Angle(0,120,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_KingSlayerAmmo( Vector(3511.0993652344, 9927.5302734375, -127.96875), Angle(0, 266.48004150391, 0) )
		Create_KingSlayerAmmo( Vector(3508.5422363281, 9884.634765625, -127.96875), Angle(0, 266.48004150391, 0) )
		Create_KingSlayerWeapon( Vector(3507.2180175781, 9841.890625, -127.96875), Angle(0, 265.60003662109, 0) )
		Create_Medkit25( Vector(4811.9775390625, 9563.2998046875, -77.393890380859), Angle(0, 179.50003051758, 0) )
		Create_Medkit25( Vector(4809.1088867188, 9610.5771484375, -77.393890380859), Angle(0, 179.06004333496, 0) )
		Create_Beer( Vector(4660.189453125, 9938.548828125, -77.393890380859), Angle(0, 282.32012939453, 0) )
		Create_Beer( Vector(4674.982421875, 9939.87890625, -77.393890380859), Angle(0, 272.42004394531, 0) )
		Create_Beer( Vector(4690.0522460938, 9939.0244140625, -77.393890380859), Angle(0, 256.57995605469, 0) )
		Create_ShotgunAmmo( Vector(4809.2373046875, 9394.2333984375, -111.96875), Angle(0, 89.740226745605, 0) )
		Create_ShotgunAmmo( Vector(4724.7485351563, 9394.615234375, -111.96875), Angle(0, 89.740226745605, 0) )
		Create_ShotgunAmmo( Vector(4766.1694335938, 9394.7216796875, -111.96875), Angle(0, 89.960227966309, 0) )

		CreateSecretArea( Vector(3520.4606933594,9866.59375,-125.69047546387), Vector(-54.328193664551,-54.328193664551,0), Vector(54.328193664551,54.328193664551,55.419845581055) )
		
	else
	
		Create_Medkit25( Vector(5449.314, -6877.795, -255.969), Angle(0, 155.369, 0) )
		Create_Medkit10( Vector(5421.841, -6847.469, -255.969), Angle(0, 208.455, 0) )
		Create_Beer( Vector(4806.122, 9397.338, -111.969), Angle(0, 100.111, 0) )
		Create_Beer( Vector(4754.483, 9396.521, -111.969), Angle(0, 99.484, 0) )
		Create_Beer( Vector(4704.467, 9396.945, -111.969), Angle(0, 64.163, 0) )
		Create_Bread( Vector(4822.661, 9865.277, -111.969), Angle(0, 198.861, 0) )
		Create_Bread( Vector(4806.722, 9831.182, -111.969), Angle(0, 164.167, 0) )
		Create_Armour( Vector(4688.974, 9901.333, -111.969), Angle(0, 259.889, 0) )
		Create_KingSlayerAmmo( Vector(4746.794, 9869.07, -111.969), Angle(0, 233.241, 0) )
		Create_KingSlayerWeapon( Vector(3547.289, 9876.188, -127.969), Angle(0, 175.403, 0) )
		Create_ShotgunAmmo( Vector(4837.423, 9513.354, -111.969), Angle(0, 174.359, 0) )
		Create_RevolverAmmo( Vector(4820.435, 9524.943, -111.969), Angle(0, 185.227, 0) )
		Create_Armour5( Vector(3950.189, 8138.889, -127.969), Angle(0, 41.017, 0) )
		Create_Armour5( Vector(3940.127, 8167.684, -127.969), Angle(0, 21.789, 0) )
		Create_Armour5( Vector(3982.749, 8136.07, -127.969), Angle(0, 59.618, 0) )
		Create_Armour5( Vector(3965.247, 8160.438, -127.969), Angle(0, 35.583, 0) )
	
		CreateSecretArea( Vector(3547.878,9870.766,-123.437), Vector(-30.526,-30.526,0), Vector(30.526,30.526,19.502) )
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)