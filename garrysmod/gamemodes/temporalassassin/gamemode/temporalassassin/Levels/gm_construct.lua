		
if CLIENT then return end

--[[ This is code for a respawning supply box. - Magenta ]]
--[[ RSB = Respawning Supply Box ]]
local SUPPLY_BOX_RESPAWNS = {}
local SUPPLY_BOX_SPAWN_NUMBER = 0

--[[ How long in seconds to wait until the box respawns. - Magenta ]]
local RSB_TIME_TO_WAIT = 30

--[[ Creates an RSB with the given parameters. - Magenta ]]
function CreateRespawningSupplyBox( pos, ang, weight )

	if not pos then return end
	if not ang then ang = Angle(0,0,0) end
	if not weight then weight = false end
	
	local BOX = CreateSupplyBox( pos, ang, weight )
	
	table.insert(SUPPLY_BOX_RESPAWNS, {BOX,pos,ang,weight})
	
end

--[[ Pre Cleanup so the boxes don't get duped or break when the player Fast Restarts. - Magenta ]]
function RSB_PreCleanupMap()

	SUPPLY_BOX_RESPAWNS = {}

	for loop = 1,SUPPLY_BOX_SPAWN_NUMBER do
		timer.Remove( tostring("SUPPLY_BOX_RESPAWN_"..loop) )
	end
	
	SUPPLY_BOX_SPAWN_NUMBER = 0

end
hook.Add("PreCleanupMap","RSB_PreCleanupMapHook",RSB_PreCleanupMap)

--[[ Entity remove hook to check for when a box gets broken. - Magenta ]]
function RSB_EntityRemoved(ENT)

	--[[ Lets cycle through all of our supply box respawns so we can get where this came from ]]
	for _,v in pairs (SUPPLY_BOX_RESPAWNS) do
	
		--[[ If the box that was broken is the same box in the table, create another box after X seconds and replace the table data ]]
		if v and v[1] and v[1] == ENT then
		
			SUPPLY_BOX_SPAWN_NUMBER = SUPPLY_BOX_SPAWN_NUMBER + 1
			
			timer.Create( tostring("SUPPLY_BOX_RESPAWN_"..SUPPLY_BOX_SPAWN_NUMBER), RSB_TIME_TO_WAIT, 1, function()
				CreatePointParticle( "Doll_EvilSpawn_Activate", SUPPLY_BOX_RESPAWNS[_][2], SUPPLY_BOX_RESPAWNS[_][2], Angle(0,0,0), Angle(0,0,0), 1 )
				sound.Play( Sound("TemporalAssassin/Doll/ExtraSpawn_Item.wav"), SUPPLY_BOX_RESPAWNS[_][2], 70, math.random(99,101), 0.7 )
				SUPPLY_BOX_RESPAWNS[_][1] = CreateSupplyBox( SUPPLY_BOX_RESPAWNS[_][2], SUPPLY_BOX_RESPAWNS[_][3], SUPPLY_BOX_RESPAWNS[_][4] )
			end)
		
		end
	
	end

end
hook.Add("EntityRemoved","RSB_EntityRemovedHook",RSB_EntityRemoved)

function Create_Light( pos, style, color, size, parent )

	local LIGHT_ENT = ents.Create("light_dynamic")
	
	LIGHT_ENT:SetPos( pos )
	
	LIGHT_ENT:SetKeyValue("style",style)
	LIGHT_ENT:SetKeyValue("pitch",270)
	LIGHT_ENT:SetKeyValue("brightness",1)
	LIGHT_ENT:SetKeyValue("spotlight_radius",size*1.1)
	LIGHT_ENT:SetKeyValue("_light",tostring(color[1].." "..color[2].." "..color[3].." "..color[4]))
	LIGHT_ENT:SetKeyValue("distance",size*1.1)
	LIGHT_ENT:SetKeyValue("_cone",360)
	LIGHT_ENT:SetKeyValue("_inner_cone",0)
	LIGHT_ENT:SetKeyValue("spawnflags",0)
	
	LIGHT_ENT:Spawn()
	LIGHT_ENT:Activate()
	
	if parent then
		LIGHT_ENT:SetParent(parent)
	end
	
	LIGHT_ENT:Fire("TurnOn",nil,0)
	
	return LIGHT_ENT

end

local AMB_GENERICS = 0

function Create_AmbientGeneric( pos, sound, volume, pitch, fadein, fadeout, radius )

	local AMBIENT_GENERIC = ents.Create("ambient_generic")
	
	AMBIENT_GENERIC:SetPos( pos )
	
	AMBIENT_GENERIC:SetKeyValue("message",sound)
	AMBIENT_GENERIC:SetKeyValue("health",volume)
	AMBIENT_GENERIC:SetKeyValue("spawnflags",0)
	AMBIENT_GENERIC:SetKeyValue("pitch",pitch)
	AMBIENT_GENERIC:SetKeyValue("pitchstart",pitch)
	AMBIENT_GENERIC:SetKeyValue("fadeinsecs",fadein)
	AMBIENT_GENERIC:SetKeyValue("fadeoutsecs",fadeout)
	AMBIENT_GENERIC:SetKeyValue("radius",radius)
	
	AMBIENT_GENERIC:Spawn()
	AMBIENT_GENERIC:Activate()
	
	AMBIENT_GENERIC.Custom = true
	
	AMBIENT_GENERIC:Fire("PlaySound",nil,0)
	
	return AMBIENT_GENERIC

end

function DISCO_TOGGLE()

	local POS = Vector(-4264.011,-1807.974,-37.195)

	if not DISCO_ENABLED then
	
		DISCO_ENABLED = true
		
		GM_CON_L1 = Create_Light( POS + VectorRand()*100, 2, {0,255,255,200}, 500 )
		GM_CON_L2 = Create_Light( POS + VectorRand()*100, 11, {255,0,255,200}, 500 )
		GM_CON_L3 = Create_Light( POS + VectorRand()*100, 5, {0,0,255,200}, 500 )
		GM_CON_L4 = Create_Light( POS + VectorRand()*100, 8, {255,0,0,200}, 500 )
	
		GM_CON_SND = Create_AmbientGeneric( POS, "temporalassassin/misc/AA_EE_OO_Loop.wav", 80, 100, 1, 1, 8000 )
		
		local RAT_POS1 = Vector(-4309.546,-1710.283,-73.969)
		local RAT_POS2 = Vector(-4315.707,-1842.454,-73.969)
		local RAT_POS3 = Vector(-4206.804,-1919.073,-73.969)
		local RAT_POS4 = Vector(-4082.064,-1887.718,-73.969)
		local RAT_POS5 = Vector(-4039.361,-1745.631,-73.969)
		local RAT_POS6 = Vector(-4096.614,-1625.264,-73.969)
		
		GM_RAT1 = ents.Create("prop_ragdoll")
		GM_RAT1:SetModel("models/TemporalAssassin/Gore/NPC/headcrabclassic_rat.mdl")
		GM_RAT1:SetPos(RAT_POS1)
		GM_RAT1:Spawn()
		GM_RAT1:Activate()
		
		GM_RAT2 = ents.Create("prop_ragdoll")
		GM_RAT2:SetModel("models/TemporalAssassin/Gore/NPC/headcrabclassic_rat.mdl")
		GM_RAT2:SetPos(RAT_POS2)
		GM_RAT2:Spawn()
		GM_RAT2:Activate()
		
		GM_RAT3 = ents.Create("prop_ragdoll")
		GM_RAT3:SetModel("models/TemporalAssassin/Gore/NPC/headcrabclassic_rat.mdl")
		GM_RAT3:SetPos(RAT_POS3)
		GM_RAT3:Spawn()
		GM_RAT3:Activate()
		
		GM_RAT4 = ents.Create("prop_ragdoll")
		GM_RAT4:SetModel("models/TemporalAssassin/Gore/NPC/headcrabclassic_rat.mdl")
		GM_RAT4:SetPos(RAT_POS4)
		GM_RAT4:Spawn()
		GM_RAT4:Activate()
		
		GM_RAT5 = ents.Create("prop_ragdoll")
		GM_RAT5:SetModel("models/TemporalAssassin/Gore/NPC/headcrabclassic_rat.mdl")
		GM_RAT5:SetPos(RAT_POS5)
		GM_RAT5:Spawn()
		GM_RAT5:Activate()
		
		GM_RAT6 = ents.Create("prop_ragdoll")
		GM_RAT6:SetModel("models/TemporalAssassin/Gore/NPC/headcrabclassic_rat.mdl")
		GM_RAT6:SetPos(RAT_POS6)
		GM_RAT6:Spawn()
		GM_RAT6:Activate()
		
		timer.Create("DANCE_BAY_BEE", 0.02, 0, function()
		
			if not TEMPORAL_SHIFT_ON then
				GM_RAT1:ApplyPhysicalForce( (GM_RAT1:GetPos() - RAT_POS1):GetNormal()*-300, VectorRand()*3000 )
				GM_RAT2:ApplyPhysicalForce( (GM_RAT2:GetPos() - RAT_POS2):GetNormal()*-300, VectorRand()*3000 )
				GM_RAT3:ApplyPhysicalForce( (GM_RAT3:GetPos() - RAT_POS3):GetNormal()*-300, VectorRand()*3000 )
				GM_RAT4:ApplyPhysicalForce( (GM_RAT4:GetPos() - RAT_POS4):GetNormal()*-300, VectorRand()*3000 )
				GM_RAT5:ApplyPhysicalForce( (GM_RAT5:GetPos() - RAT_POS5):GetNormal()*-300, VectorRand()*3000 )
				GM_RAT6:ApplyPhysicalForce( (GM_RAT6:GetPos() - RAT_POS6):GetNormal()*-300, VectorRand()*3000 )
			end
		
		end)
		
	else
	
		DISCO_ENABLED = false
		
		if GM_CON_L1 and IsValid(GM_CON_L1) then
			GM_CON_L1:Remove()
		end
		if GM_CON_L2 and IsValid(GM_CON_L2) then
			GM_CON_L2:Remove()
		end
		if GM_CON_L3 and IsValid(GM_CON_L3) then
			GM_CON_L3:Remove()
		end
		if GM_CON_L4 and IsValid(GM_CON_L4) then
			GM_CON_L4:Remove()
		end
		
		if GM_CON_SND then
			GM_CON_SND:Fire("StopSound")
			GM_CON_SND:Remove()
		end
		
		timer.Destroy("DANCE_BAY_BEE")
		
		if GM_RAT1 and IsValid(GM_RAT1) then
			GM_RAT1:Remove()
		end
		if GM_RAT2 and IsValid(GM_RAT2) then
			GM_RAT2:Remove()
		end
		if GM_RAT3 and IsValid(GM_RAT3) then
			GM_RAT3:Remove()
		end
		if GM_RAT4 and IsValid(GM_RAT4) then
			GM_RAT4:Remove()
		end
		if GM_RAT5 and IsValid(GM_RAT5) then
			GM_RAT5:Remove()
		end
		if GM_RAT6 and IsValid(GM_RAT6) then
			GM_RAT6:Remove()
		end
		
	end

end

function MAP_LOADED_FUNC()

	CreateEquipmentLocker( Vector(1691.145,-1744.176,-143.969), Angle(0,180.576,0) )
	CreateClothesLocker( Vector(1718.719,-1481.077,-143.969), Angle(0,177.203,0) )
	
	Create_InfiniteGrenades( Vector(855.337, -1455.191, -143.969), Angle(0, 246.504, 0) )
	Create_CorzoHat( Vector(810.297, -1236.9, -143.969), Angle(0, 330.104, 0) )
	Create_CorzoHat( Vector(798.928, -1338.966, -143.969), Angle(0, 358.814, 0) )
	Create_ManaCrystal100( Vector(920.628, -1135.907, -143.969), Angle(0, 317.124, 0) )
	Create_ManaCrystal100( Vector(1003.516, -1128.155, -143.969), Angle(0, 294.354, 0) )
	Create_ManaCrystal100( Vector(1088.213, -1132.118, -143.969), Angle(0, 262.563, 0) )
	Create_VolatileBattery( Vector(1686.64, -1824.926, -143.969), Angle(0, 163.893, 0) )
	Create_VolatileBattery( Vector(1685.358, -1667.269, -143.969), Angle(0, 198.103, 0) )
	
	Create_ShadeCloak( Vector(1537.735, -1598.092, 1136.031), Angle(0, 179.333, 0) )
	
	CreateSecretArea( Vector(1476.478,-1597.214,1136.031), Vector(-260.768,-260.768,0), Vector(260.768,260.768,113.794), false, "TemporalAssassin/Secrets/Secret_HollowKnight.wav" )
	
	CreateCosmicRealm( Vector(-2363.184,-2778.391,256.031), Vector(-472.567,-472.567,0), Vector(472.567,472.567,499.322) )
	CreateCosmicRealm( Vector(-2626.364,-2788.691,256.031), Vector(-270.554,-270.554,0), Vector(270.554,270.554,471.858) )
	CreateCosmicRealm( Vector(-1985.453,-2786.343,256.031), Vector(-281.42,-281.42,0), Vector(281.42,281.42,478.203) )
	CreateCosmicRealm( Vector(-1966.575,-2651.694,256.031), Vector(-314.615,-314.615,0), Vector(314.615,314.615,306.438) )
	CreateCosmicRealm( Vector(-2626.668,-2624.451,256.031), Vector(-273.619,-273.619,0), Vector(273.619,273.619,459.412) )
	CreateCosmicRealm( Vector(-2638.749,-2961.554,256.031), Vector(-261.841,-261.841,0), Vector(261.841,261.841,471.252) )
	CreateCosmicRealm( Vector(-1953.769,-2978.114,256.031), Vector(-248.172,-248.172,0), Vector(248.172,248.172,464.188) )
	
	CreateCosmicRealmEntrance( Vector(-1668.218,-2791.594,256.031), Angle(0,0,0), "DOOR_LEFT" )
	CreateCosmicRealmEntrance( Vector(-2913.472,-2805.631,256.031), Angle(0,0,0), "DOOR_RIGHT" )
	CreateCosmicRealmEntrance( Vector(-2304.159,-2318.844,256.031), Angle(0,90,0), "DOOR_FRONT" )
	
	CreateTrigger( Vector(-5223.899,-1076.968,-143.969), Vector(-23.807,-23.807,0), Vector(23.807,23.807,99.545), DISCO_TOGGLE, true )
	
	CreateRestingPoint( Vector(-5461.667,-2438.607,-143.969), Angle(0,357.983,0) )
	
	local B_PROP = CreateProp(Vector(-1697.969,-2790.187,2481.594), Angle(0.906,92.049,0.027), "models/props_lab/binderblue.mdl", true)
	
	local B_OFF = function(ply)
		ply:SetHealth(ply:GetMaxHealth()*3)
	end
	local B_ON = function(ply)
		ply:SetHealth(1)
	end
	
	AddNormalButton( B_PROP, B_ON, B_OFF, "You found my gm_construct button test. The button is ON", "You found my gm_construct button test. The button is OFF", 0.5, true, false )
	
	local SUPPLY1_PROP = CreateProp(Vector(724.188,-1996.125,-90.437), Angle(-0.016,0,0), "models/props_combine/breenclock.mdl", true)
	local SUPPLY2_PROP = CreateProp(Vector(724,-1930.062,-89.687), Angle(0.005,-90,2.34), "models/props_c17/briefcase001a.mdl", true)
	local SUPPLY3_PROP = CreateProp(Vector(720.469,-1864.219,-91.156), Angle(87.973,3.543,3.917), "models/temporalassassin/items/weapon_ebonyivory.mdl", true)
	
	--[[
		CreateSupplyBox( pos, ang, weighttable, frozen )
		
		pos = The position the box is created at, you can use CLGetObject on a spawned supply box model for this.
		ang = The angle the box is created at, you can use CLGetObject on a spawned supply box model for this.
		weighttable = If provided and not set to false, spawns the item types/items listed in the weighttable, the passable weights are "Ammo" "Health" and "Armour, you can also pass a raw creation function as shown in an example inside SUPPLY3_SPAWN.
		frozen = If set to true, the supply box will be frozen in place and be unable to be picked up and moved.
	]]
	
	local SUPPLY1_SPAWN = function(ply)
		CreateSupplyBox( Vector(986.132,-1929.912,-143.969), Angle(0,0,0) )
	end
	
	local SUPPLY2_SPAWN = function(ply)
		CreateSupplyBox( Vector(986.132,-1929.912,-143.969), Angle(0,0,0), {"Health","Health","Health","Armour","Armour"} )
	end
	
	local RANDOM_WEAPON = {
	"Create_FalanranWeapon",
	"Create_PistolWeapon",
	"Create_RevolverWeapon",
	"Create_M6GWeapon",
	"Create_ShotgunWeapon",
	"Create_SuperShotgunWeapon",
	"Create_FoolsOathWeapon",
	"Create_SMGWeapon",
	"Create_RifleWeapon",
	"Create_HarbingerWeapon",
	"Create_KingSlayerWeapon",
	"Create_CrossbowWeapon",
	"Create_FrontlinerWeapon"}
	
	local SUPPLY3_SPAWN = function(ply)
		CreateSupplyBox( Vector(986.132,-1929.912,-143.969), Angle(0,0,0), {table.Random(RANDOM_WEAPON)} )
	end
	
	AddNormalButton( SUPPLY1_PROP, SUPPLY1_SPAWN, SUPPLY1_SPAWN, "Spawning a Supply Box (Completely Random)", "Spawning a Supply Box (Completely Random)", 0.5, true, false )
	AddNormalButton( SUPPLY2_PROP, SUPPLY2_SPAWN, SUPPLY2_SPAWN, "Spawning a Supply Box (Only HP/Armour)", "Spawning a Supply Box (Only HP/Armour)", 0.5, true, false )
	AddNormalButton( SUPPLY3_PROP, SUPPLY3_SPAWN, SUPPLY3_SPAWN, "Spawning a Supply Box (Random Weapon)", "Spawning a Supply Box (Random Weapon)", 0.5, true, false )
	
	--[[
		SetupLantern( pos, ang, id, name, warp_pos, start_active, triggerfunc, noreward, persistent, foreverpersistent )
		
		pos = The position the lantern is placed at
		ang = The angle the lantern is placed at
		"id" = The unique ID the lantern has, this should be unique for all lanterns in your level, nothing should have the same ID name
		"name" = The string name that shows up in the Lantern Menu
		warp_pos = The position Kit will warp to when teleporting to this lantern
		start_active = If set to true, this lantern will start turned on, If false the player will have to snap it on first
		triggerfunc = A function to run when this lantern is snapped on, can be used for traps or progression/dialogue stuff, this works like a trigger so function(lantern,ply)
		noreward = If set to true, the player will not refresh to 100% HP and get their stamina/dodges/tmp back
		persistent = If set to true, this lantern will remain on after snapping it on even if the player restarts the level via Quick Restart after dying
		foreverpersistent = If set to true, this lantern will remain on after snapping it on forever, even if the player quits the map, or even the whole game
	]]
		
	SetupLantern( Vector(1575.276,-1961.27,-143.969), Angle(0,48.825,0), "EQUIP_ROOM", "Equipment Room", Vector(1531.195,-1923.022,-143.969), false, false, false, false, true )
	SetupLantern( Vector(561.57,4318.717,-31.969), Angle(0,245.475,0), "WATER_SIDE", "Waterside Spot", Vector(628.718,4316.469,-31.976), false, false, false, false, true )
	SetupLantern( Vector(-2434.086,-2263.701,256.031), Angle(0,25.983,0), "COSMIC_AREA", "Cosmic Realm Room", Vector(-2458.648,-2215.05,256.031), false, false, false, false, true )
	SetupLantern( Vector(-3292.91,-1496.897,-143.969), Angle(0,102.765,0), "DARK_ROOM", "Dark Room", Vector(-3348.85,-1497.609,-143.969), false, false, false, false, true )
	
	--[[
		MESSAGE:AddMessage( id, text, particletype, pos, ang, color1, color2 )
		
		id = The unique ID for this message, creating another message using this ID will remove the previous one with the same ID
		text = The text that will be visible when reading from this message in-game
		particletype = 1, 2 or 3 for different writing styles of what is visible on the ground, 4, 5 or 6 for wall messages, use CLGetEyeHit and CLGetEyeHitNormalAngle
		pos = The position of the Message, change the Z vector to help it stop clipping into the floor when used on displacements/ramps, use CLGetPos to get the position where you are standing
		ang = The angle that the Message should be facing, You simply look in the direction a reader would read the message from, use CLGetAngles and pass that angle as the ang argument for floor messages, CLGetEyeHitNormalAngle for wall messages
		color1 = The color of the solid text EG: Color(255,0,0) for red, Color(100,0,0) for dark red
		color2 = The color of the ghost/translucent fading text EG: Color(255,0,0) for red, Color(100,0,0) for dark red
		
		You can remove a message via MESSAGE:RemoveMessage(ID)
	]]
	
	--[[
		MESSAGE:AddFunction( id, func )
		
		id = The unique ID of the message that when read, runs the function
		func = The function to run when the player opens and reads the message
		
		Example that will set the players health to 300%:
		
		local FUNC = function(ply)
			ply:SetHealth(ply:GetMaxHealth()*3)
		end
		MESSAGE:AddFunction( "DEV_MSG1", FUNC )
	]]
		
	MESSAGE:AddMessage( "DEV_MSG1", "Good job you found me. Go stab things yes. - Magenta", 2, Vector(-2785.887,-2275.571,2048.02), Angle(0,-48.89,0), Color(255,0,255), Color(150,0,255) )
	MESSAGE:AddMessage( "DEV_MSGWALL2", "Touch Fire. Become funni power gremlin.", 4, Vector(-5639.969,-3246.574,308.643), Angle(0,0,0), Color(255,150,0), Color(255,255,0) )
	MESSAGE:AddMessage( "DEV_MSG3", "[You touch the Grass] You feel Disgusting.", 2, Vector(-4354.052,4428.992,-105.308), Angle(0,-58.943,0), Color(0,100,0), Color(0,50,0) )
	MESSAGE:AddMessage( "DEV_MSGWALL4", "[You touch the Stone] Smoother than the frame-rate.", 6, Vector(-4975.383,4812.031,-41.522), Angle(0,90,0), Color(0,0,150), Color(0,255,255) )
	MESSAGE:AddMessage( "DEV_MSGWALL5", "If my game ever gets popular, the first instance I hear ''The Dark Souls of First Person Shooters'' I am going to shoot myself.", 5, Vector(-3103.969,-1233.666,-45.237), Angle(0,0,0), Color(100,0,0), Color(255,0,0) )
	
	MESSAGE:AddMessage( "DEV_SUPPLYBOX1", "This Button will spawn a Supply Box containing randomized items that the game detects that you are ''Low on'', mostly ammo related. - Magenta", 1, Vector(826.816,-1996.985,-143.969), Angle(0,0,0), Color(255,255,0), Color(255,100,0) )
	MESSAGE:AddMessage( "DEV_SUPPLYBOX2", "This Button will spawn a Supply Box containing only Health and Armour. - Magenta", 1, Vector(842.776,-1930.051,-143.969), Angle(0,0,0), Color(0,255,255), Color(0,100,200) )
	MESSAGE:AddMessage( "DEV_SUPPLYBOX3", "This Button will spawn a Supply Box containing a random weapon. - Magenta", 1, Vector(826.816,-1863.985,-143.969), Angle(0,0,0), Color(255,0,255), Color(100,0,200) )
	
	--[[ Soup Secret ]]	
	local SUPERFLYS = {
	CreateProp(Vector(-4567.937,5406.75,2496.219), Angle(-0.016,-87.561,0.297), "models/temporalassassin/props/superfly_uppies.mdl", true),
	CreateProp(Vector(-4547.812,5425.844,2496.281), Angle(-0.308,-178.802,0.862), "models/temporalassassin/props/superfly_uppies.mdl", true),
	CreateProp(Vector(-4533.531,5406.75,2496.281), Angle(-0.038,89.011,-0.428), "models/temporalassassin/props/superfly_uppies.mdl", true),
	CreateProp(Vector(-4550.719,5388.281,2496.25), Angle(0.055,0.522,-0.357), "models/temporalassassin/props/superfly_uppies.mdl", true)}
	
	for _,v in pairs (SUPERFLYS) do
		if v and IsValid(v) then
			v:SetCollisionGroup(COLLISION_GROUP_WEAPON)
		end
	end
	
	local FUNC = function( self, ply )
	
		if IsValid(ply) then

			if not ply:Alive() then return end
			
			SUPERFLY_LOCKON = ply
			
			for _,v in pairs (SUPERFLYS) do
			
				if v and IsValid(v) then
				
					v:SetModel("models/temporalassassin/props/superfly_stand.mdl")
					
					timer.Create(tostring("SuperflyLockon_".._),0,0,function()
					
						if v and IsValid(v) and SUPERFLY_LOCKON and IsValid(SUPERFLY_LOCKON) then
							local ANG_P = (v:GetPos() - SUPERFLY_LOCKON:GetPos()):GetNormal()
							v:SetAngles( Angle(0,ANG_P:Angle().y+90,0) )
						else
							timer.Remove("SuperflyLockon_".._)
						end
					
					end)
					
				end
			end
			
			ply.Gift_Charge = 0
			ply.Gift_Used = false
			ply:SetSharedBool("Gift_Used",false)
			ply:Give("temporal_hergift")
			ply:SetSharedFloat("Item_ScreenFlash",CurTime() + 0.15)
			net.Start("TEMPORAL_AddLog")
			net.WriteString("HERE YOU GO BIG SIS!")
			net.Send(ply)
			ply:EmitSound( Sound("TemporalAssassin/Weapons/HerGift/Gift_Pickup.wav"), 100, math.random(98,102), 1, CHAN_BODY )
			hook.Call("TA_ItemPickup",false,ply,self)
			self:Remove()
			
		end
	
	end
	
	return CreateItemPickup( Vector(-4551.236,5409.221,2521.656), Angle(0,0,0), "models/temporalassassin/items/hergift.mdl", FUNC )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)