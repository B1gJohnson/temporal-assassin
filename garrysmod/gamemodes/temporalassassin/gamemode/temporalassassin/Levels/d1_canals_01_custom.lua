
if CLIENT then return end

--[[ To make absolutely sure we know this is the custom version of the map
Remove all the items and place a stupid boat door at spawn. ]]

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")
	
	CreatePlayerSpawn( Vector(904,-8291,0.1), Angle(0,180,0) )
	
	CreateProp(Vector(637.188,-7522.281,-75.656), Angle(0,85.325,-0.005), "models/props_borealis/borealis_door001a.mdl", true)
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)