		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["16_uvalno_undertower"] = {"17_uvalno_kopec", Vector(-8925.272,922.293,64.031), Vector(-99.556,-99.556,0), Vector(99.556,99.556,111.938)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-3305.775,-2316.097,109.57), Angle(0,-179.232,0))
	
	Create_SMGWeapon( Vector(-4768.694, -2782.354, 167.379), Angle(0, 350.384, 0) )
	Create_RifleWeapon( Vector(-4771.76, -1764.57, 165.036), Angle(9.068, -97.713, 178.21) )
	Create_RifleAmmo( Vector(-4763.551, -1787.757, 160.003), Angle(1.019, -72.648, 0.976) )
	Create_Bread( Vector(-5652.119, 48.178, 140.058), Angle(0, 1.313, 0) )
	Create_Bread( Vector(-5654.109, 26.125, 140.488), Angle(0, 38.801, 0) )
	Create_RevolverWeapon( Vector(-5474.123, 21.999, 154.96), Angle(5.379, -85.003, 0.467) )
	Create_SMGWeapon( Vector(-5535.21, 568.051, 87.267), Angle(2.107, 43.783, 4.753) )
	Create_Medkit10( Vector(-5502.823, 652.856, 84.716), Angle(-78.651, 24.277, -50.818) )
	Create_SMGWeapon( Vector(-6969.224, -933.007, 175.163), Angle(14.583, 15.481, 156.852) )
	Create_SMGAmmo( Vector(-6986.759, -933.627, 175.354), Angle(-11.459, -53.894, 166.874) )
	Create_Medkit25( Vector(-7680.959, -72.38, 208.716), Angle(0, 271.889, 0) )
	Create_Armour200( Vector(-8343.238, 170.561, 233.273), Angle(0, 80.225, 0) )
	Create_CrossbowWeapon( Vector(-8261.198, 163.917, 234.311), Angle(0, 124.049, 0) )
	Create_CrossbowAmmo( Vector(-8390.831, 171.561, 249.667), Angle(0, 73.361, 0) )
	Create_CrossbowAmmo( Vector(-8391.437, 171.099, 251.989), Angle(7.835, 127.877, 7.867) )
	Create_Medkit25( Vector(-8476.099, 167.454, 248.807), Angle(0, 75.825, 0) )

	CreateSecretArea( Vector(-8289.647,264.82,192.031), Vector(-86.696,-86.696,0), Vector(86.696,86.696,118.788), "A SECRET... WORKSHOP? HAS BEEN FOUND. (Password: Kde Domov Muj)" )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)