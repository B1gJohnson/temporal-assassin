
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("trigger_hurt")
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_SMGWeapon( Vector(3582.1088867188, 5485.7294921875, -319.96875), Angle(0, 181.79287719727, 0) )
		Create_SMGAmmo( Vector(3551.33203125, 5500.4326171875, -319.96875), Angle(0, 183.9928894043, 0) )
		Create_SMGAmmo( Vector(3552.5991210938, 5474.9658203125, -319.96875), Angle(0, 167.9328918457, 0) )
		Create_Armour100( Vector(4676.0986328125, 5256.5112304688, -319.96875), Angle(0, 179.3729095459, 0) )
		Create_SuperShotgunWeapon( Vector(3823.8784179688, 4059.4858398438, -255.96875), Angle(0, 304.33227539063, 0) )
		Create_ShotgunAmmo( Vector(3810.8620605469, 4112.3779296875, -255.96875), Angle(0, 292.89224243164, 0) )
		Create_ShotgunAmmo( Vector(3835.6335449219, 4119.9614257813, -255.96875), Angle(0, 277.4921875, 0) )
		Create_SMGAmmo( Vector(4296.451171875, 3115.9653320313, -434.96875), Angle(0, 182.95649719238, 0) )
		Create_SMGAmmo( Vector(4294.2875976563, 3101.6970214844, -434.96875), Angle(0, 180.75648498535, 0) )
		Create_SMGAmmo( Vector(4294.1416015625, 3086.1484375, -434.96875), Angle(0, 156.55648803711, 0) )
		Create_SMGAmmo( Vector(4295.1416015625, 3107.4897460938, -471.96875), Angle(0, 186.03646850586, 0) )
		Create_SMGAmmo( Vector(4292.1342773438, 3086.2097167969, -471.96875), Angle(0, 157.87646484375, 0) )
		Create_Armour( Vector(7278.4262695313, 1683.9000244141, -415.96875), Angle(0, 236.2575378418, 0) )
		Create_Medkit25( Vector(7247.4018554688, 1680.8133544922, -415.96875), Angle(0, 257.15753173828, 0) )
		Create_Medkit10( Vector(7139.5151367188, 1638.5948486328, -447.96875), Angle(0, 317.43756103516, 0) )
		Create_Medkit10( Vector(7155.7763671875, 1653.2481689453, -447.96875), Angle(0, 300.49743652344, 0) )
		Create_ShotgunAmmo( Vector(16.684844970703, -972.57073974609, -349.96875), Angle(0, 8.3372039794922, 0) )
		Create_Beer( Vector(16.057594299316, -1007.8106079102, -349.96875), Angle(0, 30.777328491211, 0) )
		Create_Beer( Vector(14.822113037109, -1031.134765625, -349.96875), Angle(0, 41.337387084961, 0) )
		Create_MegaSphere( Vector(7124.1196289063, -1527.9730224609, -428.57086181641), Angle(0, 189.53759765625, 0) )
		Create_EldritchArmour( Vector(1929.0684814453, -5045.9140625, -388.65344238281), Angle(0, 175.59503173828, 0) )

		CreateSecretArea( Vector(7125.525390625,-1526.6229248047,-428.4130859375), Vector(-53.535621643066,-53.535621643066,0), Vector(53.535621643066,53.535621643066,99.523498535156) )
		CreateSecretArea( Vector(1915.8520507813,-5038.81640625,-384.09375), Vector(-48.493583679199,-48.493583679199,0), Vector(48.493583679199,48.493583679199,42.354461669922) )
		
	else
	
		CreateProp(Vector(5150.813,280.875,-481.219), Angle(-0.044,90,0), "models/temporalassassin/doll/unknown.mdl", true)
		CreateProp(Vector(7412.531,1384.313,13.25), Angle(71.455,168.354,162.729), "models/temporalassassin/doll/unknown.mdl", true)
		
		if IsMounted("ep2") then
			CreateProp(Vector(7561.188,1456.063,5.156), Angle(60.249,112.368,-69.565), "models/props_junk/gnome.mdl", true)
		end
		
		local HINT_1 = CreateProp(Vector(3225.625,7650.719,-303.062), Angle(3.384,87.803,-179.78), "models/props_trainstation/tracksign03.mdl", true)
		
		Create_EldritchArmour( Vector(3262.175, 7502.926, 288.031), Angle(0, 185.388, 0) )
		Create_EldritchArmour( Vector(3205.894, 7504.147, 288.031), Angle(0, 256.866, 0) )
		Create_MegaSphere( Vector(3232.467, 7627.6, 288.031), Angle(0, 247.461, 0) )
		Create_Armour100( Vector(4684.399, 5265.608, -319.969), Angle(0, 192.182, 0) )
		Create_Medkit25( Vector(4688.083, 5224.688, -319.969), Angle(0, 161.459, 0) )
		Create_SMGWeapon( Vector(4206.553, 4250.561, -319.969), Angle(0, 91.342, 0) )
		Create_SMGAmmo( Vector(4232.872, 4268.257, -319.969), Angle(0, 108.062, 0) )
		Create_SMGAmmo( Vector(4178.863, 4268.699, -319.969), Angle(0, 73.995, 0) )
		Create_PistolAmmo( Vector(4292.994, 3117.63, -434.969), Angle(0, 215.907, 0) )
		Create_SMGAmmo( Vector(4291.937, 3097.7, -434.969), Angle(0, 142.13, 0) )
		Create_ShotgunAmmo( Vector(4288.693, 3120.866, -471.969), Angle(0, 231.164, 0) )
		Create_RevolverAmmo( Vector(4288.73, 3098.405, -471.969), Angle(0, 190.2, 0) )
		Create_Medkit25( Vector(4282.146, 3175.819, -471.969), Angle(0, 188.11, 0) )
		Create_Medkit25( Vector(4250.095, 3154.173, -471.969), Angle(0, 157.596, 0) )
		Create_GrenadeAmmo( Vector(5194.58, 1319.501, -425.722), Angle(0, 1.891, 0) )
		Create_Medkit25( Vector(7273.369, 1681.543, -415.969), Angle(0, 257.613, 0) )
		Create_Medkit10( Vector(7241.626, 1680.711, -415.969), Angle(0, 254.791, 0) )
		Create_Armour( Vector(7205.988, 1675.123, -415.969), Angle(0, 263.256, 0) )
		Create_Armour5( Vector(6675.874, 1567.476, -447.969), Angle(0, 358.978, 0) )
		Create_Armour5( Vector(6792.63, 1569.274, -447.969), Angle(0, 357.515, 0) )
		Create_Armour5( Vector(6506.995, 1532.169, -447.969), Angle(0, 17.579, 0) )
		Create_Armour5( Vector(6388.72, 1434.446, -447.969), Angle(0, 41.823, 0) )
		Create_Beer( Vector(6323.933, 1325.151, -447.969), Angle(0, 62.724, 0) )
		Create_Beer( Vector(6305.435, 1156.742, -447.969), Angle(0, 85.505, 0) )
		Create_Beer( Vector(6303.884, 971.778, -447.969), Angle(0, 86.759, 0) )
		Create_RevolverAmmo( Vector(7060.146, 1568.131, -447.969), Angle(0, 181.538, 0) )
		Create_ShotgunAmmo( Vector(7166.946, 1672.543, -415.969), Angle(0, 273.08, 0) )
		Create_PortableMedkit( Vector(21.921, -1063.038, -349.969), Angle(0, 42.379, 0) )
		Create_Medkit10( Vector(16.536, -1038.445, -349.969), Angle(0, 24.405, 0) )
		Create_Medkit10( Vector(19.053, -1009.493, -349.969), Angle(0, 327.765, 0) )
	
		CreateSecretArea( Vector(3233.249,7510.688,288.031), Vector(-174.707,-174.707,0), Vector(174.707,174.707,212.845) )
	
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)