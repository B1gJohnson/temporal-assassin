
if CLIENT then return end

LEVEL_STRINGS["drob2"] = {"drob3", Vector(1666.03,-899.862,-1727.969), Vector(-118.828,-118.828,0), Vector(118.828,118.828,238.816)}

function MAP_LOADED_FUNC()	

	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("ambient_generic")
	RemoveAllEnts("env_fade")
	
	CreateClothesLocker( Vector(-298.724,-1428.423,-929.969), Angle(0,134.569,0) )
	CreateProp(Vector(1379.25,-3164,-1678.156), Angle(0,164.355,0), "models/props_c17/chair_stool01a.mdl", true)
	
	local H_PROP1 = CreateProp(Vector(-651.812,-2422.469,-1822.125), Angle(0.396,86.792,-0.176), "models/props_interiors/furniture_lamp01a.mdl", true)
	local H_PROP2 = CreateProp(Vector(-756.656,-2420.531,-1822.156), Angle(-0.703,65.786,0.088), "models/props_interiors/furniture_lamp01a.mdl", true)
	H_PROP1:SetPhysicsWeight(5000)
	H_PROP2:SetPhysicsWeight(5000)
	
	for _,v in pairs (ents.FindByClass("func_door")) do
		v:SetSaveValue("noise1","")
		v:SetSaveValue("noise2","")
	end
	
	Create_Medkit25( Vector(-1441.094, -2147.444, -1151.969), Angle(0, 346.603, 0) )
	Create_Medkit25( Vector(-1441.73, -2225.965, -1151.969), Angle(0, 7.504, 0) )
	Create_M6GWeapon( Vector(-412.229, -2983.419, -895.969), Angle(0, 140.63, 0) )
	Create_M6GAmmo( Vector(-419.708, -2949.58, -895.969), Angle(0, 178.877, 0) )
	Create_M6GAmmo( Vector(-453.742, -2987.747, -895.969), Angle(0, 91.097, 0) )
	Create_ShotgunWeapon( Vector(-1589.36, -1919.578, -1151.969), Angle(0, 180.024, 0) )
	Create_PistolWeapon( Vector(-1617.557, -1919.121, -1151.969), Angle(0, 180.86, 0) )
	Create_SMGWeapon( Vector(-1553.723, -1919.94, -1151.969), Angle(0, 179.397, 0) )
	Create_RifleWeapon( Vector(-1513.669, -1919.4, -1151.969), Angle(0, 180.651, 0) )
	Create_PortableMedkit( Vector(-1436.677, -1961.125, -1151.969), Angle(0, 159.124, 0) )
	Create_PortableMedkit( Vector(-1474.724, -1957.437, -1151.969), Angle(0, 148.256, 0) )
	Create_RevolverWeapon( Vector(-1477.724, -1917.381, -1151.969), Angle(0, 177.516, 0) )
	Create_GrenadeAmmo( Vector(-1404.905, -1882.192, -1151.969), Angle(0, 186.712, 0) )
	Create_GrenadeAmmo( Vector(-1448.707, -1880.933, -1151.969), Angle(0, 194.445, 0) )
	Create_Armour100( Vector(-1685.932, -1889.091, -1151.969), Angle(0, 198.834, 0) )
	Create_EldritchArmour10( Vector(-1658.063, -1955.631, -1151.969), Angle(0, 158.497, 0) )
	Create_EldritchArmour10( Vector(-1685.807, -1956.737, -1151.969), Angle(0, 148.256, 0) )
	Create_EldritchArmour10( Vector(-1712.438, -1959.152, -1151.969), Angle(0, 127.147, 0) )
	Create_KingSlayerAmmo( Vector(-599.349, -2091.084, -1151.969), Angle(0, 85.138, 0) )
	Create_KingSlayerAmmo( Vector(-493.525, -2091.994, -1151.969), Angle(0, 101.858, 0) )
	Create_KingSlayerWeapon( Vector(-545.647, -2092.672, -1151.969), Angle(0, 90.781, 0) )
	Create_CorzoHat( Vector(-2067.296, -1195.208, -985.24), Angle(0, 358.822, 0) )
	Create_CrossbowWeapon( Vector(-678.905, -1170.236, -1667.969), Angle(0, 357.048, 0) )
	Create_CrossbowAmmo( Vector(-634.232, -1118.264, -1667.969), Angle(0, 309.395, 0) )
	Create_CrossbowAmmo( Vector(-610.248, -1191.578, -1667.969), Angle(0, 10.842, 0) )
	Create_RevolverAmmo( Vector(-206.08, -2107.211, -1151.969), Angle(0, 180.442, 0) )
	Create_Medkit25( Vector(-280.423, -1682.764, -1151.969), Angle(0, 270.103, 0) )
	Create_InfiniteGrenades( Vector(-523.392, -1427.606, -929.969), Angle(0, 42.086, 0) )
	Create_Armour( Vector(-432.17, -1435.278, -929.969), Angle(0, 97.265, 0) )
	Create_FrontlinerAmmo( Vector(-778.365, -2754.421, -1151.969), Angle(0, 0.395, 0) )
	Create_Medkit25( Vector(-746.06, -2769.728, -1335.969), Angle(0, 359.039, 0) )
	Create_Medkit25( Vector(-752.929, -2910.562, -1335.969), Angle(0, 359.039, 0) )
	Create_GrenadeAmmo( Vector(568.06, -1995.218, -1599.969), Angle(0, 290.171, 0) )
	Create_VolatileBattery( Vector(96.5, -1887.663, -1863.969), Angle(0, 178.671, 0) )
	Create_FrontlinerWeapon( Vector(34.033, -1173.297, -1667.969), Angle(0, 174.378, 0) )
	Create_Medkit25( Vector(956.023, -2542.702, -1599.969), Angle(0, 89.1, 0) )
	Create_Medkit25( Vector(768.156, -2543.907, -1599.969), Angle(0, 88.682, 0) )
	Create_Beer( Vector(863.692, -2570.183, -1599.969), Angle(0, 90.563, 0) )
	Create_Beer( Vector(864.589, -2639.21, -1623.969), Angle(0, 90.772, 0) )
	Create_Beer( Vector(863.489, -2718.421, -1653.969), Angle(0, 88.682, 0) )
	Create_Armour100( Vector(1184.257, -3293.942, -1655.969), Angle(0, 87.845, 0) )
	Create_Armour( Vector(1320.675, -1963.509, -1727.969), Angle(0, 359.224, 0) )
	Create_Armour( Vector(1391.85, -1893.406, -1727.969), Angle(0, 301.54, 0) )
	Create_Backpack( Vector(1370.72, -1939.254, -1727.969), Angle(0, 307.183, 0) )
	Create_AnthrakiaWeapon( Vector(1026.098, -3193.253, -1823.969), Angle(0, 355.563, 0) )
	Create_SuperShotgunWeapon( Vector(783.455, -4014.483, -1791.969), Angle(0, 12.483, 0) )
	Create_ShotgunAmmo( Vector(795.306, -4058.209, -1791.969), Angle(0, 46.133, 0) )
	Create_ShotgunAmmo( Vector(775.721, -3965.274, -1791.969), Angle(0, 22.306, 0) )
	Create_SMGAmmo( Vector(986.684, -4209.273, -1791.969), Angle(0, 144.153, 0) )
	Create_SMGAmmo( Vector(955.291, -4226.387, -1791.969), Angle(0, 124.507, 0) )
	Create_SMGAmmo( Vector(912.285, -4223.466, -1791.969), Angle(0, 97.964, 0) )
	Create_PistolAmmo( Vector(966.508, -4182.849, -1791.969), Angle(0, 125.134, 0) )
	Create_PistolAmmo( Vector(934.598, -4193.615, -1791.969), Angle(0, 100.054, 0) )
	Create_PistolAmmo( Vector(903.897, -4184.386, -1791.969), Angle(0, 75.183, 0) )
	Create_RevolverWeapon( Vector(850.119, -4244.859, -2071.969), Angle(0, 279.696, 0) )
	Create_RevolverAmmo( Vector(836.879, -4275.896, -2103.969), Angle(0, 293.49, 0) )
	Create_RevolverAmmo( Vector(883.379, -4270.875, -2103.969), Angle(0, 274.68, 0) )
	Create_RevolverAmmo( Vector(883.383, -4225.521, -2103.969), Angle(0, 272.381, 0) )
	Create_PortableMedkit( Vector(1244.086, -4644.137, -1855.969), Angle(0, 98.493, 0) )
	Create_PortableMedkit( Vector(1244.239, -4589.765, -1855.969), Angle(0, 113.959, 0) )
	Create_Medkit10( Vector(628.053, -3256.858, -1791.969), Angle(0, 219.499, 0) )
	Create_Medkit10( Vector(594.56, -3254.357, -1791.969), Angle(0, 265.27, 0) )
	Create_FoolsOathAmmo( Vector(414.84, -3251.012, -1791.969), Angle(0, 181.122, 0) )
	Create_Armour5( Vector(274.132, -3314.38, -1791.969), Angle(0, 330.895, 0) )
	Create_Armour5( Vector(271.878, -3360.011, -1791.969), Angle(0, 357.648, 0) )
	Create_Armour5( Vector(322.222, -3308.782, -1791.969), Angle(0, 306.442, 0) )
	Create_Armour5( Vector(314.827, -3357.364, -1791.969), Angle(0, 352.423, 0) )
	Create_Armour5( Vector(296.864, -3332.22, -1791.969), Angle(0, 334.449, 0) )
	Create_SMGWeapon( Vector(711.42, -4435.787, -1855.969), Angle(0, 191.391, 0) )
	Create_SMGAmmo( Vector(691.343, -4401.101, -1855.969), Angle(0, 213.963, 0) )
	Create_SMGAmmo( Vector(700.086, -4480.944, -1855.969), Angle(0, 164.848, 0) )
	Create_Bread( Vector(1142.534, -5321.082, -1855.969), Angle(0, 97.341, 0) )
	Create_Bread( Vector(1073.151, -5316.679, -1855.969), Angle(0, 77.277, 0) )
	Create_SpiritEye2( Vector(320.262, -5278.491, -1791.969), Angle(0, 89.399, 0) )
	Create_Beer( Vector(-214.879, -5055.986, -1775.969), Angle(0, 352.841, 0) )
	Create_Beer( Vector(-60.508, -5055.946, -1775.969), Angle(0, 192.956, 0) )
	Create_Beer( Vector(-712.326, -5028.035, -1775.969), Angle(0, 97.968, 0) )
	Create_Beer( Vector(-887.426, -5027.748, -1775.969), Angle(0, 35.895, 0) )
	Create_Armour200( Vector(-1059.897, -5056.112, -2207.969), Angle(0, 357.743, 0) )
	Create_WhisperWeapon( Vector(-1213.61, -3940.315, -2439.969), Angle(0, 285.112, 0) )
	Create_WhisperAmmo( Vector(-1221.78, -4103.004, -2439.969), Angle(0, 329.21, 0) )
	Create_WhisperAmmo( Vector(-1222.058, -4124.094, -2439.969), Angle(0, 343.214, 0) )
	Create_ShadeCloak( Vector(-1268.636, -4113.627, -2439.969), Angle(0, 271.313, 0) )
	Create_PortableMedkit( Vector(-702.696, -4172.455, -1787.969), Angle(0, 33.472, 0) )
	Create_Medkit25( Vector(-649.714, -4190.937, -1787.969), Angle(0, 89.484, 0) )
	Create_Medkit25( Vector(-747.797, -4188.409, -1787.969), Angle(0, 74.854, 0) )
	Create_ShotgunAmmo( Vector(-1056.483, -4535.096, -1919.969), Angle(0, 261.801, 0) )
	Create_ShotgunAmmo( Vector(-1119.652, -4542.62, -1919.969), Angle(0, 277.267, 0) )
	Create_SMGAmmo( Vector(-1084.543, -4579.801, -1919.969), Angle(0, 259.711, 0) )
	Create_PistolAmmo( Vector(-1111.42, -4608.125, -1919.969), Angle(0, 265.772, 0) )
	Create_PistolAmmo( Vector(-1055.299, -4616.724, -1919.969), Angle(0, 237.767, 0) )
	Create_FrontlinerAmmo( Vector(-1367.5, -4549.941, -1919.969), Angle(0, 287.09, 0) )
	Create_RevolverAmmo( Vector(-1382.197, -4590.766, -1919.969), Angle(0, 307.154, 0) )
	Create_CrossbowAmmo( Vector(-1336.552, -4577.069, -1919.969), Angle(0, 270.161, 0) )
	Create_ManaCrystal100( Vector(-679.412, -5368.877, -1923.969), Angle(0, 120.309, 0) )
	Create_ManaCrystal100( Vector(960.771, -3661.294, -1791.969), Angle(0, 257.621, 0) )
	Create_SpiritSphere( Vector(1948.167, -1919.674, -1727.969), Angle(0, 181.444, 0) )
	Create_Medkit10( Vector(1904.84, -2529.146, -1599.969), Angle(0, 180.817, 0) )
	Create_Medkit10( Vector(1743.144, -2528.783, -1599.969), Angle(0, 358.676, 0) )
	Create_HarbingerAmmo( Vector(1953.043, -2971.423, -1599.969), Angle(0, 133.617, 0) )
	Create_RifleAmmo( Vector(-708.174, -2591.966, -1599.969), Angle(0, 328.813, 0) )
	Create_RifleAmmo( Vector(-714.235, -2657.438, -1599.969), Angle(0, 12.077, 0) )
	Create_RifleAmmo( Vector(-679.199, -2624.66, -1599.969), Angle(0, 358.491, 0) )
	Create_Medkit25( Vector(-361.348, -2415.292, -1151.969), Angle(0, 142.499, 0) )
	Create_Bread( Vector(-368.301, -1186.186, -1225.969), Angle(0, 238.639, 0) )
	Create_Bread( Vector(-267.776, -1329.647, -1225.969), Angle(0, 181.582, 0) )
	Create_EldritchArmour( Vector(510.488, -3453.599, -1935.969), Angle(0, 266.432, 0) )
	Create_Beer( Vector(-477.782, -1294.763, -1225.969), Angle(0, 279.056, 0) )
	Create_Beer( Vector(-477.681, -1207.595, -1225.969), Angle(0, 272.159, 0) )
	Create_Beer( Vector(-478.295, -1155.048, -1223.969), Angle(0, 271.114, 0) )
	Create_Beer( Vector(-478.281, -1073.432, -1443.969), Angle(0, 271.114, 0) )
	Create_Beer( Vector(-478.745, -977.681, -1479.969), Angle(0, 269.86, 0) )
	Create_Beer( Vector(-478.896, -866.314, -1521.969), Angle(0, 270.069, 0) )
	Create_Beer( Vector(-480.71, -742.996, -1535.969), Angle(0, 270.696, 0) )
	Create_Armour5( Vector(-673.687, -1071.381, -1225.969), Angle(0, 264.463, 0) )
	Create_Armour5( Vector(-696.233, -1079.381, -1225.969), Angle(0, 278.884, 0) )
	Create_Armour5( Vector(-649.497, -1086.456, -1225.969), Angle(0, 247.116, 0) )
	Create_Armour5( Vector(-672.789, -1043.66, -1215.969), Angle(0, 265.299, 0) )
	Create_Armour5( Vector(-288.512, -1045.131, -1215.969), Angle(0, 260.91, 0) )
	Create_Armour5( Vector(-310.39, -1066.808, -1225.969), Angle(0, 285.258, 0) )
	Create_Armour5( Vector(-287.883, -1073.284, -1225.969), Angle(0, 253.177, 0) )
	Create_Armour5( Vector(-263.917, -1063.407, -1225.969), Angle(0, 232.799, 0) )

	CreateSecretArea( Vector(-446.263,-2951.456,-891.437), Vector(-40.668,-40.668,0), Vector(40.668,40.668,31.06) )
	CreateSecretArea( Vector(-2070.455,-1194.737,-985.348), Vector(-33.794,-33.794,0), Vector(33.794,33.794,44.581) )
	CreateSecretArea( Vector(-348.997,-1216.387,-1667.969), Vector(-63.38,-63.38,0), Vector(63.38,63.38,111.956) )
	CreateSecretArea( Vector(-1058.202,-5056.179,-2203.437), Vector(-32.202,-32.202,0), Vector(32.202,32.202,50.348) )
	CreateSecretArea( Vector(511.33,-3450.17,-1935.969), Vector(-53.833,-53.833,0), Vector(53.833,53.833,6.899) )
	
	local S_BUTTON1 = function()
		CreateTeleporter( "TELE_1", Vector(1095.928,-3125.606,-1823.969), "TELE_2", Vector(1052.668,-3169.252,-1675.969) )
	end
	
	local S_BUTTON2 = function()
		Create_FalanranWeapon( Vector(-703.689, -1387.146, -1855.969), Angle(0, 267.791, 0) )
		Create_InvulnSphere( Vector(-705.216, -1448.55, -1855.969), Angle(0, 268.418, 0) )
	end
	
	AddSecretButton( Vector(1439.969,-3168.129,-1544.561), S_BUTTON1 )
	AddSecretButton( Vector(-705.322,-2431.969,-1800.743), S_BUTTON2 )
	
	FIX_BATTERY_SHIT()
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

function FIX_BATTERY_SHIT()

	timer.Simple(20,function()
	
		for _,v in pairs (ents.FindByClass("prop_physics")) do
		
			if v and IsValid(v) then
			
				local ID = v:MapCreationID()
				
				if ID == 1505 then
					v:SetPos(Vector(-577.081,-610.398,-911.969))
					v:GetPhysicsObject():Wake()
				elseif ID == 1537 then
					v:SetPos(Vector(-379.221,-602.718,-911.969))
					v:GetPhysicsObject():Wake()
				elseif ID == 1539 then
					v:SetPos(Vector(-378.388,-415.436,-911.969))
					v:GetPhysicsObject():Wake()
				elseif ID == 1540 then
					v:SetPos(Vector(-580.045,-414.504,-911.969))
					v:GetPhysicsObject():Wake()
				end
				
			end
		
		end
	
	end)

end