		
if CLIENT then return end

LEVEL_STRINGS["islandresort"] = { "islandresort2", Vector(2037.84,-8524.383,-1639.969), Vector(-79.861,-79.861,0), Vector(79.861,79.861,161.366) }

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-6363.976,-6355.987,-1103.969),Angle(0,90,0))
	
	Create_Medkit25( Vector(-6303.381, -2815.354, -1103.969), Angle(0, 180.06, 0) )
	Create_Medkit25( Vector(-6304.992, -2897.071, -1103.969), Angle(0, 178.74, 0) )
	Create_Armour100( Vector(-6417.154, -2871.038, -1103.969), Angle(0, 343.96, 0) )
	Create_ShotgunWeapon( Vector(-6361.338, -2699.805, -1103.969), Angle(0, 270.48, 0) )
	Create_SuperShotgunWeapon( Vector(-6361.23, -2737.238, -1103.969), Angle(0, 270.7, 0) )
	Create_PistolWeapon( Vector(-6357.508, -2780.383, -1103.969), Angle(0, 267.95, 0) )
	Create_RevolverWeapon( Vector(-6359.421, -2808.04, -1103.969), Angle(0, 268.5, 0) )
	Create_RifleWeapon( Vector(-6365.483, -2846.925, -1103.969), Angle(0, 272.02, 0) )
	Create_SMGWeapon( Vector(-6362.794, -2888.947, -1103.969), Angle(0, 271.14, 0) )
	Create_CrossbowWeapon( Vector(-6360.801, -2932.946, -1103.969), Angle(0, 270.26, 0) )
	Create_KingSlayerWeapon( Vector(-6360.215, -2992.256, -1103.969), Angle(0, 269.6, 0) )
	Create_Armour5( Vector(-2593.217, -2998.233, -1219.969), Angle(0, 359.668, 0) )
	Create_Armour5( Vector(-2480.714, -2998.033, -1219.969), Angle(0, 359.888, 0) )
	Create_Armour5( Vector(-2355.531, -2999.079, -1219.969), Angle(0, 0.328, 0) )
	Create_Beer( Vector(-2481.123, -3064.889, -1219.969), Angle(0, 269.467, 0) )
	Create_Beer( Vector(-2480.234, -3156.46, -1219.417), Angle(0, 268.587, 0) )
	Create_Beer( Vector(-2480.792, -3268.763, -1218.369), Angle(0, 267.267, 0) )
	Create_EldritchArmour10( Vector(-3151.804, -3613.804, -1212.969), Angle(0, 288.827, 0) )
	Create_EldritchArmour10( Vector(-3021.182, -3613.604, -1212.969), Angle(0, 249.667, 0) )
	Create_EldritchArmour10( Vector(-3084.945, -3613.112, -1212.969), Angle(0, 264.407, 0) )
	Create_Backpack( Vector(-4043.587, -5204.548, -1219.969), Angle(0, 6.658, 0) )
	Create_Bread( Vector(-4045.927, -5169.481, -1219.969), Angle(0, 357.198, 0) )
	Create_Bread( Vector(-4015.352, -5148.024, -1219.969), Angle(0, 335.198, 0) )
	Create_SpiritSphere( Vector(-4119.457, -5268.838, -1219.969), Angle(0, 181.198, 0) )
	Create_WhisperWeapon( Vector(4258.93, -2722.007, -975.969), Angle(0, 176.04, 0) )
	Create_WhisperAmmo( Vector(3784.642, -2031.436, -1110.969), Angle(0, 185.94, 0) )
	Create_VolatileBattery( Vector(3762.388, -2083.506, -1110.969), Angle(0, 145.46, 0) )
	Create_EldritchArmour( Vector(-2015.62, 1103.822, -1091.085), Angle(0, 57.108, 0) )
	Create_EldritchArmour( Vector(-1972.889, 1103.66, -1089.258), Angle(0, 96.268, 0) )
	Create_EldritchArmour10( Vector(2173.41, -7087.354, -1238.969), Angle(0, 234.805, 0) )
	Create_EldritchArmour10( Vector(2157.512, -7088.621, -1238.969), Angle(0, 244.705, 0) )
	Create_Backpack( Vector(2290.303, -6976.944, -1409.621), Angle(0, 203.345, 0) )
	Create_SpiritEye2( Vector(1894.2, -8629.714, -1411.969), Angle(0, 287.825, 0) )
	
	CreateSecretArea( Vector(-4119.457,-5268.838,-1219.969), Vector(-40.794,-40.794,0), Vector(40.794,40.794,78.835) )
	CreateSecretArea( Vector(3763.766,-2046.533,-1110.969), Vector(-37.828,-37.828,0), Vector(37.828,37.828,57.916) )
	CreateSecretArea( Vector(-2000.286,1100.669,-1086.531), Vector(-74.424,-74.424,0), Vector(74.424,74.424,72.123) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)