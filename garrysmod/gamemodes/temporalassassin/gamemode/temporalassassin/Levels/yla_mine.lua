		
if CLIENT then return end

LEVEL_STRINGS["yla_mine"] = {"yla_bridge", Vector(-3213.733,1583.927,204.969), Vector(-76.217,-76.217,0), Vector(76.217,76.217,-55.613)}

GLOB_NO_FENIC = true
GLOB_REPLACE_FENIC_GRISEUS = true

function MAP_LOADED_FUNC()

	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("game_text")

	CreatePlayerSpawn (Vector(881.227,1319.162,832.031), Angle(0,-124.052,0))

	Create_PistolWeapon( Vector(1075.763, 1394.182, 996.05), Angle(0, -13.972, 0) )
	Create_PistolAmmo( Vector(365.243, 1468.811, 82.353), Angle(-10.931, -161.715, 3.592) )
	Create_Armour( Vector(361.842, 1462.167, 64.021), Angle(-10.055, -30.757, 5.277) )
	Create_Armour5( Vector(372.97, 1472.105, 80.551), Angle(-6.504, -178.091, 6.961) )
	Create_Beer( Vector(372.17, 1438.388, 64.037), Angle(0, 322.356, 0) )
	Create_Beer( Vector(376.846, 1432.494, 64.245), Angle(0, -45.528, 0) )
	Create_Beer( Vector(375.438, 1422.234, 66.422), Angle(-90, -41.832, 0) )
	Create_Medkit25( Vector(875.498, 1639.395, 64.418), Angle(0, 333.62, 0) )
	Create_PistolAmmo( Vector(930.291, 1177.718, -13.996), Angle(-3.206, -59.779, -1.639) )
	Create_PistolAmmo( Vector(934.05, 1176.097, -12.84), Angle(4.658, 29.394, 12.616) )
	Create_Medkit25( Vector(1402.503, 257.979, -314.965), Angle(0, 41.48, 0) )
	Create_PistolAmmo( Vector(1400.488, 247.322, -309.364), Angle(0, 239.9, 0) )
	Create_ShotgunWeapon( Vector(230.86, 263.069, -569.332), Angle(73.356, -21.443, -135.703) )
	Create_ShotgunAmmo( Vector(313.925, -88.334, -575.969), Angle(0, 91.117, 0) )
	Create_ShotgunAmmo( Vector(305.664, -87.525, -575.792), Angle(0, 82.669, 0) )
	Create_ShotgunAmmo( Vector(309.215, -88.807, -571.186), Angle(0, 48.701, 0) )
	Create_ShotgunAmmo( Vector(-820.046, -561.387, -717.795), Angle(0, 119.62, 0) )
	Create_PistolAmmo( Vector(-878.615, -561.016, -717.954), Angle(0, 15.516, 0) )
	Create_PistolWeapon( Vector(1021.602, -1622.113, -765.415), Angle(0, 136.408, -180) )
	Create_Armour5( Vector(1001.368, -1594.23, -763.317), Angle(0, 102.32, 0) )
	Create_Armour5( Vector(999.736, -1583.023, -763.314), Angle(0.187, 93.906, 0.785) )
	Create_Medkit10( Vector(683.965, -206.158, -585.515), Angle(0, 209.812, 0) )
	Create_Medkit10( Vector(679.4, -204.106, -583.353), Angle(45, -99.328, 90) )
	Create_SMGAmmo( Vector(592.594, -244.754, -606.683), Angle(0, -2.112, 0) )
	Create_GrenadeAmmo( Vector(804.562, -1054.045, -600.946), Angle(0, 427.685, 0) )
	Create_SMGWeapon( Vector(-777.831, -1619.809, -707.888), Angle(4.515, -94.66, -62.525) )
	Create_Medkit25( Vector(-1747.969, -1085.861, -667.872), Angle(0, -2.048, 0) )
	Create_ShotgunAmmo( Vector(-1113.489, -712.439, -781.842), Angle(0, 139.139, 0) )
	Create_SMGAmmo( Vector(-1114.364, -648.755, -817.715), Angle(0, 212.707, 0) )
	Create_InfiniteGrenades( Vector(-1103.359, -611.847, -831.945), Angle(0, 268.147, 0) )
	Create_Medkit25( Vector(1002.644, 1182.203, -13.692), Angle(0, 232.883, 0) )
	Create_PistolAmmo( Vector(365.926, 128.316, -574.237), Angle(0, 92.787, 0) )
	Create_PistolAmmo( Vector(365.784, 131.236, -571.206), Angle(0, 92.787, 0) )
	Create_PistolAmmo( Vector(365.348, 133.163, -568.174), Angle(0, 92.435, 0) )
	Create_PistolAmmo( Vector(-498.446, 264.63, -536.05), Angle(-6.019, -96.741, -7.233) )
	Create_PistolWeapon( Vector(-496.089, 275.885, -536.068), Angle(0, 150.947, 0) )
	Create_Armour( Vector(-479.695, 268.258, -537.591), Angle(0, 232.163, 0) )
	Create_Medkit25( Vector(-557.861, 1142.272, -388.915), Angle(0, 269.475, 0) )
	Create_Armour100( Vector(-665.913, 1057.233, -433.998), Angle(0, 7.859, 0) )
	Create_ShotgunAmmo( Vector(-661.609, 1053.749, -397.982), Angle(0, 14.371, 0) )
	Create_ShotgunAmmo( Vector(-662.219, 1061.865, -397.953), Angle(0, 2.051, 0) )
	Create_SMGAmmo( Vector(-662.898, 1106.814, -397.935), Angle(0, 694.155, 0) )
	Create_SMGAmmo( Vector(-659.1, 1101.249, -396.42), Angle(-12.219, 80.047, 1.15) )
	Create_Beer( Vector(-608.999, 1144.08, -389.378), Angle(0, 277.57, 0) )
	Create_Beer( Vector(-603.758, 1138.696, -389.457), Angle(0, 261.994, 0) )
	Create_Beer( Vector(-596.291, 1143.124, -389.483), Angle(0, 314.178, 0) )
	Create_Beer( Vector(-590.355, 1137.634, -389.255), Angle(0, 252.402, 0) )
	Create_Beer( Vector(-585.25, 1143.057, -389.314), Angle(0, 280.914, 0) )
	Create_Beer( Vector(-279.842, 958.128, -447.597), Angle(0, 199.954, 0) )
	Create_Bread( Vector(-262.879, 961.08, -433.218), Angle(-6.077, 173.049, 58.579) )
	Create_Medkit25( Vector(-2314.135, 1075.979, -655.994), Angle(0, 195.186, 0) )
	Create_InfiniteGrenades( Vector(-2514.985, 1985.228, -395.849), Angle(0, 85.81, 0) )
	Create_Medkit25( Vector(-2479.039, 1981.743, -395.611), Angle(0, 82.818, 0) )

	local GR_LOCKER = function()
	
		CreateClothesLocker( Vector(1236.661,1291.333,960.031), Angle(0,89.822,0) )
		
		local HAT = CreateProp(Vector(1207.625,1280.563,1005.031), Angle(-11.865,93.032,16.831), "models/player/items/medic/coh_medichat.mdl", true)
		HAT:SetSkin(1)

	end
	
	CreateProp(Vector(873.531,1639.188,64.438), Angle(-0.044,0.615,0), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(1403.344,256.438,-319.531), Angle(0,129.287,0), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(308.656,-94.594,-575.562), Angle(0.044,90,0.044), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(802.469,-1056.375,-607.156), Angle(0.044,-2.153,0.044), "models/items/item_item_crate.mdl",true)
	CreateProp(Vector(1403.188,256.906,-319.5), Angle(0.088,-106.479,0.088), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(-2314.812,1074,-655.562), Angle(0,162.29,0.088), "models/items/item_item_crate.mdl", true)
	CreateProp(Vector(359.938,134.094,-573.094), Angle(-7.119,0.483,-2.285), "models/items/item_item_crate.mdl", true)

	local FABULOUSGALLON = CreateProp(Vector(-317.437,544.219,720.313), Angle(18.281,36.211,20.566), "models/player/items/engineer/engineer_cowboy_hat.mdl", true)
	local FABULOUSCHAIR =  CreateProp(Vector(-318.812,543,724.625), Angle(2.637,17.622,-0.132), "models/props_c17/furniturechair001a.mdl", true)
	local FABULOUSGUITAR =  CreateProp(Vector(-317.5,529.313,714.438), Angle(74.927,-29.531,15.908), "models/player/items/engineer/guitar.mdl", true)
	
	CreateTrigger( Vector(1299.117,1672.245,960.031), Vector(-26.027,-26.027,0), Vector(26.027,26.027,110.452), GR_LOCKER )
	CreateSecretArea( Vector(-321.358,544.249,704.05), Vector(-23.01,-23.01,0), Vector(23.01,23.01,60.81), "A FABULOUS MEMORY HAS BEEN FOUND!", "TemporalAssassin/Secrets/Secret_Guitar.wav")
	CreateSecretArea( Vector(311.434,-70.75,-568.357), Vector(-17.663,-17.663,0), Vector(17.663,17.663,30.961), "A SECRET STASH OF SHELLS HAS BEEN FOUND! (Password: A Light Shining In Darkness)", "TemporalAssassin/Secrets/Secret_Small.wav" )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)