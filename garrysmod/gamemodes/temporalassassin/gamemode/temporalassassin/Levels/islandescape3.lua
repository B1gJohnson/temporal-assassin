		
if CLIENT then return end

LEVEL_STRINGS["islandescape3"] = { "islandcove", Vector(4719.112,8385.436,657.691), Vector(-89.761,-89.761,0), Vector(89.761,89.761,127.941) }

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(3998.55,2879.79,653.335),Angle(0,90,0))
	
	for _,v in pairs (ents.FindInSphere(Vector(3993.031,2970.353,652.366),100)) do
		if IsValid(v) and v:IsDoor() then
			v:Fire("Open")
		end
	end
	
	Create_Medkit25( Vector(4070.997, 2926.857, 652.366), Angle(0, 179.634, 0) )
	Create_Medkit10( Vector(3932.212, 3938.919, 652.366), Angle(0, 294.914, 0) )
	Create_Medkit10( Vector(4011.192, 4013.492, 652.366), Angle(0, 272.254, 0) )
	Create_PistolWeapon( Vector(4337.361, 3926.556, 566.031), Angle(0, 79.974, 0) )
	Create_PortableMedkit( Vector(4393.799, 3925.316, 566.031), Angle(0, 94.354, 0) )
	Create_ShotgunWeapon( Vector(4616.661, 3982.733, 525.031), Angle(0, 180.814, 0) )
	Create_ShotgunAmmo( Vector(4602.282, 4016.623, 525.031), Angle(0, 208.314, 0) )
	Create_Armour( Vector(5280.203, 4654.022, 697.168), Angle(0, 122.373, 0) )
	Create_Medkit25( Vector(5080.441, 5178.41, 673.368), Angle(0, 270.953, 0) )
	Create_ManaCrystal( Vector(4403.439, 4774.012, 525.031), Angle(0, 245.512, 0) )
	Create_ManaCrystal( Vector(4439.522, 4740.246, 525.031), Angle(0, 190.732, 0) )
	Create_ManaCrystal( Vector(4442.657, 4678.771, 525.031), Angle(0, 177.092, 0) )
	Create_ManaCrystal( Vector(4407.716, 4629.75, 525.031), Angle(0, 143.432, 0) )
	Create_ManaCrystal100( Vector(4396.343, 4705.555, 525.031), Angle(0, 189.632, 0) )
	Create_Armour5( Vector(4346.898, 4777.736, 525.031), Angle(0, 271.692, 0) )
	Create_Armour5( Vector(4347.555, 4726.939, 525.031), Angle(0, 271.912, 0) )
	Create_Armour5( Vector(4324.1, 4681.657, 525.031), Angle(0, 276.092, 0) )
	Create_Armour5( Vector(4285.812, 4635.143, 525.031), Angle(0, 311.073, 0) )
	Create_Armour5( Vector(4326.134, 4596.134, 525.031), Angle(0, 270.042, 0) )
	Create_Armour5( Vector(4351.799, 4558.043, 525.031), Angle(0, 272.352, 0) )
	Create_Armour5( Vector(4356.859, 4510.324, 525.031), Angle(0, 49.633, 0) )
	Create_Beer( Vector(4337.874, 4937.018, 525.031), Angle(0, 269.052, 0) )
	Create_Beer( Vector(4384.024, 4935.891, 525.031), Angle(0, 231.432, 0) )
	Create_Beer( Vector(4431.39, 4935.454, 525.031), Angle(0, 208.332, 0) )
	Create_Beer( Vector(4479.84, 4936.013, 525.326), Angle(0, 223.512, 0) )
	Create_VolatileBattery( Vector(5227.661, 5470.208, 548.83), Angle(0, 161.892, 0) )
	Create_PistolAmmo( Vector(5190.128, 5468.375, 525.031), Angle(0, 136.812, 0) )
	Create_PistolAmmo( Vector(5217.244, 5493.044, 525.031), Angle(0, 162.112, 0) )
	Create_ShotgunAmmo( Vector(4550.26, 5234.184, 525.326), Angle(0, 78.24, 0) )
	Create_ShotgunAmmo( Vector(4635.635, 5233.849, 525.368), Angle(0, 112.12, 0) )
	Create_RevolverAmmo( Vector(4592.718, 5232.486, 525.368), Angle(0, 77.8, 0) )
	Create_RevolverWeapon( Vector(4489.347, 5647.389, 525.326), Angle(0, 336.38, 0) )
	Create_PistolWeapon( Vector(4484.799, 5568.473, 525.326), Angle(0, 38.2, 0) )
	Create_SMGWeapon( Vector(4507.172, 5608.212, 525.326), Angle(0, 4.1, 0) )
	Create_CrossbowAmmo( Vector(5073.469, 6318.151, 673.368), Angle(0, 232.32, 0) )
	Create_CrossbowWeapon( Vector(5487.344, 6709.375, 525.031), Angle(0, 139.62, 0) )
	Create_Medkit25( Vector(5486.928, 7266.193, 657.704), Angle(0, 180.143, 0) )
	Create_Medkit10( Vector(5486.732, 7188.038, 657.704), Angle(0, 163.864, 0) )
	Create_OverdriveSphere( Vector(5564.596, 7226.452, 657.691), Angle(0, 181.244, 0) )
	Create_OverdriveSphere( Vector(5560.18, 7226.356, 660.722), Angle(0, 181.244, 0) )
	Create_KingSlayerWeapon( Vector(6436.353, 7476.344, 914.075), Angle(0, 0.616, 0) )
	Create_KingSlayerAmmo( Vector(6467.333, 7445.392, 914.075), Angle(0, 25.476, 0) )
	Create_Armour( Vector(6065.904, 7109.172, 657.691), Angle(0, 156.373, 0) )
	Create_Armour5( Vector(6100.69, 7144.531, 657.691), Angle(0, 177.273, 0) )
	Create_Armour5( Vector(6065.578, 7145.839, 657.691), Angle(0, 177.053, 0) )
	Create_Armour5( Vector(6030.084, 7145.842, 657.691), Angle(0, 175.293, 0) )
	Create_Beer( Vector(5152.361, 7255.449, 657.704), Angle(0, 2.733, 0) )
	Create_Beer( Vector(5104.627, 7255.833, 657.704), Angle(0, 1.193, 0) )
	Create_Beer( Vector(5029.728, 7259.515, 657.704), Angle(0, 328.413, 0) )
	Create_Beer( Vector(5025.748, 7332.447, 657.691), Angle(0, 269.013, 0) )
	Create_Beer( Vector(5029.716, 7395.211, 657.691), Angle(0, 310.373, 0) )
	Create_RevolverAmmo( Vector(4554.557, 7239.608, 698.695), Angle(0, 53.559, 0) )
	Create_ShotgunAmmo( Vector(4603.989, 7239.363, 698.695), Angle(0, 108.119, 0) )
	Create_SMGAmmo( Vector(4589.287, 7239.672, 698.695), Angle(0, 122.639, 0) )
	Create_SMGAmmo( Vector(4569.946, 7238.945, 698.695), Angle(0, 91.839, 0) )
	Create_PistolAmmo( Vector(3629.072, 7577.225, 657.691), Angle(0, 302.118, 0) )
	Create_KingSlayerAmmo( Vector(3620.685, 7543.686, 657.691), Angle(0, 331.598, 0) )
	Create_Medkit25( Vector(3879.712, 7857.491, 657.691), Angle(0, 271.977, 0) )
	Create_Medkit25( Vector(3864.418, 7816.032, 657.691), Angle(0, 1.518, 0) )
	Create_SpiritSphere( Vector(4218.649, 7355.831, 832.708), Angle(0, 259.058, 0) )
	
	CreateProp(Vector(4454.281,7499.313,698.719), Angle(-0.044,-1.758,0), "models/props_c17/trappropeller_engine.mdl", true)
	CreateProp(Vector(4450.688,7442.031,698.75), Angle(0,1.582,0), "models/props_c17/trappropeller_engine.mdl", true)
	
	local B_FUNC = function()
		CreateProp(Vector(4780.156,7755.063,658.094), Angle(-0.044,-179.912,0), "models/props_combine/breendesk.mdl", false)
		CreateProp(Vector(4783.594,7755.063,689.531), Angle(0,-179.253,0), "models/temporalassassin/doll/unknown.mdl", false)
	end
	
	AddSecretButton( Vector(4438.365,7468.911,706.134), B_FUNC )
	
	CreateSecretArea( Vector(5204.779,5514.729,525.031), Vector(-64.402,-64.402,0), Vector(64.402,64.402,139.51) )
	CreateSecretArea( Vector(5490.681,6711.532,529.563), Vector(-21.418,-21.418,0), Vector(21.418,21.418,12.878) )
	CreateSecretArea( Vector(6450.05,7473.372,918.594), Vector(-54.028,-54.028,0), Vector(54.028,54.028,53.013) )
	CreateSecretArea( Vector(4217.363,7318.25,832.708), Vector(-28.592,-28.592,0), Vector(28.592,28.592,92.929) )
	
	local SOFTLOCK_FIX = function()
	
		for _,v in pairs (ents.FindByClass("func_button")) do
		
			if IsValid(v) and v:MapCreationID() == 1275 then
				v:Fire("Lock")
			end
		
		end
	
	end
	
	CreateTrigger( Vector(5412.443,7082.707,736.538), Vector(-54.026,-54.026,0), Vector(54.026,54.026,49.43), SOFTLOCK_FIX )
	
	local SOFTLOCK_HINT = function()
		net.Start("DollMessage")
		net.WriteString("This button does not work, just continue on")
		net.Broadcast()
	end
	
	CreateTrigger( Vector(5339.031,7187.247,710.871), Vector(-3.543,-3.543,0), Vector(3.543,3.543,7.349), SOFTLOCK_HINT )
	
	local SOFTLOCK_BOIS = function()
		CreateSnapNPC("npc_combine_s",Vector(6201.1689453125,7709.6015625,785.9892578125),Angle(0,201.32144165039,0),"weapon_smg1",true)
		CreateSnapNPC("npc_combine_s",Vector(6209.53125,7507.8618164063,785.9892578125),Angle(0,172.06144714355,0),"weapon_ar2",true)
		CreateSnapNPC("npc_metropolice",Vector(5515.4775390625,8067.5385742188,786.03125),Angle(0,291.81848144531,0),"weapon_stunstick",true)
		CreateSnapNPC("npc_metropolice",Vector(5648.650390625,8222.54296875,786.03125),Angle(0,272.48599243164,0),"weapon_smg1",true)
		CreateSnapNPC("npc_manhack",Vector(5624.869140625,7237.9868164063,596.63507080078),Angle(0,-179.05561828613,0),"false",true)
	end
	
	CreateTrigger( Vector(5561.057,7226.221,657.691), Vector(-37.415,-37.415,0), Vector(37.415,37.415,77.369), SOFTLOCK_BOIS )
	
	local SOFTLOCK_DOOR = function()
	
		for _,v in pairs (ents.FindInSphere(Vector(5020.673,7277.969,657.704),100)) do
		
			if IsValid(v) and v:IsDoor() then
				v:Fire("Unlock")
				v:Fire("Open")
			end
		
		end
	
	end
	
	CreateTrigger( Vector(5015.896,7230.171,657.704), Vector(-57.73,-57.73,0), Vector(57.73,57.73,122.714), SOFTLOCK_DOOR, true )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)