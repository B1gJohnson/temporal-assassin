
if CLIENT then return end

NPC_NO_DUPES = true
GLOB_NO_FENIC = true
GLOB_STARTHURT = 42

function MAP_LOADED_FUNC()
	
	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")
	
	CreateClothesLocker( Vector(-600.86993408203,-6691.4799804688,576.03125), Angle(0,359.79525756836,0) )
	
		
	local SAFETY_ON = function(self,ply)
		ply:SetSharedBool("Safety_Zone",true)
	end
	local SAFETY_OFF = function(self,ply)
		ply:SetSharedBool("Safety_Zone",false)
	end
		
	CreateTrigger( Vector(-65.029,558.614,-447.969), Vector(-139.422,-139.422,0), Vector(139.422,139.422,118.255), SAFETY_ON )
	CreateTrigger( Vector(-62.644,334.077,-447.969), Vector(-66.082,-66.082,0), Vector(66.082,66.082,124.062), SAFETY_OFF )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)