		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["06_ov_hrabova"] = {"07_ov_firm", Vector(1366.129,-3058.486,0.031), Vector(-235.995,-235.995,0), Vector(235.995,235.995,260.048)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(5093.694,593.477,0.031), Angle(0,126.412,0))
	
	Create_InfiniteGrenades( Vector(-1938.346, 2563.795, 16.434), Angle(83.633, -10.895, -3.899) )
	Create_GrenadeAmmo( Vector(-1908.477, 2552.551, 11.672), Angle(0, 354.125, 0) )
	Create_GrenadeAmmo( Vector(-1891.047, 2562.663, 11.576), Angle(0, 338.812, 0) )
	Create_Medkit10( Vector(-2163.969, 1836.303, 16.031), Angle(0, 25.981, 0) )
	Create_Medkit10( Vector(-2142.695, 1818.513, 19.588), Angle(-90, 84.984, 0) )
	Create_RevolverWeapon( Vector(-3559.285, -965.795, 8.256), Angle(0, 33.741, 0) )
	Create_SMGWeapon( Vector(-2693.764, 1413.521, 8.546), Angle(0, 79.053, 0) )
	Create_GrenadeAmmo( Vector(-2696.801, 1451.392, 8.222), Angle(0, 350.877, 0) )
	Create_GrenadeAmmo( Vector(-2724.994, 1470.378, 8.257), Angle(0, 6.541, 0) )
	Create_Armour( Vector(-3781.295, 1378.381, 55.296), Angle(0, 145.581, 0) )
	Create_PortableMedkit( Vector(-3352.108, -1050.041, 40.457), Angle(0, 94.893, 0) )
	Create_Medkit25( Vector(600.359, -422.665, 157.177), Angle(0, 88.528, 0) )
	Create_Armour100( Vector(409.602, -261.074, 64.196), Angle(0, 223.168, 0) )
	Create_SMGAmmo( Vector(407.226, -240.112, 114.369), Angle(0, 234.344, 0) )
	Create_SMGAmmo( Vector(406.693, -239.034, 116.672), Angle(0, 261.448, 0) )
	Create_ShotgunAmmo( Vector(389.61, -242.846, 114.57), Angle(0, 221.408, 0) )
	Create_ShotgunAmmo( Vector(389.902, -244.53, 119.825), Angle(0, 277.816, 0) )
	Create_PortableMedkit( Vector(384.185, -261.074, 64.138), Angle(0, 229.592, 0) )
	Create_RevolverAmmo( Vector(379.629, -245.302, 116.735), Angle(-90, 78.56, 180) )
	Create_PistolAmmo( Vector(369.401, -241.794, 114.248), Angle(0, 193.512, 0) )
	Create_PistolAmmo( Vector(364.34, -242.068, 115.117), Angle(-0.341, -82.189, 11.507) )
	Create_Beer( Vector(349.702, -243.569, 114.263), Angle(0, 257.224, 0) )
	Create_Bread( Vector(333.83, -240.247, 114.241), Angle(0, 309.056, 0) )
	Create_ShotgunWeapon( Vector(-3755.173, 2528.785, 40.201), Angle(-7.957, -197.087, 64.876) )
	Create_ShotgunAmmo( Vector(-3762.217, 2528.031, 16.031), Angle(0, 172.225, 0) )
	Create_ShotgunAmmo( Vector(-3763.817, 2529.413, 21.263), Angle(0, 105.873, 0) )

	CreateSecretArea( Vector(-3746.047,2564.42,16.031), Vector(-50.371,-50.371,0), Vector(50.371,50.371,86.475) )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)