
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	
	CreatePlayerSpawn( Vector(4163.424,-4448.317,1072.031), Angle(0,180,0) )
	
	CreateClothesLocker( Vector(2912.2761230469,-3609.2065429688,1072.03125), Angle(0,270.00012207031,0) )
	
	local START = Vector(1898.765,-2048.525,1675.17)
	local END = Vector(1601.198,-2046.834,1625.49)
	local BRUSH = GetObjectFromTrace( START, END, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH) then BRUSH:Remove() end
	
	local START2 = Vector(1668.893,-2053.211,1657.314)
	local END2 = Vector(1783.634,-2048.734,1635.736)
	local BRUSH2 = GetObjectFromTrace( START2, END2, MASK_PLAYERSOLID )
	
	if IsValid(BRUSH2) then BRUSH2:Remove() end
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Backpack( Vector(2026.3067626953, -3220.1320800781, 960.03125), Angle(0, 224.08422851563, 0) )
		Create_RevolverAmmo( Vector(1189.9432373047, -4761.8837890625, 1311.6900634766), Angle(0, 222.90422058105, 0) )
		Create_RevolverAmmo( Vector(1179.7995605469, -4742.5913085938, 1312.3840332031), Angle(0, 232.8042755127, 0) )
		Create_RevolverAmmo( Vector(1208.9504394531, -4771.2260742188, 1311.7368164063), Angle(0, 211.90420532227, 0) )
		Create_Medkit25( Vector(4597.9145507813, -4127.154296875, 1290.4962158203), Angle(0, 1.3639984130859, 0) )
		Create_Medkit25( Vector(4762.0390625, -3224.2543945313, 1408.03125), Angle(0, 272.26391601563, 0) )
		Create_Medkit25( Vector(4820.083984375, -3238.3366699219, 1408.03125), Angle(0, 242.56381225586, 0) )
		Create_PortableMedkit( Vector(4784.1357421875, -3263.681640625, 1408.03125), Angle(0, 257.30389404297, 0) )
		Create_RevolverWeapon( Vector(4750.9750976563, -3294.34375, 1408.03125), Angle(0, 319.34393310547, 0) )
		Create_Armour100( Vector(3319.3957519531, -1918.7735595703, 1408.03125), Angle(0, 99.263290405273, 0) )
		Create_KingSlayerWeapon( Vector(1488.5196533203, -3511.6435546875, 1536.03125), Angle(0, 89.583709716797, 0) )
		Create_KingSlayerAmmo( Vector(1598.6312255859, -3465.4536132813, 1536.03125), Angle(0, 88.263702392578, 0) )
		Create_KingSlayerAmmo( Vector(1522.8502197266, -3463.1567382813, 1536.03125), Angle(0, 88.263702392578, 0) )
		Create_KingSlayerAmmo( Vector(1455.2963867188, -3461.109375, 1536.03125), Angle(0, 88.263702392578, 0) )
		Create_KingSlayerAmmo( Vector(1386.5631103516, -3459.0251464844, 1536.03125), Angle(0, 88.263702392578, 0) )
		Create_Beer( Vector(2173.8837890625, -1402.6441650391, 1600.03125), Angle(0, 259.10775756836, 0) )
		Create_Beer( Vector(2153.8269042969, -1400.9807128906, 1600.03125), Angle(0, 274.72784423828, 0) )
		Create_Beer( Vector(2131.1630859375, -1402.5815429688, 1600.03125), Angle(0, 291.66790771484, 0) )
		Create_Beer( Vector(2107.4924316406, -1400.3673095703, 1600.03125), Angle(0, 305.08798217773, 0) )
		Create_Beer( Vector(2079.7512207031, -1399.5925292969, 1600.03125), Angle(0, 316.748046875, 0) )
		Create_Beer( Vector(2049.3654785156, -1401.0345458984, 1600.03125), Angle(0, 326.20809936523, 0) )
		
	else
	
		local P1 = CreateProp(Vector(1807.656,-3699.562,1083.063), Angle(0,-0.088,-0.044), "models/props_c17/furnituretable003a.mdl", true)
		local P2 = CreateProp(Vector(1829,-3747.844,1091.594), Angle(-0.044,39.463,-0.132), "models/props_interiors/furniture_chair03a.mdl", true)
		P1:SetHealth(999999)
		
		Create_Medkit25( Vector(1818.117, -3714.407, 1093.8), Angle(0, 351.406, 0) )
		Create_Medkit25( Vector(1816.187, -3685.972, 1093.789), Angle(0, 17.323, 0) )
		Create_InfiniteGrenades( Vector(1808.97, -3658.064, 1072.031), Angle(0, 16.905, 0) )
		Create_RevolverWeapon( Vector(1835.069, -3743.521, 1092.955), Angle(0, 41.775, 0) )
		Create_RevolverAmmo( Vector(1830.509, -3752.275, 1092.325), Angle(10.032, 19.514, -4.41) )
		Create_ShadeCloak( Vector(1183.907, -4166.482, 1287.129), Angle(0, 88.288, 0) )
		Create_KingSlayerWeapon( Vector(2911.843, -3690.815, 1280.031), Angle(0, 270.118, 0) )
		Create_KingSlayerAmmo( Vector(2947.179, -3646.115, 1280.031), Angle(0, 249.009, 0) )
		Create_KingSlayerAmmo( Vector(2871.896, -3647.282, 1280.031), Angle(0, 293.944, 0) )
		Create_FrontlinerAmmo( Vector(4750.172, -3236.383, 1408.031), Angle(0, 291.226, 0) )
		Create_FrontlinerWeapon( Vector(4808.646, -3288.049, 1408.031), Angle(0, 241.485, 0) )
		Create_Medkit25( Vector(3767.515, -3271.238, 1408.031), Angle(0, 276.596, 0) )
		Create_Medkit25( Vector(3874.025, -3268.957, 1408.031), Angle(0, 266.773, 0) )
		Create_Bread( Vector(3802.589, -3294.079, 1408.031), Angle(0, 289.763, 0) )
		Create_Bread( Vector(3844.378, -3295.719, 1408.031), Angle(0, 257.995, 0) )
		Create_Medkit25( Vector(3327.1, -1922.776, 1408.031), Angle(0, 84.841, 0) )
		Create_PortableMedkit( Vector(3302.397, -1901.248, 1408.031), Angle(0, 57.671, 0) )
		Create_SuperShotgunWeapon( Vector(2325.478, -2547.156, 1415.763), Angle(0, 349.536, 0) )
		Create_ShotgunAmmo( Vector(2340.411, -2581.174, 1415.763), Angle(0, 16.707, 0) )
		Create_SMGWeapon( Vector(2339.411, -2620.767, 1415.763), Angle(0, 41.996, 0) )
		Create_SMGAmmo( Vector(2375.028, -2636.176, 1415.763), Angle(0, 68.121, 0) )
		Create_ManaCrystal( Vector(1619.016, -2690.034, 1536.031), Angle(0, 84.942, 0) )
		Create_ManaCrystal( Vector(1473.494, -2801.883, 1536.031), Angle(0, 95.601, 0) )
		Create_ManaCrystal( Vector(1596.864, -2961.23, 1536.031), Angle(0, 180.037, 0) )
		Create_ManaCrystal( Vector(1772.104, -2812.986, 1536.031), Angle(0, 271.579, 0) )
		Create_CrossbowWeapon( Vector(2070.46, -1420.672, 1600.031), Angle(0, 282.865, 0) )
		Create_CrossbowAmmo( Vector(2042.388, -1462.214, 1600.031), Angle(0, 285.164, 0) )
		Create_Medkit25( Vector(2173.956, -1496.894, 1600.031), Angle(0, 182.963, 0) )
		Create_Medkit25( Vector(2173.931, -1416.21, 1600.031), Angle(0, 198.22, 0) )
		Create_PistolAmmo( Vector(1075.553, -1447.67, 1600.031), Angle(0, 355.282, 0) )
		Create_PistolAmmo( Vector(1030.758, -1478.866, 1600.031), Angle(0, 0.925, 0) )
		Create_SMGAmmo( Vector(1066.78, -1522.756, 1600.031), Angle(0, 6.359, 0) )
		Create_ShotgunAmmo( Vector(1073.932, -1486.149, 1600.031), Angle(0, 342.533, 0) )
		
		CreateSecretArea( Vector(1190.534,-4169.257,1290.425), Vector(-128.125,-128.125,0), Vector(128.125,128.125,127.636), false, "TemporalAssassin/Secrets/Secret_HollowKnight.wav" )
		CreateSecretArea( Vector(4803.196,-3276.251,1412.563), Vector(-91.346,-91.346,0), Vector(91.346,91.346,104.047) )
	
	end
	
	timer.Simple(5,function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_5.txt","DATA") then
			Create_LoreItem( Vector(3875.516,-2802.211,1508.54), Angle(0,-154.252,0), "lore_mthuulra_5", "Lore File Unlocked: Mthuulra #5" )
		end
	end)
	
	local REMIND_LORE = function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_5.txt","DATA") then
			net.Start("DollMessage")
			net.WriteString("There is another book here past this point")
			net.Broadcast()
		end
	end
	
	CreateTrigger( Vector(2093.633,-3678.424,1088.347), Vector(-103.223,-103.223,0), Vector(103.223,103.223,123.514), REMIND_LORE )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)