
if CLIENT then return end

GLOB_NO_SAVEDATA = true
GLOB_SAFE_GAMEEND = true
GLOB_CONTRACTOR_KIT = true

PrecacheParticleSystem("AstralApartment_MthuulraArms")
PrecacheParticleSystem("AstralApartment_Hint")

util.AddNetworkString("Astral_Loaded")
util.AddNetworkString("MAP_HintParticle")
util.AddNetworkString("Astral_MomArms")

function Map_NoKill( ply )	
	return false
end
hook.Add("CanPlayerSuicide","Map_NoKillHook",Map_NoKill)

function MAP_DoHintParticle()

	if MAP_HINT_POSITION then
		net.Start("MAP_HintParticle")
		net.WriteVector(MAP_HINT_POSITION)
		net.Broadcast()
	end

end
timer.Create("MAP_HintParticleTimer", 2, 0, MAP_DoHintParticle)

function SetMomArms(ply,bool)

	net.Start("Astral_MomArms")
	net.WriteBool(bool)
	net.Send(ply)

end

function MAP_CheckMirror( ply )

	if not ply then return end

	local POS = Vector(-1155.584,1996.141,0.031)
	
	local DIST = ply:GetPos():Distance(POS)
	local E_ANG = ply:EyeAngles()
	local DIF = math.AngleDifference( -90, E_ANG.y )
	
	if DIST <= 30 and DIF >= -15 and DIF <= 15 and E_ANG.p < 25 and E_ANG.p > -25 then
		return true
	else
		return false
	end

end

function MAP_SETUP_PROG( num )

	if PROGRESS_POINT and PROGRESS_POINT == num then return end
	
	PROGRESS_POINT = num

	if num == 1 then
	
		MAP_HINT_POSITION = Vector(-1205.184,1964.929,598.739)
		
		GetEntityByID(1266):Fire("Lock")
	
		local RK = CreateProp(Vector(-1197.719,1970.375,580.906), Angle(21.094,-104.37,14.947), "models/temporalassassin/items/weapon_raikiri.mdl", true)
		RK:SetModelScale(1.2,0)
		
		CreateProp(Vector(-1197.031,1923.594,593.75), Angle(-7.443,32.915,0.214), "models/temporalassassin/gore/antlion/head.mdl", true)
		CreateProp(Vector(-1214.812,1883.688,584), Angle(24.873,138.082,122.08), "models/temporalassassin/gore/antlion_guard/head.mdl", true)
		CreateProp(Vector(-1248.656,1898.688,583.406), Angle(-17.21,-124.508,-106.875), "models/temporalassassin/gore/antlion_guard/left_claw.mdl", true)
		CreateProp(Vector(-1258.625,1930.344,588.5), Angle(16.397,45.066,-92.889), "models/temporalassassin/gore/combine_s/head.mdl", true)
		CreateProp(Vector(-1178,2025,582.438), Angle(-26.202,141.641,-84.051), "models/temporalassassin/gore/combine_s/head_2.mdl", true)
		CreateProp(Vector(-1147.25,2057.938,581), Angle(-23.544,21.533,107.759), "models/temporalassassin/gore/combine_s/head_5.mdl", true)
		CreateProp(Vector(-1160.687,1993.813,628.813), Angle(-0.538,136.516,0.066), "models/temporalassassin/gore/combine_s/left_foot.mdl", true)
		CreateProp(Vector(-1170.531,1978.313,626.188), Angle(-9.152,137.593,-10.234), "models/temporalassassin/gore/combine_s/right_hand.mdl", true)
		CreateProp(Vector(-1116.156,2124.781,580.969), Angle(86.869,122.558,-104.255), "models/temporalassassin/gore/headcrab_classic/body_front.mdl", true)
		CreateProp(Vector(-1211.687,2263.031,584.438), Angle(67.632,85.133,164.498), "models/temporalassassin/gore/headcrab_fast/body_front.mdl", true)
		CreateProp(Vector(-1133.156,2209.906,582.031), Angle(62.49,19.512,178.841), "models/temporalassassin/gore/headcrab_poison/body_front.mdl", true)
		
		if IsMounted("ep2") then
			CreateProp(Vector(-1133.687,2156.375,582.563), Angle(61.617,133.781,90.582), "models/temporalassassin/gore/hunter/left_leg.mdl", true)
			CreateProp(Vector(-1270.5,2217.688,583.25), Angle(-37.579,-81.914,93.966), "models/temporalassassin/gore/hunter/right_leg.mdl", true)
		end
		
		CreateProp(Vector(-1246.125,1833.844,628.844), Angle(12.184,127.601,-87.578), "models/temporalassassin/gore/meleepolice/head.mdl", true)
		CreateProp(Vector(-1186.25,1943.969,580.031), Angle(10.091,77.97,90.725), "models/temporalassassin/gore/police/head.mdl", true)
		CreateProp(Vector(-1162.875,1957.875,628.063), Angle(10.47,49.037,-96.987), "models/temporalassassin/gore/police/head_elite.mdl", true)
		CreateProp(Vector(-1233.156,2351,579.5), Angle(21.401,-116.785,-96.405), "models/temporalassassin/gore/zombie_classic/head.mdl", true)
		CreateProp(Vector(-1303.25,2397.531,579.125), Angle(18.578,-1.154,110.929), "models/temporalassassin/gore/zombie_classic/head.mdl", true)
		CreateProp(Vector(-1236.656,2424.844,579.406), Angle(-42.138,-79.332,-128.397), "models/temporalassassin/gore/zombie_classic/hand_l.mdl", true)
		CreateProp(Vector(-1268.656,2382.156,577), Angle(-0.016,-91.082,0.104), "models/temporalassassin/gore/zombie_classic/foot_l.mdl", true)
		CreateProp(Vector(-1275.437,2233.844,585.781), Angle(-7.405,101.618,2.351), "models/temporalassassin/gore/zombine/head.mdl", true)
		CreateProp(Vector(-1255.062,2070,581.094), Angle(21.676,47.944,111.094), "models/temporalassassin/gore/zombie_fast/head.mdl", true)
		CreateProp(Vector(-1295.562,1998.438,581.063), Angle(21.319,160.17,111.39), "models/temporalassassin/gore/zombie_fast/head.mdl", true)
		
		for loop = 1,100 do
		
			local TR = {}
			TR.start = Vector(-1235.495,2014.314,632.031)
			local RAND = VectorRand()*1000
			RAND.z = RAND.z*1.5
			TR.endpos = TR.start + RAND
			TR.mask = MASK_PLAYERSOLID
			local Trace = util.TraceLine(TR)
			
			if Trace.Hit then
				util.Decal( "BigBlood", Trace.HitPos - Trace.HitNormal*1, Trace.HitPos + Trace.HitNormal*1 )
			end
			
		end
		
		local PROG_1 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"This feels familiar, where did you get this?",
			"It should feel familiar, it was your fathers.",
			"Heavily modified to not require as much lightning in your veins of course.",
			"I don't remember much about him, at least not before uh.",
			"Before what, Fox?",
			"It's nothing, let's get started."}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			KIT_TALK(STRING_LIST[1])
			TRB_TALK(STRING_LIST[2])
			TRB_TALK(STRING_LIST[3],2)
			KIT_TALK(STRING_LIST[4],2)
			TRB_TALK(STRING_LIST[5],2)
			KIT_TALK(STRING_LIST[6],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(2)
				end
			end)
			
		end
		CreateTrigger( Vector(-1205.378,1968.143,576.031), Vector(-31.8,-31.8,0), Vector(31.8,31.8,56.312), PROG_1 )
		
	elseif num == 2 then
	
		MAP_HINT_POSITION = Vector(-1173.899,1856.458,225.733)
		
		CreateProp(Vector(-1191.031,1795.781,192.344), Angle(-0.044,-0.027,-0.077), "models/temporalassassin/props/clothes_locker.mdl", true)
		CreateProp(Vector(-1186.375,1874.219,208.844), Angle(-0.28,0,0.022), "models/props_wasteland/controlroom_desk001b.mdl", true)
		CreateProp(Vector(-1173.406,1831.938,226.5), Angle(0,0.022,0), "models/temporalassassin/items/weapon_fenic.mdl", true)
		CreateProp(Vector(-1183,1842.969,226.094), Angle(-1.719,4.059,-0.132), "models/temporalassassin/items/fenic_ammo.mdl", true)
		CreateProp(Vector(-1186.656,1827.281,226.063), Angle(-1.78,29.597,-0.231), "models/temporalassassin/items/fenic_ammo.mdl", true)
		CreateProp(Vector(-1174.312,1847.5,226.031), Angle(-2.329,-7.806,0.11), "models/temporalassassin/items/fenic_ammo.mdl", true)
		CreateProp(Vector(-1178.156,1883.594,226.281), Angle(-0.346,-4.268,1.824), "models/temporalassassin/items/weapon_smg.mdl", true)
		CreateProp(Vector(-1186.344,1862.125,225.969), Angle(-1.005,-18.704,2.659), "models/temporalassassin/items/smg_ammo.mdl", true)
		CreateProp(Vector(-1180.406,1907.063,225.969), Angle(-0.978,40.001,2.455), "models/temporalassassin/items/smg_ammo.mdl", true)
		CreateProp(Vector(-1194.594,1880.406,226.5), Angle(-5.433,0.137,-0.824), "models/temporalassassin/items/armour_100.mdl", true)
		CreateProp(Vector(-1196.594,1849.25,226.156), Angle(-0.417,16.595,-0.714), "models/temporalassassin/items/armour_5.mdl", true)
		CreateProp(Vector(-1191.625,1906.719,226.156), Angle(-5.85,-8.026,-0.33), "models/temporalassassin/items/medkit_25.mdl", true)
		CreateProp(Vector(-1188.375,1833.125,192.531), Angle(-0.005,0.209,-0.165), "models/temporalassassin/items/volatilebattery.mdl", true)
	
		local PROG_2 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"What's all this?",
			"Equipment we created just for you, you'll find it out in the field as you go.",
			"Don't 'Assassins' usually take care of people quietly?",
			"That's correct, however you were referred to us for a different role.",
			"What role if not an Assassin?",
			"When all else fails, you send in the Reaper herself."}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			KIT_TALK(STRING_LIST[1])
			AGENCY_TALK(STRING_LIST[2])
			KIT_TALK(STRING_LIST[3],2)
			AGENCY_TALK(STRING_LIST[4],2)
			KIT_TALK(STRING_LIST[5],2)
			AGENCY_TALK(STRING_LIST[6],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(3)
				end
			end)
			
		end
		CreateTrigger( Vector(-1167.328,1857.762,192.031), Vector(-33.232,-33.232,0), Vector(33.232,33.232,63.693), PROG_2 )
	
	elseif num == 3 then
	
		MAP_HINT_POSITION = Vector(-1112.313,2327.802,360.134)
	
		local LIST = {CreateProp(Vector(-1110.531,2326.281,340.25), Angle(0.011,157.335,-0.033), "models/props_c17/furnituredrawer001a.mdl", true),
		CreateProp(Vector(-1091,2303.813,338), Angle(-0.049,43.682,0.121), "models/props_junk/propane_tank001a.mdl", true),
		CreateProp(Vector(-1083.531,2315.531,338.125), Angle(-1.258,36.491,0.033), "models/props_junk/propane_tank001a.mdl", true),
		CreateProp(Vector(-1080.375,2329.906,338.031), Angle(-0.033,4.719,0.082), "models/props_junk/propane_tank001a.mdl", true),
		CreateProp(Vector(-1069.219,2298.344,331.313), Angle(0.06,46.972,-0.022), "models/props_junk/propanecanister001a.mdl", true),
		CreateProp(Vector(-1052.031,2325.281,335.156), Angle(0.319,99.333,-0.093), "models/props_junk/metalgascan.mdl", true),
		CreateProp(Vector(-1001.812,2273.219,368.344), Angle(-0.06,100.942,0.005), "models/props_interiors/vendingmachinesoda01a.mdl", true),
		CreateProp(Vector(-970.531,2425.031,320.594), Angle(-2.422,178.022,0.066), "models/temporalassassin/items/armour_200.mdl", true),
		CreateProp(Vector(-974.625,2393.281,320.375), Angle(-0.099,158.077,-0.005), "models/temporalassassin/items/infinite_grenades.mdl", true),
		CreateProp(Vector(-1004.656,2240.531,320.313), Angle(-2.571,-171.211,1.763), "models/temporalassassin/items/whisper_ammo.mdl", true),
		CreateProp(Vector(-1015.094,2228.906,320.281), Angle(-2.609,170.046,1.769), "models/temporalassassin/items/whisper_ammo.mdl", true),
		CreateProp(Vector(-1110.125,2342,360.469), Angle(0.912,167.399,1.368), "models/temporalassassin/items/weapon_griseus.mdl", true),
		CreateProp(Vector(-1118.594,2319.813,360.688), Angle(2.362,161.197,-0.478), "models/temporalassassin/items/weapon_grismk2.mdl", true),
		CreateProp(Vector(-1109.969,2320.438,360.781), Angle(-5.812,159.093,0.61), "models/temporalassassin/items/grismk2_ammo.mdl", true),
		CreateProp(Vector(-1114.562,2311.594,360.688), Angle(-6.553,-114.686,1.478), "models/temporalassassin/items/grismk2_ammo.mdl", true),
		CreateProp(Vector(-1101.125,2338.031,361.688), Angle(2.016,-7.509,179.758), "models/temporalassassin/items/griseus_ammo.mdl", true),
		CreateProp(Vector(-1115.875,2332.625,360.531), Angle(0.873,-96.603,0.725), "models/temporalassassin/items/griseus_ammo.mdl", true),
		CreateProp(Vector(-968.125,2327.375,317.063), Angle(9.767,-178.599,38.265), "models/temporalassassin/items/whitecrystal.mdl", true)}
		
		for _,v in pairs (LIST) do
			if v and IsValid(v) then
				v:SetHealth(999999)
			end
		end
	
		local PROG_3 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"There you are, Kit. I wanted to talk with you.",
			"Is it about the 'vacation' you gave me?",
			"No, no, I don't want to talk about that mess with my ID.",
			"I'm still sorry that happened.",
			"We're both alive so what's it matter. What did you want to talk about?",
			"There's a contract I'm getting sorted out for you and I thought you'd appreciate this little ol' thing.",
			"What's that?",
			"Glad you asked! Introducing the Mk. II upgrade for my Special.",
			"That's not an upgrade, that's a whole different gun and a heavy one at that.",
			"I'm aware, but the previous model didn't quite cut it and couldn't keep up.",
			"And what are these? Do I really need this kind of thing?",
			"Stasis bombs! Or flashbangs.",
			"Flashbangs? You know most things I face don't have normal, let alone any eyes, right?",
			"Don't worry, thanks to my alchemist these things will freeze anything in the blast radius.",
			"There's more to the pistol itself, but I won't spoil the surprises.",
			"Interesting, looking forward to trying it out for uh, where am I going for this contract?",
			"Ever heard of City 10? I believe it'll test you in more ways than one.",
			"Perhaps make you realize a thing or two about yourself too."}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			GRIS_TALK(STRING_LIST[1])
			KIT_TALK(STRING_LIST[2])
			GRIS_TALK(STRING_LIST[3],2)
			GRIS_TALK(STRING_LIST[4],2)
			KIT_TALK(STRING_LIST[5],2)
			GRIS_TALK(STRING_LIST[6],2)
			KIT_TALK(STRING_LIST[7],2)
			GRIS_TALK(STRING_LIST[8],2)
			KIT_TALK(STRING_LIST[9],2)
			GRIS_TALK(STRING_LIST[10],2)
			KIT_TALK(STRING_LIST[11],2)
			GRIS_TALK(STRING_LIST[12],2)
			KIT_TALK(STRING_LIST[13],2)
			GRIS_TALK(STRING_LIST[14],2)
			GRIS_TALK(STRING_LIST[15],2)
			KIT_TALK(STRING_LIST[16],2)
			GRIS_TALK(STRING_LIST[17],2)
			GRIS_TALK(STRING_LIST[18],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(4)
				end
			end)
			
		end
		CreateTrigger( Vector(-1120,2330.877,320.031), Vector(-27.932,-27.932,0), Vector(27.932,27.932,62.41), PROG_3 )
		
	elseif num == 4 then
	
		MAP_HINT_POSITION = Vector(-1138.699,2253.18,100.957)
	
		CreateProp(Vector(-1135.437,2248.469,99.875), Angle(0.379,157.264,-0.626), "models/temporalassassin/items/weapon_photoop.mdl", true)
		CreateProp(Vector(-1140.625,2249.844,99.281), Angle(-2.032,138.049,0.159), "models/temporalassassin/items/photoop_ammo.mdl", true)
		CreateProp(Vector(-1097.687,2333.406,94.375), Angle(0.115,140.658,-0.203), "models/temporalassassin/items/eldritch_lore.mdl", true)
		local SHELF = CreateProp(Vector(-963.719,2423.719,107.688), Angle(0.39,178.022,-0.011), "models/props_interiors/furniture_shelf01a.mdl", true)
		SHELF:SetHealth(999999)
		CreateProp(Vector(-961.781,2426.594,101.531), Angle(0.352,177.627,0.154), "models/props_lab/binderblue.mdl", true)
		CreateProp(Vector(-961.656,2434.094,119.375), Angle(0.346,177.473,0.154), "models/props_lab/bindergraylabel01a.mdl", true)
		CreateProp(Vector(-961.656,2422.031,68.844), Angle(0.423,177.319,0.472), "models/props_lab/binderredlabel.mdl", true)
		CreateProp(Vector(-961.437,2433.188,85), Angle(0.385,175.638,-0.027), "models/props_lab/bindergreen.mdl", true)
		CreateProp(Vector(-962.094,2422.344,119.25), Angle(0.39,177.682,0.357), "models/props_lab/bindergreen.mdl", true)
		CreateProp(Vector(-962.156,2412.625,84.906), Angle(0.374,178.44,0.324), "models/props_lab/binderblue.mdl", true)
		CreateProp(Vector(-961.375,2431.156,68.781), Angle(0.39,177.341,0.077), "models/props_lab/binderblue.mdl", true)
		CreateProp(Vector(-962.469,2407.719,101.563), Angle(0.379,177.182,-0.176), "models/props_lab/bindergreenlabel.mdl", true)
		CreateProp(Vector(-962.031,2418.844,101.625), Angle(0.396,177.946,-0.049), "models/props_lab/bindergraylabel01b.mdl", true)
	
		local PROG_4 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"Right, I'm here, you wouldn't stop going on about it over the phone. What's up?",
			"Surprise, I made you a GUN!",
			"I can see that.",
			"It shoots REALLY, really fast. As fast as, if not faster than a high end SLR camera.",
			"Why does it hardly have any recoil?",
			"I had to lower the caliber it uses so it wouldn't break your arms off. Alex's advice.",
			"Appreciated, baffling how it's so stable.",
			"Sooooo... you like it?",
			"Yes I like it.",
			"Awesome! I'll make some last adjustments and come over with it later, sound good?",
			"I guess so, I don't think I've ever had visitors before.",
			"Well, why not take a day off and we can get some drinks and hang out!",
			"Uh.. Sure, why not. Always have to try something once, right?"}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			KIT_TALK(STRING_LIST[1])
			TWINS_TALK(STRING_LIST[2])
			KIT_TALK(STRING_LIST[3],2)
			TWINS_TALK(STRING_LIST[4],2)
			KIT_TALK(STRING_LIST[5],2)
			TWINS_TALK(STRING_LIST[6],2)
			KIT_TALK(STRING_LIST[7],2)
			TWINS_TALK(STRING_LIST[8],2)
			KIT_TALK(STRING_LIST[9],2)
			TWINS_TALK(STRING_LIST[10],2)
			KIT_TALK(STRING_LIST[11],2)
			TWINS_TALK(STRING_LIST[12],2)
			KIT_TALK(STRING_LIST[13],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(5)
				end
			end)
			
		end
		CreateTrigger( Vector(-1140.701,2252.627,64.031), Vector(-20.854,-20.854,0), Vector(20.854,20.854,60.719), PROG_4 )
	
	elseif num == 5 then
	
		MAP_HINT_POSITION = Vector(-967.349,2426.964,108.142)

		local PROG_5 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"Ah, Kit. I didn't know you had an interest in reading, let alone books.",
			"I'm not, I was just on my way out after your sister told me about a handgun.",
			"Oh, she finally finished that thing? Took her a long time to fine tune it.",
			"Yeah she did. Thanks for not blowing my arms off.",
			"Pff... She told you about that, huh? It's only common sense for an automatic weapon.",
			"Going to guess that books taught you that?",
			"Not... quite. It's a bit more complicated than that, but it's a long story.",
			"Huh, I'll have to ask later. Got a couple of jobs that need doing.",
			"Hang up a second, Kit. You feeling alright? You've been itching to leave ever since you arrived.",
			"Yeah, yeah. I'm fine, just not really used to spending time with people.",
			"I know it's all new to you, Kit. It's not a bad thing, having company. You get used to it.",
			"I figure, I gotta go get this done. You coming along with her tonight?",
			"With Lexa? Wait, where is she going?",
			"You're meaning to tell me she sprung the visiting out of nowhere?",
			"She IS rather outgoing, after all. Well, in that case, I guess we'll see you tonight.",
			"Right. I guess so."}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			TWINS_TALK(STRING_LIST[1],3)
			KIT_TALK(STRING_LIST[2])
			TWINS_TALK(STRING_LIST[3],4)
			KIT_TALK(STRING_LIST[4],2)
			TWINS_TALK(STRING_LIST[5],4)
			KIT_TALK(STRING_LIST[6],2)
			TWINS_TALK(STRING_LIST[7],4)
			KIT_TALK(STRING_LIST[8],2)
			TWINS_TALK(STRING_LIST[9],4)
			KIT_TALK(STRING_LIST[10],2)
			TWINS_TALK(STRING_LIST[11],4)
			KIT_TALK(STRING_LIST[12],2)
			TWINS_TALK(STRING_LIST[13],4)
			KIT_TALK(STRING_LIST[14],2)
			TWINS_TALK(STRING_LIST[15],4)
			KIT_TALK(STRING_LIST[16],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(6)
				end
			end)
			
		end
		CreateTrigger( Vector(-970.964,2424.804,64.784), Vector(-24.22,-24.22,0), Vector(24.22,24.22,91.919), PROG_5 )
	
	elseif num == 6 then
		
		MAP_HINT_POSITION = Vector(-1033.562,2266.011,488.042)
	
		CreateProp(Vector(-981.844,2253.594,448.344), Angle(0,107.672,0), "models/temporalassassin/props/altar.mdl", true)
		CreateProp(Vector(-1034.844,2270.219,488.563), Angle(1.395,81.985,-0.126), "models/temporalassassin/items/weapon_outcast.mdl", true)
		local CRT = CreateProp(Vector(-1036.812,2255.844,468.313), Angle(-0.033,77.272,-0.022), "models/props_junk/wood_crate001a.mdl", true)
		CRT:SetHealth(999999)
		CreateProp(Vector(-1019.906,2265.844,488.438), Angle(0.28,151.073,-0.049), "models/temporalassassin/items/outcast_ammo.mdl", true)
		
		local PROG_6 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"This really is her's isn't it.",
			"One for one, that's the same weapon she used way back then.",
			"Why give it to me anyway. Does she see some kind of importance in me that I don't get?",
			"You could say that. There's more to her than just doom and gloom y'know.",
			"It's hard to tell, what I can remember feels like such a blur.",
			"A blur huh? I know the feeling.",
			"What do you mean?",
			"I'll tell you later Kid, I need to figure something out first."}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			KIT_TALK(STRING_LIST[1])
			MALUM_TALK(STRING_LIST[2])
			KIT_TALK(STRING_LIST[3],2)
			MALUM_TALK(STRING_LIST[4],2)
			KIT_TALK(STRING_LIST[5],2)
			MALUM_TALK(STRING_LIST[6],2)
			KIT_TALK(STRING_LIST[7],2)
			MALUM_TALK(STRING_LIST[8],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(7)
				end
			end)
			
		end
		CreateTrigger( Vector(-1031.388,2276.881,448.031), Vector(-29.641,-29.641,0), Vector(29.641,29.641,61.647), PROG_6 )

	elseif num == 7 then
		
		MAP_HINT_POSITION = Vector(-1257.679,1914.286,222.358)
	
		local EYE = CreateProp(Vector(-1259.156,1916.281,222.531), Angle(-0.28,-114.186,0.253), "models/temporalassassin/items/eye_of_chari.mdl", true)
		EYE:SetModelScale(1.3,0)
		CreateProp(Vector(-1244.312,1911.594,224), Angle(-73.856,85.309,179.253), "models/temporalassassin/items/sinfragment.mdl", true)
		CreateProp(Vector(-1240.875,1904.031,222.719), Angle(19.655,-123.673,0.264), "models/temporalassassin/items/sinfragment.mdl", true)
		CreateProp(Vector(-1227.437,1891.75,192.344), Angle(-0.505,-136.17,-1.022), "models/temporalassassin/items/reikoririfle_ammo.mdl", true)
		CreateProp(Vector(-1241.219,1910.188,207.281), Angle(-0.126,-130.666,0.231), "models/props_lab/filecabinet02.mdl", true)
		CreateProp(Vector(-1258.594,1923.781,207.438), Angle(0,-130.924,0), "models/props_lab/filecabinet02.mdl", true)

		local PROG_7 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"You appear troubled, Cub. Are you unhappy with the current events?",
			"No, I'm fine. I just don't know what.",
			"Do not know what? Please do not delay while the thought is still fresh.",
			"Who, what am I. Really?",
			"Sadly, I cannot answer that question. my knowledge does not reach that far.",
			"I thought so. Malum might know a bit more.",
			"That would be very wise indeed. He has spent many a decade alongside her."}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			CHARI_TALK(STRING_LIST[1])
			KIT_TALK(STRING_LIST[2])
			CHARI_TALK(STRING_LIST[3],2)
			KIT_TALK(STRING_LIST[4],2)
			CHARI_TALK(STRING_LIST[5],2)
			KIT_TALK(STRING_LIST[6],2)
			CHARI_TALK(STRING_LIST[7],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(8)
				end
			end)
			
		end
		CreateTrigger( Vector(-1257.516,1904.116,193.688), Vector(-21.328,-21.328,0), Vector(21.328,21.328,51.824), PROG_7 )

	elseif num == 8 then
	
		MAP_HINT_POSITION = Vector(-1078.608,1816.153,68.287)
		
		CreateProp(Vector(-1076.437,1815.656,64.469), Angle(-1.335,-179.621,-0.467), "models/temporalassassin/items/weapon_royaldecree.mdl", true)
		CreateProp(Vector(-1066.781,1828.75,64.313), Angle(0.61,-163.757,-0.857), "models/temporalassassin/items/shotgun_ammo.mdl", true)
		CreateProp(Vector(-1066.656,1793.469,64.375), Angle(-0.862,157.022,1.659), "models/temporalassassin/items/shotgun_ammo.mdl", true)
		CreateProp(Vector(-1096.281,1832.531,64.375), Angle(-0.714,-85.298,1.313), "models/temporalassassin/items/shotgun_ammo.mdl", true)
		CreateProp(Vector(-1089.625,1809.281,64.375), Angle(8.613,-165.526,-0.374), "models/temporalassassin/props/malum_knife.mdl", true)

		local PROG_8 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"You wanted to talk Kid?",
			"Yeah, I wanted to ask you about, well, who I really am.",
			"That's.. a pretty loaded question Kid, I don't really know how to answer it.",
			"Ever since Stardust and I had that fight I've been having a hard time remembering.",
			"What, just remembering things from back then?",
			"Yes, It's really complicated. I just don't want to turn into something I'm afraid of.",
			"Something you're afraid of?",
			"You remember what happened in there, in my head. Let's just leave it at that.",
			"I'm off to try and find her, I have an idea on where she might be."}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			MALUM_TALK(STRING_LIST[1])
			KIT_TALK(STRING_LIST[2])
			MALUM_TALK(STRING_LIST[3],2)
			MALUM_TALK(STRING_LIST[4],2)
			KIT_TALK(STRING_LIST[5],2)
			MALUM_TALK(STRING_LIST[6],2)
			KIT_TALK(STRING_LIST[7],2)
			MALUM_TALK(STRING_LIST[8],2)
			MALUM_TALK(STRING_LIST[9],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(9)
				end
			end)
			
		end
		CreateTrigger( Vector(-1077.741,1813.15,68.508), Vector(-47.873,-47.873,0), Vector(47.873,47.873,39.32), PROG_8 )
		
	elseif num == 9 then
	
		MAP_HINT_POSITION = Vector(-1239.132,2248.731,488.441)
	
		local CRT = CreateProp(Vector(-1237.062,2236.75,468.5), Angle(0,89.94,0), "models/props_junk/wood_crate002a.mdl", true)
		CRT:SetHealth(999999)
		CreateProp(Vector(-1219.562,2241.125,488.844), Angle(-0.044,-99.229,0.192), "models/temporalassassin/props/gramaphone.mdl", true)
		CreateProp(Vector(-1245.781,2235.969,497.531), Angle(0.022,92.961,-0.082), "models/props_c17/briefcase001a.mdl", true)
		CreateProp(Vector(-1258.812,2239.469,497.5), Angle(0.082,121.745,-0.368), "models/props_c17/briefcase001a.mdl", true)
		
		local PROG_9 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"I am always here to talk if you need my help, Miss Wilde.",
			"Thanks, I'm just here for a little while.",
			"I have all of the time you could possibly need.",
			"Right. Malum left not too long ago and I wanted to ask you.",
			"Wha- No. Who do you think I really am?",
			"Who? You are Kitsune Wilde. The one and only.",
			"Do you think otherwise?",
			"No, not like that. I mean what am I supposed to be, what do people see me as?",
			"I see you as a young girl with infinite potential but a lack of self purpose.",
			"You have been thrown into this life of bloodshed and sit idle, waiting for your next assignment.",
			"At the present moment, you are lost without someone to force you to don the Reaper's cloak.", 
			"It is an excellent time to ask yourself... Who is it that you want to be, Miss Wilde?",
			"I... don't know. This is all I know, I'm not just some normal person.",
			"Normal, no. Who is normal around here? Yet you are still a person, as are we.",
			"It is good to have aspirations, newly forged or given. So long as it is your choice.",
			"Quite literally unlimited possibilities lie ahead of you. What do you want?",
			"To help keep things stable, to fight for a better world like what Malum told me.",
			"Noble. Keep in mind that there is more than one way to change the world.",
			"You do not need to follow someone else to do it.",
			"I guess you're right. Thanks, I think I know what I want now."}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			SEAMSTRESS_TALK(STRING_LIST[1])
			KIT_TALK(STRING_LIST[2])
			SEAMSTRESS_TALK(STRING_LIST[3],2)
			KIT_TALK(STRING_LIST[4],2)
			KIT_TALK(STRING_LIST[5],2)
			SEAMSTRESS_TALK(STRING_LIST[6],2)
			SEAMSTRESS_TALK(STRING_LIST[7],2)
			KIT_TALK(STRING_LIST[8],2)
			SEAMSTRESS_TALK(STRING_LIST[9],2)
			SEAMSTRESS_TALK(STRING_LIST[10],2)
			SEAMSTRESS_TALK(STRING_LIST[11],2)
			SEAMSTRESS_TALK(STRING_LIST[12],2)
			KIT_TALK(STRING_LIST[13],2)
			SEAMSTRESS_TALK(STRING_LIST[14],2)
			SEAMSTRESS_TALK(STRING_LIST[15],2)
			SEAMSTRESS_TALK(STRING_LIST[16],2)
			KIT_TALK(STRING_LIST[17],2)
			SEAMSTRESS_TALK(STRING_LIST[18],2)
			SEAMSTRESS_TALK(STRING_LIST[19],2)
			KIT_TALK(STRING_LIST[20],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(10)
				end
			end)
			
		end
		CreateTrigger( Vector(-1237.644,2256.813,448.541), Vector(-28.149,-28.149,0), Vector(28.149,28.149,65.1), PROG_9 )
		
	elseif num == 10 then
	
		MAP_HINT_POSITION = Vector(-1002.422,1821.263,351.381)
	
		CreateProp(Vector(-985.75,1822.125,320.344), Angle(0.038,-179.989,0), "models/props_combine/breendesk.mdl", true)
		CreateProp(Vector(-981.469,1851.594,360.75), Angle(-0.066,-143.042,0.165), "models/props_combine/breenglobe.mdl", true)
		CreateProp(Vector(-996.25,1792.188,355.969), Angle(0.593,131.391,0.027), "models/props_combine/breenclock.mdl", true)
		CreateProp(Vector(-992.531,1822,359.75), Angle(0.132,-178.226,0.055), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-978.25,1917,328.375), Angle(-0.082,-140.301,-0.005), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1306.812,1910.469,328.406), Angle(-0.06,-46.439,-0.005), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1234.656,1910.563,328.344), Angle(0.022,-95.543,-0.022), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1311.719,1725.188,328.406), Angle(0.022,37.029,0.016), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1159.781,1718.063,328.344), Angle(-0.099,99.283,-0.011), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1004.062,1737.813,328.375), Angle(-0.082,135.714,-0.005), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1078.812,1727.063,328.344), Angle(-0.099,79.047,-0.011), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1066.687,1915.594,328.375), Angle(-0.066,-100.833,-0.005), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1025.156,1936.625,387.031), Angle(-0.038,-89.879,0.275), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1067.969,1844.938,432.594), Angle(89.352,-164.971,-0.352), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-967.344,1824.125,423.219), Angle(0.231,-179.885,0.39), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1162.344,1739.25,432.5), Angle(89.995,-147.079,180), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1067.562,1753.469,432.531), Angle(89.346,-29.493,179.995), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1247.312,1936.5,407.625), Angle(-0.615,-90.428,0.423), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1293.25,1814.375,432.531), Angle(89.401,152.957,-178.555), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1328.125,1710.969,431.438), Angle(64.929,39.798,0), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-1160.625,1882.969,432.594), Angle(89.786,-109.825,1.857), "models/props_c17/tv_monitor01.mdl", true)
		CreateProp(Vector(-959.312,1717.656,376.219), Angle(-0.049,-179.736,-83.749), "models/props_c17/tv_monitor01.mdl", true)
		
		local PROG_10 = function(self,ply)
		
			MAP_HINT_POSITION = false
			
			ply:Freeze(true)
			
			local STRING_LIST = {
			"i am a part of you now. one day. you may learn new things.",
			"many things require one to look at themself with clear vision.",
			"You're saying I need to find myself, within myself?",
			"not yourself. the other you. the one that came from her."}
			
			local WAIT_TIME = ContractorMessageTime(STRING_LIST)
			
			LEVIATHAN_TALK(STRING_LIST[1])
			LEVIATHAN_TALK(STRING_LIST[2],2)
			KIT_TALK(STRING_LIST[3])
			LEVIATHAN_TALK(STRING_LIST[4],2)
			
			ply:SetSharedFloat("Memory_Visuals", CurTime() + WAIT_TIME)
			
			timer.Simple(WAIT_TIME,function()
				if ply and IsValid(ply) then
					ply:Freeze(false)
					MAP_SETUP_PROG(11)
				end
			end)
			
		end
		CreateTrigger( Vector(-1009.551,1823.782,320.031), Vector(-23.616,-23.616,0), Vector(23.616,23.616,54.806), PROG_10 )
		
	elseif num == 11 then
	
		MAP_HINT_POSITION = Vector(-1156.327,1970.408,40.188)
		
		CreateProp(Vector(-1162.406,1978.344,30.938), Angle(-42.215,90.61,37.996), "models/temporalassassin/items/hergift.mdl", true)
		CreateProp(Vector(-1183.344,1974.063,-3), Angle(-2.093,-6.097,13.112), "models/temporalassassin/items/weapon_azarath.mdl", true)
		
		timer.Create("Mirror_Check_Timer", 0, 0, function()
		
			local PLAYER = player.GetAll()[1]
			
			if PLAYER and IsValid(PLAYER) and MAP_CheckMirror(PLAYER) and not MAP_MirrorDone then
			
				MAP_MirrorDone = true
				SetMomArms(PLAYER,true)
				
				MAP_HINT_POSITION = false
				
				local STRING_LIST = {
				"I know you're there.",
				"You are here to ask about yourself are you not?",
				"I am.",
				"I suppose I cannot delay it",
				"Am I really your daughter?",
				"Yes. Back when I was not very in-tune with what I am capable of",
				"I mistakingly created another dimension to try and understand myself a little better",
				"However what I created contained another of myself, a yokai hybrid",
				"She is your mother, although the gift I sent to you in that lake",
				"The one I influenced you to use is what destroyed your dimension",
				"It is what brought you here, gave you the oddities you have and forever will have",
				"So, you basically fixed your mistake by having me stab myself.",
				"Yes, perhaps I was wrong to do such a thing so blindly",
				"Yet if I did not make that mistake, you would not exist as you are now",
				"When you put it that way I guess it's not always about blood.",
				"My mother would be the one who brought me into the world. That being you.",
				"What i have done is hardly excuseable, but to know that you think of me as such",
				"I hope that maybe one day I can come back and be 'Normal'",
				"You are my daughter, along with all the connotations that brings",
				"You know what dwells within you, you know how to use it",
				"",
				"I think I do."}
				
				local STRING_LIST2 = {
				"I know you're there.",
				"You are here to ask about yourself are you not?",
				"I am.",
				"I suppose I cannot delay it",
				"Am I really your daughter?",
				"Yes. Back when I was not very in-tune with what I am capable of",
				"I mistakingly created another dimension to try and understand myself a little better",
				"However what I created contained another of myself, a yokai hybrid",
				"She is your mother, although the gift I sent to you in that lake",
				"The one I influenced you to use is what destroyed your dimension",
				"It is what brought you here, gave you the oddities you have and forever will have",
				"So, you basically fixed your mistake by having me stab myself.",
				"Yes, perhaps I was wrong to do such a thing so blindly",
				"Yet if I did not make that mistake, you would not exist as you are now",
				"When you put it that way I guess it's not always about blood.",
				"My mother would be the one who brought me into the world. That being you.",
				"What i have done is hardly excuseable, but to know that you think of me as such",
				"I hope that maybe one day I can come back and be 'Normal'",
				"You are my daughter, along with all the connotations that brings",
				"You know what dwells"}
				
				local WAIT_TIME = ContractorMessageTime(STRING_LIST)
				local WAIT_TIME2 = ContractorMessageTime(STRING_LIST2)
				
				KIT_TALK(STRING_LIST[1])
				DOLL_TALK(STRING_LIST[2])
				KIT_TALK(STRING_LIST[3],2)
				DOLL_TALK(STRING_LIST[4],2)
				KIT_TALK(STRING_LIST[5],2)
				DOLL_TALK(STRING_LIST[6],2)
				DOLL_TALK(STRING_LIST[7],2)
				DOLL_TALK(STRING_LIST[8],2)
				DOLL_TALK(STRING_LIST[9],2)
				DOLL_TALK(STRING_LIST[10],2)
				DOLL_TALK(STRING_LIST[11],2)
				KIT_TALK(STRING_LIST[12],2)
				DOLL_TALK(STRING_LIST[13],2)
				DOLL_TALK(STRING_LIST[14],2)
				KIT_TALK(STRING_LIST[15],2)
				KIT_TALK(STRING_LIST[16],2)
				DOLL_TALK(STRING_LIST[17],2)
				DOLL_TALK(STRING_LIST[18],2)
				DOLL_TALK(STRING_LIST[19],2)
				DOLL_TALK(STRING_LIST[20],2)
				KIT_TALK(STRING_LIST[21],2)
				KIT_TALK(STRING_LIST[22])
				
				timer.Simple(WAIT_TIME2,function()
					if PLAYER and IsValid(PLAYER) then
						SetMomArms(PLAYER,false)
						PLAYER:SetEldritchBuff(true)
						PLAYER:Unlock_TA_LoreFile( "mirror_complete", "" )
						PLAYER:SetAchievementProgress("DATHARON_AWOKEN",1)
						TAS:SetPocketWatch(PLAYER,0)
						TAS:SetTemporalMana(PLAYER,TAS:GetMaxTemporalMana(PLAYER)*2)
						net.Start("Achievement_ForceSaveID")
						net.WriteString("DATHARON_AWOKEN")
						net.Send(PLAYER)
						PLAYER:SetSharedFloat("RELEASE_DATHARON_HINT",CurTime() + 11)
						PLAYER:SetSharedBool("STOP_ASTRAL_MUSIC",true)
						PLAYER:SetSharedBool("Safety_Zone",false)
						timer.Create("Check_Datharon_Release",0.1,0,function()
							if PLAYER and IsValid(PLAYER) and PLAYER:GetActiveWeapon() and IsValid(PLAYER:GetActiveWeapon()) and PLAYER:GetActiveWeapon():GetClass() == "temporal_datharon" then
								timer.Destroy("Check_Datharon_Release")
								timer.Simple(2.5,function()
									if PLAYER and IsValid(PLAYER) then
										TAS:SetPocketWatch(PLAYER,0)
									end
								end)
								timer.Simple(3,function()
									RunConsoleCommand("changelevel","ta_hub")
								end)
							end
						end)
					end
				end)
				
			end
		
		end)

	end

end

function Astral_Loaded_LIB( len, pl )

	pl:SetSharedBool("Safety_Zone",true)
	
	timer.Destroy("Mirror_Check_Timer")
	MAP_MirrorDone = false

	if pl:HasLoreFile("pet_unlocked") and pl:HasLoreFile("item_mthuulrapocketwatch_1") and pl:HasLoreFile("WHITESPACE_FINISHED") and pl:HasLoreFile("weapon_outcast_1") and pl:HasLoreFile("abyss_1") and pl:HasLoreFile("weapon_royaldecree_1") and pl:HasLoreFile("trinket_eyeofchari") and pl:HasLoreFile("kit_mirror") then
		
		MAP_SETUP_PROG(1)
		
	end

end
net.Receive("Astral_Loaded",Astral_Loaded_LIB)

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("weapon_")
	RemoveAllEnts("prop_physics")
	RemoveAllEnts("prop_ragdoll")
	RemoveAllEnts("func_button")
	RemoveAllEnts("env_sprite")
	
	CreatePlayerSpawn( Vector(-840.561,2087.196,0.031), Angle(0,0,0) )
	
	MAP_HINT_POSITION = false
	
	local NO_FALLING = function(self,ply)
	
		if ply and IsValid(ply) and ply:IsPlayer() then
	
			ply:SetLocalVelocity(Vector(0,0,0))
			
			local P1, P2 = CreateTeleporter( "Whoops1", ply:GetPos(), "Whoops2", Vector(-840.561,2087.196,0.031) )
			
			timer.Simple(0.1,function()
				if IsValid(P1) and IsValid(P2) then
					P1:Remove()
					P2:Remove()
				end
			end)
		
		end
	
	end
	CreateTrigger( Vector(-831.653,2052.506,-1711.969), Vector(-3730.233,-3730.233,0), Vector(3730.233,3730.233,1473.47), NO_FALLING, true )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)
