		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["03_uvalno_night"] = {"04_uvalno_kd", Vector(4566.476,1756.096,-63.969), Vector(-307.523,-307.523,0), Vector(307.523,307.523,263.581)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(3210.875,-3026.307,80.031),Angle(0,0.062,0))

	Create_Bread( Vector(4460.496, -148.788, 62.835), Angle(45, -104.728, 0) )
	Create_SuperShotgunWeapon( Vector(4411.822, 1115.595, 76.79), Angle(-12.425, 90.688, -46.121) )
	Create_ShotgunAmmo( Vector(4417.599, 1123.599, 64.194), Angle(0, 261.262, 0) )

	local VEH = ents.Create("prop_vehicle_jeep")
	VEH:SetModel("models/vehicle.mdl")
	VEH:SetKeyValue( "vehiclescript", "scripts/vehicles/jalopy.txt" )
	VEH:SetPos( Vector(3270.5,-3146.5,80.438) )
	VEH:SetAngles( Angle(0.396,-84.375,1.846) )
	VEH:Spawn()
	VEH:Activate()
	
	CreateSecretArea( Vector(4400.067,1122.849,64.031), Vector(-14.964,-14.964,0), Vector(14.964,14.964,35.723) )

	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

function NoShooty()

	for _,v in pairs (player.GetAll()) do
		
		if v then
			v:SetSharedBool("Safety_Zone",true)
		end
		
	end
	
end
timer.Create("NoShooty_ThisMap", 1, 0, NoShooty)