
DEV_GAMEOVER = true
GLOB_MUSIC_SILENCE = true
HUD_DONTDRAW_MANA = true
HUD_DONTDRAW_POCKETWATCH = true
GLOB_NO_SWAP_NAG = true
GLOB_GAMEOVER_FORCEDIFFICULTY = 7

local GLOB_BEACONS = {}

if GLOB_LEVIATHAN and IsValid(GLOB_LEVIATHAN) then
	GLOB_LEVIATHAN:Remove()
	GLOB_LEVIATHAN = nil
end

function AbyssBeaconClear_LIB( len )

	for _,v in pairs (GLOB_BEACONS) do
	
		if v and IsValid(v) then
			v:Remove()
		end
	
	end
	
	GLOB_BEACONS = {}

end
net.Receive("AbyssBeaconClear",AbyssBeaconClear_LIB)

function AbyssBeacon_LIB( len )

	local POS = net.ReadVector()
	local ID = net.ReadString()
	local BOOL = net.ReadBool()
	
	if BOOL then
		Create_AbyssBeacon( POS, ID )
	else
		Remove_AbyssBeacon( ID )
	end

end
net.Receive("AbyssBeacon",AbyssBeacon_LIB)

function Remove_AbyssBeacon( ID )

	if _G["ABYSS_BEACON_"..ID] and IsValid(_G["ABYSS_BEACON_"..ID]) then
		_G["ABYSS_BEACON_"..ID]:Remove()
	end

end

function Create_AbyssBeacon( pos, ID )

	if _G["ABYSS_BEACON_"..ID] and IsValid(_G["ABYSS_BEACON_"..ID]) then
		_G["ABYSS_BEACON_"..ID]:Remove()
	end

	_G["ABYSS_BEACON_"..ID] = ClientsideModel( "models/TemporalAssassin/brass/9x19.mdl", RENDERGROUP_OPAQUE )
	local BEACON = _G["ABYSS_BEACON_"..ID]
	BEACON:SetPos( pos )
	BEACON:SetRenderMode( RENDERMODE_TRANSALPHA )
	BEACON:SetColor( Color(0,0,0,0) )
	BEACON:Spawn()
	BEACON:Activate()
	
	ParticleEffectAttach( "Abyss_Beacon", PATTACH_ABSORIGIN_FOLLOW, BEACON, 0 )
	
	table.insert(GLOB_BEACONS,BEACON)

end

function Abyss_Think()

	local PL = LocalPlayer()
	
	if PL and IsValid(PL) and PL.GetPos and IsValid(PL:GetActiveWeapon()) then
	
		if not GLOB_LEVIATHAN then
		
			RunConsoleCommand("cl_show_splashes",0)
		
			GLOB_LEVIATHAN = ClientsideModel( Model("models/temporalassassin/abyss/levaithan.mdl"), RENDERGROUP_OPAQUE )
			GLOB_LEVIATHAN:SetPos( Vector(-5000,-5000,-512) )
			
			local POS = PL:GetPos()
			local POS2 = GLOB_LEVIATHAN:GetPos()
			POS.z = -512
			POS2.z = -512
			
			local ANG = (POS - GLOB_LEVIATHAN:GetPos()):GetNormal()
			GLOB_LEVIATHAN:SetAngles( ANG:Angle() )
			
			GLOB_LEVIATHAN.Move_Direction = ANG
			GLOB_LEVIATHAN:SetRenderBounds( Vector(-5000,-5000,-5000), Vector(5000,5000,5000) )
			GLOB_LEVIATHAN:Spawn()
			GLOB_LEVIATHAN:Activate()
			GLOB_LEVIATHAN:SetModelScale(1.25,0)
			GLOB_LEVIATHAN:SetRenderMode( RENDERMODE_TRANSALPHA )
			GLOB_LEVIATHAN:SetColor( Color(0,0,0,10) )
			
			PL.WaterLeviathan = CreateSound( PL, Sound("TemporalAssassin/Abyss/Water_Lev_Loop.wav") )
			PL.WaterLeviathan:PlayEx(0,100)
			
			PL.Abyss_Loop1 = CreateSound( PL, Sound("TemporalAssassin/Abyss/Abyss_Ambience.mp3") )
			PL.Abyss_Loop1:PlayEx(0.25,100)
			PL.Abyss_Loop2 = CreateSound( PL, Sound("TemporalAssassin/Abyss/Malums_Mind.mp3") )
			PL.Abyss_Loop2:PlayEx(0,100)
			PL.Abyss_Loop3 = CreateSound( PL, Sound("TemporalAssassin/Abyss/Leviathans_Hold.wav") )
			PL.Abyss_Loop3:PlayEx(0,100)
			
			net.Start("Abyss_StartMap")
			net.SendToServer()
			
		elseif GLOB_LEVIATHAN and IsValid(GLOB_LEVIATHAN) then
		
			local RFT = ViewModel_AnimationTime()*260
			local MUL = 1 - ((GLOB_LEVIATHAN:GetPos():Distance(PL:GetPos()))/2500)
			local MUL2 = 1 - ((GLOB_LEVIATHAN:GetPos():Distance(PL:GetPos()))/2000)
			local MUL3 = 1 - ((GLOB_LEVIATHAN:GetPos():Distance(PL:GetPos()))/3200)
			if MUL < 0 then MUL = 0 end
			if MUL > 1 then MUL = 1 end
			if MUL2 < 0 then MUL2 = 0 end
			if MUL2 > 1 then MUL2 = 1 end
			if MUL3 < 0 then MUL3 = 0 end
			if MUL3 > 1 then MUL3 = 1 end
			GLOB_LEVIATHAN:SetColor( Color(0,0,0,120*MUL) )
			local LV_POS = GLOB_LEVIATHAN:GetPos()
			LV_POS.z = -510 - (2*MUL2)
			GLOB_LEVIATHAN:SetPos( LV_POS )
			GLOB_LEVIATHAN:SetModelScale(0.6 + (0.4*MUL2),0)
			
			local POS = PL:GetPos()
			local POS2 = GLOB_LEVIATHAN:GetPos()
			POS.z = -512
			POS2.z = -512
			
			local ANG = (POS - GLOB_LEVIATHAN:GetPos()):GetNormal()
			GLOB_LEVIATHAN:SetPos( GLOB_LEVIATHAN:GetPos() + GLOB_LEVIATHAN.Move_Direction*RFT )
			
			local DIST = POS:Distance(GLOB_LEVIATHAN:GetPos())
			
			if PL.WaterLeviathan then
				PL.WaterLeviathan:ChangeVolume( 0.3*MUL3, 0 )
				PL.WaterLeviathan:ChangePitch( 60 - (25*MUL3), 0 )
			end
			
			if not PL.Abyss_Progress then
			
				if PL.Abyss_Loop1 then
					PL.Abyss_Loop1:ChangePitch( 99.9, 0 )
					PL.Abyss_Loop1:ChangePitch( 100, 0 )
					PL.Abyss_Loop1:SetSoundLevel(0)
				end
				if PL.Abyss_Loop2 then
					PL.Abyss_Loop2:ChangeVolume( 0, 0.5 )
				end
				if PL.Abyss_Loop3 then
					PL.Abyss_Loop3:ChangeVolume( 0, 0.5 )
				end
				
			elseif PL.Abyss_Progress and PL.Abyss_Progress == 1 then
			
				if PL.Abyss_Loop1 then
					PL.Abyss_Loop1:ChangeVolume( 0, 0.5 )
				end
				if PL.Abyss_Loop2 then
					PL.Abyss_Loop2:ChangeVolume( 0.3, 0.5 )
					PL.Abyss_Loop2:ChangePitch( 99.9, 0 )
					PL.Abyss_Loop2:ChangePitch( 100, 0 )
					PL.Abyss_Loop2:SetSoundLevel(0)
				end
				if PL.Abyss_Loop3 then
					PL.Abyss_Loop3:ChangeVolume( 0, 0.5 )
				end
				
			elseif PL.Abyss_Progress and PL.Abyss_Progress == 2 then
			
				if PL.Abyss_Loop1 then
					PL.Abyss_Loop1:ChangeVolume( 0, 0.5 )
				end
				if PL.Abyss_Loop2 then
					PL.Abyss_Loop2:ChangeVolume( 0, 0.5 )
				end
				if PL.Abyss_Loop3 then
					PL.Abyss_Loop3:ChangeVolume( 0.5, 0.5 )
					PL.Abyss_Loop3:ChangePitch( 99.9, 0 )
					PL.Abyss_Loop3:ChangePitch( 100, 0 )
					PL.Abyss_Loop3:SetSoundLevel(0)
				end
				
			elseif PL.Abyss_Progress and PL.Abyss_Progress == 3 then
			
				if PL.Abyss_Loop1 then
					PL.Abyss_Loop1:ChangeVolume( 0, 0.5 )
				end
				if PL.Abyss_Loop2 then
					PL.Abyss_Loop2:ChangeVolume( 0, 0.5 )
				end
				if PL.Abyss_Loop3 then
					PL.Abyss_Loop3:ChangeVolume( 1, 0.4 )
					PL.Abyss_Loop3:ChangePitch( 99.9, 0 )
					PL.Abyss_Loop3:ChangePitch( 100, 0 )
					PL.Abyss_Loop3:SetSoundLevel(0)
				end
				
				if not G_SWAP_LEVEL then
				
					G_SWAP_LEVEL = true
					G_SWAP_PARTICLES = UnPredictedCurTime() + 5
					
					timer.Simple(8,function()
						RunConsoleCommand("changelevel","ta_hub")
					end)
					
				end
				
				if G_SWAP_PARTICLES and UnPredictedCurTime() >= G_SWAP_PARTICLES then
				
					local data = {}
					data.IgnoreEnabled = true
					data.IgnoreTemporalShift = true
					data.Material = "smoke1"
					data.FadeSpeed = 100
					data.FadeTime = UnPredictedCurTime() + 1.5
					data.Position = {math.Rand(0,ScrW()), math.Rand(0,ScrH())}
					data.PositionDrift = {0,0}
					data.PositionNoise = 0
					data.PositionShake = 0
					data.Scale = {GUI_ScaleX(4),GUI_ScaleY(4)}
					data.ScaleNoise = 0
					data.ScaleNoiseSpeed = 0
					data.ScaleIncrease = 4
					data.Rotation = math.Rand(0,360)
					data.RotationSpeed = 0
					data.Gravity = 0
					data.Alpha = 0
					data.AlphaFlicker = 0
					data.AlphaMax = 250
					data.AlphaPulse = 0
					data.AlphaPulseSpeed = 0
					data.Colour = {0,0,0}
						
					HUDParticles_AddFront(data)
				
				end
				
			end

			if DIST >= 10000 then
				GLOB_LEVIATHAN:SetAngles( ANG:Angle() )
				GLOB_LEVIATHAN.Move_Direction = ANG
			elseif DIST <= 1500 then
				SetupCameraShake( 0.5, 0.3*MUL, 0, 20 )
			end
			
		end
	
	end
	
	GAMEOVER_TITLE_TEXT = ""
	GAMEOVER_RETRY1 = ""
	GAMEOVER_RETRY2 = ""
	GAMEOVER_QUIT1 = ""
	GAMEOVER_QUIT2 = ""
	
end
hook.Add("Think","Abyss_ThinkHook",Abyss_Think)