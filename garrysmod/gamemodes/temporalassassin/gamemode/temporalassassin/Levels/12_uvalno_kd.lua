		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["12_uvalno_kd"] = {"13_uvalno_night", Vector(-5048.564,-6192.254,208.153), Vector(-335.367,-335.367,0), Vector(335.367,335.367,1381.517)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(488,-936,40.749), Angle(0,180,0))
	
	CreateClothesLocker( Vector(-650.775,401.598,-31.969), Angle(0,359.775,0) )

	Create_Beer( Vector(-1973.912, 1609.173, -37.72), Angle(-90, -52.336, 135) )
	Create_PistolWeapon( Vector(-1564.927, 549.341, -205.892), Angle(0, 179.824, -180) )
	Create_Bread( Vector(-740.532, 249.224, 8.031), Angle(0, 41.889, 0) )
	Create_Beer( Vector(-736.594, 297.585, 8.031), Angle(0, 600.144, 0) )
	Create_ShotgunAmmo( Vector(-1110.463, 47.969, 5.891), Angle(0, 198.961, 0) )
	Create_Medkit25( Vector(-832.289, 476.239, 20.505), Angle(0, 226.416, 0) )
	Create_Beer( Vector(-1452.333, 120.418, -10.71), Angle(0, 86.144, 0) )
	Create_RevolverAmmo( Vector(-730.589, -7.807, 1.507), Angle(-90, -185.632, -180) )
	Create_RevolverWeapon( Vector(-2960.974, 417.766, 32.046), Angle(0, 256.249, 0) )
	Create_Armour100( Vector(-3022.544, 256.969, 0.031), Angle(0, 310.8, 0) )
	Create_SuperShotgunWeapon( Vector(-1793.429, 235.337, -192.717), Angle(0, 143.152, -45) )
	Create_RevolverWeapon( Vector(-2120.13, 1371.297, -97.943), Angle(77.206, -46.907, -86.479) )
	Create_Beer( Vector(-2353.043, 83.045, 160.25), Angle(0, 171.496, 0) )
	Create_FrontlinerWeapon( Vector(-2307.495, -61.094, 0.297), Angle(0, 103.824, 0) )
	Create_Medkit25( Vector(-3431.793, -108.031, -15.028), Angle(0, 292.84, 0) )
	Create_FrontlinerAmmo( Vector(-2945.379, 883.013, 35.208), Angle(0, 353.728, 0) )
	Create_FrontlinerAmmo( Vector(-2943.448, 883.546, 39.921), Angle(35.641, -49.208, -15.921) )
	Create_RifleWeapon( Vector(-2931.878, 802.243, 33.698), Angle(15.957, 95.683, 68.443) )
	Create_RifleAmmo( Vector(-2935.838, 791.186, 36.438), Angle(0, 268.456, 0) )
	Create_ShotgunWeapon( Vector(-2951.113, 717.162, 36.033), Angle(0, 24.056, 0) )
	Create_ShotgunAmmo( Vector(-2941.169, 729.922, 36.495), Angle(0, 319.935, 0) )
	Create_RevolverWeapon( Vector(-2954.641, 782.547, 35.506), Angle(0, 23.999, 0) )
	Create_SMGWeapon( Vector(-2953.935, 851.678, 35.902), Angle(0, 284.911, 0) )
	Create_Bread( Vector(-1438.524, 404.184, 8.377), Angle(0, 29.871, 0) )
	Create_Bread( Vector(-1439.832, 385.566, 8.221), Angle(0, 218.095, 0) )
	Create_VolatileBattery( Vector(-1956.74, -65.178, -161.973), Angle(-1.494, 90.434, 90) )
	Create_FrontlinerAmmo( Vector(-3435.167, -1704.423, -9.981), Angle(0, 101.766, 0) )
	Create_Medkit25( Vector(-3392.077, -1701.304, -10.182), Angle(0, 124.549, 0) )

	local OBJECTS = {}
	table.insert(OBJECTS, CreateProp(Vector(-2121.594,1382.844,-108.437), Angle(-0.703,-47.285,0.571), "models/props_junk/gnome.mdl", true))
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)