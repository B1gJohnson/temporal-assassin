		
if CLIENT then return end

GLOB_REPLACE_FENIC_GRISEUS = true

LEVEL_STRINGS["01_uvalno_villa"] = {"02_uvalno_undertower", Vector(-3677.154,-2259.624,126.298), Vector(-245.822,-245.822,0), Vector(245.822,245.822,447.972)}

GLOB_NO_FENIC = true

function MAP_LOADED_FUNC()

	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("game_text")
	
	CreatePlayerSpawn(Vector(-698.004,112.598,-47.969),Angle(0,179.6,0))

	CreateClothesLocker(Vector(-918.531,128.219,-47.656), Angle(0,-0.747,0))
	
	Create_Bread( Vector(-1076.95, 797.428, -7.02), Angle(0, 126.672, 0) )
	Create_Beer( Vector(-1084.017, 842.824, -7.158), Angle(0, 248.112, 0) )
	Create_Beer( Vector(-1082.321, 698.573, 138.031), Angle(0, 91.376, 0) )
	Create_Beer( Vector(-1071.69, 698.295, 138.159), Angle(0, 98.064, 0) )
	Create_Beer( Vector(602.984, 758.474, -45.96), Angle(0, 31.808, 0) )
	Create_Bread( Vector(624.605, 758.027, -45.969), Angle(0, 259.552, 0) )
	Create_Bread( Vector(2319.292, -254.774, -42.969), Angle(0, 86.431, 0) )
	Create_PistolAmmo( Vector(2164.048, -65.265, -46.268), Angle(0, 83.872, 0) )
	Create_PistolAmmo( Vector(2162.682, -60.364, -45.1), Angle(2.004, 172.827, 14.263) )
	Create_Armour100( Vector(2241.509, -65.899, -46.51), Angle(0, 97.424, 0) )
	Create_GrenadeAmmo( Vector(475.347, 92.031, 116.084), Angle(0, 57.455, 0) )
	Create_GrenadeAmmo( Vector(474.332, 98.734, 116.175), Angle(0, 355.327, 0) )
	Create_PistolWeapon( Vector(-681.351, 301.829, -11.9), Angle(0, 243.79, 0) )

	local GRIHAT = CreateProp(Vector(-697.437,300.063,-14.969), Angle(-10.063,-143.437,15.732), "models/player/items/medic/coh_medichat.mdl", true)
	GRIHAT:SetSkin(1)
	
	local OBJECTS = {}
	table.insert(OBJECTS,GRIHAT)
	table.insert(OBJECTS,CreateProp(Vector(2350.313,-197.812,-36.969), Angle(0.044,95.625,90.308), "models/weapons/c_models/c_frying_pan/c_frying_pan.mdl", true))
	table.insert(OBJECTS,CreateProp(Vector(2359.25,-194.406,-37.312), Angle(-0.352,-173.496,-0.703), "models/weapons/c_models/c_sandwich/c_sandwich.mdl", true))
	table.insert(OBJECTS,CreateProp(Vector(2299.219,-145.562,-45.812), Angle(3.296,1.714,46.758), "models/weapons/c_models/c_minigun/c_minigun.mdl", true))
	table.insert(OBJECTS,CreateProp(Vector(-1175,489.219,141.219), Angle(0.176,10.679,-0.044), "models/props_junk/cardboard_box003a.mdl", true))
	table.insert(OBJECTS,CreateProp(Vector(-1160.406,496.75,151.281), Angle(7.69,97.383,179.473), "models/weapons/w_models/w_knife.mdl", true))

	for _,v in pairs (OBJECTS) do
		if v and IsValid(v) and IsValid(v:GetPhysicsObject()) then
			v:GetPhysicsObject():SetMass(5000)
		end
	end
	
	local DOOR = GetObjectFromTrace(Vector(-912.966,311.969,8.031),Vector(-914.561,340.031,8.031), MASK_SHOT )
		if DOOR and IsValid(DOOR) then
		DOOR:Fire("Unlock")
	end
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)
	
function NoShooty()

    for _,v in pairs (player.GetAll()) do
    
        if v then
            v:SetSharedBool("Safety_Zone",true)
        end
    
    end

end
timer.Create("NoShooty_ThisMap", 1, 0, NoShooty)