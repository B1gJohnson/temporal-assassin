		
if CLIENT then return end

LEVEL_STRINGS["ep1_c17_00"] = {"ep1_c17_00a",Vector(1843.4185791016,22.385246276855,428.03125), Vector(-140.11552429199,-140.11552429199,0), Vector(140.11552429199,140.11552429199,228.81048583984)}

function EXTRA_FAIL_STATE( npc, att, inf )

	if IsValid(npc) and ( npc:GetClass() == "npc_alyx" ) then
	
		net.Start("GameOver_Custom")
		net.WriteString("LOOKS LIKE YOU FUCKED UP HUH?")
		net.Broadcast()
	
	end

end
hook.Add("OnNPCKilled","EXTRA_FAIL_STATEHook",EXTRA_FAIL_STATE)

function MAP_LOADED_FUNC()
	
	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end
	
	RemoveAllEnts("trigger_changelevel")
	
	CreateClothesLocker( Vector(3825.8654785156,-4535.7338867188,-123.96875), Angle(0,275.23641967773,0) )

	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Medkit25( Vector(4606.7680664063, -5713.4599609375, -111.96875), Angle(0, 332.65194702148, 0) )
		Create_Medkit25( Vector(4604.701171875, -5771.6928710938, -111.96875), Angle(0, 10.712097167969, 0) )
		Create_PortableMedkit( Vector(4661.6298828125, -5686.4682617188, -111.96875), Angle(0, 291.29187011719, 0) )
		Create_PortableMedkit( Vector(4689.8408203125, -5682.5698242188, -111.96875), Angle(0, 269.07180786133, 0) )
		Create_PistolAmmo( Vector(4531.59765625, -5887.0659179688, -111.96875), Angle(0, 243.33151245117, 0) )
		Create_PistolAmmo( Vector(4482.7485351563, -5885.45703125, -111.96875), Angle(0, 280.29159545898, 0) )
		Create_PistolAmmo( Vector(4507.427734375, -5906.6044921875, -111.96875), Angle(0, 257.41149902344, 0) )
		Create_PistolAmmo( Vector(4483.6865234375, -5913.4609375, -111.96875), Angle(0, 285.35159301758, 0) )
		Create_PistolAmmo( Vector(4531.3837890625, -5914.6196289063, -111.96875), Angle(0, 230.79156494141, 0) )
		Create_CorzoHat( Vector(3361.5573730469, -6275.0141601563, -111.96875), Angle(0, 8.8710632324219, 0) )
		Create_Armour( Vector(4158.5727539063, -4559.8720703125, -87.088562011719), Angle(0, 209.67041015625, 0) )
		Create_Armour( Vector(4122.1469726563, -4583.90234375, -88.719833374023), Angle(0, 204.39039611816, 0) )
		Create_Medkit10( Vector(4089.8159179688, -4627.255859375, -90.64013671875), Angle(0, 82.290374755859, 0) )
		Create_Medkit10( Vector(4070.1015625, -4601.2314453125, -90.685836791992), Angle(0, 10.790191650391, 0) )
		Create_SMGAmmo( Vector(3683.4384765625, -4444.87109375, -122.18359375), Angle(0, 14.969985961914, 0) )
		Create_SMGAmmo( Vector(3681.2590332031, -4404.8071289063, -122.04737854004), Angle(0, 349.66986083984, 0) )
		Create_SMGAmmo( Vector(4324.3959960938, -4291.3564453125, -119.96875), Angle(0, 87.869773864746, 0) )
		Create_SMGAmmo( Vector(4389.6484375, -4292.0971679688, -119.96875), Angle(0, 101.50971221924, 0) )
		Create_PistolAmmo( Vector(4449.7153320313, -3076.1813964844, -119.96875), Angle(0, 98.649612426758, 0) )
		Create_PistolAmmo( Vector(4452.919921875, -3048.0085449219, -119.96875), Angle(0, 118.66972351074, 0) )
		Create_PistolAmmo( Vector(4462.859375, -3020.2524414063, -119.96875), Angle(0, 203.36968994141, 0) )
		Create_SMGWeapon( Vector(4850.34765625, -3125.8703613281, -119.96875), Angle(0, 265.98944091797, 0) )
		Create_SMGAmmo( Vector(4597.4321289063, -3196.7900390625, -119.96875), Angle(0, 358.38940429688, 0) )
		Create_SMGAmmo( Vector(4828.0493164063, -3140.248046875, -119.96875), Angle(0, 278.30947875977, 0) )
		Create_SMGAmmo( Vector(4875.6552734375, -3136.6535644531, -119.96875), Angle(0, 231.88938903809, 0) )
		Create_RevolverWeapon( Vector(4337.3442382813, -2239.5808105469, -119.96875), Angle(0, 325.02865600586, 0) )
		Create_RevolverAmmo( Vector(4321.6884765625, -2221.2421875, -119.96875), Angle(0, 322.16864013672, 0) )
		Create_ShotgunWeapon( Vector(4388.6982421875, -2225.2014160156, -119.96875), Angle(0, 292.24853515625, 0) )
		Create_ShotgunAmmo( Vector(4410.7368164063, -2245.6623535156, -119.96875), Angle(0, 277.72845458984, 0) )
		Create_ShotgunAmmo( Vector(4357.8447265625, -2261.634765625, -119.96875), Angle(0, 330.30859375, 0) )
		Create_Medkit25( Vector(4581.3916015625, -2555.8029785156, -119.96875), Angle(0, 120.64813232422, 0) )
		Create_Medkit25( Vector(4516.447265625, -2563.7255859375, -119.96875), Angle(0, 89.408134460449, 0) )
		Create_PortableMedkit( Vector(4309.6264648438, -2462.2602539063, -69.645263671875), Angle(0, 9.3280334472656, 0) )
		Create_PortableMedkit( Vector(4310.6484375, -2434.5227050781, -69.645263671875), Angle(0, 332.80783081055, 0) )
		Create_ShotgunAmmo( Vector(3968.9924316406, -3542.6687011719, -119.96875), Angle(0, 69.607360839844, 0) )
		Create_ShotgunAmmo( Vector(4021.1494140625, -3523.4650878906, -119.96875), Angle(0, 97.107307434082, 0) )
		Create_Beer( Vector(3676.4365234375, -3601.5903320313, -121.74597167969), Angle(0, 39.467361450195, 0) )
		Create_Beer( Vector(3709.3713378906, -3611.6845703125, -123.80439758301), Angle(0, 50.907424926758, 0) )
		Create_Beer( Vector(3693.47265625, -3568.5405273438, -122.81073760986), Angle(0, 32.867324829102, 0) )
		Create_KingSlayerAmmo( Vector(3758.1540527344, -287.27062988281, -108.42295074463), Angle(0, 5.8511657714844, 0) )
		Create_KingSlayerAmmo( Vector(3771.5275878906, -234.81513977051, -103.42718505859), Angle(0, 310.85116577148, 0) )
		Create_RevolverAmmo( Vector(3877.2907714844, 1657.6384277344, -135.96875), Angle(0, 73.850692749023, 0) )
		Create_Armour( Vector(3908.8713378906, 1669.3973388672, -135.96875), Angle(0, 95.850776672363, 0) )
		Create_RevolverAmmo( Vector(2249.6391601563, 931.87414550781, 140.03125), Angle(0, 151.36940002441, 0) )
		Create_PistolAmmo( Vector(2228.3813476563, 932.94818115234, 140.03125), Angle(0, 142.56936645508, 0) )
		Create_PistolAmmo( Vector(2205.2453613281, 932.96508789063, 140.03125), Angle(0, 124.08932495117, 0) )
		Create_ShotgunAmmo( Vector(2221.79296875, -379.65606689453, 140.03125), Angle(0, 200.2971496582, 0) )
		Create_ShotgunAmmo( Vector(2223.5715332031, -452.67916870117, 140.03125), Angle(0, 164.21716308594, 0) )
		Create_SMGAmmo( Vector(2226.8425292969, -418.86175537109, 140.03125), Angle(0, 180.93716430664, 0) )

		CreateSecretArea( Vector(3374.7033691406,-6267.9833984375,-111.96875), Vector(-74.695739746094,-74.695739746094,0), Vector(74.695739746094,74.695739746094,101.24446105957) )
		CreateSecretArea( Vector(4849.3149414063,-3166.642578125,-119.96875), Vector(-42.668746948242,-42.668746948242,0), Vector(42.668746948242,42.668746948242,39.9375) )
	
		CreateProp(Vector(3673.34375,-7529.40625,-123.0625), Angle(0.263671875,67.1044921875,3.2958984375), "models/temporalassassin/doll/unknown.mdl", true)
		
	else
	
		CreateProp(Vector(3423.688,1238.906,132.031), Angle(0.044,129.639,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
	
		Create_CrossbowWeapon( Vector(4319.819, -5918.756, -115.969), Angle(0, 346.859, 0) )
		Create_CrossbowAmmo( Vector(4379.676, -5960.081, -115.969), Angle(0, 12.985, 0) )
		Create_CrossbowAmmo( Vector(4380.273, -5915.89, -115.969), Angle(0, 332.02, 0) )
		Create_CrossbowAmmo( Vector(4404.789, -5944.265, -115.969), Angle(0, 351.04, 0) )
		Create_PistolAmmo( Vector(4434.801, -5966.753, -115.969), Angle(0, 358.982, 0) )
		Create_PistolAmmo( Vector(4442.038, -5935.536, -115.969), Angle(0, 333.065, 0) )
		Create_PistolAmmo( Vector(4455.759, -5954.741, -115.969), Angle(0, 345.187, 0) )
		Create_Medkit25( Vector(4441.486, -5894.026, -111.969), Angle(0, 264.722, 0) )
		Create_Medkit25( Vector(4400.864, -5890.786, -111.969), Angle(0, 290.429, 0) )
		Create_Armour100( Vector(4507.783, -5896.209, -111.969), Angle(0, 270.992, 0) )
		Create_Beer( Vector(3967.819, -4902.079, -119.969), Angle(0, 181.33, 0) )
		Create_Beer( Vector(3968.14, -5154.264, -119.969), Angle(0, 178.195, 0) )
		Create_Beer( Vector(3968.87, -4684.715, -119.969), Angle(0, 183.629, 0) )
		Create_Beer( Vector(3968.145, -4458.706, -119.969), Angle(0, 224.593, 0) )
		Create_Beer( Vector(3968.267, -4257.908, -119.969), Angle(0, 351.038, 0) )
		Create_Armour( Vector(4154.748, -4559.545, -87.199), Angle(0, 218.324, 0) )
		Create_SuperShotgunWeapon( Vector(4853.414, -3123.801, -119.969), Angle(0, 270.394, 0) )
		Create_ShotgunAmmo( Vector(4836.954, -3154.291, -119.969), Angle(0, 299.027, 0) )
		Create_ShotgunAmmo( Vector(4866.975, -3150.704, -119.969), Angle(0, 248.867, 0) )
		Create_EldritchArmour10( Vector(4606.805, -2800.268, 120.031), Angle(0, 174.882, 0) )
		Create_EldritchArmour10( Vector(4587.232, -2799.806, 120.031), Angle(0, 170.493, 0) )
		Create_RevolverWeapon( Vector(4332.571, -2246.509, -119.969), Angle(0, 321.77, 0) )
		Create_RevolverAmmo( Vector(4344.99, -2271.632, -119.969), Angle(0, 334.519, 0) )
		Create_Medkit25( Vector(4324.681, -2651.944, -119.969), Angle(0, 13.476, 0) )
		Create_PistolAmmo( Vector(4327.27, -2613.632, -119.969), Angle(0, 4.907, 0) )
		Create_PistolAmmo( Vector(4361.573, -2665.271, -119.969), Angle(0, 54.858, 0) )
		Create_PistolAmmo( Vector(4349.765, -2633.857, -119.969), Angle(0, 26.852, 0) )
		Create_PistolAmmo( Vector(4435.454, -3074.737, -119.969), Angle(0, 66.353, 0) )
		Create_PistolAmmo( Vector(4465.84, -3065.886, -119.969), Angle(0, 97.285, 0) )
		Create_PortableMedkit( Vector(4310.851, -2726.869, -80.208), Angle(0, 336.69, 0) )
		Create_CrossbowAmmo( Vector(4178.182, -3524.581, -123.969), Angle(0, 62.587, 0) )
		Create_ShotgunAmmo( Vector(4245.379, -3528.257, -122.632), Angle(0, 116.927, 0) )
		Create_PortableMedkit( Vector(4209.273, -3495.377, -123.969), Angle(0, 96.863, 0) )
		Create_Armour( Vector(4240.093, -3454.586, -122.963), Angle(0, 99.789, 0) )
		Create_RevolverAmmo( Vector(4179.035, -3478.328, -123.969), Angle(0, 77.008, 0) )
		Create_Beer( Vector(4184.721, -1055.423, -119.969), Angle(0, 170.533, 0) )
		Create_Beer( Vector(4151.194, -1159.586, -119.969), Angle(0, 147.961, 0) )
		Create_Armour5( Vector(4167.896, -1107.576, -119.969), Angle(0, 159.456, 0) )
		Create_Medkit25( Vector(4618.595, -394.453, -107.969), Angle(0, 226.232, 0) )
		Create_Medkit10( Vector(4587.308, -418.182, -107.969), Angle(0, 270.958, 0) )
		Create_Medkit10( Vector(4606.624, -432.579, -107.969), Angle(0, 251.73, 0) )
		Create_VolatileBattery( Vector(4221.814, -1984.288, -64.04), Angle(0, 115.987, 0) )
		Create_ManaCrystal100( Vector(3791.464, -253.686, -105.224), Angle(0, 345.662, 0) )
		Create_ManaCrystal100( Vector(4010.69, 358.751, -121.949), Angle(0, 276.692, 0) )
		Create_Armour5( Vector(3830.918, -228.797, -102.969), Angle(0, 335.63, 0) )
		Create_Armour5( Vector(3867.44, -229.603, -102.969), Angle(0, 313.476, 0) )
		Create_Armour5( Vector(3904.462, -229.923, -102.969), Angle(0, 248.268, 0) )
		Create_Armour5( Vector(3941.059, -229.064, -102.969), Angle(0, 210.648, 0) )
		Create_CrossbowAmmo( Vector(3930.448, 1719.104, -135.969), Angle(0, 93.089, 0) )
		Create_Armour( Vector(3900.601, 1679.085, -135.969), Angle(0, 73.443, 0) )
		Create_ShotgunAmmo( Vector(3937.677, 1695.776, -135.969), Angle(0, 105.629, 0) )
		Create_ShotgunAmmo( Vector(3883.253, 1717.275, -135.969), Angle(0, 63.202, 0) )
		Create_RevolverAmmo( Vector(3890.177, 1754.244, -135.969), Angle(0, 53.588, 0) )
		Create_SpiritEye1( Vector(3924.807, 1784.67, -135.969), Angle(0, 90.372, 0) )
		Create_ManaCrystal( Vector(3100.14, 1702.595, 140.031), Angle(0, 180.076, 0) )
		Create_ManaCrystal( Vector(3004.2, 1704.994, 140.031), Angle(0, 182.793, 0) )
		Create_Medkit25( Vector(2909.371, 1584.838, 140.031), Angle(0, 92.923, 0) )
		Create_PortableMedkit( Vector(2979.916, 1582.305, 140.031), Angle(0, 128.348, 0) )
		Create_Beer( Vector(2334.117, 1474.562, 140.031), Angle(0, 25.268, 0) )
		Create_GrenadeAmmo( Vector(2900.846, 1516.81, 140.031), Angle(0, 222.982, 0) )
		Create_GrenadeAmmo( Vector(2879.425, 1520.774, 140.031), Angle(0, 249.734, 0) )
		Create_GrenadeAmmo( Vector(2892.009, 1497.267, 140.031), Angle(0, 206.471, 0) )
		Create_Armour5( Vector(2298.869, 1265.609, 140.031), Angle(0, 18.338, 0) )
		Create_Armour5( Vector(2298.474, 1300.194, 140.031), Angle(0, 26.698, 0) )
		Create_Armour5( Vector(2298.833, 1332.277, 140.031), Angle(0, 359.528, 0) )
		Create_ManaCrystal( Vector(2164.017, 1296.323, 140.031), Angle(0, 357.437, 0) )
		Create_Bread( Vector(2119.393, 1391.332, 144.031), Angle(0, 301.634, 0) )
		Create_SMGWeapon( Vector(2241.927, 951.67, 140.031), Angle(0, 190.135, 0) )
		Create_SMGAmmo( Vector(2209.697, 949.756, 140.031), Angle(0, 195.778, 0) )
		Create_SMGAmmo( Vector(2219.406, 927.166, 140.031), Angle(0, 165.473, 0) )
		Create_SpiritSphere( Vector(1594.109, -923.742, 140.79), Angle(0, 85.213, 0) )

		CreateSecretArea( Vector(4852.7,-3140.462,-115.437), Vector(-28.58,-28.58,0), Vector(28.58,28.58,32.62) )
		CreateSecretArea( Vector(4600.917,-2800.56,124.563), Vector(-20.094,-20.094,0), Vector(20.094,20.094,31.406) )
		CreateSecretArea( Vector(2228.187,945.332,144.563), Vector(-32.664,-32.664,0), Vector(32.664,32.664,36.398) )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)