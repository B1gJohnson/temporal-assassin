		
if CLIENT then return end

LEVEL_STRINGS["1_sm_off_course"] = {"2_sm_due_north",Vector(4254.853515625,-11708.123046875,-1071.0960693359), Vector(-144.30435180664,-144.30435180664,0), Vector(144.30435180664,144.30435180664,193.82531738281)}

function MAP_LOADED_FUNC()

	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")

	for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do
		file.Delete( tostring("temporalassassin/items/"..v) )
	end
		
	for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do
		file.Delete( tostring("temporalassassin/resources/"..v) )
	end
	
	RemoveAllEnts("info_player_start")
	RemoveAllEnts("trigger_changelevel")
	RemoveAllEnts("trigger_hurt")
	RemoveAllEnts("env_fade")
	
	for _,v in pairs (ents.GetAll()) do 
		
		if IsValid(v) and v:GetClass() == "trigger_teleport" and v:GetPos():Distance(Vector(4127.4853515625,-6219.1743164063,-631.96875)) <= 400 then
			v:Remove()
		end
	
		if IsValid(v) and v:GetClass() == "game_text" then
		
			if string.find( string.lower(v:GetKeyValues()["message"]),"auto save" ) then
				v:SetKeyValue("message","")
			end
			
			if string.find( string.lower(v:GetKeyValues()["message"]),"go back" ) then
				v:SetKeyValue("message","")
			end
			
			if string.find( string.lower(v:GetKeyValues()["message"]),"forgot" ) then
				v:SetKeyValue("message","")
			end
			
			if string.find( string.lower(v:GetKeyValues()["message"]),"forget" ) then
				v:SetKeyValue("message","")
			end
			
		end
	
	end
	
	CreatePlayerSpawn(Vector(-604.87878417969,-7306.853515625,-530.09375),Angle(0,0,0))
	CreateClothesLocker( Vector(3385.4440917969,-7715.7197265625,-559.96875), Angle(0,0.61970520019531,0) )
	
	Create_Armour( Vector(-172.41003417969, -7721.7788085938, -911.96875), Angle(0, 188.64010620117, 0) )
	Create_PistolAmmo( Vector(-82.075614929199, -7019.0849609375, -346.29742431641), Angle(0, 301.47131347656, 0) )
	Create_PistolAmmo( Vector(-83.225883483887, -6992.869140625, -346.62289428711), Angle(0, 317.31134033203, 0) )
	Create_SpiritSphere( Vector(-407.98645019531, -8625.6826171875, -615.43078613281), Angle(0, 186.7103729248, 0) )
	Create_Armour5( Vector(-71.524803161621, -7172.1713867188, -719.96875), Angle(0, 207.90980529785, 0) )
	Create_Armour5( Vector(-73.413642883301, -7232.7109375, -719.96875), Angle(0, 122.32968139648, 0) )
	Create_Armour5( Vector(-73.101722717285, -7279.4296875, -719.96875), Angle(0, 294.14987182617, 0) )
	Create_Armour5( Vector(-71.077896118164, -7203.4096679688, -719.96875), Angle(0, 178.20985412598, 0) )
	Create_Armour5( Vector(-71.841102600098, -7257.1533203125, -719.96875), Angle(0, 175.34985351563, 0) )
	Create_EldritchArmour( Vector(-242.50662231445, -6699.7807617188, -447.96875), Angle(0, 291.21011352539, 0) )
	Create_Medkit25( Vector(368.83270263672, -6676.4711914063, -351.96875), Angle(0, 180.7700958252, 0) )
	Create_Medkit25( Vector(245.61500549316, -6677.669921875, -351.96875), Angle(0, 0.14993286132813, 0) )
	Create_PortableMedkit( Vector(1447.6567382813, -5526.7592773438, -815.96875), Angle(0, 182.31062316895, 0) )
	Create_VolatileBattery( Vector(1892.3532714844, -5611.7309570313, -367.96875), Angle(0, 182.53053283691, 0) )
	Create_Armour5( Vector(2812.6530761719, -5273.0874023438, -447.96875), Angle(0, 179.75032043457, 0) )
	Create_Armour5( Vector(2679.4155273438, -5271.9819335938, -447.96875), Angle(0, 179.97032165527, 0) )
	Create_Armour5( Vector(2553.0686035156, -5272.4658203125, -447.96875), Angle(0, 179.75032043457, 0) )
	Create_Armour5( Vector(2421.6984863281, -5271.8935546875, -447.96875), Angle(0, 179.75032043457, 0) )
	Create_Armour5( Vector(2297.3483886719, -5272.9692382813, -447.96875), Angle(0, 178.87031555176, 0) )
	Create_Armour5( Vector(2172.41015625, -5272.05859375, -447.96875), Angle(0, 180.63032531738, 0) )
	Create_Armour5( Vector(2056.0056152344, -5273.234375, -447.96875), Angle(0, 180.19032287598, 0) )
	Create_RevolverWeapon( Vector(2718.8198242188, -5152.15625, -567.86175537109), Angle(0, 182.83068847656, 0) )
	Create_Medkit10( Vector(3684.7258300781, -5362.11328125, -629.96875), Angle(0, 89.595863342285, 0) )
	Create_Medkit10( Vector(3684.6279296875, -5177.6162109375, -629.96875), Angle(0, 270.43591308594, 0) )
	Create_Bread( Vector(4246.962890625, -7642.6982421875, -471.96875), Angle(0, 187.18106079102, 0) )
	Create_Bread( Vector(4229.4399414063, -7662.6752929688, -471.96875), Angle(0, 166.50108337402, 0) )
	Create_Bread( Vector(4221.3154296875, -7643.2231445313, -471.96875), Angle(0, 190.92108154297, 0) )
	Create_Bread( Vector(4209.4711914063, -7661.978515625, -471.96875), Angle(0, 159.02105712891, 0) )
	Create_Bread( Vector(4202.890625, -7643.2485351563, -471.96875), Angle(0, 191.14096069336, 0) )
	Create_Bread( Vector(4253.8041992188, -7662.4169921875, -471.96875), Angle(0, 173.54096984863, 0) )
	Create_PistolAmmo( Vector(4002.8803710938, -7934.1743164063, -631.96875), Angle(0, 318.16046142578, 0) )
	Create_PistolAmmo( Vector(4045.3623046875, -7953.28515625, -631.96875), Angle(0, 30.1005859375, 0) )
	Create_RevolverAmmo( Vector(2479.9006347656, -8352.6767578125, -782.96875), Angle(0, 14.339096069336, 0) )
	Create_PistolWeapon( Vector(2550.1674804688, -8204.3740234375, -782.96875), Angle(0, 337.82995605469, 0) )
	Create_Medkit10( Vector(2239.5515136719, -8624.099609375, -787.96875), Angle(0, 359.60992431641, 0) )
	Create_Medkit10( Vector(1930.1268310547, -8623.9814453125, -663.96875), Angle(0, 0.26991271972656, 0) )
	Create_ManaCrystal( Vector(2125.1552734375, -9905.380859375, -607.96875), Angle(0, 182.56846618652, 0) )
	Create_ManaCrystal( Vector(2122.78125, -9937.013671875, -607.96875), Angle(0, 180.36846923828, 0) )
	Create_Armour5( Vector(2013.2509765625, -9753.095703125, -599.96875), Angle(0, 0.76861572265625, 0) )
	Create_Armour5( Vector(1976.5, -9752.1953125, -599.96875), Angle(0, 359.22860717773, 0) )
	Create_Armour5( Vector(1948.3359375, -9751.546875, -599.96875), Angle(0, 358.56860351563, 0) )
	Create_Armour5( Vector(1947.8481445313, -9710.7626953125, -599.96875), Angle(0, 269.68856811523, 0) )
	Create_Armour5( Vector(1948.1823730469, -9676.9765625, -599.96875), Angle(0, 269.68856811523, 0) )
	Create_Armour5( Vector(1953.7661132813, -9648.3642578125, -599.96875), Angle(0, 177.94854736328, 0) )
	Create_Armour5( Vector(2069.1811523438, -9645.7041015625, -599.96875), Angle(0, 180.80854797363, 0) )
	Create_Armour5( Vector(2231.4313964844, -9646.337890625, -599.96875), Angle(0, 179.92855834961, 0) )
	Create_Armour5( Vector(2362.1806640625, -9647.1416015625, -599.96875), Angle(0, 179.0485534668, 0) )
	Create_Armour5( Vector(2419.3537597656, -9653.60546875, -599.96875), Angle(0, 90.608505249023, 0) )
	Create_ShotgunAmmo( Vector(2095.9533691406, -10263.61328125, -575.96875), Angle(0, 18.734939575195, 0) )
	Create_ShotgunAmmo( Vector(2098.4104003906, -10249.04296875, -575.96875), Angle(0, 352.11480712891, 0) )
	Create_PistolWeapon( Vector(2094.7607421875, -10119.87109375, -575.96875), Angle(0, 306.35464477539, 0) )
	Create_PistolAmmo( Vector(2097.1398925781, -10137.668945313, -575.96875), Angle(0, 17.195037841797, 0) )
	Create_SMGAmmo( Vector(2096.6848144531, -10185.815429688, -575.96875), Angle(0, 342.87484741211, 0) )
	Create_Medkit10( Vector(2095.5627441406, -10201.510742188, -575.96875), Angle(0, 22.035064697266, 0) )
	Create_Medkit25( Vector(2134.5341796875, -10045.618164063, -607.96875), Angle(0, 317.49560546875, 0) )
	Create_PortableMedkit( Vector(2288.1169433594, -10198.334960938, -575.96875), Angle(0, 158.21566772461, 0) )
	Create_EldritchArmour10( Vector(3286.2966308594, -9743.5810546875, -599.96875), Angle(0, 12.210266113281, 0) )
	Create_EldritchArmour10( Vector(3290.0856933594, -9725.193359375, -599.96875), Angle(0, 353.95016479492, 0) )
	Create_Medkit25( Vector(3780.7316894531, -9780.390625, -543.96875), Angle(0, 179.19013977051, 0) )
	Create_RevolverAmmo( Vector(4136.087890625, -9831.861328125, -581.85498046875), Angle(0, 245.11026000977, 0) )
	Create_RevolverAmmo( Vector(4135.6635742188, -9903.6318359375, -581.81671142578), Angle(0, 135.1102142334, 0) )
	Create_Beer( Vector(3987.1882324219, -10335, -599.96875), Angle(0, 25.841781616211, 0) )
	Create_Beer( Vector(4004.1743164063, -10266.059570313, -599.96875), Angle(0, 327.10147094727, 0) )
	Create_Bread( Vector(4031.6682128906, -10313.74609375, -599.96875), Angle(0, 29.581802368164, 0) )
	Create_SMGWeapon( Vector(5021.3696289063, -10735.178710938, -351.96875), Angle(0, 359.74737548828, 0) )
	Create_SMGAmmo( Vector(5027.1743164063, -10771.68359375, -351.96875), Angle(0, 88.403968811035, 0) )
	Create_SMGAmmo( Vector(5027.2822265625, -10700.078125, -351.96875), Angle(0, 271.30389404297, 0) )
	Create_Armour5( Vector(5245.0537109375, -10744.388671875, -375.96875), Angle(0, 221.94384765625, 0) )
	Create_Armour5( Vector(5245.890625, -10810.7421875, -375.96875), Angle(0, 143.40380859375, 0) )
	Create_RevolverAmmo( Vector(5702.5297851563, -10496.317382813, -423.96875), Angle(0, 215.99327087402, 0) )
	Create_Armour100( Vector(5894.9462890625, -11418.084960938, -719.96875), Angle(0, 42.632598876953, 0) )
	Create_PortableMedkit( Vector(5929.2607421875, -11423.5859375, -719.96875), Angle(0, 11.392440795898, 0) )
	Create_Medkit25( Vector(5888.9995117188, -11375.029296875, -719.96875), Angle(0, 1.4923248291016, 0) )
	Create_ManaCrystal100( Vector(5819.5668945313, -11336.12109375, -719.96875), Angle(0, 91.472091674805, 0) )
	Create_SMGAmmo( Vector(5196.8950195313, -11468.661132813, -719.96875), Angle(0, 246.31227111816, 0) )
	Create_PistolAmmo( Vector(5165.998046875, -11467.192382813, -719.96875), Angle(0, 265.89239501953, 0) )
	Create_PistolAmmo( Vector(5132.2915039063, -11466.744140625, -719.96875), Angle(0, 287.67242431641, 0) )
	Create_Bread( Vector(5264.1459960938, -11581.15625, -719.96875), Angle(0, 184.05215454102, 0) )
	Create_Bread( Vector(5233.6689453125, -11582.15625, -719.96875), Angle(0, 185.15214538574, 0) )
	Create_Medkit25( Vector(4962.818359375, -11572.51171875, -719.96875), Angle(0, 359.17242431641, 0) )
	Create_MegaSphere( Vector(6080.30859375, -11199.512695313, -183.96875), Angle(0, 180.2331237793, 0) )
	
	CreateSecretArea( Vector(-407.326171875,-8622.6630859375,-615.18444824219), Vector(-63.492069244385,-63.492069244385,0), Vector(63.492069244385,63.492069244385,127.37295532227) )
	CreateSecretArea( Vector(-230.8352355957,-6722.234375,-447.96875), Vector(-31.872087478638,-31.872087478638,0), Vector(31.872087478638,31.872087478638,10.811187744141) )
	CreateSecretArea( Vector(2707.169921875,-5153.3393554688,-563.3125), Vector(-31.404954910278,-31.404954910278,0), Vector(31.404954910278,31.404954910278,44.306762695313) )
	CreateSecretArea( Vector(4200.1962890625,-7653.1860351563,-467.4375), Vector(-26.376487731934,-26.376487731934,0), Vector(26.376487731934,26.376487731934,43.40625) )
	CreateSecretArea( Vector(3302.64453125,-9714.8271484375,-595.4375), Vector(-37.810749053955,-37.810749053955,0), Vector(37.810749053955,37.810749053955,31.983459472656) )
	CreateSecretArea( Vector(6060.126953125,-11199.041992188,-183.96875), Vector(-115.4534072876,-115.4534072876,0), Vector(115.4534072876,115.4534072876,165.25106811523) )
	
	CreateTeleporter( "SKIP_1", Vector(4126.258,-6256.644,-631.969), "SKIP_12", Vector(4125.856,-6633.669,-631.969) )
	CreateTeleporter( "SKIP_2", Vector(5685.505,-10549.483,-423.969), "SKIP_22", Vector(5841.208,-11015.178,-599.969) )
	
end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)