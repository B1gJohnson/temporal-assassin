
if CLIENT then return end

function MAP_LOADED_FUNC()
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_M6GAmmo( Vector(-10059.63671875, -6023.6235351563, -55.472988128662), Angle(0, 113.86000061035, 0) )
		Create_RevolverAmmo( Vector(-10097.015625, -6015.8740234375, -55.472984313965), Angle(0, 97.139945983887, 0) )
		Create_ShotgunAmmo( Vector(-10065.897460938, -5991.62890625, -55.472988128662), Angle(0, 118.26002502441, 0) )
		Create_ShotgunAmmo( Vector(-10091.506835938, -5973.1650390625, -55.472988128662), Angle(0, 106.37995910645, 0) )
		Create_SMGAmmo( Vector(-10017.833984375, -5975.7944335938, -55.472984313965), Angle(0, 151.48001098633, 0) )
		Create_SMGAmmo( Vector(-10026.401367188, -5945.7749023438, -55.472984313965), Angle(0, 167.75997924805, 0) )
		Create_ManaCrystal100( Vector(-9166.548828125, -5456.2045898438, 0.03125), Angle(0, 179.86001586914, 0) )
		Create_KingSlayerAmmo( Vector(-9218.00390625, -5456.078125, 0.03125), Angle(0, 179.86001586914, 0) )
		Create_KingSlayerAmmo( Vector(-9260.8076171875, -5455.97265625, 0.03125), Angle(0, 179.86001586914, 0) )
		Create_KingSlayerAmmo( Vector(-9303.7705078125, -5455.8676757813, 0.03125), Angle(0, 179.86001586914, 0) )
		Create_Medkit25( Vector(-8464.0400390625, -6127.6171875, 64.03125), Angle(0, 269.17776489258, 0) )
		Create_Medkit25( Vector(-8464.7216796875, -6175.2216796875, 64.03125), Angle(0, 269.17776489258, 0) )
		Create_PortableMedkit( Vector(-8482.029296875, -6230.0498046875, 64.03125), Angle(0, 277.9778137207, 0) )
		Create_PortableMedkit( Vector(-8444.9375, -6257.8696289063, 64.03125), Angle(0, 257.51776123047, 0) )
		Create_PortableMedkit( Vector(-8483.5048828125, -6290.8208007813, 64.03125), Angle(0, 287.21783447266, 0) )
		Create_RevolverAmmo( Vector(-6589.2661132813, -5596.6689453125, 64.03125), Angle(0, 323.03375244141, 0) )
		Create_RevolverAmmo( Vector(-6598.8603515625, -5636.26171875, 64.03125), Angle(0, 349.87384033203, 0) )
		Create_RevolverAmmo( Vector(-6570.3842773438, -5622.9301757813, 64.03125), Angle(0, 332.27374267578, 0) )
		Create_PistolWeapon( Vector(-6531.4658203125, -5597.232421875, 64.03125), Angle(0, 294.21368408203, 0) )
		Create_PistolWeapon( Vector(-6492.2524414063, -5593.7944335938, 64.03125), Angle(0, 268.25360107422, 0) )
		
	else
	
		Create_Armour( Vector(-8165.444, -6019.489, -63.969), Angle(0, 358.319, 0) )
		Create_Armour5( Vector(-8048.008, -6017.239, -63.969), Angle(0, 356.647, 0) )
		Create_Armour5( Vector(-7959.399, -6020.101, -57.201), Angle(0, 355.393, 0) )
		Create_Armour5( Vector(-7581.073, -6016.573, 0.031), Angle(0, 180.251, 0) )
		Create_Armour5( Vector(-7525.302, -6092.439, 0.031), Angle(0, 122.149, 0) )
		Create_Armour5( Vector(-7487.454, -6217.465, 0.031), Angle(0, 103.13, 0) )
		Create_Armour5( Vector(-7525.209, -6338.306, 0.031), Angle(0, 77.841, 0) )
		Create_Armour5( Vector(-7649.333, -6422.66, 2.412), Angle(0, 38.34, 0) )
		Create_Armour5( Vector(-7841.968, -6418.334, 16.172), Angle(0, 359.047, 0) )
		Create_Armour5( Vector(-8072.074, -6406.438, 32.608), Angle(0, 358.002, 0) )
		Create_Armour5( Vector(-8287.282, -6395.542, 47.98), Angle(0, 356.33, 0) )
		Create_Armour5( Vector(-8440.903, -6326.705, 64.031), Angle(0, 332.294, 0) )
		Create_Armour5( Vector(-8487.11, -6186.374, 64.031), Angle(0, 304.706, 0) )
	
		local S_BUTTON1 = function()
			Create_EldritchArmour( Vector(-8104.32, -6224.001, 81.509), Angle(0, 179.831, 0) )
			Create_EldritchArmour( Vector(-8196.701, -6222.687, 81.509), Angle(0, 181.921, 0) )
			Create_Backpack( Vector(-8147.375, -6225.578, 81.509), Angle(0, 178.577, 0) )		
		end
	
		AddSecretButton( Vector(-8511.969,-6176.381,117.929), S_BUTTON1 )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)