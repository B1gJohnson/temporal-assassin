
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	file.Delete("TemporalAssassin/Town_2_OffShoot.txt")
	file.Delete("TemporalAssassin/Coast_7_OffShoot.txt")
	
	CreatePlayerSpawn( Vector(904,-8291,0.1), Angle(0,180,0) )
	
	CreateClothesLocker( Vector(-600.86993408203,-6691.4799804688,576.03125), Angle(0,359.79525756836,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		Create_Medkit25( Vector(594.20916748047, -7656.8935546875, -127.96875), Angle(0, 358.95587158203, 0) )
		Create_Medkit25( Vector(593.18640136719, -7713.0483398438, -127.96875), Angle(0, 359.17587280273, 0) )
		Create_PistolAmmo( Vector(-735.51013183594, -7338.9228515625, -127.96875), Angle(0, 325.07556152344, 0) )
		Create_PistolAmmo( Vector(-695.00921630859, -7336.39453125, -127.96875), Angle(0, 304.83544921875, 0) )
		Create_PistolWeapon( Vector(93.095962524414, -6241.5239257813, 576.03125), Angle(0, 176.63594055176, 0) )
		Create_RevolverAmmo( Vector(-59.986988067627, -5659.84765625, 576.03125), Angle(0, 357.25622558594, 0) )
		Create_RevolverWeapon( Vector(-87.001670837402, -5661.3803710938, 576.03125), Angle(0, 359.99084472656, 0) )
		Create_Beer( Vector(696.02056884766, -6115.1357421875, 512.03125), Angle(0, 0.2716064453125, 0) )
		Create_Beer( Vector(775.25939941406, -6114.7607421875, 512.03125), Angle(0, 0.2716064453125, 0) )
		Create_Beer( Vector(843.50860595703, -6114.4370117188, 512.03125), Angle(0, 0.2716064453125, 0) )
		Create_Beer( Vector(911.75793457031, -6114.11328125, 512.03125), Angle(0, 0.2716064453125, 0) )
		Create_Armour( Vector(427.11187744141, -4148.8291015625, 512.03125), Angle(0, 267.33447265625, 0) )
		Create_PortableMedkit( Vector(82.17700958252, -1692.7767333984, 384.03125), Angle(0, 265.43450927734, 0) )
		Create_Armour100( Vector(928.89312744141, -929.38836669922, 240.03125), Angle(0, 270.27447509766, 0) )
		Create_PortableMedkit( Vector(928.15075683594, -993.3974609375, 240.03125), Angle(0, 198.11445617676, 0) )

		CreateSecretArea( Vector(-73.308410644531,-5659.3935546875,579.0625), Vector(-23.026615142822,-23.026615142822,0), Vector(23.026615142822,23.026615142822,33.143737792969) )
		
	else
	
		Create_Medkit25( Vector(589.342, -7791.324, -127.969), Angle(0, 358.328, 0) )
		Create_Medkit25( Vector(591.045, -7722.148, -127.969), Angle(0, 0.209, 0) )
		Create_Medkit25( Vector(590.872, -7652.875, -127.969), Angle(0, 357.91, 0) )
		Create_VolatileBattery( Vector(1556.94, -8359.93, 256.031), Angle(0, 38.498, 0) )
		Create_PistolAmmo( Vector(-728.166, -7340.807, -127.969), Angle(0, 337.678, 0) )
		Create_PistolAmmo( Vector(-749.732, -7327.356, -127.969), Angle(0, 334.334, 0) )
		Create_RevolverWeapon( Vector(-85.915, -5660.986, 576.031), Angle(0, 91.356, 0) )
		Create_RevolverAmmo( Vector(-55.451, -5672.662, 576.031), Angle(0, 18.415, 0) )
		Create_PistolWeapon( Vector(93.587, -6241.579, 576.031), Angle(0, 180.7, 0) )
		Create_Medkit25( Vector(977.728, -6674.259, 512.031), Angle(0, 271.339, 0) )
		Create_Medkit10( Vector(924.603, -6669.867, 512.031), Angle(0, 278.027, 0) )
		Create_Beer( Vector(677.79, -6109.316, 512.031), Angle(0, 8.208, 0) )
		Create_Beer( Vector(673.028, -6066.081, 512.031), Angle(0, 351.488, 0) )
		Create_Beer( Vector(700.951, -6093.447, 512.031), Angle(0, 2.356, 0) )
		Create_Beer( Vector(698.864, -6078.63, 512.031), Angle(0, 355.458, 0) )
		Create_Beer( Vector(728.529, -6085.112, 512.031), Angle(0, 357.967, 0) )
		Create_Armour( Vector(423.273, -4152.174, 512.031), Angle(0, 276.876, 0) )
		Create_ManaCrystal100( Vector(557.204, -3388.636, 256.031), Angle(0, 0.578, 0) )
		Create_Medkit25( Vector(-69.239, -2400.417, 432.031), Angle(0, 179.374, 0) )
		Create_PortableMedkit( Vector(51.773, -1682.998, 384.031), Angle(0, 266.736, 0) )
		Create_Armour100( Vector(934.072, -924.077, 240.031), Angle(0, 247.09, 0) )
		Create_GrenadeAmmo( Vector(910.945, -957.778, 240.031), Angle(0, 256.913, 0) )
	
		CreateSecretArea( Vector(1562.566,-8355.455,260.563), Vector(-33.983,-33.983,0), Vector(33.983,33.983,104.674) )
		CreateSecretArea( Vector(-64,-5661.144,580.563), Vector(-23.199,-23.199,0), Vector(23.199,23.199,33.053) )
		
		local SAFETY_ON = function(self,ply)
			ply:SetSharedBool("Safety_Zone",true)
		end
		local SAFETY_OFF = function(self,ply)
			ply:SetSharedBool("Safety_Zone",false)
		end
		
		CreateTrigger( Vector(686.908,2405.738,-88.424), Vector(-319.853,-319.853,0), Vector(319.853,319.853,1103.27), SAFETY_ON )
		CreateTrigger( Vector(664.14,2897.988,-95.969), Vector(-53.612,-53.612,0), Vector(53.612,53.612,104.24), SAFETY_OFF )
	
	end
	
	local COPS_RETURN = function()
	
		local DONT_BE_A_CUNT = ents.Create("env_global")
		DONT_BE_A_CUNT:SetKeyValue("globalstate","gordon_precriminal")
		DONT_BE_A_CUNT:Spawn()
		DONT_BE_A_CUNT:Activate()
		
		timer.Simple(0.5,function()
			if DONT_BE_A_CUNT and IsValid(DONT_BE_A_CUNT) then
				DONT_BE_A_CUNT:Fire("TurnOff")
				DONT_BE_A_CUNT:Fire("Remove")
				timer.Simple(0.1,function()
					if DONT_BE_A_CUNT and IsValid(DONT_BE_A_CUNT) then
						DONT_BE_A_CUNT:Remove()
					end
				end)
			end
		end)
	
	end
	CreateTrigger( Vector(676.654,-8295.779,-127.969), Vector(-80,-80,0), Vector(80,80,300), COPS_RETURN )
	
	timer.Simple(5,function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_2.txt","DATA") then
			Create_LoreItem( Vector(153.29,-7681.041,256.031), Angle(0,86.602,0), "lore_mthuulra_2", "Lore File Unlocked: Mthuulra #2" )
		end
	end)
	
	local ALLOW_SCENESKIP = function()
		SetSceneSkipAvailable(true)
	end
	
	CreateTrigger( Vector(849.97,2638.103,-58.061), Vector(-41.16,-41.16,0), Vector(41.16,41.16,118.56), ALLOW_SCENESKIP )
	
	local STOP_SCENESKIP = function()
		SetSceneSkipAvailable(false)
	end
	
	CreateTrigger( Vector(715.494,2781.26,-95.969), Vector(-76.22,-76.22,0), Vector(76.22,76.22,126.386), STOP_SCENESKIP )
	
	local REMIND_LORE = function()
		if GetDifficulty2() >= 3 and not file.Exists("temporalassassin/lore/lore_mthuulra_2.txt","DATA") then
			net.Start("DollMessage")
			net.WriteString("There is another book here past this point")
			net.Broadcast()
		end
	end
	
	CreateTrigger( Vector(-19.149,-6486.635,576.031), Vector(-170.365,-170.365,0), Vector(170.365,170.365,402.6), REMIND_LORE )

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)