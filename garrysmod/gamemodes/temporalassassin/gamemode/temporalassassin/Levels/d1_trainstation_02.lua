
if CLIENT then return end

function MAP_LOADED_FUNC()

	RemoveAllEnts("info_player_start")
	
	CreatePlayerSpawn( Vector(-4304,-224,-63), Angle(0,-90,0) )
	
	if GetConVarNumber("TA_DEV_LegacyMaps") >= 1 then
	
		CreateProp(Vector(-4132.03125,-1513.625,416.34375), Angle(0.17578125,-29.6630859375,-0.087890625), "models/temporalassassin/doll/unknown.mdl", true)
		
	else
	
		CreateProp(Vector(-3865.656,-289.687,-63.625), Angle(0.088,-179.956,-0.044), "models/temporalassassin/doll/unknown.mdl", true)
		CreateProp(Vector(-3868.531,-266.906,-67.281), Angle(-23.291,133.813,-40.298), "models/temporalassassin/items/hergift.mdl", true)
		CreateProp(Vector(-2451.031,-3691.594,19.688), Angle(0,129.683,-0.308), "models/props_interiors/furniture_chair03a.mdl", true)
		CreateProp(Vector(-2455.062,-3687.656,21.469), Angle(-0.527,142.69,-0.439), "models/temporalassassin/doll/unknown.mdl", true)
	
		Create_GrenadeAmmo( Vector(-4442.76, -104.474, -31.969), Angle(0, 152.807, 0) )
		Create_PortableMedkit( Vector(-4470.708, -111.326, -31.969), Angle(0, 107.663, 0) )
		Create_PortableMedkit( Vector(-4439.869, -77.863, -31.969), Angle(0, 142.357, 0) )
		Create_SpiritSphere( Vector(-4490.393, 69.921, -31.969), Angle(0, 242.572, 0) )
		Create_Beer( Vector(-3682.035, -2213.01, 80.629), Angle(0, 150.987, 0) )
		Create_Armour200( Vector(-5519.53, -3826.656, 0.031), Angle(0, 180.234, 0) )
		
		local Secret_Button1 = function()
			CreateTeleporter( "Secret1", Vector(-5666.265,-3826.04,0.031), "Secret2", Vector(-4654.038,-3578.865,0.031) )
		end
		
		AddSecretButton( Vector(-4477.659,-3612.985,47.705), Secret_Button1 )
		CreateSecretArea( Vector(-4527.512,-74.265,-31.969), Vector(-64.621,-64.621,0), Vector(64.621,64.621,99.377) )
	
	end

end
hook.Add("InitPostEntity","MAP_LOADED_FUNCHOOK",MAP_LOADED_FUNC)

local SODA_COUNT = 0

function MACHINE_PlayerUse( ply, ent )

	if ent and IsValid(ent) then
	
		local CT = CurTime()
	
		if ent:GetClass() == "func_button" and ent:GetName() == "soda_machine_1" then
		
			if ent.Last_Use and CT < ent.Last_Use then return end
		
			ent.Last_Use = CT + 1.25
		
			ply:ConCommand("-use")
		
			if SODA_COUNT < 3 then
				SODA_COUNT = SODA_COUNT + 1
				Create_Beer( Vector(-3282.2, -2573.333, 64.031), Angle(0, 92.467, 0) )
			end
		
		end
	
	end

end
hook.Add("PlayerUse","MACHINE_PlayerUseHook",MACHINE_PlayerUse)

function MACHINE_NoSodaSpawn( ent )

	if ent and IsValid(ent) then
	
		timer.Simple(0,function()
		
			if IsValid(ent) and ent.GetModel and ent:GetModel() == "models/props_junk/popcan01a.mdl" then
				ent:Remove()
			end
		
		end)
	
	end

end
hook.Add("OnEntityCreated","MACHINE_NoSodaSpawnHook",MACHINE_NoSodaSpawn)