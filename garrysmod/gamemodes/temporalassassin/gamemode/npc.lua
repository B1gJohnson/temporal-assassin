 util.AddNetworkString( "PlayerKilledNPC" ) util.AddNetworkString( "NPCKilledNPC" ) function GM:OnNPCKilled( ent, attacker, inflictor ) if ( ent:GetClass() == "npc_bullseye" || ent:GetClass() == "npc_launcher" ) then return end if ( IsValid( attacker ) && attacker:GetClass() == "trigger_hurt" ) then attacker = ent end if ( IsValid( attacker ) && attacker:IsVehicle() && IsValid( attacker:GetDriver() ) ) then attacker = attacker:GetDriver() end if ( !IsValid( inflictor ) && IsValid( attacker ) ) then inflictor = attacker end if ( IsValid( inflictor ) && attacker == inflictor && ( inflictor:IsPlayer() || inflictor:IsNPC() ) ) then inflictor = inflictor:GetActiveWeapon() if ( !IsValid( attacker ) ) then inflictor = attacker end end local InflictorClass = "worldspawn" local AttackerClass = "worldspawn" if ( IsValid( inflictor ) ) then InflictorClass = inflictor:GetClass() end if ( IsValid( attacker ) ) then AttackerClass = attacker:GetClass() if ( attacker:IsPlayer() ) then net.Start( "PlayerKilledNPC" ) net.WriteString( ent:GetClass() ) net.WriteString( InflictorClass ) net.WriteEntity( attacker ) net.Broadcast() return end end if ( ent:GetClass() == "npc_turret_floor" ) then AttackerClass = ent:GetClass() end net.Start( "NPCKilledNPC" ) net.WriteString( ent:GetClass() ) net.WriteString( InflictorClass ) net.WriteString( AttackerClass ) net.Broadcast() end local HIT_ALT = { HITGROUP_LEFTARM, HITGROUP_RIGHTARM, HITGROUP_LEFTLEG, HITGROUP_RIGHTLEG} function GM:ScaleNPCDamage( npc, hitgroup, dmginfo ) if not IsValid(npc) then return end if not npc then return end if not hitgroup then hitgroup = HITGROUP_GEAR end if not dmginfo then return end if ( hitgroup == HITGROUP_HEAD ) then dmginfo:ScaleDamage( 1.2 ) elseif ( table.HasValue(HIT_ALT,hitgroup) ) then dmginfo:ScaleDamage( 0.5 ) elseif ( hitgroup == HITGROUP_GEAR ) then dmginfo:ScaleDamage( 0.25 ) else dmginfo:ScaleDamage( 0.75 ) end end GLOB_BLOOD_DROPS = {} local BadBlood = { -1, 3, 13, 6} function SpawnBloodDrop( pos, posrand, velocity, randomvelocity, blood ) if not pos then return end if not posrand then return end if not velocity then velocity = Vector(0,0,0) end if not randomvelocity then randomvelocity = 50 end if not blood then blood = 0 end if table.HasValue(BadBlood,blood) then return end local TYPE = "fx_blood_red" if blood == 3 then TYPE = "fx_blood_synth" elseif blood != 0 then TYPE = "fx_blood_yellow" end local Drop = ents.Create(TYPE) Drop:SetCollisionGroup( COLLISION_GROUP_WORLD ) Drop:SetPos(pos + posrand) Drop:Spawn() Drop:Activate() Drop:ApplyPhysicalForce( velocity + VectorRand()*randomvelocity ) end
 
--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]