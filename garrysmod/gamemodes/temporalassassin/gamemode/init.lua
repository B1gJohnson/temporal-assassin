AddCSLuaFile( "cl_init.lua" ) AddCSLuaFile( "shared.lua" ) include( "shared.lua" ) include( "npc.lua" ) include( 'commands.lua' ) local HIT_ALT = { HITGROUP_LEFTARM, HITGROUP_RIGHTARM, HITGROUP_LEFTLEG, HITGROUP_RIGHTLEG} function GM:ScalePlayerDamage( ply, hitgroup, dmginfo ) if not IsValid(ply) then return end if not ply then return end if not dmginfo then return end if not hitgroup then hitgroup = HITGROUP_GEAR end dmginfo:ScaleDamage( 0.8 ) end function GM:TA_EntityTakeDamage_Real( ent, dmginfo ) hook.Call("TA_EntityTakeDamage",nil,ent,dmginfo) return ent, dmginfo end function GM:TA_PostEntityTakeDamage_Real( ent, dmginfo ) hook.Call("TA_PostEntityTakeDamage",nil,ent,dmginfo) return ent, dmginfo end function GM:TA_TemporalRepelled( self, cust, time, adept ) return self, cust, time, adept end function GM:TA_TemporalRepelledEnemies( self, cust, time, adept, REPEL_TABLE ) return self, cust, time, adept, REPEL_TABLE end function GM:TA_ModifyManaBurstGained( self, amount ) return self, amount end function GM:TA_ModifyManaBurstTaken( self, amount ) return self, amount end function GM:TA_ModifyManaBurst( ply, COST, GIVE ) return ply, COST, GIVE end function GM:TA_ModifyFieldKitGiven( ply, amount ) return ply, amount end function GM:TA_ModifyFieldKitTaken( ply, amount ) return ply, amount end function GM:TA_MedkitUsage( ply, GIVE, MAX, MEDKIT_USE, CANNOT_USE ) return ply, GIVE, MAX, MEDKIT_USE, CANNOT_USE end function GM:TA_ModifySpiritEyeGiven( ply, amount ) return ply, amount end function GM:TA_ModifySpiritEyeTaken( ply, amount ) return ply, amount end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2023
]]