
function GM:SpawnMenuEnabled()
	return true
end

function GM:SpawnMenuOpen()
	return true
end

function GM:SpawnMenuOpened()
	self:SuppressHint( "OpeningMenu" )
end

function GM:SpawnMenuClosed()
end

function GM:SpawnMenuCreated(spawnmenu)
end

function GM:ContextMenuEnabled()
	return true
end

function GM:ContextMenuOpen()
	return true
end

function GM:ContextMenuOpened()
	self:SuppressHint( "OpeningContext" )
	self:AddHint( "ContextClick", 20 )
end

function GM:ContextMenuClosed()
end

function GM:ContextMenuCreated()
end

function GM:GetSpawnmenuTools( name )
	return spawnmenu.GetToolMenu( name )
end

function GM:AddSTOOL( category, itemname, text, command, controls, cpanelfunction )
	self:AddToolmenuOption( "Main", category, itemname, text, command, controls, cpanelfunction )
end

function GM:PreReloadToolsMenu()
end

function GM:AddGamemodeToolMenuTabs()

	spawnmenu.AddToolTab( "Main",		"#spawnmenu.tools_tab", "icon16/wrench.png" )
	spawnmenu.AddToolTab( "Utilities",	"#spawnmenu.utilities_tab", "icon16/page_white_wrench.png" )

end

function GM:AddToolMenuTabs()

end

function GM:AddGamemodeToolMenuCategories()

	spawnmenu.AddToolCategory( "Main", "Constraints",	"#spawnmenu.tools.constraints" )
	spawnmenu.AddToolCategory( "Main", "Construction",	"#spawnmenu.tools.construction" )
	spawnmenu.AddToolCategory( "Main", "Poser",			"#spawnmenu.tools.posing" )
	spawnmenu.AddToolCategory( "Main", "Render",		"#spawnmenu.tools.render" )

end

function GM:AddToolMenuCategories()

end

function GM:PopulateToolMenu()
end

function GM:PostReloadToolsMenu()
end

function GM:PopulatePropMenu()

	spawnmenu.PopulateFromEngineTextFiles()

end