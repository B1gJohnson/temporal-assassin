 DEFINE_BASECLASS( "player_default" ) if ( CLIENT ) then CreateConVar( "cl_playercolor", "0.24 0.34 0.41", { FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD }, "The value is a Vector - so between 0-1 - not between 0-255" ) CreateConVar( "cl_weaponcolor", "0.30 1.80 2.10", { FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD }, "The value is a Vector - so between 0-1 - not between 0-255" ) CreateConVar( "cl_playerskin", "0", { FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD }, "The skin to use, if the model has any" ) CreateConVar( "cl_playerbodygroups", "0", { FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD }, "The bodygroups to use, if the model has any" ) end local PLAYER = {} PLAYER.DuckSpeed = 0 PLAYER.UnDuckSpeed = 0 PLAYER.TauntCam = TauntCamera() PLAYER.WalkSpeed = 2000 PLAYER.RunSpeed = 2000 function PLAYER:SetupDataTables() BaseClass.SetupDataTables( self ) end function PLAYER:Loadout() self.Player:RemoveAllAmmo() self.Player:SwitchToDefaultWeapon() end function PLAYER:SetModel() BaseClass.SetModel( self ) local skin = self.Player:GetInfoNum( "cl_playerskin", 0 ) self.Player:SetSkin( skin ) local groups = self.Player:GetInfo( "cl_playerbodygroups" ) if ( groups == nil ) then groups = "" end local groups = string.Explode( " ", groups ) for k = 0, self.Player:GetNumBodyGroups() - 1 do self.Player:SetBodygroup( k, tonumber( groups[ k + 1 ] ) or 0 ) end end function PLAYER:Spawn() BaseClass.Spawn( self ) local col = self.Player:GetInfo( "cl_playercolor" ) self.Player:SetPlayerColor( Vector( col ) ) local col = self.Player:GetInfo( "cl_weaponcolor" ) self.Player:SetWeaponColor( Vector( col ) ) end function PLAYER:ShouldDrawLocal() if ( self.TauntCam:ShouldDrawLocalPlayer( self.Player, self.Player:IsPlayingTaunt() ) ) then return true end end function PLAYER:CreateMove( cmd ) if ( self.TauntCam:CreateMove( cmd, self.Player, self.Player:IsPlayingTaunt() ) ) then return true end end function PLAYER:CalcView( view ) if ( self.TauntCam:CalcView( view, self.Player, self.Player:IsPlayingTaunt() ) ) then return true end end function PLAYER:GetHandsModel() end local JUMPING function PLAYER:StartMove( move ) end function PLAYER:FinishMove( move ) end player_manager.RegisterClass( "player_sandbox", PLAYER, "player_default" ) 

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]