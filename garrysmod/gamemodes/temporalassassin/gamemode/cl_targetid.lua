 local RED = Color(255,100,0,255) local BLUE = Color(0,100,255,255) function GM:HUDDrawTargetID() --[[ local PL = LocalPlayer() local tr = util.GetPlayerTrace( PL ) local trace = util.TraceLine( tr ) if ( !trace.Hit ) then return end if ( !trace.HitNonWorld ) then return end local FONT = "TargetID" local ENT = trace.Entity if not ENT:IsPlayer() then return end draw.SimpleText( ENT:Nick(), FONT, ScrW()*0.5, ScrH()*0.55, BLUE, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER ) ]] end 

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2023
]]