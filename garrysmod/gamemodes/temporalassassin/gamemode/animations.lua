 local RPlayer = debug.getregistry().Player function GM:HandlePlayerJumping( ply, velocity ) return false end function GM:HandlePlayerDucking( ply, velocity ) end function GM:HandlePlayerNoClipping( ply, velocity ) return true end function GM:HandlePlayerVaulting( ply, velocity ) return true end function GM:HandlePlayerSwimming( ply, velocity ) return true end function GM:HandlePlayerLanding( ply, velocity, WasOnGround ) end function GM:HandlePlayerDriving( ply ) return false end function GM:HandleProneAnimation( ply ) end function GM:UpdateAnimation( ply, velocity, maxseqgroundspeed ) end local HAT_WAIT = 0 function GM:CalcMainActivity( ply, velocity ) if not ply then return end ply.CalcIdeal = ACT_IDLE ply.CalcSeqOverride = 1 if ply:DatharonForm() then ply.CalcSeqOverride = 2 if ply:Crouching() then ply.CalcIdeal = ACT_CROUCH ply.CalcSeqOverride = 1 end else if ply.Slide_Enabled or ply.Slide_Animation then ply.CalcIdeal = ACT_RUN_CROUCH ply.CalcSeqOverride = 3 elseif ply:Crouching() then ply.CalcIdeal = ACT_CROUCH ply.CalcSeqOverride = 2 end end if CurTime() >= HAT_WAIT then HAT_WAIT = CurTime() + 1 local HATS = ply:GetInventoryItem("hats") ply:SetBodygroup( 1, HATS ) end if ply.MAIN_SEQUENCE then ply.CalcSeqOverride = ply.MAIN_SEQUENCE end return ply.CalcIdeal, ply.CalcSeqOverride end function RPlayer:NetworkedGesture( slot, enum, loop, weight ) --[[ if SERVER then self:AnimRestartGesture( slot, enum, loop ) if weight then self:AnimSetGestureWeight( slot, weight ) end net.Start("Gesture_Network") net.WriteEntity(self) net.WriteFloat(slot) net.WriteFloat(enum) net.WriteBool(loop) net.WriteFloat(weight or 1) net.Broadcast() else if self == LocalPlayer() and not self:ShouldDrawLocalPlayer() then return end self:AnimRestartGesture( slot, enum, loop ) if weight then self:AnimSetGestureWeight( slot, weight ) end end ]] end 

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]