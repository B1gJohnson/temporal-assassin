AddCSLuaFile( "cl_init.lua" ) AddCSLuaFile( "shared.lua" ) include('shared.lua') PrecacheParticleSystem("Locker_Poof") util.AddNetworkString("SuitLocker_Activate") function CreateClothesLocker( pos, ang ) if not pos then return end if not ang then return end local ENT = ents.Create("temporal_locker") ENT:SetPos( pos ) ENT:SetAngles( ang ) ENT:Spawn() ENT:Activate() return ENT end function ENT:SpawnFunction( ply, tr ) if ( !tr.Hit ) then return end local POS = tr.HitPos local ANG = Angle(0,ply:EyeAngles().y + 180,0) local ENT = ents.Create("temporal_locker") ENT:SetPos( POS ) ENT:SetAngles( ANG ) ENT:Spawn() ENT:Activate() local pos = POS local ang = ANG print("CreateClothesLocker( ".."Vector("..math.Round(pos.x,3)..","..math.Round(pos.y,3)..","..math.Round(pos.z,3).."), Angle("..math.Round(ang.p,3)..","..math.Round(ang.y,3)..","..math.Round(ang.r,3)..") )") return ENT end function ENT:Initialize() local CT = CurTime() self:SetModel( Model("models/TemporalAssassin/Props/Clothes_Locker.mdl") ) self:SetMoveType( MOVETYPE_NONE ) self:SetSolid( SOLID_VPHYSICS ) self:SetCollisionGroup( COLLISION_GROUP_NONE ) self:SetUseType( SIMPLE_USE ) end function ENT:Think() local CT = CurTime() self:NextThink( CT + 1 ) return true end function ENT:Touch(ent) end function ENT:StartTouch(ply) end function ENT:OnRemove() end function ENT:OnTakeDamage( dmginfo ) end function ENT:Use( ply ) if ply and ply:IsPlayer() and ply:Alive() and not ply:MthuulraMode() then net.Start("SuitLocker_Activate") net.Send(ply) end end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]