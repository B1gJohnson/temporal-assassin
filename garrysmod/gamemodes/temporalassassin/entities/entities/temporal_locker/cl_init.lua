include('shared.lua') ENT.Has_Poofed = false if game.GetMap() == "ta_hub" then ENT.Has_Poofed = true end function ENT:Initialize() self:DrawShadow(false) self:SetModelScale(1,0) if game.GetMap() == "ta_hub" then self.Has_Poofed = true self.Poof_Time = 0 end end local TARGET_S = 0 function LockerParam() local TARGET = 100 local RFT = ViewModel_AnimationTime()*180 if SUIT_LOCKER_ENABLED then TARGET = 50 end if TARGET < 90 and TARGET_S > 90 then TARGET_S = math.Clamp( TARGET_S + RFT, 90, 200 ) if TARGET_S >= 100 then TARGET_S = TARGET_S - 100 end elseif TARGET == 50 then if TARGET_S < 50 then TARGET_S = math.Clamp( TARGET_S + RFT, 0, 50 ) elseif TARGET_S > 50 and TARGET_S < 60 then TARGET_S = math.Clamp( TARGET_S - RFT, 50, 60 ) elseif TARGET_S >= 60 then TARGET_S = math.Clamp( TARGET_S + RFT, 90, 200 ) if TARGET_S >= 100 then TARGET_S = TARGET_S - 100 end end elseif TARGET == 100 then TARGET_S = math.Clamp( TARGET_S + RFT, 0, 100 ) end end hook.Add("Think","LockerParamHook",LockerParam) function ENT:Draw() if not self.Has_Poofed then local DIST = LocalPlayer():GetPos():Distance(self:GetPos()) if DIST <= 600 then self.Has_Poofed = true self.Poof_Time = UnPredictedCurTime() + 0.35 ParticleEffect("Locker_Poof", self:GetPos(), self:GetAngles()) sound.Play( Sound("TemporalAssassin/UI/Locker_Appear.wav"), self:GetPos() + Vector(0,0,35), 100, 100, 1 ) self:DrawShadow(true) else self:DrawShadow(false) end else self:DrawShadow(true) if self.Poof_Time and UnPredictedCurTime() >= self.Poof_Time then self:DrawModel() self:SetPoseParameter("P_OpenClose",TARGET_S) end end end function ENT:Think() end function ENT:IsTranslucent() return true end function ENT:OnRemove() end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2023
]]