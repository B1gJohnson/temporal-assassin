include('shared.lua') GLOB_CL_GRENADES = {} function ENT:Initialize() table.insert(GLOB_CL_GRENADES,self) end function ENT:Draw() self:DrawModel() end function ENT:IsTranslucent() return true end function ENT:OnRemove() self:StopParticles() end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]