 --[[ (KITS LOG) I don't know how i came to find this. It's corzo's hat, wearing it makes me feel protected, from what i'm not sure, but when i'm in a bad spot it combusts into black smoke, harming everything around me. It's pretty crazy and i dare not ask him what it's all about, maybe its a form of his hat from other dimensions? It's imbued with alot of souls, Souls of the bad kind, not the good kind incase you're curious. PS: I forgot i was wearing the hat when i came across Corzo again, He gave me an evil eyed look, asking where i got his hat from, I couldn't even explain it but i guess the look on my face told him everything he needed to know, what with dimensional travel and him being connected in a way just like me. He let off a brash kind of sigh as i told him it seemed to protect me, he seemed happy that it has a use and that i'm not just trying to steal his sense of fashion. ]] ENT.Type = "anim" ENT.PrintName = "Corzos Hat" ENT.Author = "Magenta" ENT.Contact = "" ENT.Purpose = "" ENT.Instructions = "" ENT.Category = "Temporal Assassin - Power-Ups" ENT.Spawnable = true ENT.AdminOnly = true ENT.AdminSpawnable = true

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]