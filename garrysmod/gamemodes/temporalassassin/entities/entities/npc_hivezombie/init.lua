AddCSLuaFile( "cl_init.lua" ) AddCSLuaFile( "shared.lua" ) include('shared.lua') function ENT:Initialize() local COMBINE = ents.Create("npc_poisonzombie") COMBINE:SetPos( self:GetPos() ) COMBINE:SetAngles( self:GetAngles() ) COMBINE:SetName( self:GetName() ) COMBINE.NO_DUPES = true COMBINE:Spawn() COMBINE:Activate() COMBINE:MakeHiveZombie() undo.Create("Hive Zombie") undo.AddEntity(COMBINE) undo.SetPlayer(self:GetOwner()) undo.Finish() self:Remove() end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]