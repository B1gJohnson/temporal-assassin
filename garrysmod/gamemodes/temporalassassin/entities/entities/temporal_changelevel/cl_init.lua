 include('shared.lua') function ENT:Initialize() self:SetModelScale(0,0) end function ENT:Draw() end function ChangeLevel_SaveCL_LIB( len ) ShadowGauge_Save() end net.Receive("ChangeLevel_SaveCL",ChangeLevel_SaveCL_LIB) function ChangeLevel_Center_LIB( len ) local POS = net.ReadVector() local MINS = net.ReadVector() local MAXS = net.ReadVector() GOAL_CENTER = POS GOAL_MIN = MINS GOAL_MAX = MAXS end net.Receive("ChangeLevel_Center",ChangeLevel_Center_LIB) function ENT:Think() return true end function ENT:IsTranslucent() return true end function ENT:OnRemove() end function ChangeLevel_CLThink() local PL = LocalPlayer() local PL_POS = (PL.View_Position or PL:EyePos()) local ALPHA = 0 if GOAL_CENTER then local CT = UnPredictedCurTime() local DIST = (PL_POS:Distance(GOAL_CENTER)) if DIST <= 700 then local D_PULSE = math.Clamp( (1 - (DIST/350))*150, 10, 30 ) local M_PULSE = (1 - (DIST/700))*75 local PULSE = math.sin(CT*D_PULSE)*M_PULSE ALPHA = ALPHA + (1 - math.Clamp((DIST/700),0,1))*230 + PULSE - M_PULSE PL.End_Of_Level_BoxAlpha = (1 - math.Clamp((DIST/700),0,1))*230 end end PL.End_Of_Level_Alpha = ALPHA end hook.Add("Think","ChangeLevel_CLThinkHook",ChangeLevel_CLThink) local BOX_MAT = Material("Blank") local LAST_CT = 0 function ChangeLevel_DrawBox() local PL = LocalPlayer() local CT = UnPredictedCurTime() if PL and PL.End_Of_Level_Alpha and PL.End_Of_Level_Alpha > 0 and GOAL_CENTER and GOAL_MIN and GOAL_MAX then local AL = PL.End_Of_Level_BoxAlpha render.SetMaterial(BOX_MAT) render.DrawBox( GOAL_CENTER - Vector(0,0,GOAL_MAX.z*0.5), Angle(0,0,0), GOAL_MIN, GOAL_MAX, Color(50,255,50,AL*0.25) ) end end hook.Add("PostDrawOpaqueRenderables","ChangeLevel_DrawBoxHook",ChangeLevel_DrawBox)

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]