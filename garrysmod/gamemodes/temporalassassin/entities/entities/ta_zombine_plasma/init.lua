AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include('shared.lua')

local SIZE = 0.5

GLOB_TA_GRENADES = {}

function ENT:Initialize()

	local CT = CurTime()
	
	self:SetModel("models/items/combine_rifle_ammo01.mdl")
	
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetCollisionGroup( COLLISION_GROUP_NONE )
	self:SetUseType( SIMPLE_USE )
	
	self.Particle_Trail = CreateParticleEffect( "TA_ZombinePlasma_Trail", self:GetBoxCenter(), Angle(0,0,0), false, self )
	
	if IsValid(self:GetOwner()) then
	
		self.MyOwner = self:GetOwner()
		
		if self:GetOwner():IsNPC() then
			self.WasNPC = true
		end
		
	end
	
	if not self.Timer then
		self.Timer = CT + 3
	end
	
	if not self.NoThrowSound then
		self:EmitSound( ")TemporalAssassin/Weapons/EnemyWeapons/ZombinePlasma/Nade_Draw.wav", 90, 100, 1, CHAN_STATIC )
	end
	
	table.insert(GLOB_TA_GRENADES,self)
	
	local PHYS = self:GetPhysicsObject()
	PHYS:SetMass(10)
	PHYS:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
	
end

local EXPLODE_SOUND = {
Sound(")TemporalAssassin/Weapons/EnemyWeapons/ZombinePlasma/Nade_Explode.wav")}

function ENT:Explode()

	if self.HasExploded then return end
	
	self.HasExploded = true

	if self.KillOwner then
		local OWN = self:GetOwner()
		if OWN and IsValid(OWN) and not OWN:IsPlayer() then
			OWN:SetHealth(1)
		end
	end
	
	local DMG = 200	
	local SND = table.Random(EXPLODE_SOUND)
	local POS, OWN = self:GetBoxCenter(), self:GetOwner()
	
	if IsValid(OWN) and OWN:IsPlayer() then
		DMG = 210
	end
	
	if self.PlayerBuff then
		DMG = 220
	end
	
	if self.CUSTOM_DAMAGE then
		DMG = self.CUSTOM_DAMAGE
	end
	
	sound.Play( SND, self:GetPos(), 160, math.random(95,105), 1, CHAN_STATIC )
	sound.Play( SND, self:GetPos(), 160, math.random(95,105), 1, CHAN_STATIC )
	
	if self.DelayedParticle then
		timer.Simple(0,function()
			if IsValid(self) then
				CreateParticleEffect( "FutureExplosive_Grenade_Explode_Big", self:GetBoxCenter(), Angle(0,0,0), 20 )
			end
		end)
	else
		CreateParticleEffect( "FutureExplosive_Grenade_Explode_Big", self:GetBoxCenter(), Angle(0,0,0), 20 )
	end
	
	timer.Simple(0,function()
		if POS and OWN then
			CreateSourceExplosion( POS, DMG, OWN )
		end
	end)
	
	self:Remove()

end

function ENT:OnTheGround()

	local TR = {}
	TR.start = self:GetPos()
	TR.endpos = TR.start - Vector(0,0,3)
	TR.mask = MASK_SOLID
	TR.filter = {self}
	TR.mins = Vector(-0.1,-0.1,-0.1)
	TR.maxs = Vector(0.1,0.1,0.1)
	local Trace = util.TraceHull(TR)
	
	if Trace.Hit then
		return true
	end
	
	return false

end

function ENT:Think()

	local CT = CurTime()
	
	if TEMPORAL_SHIFT_ON then
	
		self.FROZEN_SOUND = true
		self.Timer = CT + 2.3
			
		if self.OpenSound then
			self.OpenSound:Stop()
			self.OpenSound = nil
		end
		
	return end
	
	if self.FROZEN_SOUND then
		self.FROZEN_SOUND = nil
		self:CreateDangerSound( 500, 2.3 )
	end

	if self.Timer and CT >= self.Timer then
		self.Timer = nil
		self:Explode()
	return end
	
	self:NextThink( CT )
	
	return true
	
end

function ENT:Touch(ent)
	
end

function ENT:OnRemove()

	if self.OpenSound then
		self.OpenSound:Stop()
		self.OpenSound = nil
	end
	
	if self.Particle_Trail and IsValid(self.Particle_Trail) then
		self.Particle_Trail:Fire("Kill",nil,0)
	end
	
	for _,v in pairs (GLOB_TA_GRENADES) do
	
		if v and IsValid(v) and v == self then
			table.remove(GLOB_TA_GRENADES,_)
		end
	
	end

end

function ENT:KickFunc()

	local CT = CurTime()
	
	self.Timer_Impact = true
	self.OpenChange = true
	self.Timer = CT + 2
		
	if self.OpenSound then
		self.OpenSound:Stop()
		self.OpenSound = nil
	end
		
	self.OpenSound = CreateSound( self, ")TemporalAssassin/Weapons/EnemyWeapons/Grenade/Grenade_Open.wav" )
	self.OpenSound:PlayEx(0.5,100)
	self:SetBodygroup(0,1)
	self:CreateDangerSound( 500, 2 )
		
	self.DMG_DEL = CT + 0.1
	self.SHAKE_DEL = CT + 0.4

end

function ENT:OnTakeDamage( dmginfo )
	
	if (dmginfo:GetDamageType() == DMG_CLUB or dmginfo:GetDamageType() == DMG_SLASH) then
	else
		self:SetOwner(dmginfo:GetAttacker())
		self:Explode()
	end
	
end

function ENT:Use( ply )

end