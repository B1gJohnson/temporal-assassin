include('shared.lua') function ENT:Initialize() self:SetModelScale(1.05,0) end function ENT:Draw() self:DrawModel() end function ENT:IsTranslucent() return true end function ENT:OnRemove() local TABLE = player.GetAll() table.Add(TABLE,GLOBAL_NPCS) table.Add(TABLE,GLOB_GORE_GIBEFFECTS) local TABLE_NPCS = player.GetAll() table.Add(TABLE_NPCS,GLOBAL_NPCS) local CT = CurTime() for _,npc in pairs (TABLE_NPCS) do local D_REQ = 400 local V_POS = self:GetPos() if IsValid(npc) and npc:GetBoxCenter():Distance(V_POS) <= D_REQ and not npc.IsDead then npc.Dissolve_Damage = CT + 0.5 end end self:StopParticles() end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]