AddCSLuaFile( "cl_init.lua" ) AddCSLuaFile( "shared.lua" ) include('shared.lua') PrecacheParticleSystem("CosmicStatue_Idle") PrecacheParticleSystem("CosmicStatue_IdleVoid") function CreateCosmicStatue( pos, ang, loreunlock, loreunlock2 ) if not pos then return end if not ang then return end local ENT = ents.Create("cosmic_statue") ENT:SetPos( pos ) ENT:SetAngles( ang ) ENT.Lore_Unlock = loreunlock ENT.Lore_Unlock2 = loreunlock2 ENT:Spawn() ENT:Activate() return ENT end function ENT:Initialize() local CT = CurTime() self:SetModel( Model("models/TemporalAssassin/Props/Altar.mdl") ) self:SetMoveType( MOVETYPE_NONE ) self:SetSolid( SOLID_VPHYSICS ) self:SetCollisionGroup( COLLISION_GROUP_NONE ) self:SetUseType( SIMPLE_USE ) end local STATUE_TR_OUTPUT = {} local STATUE_TR = {} STATUE_TR.mask = MASK_PLAYERSOLID STATUE_TR.mins = Vector(-6,-6,-4) STATUE_TR.maxs = Vector(6,6,4) STATUE_TR.output = STATUE_TR_OUTPUT function ENT:Think() local CT = CurTime() self:SetSharedString("Lore_Unlock",self.Lore_Unlock) self:SetSharedString("Lore_Unlock2",self.Lore_Unlock2) for _,v in pairs (player.GetAll()) do if v and IsValid(v) and v:Alive() then STATUE_TR.start = self:GetBoxCenter() STATUE_TR.endpos = v:GetBoxCenter() STATUE_TR.filter = self util.TraceHull(STATUE_TR) local DIST = 500 if IsValid(STATUE_TR_OUTPUT.Entity) and STATUE_TR_OUTPUT.Entity == v then DIST = v:EyePos():Distance( self:GetBoxCenter() ) end if DIST < 200 and not v.CosmicStatue_Safety then v:SetSharedBool("CosmicStatue_Safety",true) elseif DIST >= 200 and v.CosmicStatue_Safety then v:SetSharedBool("CosmicStatue_Safety",false) end end end self:NextThink( CT + 0.1 ) return true end function ENT:Touch(ent) end function ENT:StartTouch(ply) end function ENT:OnRemove() end function ENT:OnTakeDamage( dmginfo ) end function ENT:Use( ply ) end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]