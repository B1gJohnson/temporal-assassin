 --[[ (KITS LOG) I came across the man with the shrouded face again, I didn't expect him to be a drinker of beer, he offered me one but gave me this weird smirk before asking me how old i am and when i responded with 32 he couldn't stop snickering. Stuff doesn't taste bad though, however after this little interaction with him it's been showing up all over the place. I forgot to mention i finally asked him his name, although he didn't seem to mind me asking, he told me "You can call me Corzo". When someone tells you you can call them something it usually means it's not their real name, but i'll take what i can get, even though i swear i've spyed him drinking from a bottle of tequila of the same name. ]] ENT.Type = "anim" ENT.PrintName = "Beer - 5 (Overheal)" ENT.Author = "Magenta" ENT.Contact = "" ENT.Purpose = "" ENT.Instructions = "" ENT.Category = "Temporal Assassin - HP/Armour" ENT.Spawnable = true ENT.AdminOnly = true ENT.AdminSpawnable = true

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]