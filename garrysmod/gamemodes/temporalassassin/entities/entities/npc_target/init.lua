AddCSLuaFile( "cl_init.lua" ) AddCSLuaFile( "shared.lua" ) include('shared.lua') ENT.m_fMaxYawSpeed = 200 ENT.m_iClass = CLASS_CITIZEN_REBEL AccessorFunc( ENT, "m_iClass", "NPCClass" ) AccessorFunc( ENT, "m_fMaxYawSpeed", "MaxYawSpeed" ) PrecacheParticleSystem("Echo_Decoy") function ENT:Initialize() self:SetHealth(999999999) self:SetModel( "models/dav0r/hoverball.mdl" ) self:SetHullType( HULL_TINY ) self:SetHullSizeNormal() self:SetMoveType( MOVETYPE_NONE ) self:SetCollisionGroup( COLLISION_GROUP_WORLD ) self:SetCollisionBounds( Vector(-5,-5,-5), Vector(5,5,5) ) self:SetSolid( SOLID_BBOX ) end function ENT:OnTakeDamage(dmg) return false end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]