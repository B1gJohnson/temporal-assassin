include('shared.lua') ENT.RenderGroup = RENDERGROUP_OPAQUE ENT.Particle_Think = 0 ENT.Next_Shot = 0 function ENT:Initialize() self:SetModelScale( 0, 0 ) self:SetCollisionGroup( COLLISION_GROUP_WORLD ) self.Next_Shot = CurTime() + math.random(1,3) end function ENT:Draw() end function ENT:IsTranslucent() return true end function ENT:Think() local CT = CurTime() local CROUCH = LocalPlayer():Crouching() if not self.FAKE_MODEL then local ANG = Angle(0,LocalPlayer():EyeAngles().y,0) self.FAKE_MODEL = ClientsideModel( "models/temporalassassin/player_alt.mdl", RENDERGROUP_TRANSLUCENT ) if CROUCH then self.FAKE_MODEL:SetPos( self:GetPos() - Vector(0,0,17) - ANG:Forward()*14 ) else self.FAKE_MODEL:SetPos( self:GetPos() - Vector(0,0,56) - ANG:Forward()*10 ) end self.FAKE_MODEL:SetAngles( ANG ) self.FAKE_MODEL:SetMaterial("models/shadertest/shader3") self.FAKE_MODEL:SetRenderMode( RENDERMODE_TRANSALPHA ) self.FAKE_MODEL:SetColor( Color(46,176,176,255) ) self.FAKE_MODEL:Spawn() self.FAKE_MODEL:Activate() if CROUCH then self.FAKE_MODEL:SetSequence(2) end end if self.Particle_Think and CT >= self.Particle_Think then self.Particle_Think = CT + 0.5 ParticleEffectAttach( "Echo_Decoy", PATTACH_POINT_FOLLOW, self.FAKE_MODEL, 1 ) end if self.Next_Shot and CT >= self.Next_Shot then UpdateTraceToFilter() self.Next_Shot = CT + math.Rand(1.5,2.5) local M_POS = self:GetPos() local MY_TARGET = false local DIST = 1000 for _,v in pairs (GLOBAL_NPCS) do if v and IsValid(v) and (v:IsNPC() or v:IsNextBot()) and v != self and v != LocalPlayer() and not v.IsDead and not v.SV_IsDead then if v:GetBoxCenter():Distance(M_POS) < DIST and TraceToEnt( self:GetPos(), v, 20 ) then DIST = v:GetBoxCenter():Distance(M_POS) MY_TARGET = v end end end if MY_TARGET and IsValid(MY_TARGET) then local ORIGIN = M_POS + VectorRand()*10 local ANG = (MY_TARGET:GetBoxCenter() - ORIGIN):GetNormal() ParticleEffect( "Echo_Bullet", ORIGIN, ANG:Angle() ) timer.Simple(0.5,function() if self and IsValid(self) then local BULLET_TABLE = {} BULLET_TABLE[1] = ORIGIN BULLET_TABLE[27] = 0 BULLET_TABLE[2] = ANG BULLET_TABLE[3] = 80 BULLET_TABLE[4] = 50 BULLET_TABLE[15] = 1 BULLET_TABLE[5] = (0) BULLET_TABLE[6] = (1500) BULLET_TABLE[7] = (0) BULLET_TABLE[12] = 0 BULLET_TABLE[9] = 0 BULLET_TABLE[13] = self BULLET_TABLE[28] = true BULLET_TABLE[11] = "ClientBullet_MagnumCop" BULLET_TABLE[10] = 0 GLOB_FireClientBullet( BULLET_TABLE, self ) sound.Play( Sound("TemporalAssassin/Misc/EchoDecoy_Shoot.wav"), M_POS, 100, 100, 1 ) end end) end end end function ENT:OnRemove() if self.FAKE_MODEL and IsValid(self.FAKE_MODEL) then self.FAKE_MODEL:Remove() end end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2023
]]