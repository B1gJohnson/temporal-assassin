AddCSLuaFile( "cl_init.lua" ) AddCSLuaFile( "shared.lua" ) include('shared.lua') function ENT:SpawnFunction( ply, tr ) if ( !tr.Hit ) then return end;tr = ItemSpawnTrace(ply) local POS = tr.HitPos + tr.HitNormal*16 local ANG = Angle(0,ply:EyeAngles().y + 180,0) local BOX = CreateSupplyBox( POS, ANG, false, false ) local VEC = tostring("Vector("..math.Round(POS.x,2)..","..math.Round(POS.y,2)..","..math.Round(POS.z,2)..")") local ANGL = tostring("Angle("..math.Round(ANG.p,2)..","..math.Round(ANG.y,2)..","..math.Round(ANG.r,2)..")") local STRING = tostring("CreateSupplyBox( "..VEC..", "..ANGL..", false, false )") print(STRING) return BOX end function ENT:Initialize() end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]