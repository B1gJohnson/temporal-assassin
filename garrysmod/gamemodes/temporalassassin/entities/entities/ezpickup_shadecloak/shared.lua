 --[[ (KITS LOG) Once donned by a little guy in a kingdom from another dimension far beyond anything like this one. It's imbued with power from an entity known as 'The Abyss', the cloak feels soft, yet oddly pungent and manages to keep its sheen in pure darkness. It'll increase my mobility by a hell of a lot thanks to the power that resides still inside. Keep on going little ghost. ]] ENT.Type = "anim" ENT.PrintName = "Shade Cloak" ENT.Author = "Magenta" ENT.Contact = "" ENT.Purpose = "" ENT.Instructions = "" ENT.Category = "Temporal Assassin - Power-Ups" ENT.Spawnable = true ENT.AdminOnly = true ENT.AdminSpawnable = true

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]