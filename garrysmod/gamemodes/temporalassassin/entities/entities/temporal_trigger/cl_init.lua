include('shared.lua') function ENT:Initialize() self:SetModelScale(0,0) end function ENT:Draw() end function ENT:Think() end function ENT:IsTranslucent() return true end function ENT:OnRemove() end local BOX_MAT = Material("Blank") local LAST_CT = 0 local TRIGGER_TESTS = {} function TriggerTests_GetInfo_LIB( len ) TRIGGER_TESTS = net.ReadTable() end net.Receive("TriggerTests_GetInfo",TriggerTests_GetInfo_LIB) local CLASS_COL = {} CLASS_COL["temporal_trigger"] = Color(255,255,0,6) CLASS_COL["temporal_secretnotif"] = Color(0,200,200,6) CLASS_COL["temporal_cosmicrealm"] = Color(100,0,200,6) function TriggerTests_DrawBox() if #TRIGGER_TESTS <= 0 then return end local PL = LocalPlayer() local CT = UnPredictedCurTime() if GetConVarNumber("TA_DEV_DeveloperMode") < 1 then TRIGGER_TESTS = {} return end render.SetMaterial(BOX_MAT) for _,v in pairs (TRIGGER_TESTS) do if v and v[1] and v[2] and v[3] and v[4] and v[5] then local COL = CLASS_COL["temporal_trigger"] if CLASS_COL[v[5]] then COL = CLASS_COL[v[5]] end render.DrawBox( v[1] + Vector(0,0,0.1), v[4], v[2], (v[3] or Angle(0,0,0)), COL ) else table.remove(TRIGGER_TESTS,_) end end end hook.Add("PostDrawOpaqueRenderables","TriggerTests_DrawBoxHook",TriggerTests_DrawBox)

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]