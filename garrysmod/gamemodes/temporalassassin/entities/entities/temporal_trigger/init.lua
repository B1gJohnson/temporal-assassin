AddCSLuaFile( "cl_init.lua" ) AddCSLuaFile( "shared.lua" ) include('shared.lua') --[[ Entity Stuff ]] GLOB_TRIGGER_ENTS = {} util.AddNetworkString("TriggerTests_GetInfo") timer.Create("Trigger_InfoOutput", 1, 0, function() if GetConVarNumber("TA_DEV_DeveloperMode") >= 1 then local TABLE = {} for _,v in pairs (GLOB_TRIGGER_ENTS) do if v and IsValid(v) then local INFO = {v:GetPos(),v.MINS,v.MAXS,Angle(0,v:GetAngles().y,0),v:GetClass()} table.insert(TABLE,INFO) end end net.Start("TriggerTests_GetInfo") net.WriteTable(TABLE) net.Broadcast() end end) function ENT:Initialize() local CT = CurTime() self:SetModel("models/props_junk/garbage_newspaper001a.mdl") self:SetNoDraw(true) self:SetRenderMode(RENDERMODE_TRANSALPHA) self:SetColor(Color(255,255,255,0)) self:SetMoveType( MOVETYPE_NONE ) self:SetSolid( SOLID_BBOX ) self:SetCollisionGroup( COLLISION_GROUP_WORLD ) self:SetUseType( SIMPLE_USE ) self:SetTrigger(true) if self.MINS and self.MAXS then if self.NewStyle then local BOX_POS = (self.MAXS - ((self.MAXS - self.MINS) / 2)) local w = self.MAXS.x - self.MINS.x local l = self.MAXS.y - self.MINS.y local h = self.MAXS.z - self.MINS.z local min = Vector(0 - (w / 2), 0 - (l / 2), 0 - (h / 2)) local max = Vector(w / 2, l / 2, h / 2) self:SetPos( BOX_POS ) self:SetCollisionBounds( min, max ) self.MINS = min self.MAXS = max else self:SetCollisionBounds( self.MINS, self.MAXS ) end end table.insert(GLOB_TRIGGER_ENTS,self) end function ENT:Think() local CT = CurTime() self:NextThink( CT + 1 ) return true end function ENT:Touch(ent) end function ENT:StartTouch(ply) if IsValid(ply) and ply:IsPlayer() and ply:Alive() and not ply:MthuulraMode() and GetConVarNumber("TA_DEV_DeveloperMode") < 1 then if self.Trigger_Func then self:Trigger_Func(ply) if not self.CustomRemove then self:Remove() end end end end function ENT:OnRemove() for _,v in pairs (GLOB_TRIGGER_ENTS) do if v and IsValid(v) and v == self then table.remove(GLOB_TRIGGER_ENTS,_) end end end function ENT:OnTakeDamage( dmginfo ) end function ENT:Use( ply ) end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]