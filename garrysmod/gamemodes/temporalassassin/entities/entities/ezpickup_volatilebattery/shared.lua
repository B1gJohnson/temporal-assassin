 --[[ (KITS LOG) Let me tell you about the "Volatile Battery" and how it changed my life, for one easy payment of red lightning to the face of whatever the hell stands in your way. This thing changes the A.R.C blades powered up mode to use red lightning instead of the boring old blue, it explodes on contact and it lessens the impact of stuff on my block even more than the normal version, It's a straight up upgrade and it dishes out even more pain when attacking up close too. ]] ENT.Type = "anim" ENT.PrintName = "Volatile Battery" ENT.Author = "Magenta" ENT.Contact = "" ENT.Purpose = "" ENT.Instructions = "" ENT.Category = "Temporal Assassin - Power-Ups" ENT.Spawnable = true ENT.AdminOnly = true ENT.AdminSpawnable = true

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]