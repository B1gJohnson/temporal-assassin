AddCSLuaFile( "cl_init.lua" ) AddCSLuaFile( "shared.lua" ) include('shared.lua') GLOB_TRIGGER_ENTS = {} --[[ Entity Stuff ]] function ENT:Initialize() local CT = CurTime() self:SetModel("models/props_junk/garbage_newspaper001a.mdl") self:SetNoDraw(true) self:SetRenderMode(RENDERMODE_TRANSALPHA) self:SetColor(Color(255,255,255,0)) self:SetMoveType( MOVETYPE_NONE ) self:SetSolid( SOLID_BBOX ) self:SetCollisionGroup( COLLISION_GROUP_WORLD ) self:SetUseType( SIMPLE_USE ) if self.MINS and self.MAXS then self:SetCollisionBounds( self.MINS, self.MAXS ) end self:SetTrigger(true) table.insert(GLOB_TRIGGER_ENTS,self) end function ENT:Think() local CT = CurTime() self:NextThink( CT + 1 ) return true end function ENT:Touch(ent) end util.AddNetworkString("TEMPORAL_GetGameEnd") function ENT:StartTouch(ply) if IsValid(ply) and ply:IsPlayer() and GetConVarNumber("TA_DEV_DeveloperMode") < 1 and ply:Alive() and not ply:MthuulraMode() then SaveLevelCompleteAchievement() if ply:GetInventoryItem("hats") >= 1 then ply:Unlock_TA_LoreFile( "item_corzoshat_2", "Lore File Unlocked: Corzos Hat #2" ) ply:IncreaseAchievementProgress("KEEPING_IT_CLEAN",1) end net.Start("TEMPORAL_GetGameEnd") net.WriteTable(self.TEXT) net.Broadcast() for _,v in pairs (player.GetAll()) do v:GodEnable() v.GOD_ON = true v:DisableControls(true) v:Freeze(true) TAS:SetTemporalMana(v,0) STATS.AddStat( "Chapters_Completed", v, 1 ) STATS.AddStat( "Levels_Completed", v, 1 ) STATS.SaveStats(v) end if not GLOB_SAFE_GAMEEND and not SIN_COUNT_GOAL then file.Delete("temporalassassin/map_load.txt","DATA") for _,v in pairs (file.Find("temporalassassin/items/*","DATA")) do file.Delete( tostring("temporalassassin/items/"..v) ) end for _,v in pairs (file.Find("temporalassassin/resources/*","DATA")) do file.Delete( tostring("temporalassassin/resources/"..v) ) end end timer.Simple( self.DELAY, function() RunConsoleCommand("changelevel","ta_hub") end) for _,v in pairs (ents.GetAll()) do if v and IsValid(v) and (v:IsNPC() or v:IsNextBot()) then v:Remove() end end self:Remove() end end function ENT:OnRemove() for _,v in pairs (GLOB_TRIGGER_ENTS) do if v and IsValid(v) and v == self then table.remove(GLOB_TRIGGER_ENTS,_) end end end function ENT:OnTakeDamage( dmginfo ) end function ENT:Use( ply ) end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]