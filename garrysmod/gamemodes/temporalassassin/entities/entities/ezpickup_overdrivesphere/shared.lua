 --[[ (KITS LOG) Not sure how this is even possible, or how its conceived but it has a hell of a kick. I've taken to calling it an "Overdrive Sphere" due to its circular look, durr. But the main take away from this thing is what feels like a shock to my body as i'm invigorated with heaps of energy, i can block forever i can slide for as long as i can, guard as much as i please without getting tired. I can feel my body healing itself incredibly quickly and the handling of my weapons is incredible. This feeling is addictive, but i suppose most things like this would be, Holy hell. ]] ENT.Type = "anim" ENT.PrintName = "Overdrive Sphere" ENT.Author = "Magenta" ENT.Contact = "" ENT.Purpose = "" ENT.Instructions = "" ENT.Category = "Temporal Assassin - Power-Ups" ENT.Spawnable = true ENT.AdminOnly = true ENT.AdminSpawnable = true

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]