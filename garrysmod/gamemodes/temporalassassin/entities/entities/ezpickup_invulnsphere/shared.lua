 --[[ (KITS LOG) I was never taught this kind of stuff, it sorta just came to me by accident out of curiosity, The essence of a yokai is strong, extremely powerful in the wrong hands and just by looking at it you can see the beasts eyes staring at you even though its simply the remains of it. I can imbue myself with it to give myself nye invulnerability to anything that wants to hurt me, After its over i can't help but feel a part of me won't ever be the same again though, it's a strange thing. ]] ENT.Type = "anim" ENT.PrintName = "Yokai Essence" ENT.Author = "Magenta" ENT.Contact = "" ENT.Purpose = "" ENT.Instructions = "" ENT.Category = "Temporal Assassin - Power-Ups" ENT.Spawnable = true ENT.AdminOnly = true ENT.AdminSpawnable = true

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]