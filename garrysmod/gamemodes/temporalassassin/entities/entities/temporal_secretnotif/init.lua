AddCSLuaFile( "cl_init.lua" ) AddCSLuaFile( "shared.lua" ) include('shared.lua') GLOB_TRIGGER_ENTS = {} GLOB_SECRET_ENTS = {} local SECRETS_FOUND = 0 local SECRETS_TOTAL = 0 function SEC_PreCleanupMap() SECRETS_FOUND = 0 SECRETS_TOTAL = 0 end hook.Add("PreCleanupMap","SEC_PreCleanupMapHook",SEC_PreCleanupMap) --[[ Entity Stuff ]] function ENT:Initialize() local CT = CurTime() self:SetModel("models/props_junk/garbage_newspaper001a.mdl") self:SetNoDraw(true) self:SetRenderMode(RENDERMODE_TRANSALPHA) self:SetColor(Color(255,255,255,0)) self:SetMoveType( MOVETYPE_NONE ) self:SetSolid( SOLID_BBOX ) self:SetCollisionGroup( COLLISION_GROUP_WORLD ) self:SetUseType( SIMPLE_USE ) if self.MINS and self.MAXS then self:SetCollisionBounds( self.MINS, self.MAXS ) end self:SetTrigger(true) for _,v in pairs (GLOB_SECRET_ENTS) do if not v or (v and not IsValid(v)) then table.remove(GLOB_SECRET_ENTS,_) end end table.insert(GLOB_TRIGGER_ENTS,self) table.insert(GLOB_SECRET_ENTS,self) SECRETS_TOTAL = SECRETS_TOTAL + 1 end function AddSecretTotal() SECRETS_TOTAL = SECRETS_TOTAL + 1 end function ReduceSecretTotal() SECRETS_TOTAL = SECRETS_TOTAL - 1 end function AddSecretFound() SECRETS_FOUND = SECRETS_FOUND + 1 end function ENT:Think() local CT = CurTime() self:NextThink( CT + 1 ) return true end function ENT:Touch(ent) end util.AddNetworkString("TemporalAssassin_SecretNotif") util.AddNetworkString("TEMPORALHUD_GetSecrets") function Secrets_Update() net.Start("TEMPORALHUD_GetSecrets") net.WriteFloat( SECRETS_FOUND ) net.WriteFloat( SECRETS_TOTAL ) net.Broadcast() end timer.Create("Send_Secret_Counts", 5, 0, Secrets_Update) function ENT:StartTouch(ply) if IsValid(ply) and ply:IsPlayer() and ply:Alive() and not ply:MthuulraMode() and GetConVarNumber("TA_DEV_DeveloperMode") < 1 then net.Start("TemporalAssassin_SecretNotif") if self.CText then net.WriteString(self.CText) else net.WriteString("YOU FOUND A SECRET") end if self.CSound then net.WriteString(self.CSound) else local RNG_1 = math.Rand(0,100) local SOUN = "TemporalAssassin/Secrets/Secret_Find.wav" if RNG_1 <= 5 then SOUN = "TemporalAssassin/Secrets/Secret_Find2.wav" end net.WriteString(SOUN) end net.Send(ply) SECRETS_FOUND = SECRETS_FOUND + 1 STATS.AddStat( "Secrets_Found", ply, 1 ) Secrets_Update() self:Remove() end end function ENT:OnRemove() for _,v in pairs (GLOB_TRIGGER_ENTS) do if v and IsValid(v) and v == self then table.remove(GLOB_TRIGGER_ENTS,_) end end for _,v in pairs (GLOB_SECRET_ENTS) do if v and IsValid(v) and v == self then table.remove(GLOB_SECRET_ENTS,_) end end end function ENT:OnTakeDamage( dmginfo ) end function ENT:Use( ply ) end

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]