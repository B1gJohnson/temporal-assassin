 --[[ (KITS LOG) Now this is what you call power, a purified and untouched soul of a yokai. This stuff can save even the most fucked up person from death as long as they have the know how of how to absorb the stuff, it protects them as well as recovering their health, body and mind from all kinds of damage and ailments. Never leave home without one as they say. ]] ENT.Type = "anim" ENT.PrintName = "Yokai Soul" ENT.Author = "Magenta" ENT.Contact = "" ENT.Purpose = "" ENT.Instructions = "" ENT.Category = "Temporal Assassin - HP/Armour" ENT.Spawnable = true ENT.AdminOnly = true ENT.AdminSpawnable = true

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]