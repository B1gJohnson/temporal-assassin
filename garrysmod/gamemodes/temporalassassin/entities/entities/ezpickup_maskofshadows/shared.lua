 --[[ (KITS LOG) I don't know what this is, but just looking at it gives me chills, it's as if some unknown force or entity is staring back at me through the mist, just begging me to wear it. I ran across the man with the shrouded face while keeping hold of one of these to see if i could get his thoughts, he didn't say much but i could see him shudder when he thought about it, there's definatly more to this thing than he lets on, must not be very nice if even he has a reaction like that. ]] ENT.Type = "anim" ENT.PrintName = "Mask Of Shadows" ENT.Author = "Magenta" ENT.Contact = "" ENT.Purpose = "" ENT.Instructions = "" ENT.Category = "Temporal Assassin - Power-Ups" ENT.Spawnable = true ENT.AdminOnly = true ENT.AdminSpawnable = true

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]