 --[[ (KITS LOG) The same thing as the smaller crystals i've been finding, but this one practically restores my entire stock, i don't know where these things come from, but i guess that's the norm when you deal with inter-dimensional travel and what not. ]] ENT.Type = "anim" ENT.PrintName = "Great Temporal Crystal - 100" ENT.Author = "Magenta" ENT.Contact = "" ENT.Purpose = "" ENT.Instructions = "" ENT.Category = "Temporal Assassin - Power-Ups" ENT.Spawnable = true ENT.AdminOnly = true ENT.AdminSpawnable = true

--[[
Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not
created by myself and is used (hopefully, sorry in advance) under the copyright law
of recreational purposes without profit.

The credits for all of those belong to their original authors and creators who's work being
in public domain has allowed me to make this game.

Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
My Animations and Sound Effects may be repurposed for personal projects but please let me
know before hand, Credit where credit is due of course.

However my Programming and Particle Effects should not be repurposed or used under any other context.
Please respect my choice on this matter.

Magenta Iris © 2018-2022
]]