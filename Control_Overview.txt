
- THIS OVERVIEW MAY CONTAIN SOME MINOR SPOILERS, IF YOU WISH TO FIGURE THINGS OUT FOR YOURSELF, PLEASE DO NOT CONTINUE READING! -

Kitsune Wilde Controls:
	Attack/Alt-Attack/Weapon Ability: Use active weapon in a variety of ways
	Crouch: Duck
	Jump (Tap): Short Hop
	Jump (Held): Full Hop
	Jump (Tap near wall in mid-air): Wall Jump (View angle based)
	Sprint (Moving): Dodge
	Sprint (Moving + Held): Dodge then Slide
	Sprint (Held without Moving): Slide instantly with no dodge cost/requirements (Can be queued up in the air too)
	Temporal Guard: Block Damage
	Temporal Guard (Release quickly after damage): Temporal Repel
	Temporal Guard + Attack: Mana Burst (If gauge is full)
	Temporal Repel (Offensive Guard Active): Fire Arm Flux (Reload all guns)
	Temporal Summon (Tap): Summon selected creature
	Temporal Summon (Held): Pet selected creature (Applies affection bonus)
	Kick (Tap): Kick
	Kick (Hold): Charge up
	Kick (Release): Charged Kick
	Kick (Release Shortly after Dodging): Drop Kick
	Swap Summon: Switch currently selected creature
	Use Medkit: Instantly heal 50 HP & Some Stamina with a portable medkit (Requires atleast 1)
	Equip Grenade: Equip throwable grenades (Requires atleast 1)
	Equip Phone: Equip kits phone (Type in chat to use cheats)
	Play Music: Play a random song from the current genre selected in the F4 options menu
	Stop Music: Stop the current music playing
	Pause Game: Pauses the darn game
	
Weapon Controls:

	A.R.C Blade:
		Attack: Swing Sword
		Alt-Attack (Held): Block, Uses stamina
		Alt-Attack (Held + Charged Mode): Block, Uses battery
		Alt-Attack (Timed just right): Offensive Guard
		Weapon Ability: Toggle Charged Mode
		Reload: Slow Swing Sword
		Reload (Charged Mode): Slash Projectile
		Reload (Volatile Charged Mode): Explosive Slash Projectile
		Offensive Guard, while active will grant damage reduction and bonus melee damage
		
	Raikiri:
		Attack: Swing Sword
		Alt-Attack (Held): Block, Uses Battery first, Then stamina
		Alt-Attack (Timed just right): Offensive Guard
		Reload: Slash Projectile
		Weapon Ability + Movement: Special Attacks
		Offensive Guard, while active will grant damage reduction and bonus melee damage
		
	Falanran:
		Attack: Swing Sword
		Alt-Attack (Held): Block, Uses stamina
		Alt-Attack (Timed just right): Offensive Guard
		Reload: Throw Sword
		Reload (Sword Thrown): Recall Sword (Costs 10 TMP)
		Weapon Ability (Sword Thrown): Telekinetic Blast (Costs 30 TMP)
		
	Hell-Steel Ignition:
		Attack: Swing Hammer
		Alt-Attack (Held): Block, Uses stamina, Uses Forge Charge if stamina is below 10
		Reload: Strike The Iron Attack, Expends Forge Charge to increase power of explosion if charge is 10 or more
		Weapon Ability: Overhead Slam, Requires 50 Forge Charge
		Reduces movement speed by 10% while equipped
		Generates Forge Charge on kill (Normal Attack Only)
		Generates Temp Armour on kill (Gives more when using Strike The Iron)
		
	Lost Reiterpallasch:
		Attack (Normal): Thrust Attack
		Attack (Gun Mode): Fire Active Vial
		Alt-Attack (Held): Block, Uses stamina, Cannot Offensive Guard
		Reload: Slash Attack
		Weapon Ability (Tap): Swap Vial
		Weapon Ability (Hold): Transform Weapon
		Generates Blood on Hit with melee attacks
		Hold Reload while at 100 Bone Marrow Gauge to Ash Flourish
		Self Sustaining melee weapon that has bursts of ranged combat
		
	S.I.S Fenic:
		Attack: Shoot
		Attack (Burst Primed): 5 Shot Burst
		Attack (Burst Primed + Green Ammo Counter): 5 Shot Burst for Free
		Alt-Attack (Held): Prime Burst Shot
		Reload: Reload Weapon
		Weapon Ability: Nothing
		Weak pistol but when used correctly it technically has infinite ammo
		
	DI2-Photo Op:
		Attack: Fast Shoot
		Alt-Attack: VERY FAST SHOOT
		Reload: Reload Weapon
		Weapon Ability: Nothing
		Slightly stronger than the fenic, uses alot of ammo but kills faster than the Geara karme but has to reload more often
		
	Griseus Special MK.II:
		Attack: Shoot
		Alt-Attack: Nothing
		Reload: Reload Weapon
		Weapon Ability: Toss Flash-Bang (Costs 1 Grenade)
		Attack with the Griseus Special or A.R.C Blade with rally health to regenerate it up to 150 max
		More powerful than the Fenic and the Photo Op
		Flash-Bangs freeze enemies for 4 seconds (Powers up with higher rally)
		
	Roaring Wolf:
		Attack: Shoot
		Attack (Spirit Eye): Execute Targets
		Alt-Attack (Held): Wolf-Trick
		Alt-Attack (Spirit Eye): Cancel Spirit Eye
		Reload: Reload Weapon
		Weapon Ability: Activate Spirit Eye (Requires 1 charge)
		Wolf-Trick while dodging projectiles to generate Spirit Eye (1 charge max this way)
		Extremely powerful revolver
		
	Cobalt Storm-2:
		Attack: Shoot
		Alt-Attack (Held): Trick
		Reload: Reload Weapon
		Weapon Ability: Storm Charge (Requires 1 charge)
		Storm Charge makes the weapon extremely powerful
		Hit enemies to stack jolt on them, enemies that die with jolt on them zap the jolt to others
		
	The Outcast:
		Attack: Shoot
		Alt-Attack: Shoot Dust Shells (Costs 25 Spirit-Eye (25% of 1 charge) & 1 Ammo)
		Reload: Reload Weapon
		Weapon Ability: Eldritch Vortex (Requires 1 charge)
		
	M6G:
		Attack: Shoot
		Alt-Attack (Held): Zoom
		Reload: Reload Weapon
		Weapon Ability: Activate Railgun Mode
		Powerful magnum that has faster firing speed than the Roaring Wolf with nearly the same power
		
	Ebony & Ivory:
		Attack: Shoot Ivory
		Alt-Attack: Shoot Ebony
		Reload: Activate Devil Trigger (50 TMP)
		Weapon Ability: Toggle Gunslinger Stance (While in Devil Trigger)
		Dual Pistols, Ivory fires Fast while Ebony has high power
		Devil Trigger doubles the damage of the guns and reduces the ammo cost
		Gunslinger Stance has no ammo cost
		Picking up Ammo for E&I replenishes 5 TMP
		
	Snake Charmer:
		Attack (Press In): Shoot First Barrel
		Attack (Release): Shoot Second Barrel
		Alt-Attack (Held): Combustion Shot (More damage, shells & penetration)
		Reload: Reload Weapon
		Weapon Ability: Nothing
		Snake Charmer has a unique mechanic, while left in your inventory and not in your hands it automatically reloads after 5 seconds
		Ridiculously high damage close range shotgun
		
	Royal Decree:
		Attack: Shoot both barrels
		Alt-Attack (Requires 40 Shadow Gauge): Load some shadow shots into the weapon
		Reload: Reload Weapon
		Weapon Ability (Requires 80 Shadow Gauge): Activate Nightmare Shroud, becoming invisible for a time
		Shadow Gauge increases as you kill enemies with Royal Decree
		Depending on the enemy that dies to Royal Decree, a death effect will occur
		A Slower pace varient to the Snake Charmer that plays with Gauge/Resource management instead of Raw Power
		
	Gungnir:
		Attack: Shoot one barrel per press
		Alt-Attack (Requires Charge): Fire the lightning gun
		Reload: Reload Weapon
		Reload (Hold): Charge the Lightning Gun
		Weapon Ability: Swap between all 3 power modes
		An ARC powered weapon that can utilize lightning to kill enemies as a replenishing resource
		
	Cruentus:
		Attack: Shoot
		Alt-Attack: Cock Weapon (Loads multiple shots as you keep cocking the weapon)
		Reload: Reload Weapon
		Weapon Ability: Toggle between Plasma and Normal shots
		Plasma shots do not penetrate targets but have little to no drop off in damage/position
		You can enable Cruentus Auto-Cock in the F4 Options if doing it manually does not suit you
		Hybrid shotgun that can work at most ranges
		
	Demons Bane:
		Attack: Shoot
		Alt-Attack: Slug Shot
		Weapon Ability: Flame Shot
		Versatile and no reload requirement, however its fire rate is consistent and stunted compared to the Cruentus's ability to mega load tons of shots
		
	Fools Oath:
		Attack: Shoot
		Alt-Attack: Magic Infused Shots
		Reload: Reload Weapon
		Weapon Ability: Nothing
		Extremely powerful wide shot shotgun
		
	Edax Omnia:
		Attack: Shoot
		Alt-Attack: Black Flamethrower
		Weapon Ability: Star Support
		Reload: Reload Weapon
		Powerful full-auto shotgun that feeds upon enemies that are covered in black flames to satiate its everlasting hunger
		As the Edax Gauge increases the weapon fires faster and becomes more efficient, past 100% it reloads automatically and gets black fire shots
		
	Geara Karme:
		Attack: Shoot
		Alt-Attack: Tri-Shot
		Reload: Reload Weapon
		Weapon Ability: Reserves Mode
		Good for quick bursts of damage and stunlocking stuff
		
	The Omen:
		Attack: Shoot
		Alt-Attack: Power Shot
		Reload: Reload Weapon
		Weapon Ability: Over-Charge
		Slow but powerful, over-charge for alternate shots on both Attack and Alt-Attack
		
	HC-Flatline:
		Attack: Shoot
		Alt-Attack: Heavy Shot
		Reload: Reload Weapon
		Weapon Ability: Hydra Convert
		A Mix of speed and power, kill enemies to generate power then expend that power to hydra convert reserve ammo to heavy shots
		
	Long-Horn X2:
		Attack: Shoot
		Alt-Attack: Focus
		Reload: Reload Weapon
		Weapon Ability: Switch Modes
		While in Red Mode, deal balanced damage to everything
		While in Blue Mode, deal little damage to non weak points but deal huge damage to weak points
		
	Acids Reign:
		Attack: Shoot
		Alt-Attack: Focus
		Reload: Reload Weapon
		Reload (Hold): Reload Weapon using Shards
		Weapon Ability: Activate/Disable Hissing Shots
		Hitting Head/Weak point shots creates Residue, Use a hissing shot to explode the residue
		Anyone dying to the residue will leave behind free ammo in the form of Shards
		
	Harbinger:
		Attack: Shoot
		Alt-Attack: Rev Up
		Reload: Nothing
		Weapon Ability: Nothing
		Kills with weapons that are not the harbinger regenerate ammo for the harbinger depending on the enemy
		Sinful souls generate chunks of ammo for the Harbinger
		
	L-Star:
		Attack: Shoot
		Alt-Attack: Quad Laser (Requires HEAT)
		Reload: Reload Weapon
		Weapon Ability: Heat Blast (Requires near-max HEAT)
		A.R.C Blade electric melee hits generate L-Star ammo
		
	King Slayer:
		Attack: Launch Grenade
		Alt-Attack: Toggle Overload
		Reload: Reload Weapon
		Weapon Ability: Toggle Inverted Mode
		Strong grenade launcher, explosives detonate on contact with organic matter
		Overloaded explosives detonate on every bounce and become extremely unpredictable
		
	Silver Ace:
		Attack: Launch Grenade
		Alt-Attack: Launch Silver Pool Grenade
		Reload: Reload Weapon
		Weapon Ability: Launch Chaff Grenade
		More designed for blanketting an area with silver explosions and pools to deny an area
		Chaff grenades mess up mechanical opponents
		
	Queens Doom:
		Attack: Launch Bolt
		Alt-Attack: Detonate Bolts
		Reload: Reload Weapon
		Weapon Ability: Hellfire Shot
		Hellfire bolts that dig into whatever they hit
		All bolt explosions stack damage and time onto the same enemy if they are already on fire
		
	Azarath:
		Attack (Hold): Charge Cosmic Arrow
		Attack (Release): Launch Cosmic Arrow
		Attack (Release with Perfect Timing): Launch Perfect Cosmic Arrow
		Alt-Attack (Hold): Charge Tether Arrow
		Alt-Attack (Release): Launch Tether Arrow
		Alt-Attack (Release with Perfect Timing): Launch Perfect Tether Arrow
		Weapon Ability (Hold): Charge Freeze Arrow
		Weapon Ability (Release): Launch Freeze Arrow
		Weapon Ability (Release with Perfect Timing): Launch Perfect Freeze Arrow
		Reload: Cancel Charge
		Cosmic Arrows that can be charged with magic to stun and pull in swarms of enemies
		Pull of a perfectly timed cosmic arrow and hit a weak point to get a larger DOT effect
		Tether arrows waste mana if not shot with enough pullback, Time it perfectly for an even stronger tether
		
	Frontliner:
		Attack: Fire Rocket
		Alt-Attack: (W.I.P)
		Reload: Reload Weapon
		Weapon Ability: Nothing
		Rocket launcher, strongest explosive weapon in the game aside from multiple queens doom bolts on a single target
		
	Anthrakia:
		Attack (Held): Hellfire Throw
		Alt-Attack (Held): Hellfire Circle
		Reload: Nothing
		Weapon Ability: Nothing
		Magic staff that drains TMP for ammo, extremely powerful
		
	Whisper:
		Attack: Shoot
		Alt-Attack (Held): Scope in
		Alt-Attack (Hold for long time): Empower Weapon
		Reload: Reload Weapon
		Weapon Ability: Nothing
		Powerful sniper rifle that while empowered, hitting a target causes a large flaming void blast to damage everything near it
		
	Grenades:
		Attack: Throw far
		Alt-Attack: Drop near
		Reload: Nothing
		Weapon Ability: Nothing
		Grenades that the combine use, now in your hands
		
	'Her' Gift:
		Attack: Use the Gift
		Alt-Attack: Nothing
		Reload: Nothing
		Weapon Ability: Nothing
		
	Datharon Form:
		Attack: Claw Swipe
		Alt-Attack: Lunge/Bite
		Reload: Fire Ball
		Weapon Ability: Soul Shards (Homing attacks)
		Kick: Roar (Freeze enemies)
		Crouch (Double Tap mid-air): Ground Slam
		Temporal Summon: Portal Attack
		Sprint (Held without Moving): Dimensional Crawl
		Attack (While Crawling): Uppercut (Hold to rise)