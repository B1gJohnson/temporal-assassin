@echo off

IF NOT EXIST ".\garrysmod\gamemodes\temporalassassin\icon24.png" (
	ECHO Detected that only the patch has been installed, please install the main gamemode files first, then install the patch afterwards.
	ECHO.
	ECHO On Discord they can be found in the #downloads-n-patches channel, weighing at around 2.5GB
	ECHO.
	ECHO You can also find links and downloads in the google docs page here - https://docs.google.com/document/d/1CoPKXJG31M5VLLoqdZ9yS9xov0OT_ud1hM5Bf-fEfak
	(
	echo https://docs.google.com/document/d/1CoPKXJG31M5VLLoqdZ9yS9xov0OT_ud1hM5Bf-fEfak
	) | clip
	ECHO The link has been set as your clipboard text, use CTRL + V to paste it into a web browser
	pause
	EXIT /B 1
)

pushd %~dp0
hl2.exe -windowed -nojoy -nosteamcontroller -nohltv +gamemode temporalassassin +maxplayers 2 +sv_lan 1 +sv_password ta_passw +p2p_enabled 0 +map ta_hub +ta_dev_simplegpumode 1
exit