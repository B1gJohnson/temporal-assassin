
- GENERAL STUFF -

	Place the garrysmod folder and the txt/bat files into steamapps/common/GarrysMod/
	
	>> YES, That means the FIRST GarrysMod folder, NOT the second one! <<
	
	To Run TA quickly, you can create a shortcut to any of the bat files included after you extract the files then put the shortcut onto your desktop

	Temporal assassin consists of a bunch of models/textures from a ton of sources, I take no credit for any models/textures in
	this Conversion/Mod, that right goes to the original creators and authors.

	Temporal Assassin' Sounds, Programming, Particle/VFX, Animations and Writing was done by myself Magenta, This package of temporal assassin is of
	the current games functions and game feel (Version 0.4494 Public-Beta)
	
	If you are running GMod without a Temporal Assassin BAT file, you will need to select Temporal Assassin from the Game Mode section at the Bottom Right of the main menu
	and then set the Server type to 2 Players with LOCAL SERVER ticked
	
	Be sure to mount HL2, EP1, EP2 and HL:Source if possible

	Extra Credits:
	Catte - Hud Quick info idea, GUI Feedback and improvements, Bug Testing and Difficulty Balance
	Alex - Resolution bug find for mis-alligned 2d text on 3d objects & Bug Testing, Cosmonaut item placements/support, Backgrounds
	Lt. Grey - Bug Testing, Forest Train, Year Long Alarm and Silesia Country item placements/support
	CV-11 - Bug Testing
	Abyssal Crow - Bug Testing, Feedback, Balancing and Content
	Killysunt - Bug Testing, Backgrounds
	Marl - Geara Karme ammo module model port
	Maple - Bunch of crazy VMT and VTF fixes/recreations
	Nessinby - Creator of the Gungnir Original Weapon for Temporal Assassin and Creator of the Lost Reiterpallasch Weapon for Temporal Assassin
	J Shield - Artwork for Mouse Cursors and Contractors
	Verssalia - Artwork/Backgrounds

	Some Copyright stuff here Temporal Assassin © Magenta Iris 2018-2023

	Hit F4 to bind custom keys and open the options menu.
	Hit F3 to watch guide videos.

	Temporal Assassin currently supports playing HALF LIFE 2, EPISODE 1, EPISODE 2, HALF LIFE: SOURCE RESIZED, STRIDER MOUNTAIN, OFFSHORE, COSMONAUT, SILESIA COUNTRY and MINERVA, along side other small map strings like Forest Train, Mission Improbable and Year Long Alarm, this list is growing constantly.
	
	Q: Can i download all the optional content with 1 download to save on time?
	A: Yes you can, you can find them here! Just place the addons and maps folder into garrysmod/garrysmod

	Includes:
	HL:S Resized
	Minerva
	Silesia Country
	Cosmonaut
	Das Roboss
	Forest Train
	Mission Improbable
	Strider Mountain
	Year Long Alarm
	All 4 base sin hunter maps

	Google Drive (Mirror 1): https://drive.google.com/file/d/1pCiFlmtNnX7Ei2AV06f4NiRa_895RRjs/view?usp=sharing
	MEGA (Mirror 2): https://mega.nz/file/vUc1BAKQ#LX9xwCpPpss3-OsWq6ByyCEN5WlAUUvXo2VAq-gIKJc

	Q: Where do i find HL:Source Resized?
	A: You can find HL:Source Resized here - https://steamcommunity.com/sharedfiles/filedetails/?id=971893221
	
	Q: Where do i find Strider Mountain?
	A: You can download it here, custom packed for Garrysmod and Temporal Assassin compatability - https://www.dropbox.com/s/as1c6ohi73e3jae/Strider_Mountain.7z?dl=0
	MIRROR: https://drive.google.com/file/d/1lnx_-hY03MAh5a1RFhCo7Q0FdMu-7Ond/view?usp=sharing
	
	Q: Where do i find Off shore?
	A: You can download it here, custom packed for Garrysmod and Temporal Assassin compatability - https://www.dropbox.com/s/4afsd9c0h331tlp/Offshore.7z?dl=0
	MIRROR: https://drive.google.com/file/d/1cEKHVy78pzN9YS_LOzY6EAeHRRuOwv45/view?usp=sharing
	
	Q: Where do i find Cosmonaut?
	A: You can download it here, custom packed for Garrysmod and Temporal Assassin compatability - https://www.dropbox.com/s/rbppdqw6c908l74/Cosmonaut.7z?dl=0
	MIRROR: https://drive.google.com/file/d/1pITEfNZsga3K0QZeu9P6pcXa-b-B1LC1/view?usp=sharing
	
	Q: Where do i find Silesia Country?
	A: You can download it here, custom packed for Garrysmod and Temporal Assassin with map 09 fixed - https://www.dropbox.com/s/ju9x0nc03br84da/Silesia_Country.7z?dl=0
	MIRROR: https://drive.google.com/file/d/1XE3WyWNmLQB-e2cksyndgYsga5chLdA8/view?usp=sharing
	
	Q: Where do i find Minerva?
	A: You can find Minerva packed for garrysmod here - https://steamcommunity.com/sharedfiles/filedetails/?id=437327606
	
	Q: Where do i find Mission Improbable?
	A: You can download it here, custom packed for Garrysmod and Temporal Assassin compatability - https://www.dropbox.com/s/xig67oj0peg3dld/MIMP.7z?dl=0
	MIRROR: https://drive.google.com/file/d/1LgKI47VK3IK_ykSw_hW7ZA6cWohrjFUe/view?usp=sharing
	
	Q: Where do i find Forest Train?
	A: You can download it here, custom packed for Garrysmod and Temporal Assassin compatability - https://www.dropbox.com/s/fdlunxvdfecwixn/ForestTrain.7z?dl=0
	MIRROR: https://drive.google.com/file/d/1Bxbd-bNpHcmllwQEEL8QhaF0F7VJGwkM/view?usp=sharing
	
	Q: Where do i find Year Long Alarm?
	A: You can download it here, custom packed for Garrysmod and Temporal Assassin compatability - https://www.dropbox.com/s/3docok45n9ad3mk/YearLongAlarm.7z?dl=0
	MIRROR: https://drive.google.com/file/d/142i3esZufp9UGovvDH42qRjdVgBPWCp2/view?usp=sharing
	
	Q: Where do i find DROB?
	A: You can download it here, custom packed for Garrysmod and Temporal Assassin compatability - https://drive.google.com/file/d/1x0MbFrcXphSpX_EQ4AT-ctYFmrajhs_e/view?usp=sharing
	MIRROR: https://mega.nz/file/PYNViKBQ#sVil7ZdTNIehvkVwP4FX-3k-vd-Y5beWcmcY3FRpck8
	
	Q: Where do i find the maps for Sin-Hunter?
	A: You can download both of them here!
	DM-Rankin - https://steamcommunity.com/sharedfiles/filedetails/?id=2201975688
	GM_Battleground - https://steamcommunity.com/sharedfiles/filedetails/?id=1425456348
	GM_GilmanCity - https://steamcommunity.com/sharedfiles/filedetails/?id=739139559
	DM_Magic_Ruin (MIRROR 1) - https://drive.google.com/file/d/1rl640v-DudZ57v6HSRYioNTJdyKLzNMw/view?usp=sharing
	DM_Magic_Ruin (MIRROR 2) - https://mega.nz/file/CNsV3JTZ#o6ZLDOLkQuztNoBj4RedYPLNwy8rJQeBvVzySlF_XfY

- HOW STUFF SAVES -

	When you move to the next level in a campaign/string of levels the game will save your data, this includes
	your weapons, health, armour, temporal mana and inventory, you can quit the game and come back to the same
	map you just left on to resume your progress/play through
	
	NEW: You can now just load into the hub and load the game from Kits terminal if the game has a loading point

- HARDWARE SUPPORT INFO -

	90, 120 & 144 HZ Monitors are supported and work as intended
	(RECOMMENDED REQUIREMENTS) Best played at 1920 X 1080 as a Native Resolution with atleast 4GBs GPU RAM with a 2.8 GHZ or higher Quad Core processor
	Minimum requirements are completely unknown, i don't have access to really old tech, nor do i have the time fiddle around with multiple rigs, Sorry.
	
	If you find your game lagging a bit too much, Please switch to the 64 Bit GMod branch via Properties > Betas > x86-64 - Chromium + 64-bit binaries

- IMPORTANT STUFF -

	Please for the love of god disable all of your other addons and stuff like that to avoid any conflicts, i don't need bug
	reports of stuff that is avoidable by yourself.

	Run Temporal Assassin as a 2 Player LAN (Local Server box ticked) server, It will not work in single player due to how source engines server/client
	realms work, If you didn't read this and you played it in single player regardless you only have yourself to blame.

	If any issues crop up with SNPCS or Custom NPCS then save the Errors, i'll add as much support for them as possible.

	If your game is hitching alot due to assets loading you can use the precache options in the EXTRA OPTIONS section of the F4 Menu

- CONTROLS -

	Tap sprint to dodge, Hold sprint during your dodge to que up Sliding, Dodging gives you i-frames VS projectiles, Sliding
	also gives you i-frames VS projectiles for the duration of the slide. You cannot slide if you have too low stamina.
	
	You can also slide from a stand still by holding Sprint (without moving) then tapping Crouch.

	Dodges start to regenerate after a short time without dodging, you can dodge while in the air too.

	Temporal Guard gives you damage reduction and all damage you take gives you Temporal Mana, being full on temporal mana reduces
	the amount of damage reduction you get. Letting go of temporal guard after getting hit freezes all enemies around you for a short
	period of time, along with who you are aiming at in a tight cone.

	Hold Jump while in jump squat for a super jump that makes you jump higher, Tapping jump instead gives you a shorter hop.

	Tapping jump while near walls mid-air allows you to jump from them in the direction you're looking.

	Spirit eye is activated by using WEAPON SKILL button for use with the Roaring Wolf Revolver, you need a full bullet charge to use it
	activating it reloads the revolver to max capacity and activates the skill, lock on by mousing over stuff and fire to initiate, This
	update lets you spirit eye enemy grenades too, low kicks also send grenades straight forward in a nice arc.
	
- ADDING YOUR OWN MUSIC -

	Music can be found in "gamemodes\temporalassassin\content\sound\TemporalAssassin\Music\"

	Please follow this naming scheme for your custom music folders and sounds, i will use CUSTOM as the prefix example
	
	Folder name - CUSTOM
	File name - CUST
	
	Music files would be the following (Can be whatever you want apart from specials, check below for those):
	CUST001.mp3
	CUST002.mp3
	CUST003.mp3
	CUST006.mp3
	
	You must create txt files with the same name as the mp3's that contain the musics artist and song name as exampled in the other folders
	So the same as the following:
	CUST001.txt
	CUST002.txt
	CUST003.txt
	CUST006.txt
	
	You need to also create a txt file that is the same name as the folder, the game reads this for what the music category will be called
	So if your folder for your music is called CUSTOM then you will need the same name as a txt inside of it with the genre name:
	CUSTOM.txt
	
	and CUSTOM.txt simple contains "My custom tracks" or whatever you wish
	
	Special files for unique events are labelled like this, you can add as many as you want:
		
		For Game Over Music -
		CUST_GAMEOVER.mp3
		CUST_GAMEOVER2.mp3
		
		For Pause Music -
		CUST_PAUSED.mp3
		CUST_PAUSED2.mp3
		
		For Chapter End Music -
		CUST_CHAPTEREND.mp3
		CUST_CHAPTEREND2.mp3
		
		For Home/Hub Music -
		CUST_HOME.mp3
		CUST_HOME2.mp3
		
		For Game Over (Eldritch Difficulty) Music -
		CUST_GAMEOVERHARD.mp3
		CUST_GAMEOVERHARD2.mp3
		
		For Hitting the Retry Button (Green) on the Game Over Screen (SHOULD BE 4 SECONDS OR SHORTER) -
		CUST_GAMECONTINUE.mp3
		CUST_GAMECONTINUE2.mp3
		
		For Hitting the Quit Button (Red) on the Game Over Screen (SHOULD BE 4 SECONDS OR SHORTER) -
		CUST_GAMEQUIT.mp3
		CUST_GAMEQUIT2.mp3
		
	When naming your music in the txt files, if you wish to create a dynamic music pack where each track has its own Action and Calm track
	then name your tracks the exact same, but with (Action) and (Calm) on the end, here is an example of 6 tracks, 3 calm states and 3 action states of 3 songs:
	
		CUST001.txt = My Pack - Fighting 1 (Action)
		CUST_HOME1.txt = My Pack - Fighting 1 (Calm)
		CUST002.txt = My Pack - Fighting 2 (Action)
		CUST_HOME2.txt = My Pack - Fighting 2 (Calm)
		CUST003.txt = My Pack - Boss 1 (Action)
		CUST_HOME3.txt = My Pack - Boss 1 (Calm)
		
	After you've named your songs as such, you must include EXPLICIT_DYNAMIC.txt in your music pack named folder too, so it would look
	something like: "gamemodes\temporalassassin\content\sound\TemporalAssassin\Music\CUSTOM\EXPLICIT_DYNAMIC.txt"
	
	If you want specific songs to play in the HUB/HOME map without cycling through Dynamic calm music, simply have some home themes that are named
	without (Calm) in their titles, here is an exaple of 3 non dynamic home themes:
	
		CUST_HOME_N1.txt = My Pack - Home Music Rock
		CUST_HOME_N2.txt = My Pack - Home Music Techno
		CUST_HOME_N3.txt = My Pack - Home Music Ambient
		
	After you've named your songs as such, you must include EXPLICIT_HOME.txt in your music pack named folder too, so it would look
	something like: "gamemodes\temporalassassin\content\sound\TemporalAssassin\Music\CUSTOM\EXPLICIT_HOME.txt"
	
	If your music pack is not using EXPLICIT_DYNAMIC in the first place, then EXPLICIT_HOME is almost useless.
	
	TEMPORAL_DYNAMIC.txt makes it so the music pack (even with dynamic music disabled) fades the current calm track over the action track while in
	Temporal Dilation (Slowmo) or Temporal Shift (Stop time).
	
	TEMPORAL_DYNAMIC_GUARD.txt requires TEMPORAL_DYNAMIC.txt in the first place, this one makes it so T-Guarding fades in (at 30% volume default) the current
	calm track over the action track for the same effect albiet more softer.
	
	TEMPORAL_DYNAMIC_BURST.txt requires TEMPORAL_DYNAMIC.txt in the first palce, this one makes it so Mana Bursting fades in (at 50% volume default) the current
	calm track over the action track for the same effect albiet softer.
	
	The NUMBER inside of the .txt's determine the fade volume.
	
	If TEMPORAL_DYNAMIC.txt has the following inside of it:
	100,000
	it will completely negate the action track (volume to 0%) and fade in the calm track to full volume (volume to 100%).
	
	If TEMPORAL_DYNAMIC.txt has the following inside of it:
	050,010
	it will lower the action track volume down to 10% and fade in the calm track to 50% volume.
	
	The rules above also apply to TEMPORAL_DYNAMIC_GUARD.txt, the first 3 numbers determine how much the calm track should volume up to
	when T-Guard is held, the second determines how the action track should volume down to.
	
	The rules above also apply to TEMPORAL_DYNAMIC_BURST.txt the same way TEMPORAL_DYNAMIC_GUARD.txt does.
	
- ANYTHING ELSE? -

	If you're curious about something feel free to ask me personally whats up.
	
	Please report any bugs or issues you may find because without the help there's no way i would be able to fix all
	of them.
	
	Materials (Original), Models (Items/Weapons) and Music portrayed in this Work is not created by myself and is
	used under the copyright law of reproduction without profit.
	
	The credits for all of those belong to their original authors and creators who's work being
	in public domain has allowed me to make this game.
	
	Animations, Rigging, Programming, Particle Effects and Sound Effects are created by myself.
	My Animations and Sound Effects may be repurposed for personal projects without my permission.
	Credit where credit is due of course.
	
	However my Programming and Particle Effects should not be repurposed or used under any other context, Please respect my
	choice on this matter.

	Magenta Iris © 2018-2022